"use strict";

(function () {
    function Star(options) {
        this.number = options.number;
        this.starSize = options.starSize;
        this.color = options.color;
        this.element = document.querySelector(options.el);
        this.isHalf = options.isHalf;
        this.starNumber = this.isHalf ? options.starNumber : Math.floor(options.starNumber);

        if (options.success && typeof options.success === 'function') {
            this.success = options.success;
        }

        this.render();
    }

    Star.prototype = {
        render: function render(el) {
            var html = '<ul style="font-size:' + this.starSize + 'px;color:' + this.color + ';margin:0;padding:0;list-style: none;">';

            for (var i = 0; i < this.number; i++) {
                html += '<li style="float: left;margin-left: 0.15rem;cursor: pointer;"><i index="' + i + '" class="fa ';

                if (i < this.starNumber) {
                    if (this.starNumber.toString().includes('.') && i === this.starNumber - 0.5 && this.isHalf) {
                        html += 'fa-star-half-o';
                    } else {
                        html += 'fa-star';
                    }
                } else {
                    html += 'fa-star-o';
                }

                html += '"></i></li>';
            }

            html += '</ul>';
            this.element.innerHTML = html;
            this.bindEvent();
        },
        bindEvent: function bindEvent() {
            // this.element.addEventListener('mousemove', this.overDeal.bind(this));
            // this.element.addEventListener('mouseout', this.outDeal.bind(this));
            // this.element.addEventListener('click', this.clickDeal.bind(this));
        },
        clickDeal: function clickDeal(e) {
            if (e.target && e.target.nodeName.toUpperCase() === 'I') {
                var isHalf = this.getXAndY(e).x < Number.parseInt(window.getComputedStyle(e.target).width) / 2;
                var index = Number.parseInt(e.target.getAttribute('index'));
                this.starNumber = isHalf && this.isHalf ? index + 0.5 : index + 1;
                this.showStarNumber(this.starNumber, true);
            }
        },
        overDeal: function overDeal(e) {
            if (e.target && e.target.nodeName.toUpperCase() === 'I') {
                var isHalf = this.getXAndY(e).x < Number.parseInt(window.getComputedStyle(e.target).width) / 2;
                var index = Number.parseInt(e.target.getAttribute('index'));
                var inx = isHalf ? index + 0.5 : index + 1;
                this.showStarNumber(inx);
            }
        },
        outDeal: function outDeal(e) {
            if (e.target && e.target.nodeName.toUpperCase() === 'I') {
                this.showStarNumber(this.starNumber);
            }
        },
        showStarNumber: function showStarNumber(starNumber, clickFlag) {
            var self = this;
            var childNodes = this.element.firstChild.childNodes,
                starNumberBy = 0;
            childNodes.forEach(function (node, index) {
                var ii = node.firstChild;

                if (index < starNumber) {
                    if (starNumber.toString().includes('.') && index === starNumber - 0.5 && self.isHalf) {
                        //有半星
                        ii.setAttribute('class', 'fa fa-star-half-o');
                    } else {
                        ii.setAttribute('class', 'fa fa-star');
                    }
                } else {
                    ii.setAttribute('class', 'fa fa-star-o');
                }
            });

            if (clickFlag && this.success) {
                this.success(this.starNumber);
            }
        },
        getXAndY: function getXAndY(event) {
            event = event || window.event;
            var mousePos = this.mouseCoords(event);
            var x = mousePos.x;
            var y = mousePos.y;
            var x1 = event.target.getBoundingClientRect().left;
            var y1 = event.target.getBoundingClientRect().top;
            var x2 = x - x1;
            var y2 = y - y1;
            return {
                x: x2,
                y: y2
            };
        },
        mouseCoords: function mouseCoords(event) {
            if (event.pageX || event.pageY) {
                return {
                    x: event.pageX,
                    y: event.pageY
                };
            }

            return {
                x: event.clientX + document.body.scrollLeft - document.body.clientLeft,
                y: event.clientY + document.body.scrollTop - document.body.clientTop
            };
        }
    };
    window.Star = Star;
})(window);