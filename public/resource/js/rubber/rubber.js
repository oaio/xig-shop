(function () {
    //解决ios橡皮筋问题
    function iosTrouchFn(el) {
        //el需要滑动的元素
        el.addEventListener('touchmove', function (e) {
            e.isSCROLL = true;
        })
        document.body.addEventListener('touchmove', function (e) {
            if (!e.isSCROLL) {
                e.preventDefault(); //阻止默认事件(上下滑动)
            } else {
                //需要滑动的区域
                var top = el.scrollTop; //对象最顶端和窗口最顶端之间的距离 
                var scrollH = el.scrollHeight; //含滚动内容的元素大小
                var offsetH = el.offsetHeight; //网页可见区域高
                var cScroll = top + offsetH; //当前滚动的距离

                //被滑动到最上方和最下方的时候
                if (top == 0) {
                    top = 1; //0～1之间的小数会被当成0
                } else if (cScroll === scrollH) {
                    el.scrollTop = top - 0.1;
                }
            }
        }, { passive: false }) //passive防止阻止默认事件不生效
    }
    //一进页面的时候执行的方法
    function isIosStopScroll() {
        var ios = !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // 判断是否为ios
        if (ios) {
            var divEl = document.getElementById('scrol'); //这是你需要定义的容器，这个容器里滑动正常，出了这个容器，页面的橡皮筋效果就被禁用掉了
            iosTrouchFn(divEl);
        }
    }

    function componentDidMount() {
        // this.init(); //这是页面加载内容的事件，可以忽略
        isIosStopScroll(); //这里开始执行
    }
    componentDidMount()
})()