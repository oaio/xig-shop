$(function () {
    //删除控制台
    $('#ribbon').html('')
})

//禁用输入端上的鼠标滚轮在焦点时的数字字段
//（防止Cromium浏览器在滚动时更改值）
$('form').on('focus', 'input[type=number]', function () {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })

    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })
})