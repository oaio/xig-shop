define(['jquery', 'bootstrap', 'form'], function ($, undefined,Form) {
    var Controller = {
        index: function () {

        },
        add: function () {
            Controller.api.bindevent();
        },
        get_user_detail: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), function (ret) {
                    setTimeout(location.href='/admin/shopuser/user/get_info', 1600);
                });
            }
        }
    };
    return Controller;
});