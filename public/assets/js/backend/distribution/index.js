define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'distribution/index/index',
                    del_url: 'distribution/index/del',
                    multi_url: 'distribution/index/multi',
                }
            });


            var table = $("#table");


            // 为表格绑定事件
            Table.api.bindevent(table);
            Form.api.bindevent($("form[role=form]"));
        },
        detail: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on("change", "#c-type", function () {
                    $("#c-pid option[data-type='all']").prop("selected", true);
                    $("#c-pid option").removeClass("hide");
                    $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
                    $("#c-pid").selectpicker("refresh");
                });
                Form.api.bindeveent($("form[role=form]"));
            }
        }
    };
    return Controller;
});