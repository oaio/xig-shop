define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/bumcommit/index',
                    add_url: 'shop/bumcommit/add',
                    edit_url: 'shop/bumcommit/edit',
                    del_url: 'shop/bumcommit/del',
                    multi_url: 'shop/bumcommit/multi',
                    table: 'bumcommit',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: true,
                pagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'icon', title: '图片链接',formatter: Table.api.formatter.image},
                        {field: 'uname', title: '用户名'},
                        {field: 'commit', title:'评价'},
                        {field: 'star', title:'评星'},
                        {field: 'bum_time', title:'虚假创建时间'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});