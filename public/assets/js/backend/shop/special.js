define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var uid=0,Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/special/index',
                    add_url: 'shop/special/add',
                    edit_url: 'shop/special/edit',
                }
            });

            var table = $("#table"), sources, source =function (a) {return sources[a]};
            $.getJSON('shop/order/source',function (s) {sources = s;});

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: __('ID'), visible: !1, operate: !1, sortable: 1},
                        {field: 'no', title: __('编号'), sortable: 1, operate: 'LIKE'},
                        {field: 'uid', title: __('Uid'), visible: !1, sortable: 1,
                            formatter: function (a) {!a&&uid++;return a;}, sortable: 1},
                        {field: 'user.name', title: __('Username'), align: 'left',
                            operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'user.nickname', title: __('微信昵称'), align: 'left',
                            operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'user.phone', title: __('注册电话'), operate: 'LIKE',
                            formatter: Table.api.formatter.search},
                        {field: 'user.area', title: __('所在地区'),
                            formatter: Table.api.formatter.label},
                        {field: 'user.address', title: __('注册地址'), align: 'left', operate: 'LIKE'},
                        {field: 'money', title: __('付款金额'), operate: !1,
                            formatter: function(a){return "￥"+(a/100).toFixed(2)}, sortable: 1},
                        {field: 'note', title: __('备注'), align: 'left', operate: 'LIKE'},
                        {field: 'origin', title: __('来源'), visible: !1, formatter: source, operate: !1},
                        {field: 'create_time', title: __('支付时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate, visible: !1}
                    ]
                ],
                sortName: 'create_time',
                search: !1,
                showExport: !0,
                showColumns: !0,
                commonSearch: !0,
                queryParams: function (params) {
                    return params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
               // uid && Fast.api.ajax('shop/self_pay/fill');
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});