define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/active/index',
                    add_url: 'shop/active/add',
                    edit_url: 'shop/active/edit',
                    del_url: 'shop/active/del',
                    multi_url: 'shop/active/multi',
                    table: 'active',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                pagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'title', title: '活动名称'},
                        {field: 'pic', title: '图片链接',formatter: Table.api.formatter.image},
                        {field: 'link', title:'链接',operate: false, formatter: Table.api.formatter.url},
                        {field: 'power', title:'权重',operate: false},
                        {field: 'is_end', title: '到期',formatter: Table.api.formatter.toggle},
                        {field: 'endtime', title:'默认到期时间',formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});