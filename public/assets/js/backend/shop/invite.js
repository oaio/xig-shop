define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/invite/index',
                    multi_url: 'shop/invite/multi',
                    table: 'active',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                pagination: true,
                columns: [
                    [
                        {field: 'id', title: '序号', sortable: 1},
                        {field: 'c.region_name', title: '城市',operate: !1},
                        {field: 'uid', title: '被邀请人id',formatter: Table.api.formatter.search,sortable:1},
                        {field: 'avatar', title: '头像',formatter: Controller.api.image,operate: !1},
                        {field: 'invitee', title: '姓名',formatter: Table.api.formatter.search},
                        {field: 'tel', title: '手机号',formatter: Table.api.formatter.search,sortable:1},
                        {field: 'fuid', title: '邀请人id',formatter: Table.api.formatter.search,sortable:1},
                        {field: 'm.num', title:'邀请人总邀请次数',operate: !1},
                        {field: 'origin', title:'邀请渠道',operate: !1},
                        {field: 'create_time', title:'邀请时间',formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        config: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            image : function (value) {
                return '<a href="' + value + '" target="_blank" class="popup">' +
                    '<img class="img-center" onerror="$1()" src="' + value + '"  style="max-height:30px;max-width:50px" /></a>';
            }
        }
    };
    return Controller;
});