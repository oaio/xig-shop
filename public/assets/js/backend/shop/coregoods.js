define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/core_goods/index',
                    add_url: 'shop/core_goods/add',
                    edit_url: 'shop/core_goods/edit',
                    del_url: 'shop/core_goods/del',
                    multi_url: 'shop/core_goods/multi',
                    table: 'goods_core'
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                pk: 'id',
                sortName: 'weigh',
                pagination: false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'), formatter: function (a,b,c) {return c+1}},
                        {field: 'weigh', title: __('Weigh')},
                        {field: 'pic', title: __('图片'), formatter: Table.api.formatter.image, operate: false},
                        {field: 'goods.name', title: __('产品名')},
                        {field: 'goods.commonprice', title: __('价格')},
                        {field: 'goods.valid_times', title: __('已售数量')},
                        {field: 'label', title: __('标签'), formatter: Controller.api.label},
                        {field: 'title', title: __('标题')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('load-success.bs.table', function (e, data) {
                if(data.total) {
                }
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));

                $(".btn-append").on("click", function () {
                    if($("dd.form-inline").length>3){
                        Layer.msg(__('You can add up to %d row', 4));
                        return !1;
                    }
                });
            },
            label: function (a) {
                if(!a.length) return '';
                var b=[],c=JSON.parse(a);c=c[0];
                if(c){for(var i in c) {
                    b.push('<span class="label label-success">' + c[i] + '</span>') ;
                }}
                return b.join(' ');
            }
        }
    };
    return Controller;
});