define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/text/index',
                    add_url: 'shop/text/add',
                    edit_url: 'shop/text/edit',
                    del_url: 'shop/text/del',
                    multi_url: 'shop/text/multi',
                    table: 'text',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: 'id',
                sortName: 'id',
                pagination: true,
                commonSearch: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'nickname', title: '昵称'},
                        {field: 'shop_admin', title:'不知打牌'},
                        {field: 'status', title: '是否删除', searchList: {1: '删除', 0: '不删除'}, formatter: Table.api.formatter.toggle},
                        // {field: 'hot', title: '权重', operate: 'BETWEEN', sortable: true},
                        // {field: 'update_time', title: __('Gender'), visible: false},
                         {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    debugger;
    return Controller;
});