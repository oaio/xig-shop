define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/distributionrule/index',
                    add_url: 'shop/distributionrule/add',
                    edit_url: 'shop/distributionrule/edit',
                    del_url: 'shop/distributionrule/del',
                    multi_url: 'shop/distributionrule/multi',
                    table: 'distributionrule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                pagination: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'name', title: '类别'},
                        {field: 'type', title: '分成比例',formatter:function(a,b,i){
                            if(a==1){

                                return b.rule+'%';
                            
                            }
                            else if(a==2){
                            
                                return '每单'+b.rule+'元';
                            
                            }
                        }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(".btn-add").data("area", ["300px", "400px"]);
            table.on('post-body.bs.table', function () {
                $(".btn-editone").data("area", ["300", "400px"]);
            });

        },
        add: function () {
            Controller.api.bindevent();
            $(document).on('change', 'select', function() {
                $("input:first").attr("value", $(this).find('option:selected').html());
            });
        },
        edit: function () {
            Controller.api.bindevent();
            var cur,input,disabled=function (e) {
                e.attr("disabled", !0).val('').removeAttr('value');
            };
            $('input:text:first').attr('data-rule', 'number;range(0~100)');

            $('input:radio').on('click', function() {
                cur = $(this).val()-1;
                input =$('input:text');
                input.eq(cur).attr("name","row[rule]").attr("disabled", !1);
                disabled(input.eq(cur?0:1).attr("name",""));
            }).each(function (i,e) {
                input= $('input:text').eq(i);
                if (!$(e).attr('checked')) {
                    disabled(input);
                }
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});