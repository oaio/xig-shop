define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    table: 'order',
                }
            });

            var table = $("#table"),status, schedule = function (a,r) {
                return Table.api.formatter.status.call(this, status[a], r);
            };
            $.getJSON('shop/order/status',function (s) {status = s;});
            // 初始化表格
            table.bootstrapTable({
                url: 'shop/relate/index',
                search: !1,
                sortName: 'create_time',
                pagination: true,
                columns: [
                    [
                        {field: 'id', title: '序号', operate: false, formatter: function (a,b,c) {
                                a=c+1;
                                if(b.mark&&b.mark.length>0)a += '<img src="/assets/img/ac.png" style="width:38px;height:18px; ' +
                                    'transform: rotate(-30deg);position: absolute;opacity: 0.5;" />';
                                return a;
                            }},
                        {field: 'goods_name', title: __('服务项目'), operate: 'LIKE'},
                        {field: 'snorder', title:  __('订单编号'), align: 'left'},
                        {field: 'nickname', title:'用户', operate: 'LIKE'},
                        {field: 'mobile', title:'联系电话', operate: 'LIKE',sortable: true},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', visible: !1, formatter: Table.api.formatter.search},
                        {field: 'region_name', title: '区域',formatter: Table.api.formatter.label},
                        {field: 'num', title:'数量', operate: false},
                        {field: 'shouldpay', title:'总价格', operate: false,
                            formatter: function (a,c) {
                                var n=0,l=c.paylog;
                                if(!c || !l || l.length<2) return a;
                                for(var i in l )n+=l[i].total_fee - 0;
                                return (n/100).toFixed(2);
                            }},
                        {field: 'create_time', title: __('下单时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange',sortable: true},
                        {field: 'schedule', title: __('订单状态'), formatter: schedule,sortable: true, operate: 'IN',
                            searchList:{"0":"新订单", 1:"待客服指派",2:"待师傅确认",3:"待服务",
                                5:"服务中",6:"服务完成",7:"CLOSE",9:"待办"},custom: {"CLOSE":"gray",
                                "新订单":"success","待师傅确认":"info","待办":"danger", "待客服指派":"warning"}},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'mark',
                                    title: __('标记客诉'),
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    icon: 'fa fa-bookmark',
                                    url: 'shop/todo/sign'
                                },
                                {
                                    name: 'detail',
                                    title: __('查看订单'),
                                    text: __('订单信息'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/close/detail'
                                }],
                            formatter: Controller.operate}
                    ]
                ],
                queryParams: function (params) {
                    params.sort = params.sort.replace('mobile', 'o.mobile')
                        .replace(/^create_time/, 'o.create_time');
                    params.op = params.op.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    params.filter = params.filter.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    return params;
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-warning").data("area", ["300px", "350px"]);
                table.find("img").each(function (i,e) {
                    var p = $(e).parent().parent().find("td:eq(1)").position();
                    $(e).css({left:p.left+"px",top:p.top+"px"});
                });
                $(document).on('click', '.btn-re', function () {
                    var that = this,
                        url = Backend.api.replaceids(that, $(that).attr('href'));
                    Backend.api.addtabs(url, status[$(that).data('title')]);
                    return false;
                })
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
            $(document).on("click", ".btn-close", function () {
                Layer.closeAll();
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
        operate: function (a, b, c) {
            var buttons = $.extend([], this.buttons || []),
                us = ["customer","customerassign","customersure", "customerwait",
                    "close","customerin","customerend","close","","todo"];
            buttons.push({
                icon: 'fa fa-share',
                title: __('Edit'),
                text: __('跳转'),
                extend: 'data-title="'+b.schedule+'"',
                classname: 'btn btn-xs btn-success btn-re',
                url: 'shop/'+us[b.schedule]+'/index'
            });
            return Table.api.buttonlink(this, buttons, a, b, c, 'operate');
        },
        counter: null
    };
    return Controller;
});