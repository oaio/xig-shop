define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    Table.api.formatter.trigger = function (value, row, index) {
        return row.type?'':Table.api.formatter.toggle.call(this, value, row, index);
    }
    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 商品名称 / 评论内容"};

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/comment/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'shop/comment/top',
                    table: 'comment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: "id",
                sortName: "create_time",
                commonSearch: !0,
                columns: [
                    [
                        {field: 'id', title: __('序号'), formatter: function (a,b,c) {return c+1}},
                        {field: 'user.name', title: __('姓名'),  operate: !1},
                        {field: 'user.phone', title: __('手机号码'), operate: !1},
                        {field: 'shop.snorder', title: __('订单编号')},
                        {field: 'goods.name', title: __('商品名称')},
                        {field: 'star', title: __('评论星级'),width: "139px", formatter: Controller.api.rate},
                        {field: 'comment', title: __('评论内容'), formatter: Controller.api.note},
                        {field: 'type', title: __('评论类型'), formatter: Table.api.formatter.normal,
                            searchList:{"0":__('正常评论'),"1":__('追加评论')}, operate: !1},
                        {field: 'is_show', title: __('是否显示'), formatter: Table.api.formatter.status,
                            searchList:{"0":__('显示'),"1":__('隐藏')}, operate: !1},
                        {field: 'is_top', title: __('是否置顶'), formatter: Table.api.formatter.trigger},
                        {field: 'create_time', title:  __('评论时间'), formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            formatter:  function (value, row, index) {
                                var buttons = $.extend([], [row.is_show?this.buttons[1]:this.buttons[0]]);
                                return Table.api.buttonlink(this, buttons, value, row, index, 'buttons');
                            },
                            buttons:[
                                {
                                    name: 'ajax',
                                    text: __('隐藏评论'),
                                    title: __('隐藏'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'shop/comment/hide',
                                    confirm: '确认隐藏',
                                    success: function (data, ret) {
                                        $('.btn-refresh').trigger('click');
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: __('显示评论'),
                                    title: __('显示'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'shop/comment/show',
                                    confirm: '确认显示',
                                    success: function (data, ret) {
                                        $('.btn-refresh').trigger('click');
                                    }
                                },
                            ]
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('.search').css({'width':'360px','margin-right':'10px'});
            $('button[name="commonSearch"]').off('click').on('click',function () {
                $('.btn-refresh').trigger('click');
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    window.Layer.closeAll();
                });
                $("#c-avatar").bind("change",function() {
                    $(this).isValid();
                });
            },
            rate:function (v) {
                for (var i = 0, html = ''; i < 5; i++) {
                    html += '<i class="icon-rate' + (i < v ? " rate-solid" : "") + '"></i>';
                }
                return html;
            },
            note:function (v,r) {
                return v.length < 18 ? v : '<a href="/admin/shop/comment/note/ids/'.concat(r.id)
                    .concat('" class="btn-dialog" data-area="300">' + v.slice(0, 16) + '…</a>');
            }
        }
    };
    return Controller;
});