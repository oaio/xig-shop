define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/serverassess/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'shop/serverassess/multi',
                    table: 'serverassess',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "电话/姓名/详情";};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search:false,
                commonSearch:true,
                sortName: "assess.id",
                showExport:true,
                columns: [
                    [
                        {field: 'id', title: '序号',operate:false},
                        {field: 'user.phone', title: '电话'},
                        {field: 'user.name', title: '用户名'},
                        {field: 'goods_name', title: '购买项目'},
                        {field: 'emp.name', title: '服务人员',operate:'FIND_IN_SET',visible:0},
                        {field: 'emp', title: '服务人员',operate:false,formatter:function(val,arr,key){
                            
                            var nick='';

                            for (var i = val.length - 1; i >= 0; i--) {
                                nick+=' '+val[i]['name'];
                            }

                            return nick;
                        }},
                        {field: 'avg_assess', title: '综合评分',operate:false,sortable: true},
                        {field: 'detail', title: '评价内容',operate:false,},
                        {field: 'create_time', title: '评价时间', formatter: Table.api.formatter.datetime, operate:false},
                        {field: 'assess.create_time', title: __('评价时间'),visible: false, formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('操作'), table: table, 
                         events: Table.api.events.operate,
                         buttons: [{
                                name: 'score',
                                text:'评分',
                                icon: 'fa fa-superpowers',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog btn-score',
                                url: 'shop/serverassess/score'
                            },{
                                name: 'mydetail',
                                text:'详情',
                                icon: 'fa fa-superpowers',
                                classname: 'btn btn-info btn-xs btn-dialog btn-mydetail',
                                url: 'shop/serverassess/mydetail'
                            }],
                         formatter: function(value, row, index){
                                var table = this.table;
                                // 操作配置
                                var options = table ? table.bootstrapTable('getOptions') : {};
                                // 默认按钮组
                                var buttons = $.extend([], this.buttons || []);
                                // 所有按钮名称
                                if(row.status==0) {
                                     buttons.push({
                                        name: 'check',
                                        text:'审核',
                                        icon: 'fa fa-check-square',
                                        classname: 'btn btn-danger btn-xs btn-dialog btn-check',
                                        url: 'shop/serverassess/check'
                                    });
                                };
                                return Table.api.buttonlink(this, buttons, value, row, index, 'operate');
                         }
                     }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);

            /**
             * 全部
             * @Author   ksea
             * @DateTime 2019-05-23T17:14:34+0800
             * @param    {[type]}                 ) {                           var options [description]
             * @return   {[type]}                   [description]
             */
            $(document).on('click', '.btn-bt-default', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {"assess.status":-1}));
                    params.op = JSON.stringify($.extend({}, params.op,{"assess.status":">"}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });

            /**
             * 待审核
             * @Author   ksea
             * @DateTime 2019-05-23T17:13:48+0800
             * @param    {[type]}                 ) {                           var options [description]
             * @return   {[type]}                   [description]
             */
            $(document).on('click', '.btn-status-on', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {"assess.status":0}));
                    params.op = JSON.stringify($.extend({}, params.op,{"assess.status":"="}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });
            /**
             * 审核成功
             * @Author   ksea
             * @DateTime 2019-05-23T17:13:48+0800
             * @param    {[type]}                 ) {                           var options [description]
             * @return   {[type]}                   [description]
             */
            $(document).on('click', '.btn-status-suc', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {"assess.status":1}));
                    params.op = JSON.stringify($.extend({}, params.op,{"assess.status":"="}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });
            /**
             * 审核失败
             * @Author   ksea
             * @DateTime 2019-05-23T17:14:11+0800
             * @param    {[type]}                 ) {                           var options [description]
             * @return   {[type]}                   [description]
             */
            $(document).on('click', '.btn-status-err', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {"assess.status":2}));
                    params.op = JSON.stringify($.extend({}, params.op,{"assess.status":"="}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });

            table.on('post-body.bs.table', function () {
                $(".btn-score").data("area", ["300px", "50%"]);
                $(".btn-mydetail").data("area", ["400px", "50%"]);
                $(".btn-check").data("area", ["200px", "25%"]);

            });
        },
        check: function(){
            $("head").append("<style>.content{min-height:auto}</style>");
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});