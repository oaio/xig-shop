define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var table = $("#table"),sources,source = function (a) {return sources[a]},
        Controller = {
        index: function () {
            $.getJSON('shop/order/source',function (s) {sources = s;});
            Table.api.init({
                extend: {
                    index_url: 'shop/close/index',
                    add_url: '',
                    edit_url: 'shop/close/detail',
                    del_url: '',
                    multi_url: '',
                    table: 'order',
                }
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: !1,
                sortName: 'create_time',
                pagination: !0,
                showExport: !0,
                showColumns: !0,
                columns: [
                    [
                        {field: 'id', title: '序号', operate: false, formatter: function (a,b,c) {
                                a=c+1;
                                if(b.mark&&b.mark.length>0)a += '<img src="/assets/img/ac.png" style="width:38px;height:18px; ' +
                                    'transform: rotate(-30deg);position: absolute;opacity: 0.5;" />';
                                return a;
                            }},
                        {field: 'goods_name', title: __('服务项目'), operate: 'LIKE'},
                        {field: 'snorder', title:  __('订单编号'), align: 'left', operate: 'LIKE'},
                        {field: 'nickname', title:'用户', operate: 'LIKE'},
                        {field: 'mobile', title:'联系电话', operate: 'LIKE',sortable: true},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', visible: !1, formatter: Table.api.formatter.search},
                        {field: 'user.name', title: '注册姓名', operate: !1, visible: !1},
                        {field: 'user.nickname', title: '注册昵称', operate: !1, visible: !1},
                        {field: 'region_name', title: '区域',formatter: Table.api.formatter.label},
                        {field: 'address', title:'地址', visible: !1, operate: false},
                        {field: 'num', title:'数量', operate: false},
                        {field: 'shouldpay', title: '总价', operate: !1, sortable: 1},
                        {field: 'actualpay', title:'实付', visible: !1, operate: !1},
                        {field: 'actualpay', title:'抵扣', visible: !1, operate: !1, formatter: function (a,c) {return c.shouldpay-a}},
                        {field: 'expand', title: '追加', visible: !1, operate: !1, formatter: function (a) {
                                let n = 0;if (!a || a.length < 1) return '-';
                                for (let i in a)n += a[i].money - 0;
                                return n.toFixed(2);
                            }
                        },
                        {field: 'cate', title:'分类',searchList: cate,formatter:Table.api.formatter.label, visible: !1},
                        {field: 'server_many.0.emp', title: __('服务人员'), operate: false,formatter:function(val,arr,key){
                            
                            var nick='';

                            for (var i = val.length - 1; i >= 0; i--) {
                                nick+=' '+val[i]['name'];
                            }

                            return nick;
                        }},
                        {
                            field: 'service.sfuserid', title: '服务人员', operate: 'FIND', visible: !1,
                            addclass: 'selectpage',
                            extend: 'data-source="shop/employee/get" data-field="name" autocomplete="off"'
                        },
                        {field: 'admin', title:'派单人员', operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'create_time', title: __('下单时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange',sortable: true, extend:'autocomplete="off"'},
                        {field: 'service.fu_start', title:'开始时间',  formatter: Table.api.formatter.datetime,visible: !1,
                            operate: 'RANGE', addclass: 'datetimerange',sortable: true, extend:'autocomplete="off"'},
                        {field: 'service.fu_end', title:'完成时间',  formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange',sortable: true, extend:'autocomplete="off"'},
                        {field: 'counting', title:'服务时长', formatter: Controller.api.counting, operate: false,sortable: true},
                        {field: 'service.update_time', title:'完结时间',  formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange',sortable: true, extend:'autocomplete="off"'},
                        {field: 'schedule', title: __('订单状态'), formatter: function (a,b) {
                                return Table.api.formatter.status.call(this, a==7 ? '完结' : '已取消', b);
                            },operate: false, custom: {"完结":"success","已取消":"danger"}},
                        {field: 'service.visit_note', title:'回访记录', visible: !1, operate: !1},
                        {field: 'service.note', title:'客户留言', visible: !1, operate: !1},
                        {field: 'service.usernote', title:'师傅备注', visible: !1, operate: !1},
                        {field: 'service.servernote', title:'客服备注', visible: !1, operate: !1},
                        {field: 'type', title:'团购', searchList:{0:"否",1:"是",2:"否"},
                            visible: !1, operate: !1, formatter:Table.api.formatter.normal},
                        {field: 'orgin', title:'来源', formatter:source, visible: !1, operate: !1},
                        {field: 'coupons', title:'卷号', visible: !1, operate: !1},
                        {field: 'eventtype', title:'扫码', visible: !1, operate: !1,
                            formatter: function(a){return a&&a.length&&a.length>1?"是":"否"}},
                        {field: 'cash', title:'结算', searchList:{0:"否",1:"是"}, visible: !1,
                            operate: !1,formatter:Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'edit',
                                    icon: 'fa fa-user',
                                    text: __('人工处理'),
                                    extend: 'data-toggle="tooltip" data-title="订单人工处理"',
                                    classname: 'btn btn-xs btn-success btn-editone',
                                    url: 'shop/todo/edit'
                                },
                                {
                                    name: 'mark',
                                    title: __('标记客诉'),
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    icon: 'fa fa-bookmark',
                                    url: 'shop/todo/sign'
                                },
                                {
                                    name: 'detail',
                                    title: __('订单流转记录'),
                                    text: __('查看订单流转'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/todo/diary'
                                }],
                            formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams: function (params) {
                    if(ids>0)params.ids = ids;
                    params.sort = params.sort.replace('mobile', 'o.mobile')
                        .replace('create_time','o.create_time');
                    params.op = params.op.replace('"name"','"e.name"')
                        .replace('nickname','o.nickname')
                        .replace('admin', 'a.nickname')
                        .replace('mobile', 'o.mobile').replace('cate', 'g.cate')
                        .replace('create_time', 'o.create_time');
                    params.filter = params.filter.replace('"name"','"e.name"')
                        .replace('nickname','o.nickname')
                        .replace('admin', 'a.nickname')
                        .replace('mobile', 'o.mobile').replace('cate', 'g.cate')
                        .replace('create_time', 'o.create_time');
                    return params;
                },
            });
            Table.api.bindevent(table);
            /**
             * 全部
             * @Author   ksea
             * @DateTime 2019-05-23T17:14:34+0800
             * @param    {[type]}                 ) {                           var options [description]
             * @return   {[type]}                   [description]
             */
            $(document).on('click', '.btn-bt-default', function () {

                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.sort = params.sort.replace('create_time','o.create_time');
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {schedule:-1}));
                    params.op = JSON.stringify($.extend({}, params.op,{schedule:">"}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });
            $(document).on('click', '.btn-schedule-on', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.sort = params.sort.replace('create_time','o.create_time');
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {schedule:4}));
                    params.op = JSON.stringify($.extend({}, params.op,{schedule:"="}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });

            $(document).on('click', '.btn-schedule-off', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.sort = params.sort.replace('create_time','o.create_time');
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {schedule:7}));
                    params.op = JSON.stringify($.extend({}, params.op,{schedule:"="}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });

            table.on('post-body.bs.table', function () {
                $(".btn-info").data("area", ["300px", "500px"]);
                $(".btn-warning").data("area", ["300px", "350px"]);
                top.$('ul.nav-tabs > li.active').find('span').html('CLOSE');
                table.find("img").each(function (i,e) {
                    var p = $(e).parent().parent().find("td:eq(1)").position();
                    $(e).css({left:p.left+"px",top:p.top+"px"});
                });
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
            $(document).on("click", ".btn-close", function () {
                Layer.closeAll();
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            counting: function (a,b) {b=b.service;
                if(b.fu_end-0<1)return '无';
                var a=b.fu_end-b.fu_start,
                    h=parseInt(a/3600),b=a-h*3600,s=parseInt(b/60);
                return ['<span class="hour">',h,'</span>小时<span class="minute">',s,
                    '</span>分钟<span class="second hide">',b,'</span>'].join('')
            }
        },
        counter: null
    };
    return Controller;
});