define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/todo/index',
                    add_url: '',
                    edit_url: 'shop/todo/edit',
                    del_url: '',
                    multi_url: '',
                    table: 'order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: !0,
                showExport: !0,
                showColumns: !0,
                sortName: 'create_time',
                pagination: !0,
                columns: [
                    [
                        {field: 'id', title: '序号', operate: false, formatter: function (a,b,c) {
                                a=c+1;
                                if(b.mark&&b.mark.length>0)a += '<img src="/assets/img/ac.png" style="width:38px;height:18px; ' +
                                    'transform: rotate(-30deg);position: absolute;opacity: 0.5;" />';
                                return a;
                            }},
                        {field: 'goods_name', title: __('服务项目'), operate: 'LIKE'},
                        {field: 'snorder', title:  __('订单编号'), align: 'left'},
                        {field: 'nickname', title:'用户', operate: 'LIKE'},
                        {field: 'name', title:'姓名', operate: !1, visible: !1},
                        {field: 'mobile', title:'联系电话', operate: 'LIKE', sortable: true},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', visible: !1, formatter: Table.api.formatter.search},
                        {field: 'region_name', title: '区域',formatter: Table.api.formatter.label},
                        {field: 'address', title:'地址', operate: !1, visible: !1},
                        {field: 'num', title:'数量', operate: !1},
                        {field: 'shouldpay', title:'总价格', operate: !1,
                            formatter: function (a,c) {
                                var n=0,l=c.paylog;
                                if(!c || !l || l.length<2) return a;
                                for(var i in l )n+=l[i].total_fee - 0;
                                return (n/100).toFixed(2);
                            }},
                        {field: 'todo.admin', title:'操作人员', operate: 'LIKE'},
                        {field: 'create_time', title: __('下单时间'), formatter: Table.api.formatter.datetime,
                        operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'counting', title:'距离下单已', formatter: Controller.api.counting, sortable: true, operate: false},
                        {field: 'todo.create_time', title: __('转入待办时间'), formatter: Table.api.formatter.datetime,operate: false, sortable: true},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'edit',
                                    icon: 'fa fa-user',
                                    title: __('Edit'),
                                    text: __('人工处理'),
                                    extend: 'data-toggle="tooltip"',
                                    classname: 'btn btn-xs btn-success btn-editone',
                                    url: 'shop/todo/edit'
                                },
                                {
                                    name: 'mark',
                                    title: __('标记客诉'),
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    icon: 'fa fa-bookmark',
                                    url: 'shop/todo/sign'
                                },
                                {
                                    name: 'detail',
                                    title: __('订单流转记录'),
                                    text: __('查看订单流转'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/todo/diary'
                                }],
                            formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    params.sort = params.sort.replace('mobile', 'o.mobile')
                        .replace(/^create_time$/i, 'o.create_time');
                    params.op = params.op.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    params.filter = params.filter.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    return params;
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                var h,m,v,over=0;
                clearInterval(Controller.counter);
                Controller.counter=setInterval(function(){
                    over++;
                    $("span.minute").each(function (i,e){
                        v=$(e).parent().find('span:last-child').html()-0+over;
                        if(v%60==0){
                            m=$(e).html()-0+1;
                            $(e).html(m);
                            if(m==60){
                                h = $(e).parent().find('span:first-child');
                                h.html(h.html()-0+1);
                                $(e).html(0);
                            }
                        }
                    });
                }, 1000);
                $(".btn-info").data("area", ["300px", "500px"])
                $(".btn-warning").data("area", ["300px", "350px"])
                $(".btn-editone").data("title","订单人工处理");
                top.$('ul.nav-tabs > li.active').find('span').html('待办池');
                ids>0&&top.$('#tab_194').find('i.close-tab').trigger('click');
                table.find("img").each(function (i,e) {
                    var p = $(e).parent().parent().find("td:eq(1)").position();
                    $(e).css({left:p.left+"px",top:p.top+"px"});
                });
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
        },
        mark: function () {
            Controller.api.bindevent();
        },
        sign: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            $(document).on("click", ".btn-cancel", function () {
                if ('undefined' != typeof eid)
                Layer.confirm('确认要取消订单吗？',
                    {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
                    function () {
                            parent.window.location = '/admin/shop/todo/cancel/ids/'+eid;
                    });
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            counting: function (a) {
                var h=parseInt(a/3600),b=a-h*3600,s=parseInt(b/60);
                return ['<span class="hour">',h,'</span>小时<span class="minute">',s,
                    '</span>分钟<span class="second hide">',b,'</span>'].join('')
            }
        },
        counter: null
    };
    return Controller;
});