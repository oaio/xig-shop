define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/customer/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: 'customer',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: "id",
                sortName: "server.id",
                search:false,
                commonSearch:true,
                showExport:true,
                columns: [
                    [
                        {field: 'server.id', title: __('序号'), formatter: function (a,b,c) {return c+1}},
                        {field: 'order.goods_name', title: __('服务项目'),operate: 'LIKE',sortable: true},
                        {field: 'order.snorder', title: __('订单编号'),operate: 'LIKE',sortable: true},
                        {field: 'order.nickname', title: __('用户'),operate: 'LIKE',sortable: true},
                        {field: 'order.mobile', title: __('联系电话'),operate: 'LIKE',sortable: true},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'order.goods_origin', title:  __('商品类型'), sortable: false,formatter:function(a,b,c){
                            if(a.indexOf('shop_goods_bargain')!=-1){
                                return '特惠商品';
                            }
                            else{
                                return '普通商品';
                            }
                        }},
                        {field: 'region_name', title:  __('区域'), operate: 'LIKE',sortable: true},
                        {field: 'order.num', title: __('数量'),  operate: 'LIKE',sortable: true},
                        {field: 'shouldpay', title: __('总价格'), operate: 'LIKE',sortable: true},
                        {field: 'temp.d_time', title: __('客户预约时间'),operate: !1,sortable: !1, formatter:(a,b)=>{return b.temp.d_data+" "+a;}},
                        {field: 'server.create_time', title: __('下单时间'),visible: false, formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'order.create_time', title: __('下单时间'), formatter: Table.api.formatter.datetime, operate: false, sortable: true},
                        {field: 'create_time', title: __('距下单已过'), formatter: Controller.api.counting, operate: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'deal',
                                    title: __('订单人工处理'),
                                    text: __('人工处理'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/customer/deal'
                                },
                                {
                                    icon: 'fa fa-check-square',
                                    title: __('一键完成'),
                                    extend: {title: __('一键完成')},
                                    classname: 'btn btn-xs btn-danger btn-complete btn-ajax',
                                    confirm: __('Are you sure to completed this order?'),
                                    url: 'shop/customer/complete',
                                    success: function () {
                                        $('.btn-refresh').trigger('click');
                                    }
                                }],
                            formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                var h,m,v,over=0;
                clearInterval(Controller.counter);
                Controller.counter=setInterval(function(){
                    over++;
                    $("span.minute").each(function (i,e){
                        v=$(e).parent().find('span:last-child').html()-0+over;
                        if(v%60==0){
                            m=$(e).html()-0+1;
                            $(e).html(m);
                            if(m==60){
                                h = $(e).parent().find('span:first-child');
                                h.html(h.html()-0+1);
                                $(e).html(0);
                            }
                        }
                    });
                }, 1000);
                $(".btn-info").data("area", ["800px", "800px"])
                $(".btn-editone").data("title","订单人工处理").data("area", ["800px", "800px"]);
            });
            $(document).on("click", ".btn-close", function () {
                Layer.closeAll();
            });
        },
        add: function () {
            Controller.api.bindevent();
                $("button.btn-close").click(function () {
                    window.Layer.closeAll();
                });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        deal: function(){

            var table = $("#table");
            Form.api.bindevent($("form[role=form]"));
            $("button.btn-close").click(function () {
                window.Layer.closeAll();
            });
            Table.api.init({
                extend: {
                    index_url: 'shop/customer/deal',
                    table: 'deal',
                }
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    window.Layer.closeAll();
                });
                $("#c-avatar").bind("change",function() {
                    $(this).isValid();
                });
            },
            counting: function (a) {
                a= parseInt((new Date().getTime())/1000)-a;
                var h=parseInt(a/3600),b=a-h*3600,s=parseInt(b/60);
                return ['<span class="hour">',h,'</span>小时<span class="minute">',s,
                    '</span>分钟<span class="second hide">',b,'</span>'].join('')
            }
        }
    };
    return Controller;
});