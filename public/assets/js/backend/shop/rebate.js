define(['jquery', 'table', 'form','bootstrap-datepicker'], function ($, Table, Form, undefined) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "订单编号/姓名";};
            Table.api.init({
                extend: {
                    index_url: 'shop/rebate/index',
                    table: 'rebate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'a.id',
                commonSearch: true,
                columns: [
                    [
                        {field: 'id', title: __('序号'), operate:false, formatter: function (a,b,c) {return c+1}},
                        {field: 'trade_no', title: __('订单编号'), operate:false, formatter:function (a,b,c) {
                                return b.pay_type==1?a:b.note;
                            }},
                        //{field: 'username', title: __('姓名'),  operate:false},
                        {field: 'total_fee', title: __('订单金额（元）'), operate:false},
                        {field: 'rebate_fee', title: __('分销佣金（元）'), operate:false},
                        {field: 'order_time', title: __('下单时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('.search').css({'width':'260px','margin-right':'10px'});
            $('button[name="commonSearch"]').off('click').on('click',function () {
                $('.btn-refresh').trigger('click');
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});