require.config({
    paths: {
        'laydate': '/resource/js/laydate/laydate'
    }
});
define(['jquery', 'bootstrap', 'backend', 'table', 'form','laydate'], function ($, undefined, Backend, Table, Form,laydate) {

    var Controller = {
        index: function () {
            Table.api.init();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                $(this).unbind('shown.bs.tab');
            });
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        table:{
            mixanalysis: function(){
                $.getJSON('shop/Analysis/ptcity',function (porgindef) {
                    var citylist={};
                    var porgin={};
                    for (var i = 0; i < porgindef.pt.length; i++) {
                        if(porgindef.pt[i]&&porgindef.pt[i]['name']!=''){
                            debugger;
                            porgin[porgindef.pt[i]['value']]=porgindef.pt[i]['name'];
                        }
                    }
                    for (var i = 0; i < porgindef.city.length; i++) {
                            citylist[porgindef.city[i]['value']]=porgindef.city[i]['name'];
                    }
                    debugger;
                    var table = $("#table");
                    // 初始化表格
                    table.bootstrapTable({
                        url: 'shop/analysis/mixanalysis',
                        toolbar: '#toolbar',
                        searchFormVisible:true,
                        search: false, //是否启用快速搜索
                        commonSearch: true, //是否启用通用搜索
                        pageSize: 20,
                        pageList: [20, 45, 60, 'All'],
                        showExport:true,
                        columns: [
                            [
                                {field: 'loginin', title: __('登陆人数'), operate: false, sortable: true},
                                {field: 'reviewuser', title: __('绑定人数'), operate: false, sortable: true},
                                {field: 'payonline', title: __('线上下单人数'), operate: false, sortable: true},
                                {field: 'paystorenum', title: __('店铺支付人数'), operate: false, sortable: true},
                                {field: 'payonlinestorenum', title: __('店铺商户线上下单人数'), operate: false, sortable: true},
                                {field: 'orgin', title:'平台类型', visible: false, searchList:{
                                    "1":"公众号",
                                    "2":"小程序",
                                },formatter:function(a,b,c){
                                    if(a==1){
                                        return '公众号';
                                    }else if(a==2){
                                        return '小程序';
                                    }
                                }},
                                {field: 'city_code', title:'城市类型',visible: false, searchList:citylist,formatter:function(a,b,c){
                                    debugger;
                                    return citylist[a];
                                }/*visible:0*/},
                                {field: 'create_time', title: __('日期'), formatter: Controller.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true}
                            ]
                        ]
                    });

                    table.on('load-success.bs.table', function (e, data) {
                        $("#loginin").text(data.extend.loginin);
                        $("#reviewuser").text(data.extend.reviewuser);
                        $("#payonline").text(data.extend.payonline);
                        $("#paystorenum").text(data.extend.paystorenum);
                        $("#payonlinestorenum").text(data.extend.payonlinestorenum);
                    });

                    // 为表格绑定事件
                    Table.api.bindevent(table);
                    $('.search').css({'width':'360px','margin-right':'10px'});
                    $('button[type=submit]').html('搜索');
                    $('#nickname').attr('name','shop_order.nickname');
                    $('input[name=nickname-operate]').data('name','shop_order.nickname');

                });
       
            },
            //用户购买详情
            buyanalysis: function(){
                $.getJSON('shop/Analysis/ptcity',function (porgindef) {
                    var citylist={};
                    var porgin={};
                    for (var i = 0; i < porgindef.pt.length; i++) {
                        if(porgindef.pt[i]&&porgindef.pt[i]['name']!=''){
                            debugger;
                            porgin[porgindef.pt[i]['value']]=porgindef.pt[i]['name'];
                        }
                    }
                    for (var i = 0; i < porgindef.city.length; i++) {
                            citylist[porgindef.city[i]['value']]=porgindef.city[i]['name'];
                    }
                    debugger;
                    var table = $("#table2");
                    var count,count1,sumcount1,sumcount2,sumcount3=0;
                    table.bootstrapTable({
                        url: 'shop/analysis/buyanalysis',
                        toolbar: '#toolbar2',
                        sortName: 'create_time',
                        searchFormVisible:true,
                        search: false, //是否启用快速搜索
                        commonSearch: true, //是否启用通用搜索
                        pageSize: 20,
                        pageList: [20, 45, 60, 'All'],
                        showExport:true,
                        columns: [
                            [
                                {field: 'uid', title: '用户ID'},
                                {field: 'user.nickname', title: __('微信昵称'), operate: false},
                                {field: 'userpaynum', title: __('用户消费次数'), operate: false, sortable: true},
                                {field: 'suserpaysum', title: __('订单总金额'), operate: false, sortable: true},
                                {field: 'auserpaysum', title: __('消费总金额'), operate: false, sortable: true},
                                {field: 'discountsum', title: __('抵扣总金额'), operate: false, sortable: true},
                                {field: 'getbean', title: __('小哥豆领取次数'), operate: false},
                                {field: 'orgin', title:'平台类型',  searchList:{
                                    "1":"公众号",
                                    "2":"小程序",
                                }, visible:0},
                                {field: 'city_code', title:'城市类型', searchList:citylist,visible:0},
                                {field: 'create_time', title: __('请输入时间'),visible: false, formatter: Controller.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true}
                            ]
                        ]
                    });
                    table.on('load-success.bs.table', function (e, data) {
                        $("#userpaynum1").text(data.extend.userpaynum);
                        $("#suserpaysum1").text(data.extend.suserpaysum);
                        $("#auserpaysum1").text(data.extend.auserpaysum);
                        $("#discountsum1").text(data.extend.discountsum);
                        $("#getbean1").text(data.extend.getbean);
                    });

                    // 为表格1绑定事件
                    Table.api.bindevent(table);
                    $('.search').css({'width':'360px','margin-right':'10px'});
                });    
            },
            //店铺支付详情
            storeanalysis: function(){
                $.getJSON('shop/Analysis/ptcity',function (porgindef) {
                    var citylist={};
                    var porgin={};
                    for (var i = 0; i < porgindef.pt.length; i++) {
                        if(porgindef.pt[i]&&porgindef.pt[i]['name']!=''){
                            debugger;
                            porgin[porgindef.pt[i]['value']]=porgindef.pt[i]['name'];
                        }
                    }
                    for (var i = 0; i < porgindef.city.length; i++) {
                            citylist[porgindef.city[i]['value']]=porgindef.city[i]['name'];
                    }
                    debugger;

                    var table = $("#table3");
                    table.bootstrapTable({
                        url: 'shop/analysis/storeanalysis',
                        toolbar: '#toolbar3',
                        sortName: 'create_time',
                        searchFormVisible:true,
                        search: false, //是否启用快速搜索
                        commonSearch: true, //是否启用通用搜索
                        pageSize: 20,
                        pageList: [20, 45, 60, 'All'],
                        showExport:true,
                        columns: [
                            [
                                {field: 'store_id', title: '店铺ID', operate: false},
                                {field: 'store.nickname', title: __('店铺名称'), operate: false},
                                {field: 'userpaynum', title: __('用户消费次数'), operate: false, sortable: true},
                                {field: 'paynum', title: __('消费人数'), operate: false, sortable: true},
                                {field: 'suserpaysum', title: __('订单总金额'), operate: false, sortable: true},
                                {field: 'auserpaysum', title: __('消费总金额'), operate: false, sortable: true},
                                {field: 'discountsum', title: __('抵扣总金额'), operate: false, sortable: true},
                                {field: 'city_code', title:'城市类型', searchList:citylist,visible:0},
                                {field: 'create_time', title: __('日期'),visible: false,  formatter: Controller.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true}
                            ]
                        ]
                    });

                    table.on('load-success.bs.table', function (e, data) {
                        $("#userpaynum2").text(data.extend.userpaynum);
                        $("#paynum2").text(data.extend.paynum);
                        $("#suserpaysum2").text(data.extend.suserpaysum);
                        $("#auserpaysum2").text(data.extend.auserpaysum);
                        $("#discountsum2").text(data.extend.discountsum);
                    });

                    // 为表格1绑定事件
                    Table.api.bindevent(table);
                    $('.search').css({'width':'360px','margin-right':'10px'});
                });  
            },
            tjanalysis: function(){
                var table = $("#table4");
                table.bootstrapTable({
                    url: 'shop/analysis/tjanalysis',
                    toolbar: '#toolbar4',
                    sortName: 'create_time',
                    searchFormVisible:true,
                    search: false, //是否启用快速搜索
                    commonSearch: true, //是否启用通用搜索
                    pageSize: 20,
                    pageList: [20, 45, 60, 'All'],
                    showExport:true,
                    columns: [
                        [
                            {field: 'tj', title: __('推荐人'), operate: false},
                            // {field: 'tjname', title: __('姓名'), operate: false},
                            {field: 'tjstorecount', title: __('推荐店铺数'), operate: false, sortable: true},
                            {field: 'payusercount', title: __('店铺消费总人数'), operate: false, sortable: true},
                            {field: 'suserpaysum', title: __('订单总金额'), operate: false, sortable: true},
                            {field: 'auserpaysum', title: __('消费总金额'), operate: false, sortable: true},
                            {field: 'discountsum', title: __('抵扣总金额'),operate: false, sortable: true},
                            {field: 'create_time', title: __('日期'),visible: false,formatter: Controller.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true}
                        ]
                    ]
                });

                table.on('load-success.bs.table', function (e, data) {
                    $("#tjstorecount3").text(data.extend.tjstorecount);
                    $("#payusercount3").text(data.extend.payusercount);
                    $("#suserpaysum3").text(data.extend.suserpaysum);
                    $("#auserpaysum3").text(data.extend.auserpaysum);
                    $("#discountsum3").text(data.extend.discountsum);
                });

                // 为表格1绑定事件
                Table.api.bindevent(table);
                $('.search').css({'width':'360px','margin-right':'10px'});
            },
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                browser:  function (v,b) {
                    return ['<a href="/admin/shop/orders/detail/ids/',b.id,'" data-title="详情" class="btn-dialog">',v,'</a>'].join('');
                },
                expand : function (a) {
                    if(a instanceof Array) {
                        var r = 0;
                        $.each(a, function (i,v) {
                            if(v.attach == 3)r += parseFloat(v.total_fee);
                        })
                        return '￥' + (r / 100).toFixed(2);
                    }return a;
                },
                server: function (ary) {
                    if(!ary || !ary.length)return '';
                    var c,ret = [];
                    for(c in ary)ret.push(ary[c].name);
                    return ret.join('，');
                },
                datetime: function (value, row, index) {
                    var datetimeFormat = typeof this.datetimeFormat === 'undefined' ? 'YYYY-MM-DD' : this.datetimeFormat;
                    if (isNaN(value)) {
                        return value ? Moment(value).format(datetimeFormat) : __('None');
                    } else {
                        return value ? Moment(parseInt(value) * 1000).format(datetimeFormat) : __('None');
                    }
                },
            },
        }
    };
    return Controller;
});