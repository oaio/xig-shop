define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/customerin/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: 'customerin',
                }
            });

            var table = $("#table");

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: "id",
                sortName: "server.id",
                search:false,
                commonSearch:true,
                showExport:true,
                columns: [
                    [
                        {field: 'server.id', title: __('序号'), formatter: function (a,b,c) {
                                a=c+1;
                                if(b.mark&&b.mark.length>0)a += '<img src="/assets/img/ac.png" style="width:38px;height:18px; ' +
                                    'transform: rotate(-30deg);position: absolute;opacity: 0.5;" />';
                                return a;
                            }},
                        {field: 'order.goods_name', title: __('服务项目'),operate: 'LIKE',sortable: true},
                        {field: 'order.snorder', title: __('订单编号'),operate: 'LIKE',sortable: true},
                        {field: 'order.nickname', title: __('用户'),operate: 'LIKE',sortable: true},
                        {field: 'order.mobile', title: __('联系电话'),operate: 'LIKE',sortable: true},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'order.goods_origin', title:  __('商品类型'), sortable: false,formatter:function(z){
                            if(z){
                                if(z.indexOf('shop_goods_bargain')!=-1){
                                    return '特惠商品';
                                }
                                else{
                                    return '普通商品';
                                }
                            }
                        }},
                        {field: 'region_name', title:  __('区域'), operate: 'LIKE',formatter: Table.api.formatter.label,sortable: true},
                        {field: 'order.num', title: __('数量'),  operate: 'LIKE',sortable: true},
                        {field: 'shouldpay', title: __('总价格'), operate: 'LIKE',sortable: true,
                            formatter: function (a,c) {
                                var n=0,l=c.order.paylog;
                                if(!l || l.length<2) return a;
                                for(var i in l )n+=l[i].total_fee - 0;
                                return (n/100).toFixed(2);
                            }},
                        {field: 'emp', title: __('服务人员'), operate: false,formatter:function(val,arr,key){
                            
                            var nick='';

                            for (var i = val.length - 1; i >= 0; i--) {
                                nick+=' '+val[i]['name'];
                            }

                            return nick;
                        }},
                        {
                            field: 'server.sfuserid', title: '服务人员', operate: 'FIND_IN_SET', visible: !1,
                            addclass: 'selectpage',
                            extend: 'data-source="shop/employee/get" data-field="name" autocomplete="off"'
                        },
                        {field: 'admin.nickname', title: __('派单人员'), operate: false},
                        {field: 'appoint_start', title: __('师傅预约上门时间'), formatter: Table.api.formatter.datetime,operate: false, addclass: 'datetimerange', sortable: true},
                        {field: 'server.fu_start', title: __('开始服务时间'),visible: false, formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'fu_start', title: __('开始服务时间'),formatter: Table.api.formatter.datetime, operate: false, addclass: 'datetimerange', sortable: true},
                        {field: 'fu_start', title: __('已服务时长'),formatter: Controller.api.counting, operate: false, sortable: true},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'deal',
                                    title: __('订单人工处理'),
                                    text: __('人工处理'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/customer/deal'
                                },
                                {
                                    name: 'mark',
                                    title: __('标记客诉'),
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    icon: 'fa fa-bookmark',
                                    url: 'shop/todo/mark'
                                },
                                {
                                    icon: 'fa fa-check-square',
                                    title: __('一键完成'),
                                    extend: {title: __('一键完成')},
                                    classname: 'btn btn-xs btn-danger btn-complete btn-ajax',
                                    confirm: __('Are you sure to completed this order?'),
                                    url: 'shop/customer/complete',
                                    success: function () {
                                        $('.btn-refresh').trigger('click');
                                    }
                                },
                                {
                                    name: 'detail',
                                    title: __('订单流转记录'),
                                    text: __('查看订单流转'),
                                    classname: 'btn btn-xs btn-default btn-dialog',
                                    icon: 'fa fa-exchange',
                                    url: 'shop/todo/history'
                                }],
                            formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                var h,m,v,over=0;
                clearInterval(Controller.counter);
                Controller.counter=setInterval(function(){
                    over++;
                    $("span.minute").each(function (i,e){
                        v=$(e).parent().find('span:last-child').html()-0+over;
                        if(v%60==0){
                            m=$(e).html()-0+1;
                            $(e).html(m);
                            if(m==60){
                                h = $(e).parent().find('span:first-child');
                                h.html(h.html()-0+1);
                                $(e).html(0);
                            }
                        }
                    });
                }, 1000);
                $(".btn-info").data("area", ["800px", "800px"])
                $(".btn-default").data("area", ["300px", "500px"])
                $(".btn-warning").data("area", ["300px", "350px"])
                $(".btn-editone").data("title","订单人工处理").data("area", ["800px", "800px"]);
                table.find("img").each(function (i,e) {
                    var p = $(e).parent().parent().find("td:eq(1)").position();
                    $(e).css({left:p.left+"px",top:p.top+"px"});
                });
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
            $(document).on("click", ".btn-close", function () {
                Layer.closeAll();
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        deal: function(){

            var table = $("#table");
            Form.api.bindevent($("form[role=form]"));
            $("button.btn-close").click(function () {
                window.Layer.closeAll();
            });
            Table.api.init({
                extend: {
                    index_url: 'shop/customer/deal',
                    table: 'deal',
                }
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    window.Layer.closeAll();
                });
                $("#c-avatar").bind("change",function() {
                    $(this).isValid();
                });
            },
            counting: function (a) {
                a= parseInt((new Date().getTime())/1000)-a;
                var h=parseInt(a/3600),b=a-h*3600,s=parseInt(b/60);
                return ['<span class="hour">',h,'</span>小时<span class="minute">',s,
                    '</span>分钟<span class="second hide">',b,'</span>'].join('')
            }


        }
    };
    return Controller;
});