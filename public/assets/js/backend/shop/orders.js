require.config({
    paths: {
        'laydate': '/resource/js/laydate/laydate'
    }
});
define(['jquery', 'bootstrap', 'backend', 'table', 'form','laydate'], function ($, undefined, Backend, Table, Form,laydate) {

    var Controller = {
        index: function () {
            Table.api.init();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                $(this).unbind('shown.bs.tab');
            });
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        table:{
            custom: function(){
                $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 客户姓名/手机号/地址"}
                var table = $("#table2");
                table.bootstrapTable({
                    url: 'shop/orders/custom',
                    toolbar: '#toolbar2',
                    sortName: 'a.create_time',
                    searchFormVisible:true,
                    columns: [
                        [
                            {field: 'state', checkbox: true, },
                            {field: 'id', title: 'ID', formatter: function(a,b,c){return c+1}},
                            {field: 'name', title: __('客户姓名'), operate: 'LIKE %...%'},
                            {field: 'phone', title: __('客户手机号'), operate: 'LIKE %...%'},
                            {field: 'names', title: __('微信昵称'), operate: false},
                            {field: 'total_fee', title: __('付款金额'), formatter: function(r){return '￥'+(r/100).toFixed(2);}},
                            {field: 'note', title: __('备注'), operate: 'LIKE %...%'},
                            {field: 'address', title: __('客户地址'), operate: 'LIKE %...%'},
                            {field: 'nickname', title: __('绑定店铺')},
                            {field: 'ruler', title: __('店主'), operate: false},
                            {field: 'ruler_phone', title: __('店主手机号'), operate: false},
                            {field: 'create_time',  title: __('下单时间'), operate: 'RANGE', sortable: !0,
                                formatter: Table.api.formatter.datetime,e: table}
                        ]
                    ]
                });

                table.on('load-success.bs.table', function (e, data) {
                    $("#total").text(data.extend);
                });

                // 为表格1绑定事件
                Table.api.bindevent(table);
                $('.search').css({'width':'360px','margin-right':'10px'});
            },
            all: function(){
                $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 订单编号/姓名/手机号/地址"};
                $.fn.bootstrapTable.locales[Table.defaults.locale]['formatShowingRows'] = function (pageFrom, pageTo, totalRows) {
                    return '显示第 ' + pageFrom + ' 到第 ' + pageTo + ' 个订单，总共 ' + totalRows + ' 个订单';
                };

                var table = $("#table");

                // 初始化表格
                table.bootstrapTable({
                    url: 'shop/orders/index',
                    toolbar: '#toolbar',
                    commonSearch: !0,
                    showColumns: 1,
                    columns: [
                        [
                            {field: 'id', title: __('序号'), operate: false, formatter: function (a,b,i) {
                                    c=b.card,d=(c&&c.status==1);return '<span class="label ' +
                                        (d?'label-info':'label-success')+ '">' + (i +1)+
                                        (d?'<img src="/resource/png/icon/newOrder.png" style="width: 32px;' +
                                            'margin:-12px 0 0 0 ;position: absolute">':'') +'</span>';
                                }},
                            {field: 'snorder', title: __('订单编号'), placeholder: '如：20190206',
                                formatter:Controller.api.formatter.browser, operate: 'LIKE %...%'},
                            {field: 'store.nickname', title: __('店铺名称'), operate: 'LIKE %...%'},
                            {field: 'store.ruler', title: __('店主'), operate: false},
                            {field: 'store.ruler_phone', title: __('店主手机号'), operate: false},
                            {field: 'goods_name', title: __('商品名称'), operate: 'LIKE %...%'},
                            {field: 'type', title: __('团购'), operate: false, formatter:function (a) {return a==1?' 是':'否'}},
                            {field: 'actualpay', title: __('实付金额'), operate: false, formatter:function (v) {return "￥"+v}},
                            {field: 'virdiscount.dismoney', title: __('抵扣金额'), operate: false, formatter:function (v) {
                                if(v!=null){
                                    return "￥"+v
                                }else{
                                    return "￥0.00"
                                }
                            }},
                            {field: 'paylog', title: __('追加金额'), operate: false, formatter:Controller.api.formatter.expand},
                            {field: 'a.nickname', title: __('客户姓名'), operate: 'LIKE %...%'},
                            {field: 'a.mobile', title: __('客户手机号'), operate: 'LIKE %...%'},
                            {field: 'f.name', title: __('注册姓名'), operate: 'LIKE %...%'},
                            {field: 'f.phone', title: __('注册手机号'), operate: 'LIKE %...%'},
                            {field: 'a.address', title: __('服务地址'), operate: 'LIKE %...%'},
                            {field: 'service.emp', title: __('服务人员'), operate:false, formatter:Controller.api.formatter.server},
                            {field: 'num', title: __('数量'), operate: false},
                            {field: 'card.sum_times', title: __('总服务次数'), operate: false},
                            {field: 'card.times', title: __('剩余服务次数'), operate: false},
                            {field: 'create_time', title: __('下单时间'), operate: 'RANGE',
                                formatter: Table.api.formatter.datetime, extend: 'autocomplete="off"',
                                placeholder: '点击选择两个日期', sortable: !0}
                        ]
                    ],
                    queryParams: function (params) {
                        if(params.sort=="create_time")params.sort="a.create_time";
                        params.op = params.op.replace('create_time', 'a.create_time');
                        params.filter = params.filter.replace('create_time', 'a.create_time');
                        return params;
                    },
                });

                table.on('load-success.bs.table', function (e, data) {
                    $("#money").text(data.extend);
                    $("#real").text(data.sum);
                });

                // 为表格绑定事件
                Table.api.bindevent(table);
                $('.search').css({'width':'360px','margin-right':'10px'});
                $('button[type=submit]').html('搜索');
                // $('#nickname').attr('name','a.nickname');
                // $('input[name=nickname-operate]').data('name','a.nickname');
                laydate.render({
                    elem: '#create_time',
                    range: true
                });
            }
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                browser:  function (v,b) {
                    return ['<a href="/admin/shop/orders/detail/ids/',b.id,'" data-title="详情" class="btn-dialog">',v,'</a>'].join('');
                },
                expand : function (a) {
                    if(a instanceof Array) {
                        var r = 0;
                        $.each(a, function (i,v) {
                            if(v.attach == 3)r += parseFloat(v.total_fee);
                        })
                        return '￥' + (r / 100).toFixed(2);
                    }return a;
                },
                server: function (ary) {
                    if(!ary || !ary.length)return '';
                    var c,ret = [];
                    for(c in ary)ret.push(ary[c].name);
                    return ret.join('，');
                }
            },
        }
    };
    return Controller;
});