define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/articlelibrary/index',
                    add_url: 'shop/articlelibrary/add',
                    edit_url: 'shop/articlelibrary/edit',
                    del_url: 'shop/articlelibrary/del',
                    multi_url: 'shop/articlelibrary/multi',
                    table: 'articlelibrary',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "标题/简要";};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: true,
                pagination: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'title', title: '标题'},
                        {field: 'brief', title: '简要'},
                        {field: 'link', title: '访问链接',formatter:function(a,b,i){return document.domain+a}},
                        {field: 'usecode', title: '调用码'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});