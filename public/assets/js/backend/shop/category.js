define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/category/index',
                    add_url: 'shop/category/add',
                    edit_url: 'shop/category/edit',
                    del_url: 'shop/category/del',
                    multi_url: 'shop/category/multi',
                    table: 'banner',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                pk: 'id',
                sortName: 'admin_id',
                pagination: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'pid', title: __('Parent')},
                        {field: 'name', title:  __('Name'), align: 'left'},
                        {field: 'status', title:'状态', visible: false},
                        {field: 'power', title:'权重'},
                        {field: 'admin_id', title: '管理ID', operate: 'BETWEEN', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});