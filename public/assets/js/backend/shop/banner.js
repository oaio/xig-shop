define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/banner/index',
                    add_url: 'shop/banner/add',
                    edit_url: 'shop/banner/edit',
                    del_url: 'shop/banner/del',
                    multi_url: 'shop/banner/multi',
                    table: 'banner',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                pk: 'id',
                sortName: 'pow',
                pagination: false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'), sortable: true, formatter: function (a,b,c) {return c+1}},
                        {field: 'img_link', title: __('首页图'), formatter: Table.api.formatter.image, operate: false},
                        //{field: 'status', title: __('Status'), searchList: {1: __('Normal'), 0: __('Hidden')}, formatter: Table.api.formatter.toggle},
                        // {field: 'pow', title: __('权重'), operate: 'BETWEEN', sortable: true},
                        // {field: 'admin_id', title: __('Gender'), visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('load-success.bs.table', function (e, data) {
                if(data.total) {
                    $.e=$(".btn-add").removeClass("btn-add").click(function () {
                        Layer.msg('广告图已经存在，不能再添加了！');
                    });
                }else{
                    $.e && $.e.addClass("btn-add").off("click");
                }
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});