define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/vipcard/index',
                    add_url: 'shop/vipcard/add',
                    edit_url: 'shop/vipcard/edit',
                    del_url: 'shop/vipcard/del',
                    multi_url: 'shop/vipcard/multi',
                    table: 'vipcard',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: 'name', title: '类目名称'},
                        {field: 'power', title: '权重'},
                        {field: 'price', title: '原价'},
                        {field: 'discoutprice', title: '优惠价'},
                        /*{field: 'icon', title: '图片',formatter: Table.api.formatter.image},*/
                        {field: 'effectivetime', title: '有效时间',formatter:function(a,b,i){return a+'个月'}},
                        {field: 'status', title: '是否显示',formatter:function(a,b,i){ 
                            if(a=="0") return '隐藏';
                            if(a=='1') return '显示';
                        }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});