define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "员工姓名/手机号";};

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/employee/index',
                    add_url: 'shop/employee/add',
                    edit_url: 'shop/employee/edit',
                    del_url: 'shop/employee/del',
                    multi_url: 'shop/employee/multi',
                    table: 'employee',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: "id",
                sortName: "create_time",
                commonSearch: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'), formatter: function (a,b,c) {return c+1}},
                        {field: 'cid', title:  __('员工编号'), operate: 'LIKE'},
                        {field: 'name', title: __('姓名'),  operate: 'LIKE'},
                        {field: 'phone', title: __('手机号码'), operate: 'LIKE'},
                        {field: 'create_time', title:  __('注册时间'), formatter: Table.api.formatter.datetime, sortable: true},
                        {field: 'job', title: __('岗位'), formatter: Controller.api.job},
                        {field: 'admin_id', title: __('Gender'), visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('.search').css({'width':'260px','margin-right':'10px'});
            $('button[name="commonSearch"]').off('click').on('click',function () {
                $('.btn-refresh').trigger('click');
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    window.Layer.closeAll();
                });
                $("#c-avatar").bind("change",function() {
                    $(this).isValid();
                });
            },
            job: function (v) {
                var a=[],b=v.split(',');
                $.each(b,function (c,i) {
                    a.push(jobs[i]);
                });
                return a.join('/');
            }
        }
    };
    return Controller;
});