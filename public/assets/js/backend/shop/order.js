define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            var table = $("#table"),status, schedule = function (a,row) {
                return Table.api.formatter.status.call(this, status[a], row);
            },sources,source = function (a) {return sources[a]};
            $.getJSON('shop/order/status',function (s) {status = s;});
            $.getJSON('shop/order/source',function (s) {sources = s;});
            Table.api.init();
            table.bootstrapTable({
                url: 'shop/order/index',
                search: !1,
                showColumns: !0,
                sortName: 'o.id',
                pagination: true,
                showExport: true,
                columns: [
                    [
                        {
                            field: 'id', title: '序号', operate: false, formatter: function (a, b, c) {
                                a = c + 1;
                                if (b.mark && b.mark.length > 0) a += '<img src="/assets/img/ac.png" style="width:38px;height:18px; ' +
                                    'transform: rotate(-30deg);position: absolute;opacity: 0.5;" />';
                                return a;
                            }
                        },
                        {field: 'goods_name', title: __('服务项目'), operate: 'LIKE'},
                        {field: 'snorder', title: __('订单编号'), align: 'left', operate: 'LIKE', sortable: !0},
                        {field: 'pay_id', title: '支付ID', operate: !1, visible: !1, sortable: !0},
                        {field: 'nickname', title: '用户', operate: 'LIKE'},
                        {field: 'mobile', title: '联系电话', operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'user.phone', title: '注册电话', operate: 'LIKE', visible: !1, formatter: Table.api.formatter.search},
                        {field: 'user.name', title: '注册姓名', operate: !1, visible: !1},
                        {field: 'user.nickname', title: '注册昵称', operate: !1, visible: !1},
                        {field: 'goods_origin', title:  __('商品类型'), sortable: false,formatter:function(a,b,c){
                            if(a.indexOf('shop_goods_bargain')!=-1){
                                return '特惠商品';
                            }
                            else{
                                return '普通商品';
                            }
                        }},
                        {field: 'rn', title: '城市', operate: !1, visible: !1},
                        {field: 'region_name', title: '区域', formatter: Table.api.formatter.label},
                        {field: 'address', title: '地址', operate: !1, visible: !1},
                        {field: 'num', title: '数量', operate: !1},
                        {field: 'shouldpay', title: '总价', operate: !1, sortable: 1},
                        {field: 'actualpay', title: '实付', operate: !1, sortable: 1, visible: !1},
                        {field: 'dismoney', title: '抵扣', operate: !1, sortable: 1, visible: !1},
                        {field: 'expand', title: '追加', operate: !1, formatter: function (a) {
                                let n = 0;for (let i in a) n += a[i].money - 0;return n;}},
                        {field: 'address', title: '地址', operate: !1, visible: !1},
                        {
                            field: 'create_time',
                            title: __('下单时间'),
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            sortable: true,
                            extend: 'autocomplete="off"'
                        },
                        {field: 'temp', title: '客户预约时间', operate: !1, visible: !1,
                            formatter:(a)=>{return a?a.d_data + ' ' + a.d_time:'-'}},
                        {field: 'service.fu_end', title: '完成时间', operate: !1, sortable: 1, visible: !1,
                            formatter: Table.api.formatter.datetime},
                        {field: 'service.update_time', title: '完结时间', operate: !1, visible: !1,
                            formatter: (a,b)=>{return 4==b.schedule||7==b.schedule?
                                Table.api.formatter.datetime.call(this, a, b):'无'}},
                        {
                            field: 'schedule', title: '订单状态',
                            formatter: Table.api.formatter.status,
                            searchList: {
                                0: "新订单", 1: "待客服指派", 2: "待师傅确认", 3: "待服务",
                                4: "取消", 5: "服务中", 6: "服务完成", 7: "完结", 9: "待办"
                            },
                            custom: {
                                "CLOSE": "gray", "新订单": "success", "待师傅确认": "info",
                                "待办": "danger", "待客服指派": "warning"
                            },
                            sortable: true
                        },
                        {field: 'service.emp', title: __('服务人员'), operate: !1,formatter:function(a,b){
                            b='';for(var i in a)b += a[i]['name']+' ';return b}
                        },
                        {
                            field: 'service.sfuserid', title: '服务人员', operate: 'FIND_IN_SET', visible: !1,
                            addclass: 'selectpage',
                            extend: 'data-source="shop/employee/get/" data-field="name" autocomplete="off"'
                        },
                        {
                            field: 'service.push_time', title: '派单时间', operate: !1, visible: !1,
                            formatter: function (a) {
                                return a > 0 ? Table.api.formatter.datetime(a) : ''
                            }
                        },
                        {
                            field: 'service.fu_start',
                            title: '服务时间',
                            operate: !1,
                            visible: !1,
                            formatter: Table.api.formatter.datetime
                        },
                        {field: 'service.note', title: '预约备注', operate: !1, visible: !1},
                        {field: 'service.usernote', title: '师傅备注', operate: !1, visible: !1},
                        {field: 'service.servernote', title: '客服备注', operate: !1, visible: !1},
                        {field: 'orgin', title: '订单来源', formatter: source, operate: !1, visible: !1},
                        {field: 'store.nickname', title: '店铺名称', operate: !1, visible: !1},
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'mark',
                                    title: __('标记客诉'),
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    icon: 'fa fa-bookmark',
                                    url: 'shop/todo/sign'
                                },
                                {
                                    name: 'detail',
                                    title: __('查看订单'),
                                    text: __('订单信息'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'shop/close/detail'
                                }],
                            formatter: Controller.operate
                        }
                    ]
                ],
                queryParams: function (params) {
                    params.sort = params.sort.replace('create_time', 'o.create_time');
                    params.op = params.op.replace('nickname', 'o.nickname')
                        .replace('mobile', 'o.mobile').replace('region_name', 'r.region_name')
                        .replace('create_time', 'o.create_time');
                    params.filter = params.filter.replace('nickname', 'o.nickname')
                        .replace('mobile', 'o.mobile').replace('region_name', 'r.region_name')
                        .replace('create_time', 'o.create_time');
                    return params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-info").data("area", ["800px", "650px"]);
                $(".btn-warning").data("area", ["300px", "350px"]);
                table.find("img").each(function (i,e) {
                    var p = $(e).parent().parent().find("td:eq(1)").position();
                    $(e).css({left:p.left+"px",top:p.top+"px"});
                });
                $(document).on('click', '.btn-re', function () {
                    var that = this,
                    url = Backend.api.replaceids(that, $(that).attr('href'));
                    Backend.api.addtabs(url, status[$(that).data('title')]);
                    return false;
                })
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
        operate: function (a, b, c) {
            var buttons = $.extend([], this.buttons || []),
            us = ["customer","customerassign","customersure", "customerwait",
                "close","customerin","customerend","close","","todo"];
            b.schedule>1 && buttons.push({
                icon: 'fa fa-share',
                title: __('Edit'),
                text: __('跳转'),
                extend: 'data-title="'+b.schedule+'"',
                classname: 'btn btn-xs btn-success btn-re',
                url: 'shop/'+us[b.schedule]+'/index'
            });
            return Table.api.buttonlink(this, buttons, a, b, c, 'operate');
        }
    };
    return Controller;
});