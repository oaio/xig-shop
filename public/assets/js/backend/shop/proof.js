define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var uid=0,Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/proof/index',
                    add_url: 'shop/proof/add',
                    edit_url: 'shop/proof/edit',
                }
            });

            var table = $("#table");
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'm.id', title: __('卷号'), operate: !1, sortable: 1},
                        {field: 'm.name', title: __('卷名'), sortable: 1, operate: 'LIKE'},
                        {field: 'm.scope', title: __('使用范围'), sortable: 1, operate: !1},
                        {field: 'uid', title: __('Uid'), visible: !1, sortable: 1},
                        {field: 'user.name', title: __('Username'), align: 'left',
                            operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'user.nickname', title: __('微信昵称'), align: 'left',
                            operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'user.phone', title: __('注册电话'), operate: 'LIKE',
                            formatter: Table.api.formatter.search, sortable: 1},
                        {field: 'm.status', title: __('状态'), operate: 'LIKE', searchList:{0:"未使用", 1:"已使用", 2:"过期"},
                            formatter: Table.api.formatter.normal, sortable: 1},
                        {field: 'm.create_time', title: __('领取时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        {field: 'validtime', title: __('有效时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate, visible: !1}
                    ]
                ],
                search: !1,
                showExport: 1,
                showColumns: 1,
                commonSearch: 1,
                queryParams: function (params) {
                    return params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
               // uid && Fast.api.ajax('shop/self_pay/fill');
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});