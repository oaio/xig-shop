define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/viprecord/index',
                    add_url: 'shop/viprecord/add',
                    edit_url: 'shop/viprecord/edit',
                    del_url: 'shop/viprecord/del',
                    multi_url: 'shop/viprecord/multi',
                    table: 'viprecord',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                sortName:'a.id',
                showExport: !0,
                showColumns: !0,
                columns: [
                    [
                        {field: 'a.id', title: '序号', sortable: true, formatter:function (a,b) {return b.id;}},
                        {field: "uid", title: '用户ID', visible: !1},
                        {field: "user.name", title: '用户名称', operate: 'LIKE'},
                        {field: "user.nickname", title: ' 微信呢称', operate: 'LIKE'},
                        {field: "user.phone", title: '电话号码', operate: 'LIKE'},
                        {field: 'vipname', title: '类目', operate:!1},
                        {field: 'price', title: '价格', operate:!1},
                        {field: 'num', title: '数量', operate:!1},
                        {field: 'actiontime', title: '开始时间',formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'stoptime', title: '结束时间',formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: "vip_code.effectivetime", title: '有效时长', operate:!1,formatter:function(a,b,c){
                            return (a?a:Math.floor((b.stoptime-b.actiontime)/2592000))+'个月';
                        }},
                        {field: 'createtime', title: '创建时间',formatter: Table.api.formatter.datetime, operate:!1, addclass: 'datetimerange', sortable: true},
                        {field: 'is_new', title: '状态', operate:!1,formatter:function(a){return ['续费', '开通', '到期'][a]}},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(".btn-add").data("area", ["300px", "350px"]).html('<i class="fa fa-plus"> 赠送会员</i>');
        },
        add: function () {
            Controller.api.bindevent();
            $("#c-uid").on("keyup", function (e) {
                $.getJSON("user/info/json",{ids:this.value}, function (s) {
                    s && $(".u_info").html([s.nickname,"（",s.phone,"）"].join(""));
                });
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});