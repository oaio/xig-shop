define(['jquery', 'form'], function ($, Form) {
    var Controller = {
        index: function () {
            $("<link>").attr({ rel: "stylesheet", type: "text/css",
                href: "/assets/css/sean.css"}).appendTo("head");
            $("#full").on("click", ".layui-unselect",function () {
                $(this).toggleClass("layui-form-onswitch");
                var a = $("#full").find('input'),b=a.val();
                a.val( b!=0? 0 : 1);
                $("#full").find("em").html(b!=0?"否":"是");
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.submit  = function(form){form[0].submit();};
                Form.api.bindevent($("form[role=form]"));
                if($("#full").find("input").prop('value')==0){
                    $('.layui-unselect').removeClass("layui-form-onswitch");
                }
            }
        }
    };
    return Controller;
});