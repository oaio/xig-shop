define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/goodslibrary/index',
                    add_url: 'shop/goodslibrary/add',
                    edit_url: 'shop/goodslibrary/edit',
                    del_url: 'shop/goodslibrary/del',
                    multi_url: 'shop/goodslibrary/multi',
                    table: 'goodslibrary',
                }
            });

            var table = $("#table"),sources,source = function (a) {return sources[a]};
            $.getJSON('shop/order/source',function (s) {sources = s;});
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "名称";};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: true,
                pagination: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', sortable: true},
                        {field: "cate", title: '分类', searchList:cate, formatter:Table.api.formatter.flag},
                        {field: 'name', title: '名称'},
                        {field: 'commonprice', title: '价格'},
                        {field: 'times', title: '服务次数'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});