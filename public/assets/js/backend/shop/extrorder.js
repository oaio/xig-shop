define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var table = $("#table"),status,source,porgin;
                schedule = function (a,row) {
                 return Table.api.formatter.status.call(this, status[a], row);
            };
            $.getJSON('shop/order/status',function (s) {
                status = s;
            });
            $.getJSON('shop/order/source',function (s) {
                source = s;
            });
            debugger;
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/extrorder/index',
                    add_url: 'shop/extrorder/add',
                    edit_url: 'shop/extrorder/edit',
                    extr: 'shop/extrorder/extr',
                    del_url: '',
                    multi_url: '',
                    table: 'order',
                }
            });
            $.getJSON('shop/Extrorder/porgin',function (porgindef) {
                var porgin={};
                for (var i = 0; i < porgindef.source.length; i++) {
                    if(porgindef.source[i]&&i>2){
                        porgin[i]=porgindef.source[i];
                    }
                }
                table.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.index_url,
                    search: false,
                    pagination: true,
                    showExport:true,
                    showColumns:false,
                    columns: [
                        [
                            {field: 'id', title: '序号', operate: false},
                            {field: 'coupons', title: __('券号'), operate: 'LIKE'},
                            {field: 'nickname', title:'姓名', operate: 'LIKE'},
                            {field: 'mobile', title:'电话', operate: 'LIKE'},
                            {field: 'actualpay', title:'付款', operate: false, formatter:  function (a,b,c) {return b.num*b.goods.commonprice}},
                            {field: 'goods.name', title:'购买项目', operate: false},
                            {field: 'address', title: '服务地址', operate: 'LIKE'},
                            {field: 'orgin', title:'录入平台', searchList:porgin, formatter:  function (a) {
                                return source[a]
                            }},
                            {field: 'status', title:'录入状态',  searchList:{
                                0:'未录入',
                                1:'已录入'
                            },visible:0},
                            {field: 'admin.nickname', title:'录入人员', operate: false},
                            {field: 'admin_id', title:'录入人员' , searchList:porgindef.admin,visible:0},
                            {field: 'create_time', title: __('录入时间'), formatter: Table.api.formatter.datetime,
                                operate: 'RANGE', addclass: 'datetimerange'},
                            {field: 'operate', title: __('Operate'), table: table,
                                events: Table.api.events.operate,
                                buttons: [
                                    {
                                        name: 'show',
                                        icon: 'fa fa-user',
                                        text: __('查看'),
                                        extend: 'data-toggle="tooltip" data-title="查看"',
                                        classname: 'btn btn-xs btn-info btn-dialog',
                                        url: 'shop/extrorder/show'
                                    }],
                                formatter: function(value, row, index){

                                    var table = this.table;
                                    // 操作配置
                                    var options = table ? table.bootstrapTable('getOptions') : {};
                                    // 默认按钮组
                                    var buttons = $.extend([], this.buttons || []);
                                    // 所有按钮名称
                                    if(row.status==0){
                                         buttons.push({
                                            name: 'edit',
                                            icon: 'fa fa-user',
                                            text: __('修改'),
                                            extend: 'data-toggle="tooltip" data-title="修改"',
                                            classname: 'btn btn-xs btn-success btn-editone',
                                            url: 'shop/extrorder/edit'
                                         });
                                         buttons.push({
                                            icon: 'fa fa-check-square',
                                            title: __('外部录入订单'),
                                            extend: {title: __('外部录入订单')},
                                            classname: 'btn btn-xs btn-danger btn-complete btn-ajax',
                                            confirm: __('确定生成订单吗?'),
                                            url: 'shop/extrorder/cdata',
                                            success: function () {
                                                $('.btn-refresh').trigger('click');
                                            }
                                         });

                                    }
                                    if(row.status==1){
                                         buttons.push({
                                            name: 'extr',
                                            icon: 'fa fa-user',
                                            text: __('额外处理'),
                                            extend: 'data-toggle="tooltip" data-title="额外处理"',
                                            classname: 'btn btn-xs btn-warning btn-dialog',
                                            url: $.fn.bootstrapTable.defaults.extend.extr
                                         });
                                    }
                                    return Table.api.buttonlink(this, buttons, value, row, index, 'operate');

                                }}
                          
                        ]
                    ],
                });

            // 为表格绑定事件
            Table.api.bindevent(table);
            });

            table.on('post-body.bs.table', function () {
                $(".btn-warning").data("area", ["300px", "350px"]);
                $(".btn-facebook").data("area", ["300px", "500px"]);
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
            $(document).on("click", ".btn-facebook", function () {
                Fast.api.open('shop/extrorder/setting', __('Setting'), $(this).data() || {});
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        show: function () {
            Controller.api.bindevent();
        },
        extr: function () {
            Controller.api.bindevent();
        },
        setting: function(){
            Controller.api.bindevent();
            setTimeout(function (){$(".fieldlist input:lt(3)").hide();}, 100)
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
        },
        counter: null
    };
    debugger;
    return Controller;
});