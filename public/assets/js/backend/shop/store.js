define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/store/index',
                    add_url: 'shop/store/add',
                    edit_url: 'shop/store/edit',
                    del_url: 'shop/store/del',
                    multi_url: 'shop/store/multi',
                    table: 'store',
                }
            });

            var table = $("#table");

            // 初始化表格  
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: 'store_id',
                sortName: 'store_id',
                pagination: true,
                commonSearch: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'store_id', title: '序号', sortable: true},
                        {field: 'nickname', title: '门店名称'},
                        {field: 'store_type', title:'门店类型', formatter:Table.api.formatter.search},
                        {field: 'company', title:'公司名称'},
                        {field: 'is_af', title:'开通状态',searchList: {"-1":"已冻结","0": "未开通","1":"已开通"}, formatter:Table.api.formatter.search},
                        {field: 'create_time', title:'创建时间'},
                        // {field: 'hot', title: '权重', operate: 'BETWEEN', sortable: true},
                        {field: 'update_time', title: __('Gender'), visible: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    text: '开通',
                                    title: __('发送Ajax'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'example/bootstraptable/detail',
                                    confirm: '确认发送',
                                    visible: function (row) {
                                        return row.is_af==-1;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: '重新开通',
                                    title: __('发送Ajax'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'example/bootstraptable/detail',
                                    confirm: '确认发送',
                                    visible: function (row) {
                                        return row.is_af==0;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: '冻结',
                                    title: __('发送Ajax'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'example/bootstraptable/detail',
                                    confirm: '确认发送',
                                    visible: function (row) {
                                        return row.is_af ==1;
                                    }
                                },
                            ]
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        update: function () {
            Form.api.bindevent($("form[role=form]"),function (a, ret) {
                setTimeout(function (){location.href = ret.url}, 2000);
            });
        },
        check: function () {
            Form.api.bindevent($("form[role=form]"), function () {
                if(arguments[1].code>0){
                    top.url = location.href;
                }
            },function () {

            },function () {
                $("#result").val(Controller.status&&Controller.status>0 ? 1 :0);
            });
            $(document).on("click", ".btn-success", function () {
                Controller.status = 1;
            });
            $(document).on('click', 'a.popup2', function () {
                var imgWidth, img = new Image(), max = parent.$('.layui-layer-fast').width() - 50;
                img.src = this.getAttribute('href') || this.src;
                imgWidth = img.width > 9 ? img.width < max ? img.width + 'px' : max : '480px';
                img.onload = function () {
                    var $content = $(img).appendTo('body').css({background: '#fff', width: imgWidth, height: 'auto'}),
                        h = parent.$('.layui-layer-fast').height()-120;
                    if($(img).height()>h)$(img).css({width:'auto', height:h+'px'})
                    Layer.open({
                        type: 1, area: imgWidth, title: false, closeBtn: 1,
                        skin: 'layui-layer-nobg', shadeClose: true, content: $content,
                        end: function () {
                            $(img).remove();
                        }
                    });
                };
                img.onerror = window.$1;
                return false;
            });
        },
        store_list: function(){
            Controller.status = 0;
            var h=setInterval(function () {
                if(undefined != top.url){
                    Controller.api.open(top.url);
                }
            }, 500);
            $(document).on("click", ".btn-close", function () {
                Layer.closeAll();
                layer.prompt({title: '请输入拒绝的原因：', formType: 2}, function(text, index){
                    Fast.api.ajax({
                        url: "shop/store/deny",
                        data: {id: top.store_id, reason: text}
                    });
                    layer.close(index);
                    setTimeout(function (){location.reload()}, 2000);
                });
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            open: function (url) {
                Layer.confirm(
                    "审核成功！是否开通店铺？",
                    {icon: 3, title: __('Warning'), offset: "30%", shadeClose: true},
                    function (index) {
                        Fast.api.ajax({
                            url: "shop/store/open",
                            data: {id: top.store_id},
                        }, function () {
                            setTimeout(function (){location.reload()}, 1000);
                        });
                        Layer.close(index);
                    }
                );
                delete top.url;
            }
        }
    };
    return Controller;
});