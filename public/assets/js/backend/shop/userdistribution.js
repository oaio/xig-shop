define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/userdistribution/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'shop/userdistribution/multi',
                    table: 'userdistribution',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "ID/名称/手机号";};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: true,
                pagination: true,
                sortName: 'id',
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '用户ID', sortable: 1},
                        {field: 'phone', title: '电话'},
                        {field: 'name', title: '姓名'},
                        {field: 'store.nickname', title: '绑定店铺', operate: !1},
                        {field: 'store_bind', title: '小店类型', searchList:{0:"门店"}, operate: '>',
                        formatter: function (a) {return a>0?"门店":"个人"}},
                        {field: 'income.num', title: '总单数', operate: !1},
                        {field: 'income.total', title: '总销售额', operate: !1},
                        {field: 'sumincome', title: '总收益', operate: !1},
                        {field: 'sumwithdraw', title: '剩余提现额度提现', sortable: true, operate: !1},
                        {field: 'useincome', title: '已提现额度', sortable: true, operate: !1},
                        {field: 'operate', title: __('Operate'), table: table, 
                         events: Table.api.events.operate,
                         buttons: [{
                                name: 'fans',
                                text:'粉丝概况',
                                icon: 'fa fa-snowflake-o',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog btn-fans',
                                url: 'shop/userdistribution/fans'
                            },{
                                name: 'fansdetail',
                                text:'粉丝明细',
                                icon: 'fa fa-snowflake-o',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'shop/userdistribution/fansdetail'
                            },{
                                name: 'incomedetail',
                                text:'收益明细',
                                icon: 'fa fa-money',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'shop/userdistribution/incomedetail'
                            },{
                                name: 'store',
                                text:'店铺信息',
                                icon: 'fa fa-window-restore',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'shop/userdistribution/store'
                            }],
                         formatter: Table.api.formatter.operate
                     }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-fans").data("area", ["450px", "350px"]);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        fansdetail: function(){
            Table.api.init({
                extend: {
                    index_url: 'shop/userdistribution/fansdetail',
                    table: 'fansdetail',
                }
            });

            var table = $("#table"), options = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                commonSearch: false,

                columns: [
                    [
                        {field: 'userpic', title:'粉丝头像',formatter: Table.api.formatter.image},
                        {field: 'name', title: '粉丝名称'},
                        {field: 'sumorder', title: '累记总订单数'},
                        {field: 'sumsales', title: '累记总销售'},
                        {field: 'sumoutcome', title: '累记总贡献'},
                        {field: 'status', title: '当前状态',formatter:function(a,b,i){
                            if(a==0){
                                return '粉丝已切换';
                            }
                            else if(a==1){
                                return '已成您的粉丝';
                            }
                        }},
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            };
            table.bootstrapTable(options);

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        incomedetail: function(){
            Table.api.init({
                extend: {
                    index_url: 'shop/userdistribution/incomedetail',
                    table: 'incomedetail',
                }
            });
            var table = $("#table"), options = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                commonSearch: false,
                columns: [
                    [
                        {title: '序号', formatter: function(a,b,c){return c+1}},
                        {field: 'order.snorder', title: '订单编号'},
                        {field: 'order.mobile', title: '用户手机号'},
                        {field: 'create_time', title: '购买时间',formatter:function(a,b,i){
                            var t=a*1000;
                            var date=new Date(t);
                            return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
                        }},
                        {field: 'backup', title: '购买产品'},
                        {field: 'price', title: '营业额', formatter:function(a){return '+ '+a}},
                        {field: 'points', title: '增加收益',formatter:function(a,b,i){return '+ '+a}},
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            };
            table.bootstrapTable(options);

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});