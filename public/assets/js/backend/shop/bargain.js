define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/bargain/index',
                    add_url: 'shop/bargain/add',
                    edit_url: 'shop/bargain/edit',
                    del_url: 'shop/bargain/del',
                    multi_url: 'shop/bargain/multi',
                    table: 'bargain',
                }
            });

            var table = $("#table");
            // 初始化表格
            $.getJSON('shop/bargain/cate',function (porgindef) {
                var cate=city={};

                cate=porgindef[0];

                city=porgindef[1];

                table.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.index_url,
                    search: false,
                    pagination: true,
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'id', title: '序号', sortable: true},
                            {field: 'city_code', title: '城市',searchList:city,visible: false},
                            {field: 'city_data.city_name', title: '城市',operate: false},
                            {field: 'sngoods', title: '商品编号', sortable: true},
                            {field: 'name', title: '商品名称', sortable: true},
                            {field: 'cate', title: '商品分类',searchList:cate,visible: false},
                            {field: 'cate_data.name', title: '商品分类',operate: false},

                            {field: 'commonprice', title: '商品原价', sortable: true,operate: false},
                            {field: 'bargain_data.freecount', title: '特惠价', sortable: false,operate: false},
                            {field: 'deal_times', title: '真实销量', sortable: true,operate: false},
                            {field: 'create_time', title: '创建时间', sortable: true,formatter: Table.api.formatter.datetime},
                            {field: 'status', title: '特惠区上架状态',formatter: Table.api.formatter.toggle,operate: false},
                            {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ]
                });

                // 为表格绑定事件
                Table.api.bindevent(table);
                
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $("button.btn-close").click(function () {
                    Layer.closeAll();
                });
            }
        }
    };
    return Controller;
});