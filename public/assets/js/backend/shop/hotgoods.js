define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/hot_goods/index',
                    add_url: 'shop/hot_goods/add',
                    edit_url: 'shop/hot_goods/edit',
                    del_url: 'shop/hot_goods/del',
                    multi_url: 'shop/hot_goods/multi',
                    table: 'goods_hot'
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                pk: 'id',
                sortName: 'weigh',
                pagination: false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'), formatter: function (a,b,c) {return c+1}},
                        {field: 'weigh', title: __('权重'), operate: 'BETWEEN', sortable: true},
                        {field: 'pic', title: __('图片'), formatter: Table.api.formatter.image, operate: false},
                        {field: 'goods.name', title: __('产品名')},
                        {field: 'goods.commonprice', title: __('价格')},
                        {field: 'goods.valid_times', title: __('已售数量')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('load-success.bs.table', function (e, data) {
                if(data.total) {

                }
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});