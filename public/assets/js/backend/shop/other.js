define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var table = $("#table"),status,source,
                schedule = function (a,row) {
                 return Table.api.formatter.status.call(this, status[a], row);
            };
            $.getJSON('shop/order/status',function (s) {
                status = s;
            });
            $.getJSON('shop/order/source',function (s) {
                source = s;
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop/other/index',
                    add_url: 'shop/other/add',
                    edit_url: 'shop/other/edit',
                    del_url: '',
                    multi_url: '',
                    table: 'order',
                }
            });

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: true,
                pk: 'id',
                sortName: 'o.create_time',
                pagination: true,
                columns: [
                    [
                        {field: 'id', title: '序号', operate: false, formatter: function (a,b,c) {
                                a=c+1;c = $(window).width()>1200;
                                if(b.mark&&b.mark.length>0)a += '<img src="/assets/img/ac.png" style="width:38px;height:18px;margin:-10px 0 0 ' +
                                    (c? 40:10)+'px;transform: rotate(-40deg);position: absolute;opacity: 0.8;" />';
                                return a
                            }},
                        {field: 'coupons', title: __('券号'), operate: 'LIKE'},
                        {field: 'nickname', title:'姓名', operate: 'LIKE'},
                        {field: 'mobile', title:'电话', operate: 'LIKE'},
                        {field: 'actualpay', title:'付款', operate: false},
                        {field: 'address', title: '服务地址', operate: 'LIKE'},
                        {field: 'orgin', title:'录入平台', operate: false, formatter:  function (a) {return source[a];}},
                        {field: 'admin', title:'录入人员', operate: 'LIKE'},
                        {field: 'create_time', title: __('录入时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'edit',
                                    icon: 'fa fa-user',
                                    text: __('修改'),
                                    extend: 'data-toggle="tooltip" data-title="修改"',
                                    classname: 'btn btn-xs btn-success btn-editone',
                                    url: 'shop/todo/edit'
                                }],
                            formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams: function (params) {
                    params.op = params.op.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    params.filter = params.filter.replace('nickname','o.nickname')
                        .replace('mobile', 'o.mobile')
                        .replace('create_time', 'o.create_time');
                    return params;
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-warning").data("area", ["300px", "350px"]);
            });
            table.on("mouseenter", "img", function (e) {
                var ids = $(this).parent().parent().data('index'),
                    d = Table.api.getrowbyindex(table, ids),
                    l = $("<div/>").html(d.mark).css({"left":e.pageX+2+"px","top":e.pageY+2+"px",
                        "position":"absolute","background":"#fff", "border":"1px solid #999",
                        "font-size":"15px","box-shadow": "1px 1px 20px rgba(0, 0, 0, 0.5)",
                        "padding":"10px","width":"200px","min-height":"150px"}).appendTo(document.body);
                $(this).mouseleave(function () {
                    setTimeout(function (){l.remove()},100);
                });
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
        },
        operate: function (a, b, c) {
            var buttons = $.extend([], this.buttons || []),
                us = ["customer","customerassign","customersure", "customerwait",
                    "close","customerin","customerend","close","","todo"];
            buttons.push({
                icon: 'fa fa-share',
                title: __('Edit'),
                text: __('跳转'),
                extend: 'data-title="'+b.schedule+'"',
                classname: 'btn btn-xs btn-success btn-re',
                url: 'shop/'+us[b.schedule]+'/index'
            });
            return Table.api.buttonlink(this, buttons, a, b, c, 'operate');
        },
        counter: null
    };
    return Controller;
});