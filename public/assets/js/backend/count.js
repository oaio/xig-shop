define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            Table.api.init({
                extend: {
                    index_url: 'count/index',
                    table: 'count',
                }
            });

            var table = $("#table"), options = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                pagination: false,
                commonSearch: false,
                sortName: 'date',
                pk: 'date',
                columns: [
                    [
                        {field: 'id', title: __('序号'), formatter:function(a,b,i){return i+1}},
                        {field: 'date', title: __('日期'),sortable: true},
                        {field: 'id', title: __('笔数')},
                        {field: 'money', title: __('付款总额'), formatter: Controller.api.money},
                        {
                            field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    title: __('详情'),
                                    text: __('详情'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'count/detail'
                                }],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            };
            table.bootstrapTable(options);

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', '.btn-change', function () {
                var that = this;
                options = $.extend(options, $(that).data() || {});
                table.bootstrapTable('refresh', options);
            });
        },
        detail: function(){
            Table.api.init({
                extend: {
                    index_url: 'count/detail',
                    table: 'count',
                }
            });

            var table = $("#table"), options = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                search: false,
                commonSearch: false,
                columns: [
                    [
                        {field: 'id', title: __('ID'),sortable: true},
                        {field: 'trade_no', title: __('订单号')},
                        {field: 'pay.type', title: __('类型')},
                        {field: 'info.name', title: __('客户'), formatter: Controller.api.name},
                        {field: 'info.phone', title: __('电话'), formatter: Controller.api.phone},
                        {field: 'info.address', title: __('地址'), formatter: Controller.api.address},
                        {field: 'note', title: __('备注')},
                        {field: 'total_fee', title: __('支付金额'), formatter: Controller.api.money},
                        {field: 'create_time', title: __('支付时间'), formatter: Table.api.formatter.datetime}
                    ]
                ],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            };
            table.bootstrapTable(options);

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            name: function(a,b){
                if(b.orders && b.orders.nickname)return b.orders.nickname;
                return a;
            },
            phone:function(a,b){
                if(b.orders && b.orders.mobile)return b.orders.mobile;
                return a;
            },
            address:function(a,b){
                if(b.orders && b.orders.address)return b.orders.address;
                return a;
            },
            money: function (a) {
                return '￥' + (a / 100).toFixed(2);
            }
        }
    };
    return Controller;
});