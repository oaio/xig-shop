define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "请输入电话、店主名、店铺"};

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index: 'store/cash/index',
                    check: 'user/cash/check',
                    detail: 'store/cash/detail',
                    table: 'cash',
                }
            });

            var table = $("#table");
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index,
                columns: [
                    [
                        {field: 'id', title: __('序号'), formatter: function(a,b,c) {return c+1}, operate: !1},
                        {field: 'create_time', title: __('发起时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        {field: 'cash_num', title: __('订单编号'), extend:'autocomplete="off"'},
                        {field: 'user.name', title: __('用户名'),  visible: !1, operate: !1},
                        {field: 'user.phone', title: __('电话'), operate: 'LIKE %...%', placeholder: '提现人电话',
                            formatter: Table.api.formatter.search, extend:'autocomplete="off"'},
                        {field: 'store.ruler', title: __('绑定店主'), operate: 'LIKE %...%', placeholder: '模糊搜索',
                            formatter: Table.api.formatter.search, extend:'autocomplete="off"'},
                        {field: 'store.ruler_phone', title: __('店主电话'), visible: !1, operate: !1},
                        {field: 'store.nickname', title: __('店铺名称'), operate: 'LIKE %...%', placeholder: '模糊搜索',
                            formatter: Table.api.formatter.search, extend:'autocomplete="off"'},
                        {field: 'store.admin.city.region_name', title: __('城市'), operate: !1},
                        {field: 'costs', title: __('总金额'), sortable: 1, operate: !1},
                        {field: 'amount', title: __('自动打款'), sortable: 1, operate: !1},
                        {field: 'transfer', title: __('手动转账'), sortable: 1, operate: !1},
                        {field: 'status', title: __('Status'), sortable: 1, formatter: Controller.api.status, operate: !1},
                        {field: 'notes', title: __('驳回原因'),  visible: !1, operate: !1},
                        {field: 'user.nickname', title: __('微信名'),  visible: !1, operate: !1},
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Controller.api.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: __('收益明细'),
                                    icon: 'fa fa-list',
                                    classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                    url: $.fn.bootstrapTable.defaults.extend.detail
                                },
                                {
                                    name: 'check',
                                    text: __('审核选项'),
                                    icon: 'fa fa-check-square',
                                    classname: 'btn btn-danger btn-xs btn-detail btn-dialog',
                                    url: $.fn.bootstrapTable.defaults.extend.check
                                }
                            ]
                        }
                    ]
                ],
                showExport: !0,
                showColumns: !0,
                queryParams: function (params) {
                    params.sort = params.sort.replace('create_time', 'cash.create_time');
                    params.op = params.op.replace('create_time', 'cash.create_time');
                    params.filter = params.filter.replace('create_time', 'cash.create_time');
                    return params;
                }
            });
            table.on('load-success.bs.table', function (e, data) {
                $("#money").text(data.sum);
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('.search').css({'width':'360px','margin-right':'10px'});
            table.on('post-body.bs.table', function () {
                $(".btn-danger").data("area", ["350px", "410px"]);
            });
        },
        check:function () {
            $(document).on('click', 'input:radio',function () {
                Controller.api.bindevent();
            })
        },
        detail: function () {
            Table.api.init();
            var table = $("#table");
            table.bootstrapTable({
                url: 'store/cash/detail',
                columns: [
                    [
                        {field: 'id', title: __('序号'), operate: false, formatter: function (a,b,c) {return c+1}},
                        {field: 'pay_id', title: __('支付ID'), visible: !1},
                        {field: 'snorder', title: __('订单编号'), visible: !1},
                        {field: 'create_time', title: __('购买时间'), formatter: Table.api.formatter.datetime},
                        {field: 'goods_name', title: __('购买产品')},
                        {field: 'nickname', title: __('Username')},
                        {field: 'address', title: __('地址'), visible: !1},
                        {field: 'mobile', title: __('手机号'),  formatter: Table.api.formatter.search},
                        {field: 'shouldpay', title: __('营业额'), formatter: function(a,b){return "+￥"+a}},
                    ]
                ],
                search: !1,
                commonSearch: !1,
                showExport: !0,
                showColumns: !0,
                exportTypes: ['excel'],
                queryParams: function (params) {
                    params.ids = ids;
                    return params;
                }
            });
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            operate: function (a,b,c) {
                var buttons = $.extend([], this.buttons);
                b.status != 1 && buttons.pop();
                return Table.api.buttonlink(this, buttons, a, b, c, 'buttons');
            },
            status: function (c) {
                var a=['','<b>待审核</b>','通过','打款成功','已冻结','被驳回'];
                return a[c];
            },
            type: function (a) {
                return a==1? '微信钱包' : '银行卡';
            }
        }
    }, ay=function (a) {return a != null ?"￥"+a:a};
    return Controller;
});