define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'store/platform/index',
                    add_url: 'store/platform/add',
                    edit_url: 'store/platform/edit',
                    del_url: 'store/platform/del',
                    multi_url: 'store/platform/multi',
                    table: 'platform',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {checkbox: !0},
                        {field: 'id', title: __('推荐ID')},
                        {field: 'name', title: __('平台名称'), align: 'left'},
                        {field: 'contacts', title: __('联系人')},
                        {field: 'company', title: __('公司名称'), align: 'left'},
                        {field: 'note', title: __('备注')},
                        {field: 'invite', title: __('邀请人数')},
                        {field: 'createtime', title: __('添加时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true, visible: false},
                        {field: 'updatetime', title: __('更新时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true, visible: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [{
                                name: 'fans',
                                icon: 'fa fa-qrcode',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog btn-qr',
                                url: 'store/platform/qrcode'
                            }]
                        }
                    ]
                ],
                search: !0,
                commonSearch: !1,
                pagination: !0,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-qr").data("area", ["480px", "480px"]);
            });
            $(document).on("click", ".btn-update", function () {
                Fast.api.ajax({
                    url: 'store/platform/update',
                }, function (data, ret) {
                    table.bootstrapTable('refresh');
                });
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});