define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 ID/姓名/手机号"};
            Table.api.init({
                extend: {
                    index_url: 'store/withdraw/index',
                    table: 'platform',
                }
            });

            var table = $("#table");

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [//visible: false
                        {field: 'id', title: __('序号'), operate: !1, formatter: function(){return arguments[2]+1}},
                        {field: 'w.create_time', title: __('时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                        {field: 'u.s.nickname', title: __('门店名称'), operate: 'LIKE', align: 'left'},
                        {field: 'u.s.ruler', title: __('店主姓名'), operate: 'LIKE', align: 'left'},
                        {field: 'w.uid', title: __('店主用户ID'), sortable: 1},
                        {field: 'u.phone', title: __('店主注册手机号'), sortable: 1},
                        {field: 'w.money', title: __('提现金额'), sortable: 1},
                        {field: 'before_money', title: __('提现前余额'), sortable: 1, operate: !1},
                        {field: 'after_money', title: __('提现后余额'), sortable: 1, operate: !1},
                        {field: 'w.status', title: __('提现状态'), formatter: Table.api.formatter.status,
                            searchList: {1:"成功", 2: "失败"}, custom:{2:"danger",1:"success"}}
                    ]
                ],
                search: !0,
                commonSearch: 1,
                pageSize: 50,
                queryParams: function (params) {
                    params.op = params.op.replace('u.s', 's');
                    params.filter = params.filter.replace('u.s', 's');
                    return params;
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-qr").data("area", ["480px", "480px"]);
            });
            table.on('load-success.bs.table', function (e, data) {
                $("span.extend span").text("￥" + data.extend.toFixed(2));
            });
            $(document).on("click", ".btn-update", function () {
                Fast.api.ajax({
                    url: 'store/platform/update',
                }, function (data, ret) {
                    table.bootstrapTable('refresh');
                });
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});