define(['jquery', 'table'], function ($, Table) {
    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索门店/管理人/电话"};
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    "index_url": "store/index/index",
                    "add_url": "",
                    "edit_url": "",
                    "del_url": "",
                    "multi_url": "",
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: 'admin_id',
                sortName: 'store_id',
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'store_id', title: '序号', formatter: function (a,c,i) {
                                return i+1;
                            }},
                        {field: 'nickname', title: __('门店名称'), align: 'left'},
                        {field: 'company', title: __('公司名称'), align: 'left'},
                        {field: 'ruler', title: __('管理人')},
                        {field: 'ruler_phone', title: __('管理人电话')},
                        {field: 'admin_nickname', title: __('账号')},
                        {
                            field: 'is_af', title: __('开通状态'),
                            searchList:{
                                '0': __('未开通'),
                                '1': __('开通'),
                                '2': __('已开通'),
                                '3': __('冻结'),
                                '4': __('已冻结'),
                                '5': __('重新开通'),
                            },
                            custom: {
                                "2":"success",
                                "4":"red"
                            },
                            operate: false,
                            icon: '',
                            formatter: Table.api.formatter.normal
                        },
                        /*{
                            field: 'create_time',
                            title: __('Jointime'),
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            sortable: true
                        },*/
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [{
                                name: 'detail',
                                text: __('重置密码'),
                                icon: 'fa fa-unlock-alt',
                                extend: 'data-toggle="tooltip"',
                                classname: 'btn btn-info btn-xs btn-detail btn-pass',
                                url: 'store/index/pass'
                            }],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ],
                //pagination: false,
                commonSearch: false,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('.search').css({'width':'300px','margin-right':'10px'});

            $(document).on('click', '.btn-pass', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var that = this;
                var top = $(that).offset().top - $(window).scrollTop();
                var left = $(that).offset().left - $(window).scrollLeft() - 260;
                if (top + 154 > $(window).height()) {
                    top = top - 154;
                }
                if ($(window).width() < 480) {
                    top = left = undefined;
                }
                Layer.confirm(
                    __('确定要重置密码?'),
                    {icon: 3, title: __('Warning'), offset: [top, left], shadeClose: true},
                    function (index) {
                        Layer.close(index);
                        Layer.prompt({title: '请输入新密码', value:'xgb123456', formType: 3},function (val, index) {
                            $.post($(that).prop('href'),'password='+val, function (s) {
                                s && s.msg && Toastr.success(s.msg);
                            });
                            Layer.close(index);
                        });
                    }
                );
                return !1;
            });//click
        },
        api: {
            bindevent: function () {

            }
        }
    };
    return Controller;
});