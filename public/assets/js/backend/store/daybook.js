define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "输入电话、姓名、店铺"};
            Table.api.init();
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'store/day_book/index',
                columns: [
                    [
                        {field: 'id', title: __('序号'), formatter: function(a,b,c){return c+1}, operate: !1},
                        {field: 'store.ruler_phone', title: __('电话'), formatter: Table.api.formatter.search},
                        {field: 'store.ruler', title: __('店主名'), operate: 'LIKE %...%', placeholder: '模糊搜索',
                            formatter: Table.api.formatter.search, extend:'autocomplete="off"'},
                        {field: 'store.nickname', title: __('Nickname'), align: 'left', operate: 'LIKE %...%', placeholder: '模糊搜索',
                            formatter: Table.api.formatter.search, extend:'autocomplete="off"'},
                        {field: 'store_count', title: __('总单数'), operate: !1, sortable: !0},
                        {field: 'store_total', title: __('总销售额'), operate: !1, sortable: !0, formatter: ay},
                        {field: 'pay', title: __('总实付'), operate: !1, sortable: !0, formatter: ay},
                        {field: 'coupon', title: __('总抵扣'), operate: !1, sortable: !0, formatter: ay},
                        {field: 'storeuserinfo.sumincome', title: __('人个分销'), operate: !1, formatter: ay},
                        {field: 'storeuserinfo.all_can_withdraw', title: __('可提额度'),
                            operate: !1, formatter: ay},
                        // {field: 'store.cash.money', title: __('已审额度'), operate: !1, formatter: ay},
                        {field: 'create_time', title: __('时间段'),placeholder: '请点击，选择自定义',
                            formatter: Table.api.formatter.datetime, extend:'autocomplete="off"',
                            operate: 'RANGE', addclass: 'datetimerange', visible: !1},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [{
                                name: 'detail',
                                text: __('Detail'),
                                icon: 'fa fa-list',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'store/day_book/detail'
                            }],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ],
                pk: 'store_id',
                showExport: !0,
                // showColumns: !0,
                sortName: 'order.store_id',
                queryParams: function (params) {
                    params.op = params.op.replace('create_time', 'order.create_time');
                    params.filter = params.filter.replace('create_time', 'order.create_time');
                    return top.params = params;
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            Table.api.init();
            var table = $("#table");
            table.bootstrapTable({
                url: 'store/day_book/detail',
                columns: [
                    [
                        {field: 'id', title: __('序号'), operate: false, formatter: function (a,b,c) {return c+1}},
                        {field: 'pay_id', title: __('支付ID'), visible: !1},
                        {field: 'snorder', title: __('订单编号'), visible: !1},
                        {field: 'create_time', title: __('购买时间'), formatter: Table.api.formatter.datetime},
                        {field: 'goods_name', title: __('购买产品')},
                        {field: 'nickname', title: __('Username')},
                        {field: 'address', title: __('地址'), visible: !1},
                        {field: 'mobile', title: __('手机号'),  formatter: Table.api.formatter.search},
                        {field: 'actualpay', title: __('营业额'), formatter: function(a,b){return "￥"+((a-0)+(b.dismoney-0)).toFixed(2)}},
                        {field: 'actualpay', title: __('实付'), formatter: ay},
                        {field: 'dismoney', title: __('抵扣'), formatter: ay},
                    ]
                ],
                sortName: 'order.create_time',
                search: !1,
                commonSearch: !1,
                showExport: !0,
                showColumns: !0,
                exportTypes: ['excel'],
                queryParams: function (params) {
                    params.ids = ids;
                    // params.filter = top.params.filter;
                    // params.op = top.params.op;
                    return params;
                }
            });
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
        }
    }, ay=function (a) {return a != null ?"￥"+a:a};
    return Controller;
});