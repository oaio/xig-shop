define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'general/deduction/index',
                    add_url: 'general/deduction/add',
                    edit_url: 'general/deduction/edit',
                    del_url: 'general/deduction/del',
                    table: 'shop_userbean_rule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {checkbox: !0},
                        {field: 'id', title: __('序号')},
                        {field: 'cid', title: __('Cid'), visible: !1},
                        {field: 'name', title: __('规则名称')},
                        {field: 'status', title: __('状态'), align: 'right'},
                        {field: 'times', title: __('订单序数')},
                        {field: 'discount', title: __('抵扣百分比')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                search: !1,
                commonSearch: !1,
                pagination: !1,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(".btn-date").data("area", ["300px", "400px"]);
            $(document).on("click", ".btn-setting", function () {
                var ids = Table.api.selectedids(table);
                var url = $.fn.bootstrapTable.defaults.extend.setting;
                if (url.indexOf("{ids}") !== -1) {
                    url = Table.api.replaceurl(url, {ids: ids.length > 0 ? ids.join(",") : 0}, table);
                }
                Fast.api.open(url, __('Setting'), $(this).data() || {});
            });
            $(document).on("click", ".btn-date", function () {
                Fast.api.open('general/time/date', $(this).attr('title'), $(this).data() || {});
            });
        },
        add: function () {
            Controller.api.bindevent();
            $(".btn-append").on("click", function () {
                if($("dd.form-inline").length>19){
                    Toastr.error(__('You can add up to %d row', 20));
                    return !1;
                }
            });
        },
        edit: function () {
            Controller.api.bindevent();
            $(".btn-append").on("click", function () {
                if($("dd.form-inline").length>19){
                    Toastr.error(__('You can add up to %d row', 20));
                    return !1;
                }
            });
        },
        setting: function(){
            Controller.api.bindevent();
        },
        date: function(){
            Form.api.bindevent($("form[role=form]"),'',function () {
                $.a=0;
            },function () {
                !$.a && Layer.confirm(
                    __('This operation is irreversible, please be careful!'),
                    {icon: 3, title: __('Warning'), offset: 0, shadeClose: !0,
                        skin: 'layui-layer-molv', btn:[__('Go on'), __('Cancel')]},
                    function (index) {$.a=1;
                        $("form[role=form]").submit();
                        Layer.close(index);
                    }
                );
                return !!$.a;
            })
            $(document).on("change", ".cate", function () {
                $.ajax({
                    url: "general/time/part/n/" + $(this).val(),
                    success: function (data) {
                        data.msg && $(".m-box").each(function (i,e) {
                            $(e).html(data.msg[i])
                        })
                    }
                });
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"),'','',function () {
                    for (var i=0,a=$("[name$='[]']"), b=a.length;i<b;i++)
                        if(a.eq(2*i).val()>a.eq(1+2*i).val()){
                        if(i==2&&parseInt(a.eq(5).val())==0)return;
                        Toastr.error(__('Initial to end time is incorrect'));return !1;
                    }
                });
            }
        }
    };
    return Controller;
});