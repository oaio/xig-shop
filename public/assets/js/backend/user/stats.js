define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/stats/index',
                    multi_url: 'user/group/multi',
                    table: 'user_stats',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pageSize: 25,
                showColumns: 1,
                showExport: 1,
                sortName: 'b.id',
                columns: [
                    [
                        {field: 'id', title: '序号', operate: !1, formatter:function (a,b,c) {return c+1}},
                        {field: 'uid', title: '用户ID', operate: 'LIKE', sortable: 1,
                            formatter: Table.api.formatter.search},
                        {field: 'u.phone', title: '手机', operate: 'LIKE', formatter: Table.api.formatter.search},
                        {field: 'u.name', title: '姓名', operate: 'LIKE'},
                        {field: 'kind', title: '类型', sortable: 1, custom:{1:"danger",2:"success"},
                            searchList:{1:"红包池", 2:"可用池"}, formatter: Table.api.formatter.flag},
                        {field: 'cost', title: '数值', operate: !1, sortable: 1},
                        {field: 'action', title: '方向', operate: !1, sortable: 1,
                            searchList:{1:"增加", 2:"减少"}, formatter: Table.api.formatter.normal},
                        {field: 'ago', title: '交易前余额', operate: !1, sortable: 1},
                        {field: 'end', title: '交易后余额', operate: !1, sortable: 1},
                        {field: 'b.create_time', title: '交易时间', formatter: Table.api.formatter.datetime,
                            operate: 'RANGE', addclass: 'datetimerange', sortable: 1, extend:'autocomplete="off"'},
                    ]
                ]
            });

            table.on('load-success.bs.table', function (e, data) {
                var a=['gift', 'active', 'got', 'used'],
                    b=data.extend,c=['open', 'able', 'red'];
                $(".sm-st-info span").each(function (i, e) {
                    $(e).text(b[a[i]]);
                });
                $(".ibox-title span").text(data.extend.time);
                $("span.extend span").each(function (i, e) {
                    $(e).text(b[c[i]]);
                });
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {},
        gift: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 用户/手机号/昵称"};
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatShowingRows'] =
                $.fn.bootstrapTable.locales[Table.defaults.locale]['formatRecordsPerPage'] = function () {
                return '';
            };
            Table.api.init();
            var ids = [],table = $("#table");
            table.bootstrapTable({
                url: 'user/stats/user',
                pageSize: 6,
                commonSearch: !1,
                smartDisplay: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: 'ID', sortable: 1},
                        {field: 'name', title: '用户', operate: !1},
                        {field: 'nickname', title: '微信昵称', operate: !1}
                    ]
                ]
            });
            Table.api.bindevent(table);
            $('.search').css({'width':'280px','margin-right':'10px'});
            $(document).on('click', '.btn-add', function () {
                if($("input:checked").length<1)Toastr.error("至少选择一个用户");
                ids = $.unique(ids.concat(Table.api.selectedids(table)).sort());
                $(".add_ids").val(ids.join(','));
                $("#add_num").text(ids.length);
            });
            $(document).on('click', '.del-ids', function () {
                $.each(Table.api.selectedids(table), function (i,id) {
                    ids = $.grep(ids, function (v) {
                        return v !=id;
                    });
                });
                $(".add_ids").val(ids.join(','));
                $("#add_num").text(ids.length);
            });
            $(document).on("click", ".btn-submit",function (a) {
                a = $("#c-weigh").val()-0;
                if(ids.length)
                Layer.confirm(
                    ['即将给',ids.length,'客户发送小哥豆红包，每个红包', a,'元，合计',
                        (a*ids.length).toFixed(2), '元'].join(''),
                    {icon: 3, title: __('Warning'), shadeClose: true},
                    function (index) {
                        $(".btn-embossed").trigger("click");
                        Layer.close(index);
                    }
                );
                else Toastr.error("请添加用户");
            });
            Form.api.bindevent($("form[role=form]"));
        }
    };
    return Controller;
});