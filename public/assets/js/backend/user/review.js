define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "搜索 姓名"};
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/review/index',
                    add_url: 'user/review/add',
                    check: 'user/review/check',
                    edit_url: 'user/review/edit',
                    del_url: 'user/review/del',
                    multi_url: 'user/review/multi',
                    table: 'shop_userinfo_store_review',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                escape: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false, sortable: true},
                        {field: 'review.uid', title: '用户id', operate: false, sortable: true},
                        {field: 'review.phone', title: __('电话'), align: 'left'},
                        {field: 'review.realname', title: __('姓名'), align: 'left'},
                        {field: 'sex', title: __('姓别'), operate: false,sortable: true,formatter: function (a,b,c) {
                             if(a==1){
                                return '女';
                             }
                             else if(a==0){
                                return '男';
                             }
                             else{
                                return '未知性别';
                             }
                        }},
                        {field: 'address', title: __('地址'), operate: false},
                        {field: 'idnumber', title: __('身份证号'), operate: false},
                        {field: 'status', title: __('Status'), formatter: Controller.api.status, operate: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Controller.api.operate,
                            buttons: [
                                {
                                    name: 'check',
                                    text: __('审核选项'),
                                    icon: 'fa fa-check-square',
                                    classname: 'btn btn-danger btn-xs btn-detail btn-dialog',
                                    url: $.fn.bootstrapTable.defaults.extend.check
                                }
                            ]
                        }
                    ]
                ],
                search: !0,
                commonSearch: !0,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-danger").data("area", ["300px", "200px"]);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        check: function(){
            $("head").append("<style>.content{min-height:auto}</style>");
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            },
            operate: function (a,b,c) {
                return !b.status?Table.api.buttonlink(this, this.buttons, a, b, c, 'buttons') :'';
            },
            status: function (a) {
                return ["审核中", "过审", "驳回"][a];
            }
        }
    };
    return Controller;
});