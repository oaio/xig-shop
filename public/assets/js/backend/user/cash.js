define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $.fn.bootstrapTable.locales[Table.defaults.locale]['formatSearch'] = function(){return "请输入用户名"};

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index: 'user/cash/index',
                    check: 'user/cash/check',
                    detail: 'user/cash/detail',
                    table: 'cash',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index,
                sortName: 'cash.id',
                showExport: !0,
                showColumns: !0,
                columns: [
                    [
                        {field: 'id', title: __('序号'), operate: !1, formatter: function(a,b,c){return c+1}},
                        {field: 'create_time', title: __('发起时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE',
                            addclass: 'datetimerange', sortable: true, extend:'autocomplete="off"'},
                        {field: 'cash_num', title: __('订单编号'), operate: 'LIKE'},
                        {field: 'user.phone', title: __('电话'), operate: 'LIKE'},
                        {field: 'username', title: __('姓名'), operate: 'LIKE'},
                        {field: 'user.store.nickname', title: __('绑定店铺'), operate: !1},
                        {field: 'notes', title: __('驳回原因'),  visible: !1, operate: !1},
                        {field: 'amount', title: __('金额'), sortable: true, operate: !1},
                        {field: 'status', title: __('Status'), searchList:{1:"待审核",2:"通过",4:"冻结"},
                            sortable: true, formatter: Controller.api.status},
                        {field: 'type', title: __('提现方式'), operate: !1, formatter: Controller.api.type},
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Controller.api.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: __('查看信息'),
                                    icon: 'fa fa-list',
                                    classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                    url: $.fn.bootstrapTable.defaults.extend.detail
                                },
                                {
                                    name: 'check',
                                    text: __('审核选项'),
                                    icon: 'fa fa-check-square',
                                    classname: 'btn btn-danger btn-xs btn-detail btn-dialog',
                                    url: $.fn.bootstrapTable.defaults.extend.check
                                }
                            ]
                        }
                    ]
                ],
                queryParams: function (params) {
                    params.sort = params.sort.replace('status', 'cash.status')
                        .replace('create_time', 'cash.create_time');
                    params.op = params.op.replace('status', 'cash.status')
                        .replace('create_time', 'cash.create_time');
                    params.filter = params.filter.replace('status', 'cash.status')
                        .replace('create_time', 'cash.create_time');
                    return params;
                }
            });
            table.on('load-success.bs.table', function (e, data) {
                $("#money").text(data.sum);
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-info").data("area", ["350px", "400px"]);
                $(".btn-danger").data("area", ["350px", "410px"]);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        check:function () {
            $(document).on('click', 'input:radio',function (e) {
                Controller.api.bindevent();
                // e = $('input:text');
                // $('input:radio:first').prop('checked') ? e.show() : e.hide();
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            operate: function (a,b,c) {
                var ary = [],buttons = $.extend([], this.buttons);
                if(b.type == 2) ary.push(buttons[0]);
                if(b.status == 1) ary.push(buttons[1]);
                return Table.api.buttonlink(this, ary, a, b, c, 'buttons');
            },
            status: function (c) {
                var a=['','<b>待审核</b>','通过','打款成功','已冻结','被驳回'];
                return a[c];
            },
            type: function (a) {
                return a==1? '微信钱包' : '银行卡';
            }
        }
    };
    return Controller;
});