define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/feedback/index',
                    del_url: 'user/feedback/del',
                    multi_url: 'user/feedback/multi',
                    table: 'banner',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'create_time',
                pagination: true,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('ID'), sortable: true},
                        {field: 'feedback', title: __('反馈内容'), formatter:function (v, r) {
                                if(v.length>20) v = v.substring(0,20)+"...";
                                return !r.admin_id ? "<b>"+v+"</b>" : v;
                            }
                        },
                        {field: 'user.name', title:  __('用户名')},
                        {field: 'create_time', title: __('反馈时间'), formatter: Table.api.formatter.datetime, sortable: true},
                        {
                            field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: __('Detail'),
                                    classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'user/feedback/detail'
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                     ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});