define(['jquery', 'table', 'form'], function ($, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/info/index',
                    add_url: 'user/info/add',
                    edit_url: 'user/info/edit',
                    multi_url: 'user/info/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                commonSearch: true,
                search:false,
                sortOrder:'asc',
                columns: [
                    [
                        {field: 'id', title: __('ID'), sortable: 1},
                        {field: 'fa_shop_userinfo.name', title: __('姓名'), formatter: function (v, r) {
                                return '<a href="/admin/user/info/detail/ids/'+r.id+'" class="btn-dialog" title="查看">'+v+'</a>';
                            }, operate:'LIKE'},
                        {field: 'fa_shop_userinfo.phone', title: __('手机号码'),  operate: 'LIKE'},
                        {field: 'fa_shop_userinfo.nickname', title: __('微信昵称'), operate: 'LIKE'},
                        {field: 'email', title: __('会员'), operate: !1},
                        {field: 'city.region_name', title: __('所在城市'), operate: !1},
                        {field: 'area', title: __('区域 (注册)'), operate: !1},
                        {field: 'address', title: __('详细地址'), operate: !1},
                        {field: 'sumincome', title: __('分销收益'), sortable: 1, operate: !1},
                        {field: 'lottery.effective_bean', title: __('小哥豆'), operate: !1},
                        {field: 'store.nickname', title: __('绑定店铺'), operate: 'LIKE', formatter:Controller.api.formatter.self},
                        {field: 'fa_shop_userinfo.create_time', title: __('注册时间'), formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',addclass: 'datetimerange', sortable: 1, extend: 'autocomplete="off"'},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                {
                                    name: 'edit',
                                    icon: 'fa fa-plus',
                                    title: __('赠送小哥豆'),
                                    extend: 'data-toggle="tooltip" data-title="赠送"',
                                    classname: 'btn btn-xs btn-danger btn-editone',
                                    url: $.fn.bootstrapTable.defaults.extend.edit_url
                                }
                            ]
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function () {
                $(".btn-danger").data("area", ["280px", "350px"]);
            });

            $(document).on('click', '.btn-vip', function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    params.filter = params.filter ? JSON.parse(params.filter) : {};
                    params.op = params.op ? JSON.parse(params.op) : {};

                    params.filter = JSON.stringify($.extend({}, params.filter,
                        {vip_end_time:parseInt(new Date().getTime()/1000)}));
                    params.op = JSON.stringify($.extend({}, params.op,{vip_end_time:">"}));
                    return params;
                };
                table.bootstrapTable('refresh', {});
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                self: function (v) {
                    return v === null ? __('暂无') : v;
                }
            }
        }
    };
    return Controller;
});