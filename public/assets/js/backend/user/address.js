define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/address/index',
                    add_url: 'user/address/add',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'user/address/multi',
                    table: 'shop_address',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: __('ID')},
                        {field: 'uid', title: __('Uid'), visible: false},
                        {field: 'name', title: __('Name'), align: 'left'},
                        {field: 'phone', title: __('电话')},
                        {field: 'address', title: __('地址'), align: 'left'},
                        {field: 'is_default', title: __('默认'), formatter: function (a) {return a?'是':'否'}},
                        {field: 'createtime', title: __('添加时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true, visible: false},
                        {field: 'updatetime', title: __('更新时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true, visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate, visible: false}
                    ]
                ],
                search: !0,
                commonSearch: !0,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});