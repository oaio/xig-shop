define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        panel.find(".btn-refresh").trigger("click");
                    });
                }
                $(this).unbind('shown.bs.tab');
            });
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        two: function () {
            var table = $("#table2");

            // 初始化表格
            table.bootstrapTable({
                url: 'group/user',
                escape: false,
                search: false,
                toolbar: 'toolbar2',
                sortName: 'group_id',
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id', title: 'ID', sortable: true},
                        {field: 'group_id', title: __('拼团ID'), sortable: true},
                        {field: 'user.nickname', title: __('微信昵称')},
                        {field: 'user.name', title: __('姓名')},
                        {field: 'user.phone', title: __('电话')},
                        {field: 'user.address', title: __('地址')},
                        // {field: 'creation_time', title: __('参团时间'),formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            $('.btn-refresh').off('click').on('click',function (e) {
                table.bootstrapTable('refresh');
            });
            Table.api.bindevent(table);
        },
        one: function () {
            var table = $("#table1");

            // 初始化表格
            table.bootstrapTable({
                url: 'group/index',
                toolbar:'toolbar1',
                escape: false,
                search: false,
                commonSearch: false,
                columns: [
                    [
                        {field: 'id', title: 'ID', sortable: true},
                        {field: 'goods.name', title:__('商品名称')},
                        // {field: 'goods.inoc', title: __('商品图片'), formatter: Table.api.formatter.image, operate: false},
                        {field: 'goods.group_number', title:__('成团人数')},
                        {field: 'number', title:__('参加人数')},
                        {field: 'user.name', title:__('发起人')},
                        {field: 'creation_time', title: __('发起时间'),formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            $('.btn-refresh').off('click').on('click',function (e) {
                table.bootstrapTable('refresh');
            });
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});