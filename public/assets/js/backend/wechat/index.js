define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'wechat/index/index',
                    del_url: 'wechat/index/del',
                    multi_url: 'wechat/index/multi',
                    template: 'wechat/index/template'
                }
            });


            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.template,
                escape: false,
                pk: 'id',
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'template_id', title: 'ID'},
                        {field: 'title', title: '标题'},
                        {field: 'content', title: '模板内容', align: 'left'},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [{
                                name: 'detail',
                                text: '编辑消息',
                                icon: 'fa fa-list',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'wechat/index/detail'
                            }],
                            formatter: function (value, row, index) {return Table.api.formatter.operate.call(this,value, row, index)}
                        }
                    ]
                ],
                pageSize: 50
            };
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);
            Form.api.bindevent($("form[role=form]"));
            $(document).on("click", ".btn-setting", function () {
                var ids = Table.api.selectedids(table);
                if(!ids.length)return Toastr.error("请钩选一个模板，用来发消息");
                if(ids.length>1)return Toastr.error("只能选一个");
                Fast.api.open('wechat/index/push/ids/'+ids, $(this).attr('title'), $(this).data() || {});
            });
        },
        detail: function () {
            Controller.api.bindevent();
        },
        push: function(){
            Table.api.init();
            let table = $("#table");
            table.bootstrapTable({
                sortName: 'm.store_id',
                url: 'wechat/index/push',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'm.id', title: 'ID', visible: !1, operate: !1},
                        {field: 'bu.id', title: '绑定ID'},
                        {field: 'bu.nickname', title: '微信昵称'},
                        {field: 'm.nickname', title: '店铺', align: 'left'},
                        {field: 'm.ruler', title: '店主'},
                        {field: 'm.ruler_phone', title: '电话'}
                    ]
                ]
            });
            Table.api.bindevent(table);
            Form.api.bindevent($("form[role=form]"), null, null, function () {
                if($("input:checked").length<1){
                    Toastr.error("至少选择一个店铺");
                    return !1;
                }
                $('textarea.hide').val(Table.api.selectedids(table).join(','));
            });
        },
        api: {
            bindevent: function () {
                $(document).on("change", "#c-type", function () {
                    $("#c-pid option[data-type='all']").prop("selected", true);
                    $("#c-pid option").removeClass("hide");
                    $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
                    $("#c-pid").selectpicker("refresh");
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});