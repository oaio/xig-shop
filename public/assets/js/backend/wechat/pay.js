define(['jquery', 'bootstrap', 'backend', 'table', 'form','bootstrap-datepicker'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'wechat/pay/index',
                    del_url: '',
                    detail_url: 'wechat/pay/detail',
                    multi_url: 'wechat/pay/multi'
                }
            });


            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 100,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('ID')},
                        {field: 'oid', title:'订单ID'},
                        {field: 'openid', title: __('用户'), align: 'left'},
                        {field: 'trade_no', title: '订单编号'},
                        {field: 'result_code', title: __('Flag'), align: 'left'},
                        {field: 'total_fee', title: '支付金额', formatter:function (v) {
                                return v/100;
                            }},
                        {field: 'create_time', title: '支付时间',formatter: Table.api.formatter.datetime},
                        {field: 'is_subscribe', title: __('Status'), searchList: {"Y": "关注公众号", "N":'未关注'},formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table,
                            events: Table.api.events.operate,
                            buttons: [{
                                name: 'detail',
                                text: __('Detail'),
                                icon: 'fa fa-list',
                                classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                                url: 'wechat/pay/detail'
                            }],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            };
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);
            Form.api.bindevent($("form[role=form]"));

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var typeStr = $(this).attr("href").replace('#','');
                if(typeStr == 'config'){
                    $.getJSON('/admin/wechat/pay/config',function (s) {
                        $.each(s,function (i,v) {$('#'+i).val(v)});
                    });
                }
                //table.bootstrapTable('refresh', {});
                return false;

            });

            //必须默认触发shown.bs.tab事件
            // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");

        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                browser: function (value, row, index) {
                    return '<a class="btn btn-xs btn-browser">' + row.useragent.split(" ")[0] + '</a>';
                },
            },
        }
    };
    /*(function($){
        $.fn.datepicker.dates['zh-CN'] = {
            days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
            daysMin: ["日", "一", "二", "三", "四", "五", "六"],
            months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            today: "今天",
            monthsTitle: "选择月份",
            clear: "清除",
            format: "yyyy-mm-dd",
            titleFormat: "yyyy年mm月",
            weekStart: 1
        };
        $("<link>").attr({ rel: "stylesheet", type: "text/css",
            href: "/assets/css/bootstrap-datepicker.css"}).appendTo("head");
        $(".datepicker").datepicker({
            language: "zh-CN",
            autoclose: true,//选中之后自动隐藏日期选择框
            clearBtn: true,//清除按钮
            todayBtn: true,//今日按钮
            format: "yyyy-mm-dd"
        });
    }(jQuery));*/

    return Controller;
});