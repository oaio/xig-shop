define(['jquery','backend','table'], function ($,Backend,Table) {
    Table.api.formatter = $.extend(Table.api.formatter, {
        image : function (value) {
            return '<a href="' + value + '" target="_blank" class="popup">' +
                '<img class="img-center" onerror="$1()" src="' + value + '"  style="max-height:90px;max-width:120px" /></a>';
        }
    });
    $("img").on("error",".img-center", window.$1 = function () {
        var img = event.srcElement;
        img.src="/assets/img/default.png";
        img.onerror=null;
    });
    $('body').on('click', 'a.popup', function () {
        var imgWidth, img = new Image(), max = $(window).width() - 50;
        img.src = this.getAttribute('href') || this.src;
        imgWidth = img.width > 9 ? img.width < max ? img.width + 'px' : max : '480px';
        img.onload = function () {
            var $content = $(img).appendTo('body').css({background: '#fff', width: imgWidth, height: 'auto'});
            Layer.open({
                type: 1, area: imgWidth, title: false, closeBtn: 1,
                skin: 'layui-layer-nobg', shadeClose: true, content: $content,
                end: function () {
                    $(img).remove();
                }
            });
        };
        img.onerror = window.$1;
        return false;
    });
});