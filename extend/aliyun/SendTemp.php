<?php

namespace aliyun;
use aliyun\SignatureHelper;

/**
 * 短信发送模板定义
 */
class SendTemp{
	
	protected static $SignatureHelper;

	protected static $accessKeyId ='LTAI5bhlLYgvGeAn';

	protected static $accessKeySecret ='SH7hS07IQp2yf8oYVnECbfuVIxXpSM';

	protected static $SignName='小哥帮';

	protected static $PhoneNumbers='';

	protected static $TemplateCode='';

	protected static $TemplateParam=[];

	protected static $security = false;//是否启用https


	/********************通用析构******************************/
	public function __construct($config=array())
	{

		if(!empty($config)){

			$accessKeyId=self::$accessKeyId;

			$accessKeySecret=self::$accessKeySecret;

			$SignName=self::$SignName;

			$smsTemp=[
				'accessKeyId'    =>$accessKeyId,
				'accessKeySecret'=>$accessKeySecret,
				'SignName'=>$SignName,
			];
			
			$smsTemp=array_merge($smsTemp,$config);

			self::setaccessKeyId($smsTemp['accessKeyId']);

			self::setaccessKeySecret($smsTemp['accessKeySecret']);

			self::setSignName($smsTemp['SignName']);
		}

		self::setSignatureHelper(new SignatureHelper()) ;

	}
	public static function setaccessKeyId($accessKeyId){
		self::$accessKeyId=$accessKeyId;
	}
	public static function setaccessKeySecret($accessKeySecret){
		self::$accessKeySecret=$accessKeySecret;
	}
	public static function setSignName($SignName){
		self::$SignName=$SignName;
	}
	public static function setSignatureHelper($SignatureHelper){
		self::$SignatureHelper=$SignatureHelper;
	}
	public static function setTemplateCode($TemplateCode){
		self::$TemplateCode=$TemplateCode;
	}
	public static function setTemplateParam($TemplateParam){
		self::$TemplateParam=$TemplateParam;
	}
	public static function setPhoneNumbers($PhoneNumbers){
		self::$PhoneNumbers=$PhoneNumbers;
	}
	public static function setsecurity($security){
		self::$security=$security;
	}
	/*******************通用析构*******************************/

	/**
	 * 获取类基础数据
	 * @Author   ksea
	 * @DateTime 2019-06-18T11:38:21+0800
	 * @return   [type]                   [description]
	 */
	public static function getData(){
		$data['SignatureHelper']=self::$SignatureHelper;
		$data['accessKeyId']=self::$accessKeyId;
		$data['accessKeySecret']=self::$accessKeySecret;
		$data['SignName']=self::$SignName;
		$data['PhoneNumbers']=self::$PhoneNumbers;
		$data['TemplateCode']=self::$TemplateCode;
		$data['TemplateParam']=self::$TemplateParam;
		return $data;
	}


	/**
	 * 自定义选择模板发送
	 * @Author   ksea
	 * @DateTime 2019-06-18T13:02:27+0800
	 * @param    [type]                   $PhoneNumbers  [手机号码]
	 * @param    [type]                   $TemplateCode  [模板编号]
	 * @param    [type]                   $TemplateParam [模板参数]
	 */
	public static function SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam){

		$accessKeyId=self::$accessKeyId;
		$accessKeySecret=self::$accessKeySecret;
		$security=self::$security;
		self::setTemplateParam($TemplateParam);
		self::setTemplateCode($TemplateCode);
		self::setPhoneNumbers($PhoneNumbers);

		$params["TemplateCode"]=self::$TemplateCode;
		$params["PhoneNumbers"]=self::$PhoneNumbers;
		$params["TemplateParam"]=self::$TemplateParam;
		$params["SignName"]=self::$SignName;
	    
	    if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
	        $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
	    }

	    $helper =self::$SignatureHelper;
	    $content = $helper->request(
	        $accessKeyId,
	        $accessKeySecret,
	        "dysmsapi.aliyuncs.com",
	        array_merge($params, array(
	            "RegionId" => "cn-hangzhou",
	            "Action" => "SendSms",
	            "Version" => "2017-05-25",
	        )),
	        $security
	    );
	    return $content;

	}
	/**
	 * 提醒师傅服务完成
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [数组者递归]
	 * @param    [type]                   $var1         [description]
	 */
	public static function SendEnd($PhoneNumbers,$key=0,$count=0){

		$TemplateCode='SMS_17135507';
		$TemplateParam=[
			'name'=>' '
		];
		if(!is_array($PhoneNumbers)){
			return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
		}
		else{
			if($count==0){
				$count=count($PhoneNumbers);
			}
			$sendNum=$PhoneNumbers[$key];
			self::SendDiy($sendNum,$TemplateCode,$TemplateParam);
			if($key+1==$count){
				return $key;
			}else{
				$key++;
				return self::SendEnd($PhoneNumbers,$key,$count);
			}
		}

	}

	/**
	 * 验证码
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 */
	public static function SendCode($PhoneNumbers,$var1){
		$TemplateCode='SMS_168311039';
		$TemplateParam=[
			'code'=>$var1,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 评价（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:43:11+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @return   [type]                                 [description]
	 */
	public static function assess($PhoneNumbers,$var1){

		$TemplateCode='SMS_168310644';
		$TemplateParam=[
			'name'=>$var1
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 订单派送通知（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:43:11+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [订单编号]
	 * @return   [type]                                 [description]
	 */
	public static function order($PhoneNumbers,$var1){
		
		$TemplateCode='SMS_167530617';
		$TemplateParam=[
			'order_no'=>$var1
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 联系失败（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T16:12:49+0800
	 * @param    [type]                   $PhoneNumbers [手机号码]
	 * @param    [type]                   $var1         [订单编号]
	 * @param    [type]                   $var2         [重新改约时间]
	 */
	public static function ServerlinkErr($PhoneNumbers,$var1,$var2){
		
		$TemplateCode='SMS_168306201';
		$TemplateParam=[
			'order_no'=>$var1,
			'time'	  =>$var2,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 联系失败（用户收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T16:14:49+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [订单号]
	 * @param    [type]                   $var2         [服务人员]
	 * @param    [type]                   $var3         [当前时间]
	 * @param    [type]                   $var4         [预约预约修改时间]
	 */
	public static function UserlinkErr($PhoneNumbers,$var1,$var2,$var3,$var4){
		
		$TemplateCode='SMS_168585783';
		$TemplateParam=[
			'order_no'=>$var1,
			'name'	  =>$var2,
			'time1'	  =>$var3,
			'time2'	  =>$var4,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);

	}
	/**
	 * 改约（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:56:59+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 */
	public static function ServerChange($PhoneNumbers,$var1,$var2){
		$TemplateCode='SMS_168311083';
		$TemplateParam=[
			'order_no'=>$var1,
			'time'	  =>$var2,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 改约（用户）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:56:45+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 * @param    [type]                   $var3         [description]
	 * @param    [type]                   $var4         [description]
	 */
	public static function UserChange($PhoneNumbers,$var1,$var2,$var3,$var4){
		$TemplateCode='SMS_168586453';
		$TemplateParam=[
			'order_no'=>$var1,
			'name'	  =>$var2,
			'time1'	  =>$var3,
			'time2'	  =>$var4,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 抢单（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 * @param    [type]                   $var3         [description]
	 * @param    [type]                   $var4         [description]
	 */
	public static function ServerGrab($PhoneNumbers,$var1){
		$TemplateCode='SMS_168306344';
		$TemplateParam=[
			'order_no'=>$var1,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 抢单（用户收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 * @param    [type]                   $var3         [description]
	 * @param    [type]                   $var4         [description]
	 */
	public static function UserGrab($PhoneNumbers,$var1){
		$TemplateCode='SMS_168311099';
		$TemplateParam=[
			'order_no'=>$var1,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 确定上门（服务人员收到）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 * @param    [type]                   $var3         [description]
	 * @param    [type]                   $var4         [description]
	 */
	public static function ServerSureGo($PhoneNumbers,$var1,$var2){
		$TemplateCode='SMS_168311097';
		$TemplateParam=[
			'order_no'=>$var1,
			'time'	  =>$var2,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 确定上门（用户）
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 * @param    [type]                   $var2         [description]
	 * @param    [type]                   $var3         [description]
	 * @param    [type]                   $var4         [description]
	 */
	public static function UserSureGo($PhoneNumbers,$var1,$var2,$var3,$var4){
		$TemplateCode='SMS_168306192';
		$TemplateParam=[
			'order_no'=>$var1,
			'name'	  =>$var2,
			'time1'	  =>$var3,
			'time2'	  =>$var4,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}

	/**
	 * 审核结果
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 */
	public static function AuditResultsSuccess($PhoneNumbers,$msg='！'){
		$TemplateCode='SMS_171858793';
		$TemplateParam=[
			'results'=>$msg,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
	/**
	 * 审核结果
	 * @Author   ksea
	 * @DateTime 2019-06-18T15:59:05+0800
	 * @param    [type]                   $PhoneNumbers [description]
	 * @param    [type]                   $var1         [description]
	 */
	public static function AuditResultsError($PhoneNumbers,$msg){
		$TemplateCode='SMS_171853785';
		$TemplateParam=[
			'results'=>$msg,
		];
		return self::SendDiy($PhoneNumbers,$TemplateCode,$TemplateParam);
	}
}