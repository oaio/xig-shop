<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace wechat;

use think\App;
use think\Config;
/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/8  8:33
 * Desc：微信支付
 */
class Wepay
{
	/**
	 * @var array $config 配置数组
	 */
	private $config;
	/**
	 * @var string $mch_id 绑定商户号（开户邮件中可查看）
	 */
	private $mch_id;
	/**
	 * @var string $appid 绑定公众账号ID
	 */
	private $appid;
	/**
	 * @var string $openid 支付的用户标识
	 */
	private $openid;
	/**
	 * @var string $secret 公众帐号AppSecret
	 */
	private $secret;
	/**
	 * /**
	 * @var string $notify_url 支付成功回调地址
	 */
	private $notify_url;
	private $unified = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
	private $debug = 0;
	/**
	 * @var Wepay  保存单实例的对象
	 */
	private static $instance = null;

	/**
	 *  初始化配置参数
	 *
	 * @param array $config 如：[mch_id'=>'','appid'=>'','secret'=>'','notify_url'=>'']
	 */
	public function __construct($config = array())
	{
		$file = isset($config['log']) ? $config['log'] :
			(__DIR__.'/../../runtime/log/pay' . date('Y-m-d') . '.log');
//			(__DIR__.'/../../public/pay' . date('d') . '.txt');
		Log::init($file);
		$this->config = $config;
		foreach ($config as $key => $value) {
			$this->$key = $value;
		}
		$this->debug = App::$debug;
	}

	/**
	 *  获取并初始化对象
	 *
	 * @return Wepay|static
	 */
	public static function getInstance()
	{
		if(!self::$instance instanceof static) {
			self::$instance = new static(Config::get('pay'));
		}
		return self::$instance;
	}

	/**
	 *  设置用户标识openid
	 *
	 * @param $openid string
	 * @return $this
	 */
	public function setId($openid)
	{
		$this->openid = $openid;
		return $this;
	}

	/**
	 *  设置App id
	 *
	 * @param $id
	 * @return $this
	 */
	public function setApp($id)
	{
		$this->appid = $id;
		return $this;
	}

	/**
	 *  设置appSecret
	 *
	 * @param $key
	 * @return $this
	 */
	public function setKey($key)
	{
		$this->secret = $key;
		return $this;
	}

	/**
	 *  设置回调地址notify_url
	 *
	 * @param $url string
	 * @return $this
	 */
	public function setNotify($url)
	{
		$this->notify_url = $url;
		return $this;
	}

	/**
	 * @param bool|int $debug
	 * @return $this
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
		return $this;
	}

	/**
	 * 微信支付回调通知
	 *
	 * @return array
	 */
	public function notify()
	{
		$str = file_get_contents('php://input');
		$this->debug && Log::debug('【notify start】'. $str);
		$obj = simplexml_load_string($str, 'SimpleXMLElement', LIBXML_NOCDATA);
		/*$return = array(
			'out_trade_no' => sprintf('%s', $obj->out_trade_no),
			'total_fee' => sprintf('%s', $obj->total_fee),
			'openid' => sprintf('%s', $obj->openid),
			'return_code' => sprintf('%s', $obj->return_code),
			'return_msg' => sprintf('%s', $obj->return_msg),
			'transaction_id' => sprintf('%s', $obj->transaction_id),
			'is_subscribe' => sprintf('%s', $obj->is_subscribe),
			'attach' => sprintf('%s', $obj->attach),
			'time_end' => sprintf('%s', $obj->time_end),
		);*/
		$return = (array)$obj;
		$sign = !isset($return['sign']) ?: $return['sign'];
		unset($return['sign']);
		if ($this->sign($return, $this->secret) === $sign) {
			isset($return['time_end']) && $return['time_end']=strtotime($return['time_end']);
			$this->debug && Log::info('notify success sign : '.$return['out_trade_no']);
			return $return;
		}else{
			Log::warn($return);//notify invalid
			return array('return_code'=>'FAIL','return_msg'=>'sign invalid');
		}
	}

	/**
	 *  发起微信支付
	 *
	 * 付款时调用，返回结果数组
	 * @param $orderName string 订单名称或标题
	 * @param $tradeNo string 订单编号
	 * @param $totalFee int numeric 订单总价格
	 * @param int $time
	 * @param string $type
	 * @return array
	 * @throws \Exception
	 */
	public function createPay($orderName, $tradeNo, $totalFee, $time=0,$type='1'){
		if(empty($time)){
			$time = $_SERVER['REQUEST_TIME'];
		}
		// notify_url 中包含问号时，有可能会导致回调时签名验证不成功
		if (strpos($this->notify_url, '?') !== false) {
			Log::warn('notify_url中包含问号:' . $this->notify_url);
			throw new \Exception('notify_url error: cannot contain "?"');
		}
		$unified = array(
			'appid' => $this->appid,
			'attach' => $type,                          //商家数据包，原样返回
			'body' => $orderName,
			'mch_id' => $this->mch_id,
			'nonce_str' => $this->nonce(),
			'notify_url' => $this->notify_url,
			'openid' => $this->openid,                        //trade_type=JSAPI，此参数必传
			'out_trade_no' => $tradeNo,
			'spbill_create_ip' => '127.0.0.1',
			'total_fee' => sprintf('%.0f', $this->calc($totalFee, 100, '*', 2)),//单位 转为分
			'trade_type' => 'JSAPI',
		);
//        Log::warn('调起支付:' . json_encode($unified));
		$unified['sign'] = $this->sign($unified, $this->secret);
		$xml = $this->curl('https://api.mch.weixin.qq.com/pay/unifiedorder',
			$this->array2xml(['xml'=>$unified]),'POST', !0,array(CURLOPT_TIMEOUT=>30));
		$obj = @simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
		if(empty($obj)){
			Log::warn('parse xml error: ' . $xml);
			throw new \Exception('Parse xml error');
		}else{
			if ('SUCCESS' == $obj->return_code){
				return $this->verify((array)$obj, $time);
			}else {
				Log::warn('Create_err' . $xml);
				exit($xml);
				//throw new \Exception('Payment transactions error');
			}
		}
	}

	public function verify($ary, $time)
	{
		if('SUCCESS' != $ary['return_code']){
			Log::warn($ary);
			return $ary;
		}
		if('SUCCESS' != $ary['result_code']){
			Log::warn($ary);
			return $ary;
		}
		$sign = isset($ary['sign']) ? $ary['sign'] : '';
		unset($ary['sign']);
		if($sign == $this->sign($ary)){
			$code = array(
				'appId' => $this->appid,
				'timeStamp' => $time,
				'nonceStr' => isset($ary['nonce_str']) ? $ary['nonce_str'] : '',
				'package' => 'prepay_id=' . (isset($ary['prepay_id'])?$ary['prepay_id'] :''),
				'signType' => 'MD5'
			);
			$code['paySign'] = $this->sign($code);
			$this->debug && Log::debug($code);
			return $code;
		}else{
			Log::warn('Invalid pay sign:  ');
			Log::warn($ary);
			return [];
		}
	}

	/**
	 *  预支付数据接口
	 *
	 * @access public
	 * @param $params
	 * @return array|bool
	 */
	public function prepay($params)
	{
		$time = $_SERVER['REQUEST_TIME'];
		$default = array(
			'appid' => $this->appid,
			'attach' => isset($params['type']) ? $params['type'] : 1,
			'body' => 'test',
			'mch_id' => $this->mch_id,
			'nonce_str' => $this->nonce(),
			'notify_url' => $this->notify_url,
			'openid' => $this->openid,
			'out_trade_no' => $time,
			'spbill_create_ip' => '120.78.153.24',
			'total_fee' => 1,
			'trade_type' => 'NATIVE',
		);
		if(isset($params['key'])){
			$this->secret = $params['key'];
		}
		$params = array_intersect_key(array_merge($default, $params), $default);
		$params['sign'] = $this->sign($params);
		try {
			$res = $this->curl($this->unified, $this->array2xml(['xml'=>$params]), 'POST', !0);
			if($res){
				return $this->verify($this->xml2array($res), $time);
			}else{
				Log::warn('unable to connect api.mch.weixin.qq.com');
			}
		} catch (\Exception $e) {
			Log::warn('prepay_err: ' . $e->getMessage());
			Log::warn($params);
		}
		return [];
	}

	/**
	 *  随机字符签名
	 *
	 * @param int $length
	 * @return string
	 */
	private function nonce($length = 16)
	{
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012356789';
		$str = '';
		$len = strlen($chars) - 1;
		for ($i = $length; $i > 0; $i--) {
			$str .= substr($chars, mt_rand(0, $len ), 1);
		}
		return $str;
	}

	/**
	 *  拼接支付密钥
	 *
	 * @param $params array  连接参数
	 * @param $key string 密钥
	 * @return string
	 */
	private function sign($params, $key='')
	{
		$key = $key ? $key : $this->secret;
		ksort($params, SORT_STRING);
		$un = $this->splice($params, false);
		$sign = strtoupper(md5($un . "&key=" . $key));
		return $sign;
	}

	/**
	 *  拼接链接参数
	 *
	 * @param $array
	 * @param bool $encode
	 * @return bool|string
	 */
	private function splice($array, $encode = false)
	{
		$buff = "";
		ksort($array);
		foreach ($array as $k => $v) {
			if (null != $v && "null" != $v) {
				if ($encode) {
					$v = urlencode($v);
				}
				$buff .= $k . "=" . $v . "&";
			}
		}
		$request = '';
		if (strlen($buff) > 0) {
			$request = substr($buff, 0, strlen($buff) - 1);
		}
		return $request;
	}

	/**
	 *  模拟http请求
	 *
	 * @param  string $url 可访问的url地址
	 * @param string|array $params 请求参数
	 * @param string|null $method 请求类型
	 * @param bool |null $multi 使用post时设置为真
	 * @param array $options
	 * @param array|null $header
	 * @return mixed
	 * @throws \Exception
	 */
	public function curl($url, $params, $method = 'GET', $multi = !1, $options = array(), $header = array())
	{
		$opts = array(
			CURLOPT_TIMEOUT => 30,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_HTTPHEADER => $header
		);

		/* 根据请求类型设置特定参数 */
		switch (strtoupper($method)) {
			case 'GET':
				$opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
				break;
			case 'POST':
				//判断是否传输文件
				$params = $multi ? $params : http_build_query($params);
				$opts[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = $params;
				break;
			default:
				throw new \Exception('不支持的请求方式！');
		}

		/* 初始化并执行curl请求 */
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		if (!empty($options)) {
			curl_setopt_array($ch, $options);
		}
		$data = curl_exec($ch);
		$error = curl_error($ch);
		curl_close($ch);
		if ($error) throw new \Exception('请求发生错误：' . $error);
		return $data;
	}

	/**
	 * xml 转数组
	 *
	 * @access public
	 * @param $xml
	 * @return mixed
	 */
	public function xml2array($xml)
	{
		libxml_disable_entity_loader(!0);
		$string = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
		return json_decode(json_encode($string), !0);
	}

	/**
	 * 数组转 xml
	 *
	 * @param array $array 目标数组
	 * @param string|null $header 定义起始标签
	 * @param bool $cdata
	 * @return string
	 */
	public function array2xml($array, $cdata = !1, $header = '<?xml version="1.0" encoding="utf-8"?>')
	{
		if (array_key_exists('xml', $array)) {
			$header = $this->_array2xml($array, $cdata);
		} else {
			$header .= $this->_array2xml($array, $cdata);
		}
		return $header;
	}

	/**
	 * @param array $array
	 * @param bool $cdata
	 * @return string
	 */
	private function _array2xml($array, $cdata = !1)
	{
		$xml = '';
		foreach ($array as $key => $val) {
			if (is_numeric($key)) {
				$key = "item id=\"$key\"";
			} else {
				//去掉空格，只取空格之前文字为key
				list($key,) = explode(' ', $key);
			}
			$xml .= "<$key>";
			if ($cdata && $key!='xml') {
				$xml .= '<![CDATA[';
			}
			$xml .= is_array($val) ? "\n" . $this->_array2xml($val, $cdata) : $val;
			//去掉空格，只取空格之前文字为key
			list($key,) = explode(' ', $key);
			$xml .= $cdata && $key!='xml'  ? "]]></$key>\n" : "</$key>\n";
		}
		return $xml;
	}
	/**
	 * 精度计算
	 *
	 * 如果没有启用bcmath扩展: 比较大小时转为整数比较、计算时使用number_format格式化返回值
	 * @param string $a 数字
	 * @param string $b 数字
	 * @param string $operator 操作符 支持: "+"、 "-"、 "*"、 "/"、 "comp"
	 * @param int $scale 小数精度位数，默认2位
	 * @return string|int 加减乖除运算，返回string。比较大小时，返回int(相等返回0, $a大于$b返回1, 否则返回-1)
	 * @throws \Exception
	 */
	private function calc($a, $b, $operator, $scale = 2)
	{
		$scale = (int)$scale;
		$scale = $scale < 0 ? 0 : $scale;

		$bc = array(
			'+' => 'bcadd',
			'-' => 'bcsub',
			'*' => 'bcmul',
			'/' => 'bcdiv',
			'comp' => 'bccomp',
		);

		if (!array_key_exists($operator, $bc)) {
			throw new \Exception('Operator error');
		}

		if (function_exists($bc[$operator])) {
			$fun = $bc[$operator];
			return $fun($a, $b, $scale);
		}

		switch ($operator) {
			case '+':
				$c = ($a - 0) +  ($b - 0) ;
				break;
			case '-':
				$c = $a - $b;
				break;
			case '*':
				$c = $a * $b;
				break;
			case '/':
				$c = $a / $b;
				break;
			case 'comp':

				// 按指定精度，去掉小数点，放大为整数字符串
				$a = ltrim(number_format((float)$a, $scale, '', ''), '0');
				$b = ltrim(number_format((float)$b, $scale, '', ''), '0');
				$a = $a === '' ? '0' : $a;
				$b = $b === '' ? '0' : $b;

				if ($a === $b) {
					return 0;
				}

				return $a > $b ? 1 : -1;

			default:
				throw new \Exception('Operator error');
		}

		$c = number_format($c, $scale, '.', '');

		return $c;
	}
}