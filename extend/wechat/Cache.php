<?php
namespace wechat;

/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/16  16:48
 * Desc：
 */
class Cache
{
	/**
	 * @var \Redis  保存操作句柄
	 */
	protected $handler;
	private $redis = 1;
		/**
	 * @var Cache  保存单实例的对象
	 */
	private static $instance = null;

	private function __set_handler()
	{
		try {
			self::$instance->handler = new \Redis();
			self::$instance->handler->popen('127.0.0.1', 6379, 1);
		}catch (\Exception $e){
			self::$instance->handler = \think\Cache::init();
			self::$instance->redis = 0;
		}
	}

	/**
	 *  获取并初始化缓存对象
	 *
	 * @return Cache|static
	 */
	public static function getInstance(){
		if(!self::$instance instanceof static) {
			self::$instance = new static();
			self::$instance->__set_handler();
		}
		return self::$instance;
	}

	/**
	 *  获取缓存数据
	 *
	 * @param string|array $key
	 * @return mixed
	 */
	public static function get($key)
	{
		return self::$instance->handler->get($key);
	}

	/**
	 *  设置缓存数据
	 *
	 * @param string $key 键名
	 * @param mixed $value 数值
	 * @param int $expire 过期时间
	 * @return int
	 */
	public static function set($key, $value, $expire=3600){
		return self::$instance->handler->set($key, $value, $expire);//$_SERVER['REQUEST_TIME']+
		//return self::$instance->handler->ttl($key);
	}

	/**
	 * 返回给定key的剩余生存时间
	 *
	 * @param $key
	 * @return int
	 */
	public static function ttl($key){
		return self::$instance->handler->ttl($key);
	}

	/**
	 *  删除单个数据
	 *
	 * @param string|array $key
	 * @return int
	 */
	public static function del($key){
		return self::$instance->handler->del($key);
	}

	/**
	 *  清空所有缓存
	 *
	 * @return bool
	 */
	public static function clear()
	{
		return self::$instance->handler->flushAll();
	}

	public function __destruct()
	{
		self::$instance->redis && self::$instance->handler->close();
	}
}