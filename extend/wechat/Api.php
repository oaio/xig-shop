<?php
namespace wechat;

use think\Config;
/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/16  15:29
 * Desc：公众平台接口基类
 */
class Api
{
    // 微信API域名
    const API_DOMAIN = 'https://api.weixin.qq.com/';
    /* 二维码类型常量 */
    const QR_SCENE       = 'QR_SCENE';
    const QR_LIMIT_SCENE = 'QR_LIMIT_SCENE';
    /**
     * 字符串二维码
     */
    const QR_LIMIT_STR_SCENE='QR_LIMIT_STR_SCENE';
    const QR_STR_SCENE       = 'QR_STR_SCENE';



	/**
	 * @var mixed  自定义公众号消息回复函数
	 */
	public static $reply_func;
	/**
	 * @var string  调用凭证access_token
	 */
	protected $access_token;
	/**
	 * @var string 开发者中心-配置项-AppID(公众平台ID)
	 */
	protected $appid;
	/**
	 * @var string 开发者中心-配置项-服务器配置-Token(令牌)
	 */
	protected $token;
	/**
	 * @var string  公众平台AppSecret
	 */
	protected $secret;
	/**
	 * @var string  开发者中心-配置项-服务器配置-EncodingAESKey(消息加解密密钥)
	 */
	protected $key;
	/**
	 * @var Api  保存单实例的对象
	 */
	private static $instance = null;
	private $host = 'https://api.weixin.qq.com/';
    /**
     * 微信api根路径
     * @var string
     */
    private $apiURL = 'https://api.weixin.qq.com/cgi-bin';

    /**
     * 微信二维码根路径
     * @var string
     */
    private $qrcodeURL = 'https://mp.weixin.qq.com/cgi-bin';

    private $requestCodeURL = 'https://open.weixin.qq.com/connect/oauth2/authorize';

    private $oauthApiURL = 'https://api.weixin.qq.com/sns';
	/**
	 * 初始化微信配置、调用凭证，绑定应用标识
	 */
	public function __construct()
	{
		Log::init('../runtime/log/a.txt');
		$conf = Config::get('wechat');
		if (!empty($conf)) {
			$this->appid = $conf['appid'];
			$this->secret = $conf['secret'];
			$this->token = $conf['token'];
			$this->key = $conf['key'];
		}
		Cache::getInstance();
		$this->get_access_token();
	}

	/**
	 *  初始化单实例平台接口
	 *
	 * @return Api|static
	 */
	public static function getInstance(){
		if(!self::$instance instanceof static) {
			self::$instance = new static();
		}
		return self::$instance;
	}

	/**
	 * 获取access_token
	 *
	 * @return string
	 */
	public function get_access_token()
	{
		$this->access_token = Cache::get('access_token');
		if (empty($this->access_token)) {
			$ary = array(
				'grant_type' => 'client_credential',
				'appid' => $this->appid,
				'secret' => $this->secret
			);//sns
			$res = Helper::http_curl($this->host . 'cgi-bin/token?' . http_build_query($ary));
			if (isset($res['access_token'])) {
				$this->access_token = $res['access_token'];
				Cache::set('access_token', $this->access_token, 3600);
			} else {
				Log::warn('Token：' . $res['errmsg']);
			}
		}
		return $this->access_token;
	}

	/**
	 * 验证和Listen监听接口
	 *
	 * 用法：
	 * ```
	 *  Api::getInstance()->receive();
	 *
	 */
	public function receive()
	{
		if (input('echostr') && input('signature') && input('nonce')) {
			$this->valid();
		} else {
			$this->__listen();
		}
	}

	/**
	 * 验证接入请求
	 */
	protected function valid()
	{
		$echostr = input('echostr');
		if ($this->__check_signature()) {
			echo $echostr;
			exit;
		}
	}

	/**
	 * 验证请求参数的签名
	 *
	 * @return bool
	 */
	private function __check_signature()
	{
		$signature = input('signature');
		$timestamp = input('timestamp');
		$nonce = input('nonce');
		$tmp_arr = array($this->token, $timestamp, $nonce);
		sort($tmp_arr, SORT_STRING);
		$tmp_str = implode($tmp_arr);
		$tmp_str = sha1($tmp_str);
		return $tmp_str == $signature ? true : !1;
	}

	/**
	 *  响应微信发送的信息（自动回复）
	 */
	private function __listen()
	{
		$timestamp = input('timestamp');
		$nonce = input('nonce');
		$msg_signature = input('msg_signature');
		$encrypt_type = (input('encrypt_type') == 'aes') ? "aes" : "raw";

		$post_str = file_get_contents("php://input");
		//Log::info($post_str);
		if (!empty($post_str)) {
			if ($encrypt_type == 'aes') {
				Encoder::set($this->token, $this->key, $this->appid);
				$msg = "";
				Encoder::decryptMsg($msg_signature, $timestamp, $nonce, $post_str, $msg);
				$post_str = $msg;
			}

			$obj = simplexml_load_string($post_str, 'SimpleXMLElement', LIBXML_NOCDATA);
			$ary = array('event', 'image', 'link', 'location', 'video', 'voice', 'text');
			$function = trim($obj->MsgType);
			if (!in_array($function, $ary)) {
				Log::warn('receive msg type: ' . $function);
				exit('unknown msg type: ' . $function);
			}
			echo call_user_func(array(Receive::getInstance(), $function), $obj);
			exit();
		} else {
			exit();
		}
	}

	/**
	 * 发送客服消息
	 *
	 * Examples:
	 * ```
	 * $api = Api::getInstance();
	 * $api->send(
	 *     $msg->FromUserName,
	 *     'array(
	 *     'content' => '这是一个客服主动发送的一条消息!'',
	 *     'kf_account' => 'test1@kftest'
	 * ));
	 * ```
	 *
	 * @param string $openid
	 * @param array $msg
	 * @return mixed
	 */
	public function send($openid, $msg = array())
	{
		if(gettype($openid) == 'string' && isset($msg['content'])) {
			$data = array(
				'touser' => $openid,
				'msgtype' => 'text',
				'text' => ['content' => $msg['content']],
			);
			isset($msg['kf_account']) && $data['customservice']=['kf_account'=>$msg['kf_account']];
		}else{
			$data = $openid;
		}
		return $this->http_post('cgi-bin/message/custom/send?access_token=', $data );
	}

	/**
	 *  发送模板消息
	 *
	 * @param mixed $openid 接收者openid
	 * @param string $template_id 模板ID
	 * @param array $data 模板数据 ，一维数组自动转化
	 * @param string $link 模板跳转链接
	 * @return mixed
	 */
	public function send_template($openid, $template_id='', $data=[], $link='')
	{
		if(!is_array($openid)){
			if (!is_array(current($data))) {
				foreach ($data as $key => $value) {
					$data[$key] = array('value' => urlencode($value), 'color' => '#1631FF');
				}
			}
			$openid = array(
				'touser' => $openid,
				'template_id' => $template_id,
				'url' => $link,
				'data' => $data
			);
		}
		return $this->http_post('cgi-bin/message/template/send?access_token=', $openid );
	}

	/**
	 *  删除模板
	 *
	 * @param $template_id
	 * @return mixed
	 */
	public function del_template($template_id)
	{
		$data = sprintf('{"template_id":"%s"}', $template_id);
		return $this->http_post('cgi-bin/template/del_private_template?access_token=', $data);
	}

	/**
	 *  获取模板ID
	 *
	 * @param string $id
	 * @return mixed
	 */
	public function get_template($id)
	{
		return $this->http_post('cgi-bin/template/api_add_template?access_token=',
			"{\"template_id_short\":\"{$id}\"}");
	}

	/**
	 *  获取模板列表
	 *
	 * @return mixed
	 */
	public function get_template_list()
	{
		return $this->http('cgi-bin/template/get_all_private_template?access_token=');
	}

	/**
	 *  获取当前设置的行业信息
	 *
	 * @return mixed
	 */
	public function get_industry()
	{
		return $this->http('cgi-bin/template/get_industry?access_token=');
	}

	/**
	 *  设置所属行业
	 *
	 * @param int $primary 主营行业编号
	 * @param int $secondary 副行业编号
	 * @return mixed
	 */
	public function set_industry($primary, $secondary)
	{
		$data = sprintf('{"industry_id1":"%s","industry_id2":"%s"}', $primary, $secondary);
		return $this->http_post( 'cgi-bin/template/api_set_industry?access_token=', $data);
	}

	/**
	 *  向微信API发送http请求，url后添加access_token
	 *
	 * @param string $url 请求地址
	 * @return mixed
	 */
	protected function http($url)
	{
		$res = Helper::http_curl($this->host . $url . $this->access_token);
		if (isset($res['errcode']) && in_array($res['errcode'], ['40001','42001'])) {
			Cache::del('access_token');
			$this->get_access_token();
			$res = Helper::http_curl($url . $this->access_token);
		}
		return $res;
	}

	/**
	 *  向微信API提交数据
	 *
	 * @param string $url 请求地址
	 * @param mixed $data POST数组
	 * @return mixed
	 */
	protected function http_post($url, $data = array())
	{
		$addr = $this->host . $url . $this->access_token;
		is_array($data) && $data = urldecode(json_encode($data));
		$res = Helper::http_curl($addr, 'post', $data);
		if (isset($res['errcode']) && in_array($res['errcode'], ['40001','41001','42001'])) {
			Cache::del('access_token');
			$token = $this->get_access_token();
			$res = Helper::http_curl($this->host . $url . $token, 'post', $data);
		}
		return $res;
	}

	/**
	 * 用户分组管理 - 批量移动用户分组
	 *
	 * Examples:
	 * ```
	 * $api->batchupdate_user_group(array(
	 *     'ocNtAt0YPGDme5tJBXyTphvrQIrc',
	 *     'ocNtAt_TirhYM6waGeNUbCfhtZoA',
	 *     'ocNtAt_K8nRlAdmNEo_R0WVg_rRw'
	 *     ), 100);
	 *
	 * @param  array $openid_arr
	 * @param int $to_groupid
	 *
	 * @return mixed
	 */
	public function batchupdate_user_group($openid_arr, $to_groupid)
	{
		$open_ids = json_encode($openid_arr);
		$data = sprintf('{"openid_list":%s,"to_groupid":"%s"}', $open_ids, $to_groupid);
		return $this->http_post('cgi-bin/groups/members/batchupdate?access_token=', $data);
	}

	/**
	 * 用户分组管理 - 移动用户分组
	 *
	 * Examples:
	 *
	 * $api->update_user_group('ocNtAt0YPGDme5tJBXyTphvrQIrc', 100);
	 *
	 * @param string $open_id
	 * @param  int $to_groupid
	 *
	 * @return mixed
	 */
	public function update_user_group($open_id, $to_groupid)
	{
		$data = sprintf('{"openid":"%s","to_groupid":"%s"}', $open_id, $to_groupid);
		return $this->http_post( 'cgi-bin/groups/members/update?access_token=', $data);
	}

	/**
	 *  用户分组管理 - 修改分组名
	 *
	 * Examples:
	 *
	 * $api->update_group(100, '自定义分组名字');
	 * @param int $group_id
	 * @param string $group_name
	 * @return mixed
	 */
	public function update_group($group_id, $group_name)
	{
		$data = sprintf('{"group":{"id":"%s","name":"%s"}}', $group_id, $group_name);
		return $this->http_post('cgi-bin/groups/update?access_token=',$data);
	}

	/**
	 * 用户分组管理 - 删除分组
	 *
	 * @param string $group_id
	 * @return mixed
	 */
	public function delete_group($group_id)
	{
		$data = sprintf('{"group":{"id":"%s"}}', $group_id);
		return $this->http_post('cgi-bin/groups/delete?access_token=',$data);
	}

	/**
	 * 用户分组管理 - 查询用户所在分组
	 *
	 * Examples:
	 *
	 * $api->get_user_group('ocNtAt0YPGDme5tJBXyTphvrQIrc');
	 * @param  string $openid 用户的openid
	 * @return mixed
	 */
	public function get_user_group($openid)
	{
		$data = sprintf('{"openid":"%s"}', $openid);
		return $this->http_post('cgi-bin/groups/getid?access_token=',$data);
	}

	/**
	 * 获取用户基本信息
	 *
	 * Examples:
	 * ```
	 * $api->get_user_info('o863w08JnZLvCGmp3AKTCyntoD9k', 'zh_TW');
	 * ```
	 * @param  string $openid 普通用户的标识，对当前公众号唯一
	 * @param string $lang [可选：返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语]
	 * @return mixed
	 */
	public function get_user_info($openid, $lang = '')
	{
		return $this->http('cgi-bin/user/info?openid='.$openid. $lang.'&access_token=');
	}

	/**
	 * 获取用户列表
	 *
	 * Examples:
	 * ```
	 * $api->get_user_list('o863w08JnZLvCGmp3AKTCyntoD9k');
	 * ```
	 * @param string $next_openid [可选：第一个拉取的OPENID，不填默认从头开始拉取]
	 * @return mixed
	 */
	public function get_user_list($next_openid = '')
	{
		return $this->http('cgi-bin/user/info?next_openid='.$next_openid.'&access_token=');
	}

	/**
	 * 设置用户备注名
	 *
	 * Examples:
	 * ```
	 * $api->set_user_remark('o863w08JnZLvCGmp3AKTCyntoD9k', 'nane');
	 * ```
	 * @param string $openid 用户标识
	 * @param string $remark 新的备注名，长度必须小于30字符
	 * @return mixed
	 */
	public function set_user_remark($openid, $remark)
	{
		$data = sprintf('{"openid":"%s","remark":"%s"}', $openid, $remark);
		return $this->http_post('cgi-bin/user/info/updateremark?access_token=',$data);
	}

	/**
	 * 批量为用户打标签
	 *
	 * Examples:
	 * ```
	 * $api->set_users_tags(['o863w08JnZLvCGmp3AKTCyntoD9k', 'ocYxcuBt0mRugKZ7tGAHPnUaOW7Y'], 10);
	 * ```
	 * @param array $openids 用户openid例表
	 * @param int $tagid
	 * @return mixed ["errcode":0,"errmsg":"ok"]
	 */
	public function set_users_tags($openids, $tagid)
	{
		$list = implode('","', $openids);
		$data = sprintf('{"openid_list" : ["%s"], "tagid" : "%s" }', $list, $tagid);
		return $this->http_post('cgi-bin/tags/members/batchtagging?access_token=',$data);
	}

	/**
	 * 批量为用户取消标签
	 *
	 * Examples:
	 * ```
	 * $api->del_users_tags(['o863w08JnZLvCGmp3AKTCyntoD9k', 'ocYxcuBt0mRugKZ7tGAHPnUaOW7Y'], 10);
	 * ```
	 * @param array $openids 用户openid例表
	 * @param int $tagid
	 * @return mixed ["errcode":0,"errmsg":"ok"]
	 */
	public function del_users_tags($openids, $tagid)
	{
		$list = implode('","', $openids);
		$data = sprintf('{"openid_list" : ["%s"], "tagid" : "%s" }', $list, $tagid);
		return $this->http_post('cgi-bin/tags/members/batchuntagging',$data);
	}

	/**
	 * 获取用户身上的标签列表
	 *
	 * Examples:
	 * ```
	 * $api->get_user_tags(['o863w08JnZLvCGmp3AKTCyntoD9k');
	 * ```
	 * @param string $openid 用户openid
	 * @return mixed ["tagid_list":[10,5]]
	 */
	public function get_user_tags($openid)
	{
		$data = sprintf('{"openid_list" : "%s" }', $openid);
		return $this->http_post('cgi-bin/tags/getidlist?access_token=',$data);
	}

	/**
	 * 创建用户标签
	 *
	 * @param string $name 标签名（30个字符以内）
	 * @return mixed   ["tag":[id":10,"name":"广东" ]]
	 */
	public function create_tags($name)
	{
		$data = sprintf('{"tag":{"name":"%s"}}', $name);
		return $this->http_post('cgi-bin/tags/create?access_token=',$data);
	}

	/**
	 *  获取公众号已创建的标签
	 *
	 * @return mixed  ["tag":["id":127,   "name":"广东",   "count":5 ][...]   ]
	 */
	public function get_all_tags()
	{
		return $this->http('cgi-bin/tags/get?access_token=');
	}

	/**
	 *  获取标签下粉丝列表
	 *
	 * @param $tagid
	 * @param $openid [选填，不填默认从头开始拉取]
	 * @return mixed
	 * [
	 *   "count":2,
	 *   "data":{
	 *   "openid":[ "id1","id2"...]
	 *    },
	 *   "next_openid":""
	 * ]
	 */
	public function get_tag_users($tagid, $openid='')
	{
		$data = sprintf('{"tagid":"%s","next_openid":"%s"}', $tagid, $openid);
		return $this->http_post('cgi-bin/user/tag/get?access_token=',$data);
	}

	/**
	 * 编辑设置用户标签
	 *
	 * Examples:
	 * ```
	 * $api->set_user_tag('10', 'nane');
	 * ```
	 * @param $id
	 * @param  string $name 标签名（30个字符以内）
	 * @return mixed
	 */
	public function set_user_tag($id, $name)
	{
		$data = sprintf('{"tags":{"id":"%s","name":"%s"}}', $id, $name);
		return $this->http_post('cgi-bin/tags/update?access_token=',$data);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function delete_tags($id)
	{
		$data = sprintf('{"tags":{"id":"%s"}}', $id);
		return $this->http_post('cgi-bin/user/info/updateremark?access_token=',$data);
	}

	/**
	 * 设置用户备注名
	 *
	 * Examples:
	 * ```
	 * $api->set_user_remark('o863w08JnZLvCGmp3AKTCyntoD9k', 'nane');
	 * ```
	 * @param string $openid 用户标识
	 * @param string $remark 新的备注名，长度必须小于30字符
	 * @return mixed
	 */
	public function set_user_remark2($openid, $remark)
	{
		$data = sprintf('{"openid":"%s","remark":"%s"}', $openid, $remark);
		return $this->http_post('cgi-bin/user/info/updateremark?access_token=',$data);
	}

	//自定义菜单

	/**
	 *  创建菜单
	 *
	 * @param $json
	 * @return mixed
	 */
	public function create_menu($json)
	{
		return $this->http_post('cgi-bin/menu/create?access_token=',$json);
	}

	/**
	 *  查询菜单
	 *
	 * @return mixed
	 */
	public function get_menu()
	{
		return $this->http('cgi-bin/menu/get?access_token=');
	}

	/**
	 *  删除菜单
	 *
	 * @return mixed {"errcode":0,"errmsg":"ok"}
	 */
	public function delete_menu()
	{
		return $this->http_post('cgi-bin/menu/delete?access_token=');
	}

	/**
	 *  获取自定义菜单配置接口
	 *
	 * @return mixed
	 */
	public function get_selfmenu()
	{
		return $this->http('cgi-bin/get_current_selfmenu_info?access_token=');
	}

	/**
	 * 创建个性化菜单
	 * @param $json
	 * @return mixed
	 */
	public function create_selfmenu($json)
	{
		return $this->http_post('cgi-bin/menu/addconditional?access_token=',$json);
	}

	/**
	 * 删除个性化菜单
	 * @param $menuid
	 * @return mixed
	 */
	public function delete_selfmenu($menuid)
	{
		$data = sprintf('{"menuid":"%s"}', $menuid);
		return $this->http_post('cgi-bin/menu/delconditional?access_token=',$data);
	}

	/**
	 *  测试个性化菜单匹配结果
	 *
	 * @param $openid
	 * @return mixed
	 */
	public function trymatch($openid)
	{
		$data = sprintf('{"user_id":"%s"}', $openid);
		return $this->http_post('cgi-bin/menu/delconditional?access_token=',$data);
	}

	/**
	 *  得到获取用户授权需要打开的页面链接(第一步)
	 *
	 * @param string $scope 应用授权作用域 snsapi_base | snsapi_userinfo
	 * @param string $redirect_uri 授权后重定向的地址
	 * @param string $state [可以填写a-zA-Z0-9的参数值，最多128字节]
	 * @return string
	 */
	public function get_authorize_url($scope, $redirect_uri, $state = '')
	{
		$redirect_uri = urlencode($redirect_uri);
		$url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $this->appid .
			'&redirect_uri=' . $redirect_uri . '&response_type=code&scope=' . $scope .
			'&state=' . $state . '#wechat_redirect';
		return $url;
	}

	/**
	 *  获取用户授权后回调页面根据获取到的code，获取用户信息(第二步)
	 *
	 * @param  string $scope snsapi_base | snsapi_userinfo
	 * @param string $lang [可选，返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语]
	 * @return bool|mixed
	 */
	public function get_userinfo_by_authorize($scope, $lang = 'zh_CN')
	{
		$code = input('code');
		if($code){
			$url = 'sns/oauth2/access_token?appid=' . $this->appid.
				"&secret={$this->secret}&code={$code}&grant_type=authorization_code";
			$res = Helper::get_html($this->host . $url);
			if($res && isset($res['access_token'])){
				if ($scope == 'snsapi_userinfo') {
					$url = "sns/userinfo?access_token={$res['access_token']}&openid={$res['openid']}&lang={$lang}";
					return Helper::get_html($this->host . $url);
				}
			}
			return $res;
		}else{
			return !1;
		}
	}

	/**
	 *  多媒体文件上传接口
	 *
	 * Examples:
	 * ```
	 * $api = Api::getInstance();
	 * $api->upload('/www/temp/abc.jpg', 'image');
	 *
	 * ```
	 * @param string $path 完整的文件路径
	 * @param string $type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @author d3li 2/15/2019
	 * @return mixed
	 */
	public function upload($path, $type)
	{
		$url = 'http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=' . $this->access_token;
		return Helper::curl($url . '&type=' . $type, ['media'=> new \CURLFile($path)]);
	}

	/**
	 * 下载多媒体文件接口
	 *
	 * @author d3li 2/15/2019
	 * @param $media_id
	 * @return mixed
	 */
	public function download($media_id)
	{
		$url = 'http://file.api.weixin.qq.com/cgi-bin/media/get?access_token='. $this->access_token;
		return Helper::http_curl($url. '&media_id=' . $media_id, '', '', '');
	}

	/**
	 * 生成带参数的二维码
	 *
	 * @author d3li 17/04/2019
	 * @param mixed $key 参数
	 * @param string $path
	 * @param string $filename
	 * @param int $expire 过期秒数
	 * @return mixed
	 */
	public function qrcode($key, $filename, $path ='qrcode', $expire = 0)
	{
		$file = preg_replace("/^\.?\//", '', "$path/$filename.png");
		if(file_exists('./'.$file))return '/'.$file;
		if(is_numeric($key)){
			$action =$expire ? 'QR_SCENE' : 'QR_LIMIT_SCENE';
			$data = sprintf('{"action_name": "%s", "action_info": {"scene": {"scene_id": %s}}}', $action, $key);
		}else{
			$action = $expire ? 'QR_STR_SCENE' : 'QR_LIMIT_STR_SCENE';
			$data = sprintf('{"action_name":"%s","action_info":{"scene":{"scene_str":"%s"}}}', $action, $key);
		}
		if($expire){
			$data = implode('', ['{"expire_seconds":', $expire, ',', substr($data, 1)]);
		}
		$res = $this->http_post('cgi-bin/qrcode/create?access_token=',$data);
//		if($image && $res && isset($res['ticket'])){
//			return Helper::get_html('https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$res['ticket']);
//		}
		if($res && isset($res['url'])){
			$dir = dirname('./'.$file);
			if(!is_dir($dir)) mkdir($dir, 0777, true);
			$gen = Generate::instance();
			$gen->set_config([
				'ecc' => 'H',
				'size' => 10,
				'file' => $file,
				'quality' => 90,
				'logo' => 'resource/png/logo.png',
				'logo_size' => 100,
				'logo_outline_size' => 20,
				'logo_outline_color' => '#FFFFFF',
				'logo_radius' => 15,
				'logo_opacity' => 100,
			]);
			$res = '/'.$gen->qrcode($res['url']);
		}
		return $res;
	}



	/**************************二维码相关***************************/
    /**
     * 创建二维码，可创建指定有效期的二维码和永久二维码
     * @param  integer $scene_id       二维码参数
     * @param  integer $expire_seconds 二维码有效期，0-永久有效
     */
    public function qrcodeCreate($scene_id, $expire_seconds = 1200){
        
        $data = array();
        if(is_numeric($expire_seconds) && $expire_seconds > 0){
            $data['expire_seconds'] = $expire_seconds;
            $data['action_name']    = self::QR_SCENE;
        } else {
            $data['action_name']    = self::QR_LIMIT_SCENE;
        }

        $data['action_info']['scene']['scene_id'] = $scene_id;
        return $this->api('qrcode/create', $data);
    }
    /**
     * 字符串ticket
     * @return [type] [description]
     */
    public function textCreat($str, $expire_seconds = 1200){
        $data = array();
        if(is_numeric($expire_seconds) && $expire_seconds > 0){
            $data['expire_seconds'] = $expire_seconds;
            $data['action_name']    = self::QR_STR_SCENE;
        } else {
            $data['action_name']    = self::QR_LIMIT_STR_SCENE;
        }
        $data['action_info']['scene']['scene_str'] = $str;
        return $this->api('qrcode/create', $data);
    }


        /**
         * JS-SDK 生成一个新的jsapi_ticket->这一届获取带参数验证码
         *
         * @return mixed
         */
        public function new_jsapi_ticket()
        {
        $url = self::API_DOMAIN . 'cgi-bin/ticket/getticket?access_token=' . $this->get_access_token() . '&type=jsapi';


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
        $data = curl_exec($curl);

        $leng=strlen($data);
        $star=strpos($data,"{");
        $json=substr($data,$star,$leng);
        $data_end=json_decode($json,true);


        curl_close($curl);

        return $data_end;


        }
    /**
     * 根据ticket获取二维码URL
     * @param  string $ticket 通过 qrcodeCreate接口获取到的ticket
     * @return string         二维码URL
     */
    public function showqrcode($ticket){
        $ticket=urlencode($ticket);
        return "{$this->qrcodeURL}/showqrcode?ticket={$ticket}";
    }
	/**************************二维码相关***************************/
    
    /**
     * 调用微信api获取响应数据
     * @param  string $name   API名称
     * @param  string $data   POST请求数据
     * @param  string $method 请求方式
     * @param  string $param  GET请求参数
     * @return array          api返回结果qrcode/create
     */
    protected function api($name, $data = '', $method = 'POST', $param = '', $json = true){
        $params = array('access_token' => $this->access_token);

        if(!empty($param) && is_array($param)){
            $params = array_merge($params, $param);
        }

        $url  = "{$this->apiURL}/{$name}";
        if($json && !empty($data)){
            //保护中文，微信api不支持中文转义的json结构
            array_walk_recursive($data, function(&$value){
                $value = urlencode($value);
            });
            $data = urldecode(json_encode($data));
        }
        $data = self::http_api($url, $params, $data, $method);
      
        return json_decode($data, true);
    }

    
    
    
    
    /**
     * 发送HTTP请求方法，目前只支持CURL发送请求
     * @param  string $url    请求URL
     * @param  array  $param  GET参数数组
     * @param  array  $data   POST的数据，GET请求时该参数无效
     * @param  string $method 请求方法GET/POST
     * @return array          响应数据
     */
    protected static function http_api($url, $param, $data = '', $method = 'GET'){
        $opts = array(
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        );

        /* 根据请求类型设置特定参数 */
        $opts[CURLOPT_URL] = $url . '?' . http_build_query($param);

        if(strtoupper($method) == 'POST'){
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $data;
            
            if(is_string($data)){ //发送JSON数据
                $opts[CURLOPT_HTTPHEADER] = array(
                    'Content-Type: application/json; charset=utf-8',  
                    'Content-Length: ' . strlen($data),
                );
            }
        }

        /* 初始化并执行curl请求 */
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        $data  = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        //发生错误，抛出异常
        if($error) throw new \Exception('请求发生错误：' . $error);

        return  $data;
    }
}