<?php
namespace wechat;

use Exception;
/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/13  16:55
 * Desc：提供基于PKCS7算法的加解密接口.
 */
class Encoder
{
	public static $block_size = 32;
	public static $key;
	private static $token;
	private static $encodingAesKey;
	private static $appId;
	/**
	 * 消息初始函数
	 * @param $token string 公众平台上，开发者设置的token
	 * @param $encodingAesKey string 公众平台上，开发者设置的EncodingAESKey
	 * @param $appId string 公众平台的appId
	 */
	public static function set($token, $encodingAesKey, $appId)
	{
		self::$token = $token;
		self::$encodingAesKey = $encodingAesKey;
		self::$appId = $appId;
	}
	/**
	 * 将公众平台回复用户的消息加密打包.
	 * <ol>
	 *    <li>对要发送的消息进行AES-CBC加密</li>
	 *    <li>生成安全签名</li>
	 *    <li>将消息密文和安全签名打包成xml格式</li>
	 * </ol>
	 *
	 * @param $replyMsg string 公众平台待回复用户的消息，xml格式的字符串
	 * @param $timeStamp string 时间戳，可以自己生成，也可以用URL参数的timestamp
	 * @param $nonce string 随机串，可以自己生成，也可以用URL参数的nonce
	 * @param &$encryptMsg string 加密后的可以直接回复用户的密文，包括msg_signature, timestamp, nonce, encrypt的xml格式的字符串,
	 *                      当return返回0时有效
	 *
	 * @return int 成功0，失败返回对应的错误码
	 */
	public static function encryptMsg($replyMsg, $timeStamp, $nonce, &$encryptMsg)
	{
		self::$key = self::$encodingAesKey;

		//加密
		$array = self::encrypt($replyMsg, self::$appId);
		$ret = $array[0];
		if ($ret != 0) {
			return $ret;
		}

		if ($timeStamp == null) {
			$timeStamp = time();
		}
		$encrypt = $array[1];

		//生成安全签名
		$array = self::getSHA1(self::$token, $timeStamp, $nonce, $encrypt);
		$ret = $array[0];
		if ($ret != 0) {
			return $ret;
		}
		$signature = $array[1];

		//生成发送的xml
		$parse = array($encrypt, $signature, $timeStamp, $nonce);
		$encryptMsg = Helper::array2xml($parse);
		return Error::$OK;
	}


	/**
	 * 检验消息的真实性，并且获取解密后的明文.
	 * <ol>
	 *    <li>利用收到的密文生成安全签名，进行签名验证</li>
	 *    <li>若验证通过，则提取xml中的加密消息</li>
	 *    <li>对消息进行解密</li>
	 * </ol>
	 *
	 * @param $msgSignature string 签名串，对应URL参数的msg_signature
	 * @param $timestamp string 时间戳 对应URL参数的timestamp
	 * @param $nonce string 随机串，对应URL参数的nonce
	 * @param $postData string 密文，对应POST请求的数据
	 * @param &$msg string 解密后的原文，当return返回0时有效
	 *
	 * @return int 成功0，失败返回对应的错误码
	 */
	public static function decryptMsg($msgSignature, $timestamp = null, $nonce, $postData, &$msg)
	{
		if (strlen(self::$encodingAesKey) != 43) {
			return Error::$IllegalAesKey;
		}

		self::$key = self::$encodingAesKey;

		//提取密文
		$array = Helper::xml2array($postData);

		if (empty($array)) {
			return  Error::$ParseXmlError;
		}

		if ($timestamp == null) {
			$timestamp = time();
		}

		$encrypt = $array['Encrypt'];

		//验证安全签名
		$array =self::getSHA1(self::$token, $timestamp, $nonce, $encrypt);
		$ret = $array[0];

		if ($ret != 0) {
			return $ret;
		}

		$signature = $array[1];
		if ($signature != $msgSignature) {
			return Error::$ValidateSignatureError;
		}

		$result = self::decrypt($encrypt, self::$appId);
		if ($result[0] != 0) {
			return $result[0];
		}
		$msg = $result[1];

		return Error::$OK;
	}

	/**
	 * 对需要加密的明文进行填充补位
	 *
	 * @param  string $text 需要进行填充补位操作的明文
	 * @return string 补齐明文字符串
	 */
	public static function encode($text)
	{
		$text_length = strlen($text);
		//计算需要填充的位数
		$amount_to_pad = self::$block_size - ($text_length % self::$block_size);
		if ($amount_to_pad == 0) {
			$amount_to_pad = self::$block_size;
		}
		//获得补位所用的字符
		$pad_chr = chr($amount_to_pad);
		$tmp = "";
		for ($index = 0; $index < $amount_to_pad; $index++) {
			$tmp .= $pad_chr;
		}
		return $text . $tmp;
	}

	/**
	 * 对解密后的明文进行补位删除
	 *
	 * @param string decrypted 解密后的明文
	 * @return string 删除填充补位后的明文
	 */
	public static function decode($text)
	{

		$pad = ord(substr($text, -1));
		if ($pad < 1 || $pad > 32) {
			$pad = 0;
		}
		return substr($text, 0, (strlen($text) - $pad));
	}

	/**
	 * 对明文进行加密
	 *
	 * @param string $text 需要加密的明文
	 * @param $appid
	 * @return array 加密后的密文
	 */
	public static function encrypt($text, $appid)
	{

		try {
			//获得16位随机字符串，填充到明文之前
			$random = self::createNonce();
			$text = $random . pack("N", strlen($text)) . $text . $appid;
			// 网络字节序
			//$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
			$iv = substr(self::$key, 0, 16);
			//使用自定义的填充方式对明文进行补位填充
			$text = self::encode($text);
			mcrypt_generic_init($module, self::$key, $iv);
			//加密
			$encrypted = mcrypt_generic($module, $text);
			mcrypt_generic_deinit($module);
			mcrypt_module_close($module);

			//print(base64_encode($encrypted));
			//使用BASE64对加密后的字符串进行编码
			return array(Error::$OK, base64_encode($encrypted));
		} catch (Exception $e) {
			//print $e;
			return array(Error::$EncryptAESError, null);
		}
	}

	/**
	 * 对密文进行解密
	 *
	 * @param string $encrypted 需要解密的密文
	 * @return mixed 解密得到的明文
	 */
	public static function decrypt($encrypted, $appid)
	{

		try {
			//使用BASE64对需要解密的字符串进行解码
			$ciphertext_dec = base64_decode($encrypted);
			$module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
			$iv = substr(self::$key, 0, 16);
			mcrypt_generic_init($module, self::$key, $iv);

			//解密
			$decrypted = mdecrypt_generic($module, $ciphertext_dec);
			mcrypt_generic_deinit($module);
			mcrypt_module_close($module);
		} catch (Exception $e) {
			return array(Error::$DecryptAESError, null);
		}


		try {
			//去除补位字符
			$result = self::decode($decrypted);
			//去除16位随机字符串,网络字节序和AppId
			if (strlen($result) < 16)
				return "";
			$content = substr($result, 16, strlen($result));
			$len_list = unpack("N", substr($content, 0, 4));
			$xml_len = $len_list[1];
			$xml_content = substr($content, 4, $xml_len);
			$from_appid = substr($content, $xml_len + 4);
		} catch (Exception $e) {
			//print $e;
			return array(Error::$IllegalBuffer, null);
		}
		if ($from_appid != $appid)
			return array(Error::$ValidateAppidError, null);
		return array(0, $xml_content);

	}

	/**
	 * 用SHA1算法生成安全签名
	 *
	 * @param string $token 票据
	 * @param string $timestamp 时间戳
	 * @param string $nonce 随机字符串
	 * @param string $encrypt 密文消息
	 * @return array
	 */
	public static function getSHA1($token, $timestamp, $nonce, $encrypt)
	{
		//排序
		try {
			$array = array($encrypt, $token, $timestamp, $nonce);
			sort($array, SORT_STRING);
			$str = implode($array);
			return array(Error::$OK, sha1($str));
		} catch (Exception $e) {
			//print $e . "\n";
			return array(Error::$ComputeSignatureError, null);
		}
	}

	/**
	 * 随机生成16位字符串
	 *
	 * @param int $length
	 * @return string 生成的字符串
	 */
	private static function createNonce($length = 16)
	{

		$str = "";
		$str_pol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		$max = strlen($str_pol) - 1;
		for ($i = 0; $i < $length; $i++) {
			$str .= $str_pol[mt_rand(0, $max)];
		}
		return $str;
	}
}