<?php

namespace wechat;

use \think\Db;
use \think\Log;

class Grab{

	/**
	 * 
	 * 获取抢单用户信息
	 * 生成抢单信息
	 * 发送模板消息消息
	 * 接受抢单
	 * 拒接抢单
	 * 生成文件
	 * 文件锁定
	 * 
	 * @var [type]
	 */
	protected static $template_id='ukseMlCqqPAGLy52j1JpZo42DyuxREjNhXGFQdTtuaM';//使用模板消息
	protected static $template_data=[
		'first'=>'你有一个新的订单请注意查收',
		'OrderSn'=>'E55666222',
		'OrderStatus'=>'待抢单',
		'remark'=>'有一个新的订单'
	];
	protected static $path='grouplock/';//文件锁路径
	protected static $link;
	public function __construct($template_id='',$path='')
	{

		if(!empty($template_id)){
			$this->setTemplate($template_id);
		}
		if(!empty($path)){
			$this->setPath($path);
		}
	}
	
	public  function setTemplate($template_id='ukseMlCqqPAGLy52j1JpZo42DyuxREjNhXGFQdTtuaM'){
		self::$template_id=$template_id;
	}

	public  function setPath($path='grouplock/'){
		self::$path=$path;
	}
	/**
	 * 同意接单
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:01:04+0800
	 */
	public static function Allow(){
		echo "同意";
	}
	/**
	 * 拒绝接单
	 * [Refuse description]
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:00:56+0800
	 */
	public static function Refuse(){
		echo '拒绝';
	}
	/**
	 * 获取当前抢单用户信息
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:01:29+0800
	 */
	public static function UserInfo(){
		echo "获取用户信息";
	}
	/**
	 * 获取店铺微信绑定用户
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:33:37+0800
	 */
	public static function StoreUser(){
		echo '获取店铺微信绑定用户';
	}
	/**
	 * 获取员工微信绑定用户
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:33:16+0800
	 * @return   [type]                   [description]
	 */
	public static  function employee(){

	}
	/**
	 * 修改发送通知信息
	 * admin
	 * @Author   ksea
	 * @DateTime 2019-03-13T13:51:36+0800
	 * @param    [type]                   $data [server_id,sfuserid]
	 * @return   [type]                         [description]
	 */
	public static function GetTemplate($data){

		$server_data = Db::name('shop_service_order')->where(['id'=>$data['server_id']])->find();

		$shop_goods  = Db::name('shop_goods')->where(['id'=>$server_data['goodsid']])->find();

		$userinfo  = Db::name('shop_userinfo')->where(['id'=>$server_data['uid']])->find();

		$order=Db::name('shop_order')->where('id',$server_data['orderid'])->find();

		$name=$shop_goods['name'];
		$type=$shop_goods['cate'];
		if(!$type){
			Log::info([
				'type'    => 'goodid error',
				'message' =>'商品类型没找到',
				'data'	  =>$type,
				'where'	  =>$server_data['goodsid'],
				]);
		}
		$server_id= $data['server_id'];
		$area     = $userinfo['area_id'];
		$sfuserid = $data['sfuserid'];
		self::$link=\think\Request::instance()->domain().'/aunt';//抢单默认跳转页面;
		$link=self::$link;
		Grab::$template_data['OrderSn']=$server_data['snserver'];
		$ret = model('admin/shop/OrderTemp')::get(['orderid'=> $data['orderid']]);
		Grab::$template_data['remark']=sprintf('%s\n预约时间：%s %s\n服务地址：%s',$name, $ret['d_data'], $ret['d_time'], $server_data['address']);
//		if($order['create_time']>'1556121600'&&$shop_goods['cate']!=1){
//			//Grab::$template_data['remark']=$name.'×'.$order['num'].'\n'.'预约时间：'.date('Y年m月d日 H:i:s',$server_data['appoint_start']).'\n'.'服务地址：'.$server_data['address'];
//			Grab::$template_data['remark']=$name.'\n'.'预约时间：'.date('Y年m月d日 H:i:s',$server_data['appoint_start']).'\n'.'服务地址：'.$server_data['address'];
//
//		}
//		else{
//			Grab::$template_data['remark']=$name.'\n'.'预约时间：'.date('Y年m月d日 H:i:s',$server_data['appoint_start']).'\n'.'服务地址：'.$server_data['address'];
//		}
//		Grab::SendGroup($type,$area,$sfuserid,$link);
	}
	/**
	 * 预约位置绑定抢单
	 * @param [type] $data [goodsid,areaid,server_id,sfuserid]
	 * 2019-03-12 23:14:03
	 */
	public static function Stemplate($data){
		$goodsid=$data['goodsid'];
		$shop_goods=Db::name('shop_goods')->where(['id'=>$goodsid])->find();
		//限制某个店铺才能接受抢单
		if($shop_goods['admin_id']!=2){
			return false;
		}
		$name=$shop_goods['name'];
		$type=$shop_goods['cate'];
		if(!$type){
			Log::info([
				'type'    => 'goodid error',
				'message' =>'商品类型没找到',
				'data'	  =>$type,
				'where'	  =>$goodsid,
				]);
		}
		$server_id=$data['server_id'];
		$area=$data['areaid'];
		$sfuserid=$data['sfuserid'];
		$shop_service_order=Db::name('shop_service_order')->where('id',$server_id)->find();
		$order=Db::name('shop_order')->where('id',$shop_service_order['orderid'])->find();
		$region_name=Db::name('region')->where('region_id',$area)->value('region_name');
		$street_name = Db::name('shop_order_temp')->where('orderid',$shop_service_order['orderid'])->value('street');
		self::$link=\think\Request::instance()->domain().'/aunt';//抢单默认跳转页面;
		$link=self::$link;
		Grab::$template_data['OrderSn']=$shop_service_order['snserver'];
		$time = date('Y年m月d日 H:i:s',(int)$shop_service_order['appoint_start']);
		$ret = model('admin/shop/OrderTemp')::get(['orderid'=> $shop_service_order['orderid']]);
		if($ret){
			$time = $ret['d_data'] . ' ' . $ret['d_time'];
		}
		if(strlen($shop_service_order['address'])>0){
			if($order['create_time']>'1556121600'&&$shop_goods['cate']!=1){
				//Grab::$template_data['remark']=$name.'×'.$order['num'].'\n'.'预约时间：'.date('Y年m月d日 H:i:s',(int)$shop_service_order['appoint_start']).'\n'.'服务地址：'.$shop_service_order['address'];
				$tempText = $name.'\n'.
                    '预约时间：'.$time.'\n'.
                    '服务区域：'.$region_name.'\n';
				if(!empty($street_name)){
                    $tempText = $tempText.'服务街道：'.$street_name.'\n';
                }
                $tempText = $tempText.'服务地址：'.$shop_service_order['address'];
                Grab::$template_data['remark']=$tempText;
			} else {
                $tempText = $name.'\n'.
                    '预约时间：'.$time.'\n'.
                    '服务区域：'.$region_name.'\n';
                if(!empty($street_name)){
                    $tempText = $tempText.'服务街道：'.$street_name.'\n';
                }
                $tempText = $tempText.'服务地址：'.$shop_service_order['address'];
                Grab::$template_data['remark']=$tempText;
//				Grab::$template_data['remark']=$name.'\n'.
//                    '预约时间：'.date('Y年m月d日 H:i:s',(int)$shop_service_order['appoint_start']).'\n'.
//                    '服务区域：'.$region_name.'\n'.
//                    '服务街道：'.$street_name.'\n'.
//                    '服务地址：'.$shop_service_order['address'];
			}
		     self::SendGroup($type,$area,$sfuserid,$link);
	    }
	}
	/**
	 * 准备发送模板消息用户列表
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:49:59+0800
	 */
	public static function SendGroup($type,$area,$sfuserid='',$link=''){
		if(strlen($sfuserid)<=0){
			$open_arrt=Db::name('Employee')->where('openid is not null')->where("FIND_IN_SET($type,job)  and FIND_IN_SET($area,area_id)")->column('openid');
		}
		else{

			$link=self::$link;
        

			Grab::$template_data['OrderStatus']='已接收预约';


			$where['id']=array('in',$sfuserid);

            $open_arrt=Db::name('Employee')->where('openid is not null')->where("FIND_IN_SET($type,job)  and FIND_IN_SET($area,area_id)")->where($where)->column('openid');
		}
		$resd=self::SendMessage($open_arrt,$link);
        Log::info('=========////=======////====|||III=======///===');
        Log::info($resd);
        Log::info('=========////=======////====|||IIIII=======///===');
      
	}
	/**********************************文件锁操作*********************************************/
										/***使用方法****/
						/***************self::Lock($server_id)**************/
						/*if(***){
							***
						}
						*/
					     /***************self::UnLock($server_id)*****************/

	/**
	 * 读取文件锁定状态
	 * @Author   ksea
	 * @DateTime 2019-02-27T17:26:56+0800
	 * @param    [type]                   $server_id [工单id]
	 */
	public static function ReadLock($server_id){
		$lock_name=$server_id;
		$path=self::$path;
		$lock_file=ROOT_PATH ."/public/".$path.$lock_name.'.txt';
		$resource=fopen($lock_file,"r");
		$res=fread($resource,filesize($lock_file));
		fclose($resource);
		return $res;
	}
	/**
	 * 锁定文件(抢单使用)
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:01:43+0800
	 * @param    [type]                   $server_id [工单id]
	 */
	public static function Lock($server_id){
		$lock_name=$server_id;
		$path=self::$path;
		if(!is_dir($path)){
			self::CreateFile();
		}
		$lock_file=ROOT_PATH ."/public/".$path.$lock_name.'.txt';
		$lock_data=1;
		if(is_file($lock_file)){
			if(self::ReadLock($server_id)!=0){
				Log::write(['type'=>'error','data'=>[$lock_file,$lock_data],'message'=>'添加锁定文件失败','time'=>date('Y/m/d H:i:s')]);
				return false;
				exit();
			}
		}
		$r = file_put_contents($lock_file,$lock_data);
		if (!$r) {
			Log::write(['type'=>'error','data'=>[$lock_file,$lock_data],'message'=>'添加锁定文件失败','time'=>date('Y/m/d H:i:s')]);
			return false;
		}
        return true;
	}
	/**
	 * 解锁文件(抢单使用)
	 * @Author   ksea
	 * @DateTime 2019-02-27T16:29:14+0800
	 * @param    [type]                   $server_id [工单id]
	 */
	public static function UnLock($server_id){
		$lock_name=$server_id;
		$path=self::$path;
		if(!is_dir($path)){
			self::CreateFile();
		}
		$lock_file=ROOT_PATH ."/public/".$path.$lock_name.'.txt';
		$lock_data=0;
		$r = file_put_contents($lock_file,$lock_data);
		if (!$r) {
			Log::write(['type'=>'error','data'=>[$lock_file,$lock_data],'message'=>'解放锁定文件失败','time'=>date('Y/m/d H:i:s')]);
		}
	}
	/**
	 * 创建锁定文件(抢单使用)
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:02:31+0800
	 * @param    [type]                   $server_id [工单id]
	 */
	public static function CreateFile(){
		$path=self::$path;
		$res_dir=mkdir($path,0777,true);
		if(!$res_dir){
			Log::write(['type'=>'添加路径失败','message'=>$path,'time'=>date('Y/m/d H:i:s')]);
		}
	}
	/*************************************文件锁操作******************************************/
	/**
	 * 发送抢单模板消息
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:02:47+0800
	 */
	public static function SendMessage($openid_arr,$link='',$count='',$obj=''){
		$data=self::$template_data;
        $api=new \wechat\Api();
        if(empty($obj)){
        	$obj=$api;
        	$count=count($openid_arr);
        }
        if($count>0){
	        $openid=$openid_arr[$count-1];
	        $template_id=self::$template_id;
	        $link=$link?$link:'https://www.xiaogebang.com';
	        $res=$api->send_template($openid,$template_id,$data,$link);
	        $count-=1;
        	return self::SendMessage($openid_arr,$link,$count,$obj);
        }
        return '处理成功';

	}

}