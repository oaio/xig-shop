<?php
namespace wechat;

/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/13  15:45
 * Desc：扩展助手函数
 */
class Helper
{
	/**
	 *  简单数组转换xml
	 *
	 * @param array $ary 目标数组
	 * @param string $first 设置根标签，默认xml
	 * @param bool $cdata 是否忽略解析，默认忽略
	 * @param string $item 数字键名，默认item
	 * @return string
	 */
	public static function array2xml($ary, $first='xml', $cdata=true, $item = 'item')
	{
		$xml = "<{$first}>";
		foreach ($ary as $key => $value) {
			is_numeric($key) && $key = $item;
			$xml .= "\n<{$key}>";
			$xml .= $cdata && !is_numeric($value) ? "<![CDATA[{$value}]]></{$key}>" : $value . "</{$key}>";
		}
		return $xml . "\n</{$first}>";
	}

	/**
	 *  将XML转为数组
	 *
	 * @param string $xml 目标文档
	 * @return mixed
	 */
	public static function xml2array($xml)
	{
		libxml_disable_entity_loader(true);
		return json_decode(json_encode(simplexml_load_string($xml,
			'SimpleXMLElement', LIBXML_NOCDATA)), true);
	}

	/**
	 *  请求 html 或 xml
	 *
	 * @param $url
	 * @return mixed
	 */
	public static function get_html($url)
	{
		return self::http_curl($url,'get','','');
	}

	/**
	 *  通过curl扩展发送HTTP请求
	 *
	 * @param string $url 请求地址
	 * @param string $type 请求方式，默认get
	 * @param string|array $data post数据，默认为空
	 * @param string $res 返回结果，默认json
	 * @return mixed
	 */
	public static function http_curl($url, $type = 'get', $data = '', $res = 'json')
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, !1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, !1);
		if ($type == 'post') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
				is_array($data) ? http_build_query($data) : $data);
		}
		$output = curl_exec($ch);
		curl_close($ch);
		return $res == 'json' ? json_decode($output, true) : $output;
	}

	public static function curl($options, $data=0, $result = 'json')
	{
		$default = array(
			CURLOPT_SSL_VERIFYPEER => !1,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_RETURNTRANSFER => 1,
		);
		if($data){
			$default[CURLOPT_POST] = 1;
			$default[CURLOPT_POSTFIELDS] = $data;
		}
		$ch = curl_init();
		curl_setopt_array($ch, $default);
		is_array($options) ? curl_setopt_array($ch, $options)
			: curl_setopt($ch, CURLOPT_URL, $options);
		$res = curl_exec($ch);
		curl_close($ch);
		return $result != 'json' ? $res : json_decode($res, !0);
	}
}