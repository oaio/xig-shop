<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace wechat;
/**
 * Generate.php 生成带logo的二维码
 *
 * @author d3li <d3li@sina.com>
 * @create：19/04/2019  9:56 AM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.19
 * @describe
 */
class Generate
{
	private static $instance = null;
	private $config = array(
		'ecc' => 'H',                       // 二维码质量 L-smallest, M, Q, H-best
		'size' => 2,                       // 二维码尺寸 1-50
		'file' => 'qrcode.png',        // 创建的二维码路径
		'quality' => 100,                    // 图片质量
		'logo' => '',                       // logo路径，为空表示没有logo
		'logo_size' => null,                // logo尺寸，null表示按二维码尺寸比例自动计算
		'logo_outline_size' => null,        // logo描边尺寸，null表示按logo尺寸按比例自动计算
		'logo_outline_color' => '#FFFFFF',  // logo描边颜色
		'logo_opacity' => 100,              // logo不透明度 0-100
		'logo_radius' => 0,                 // logo圆角角度 0-30
	);

	/**
	 * @param array $config
	 */
	public function set_config($config)
	{
		$this->config = $config;
	}

	public static function instance(){
		if(!self::$instance instanceof self) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * 创建二维码
	 *
	 * @param $data
	 * @return mixed|string
	 */
	public function qrcode($data)
	{
		vendor('phpqrcode.phpqrcode');
		$tmp_qrcode_file = dirname(__FILE__).'/tmp_qrcode_'.$_SERVER['REQUEST_TIME'].mt_rand(100,999).'.png';
		\QRcode::png($data, $tmp_qrcode_file, $this->config['ecc'], $this->config['size'], 2);
		$this->add_logo($tmp_qrcode_file);
		if($tmp_qrcode_file!='' && file_exists($tmp_qrcode_file)){
			unlink($tmp_qrcode_file);
		}
		return file_exists($this->config['file'])? $this->config['file'] : '';
	}

	/**
	 * 合拼临时二维码图片与logo图片
	 *
	 * @param string $tmp_qrcode_file 临时二维码图片
	 * @return int
	 */
	private function add_logo($tmp_qrcode_file){

		// 创建目标文件夹
		$this->create_dirs(dirname($this->config['file']));

		// 获取目标图片的类型
		$dest_ext = $this->suffix($this->config['file']);

		// 需要加入logo
		if(file_exists($this->config['logo'])){
			$tmp_qrcode_img = imagecreatefrompng($tmp_qrcode_file);
			list($qrcode_w, $qrcode_h) = getimagesize($tmp_qrcode_file);
			list($logo_w, $logo_h, $logo_type) = getimagesize($this->config['logo']);

			// 创建logo图片对象
			switch($logo_type){
				case 1: $logo_img = imagecreatefromgif($this->config['logo']); break;
				case 2: $logo_img = imagecreatefromjpeg($this->config['logo']); break;
				case 3: $logo_img = imagecreatefrompng($this->config['logo']); break;
				default: return '';
			}

			// 设定logo图片合拼尺寸，没有设定则按比例自动计算
			$new_logo_w = isset($this->config['logo_size'])? $this->config['logo_size'] : (int)($qrcode_w/5);
			$new_logo_h = isset($this->config['logo_size'])? $this->config['logo_size'] : (int)($qrcode_h/5);

			// 按设定尺寸调整logo图片
			$new_logo_img = imagecreatetruecolor($new_logo_w, $new_logo_h);
			$background = imagecolorallocate($new_logo_img, 255, 255, 255);
			imagefilledrectangle($new_logo_img, 0, 0, $new_logo_w, $new_logo_h, $background);
			imagecopyresampled($new_logo_img, $logo_img,
				0, 0, 0, 0, $new_logo_w, $new_logo_h, $logo_w, $logo_h);

			// 判断是否需要描边
			if(!isset($this->config['logo_outline_size']) || $this->config['logo_outline_size']>0){
				list($new_logo_img, $new_logo_w, $new_logo_h) = $this->image_outline($new_logo_img);
			}

			// 判断是否需要圆角处理
			if($this->config['logo_radius']>0){
				$new_logo_img = $this->image_fillet($new_logo_img);
			}

			// 合拼logo与临时二维码
			$pos_x = ($qrcode_w-$new_logo_w)/2;
			$pos_y = ($qrcode_h-$new_logo_h)/2;

			imagealphablending($tmp_qrcode_img, true);

			// 合拼图片并保留各自透明度
			$img_data = $this->alpha_merge($tmp_qrcode_img, $new_logo_img,
				$pos_x, $pos_y, 0, 0, $new_logo_w, $new_logo_h, $this->config['logo_opacity']);
			$this->draw($img_data, $dest_ext);
		}else{
			$this->draw(imagecreatefrompng($tmp_qrcode_file), $dest_ext);
		}
		return 1;
	}

	/**
	 * 生成图片
	 *
	 * @param $img_data
	 * @param $suffix
	 */
	private function draw($img_data, $suffix)
	{
		switch ($suffix){
			case 'jpg':
			case 'jpeg':
				imagejpeg($img_data, $this->config['file'], $this->config['quality']);
				break;
			case 'gif':
				imagegif($img_data, $this->config['file']);
				break;
			case 'png':
				imagepng($img_data, $this->config['file'], (int)(($this->config['quality']-1)/10));
				break;
		}
	}

	private function create_dirs($path)
	{
		if(!is_dir($path)){
			return mkdir($path, 0777, true);
		}
		return true;
	}

	/**
	 * 对图片对象进行描边
	 *
	 * @param $img
	 * @return array
	 */
	private function image_outline($img)
	{
		// 获取图片宽高
		$img_w = imagesx($img);
		$img_h = imagesy($img);

		// 计算描边尺寸，没有设定则按比例自动计算
		$bg_w = isset($this->config['logo_outline_size'])? intval($img_w + $this->config['logo_outline_size']) : $img_w + (int)($img_w/5);
		$bg_h = isset($this->config['logo_outline_size'])? intval($img_h + $this->config['logo_outline_size']) : $img_h + (int)($img_h/5);

		// 创建底图对象
		$bg_img = imagecreatetruecolor($bg_w, $bg_h);

		// 设置底图颜色
		$rgb = $this->hex2rgb($this->config['logo_outline_color']);
		$bgcolor = imagecolorallocate($bg_img, $rgb['r'], $rgb['g'], $rgb['b']);

		// 填充底图颜色
		imagefill($bg_img, 0, 0, $bgcolor);

		// 合拼图片与底图，实现描边效果
		imagecopy($bg_img, $img, (int)(($bg_w-$img_w)/2), (int)(($bg_h-$img_h)/2), 0, 0, $img_w, $img_h);

		$img = $bg_img;

		return array($img, $bg_w, $bg_h);

	}

	/**
	 * 对图片对象进行圆角处理
	 *
	 * @param $img
	 * @return resource
	 */
	private function image_fillet($img)
	{
		// 获取图片宽高
		$img_w = imagesx($img);
		$img_h = imagesy($img);

		// 创建圆角图片对象
		$new_img = imagecreatetruecolor($img_w, $img_h);

		// 保存透明通道
		imagesavealpha($new_img, true);

		// 填充圆角图片
		$bg = imagecolorallocatealpha($new_img, 255, 255, 255, 127);
		imagefill($new_img, 0, 0, $bg);

		// 圆角半径
		$r = $this->config['logo_radius'];

		// 执行圆角处理
		for($x=0; $x<$img_w; $x++){
			for($y=0; $y<$img_h; $y++){
				$rgb = imagecolorat($img, $x, $y);

				// 不在图片四角范围，直接画图
				if(($x>=$r && $x<=($img_w-$r)) || ($y>=$r && $y<=($img_h-$r))){
					imagesetpixel($new_img, $x, $y, $rgb);

					// 在图片四角范围，选择画图
				}else{
					// 上左
					$ox = $r; // 圆心x坐标
					$oy = $r; // 圆心y坐标
					if( ( ($x-$ox)*($x-$ox) + ($y-$oy)*($y-$oy) ) <= ($r*$r) ){
						imagesetpixel($new_img, $x, $y, $rgb);
					}

					// 上右
					$ox = $img_w-$r; // 圆心x坐标
					$oy = $r;        // 圆心y坐标
					if( ( ($x-$ox)*($x-$ox) + ($y-$oy)*($y-$oy) ) <= ($r*$r) ){
						imagesetpixel($new_img, $x, $y, $rgb);
					}

					// 下左
					$ox = $r;        // 圆心x坐标
					$oy = $img_h-$r; // 圆心y坐标
					if( ( ($x-$ox)*($x-$ox) + ($y-$oy)*($y-$oy) ) <= ($r*$r) ){
						imagesetpixel($new_img, $x, $y, $rgb);
					}

					// 下右
					$ox = $img_w-$r; // 圆心x坐标
					$oy = $img_h-$r; // 圆心y坐标
					if( ( ($x-$ox)*($x-$ox) + ($y-$oy)*($y-$oy) ) <= ($r*$r) ){
						imagesetpixel($new_img, $x, $y, $rgb);
					}

				}

			}
		}

		return $new_img;
	}

	/**
	 * 合拼图片并保留各自透明度
	 *
	 * @param $dest_img
	 * @param $src_img
	 * @param $pos_x
	 * @param $pos_y
	 * @param $src_x
	 * @param $src_y
	 * @param $src_w
	 * @param $src_h
	 * @param $opacity
	 * @return string
	 */
	private function alpha_merge($dest_img, $src_img, $pos_x, $pos_y, $src_x, $src_y, $src_w, $src_h, $opacity)
	{
		$tmp_img = imagecreatetruecolor($src_w, $src_h);

		imagecopy($tmp_img, $dest_img, 0, 0, $pos_x, $pos_y, $src_w, $src_h);
		imagecopy($tmp_img, $src_img, 0, 0, $src_x, $src_y, $src_w, $src_h);
		imagecopymerge($dest_img, $tmp_img, $pos_x, $pos_y, $src_x, $src_y, $src_w, $src_h, $opacity);

		return $dest_img;
	}

	/**
	 * 获取图片类型
	 *
	 * @param $filename
	 * @return string
	 */
	private function suffix($filename)
	{
		return strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	}

	/**
	 * 十六位颜色代码转化成RGB颜色
	 *
	 * @param $hexcolor
	 * @return array
	 */
	private function hex2rgb($hexcolor)
	{
		$color = str_replace('#', '', $hexcolor);
		if (strlen($color) > 3) {
			$rgb = array(
				'r' => hexdec(substr($color, 0, 2)),
				'g' => hexdec(substr($color, 2, 2)),
				'b' => hexdec(substr($color, 4, 2))
			);
		} else {
			$r = str_repeat(substr($color, 0, 1),1);
			$g = str_repeat(substr($color, 1, 1), 1);
			$b = str_repeat(substr($color, 2, 1), 1);
			$rgb = array(
				'r' => hexdec($r),
				'g' => hexdec($g),
				'b' => hexdec($b)
			);
		}
		return $rgb;
	}
}