<?php

namespace wechat;

use \think\Db;
use \think\Log;

class Fictitious{
    protected $realUrl = 'https://rtcapi.cn-north-1.myhuaweicloud.com:12543/rest/caas/relationnumber/partners/v1.0'; // APP接入地址+接口访问URI
    protected $APP_KEY = 'S5756L4U86S65o62TXh00NjM3V8u'; // APP_Key
    protected $APP_SECRET = 'nC3rlk2bnKV9h8sbB1bb30Bt6C97'; // APP_Secret
    /**
     * 默认绑定有效时间一个月，需要中间解绑数据
     * @return [type] [description]
     */
    public function createphone($relationNum,$callerNum,$calleeNum,$duration=2592000)
    {
        /*
         * 选填,各参数要求请参考"AXB模式绑定接口"
         */
        // $areaCode = '0755'; // 需要绑定的X号码对应的城市码
        // $callDirection = 0; // 允许呼叫的方向
        // $duration = 86400; // 绑定关系保持时间,到期后会被系统自动解除绑定关系
        // $recordFlag = 'false'; // 是否需要针对该绑定关系产生的所有通话录音
        // $recordHintTone = 'recordHintTone.wav'; // 设置录音提示音
        // $maxDuration = 60; // 设置允许单次通话进行的最长时间,通话时间从接通被叫的时刻开始计算
        // $lastMinVoice = 'lastMinVoice.wav'; // 设置通话剩余最后一分钟时的提示音
        // $privateSms = 'true'; // 设置该绑定关系是否支持短信功能

        // $callerHintTone = 'callerHintTone.wav'; // 设置A拨打X号码时的通话前等待音
        // $calleeHintTone = 'calleeHintTone.wav'; // 设置B拨打X号码时的通话前等待音
        // $preVoice = [
        //     'callerHintTone' => $callerHintTone,
        //     'calleeHintTone' => $calleeHintTone
        // ];

        // 请求Headers
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json;charset=UTF-8',
            'Authorization: WSSE realm="SDP",profile="UsernameToken",type="Appkey"',
            'X-WSSE: ' . $this->buildWsseHeader($this->APP_KEY, $this->APP_SECRET)
        ];
        // 请求Body,可按需删除选填参数
        $data = json_encode([
            'relationNum' => '+86'.$relationNum,
            // 'areaCode' => $areaCode,
            'callerNum' => '+86'.$callerNum,
            'calleeNum' => '+86'.$calleeNum,
            // 'callDirection' => $callDirection,
            'duration' => $duration,
            // 'recordFlag' => $recordFlag,
            // 'recordHintTone' => $recordHintTone,
            // 'maxDuration' => $maxDuration,
            // 'lastMinVoice' => $lastMinVoice,
            // 'privateSms' => $privateSms,
            // 'preVoice' => $preVoice
        ]);

        $context_options = [
            'http' => [
                'method' => 'POST', // 请求方法为POST
                'header' => $headers,
                'content' => $data,
                'ignore_errors' => true // 获取错误码,方便调测
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ] // 为防止因HTTPS证书认证失败造成API调用失败,需要先忽略证书信任问题
        ];

        try {
            $file = fopen('bind_data.txt', 'a'); //打开文件
            //print_r($data . PHP_EOL); // 打印请求数据
            fwrite($file, '绑定请求数据：' . $data . PHP_EOL); //绑定请求参数记录到本地文件,方便定位问题
            $response = file_get_contents($this->realUrl, false, stream_context_create($context_options)); // 发送请求
            return json_decode($response,true);

        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function Untying($relationNum){
        // 必填,请参考"开发准备"获取如下数据,替换为实际值

        /*
         * 选填,各参数要求请参考"AXB模式解绑接口"
         * subscriptionId和relationNum为二选一关系,两者都携带时以subscriptionId为准
         */
        $relationNum = '+86'.$relationNum;

// 请求Headers
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json;charset=UTF-8',
            'Authorization: WSSE realm="SDP",profile="UsernameToken",type="Appkey"',
            'X-WSSE: ' . $this->buildWsseHeader($this->APP_KEY, $this->APP_SECRET)
        ];
// 请求URL参数
        $data = http_build_query([
            //'subscriptionId' => $subscriptionId,
            'relationNum' => $relationNum
        ]);
// 完整请求地址
        $fullUrl = $this->realUrl . '?' . $data;

        $context_options = [
            'http' => [
                'method' => 'DELETE', // 请求方法为DELETE
                'header' => $headers,
                'ignore_errors' => true // 获取错误码,方便调测
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ] // 为防止因HTTPS证书认证失败造成API调用失败,需要先忽略证书信任问题
        ];

        try {
            //print_r($data . PHP_EOL); // 打印请求数据
            $response = file_get_contents($fullUrl, false, stream_context_create($context_options)); // 发送请求
            //print_r($response . PHP_EOL); // 打印响应结果
            return json_decode($response,true);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    /**
     * 构建X-WSSE值
     *
     * @param string $appKey
     * @param string $appSecret
     * @return string
     */
    public function buildWsseHeader($appKey, $appSecret) {
        date_default_timezone_set("UTC");
        $Created = date('Y-m-d\TH:i:s\Z'); //Created
        $nonce = uniqid(); //Nonce
        $base64 = base64_encode(hash('sha256', ($nonce . $Created . $appSecret), TRUE)); //PasswordDigest

        return sprintf("UsernameToken Username=\"%s\",PasswordDigest=\"%s\",Nonce=\"%s\",Created=\"%s\"", $appKey, $base64, $nonce, $Created);
    }

}