<?php
namespace wechat;

use think\Request;
/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/19  11:10
 * Desc：自动回复消息
 */
class Receive extends Api
{
	/**@var Receive  对象实例 */
	private static $instance;
	/**
	 *  接收事件推送
	 * @param $object
	 * @return string
	 */
	public static function event($object)
	{
		$object->MsgType = 'text';
		$msg = '';
		switch ($object->Event){
			case 'CLICK':
				switch ($object->EventKey){
					case 'V1001_GOOD':
						$msg = '感谢您的支持！';
						break;
					case 'V1002_OPENID':
						$msg = $object->FromUserName;
						break;
					case 'V1003_TOKEN':
						$msg = self::$instance->access_token;
						break;
					default:
				}
				$object->Event .= '.'.$object->EventKey;
				break;
			case 'VIEW':
				return '';
				break;
			case 'scancode_push':
				break;
			case 'scancode_waitmsg':
				break;
			case 'subscribe':
				// 默认消息
				if(empty($object->EventKey)){
					$msg = config('reply.default_msg');
				}else{
					$url = substr($object->EventKey, strpos($object->EventKey, '_')+1);
					trace("qrscene:");
					trace($url);
					$params = explode('|',$url);
					$msg = self::scan($object, $params);

					//luo.190816
                    self::handle_subscribe($object, $params);
				}
				break;
			case 'SCAN':
				// 默认消息
				$params = explode('|', $object->EventKey);
				$msg = self::scan($object, $params);
				break;
			case 'unsubscribe':
				//取消订阅
				break;
			case 'pic_sysphoto':
				break;
			case 'pic_photo_or_album':
				break;
			case 'pic_weixin':
				break;
			case 'location_select':
				$object->MsgID = $object->SendLocationInfo->Location_X ;
				$object->Status = $object->SendLocationInfo->Location_Y;
				$msg = '经度：'.$object->MsgID.' | 纬度：'.$object->Status;
				break;
			case 'LOCATION': //上报地理位置事件
				$object->MsgID = $object->Latitude;
				$object->Status = $object->Longitude;
			default:
		}
		try{
			model('WechatEvent')->create(
				array(
					'msgid'=>$object->MsgID,
					'openid'=>$object->FromUserName,
					'event_type'=>$object->Event,
					'event_key' => $object->EventKey,
					'status'=>$object->Status,
					'create_time'=>$object->CreateTime
				)
			);
		}catch (\Exception $e){
			Log::error('event：'.$object->Status.'|'.$object->Event);
		}
		return empty($msg) ?: self::compose($object, array('Content' => $msg));
	}

	/**
	 *  接收图片消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function image($object)
	{
		$ary = array(
			'Image'=>self::cdata(['MediaId'=>$object->MsgId])
		);
		return self::compose($object, $ary);
	}

	/**
	 *  接收链接消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function link($object)
	{
		$content = '你发送的是链接，标题为：'.$object->Title.'；内容为：'.
			$object->Description.'；链接地址为：'.$object->Url;
		return self::compose($object, array('Content' => $content));
	}

	/**
	 *  接收地理位置消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function location($object)
	{
		return self::compose($object, array('Content' =>
			'你发送的是位置，经度为：'.$object->Location_Y.
			'；纬度为：'.$object->Location_X.
			'；缩放级别为：'.$object->Scale.'；位置为：'.$object->Label
		));
	}

	/**
	 *  接收视频消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function video($object)
	{
		$ary = array(
			'MediaId' => $object->MediaId,
			'ThumbMediaId' => $object->ThumbMediaId,
			'Title' => '',
			'Description' => ''
		);
		$ary = array(
			'Video' => self::cdata($ary),
		);
		return self::compose($object, $ary);
	}

	/**
	 *  接收语音消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function voice($object)
	{
		return self::compose($object,
			isset($object->Recognition) && !empty($object->Recognition) ?
				'你刚才说的是：'.$object->Recognition:
				array("MediaId"=>$object->MediaId)
			);
	}

	/**
	 *  接收文本消息
	 *
	 * @param $object
	 * @return string
	 */
	public static function text($object)
	{
		$msg = $object->Content;
		switch ($msg){
			case 'openid':
				$msg = $object->FromUserName;
				break;
			default:
				$conf = json_decode(config('reply.reply'), !0);
				if(empty($conf))break;
				//$len = count(self::split($msg));
				if(isset($conf["$msg"])){
					$msg = $conf["$msg"];
					break;
				}
				try {
					$cache = Cache::getInstance();
					if ($cache::get($object->FromUserName)) return '';
					$cache::set($object->FromUserName, 1, 300);
				}catch (\Exception $e){}
				$msg = config('reply.auto');
		}
		return self::compose($object, array('Content' =>$msg));
	}

	public static function split($str)
	{
		return preg_split('/(?<!^)(?!$)/u', $str );
	}

	/**
	 * 转发到在线客服
	 *
	 * @author d3li 12/15/2018
	 * @param $object
	 * @return string
	 */
	public static function waiter($object)
	{
		$object->MsgType = 'transfer_customer_service';
		return self::compose($object);
	}

	/**
	 * 微信开放平台语义理解
	 * @author d3li 12/15/2018
	 * @param $remark
	 * @param $openid
	 * @return string
	 */
	public static function semantic($remark, $openid)
	{
		$cate = array('news');
		strpos($remark, '深圳') && $cate[] = 'nearby';
		strpos($remark, '天气') && $cate[] = 'weather';
		strpos($remark, '游') && $cate[] = 'travel';
		strpos($remark, '车') && $cate[] = 'train';
		strpos($remark, '机') && $cate[] = 'flight';
		strpos($remark, '优惠') && $cate[] = 'coupon';
		$data = sprintf('{"query":"%s","category":"%s","city":"深圳","appid":"%s","uid":"12"}',
			$remark, implode(',', $cate), $openid);
		$res = self::getInstance()->http_post('semantic/semproxy/search?access_token=',$data);
		if(isset($res['errcode'])&&$res['errcode']==0){
			$msg = '';
			array_walk_recursive($res['semantic']['details'], function($v) use (&$msg){
				$msg .= $v;
			});
			return $msg;
		}
		return json_encode(array_values($res));
	}

	/**
	 *  转译xml
	 *
	 * @param array $ary
	 * @param string $item
	 * @return string
	 */
	public static function cdata($ary, $item = 'item')
	{
		$xml = '';
		foreach ($ary as $key => $value) {
			is_numeric($key) && $key = $item;
			$xml .= "\n<{$key}>";
			$xml .= is_numeric($value) ? $value . "</{$key}>":"<![CDATA[{$value}]]></{$key}>";
		}
		return $xml;
	}

	private static function compose($object, $cont = array())
	{
		$ary = array(
			'ToUserName' => $object->FromUserName,
			'FromUserName' => $object->ToUserName,
			'CreateTime' => $_SERVER['REQUEST_TIME'],
			'MsgType' => $object->MsgType,
		);
		if(!empty($cont)){
			$ary = array_merge($ary, $cont);
		}
		$result = Helper::array2xml($ary);
		if (input('encrypt_type') == 'aes') {
			$msg = '';
			Encoder::encryptMsg($result, input('timestamp'), input('nonce'), $msg);
			$result = $msg;
		}
		return $result;
	}

	/**
	 * 扫描带参数二维码事件
	 *
	 * @author d3li 17/04/2019
	 * @param $object
	 * @param array $params 所传参数
	 * @return string
	 */
	private static function scan($object, $params)
	{
		$msg = config('reply.default_msg');
		try {
			$model = model('WechatInvite');
			$row = $model->get(['openid'=>$object->FromUserName]);
			if(!$row){
				$uid = intval($params[1]) ?: intval(end($params));
				$model->create(
					array(
						'uid' => $uid,
						'source' => $params[0],
						'openid' => $object->FromUserName,
						'scene' => $object->EventKey,
						'ticket' => $object->Ticket,
					)
				);
			}
			switch ($params[0]){
				case 'platform':
					model('Platform')
						->where('id', $params[1])
						->setInc('scan');
					return $msg;
				case 'goods':
//					$msg .= "\n\n您要的商品链接：\n";
					$params[0] = Request::instance()->domain();
					$sid= end($params);
					array_splice($params, 4, 0,  ['event','storescan']);
					$params[] = 'mstore_id';
					$params[] = $sid;
					$url = implode('/', $params);
					if(strpos($msg,'http')) {
						return preg_replace('/http[^\"]*/', $url, $msg);
					}
					return  sprintf('%s<a href="%s">%s</a>', $msg."\n", $url, $url);
				case 'goodsbase':
//					$msg .= "\n\n您要的商品链接：\n";
					$params[0] = Request::instance()->domain();
					$sid= end($params);
					array_splice($params, 1, 0,  ['mobile','goods','goods_detail','event','storescan','s','1']);
					$params[] = 'mstore_id';
					$params[] = $sid;
					$url = implode('/', $params);
					if(strpos($msg,'http')) {
						return preg_replace('/http[^\"]*/', $url, $msg);
					}
					return  sprintf('%s<a href="%s">%s</a>', $msg."\n", $url, $url);
				case 'fans':
				case 'store':
				case 'user':
//					$msg .= "\n\n请点击链接：\n";
					$ary = array_slice($params, 1);
					array_unshift($ary,  Request::instance()->domain());
					$url = implode('/', $ary);

					if(strpos($msg,'http:')) {
						return preg_replace('/http\:[^\"]*/', $url, $msg);
					}
					return  sprintf('%s<a href="%s">%s</a>', $msg, $url, $url);
				default:
			}
		}catch (\Exception $e){}
		return implode('/', $params);
	}

    //luo.190816
    private static function handle_subscribe($object, $params)
    {
        try {
            if (empty($object) || empty($params)) {
                return;
            }
            $openid = $object->FromUserName;
            $msg_time = $object->CreateTime;
            $msg_type = $object->MsgType;
            $event = $object->Event;
            $event_key = $object->EventKey;
            $ticket = $object->Ticket;
            $model1 = model('WechatEventRecords');
            $model1->save(
                array(
                    'openid' => $openid,
                    'msg_time' => $msg_time,
                    'msg_type' => $msg_type,
                    'event' => $event,
                    'event_key' => $event_key,
                    'ticket' => $ticket
                )
            );
            $recordid = $model1->id;
            $head = $params[0];
            switch ($head) {
                case 'goodsbase':
                    if ($params[1] == 'id') {
                        $goodid = $params[2];
                    }
                    if ($params[3] == 'to') {
                        $userid = $params[4];
                    }
                    if ($params[5] == 'store_id') {
                        $storeid = $params[6];
                    }
                    $model2 = model('WechatUserRelation');
                    $model2->save(
                        array(
                            'recordid' => $recordid,
                            'openid' => $openid,
                            'head' => $head,
                            'goodid' => $goodid,
                            'userid' => $userid,
                            'storeid' => $storeid
                        )
                    );
                    break;
            }
        } catch (\Exception $e) {
            trace('handle_subscribe e:');
            trace($e);
        }
    }
}