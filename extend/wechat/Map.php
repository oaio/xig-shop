<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/29
 * Time: 17:12
 */

namespace wechat;

/**
 * Created by PhpStorm.
 * User: baohb
 * Date: 2018/12/29
 * Time: 15:55
 */

class Map
{
    /**
     * 腾讯地图坐标转百度地图坐标
     * @param [String] $lat 腾讯地图坐标的纬度
     * @param [String] $lng 腾讯地图坐标的经度
     * @return [Array] 返回记录纬度经度的数组
     */

    function Convert_GCJ02_To_BD09($lat, $lng)
    {

        $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
        $x = $lng;
        $y = $lat;
        $z = sqrt($x * $x + $y * $y) + 0.00002 * sin($y * $x_pi);
        $theta = atan2($y, $x) + 0.000003 * cos($x * $x_pi);
        $lng = $z * cos($theta) + 0.0065;
        $lat = $z * sin($theta) + 0.006;

        return array('lng' => $lng, 'lat' => $lat);
    }


    //百度地图坐标计算
//    function rad($d)
//    {
//        return $d * 3.1415926535898 / 180.0;
//    }


    /**
     * 腾讯地图坐标转百度地图坐标
     * @param [String] $lat1 A点的纬度
     * @param [String] $lng1 A点的经度
     * @param [String] $lat2 B点的纬度
     * @param [String] $lng2 B点的经度
     * @return [String] 两点坐标间的距离，输出单位为米
     */

//    function GetDistance($lat1, $lng1, $lat2, $lng2)
//    {
//        $EARTH_RADIUS = 6378.137;//地球的半径
////        $radLat1 = rad($lat1);
////        $radLat2 = rad($lat2);
//        $radLat1 = $lat1* 3.1415926535898 / 180.0;
//        $radLat2 = $lat2* 3.1415926535898 / 180.0;
//        $radLng1 = $lng1* 3.1415926535898 / 180.0;
//        $radLng2 = $lng2* 3.1415926535898 / 180.0;
//        $a = $radLat1 - $radLat2;
//        $b = $radLng1 - $radLng2;
//        $s = 2 * asin(sqrt(pow(sin($a / 2), 2) +
//                cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
//        $s = $s * $EARTH_RADIUS;
//        $s = round($s * 10000) / 10000;
//        $s = $s * 1000;
//        return ceil($s);
//    }


    function getDistance($longitude,$latitude,$longitude2,$latitude2,  $unit=2, $decimal=2){

        $EARTH_RADIUS = 6370.996; // 地球半径系数
        $PI = 3.1415926;

        $radLat1 = $latitude * $PI / 180.0;
        $radLat2 = $latitude2 * $PI / 180.0;

        $radLng1 = $longitude * $PI / 180.0;
        $radLng2 = $longitude2 * $PI /180.0;

        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;

        $distance = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
        $distance = $distance * $EARTH_RADIUS * 1000;

        if($unit==2){
            $distance = $distance / 1000;
        }

        return round($distance, $decimal);

    }


    /**
     * 标记大概的距离，做出友好的距离提示
     * @param [$number] 距离数量
     * @return[String] 距离提示
     */
    function mToKm($number)
    {

        if (!is_numeric($number)) return ' ';
        switch ($number) {
            case $number > 1800 && $number <= 2000:
                $v = '2';
                break;

            case $number > 1500 && $number <= 1800:
                $v = '1.8';
                break;

            case $number > 1200 && $number <= 1500:
                $v = '1.5';
                break;
            case $number > 1000 && $number <= 1200:
                $v = '1.2';
                break;

            case $number > 900 && $number <= 1000:
                $v = '1';
                break;

            default:

                $v = ceil($number / 100) * 100;
                break;

        }

        if ($v < 100) {
            $v = '距离我【<font color="#FF4C06"><b>' . $v . '</b></font>】千米内。';
        } else {
            $v = '距离我【<font color="#FF4C06"><b>' . $v . '</b></font>】米内。';
        }
        return $v;
    }



}