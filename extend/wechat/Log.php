<?php
namespace wechat;

/**
 * Created by d3li@sina.com
 * User：xiaogebang.com
 * Date：2018/11/9  14:12
 * Desc：支付记录
 */
class Log
{
	/**
	 * @var resource $handler
	 */
	public  static $handler = null;
	private static $level = 15;
	/**
	 * @var Log $instance
	 */
	private static $instance = null;

	public function __construct($file = '')
	{

	}

	public static function init($file = '',$level = 15)
	{
		if(!self::$instance instanceof self) {
			self::$instance = new self();
			self::$instance->__setLevel($level);
			self::$instance->__setHandle($file);
		}
		return self::$instance;
	}

	public static function set($file)
	{
		self::$handler = fopen($file,'a');
	}

	private function __setHandle($file)
	{
		self::$handler = fopen($file,'a');
	}

	private function __setLevel($level)
	{
		self::$level = $level;
	}

	public static function debug($msg)
	{
		self::$instance->write(1, $msg);
	}

	public static function info($msg)
	{
		self::$instance->write(2, $msg);
	}

	public static function warn($msg)
	{
		self::$instance->write(4, $msg);
	}

	public static function error($msg)
	{
		$debugInfo = debug_backtrace();
		$stack = "[";
		foreach($debugInfo as $key => $val){
			if(array_key_exists("file", $val)){
				$stack .= ",file:" . $val["file"];
			}
			if(array_key_exists("line", $val)){
				$stack .= ",line:" . $val["line"];
			}
			if(array_key_exists("function", $val)){
				$stack .= ",function:" . $val["function"];
			}
		}
		$stack .= "]";
		self::$instance->write(8, $stack);
		self::$instance->write(8, $msg);
	}

	private function __get_level($level)
	{
		switch ($level)
		{
			case 1:
				return 'debug';
				break;
			case 2:
				return 'info';
				break;
			case 4:
				return 'warn';
				break;
			case 8:
				return 'error';
				break;
			default:
		}
		return 'Undefined';
	}

	protected function write($level,$msg)
	{
		if(($level & self::$level) == $level )
		{
			if(is_array($msg) || is_object($msg)) $msg = json_encode($msg,
				JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			$msg = implode('', ['[',date('Y-m-d H:i:s'),'][',
				self::$instance->__get_level($level),'] ',$msg,"\n"]);
			fwrite(self::$handler, $msg, 4096);
		}
	}
	public function __destruct()
	{
		fclose(self::$handler);
	}
}