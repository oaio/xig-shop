<?php

namespace wechat;

use \think\Db;
use \think\Log;

class DiytTemplate{


	protected static $template_id='MlCAx6tzkVg_2FtCryRL_unSH0IvLwA2N8_V7GrHs_0';//使用模板消息

	protected static $link='';

	public function __construct($template_id='', $link='')
	{

		if(!empty($template_id)){
			$this->setTemplate($template_id);
		}
		if(!empty($link)){
			$this->setLink($link);
		}
	}
	/**
	 * 设置使用模板
	 * @Author   ksea
	 * @DateTime 2019-07-07T16:37:33+0800
	 * @param    string                   $template_id [description]
	 */
	public  function setTemplate($template_id='MlCAx6tzkVg_2FtCryRL_unSH0IvLwA2N8_V7GrHs_0'){
		self::$template_id=$template_id;
	}
	/**
	 * 设置跳转链接
	 * @Author   ksea
	 * @DateTime 2019-07-07T16:37:06+0800
	 * @param    [type]                   $link [description]
	 */
	public  function setLink($link){
		self::$link=$link;
	}

	/**
	 * 准备发送模板消息用户列表
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:49:59+0800
	 */
	public static function SendGroup($open_arr='',$data=array(),$link=''){

		$red=self::SendMessage($open_arr,$data,$link);
		return $red;
	}

	/**
	 * 发送抢单模板消息
	 * @Author   ksea
	 * @DateTime 2019-02-27T15:02:47+0800
	 */
	public static function SendMessage($openid_arr,$data='',$link='',$count='',$obj=''){
        $api=new \wechat\Api();
        if(empty($obj)){
        	$obj=$api;
        	$count=count($openid_arr);
        }
        if($count>0){
	        $openid=$openid_arr[$count-1];
	        $template_id=self::$template_id;
	        $link=$link?$link:'';
	        $res=$api->send_template($openid,$template_id,$data,$link);
	        $count-=1;
        	return self::SendMessage($openid_arr,$data,$link,$count,$obj);
        }
        return '处理成功';

	}

}