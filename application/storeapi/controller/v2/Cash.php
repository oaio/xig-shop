<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/1 0001
 * Time: 15:44
 */

namespace app\storeapi\controller\v2;
use think\Db;
use think\Validate;
use think\Log;
use app\mapi\controller\commonapi\Mycommon;
use app\admin\model\Cash as mycash;
use think\Session;

class Cash extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new mycash();

    }
    /***************************************异步操作***************************************/

    /**
     * @param int $length
     * @return string
     * @生成随机数
     */
    public function createNonceStr_q($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
    /**
     *发起提现到零钱
     */
    public function small_change_cash(){
        //提现金额
        $userinfo = new User();
        $users = $userinfo->where('id',Session::get('user.id'))->find();
        $sumwithdraw = $users['sumwithdraw'] - input('param.amount');
        if($sumwithdraw < 0){
            return $this->error('余额不足', '', array('status'=>0));
        }
        Db::startTrans();
        try {
            $data['amount'] = input('param.amount');
            $data['cash_num'] =  uniqid();//$this->createNonceStr_q(32);
            $data['username'] =  Session::get('user.name');
            $data['openid'] =  Session::get('user.openid');
            $data['status'] =  1;
            $data['type'] =  1;
            $data['notes'] =  input('param.notes');
            $data['userid'] =  Session::get('user.id');
            $insert=$this->model->insertGetId($data);
            $user = new User();
            $user->where('id',$data['userid'])->update(['sumwithdraw'=>$sumwithdraw]);
            Db::commit();
            return $this->success('申请成功', config('mobileUrl.WithdrawApp'), array('status'=>1));
        }catch (\Exception $e) {
            $res['code']=0;
            $res['msg']='申请失败,请稍后重试';
            Db::rollback();
            return $this->error('申请失败', '', array('status'=>0));
        }
    }
    /**
     * 获取提现数据
     * @Author   ksea
     * @DateTime 2019-05-30T09:21:48+0800
     * @param    string                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function getCash($uid='',$limit='',$getTime='',$search='',$other=array()){
        
        $res=$this->model->getCash($uid,$limit,$getTime,$search,$other);
        if(empty($res)){
            return $this->error('没有数据','',$res);
        }
        else{
            return $this->success('获取成功','',$res);
        } 
    }

    /**
     * 已提额度
     * @Author   ksea
     * @DateTime 2019-05-30T10:56:09+0800
     * @param    string                   $uid [description]
     * @return   [type]                        [description]
     */
    public function sumcash($uid=''){
        $where['userid']=$uid;
        $where['origin']=2;
        $res=$this->model->where($where)->sum("costs");
        return $this->success('获取成功','/',$res);
    }


}
