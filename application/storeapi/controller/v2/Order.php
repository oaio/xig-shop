<?php

namespace app\storeapi\controller\v2;

use think\Db;
use think\controller\Rest;
use app\admin\model\shop\Order as StoreOrder;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;

/**
 * 店铺订单数据
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */

class Order extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new StoreOrder();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                break;
            case 'get':
                $admin_id=input('param.admin_id','');
                $btime=input('param.btime','0');
                $limit=input('param.limit','');
                $getTime=input('param.etime','');
                $getTime=strtotime($getTime);
                $search=input('param.search','');
                $other=input('param.other/a');
                return $this->StoreGetOrder($admin_id,$btime,$limit,$getTime,$search,$other);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/


    /**
     * 获取服务者
     * @Author   ksea
     * @DateTime 2019-03-28T20:38:32+0800
     * @param    [type]                   $uid     [description]
     * @param    [type]                   $fansid  [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    integer                  $status  [description]
     * @return   [type]                            [description]
     */
    protected function StoreGetOrder($admin_id,$btime,$limit,$getTime,$search,$other){
            
            $validate=new Validate([
                'admin_id' => 'require',
            ]);
            if(!$validate->check(['admin_id'=>$admin_id])){
                return $this->error($validate->getError());
            }

            $res=$this->model->StoreGetOrder($admin_id,$btime,$limit,$getTime,$search,$other);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 

    }
    /**
     * 累积
     * @Author   ksea
     * @DateTime 2019-05-29T14:56:07+0800
     * @param    string                   $admin_id [description]
     * @param    string                   $btime    [description]
     * @param    string                   $etime    [description]
     * @return   [type]                             [description]
     */
    public function sumorder($admin_id='',$btime='',$etime=''){
        $query='';
        $etime=empty($etime)?time():strtotime($etime);
        if(empty($btime)){
            $query='a.create_time <'.$etime;
        }
        else{
            $btime=strtotime($btime);
            $getTime=$etime;
            $query="a.create_time BETWEEN $btime  AND  $getTime";
        }

        $admin_id=empty($admin_id)?session('admin.id'):$admin_id;
        $data=$this->model
        	  ->alias('a')
        	  ->join('fa_virdiscount discount','`a`.`discount_id`=`discount`.`id`','LEFT')
        	  ->where(['admin_id'=>$admin_id,'a.is_pay'=>1])
        	  ->where($query)
        	  ->field('SUM(`a`.`actualpay`) AS sumactualpay,SUM(`discount`.`dismoney`) AS sumdismoney,`a`.`create_time`,`a`.`discount_id`,`a`.`is_pay` , `discount`.`id` as dis_id ,`discount`.`dismoney`')
        	  ->find();
        if($data['sumactualpay']==null){
            $data['sumactualpay']=0;
        }
        if(empty($data)){
            return $this->error('没有数据','',$data);
        }
        else{
            $data=$data->toArray();
            if(!$data['sumdismoney']){
                $data['sumdismoney']=0;
            }
            $data['sumactualpay']=round(($data['sumactualpay']+ $data['sumdismoney']),2);
            return $this->success('获取成功','',$data);
        } 
    }

    /***************************************异步操作***************************************/
}