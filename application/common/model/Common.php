<?php
namespace app\common\model;

use think\Model;

class Common Extends Model
{
  /**
   * 解析数据对象
   * @param  [type]  $arrobj    [description]
   * @param  integer $now_index [description]
   * @param  array   $now_data  [description]
   * @return [type]             [description]
   */
  public function getdataAll($arrobj,$now_index=0,$now_data=array()){
     if(is_array($arrobj)&&count($arrobj)>0){
       if(is_object($arrobj[$now_index])){
            $add_arr=$arrobj[$now_index]->getData();
            array_push($now_data, $add_arr);
       }
       $now_index++;
       if($now_index>=count($arrobj)){
            return $now_data;
       }
       else{
            return $this->getdataAll($arrobj,$now_index,$now_data);
       }
     }
     else{
          return null;
     }
  }
}