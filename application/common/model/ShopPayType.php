<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
/**
 * ShopPayType.php
 *
 * @author d3li <d3li@sina.com>
 * @create：20/03/2019  2:20 PM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.20
 * @describe
 */
class ShopPayType extends Model
{

}