<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
class WechatPay extends Model
{
	// 自动写入时间戳字段
	protected $autoWriteTimestamp = 'int';
	// 定义时间戳字段名
	protected $createTime = 'createtime';
	protected $updateTime = ''; //paytime

	public function getNoteAttr($value)
	{
		return urldecode($value);
	}

	public function pay()
	{
		return $this->hasOne('ShopPayType','id', 'attach')->where('key', 'wechat');
	}

	public function info()
	{
		return $this->hasOne('app\admin\model\ShopUserinfo', 'openid', 'openid');
	}

	public function orders()
	{
		return $this->hasOne('app\admin\model\shop\Order', 'id', 'oid');
	}
}