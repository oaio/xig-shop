<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
/**
 * WechatEvent.php
 *
 * @author d3li <d3li@sina.com>
 * @create：08/04/2019  2:15 PM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.08
 * @describe
 */
class WechatEvent extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}