<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;
use think\Model;

/**
 * Platform.php 平台推荐
 *
 * @author d3li <d3li@sina.com>
 * @create：05/05/2019  2:05 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.05
 * @describe
 */
class Platform extends Model
{

}