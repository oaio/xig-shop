<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
/**
 * SecurityLog.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/6/21  10:39
 * @see      https://gitee.com/d3li/
 * @version 2.06.21
 * @describe
 */
class SecurityLog extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}