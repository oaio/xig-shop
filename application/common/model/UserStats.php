<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
/**
 * UserStats.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/6/29  13:39
 * @see      https://gitee.com/d3li/
 * @version 2.06.29
 * @describe
 */
class UserStats extends Model
{
	/**
	 * 批量更新数据
	 *
	 * @param array $data
	 * @param int $time
	 * @return $this|false|int
	 * @throws \think\exception\DbException
	 */
	public function add($data = [], $time = 0)
	{
		$zero = strtotime($time ? date('Ymd', $time) : date('Ymd'));
		$row = $this->get(['create_time' => $zero]);
		if($row){
			foreach($data as $k=>&$v){
				isset($row[$k]) && $v -= -($row[$k]);
			}
			return $this->update($data, ['id' => $row['id']]);
		}
		$data['date'] = date('m-d', $zero);
		$data['create_time'] = $zero;
		return $this->save($data);
	}

}