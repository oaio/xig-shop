<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\model;

use think\Model;
/**
 * WechatInvite.php
 *
 * @author d3li <d3li@sina.com>
 * @create：17/04/2019  3:28 PM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.17
 * @describe
 */
class WechatInvite extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}