<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\common\controller;
/**
 * Partner.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/6/21  22:06
 * @see      https://gitee.com/d3li
 * @version 2.06.21
 * @describe
 */
class Partner extends Backend
{
	protected $dataLimitField = 'city_code';

	protected function getDataLimitAdminIds()
	{
		if(2 > $this->auth->city_code) return 0;
		return [$this->auth->city_code];
	}

}