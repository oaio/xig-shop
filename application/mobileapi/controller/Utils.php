<?php

namespace app\mobileapi\controller;


use app\mapi\controller\commonapi\Mycommon;

header('Access-Control-Allow-Origin:*');
// 响应类型
header('Access-Control-Allow-Methods:*');
// 响应头设置
header('Access-Control-Allow-Headers:x-requested-with,Authorization,content-type');


class Utils extends Mycommon
{

    //根据腾讯GPS信息获取位置信息
    public function get_place_info_from_tx()
    {
        trace("get_place_info_from_tx start");
        $lng = input("param.lng");
        $lat = input("param.lat");
        $get_poi = input("param.get_poi");
        $poi_options = input("param.poi_options");
        trace("lng:");
        trace($lng);
        trace("lat:");
        trace($lat);
        trace("get_poi:");
        trace($get_poi);
        trace("poi_options:");
        trace($poi_options);
        if (empty($lng)) {
            return json(['status' => 501, 'message' => '经度为空']);
        }
        if (empty($lat)) {
            return json(['status' => 502, 'message' => '纬度为空']);
        }
        if (empty($get_poi)) {
            $get_poi = 0;
        }
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?location=" . $lat . "," . $lng . "&get_poi={$get_poi}&key=H6OBZ-35X3X-Y4Z4J-TLKLV-3CER7-JUBYP&output=json";
        if (!empty($poi_options)) {
            $url = $url . "&poi_options={$poi_options}";
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        $res = strstr($res, "{");
        trace("get_place_info_from_tx res:");
        trace($res);
        return $res;
    }
}