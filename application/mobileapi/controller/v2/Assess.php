<?php

namespace app\mobileapi\controller\v2;

use think\Db;
use app\mobileapi\common\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\ShopServerAssess as MyAssess;

class Assess extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('user.id');
        parent::__construct();
        $this->model = new MyAssess();

    }

    /**
     * 添加评论
     * @Author   ksea
     * @DateTime 2019-05-25T17:39:11+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function webAdd($uid='',$data=array()){
        
        if(!$uid){
            $uid=$this->uid;
        }
        $data['uid']=$uid;
        $res=$this->model->webAdd($data);
        if(empty($res)){
            $this->error('添加失败','',$res);
        }
        else{
            $this->success('添加成功','',$res);
        } 
    }
    /**
     * 商品查找评价
     * @Author   ksea
     * @DateTime 2019-05-25T17:39:11+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function getGodosAssess($goodsid='',$limit='',$getTime='',$search='',$other=array()){
        
        $other['status']=1;
        $res=$this->model->getGodosAssess($goodsid,$limit,$getTime,$search,$other);
        if(empty($res)){
            $this->error('没有数据','',$res);
        }
        else{
            $this->success('获取成功','',$res);
        } 

    }

    /**
     * 商品查找评价
     * @Author   ksea
     * @DateTime 2019-05-25T17:39:11+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function getGodosAssessMix($limit='',$getTime=''){
        
        $res=$this->model->getGodosAssessMix($limit,$getTime);
        
        if(empty($res)){
            $this->error('没有数据','',$res);
        }
        else{
            $this->success('获取成功','',$res);
        } 

    }

    /**
     * 商品针对平均分
     * @param  [type] $goodsid [description]
     * @return [type]          [description]
     */
    public function getAvgAssess($goodsid){
        $where['goodsid'] = $goodsid;
        $where['status']  = 1;
        $res=$this->model->field("ROUND(AVG(avg_assess),2) as avg_s")->where($where)->select();
        if(!$res[0]['avg_s']){
            $res[0]['avg_s']="5.0";
        }
        $this->success('获取成功','/',$res);
    }
}