<?php

namespace app\mobileapi\controller\v2;

use think\Db;
use app\mobileapi\common\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\CashBack;

class Back extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('user.id');
        $this->model=new CashBack();
        parent::__construct();

    }

    /**
     * 返利计算接口
     * @Author   ksea
     * @DateTime 2019-07-23T14:43:03+0800
     * @param    [type]                   $oid [description]
     * @param    string                   $uid [description]
     */
    public function GetBack($oid,$uid=''){


        /************************基础校验**********************************/

        $uid=isset($uid)?$uid:$this->uid;

        $rule=[
            'oid'     =>'require',
            'uid'     =>'require',
        ];

        $data=[
            'oid'     =>$oid,
            'uid'     =>$uid,
        ];    

        $valid=new Validate($rule);

        if(!$valid->check($data)){
            $this->error($valid->getError());
        }

        /************************基础校验**********************************/
        
        $ret=$this->model->GetBack($oid,$uid);

        if($ret['code']==1){
            $this->success($ret['msg'],'',$ret['data']);
        }
        else{
            $this->error($ret['msg'],'',$ret['data']);
        }

    }

}