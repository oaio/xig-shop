<?php

namespace app\mobileapi\controller\v2;

use think\Db;
use app\mobileapi\common\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Virdiscount as Mydiscount;

class Virdiscount extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('user.id');
        parent::__construct();
        $this->model = new Mydiscount();

    }

    /**
     * 订单追加抵扣实例化(模拟下单逻辑)
     * @Author   ksea
     * @DateTime 2019-08-06T18:37:09+0800
     * @param    [type]                   $userpaym [description]
     * @param    [type]                   $orderid  [description]
     * @param    string                   $uid      [description]
     * @param    string                   $name     [description]
     */
    public function AppendDis($userpaym,$orderid,$bean='',$uid='',$name='',$note=''){
        
        if(!$uid){
            $uid=$this->uid;
        }
        
        if(!empty($bean)){

            $name=empty($name)?'订单追加小哥豆转换':$name;
            $beanData=model('admin/shop/BeanRule')->disMoney($userpaym,$uid,$orderid);
            
            if($beanData['code']==0 && $beanData['data']>0){
                $this->error($beanData['msg'],'',$beanData['data']);
            }
            
            $bean=$beanData['data'];

            $rDis=$this->model->addDis($bean,$uid,$name);
            
            if(empty($rDis)){
                $this->error('实例化失败','',$rDis);
            }
            else{
                
                $SaveData=[
                    'uid'         =>$uid,
                    'money'       =>$userpaym,
                    'discount'    =>round(($userpaym-$rDis['dismoney']),2),
                    'discount_id' =>$rDis['data'],
                    'orderid'     =>$orderid,
                    'note'        =>$note,
                    'is_pay'      =>0,
                ];

                $res=model('admin/shop/UserWantPay')->allowField(true)->create($SaveData);
                $this->success('实例化成功','',$res);
            } 

        }
        else{
                $SaveData=[
                    'uid'         =>$uid,
                    'money'       =>$userpaym,
                    'discount'    =>0,
                    'discount_id' =>0,
                    'orderid'     =>$orderid,
                    'note'        =>$note,
                    'is_pay'      =>0,
                ];

                $res=model('admin/shop/UserWantPay')->allowField(true)->create($SaveData);
                $this->success('实例化成功','',$res);
        }

    }
}