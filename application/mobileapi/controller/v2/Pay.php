<?php

namespace app\mobileapi\controller\v2;

use app\mobileapi\common\Mycommon;
use think\Cache;
use think\Hook;

class Pay extends Mycommon {

	/**
	 * 订单失效检测
	 * @Author   ksea
	 * @DateTime 2019-06-24T20:15:59+0800
	 * @return   [type]                   [description]
	 */
	public function ckorder($actionname) {
		$res = Cache::get($actionname);
		if ($res) {
			$endtime = $res['actiontime'] - time() - 60;
			if ($endtime > 0) {
				$this->success('剩余还剩', '/', $endtime);
			} else {
				$this->error('订单已关闭', '/', 0);
			}
		} else {
			$this->error('订单已关闭', '/', 0);
		}
	}
	/**
	 * 零元支付(兑换专用)
	 * @Author   ksea
	 * @DateTime 2019-07-16T22:30:37+0800
	 * @param    [type]                   $oid [description]
	 * @return   [type]                        [description]
	 */
	public function zeropayBargain($oid) {
		//基础数据
		$order = model('admin/shop/order')->get($oid);
		$MFreeRoll = model('admin/shop/FreeRoll');
		$uid = session('user.id');

		/******************订单校验*********************/

		//防止重复支付
		if ($order['is_pay'] == 1) {
			$this->error('请勿重复支付');
		}

		//用户对应
		if ($order['uid'] != $uid) {
			$this->error('请用当前账号支付');
		}

		//订单类型对应
		$strArr = explode('_', $order['goods_origin']);

		//所需免单卷
		$needfree = $strArr[count($strArr) - 1];
		unset($strArr[count($strArr) - 1]);
		$strArrTemp = implode('_', $strArr);
		if ($strArrTemp != 'shop_goods_bargain') {
			$this->error('普通商品不允许兑换支付');
		}

		//商品免单卷校对
		if ($needfree <= 0 || empty($needfree)) {
			$this->error('商品配置错误');
		}

		//使用免单卷校对
		$whereFree['status'] = ['lt', 1];
		$whereFree['validtime'] = ['egt', time()];
		$whereFree['uid'] = $uid;
		$FreeRoll = $MFreeRoll->where($whereFree)->order('validtime')->select();
		$ArrFreeRoll = json_decode(json_encode($FreeRoll), true); //对象数组化php7.0~7.2兼容
		if (count($ArrFreeRoll) < $needfree) {
			$this->error('免单卷不足以支付该商品');
		}

		/******************订单校验*********************/

		/*******************商品数据修改********************/

		model('admin/shop/goods')->where('id', $order['goodsid'])->setInc('deal_times'); //真实数量

		model('admin/shop/goods')->where('id', $order['goodsid'])->setInc('virtual_times'); //虚假数量

		/*******************商品数据修改********************/

		/******************免单卷准备扣除数据********************/

		$deduct = [];
		$k = 0;
		foreach ($FreeRoll as $key => $value) {
			if ($k >= $needfree) {
				break;
			}

			array_push($deduct, $value->id);
			$k++;
		}
		$deduct = implode(',', $deduct);

		/******************免单卷准备扣除数据********************/

		$data = [
			'id' => $order['id'],
			'status' => '1',
			'is_pay' => '1',
			'pay_id' => '00',
		];
		$res = model('admin/shop/order')->update_order($data);
		$Cachedata = model('admin/shop/OrderTemp')->where('orderid', $order['id'])->find();
		$cares = controller('api/commonapi/Scan')->CreateScanCode($Cachedata['orderid'], $Cachedata['appoint_start'], $Cachedata['sfuserid'], $Cachedata['areaid'], $Cachedata['address'], $Cachedata['note']);
		if ($cares && isset($cares['msg']) && $cares['msg'] == '预约成功' && $cares['code'] == 1) {
			model('admin/shop/OrderTemp')->where('orderid', $Cachedata['orderid'])->update(['status' => 1]);

		} else {

			$dataMsg = [
				'code' => 0,
				'data' => $Cachedata,
				'url' => '',
				'msg' => '预约失败',
				'wait' => 3,
			];
			//错误记录
			myErrorLog('bargainpay.txt', '发送数据' . json_encode($Cachedata), 'bargainpay');
			myErrorLog('bargainpay.txt', '返回数据' . json_encode($cares), 'bargainpay');

			return json($dataMsg);

		}

		//免单卷扣除
		$MFreeRoll->where(['id' => ['in', $deduct]])->update(['status' => 1]);

		$dataMsg = [
			'code' => 1,
			'data' => $order,
			'url' => '',
			'msg' => '支付成功',
			'wait' => 3,
		];

        /*******************零元支付成功锚点***********************/
        Hook::listen('zeropay',$order);
        /*******************零元支付成功锚点***********************/

		return json($dataMsg);
	}

}