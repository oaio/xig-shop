<?php

namespace app\mobileapi\controller\v2;

use think\Db;
use app\mobileapi\common\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as MyServer;

class Server extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('user.id');
        parent::__construct();
        $this->model = new MyServer();

    }

    /**
     * 预约状态查询
     * @Author   ksea
     * @DateTime 2019-05-25T17:39:11+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function getMserverStatus($uid='',$limit='',$getTime='',$search='',$other=array()){
        
        if(!$uid){
            $uid=$this->uid;
        }

        $res=$this->model->getMserverStatus($uid,$limit,$getTime,$search,$other);
        if(empty($res)){
            $this->error('没有数据','',$res);
        }
        else{
            $this->success('获取成功','',$res);
        } 

    }
}