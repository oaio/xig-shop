<?php

namespace app\mobileapi\controller\v2;

use think\Db;
use app\mobileapi\common\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\BeanRule;

class Bean extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('user.id');
        $this->model=new BeanRule();
        parent::__construct();

    }

    /**
     * 抵扣小哥豆计算(接口返回处理)(调用)
     * @Author   ksea
     * @DateTime 2019-07-22T14:18:04+0800
     * @param    [type]                   $goodsid [商品id]
     * @param    [type]                   $num     [数量]
     * @param    string                   $uid     [description]
     */
    public function GetBean($goodsid='',$num='',$uid=''){

        /************************基础校验**********************************/

        $uid=isset($uid)?$uid:$this->uid;

        $rule=[
            'goodsid' =>'require',
            'num'     =>'require',
            'uid'     =>'require',
        ];

        $data=[
            'goodsid' =>$goodsid,
            'num'     =>$num,
            'uid'     =>$uid,
        ];    

        $valid=new Validate($rule);

        if(!$valid->check($data)){
            $this->error($valid->getError());
        }

        /************************基础校验**********************************/
        
        $ret=$this->model->GetBean($goodsid,$num,$uid);

        if($ret['code']==1){
            $this->success($ret['msg'],'',$ret['data']);
        }
        else{
            $this->error($ret['msg'],'',$ret['data']);
        }

    }

    /**
     * 追加抵扣
     * @Author   ksea
     * @DateTime 2019-07-22T14:18:04+0800
     * @param    [type]                   $userpaym [追加金额]
     * @param    [type]                   $uid      [用户id]
     * @param    string                   $orderid  [订单]
     */
    public function disMoney($userpaym='',$uid='',$orderid=''){

        /************************基础校验**********************************/

        $uid=isset($uid)?$uid:$this->uid;

        $rule=[
            'userpaym' =>'require',
            'uid'     =>'require',
            'orderid'     =>'require',
        ];

        $data=[
            'userpaym' =>$userpaym,
            'uid'     =>$uid,
            'orderid'     =>$orderid,
        ];    

        $valid=new Validate($rule);

        if(!$valid->check($data)){
            $this->error($valid->getError());
        }

        /************************基础校验**********************************/
        
        $ret=$this->model->disMoney($userpaym,$uid,$orderid);

        if($ret['code']==1){
            $this->success($ret['msg'],'',$ret['data']);
        }
        else{
            $this->error($ret['msg'],'',$ret['data']);
        }

    }
}