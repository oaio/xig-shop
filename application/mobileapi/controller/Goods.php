<?php
namespace app\mobileapi\controller;
/**
 * Created by PhpStorm.
 * User: jj
 * Date: 2019/7/16
 * Time: 下午1:58
 */

use app\mapi\controller\commonapi\Mycommon;

class Goods extends Mycommon
{

    public function specialGoods(){
        $page = (int)input('param.page',1);
        $page = $page<1?1:$page;
        $limit = 10;
        $offset = ($page-1) * $limit;
        $list['list'] = model('admin/shop/goods')
            ->with(['bargainData','detailData','bargain_data'])
            ->where(['status'=>1,'origin_table'=>'shop_goods_bargain','is_del'=>0])
            ->order('power')
            ->limit($offset, $limit)
            ->select();

        $count = model('admin/shop/goods')
            ->with(['bargainData','detailData','bargain_data'])
            ->where(['status'=>1,'origin_table'=>'shop_goods_bargain','is_del'=>0])
            ->count();
        $list['pages'] = ceil($count/$limit);
        $list['list'] = collection($list['list'])->toArray();
        return $this->success('获取成功','',$list);
    }
}