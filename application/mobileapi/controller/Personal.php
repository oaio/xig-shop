<?php
namespace app\mobileapi\controller;
/**
 * Created by PhpStorm.
 * User: jj
 * Date: 2019/7/16
 * Time: 下午1:58
 */

use app\mapi\controller\commonapi\Mycommon;
class Personal extends Mycommon
{
    private $uid;

    public function __construct()
    {

        parent::__construct();
        if(!session('user')){
            return $this->error('获取成功','User/login');
        }

        $this->uid = intval(session('user.id'));
    }

    /**
     * 个人中心-获取用户免费券列表
     * @return \think\Response
     */
    public function freeCouponList(){
        $page = (int)input('param.page',1);
        $page = $page<1?1:$page;
        $limit = 10;
        $offset = ($page-1) * $limit;
        $list['list'] = model('admin/shop/FreeRoll')
            ->where(['uid'=>$this->uid])
            ->order('status')
            ->limit($offset, $limit)
            ->select();
        $count = model('admin/shop/FreeRoll')
            ->where(['uid'=>$this->uid])
            ->count();
        $list['list'] = collection($list['list'])->toArray();
        if($list['list']){
            foreach ($list['list'] as &$v){
                if($v['validtime']<time()){
                    $v['status'] = 2;
                }
            }
        }
        $list['pages'] = ceil($count/$limit);
        return $this->success('获取成功','',$list);
    }
}