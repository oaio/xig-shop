<?php

namespace app\mobileapi\common;

use think\Controller;
use think\Validate;
use think\Hook;
use think\Db;

class Mycommon extends Controller
{

  public function _initialize(){
    if(!session('user')){
      //$this->error('请先登陆');
    }
  }
  /**
   * 空操作
   * @Author   ksea
   * @DateTime 2018-12-13T14:24:15+0800
   * @param    [type]                   $name [description]
   * @return   [type]                         [description]
   */
  public function _empty($name){
     $this->error('请确认方法是否正确',$name);
  }
}


