<?php

namespace app\shopstore\common;

use think\Controller;
use think\Validate;
use think\Hook;
use think\Db;

class Mycommon extends Controller
{

  public function _initialize(){
  	if(!session('admin')){
      $this->redirect('user/login');
    }
  }
  /**
   * 空操作
   * @Author   ksea
   * @DateTime 2018-12-13T14:24:15+0800
   * @param    [type]                   $name [description]
   * @return   [type]                         [description]
   */
  public function _empty($name){
     $view=request()->controller().'/'.$name;
     return $this->fetch(strtolower($view));
  }
}


