<?php

namespace app\shopstore\controller;

use app\shopstore\common\Mycommon;
use think\Db;
use think\Config;
use think\Hook;
use think\Validate;
use app\admin\model\AdminLog;
use think\Controller;
use think\Session;

class User extends Controller{
	/**
	 * 用户基础统一操作
	 * @Author   ksea
	 * @DateTime 2019-04-29T09:51:07+0800
	 * @return   [type]                   [description]
	 */
    public function _initialize()
    {
        parent::_initialize();
    }

    public function login(){
        if(session('admin')){
            //$this->redirect('/shopstore/order/serial');
        }
        $url = $this->request->get('url', '/shopstore/order/serial');
        if ($this->request->isPost()) {
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            $token = $this->request->post('__token__');
            $rule = [
                'username'  => 'require|length:3,30',
                'password'  => 'require|length:3,30',
            ];
            $data = [
                'username'  => $username,
                'password'  => $password,
                '__token__' => $token,
            ];
            /********************************************/
            $store_data=model('admin/shop/store')->w_store($username);
            if(is_array($store_data)&&!empty($store_data)){
                 if($store_data['is_af']==''){
                    $this->error(lang('af_status_on'));
                 }
                 switch ($store_data['is_af']) {
                     case '0':
                         $this->error(lang('af_status_on'));
                         break;
                     case '1':
                         $this->error(lang('af_status_on'));
                         break;
                     case '2':
                            $url="/shopstore/order/serial";
                         break;
                     case '3':
                            $url="/shopstore/order/serial";
                         break;
                     case '4':
                         $this->error(lang('af_status_off'));
                         break;
                     case '5':
                         $this->error(lang('af_status_off'));
                         break;
                     default:
                         $this->error(lang('af_status'));
                         break;
                 }
            }
            /********************************************/

            $validate = new Validate($rule);
            $result = $validate->check($data);
            if (!$result) {
                $this->error($validate->getError(), $url);
            }

            AdminLog::setTitle(__('Login'));

            $result = model('admin/Admin')->cklogin($username, $password);
            if ($result === true) {
                $bind_store =model('admin/shop/Store')->where('admin_id',session('admin.id'))->find()->toArray();
                $bind_user  =model('admin/shopuser/user')->where('store_bind',$bind_store['store_id'])->find();
                if($bind_user){
                    $bind_user=$bind_user->toArray();
                }
                $this->success(__('Login successful'), $url, ['url' => $url, 'id' => session('admin.id'), 'username' =>session('admin.username'), 'avatar' => session('admin.avatar'),'bind_store'=>$bind_store,'bind_user'=>$bind_user]);
            } else {
                $msg = '';
                $msg = $msg ? $msg : __('Username or password is incorrect');
                $this->error($msg, $url, ['token' => $this->request->token()]);
            }
        }
        $this->assign('__token__',request()->token());

        return $this->fetch();
    }
    public function logout(){
        Session::clear();
        $this->redirect('user/login');
    }
    /**
     * [index description]
     * @return [type] [description]
     */
    public function index(){
        if(!session('admin')){
            $this->redirect('user/login');
        }
        return $this->fetch();
    }


}