<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as MyServer;

/**
 * 
 * 服务中
 * 
 */

class Server extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        parent::__construct();
        $this->model = new MyServer();

        $this->uid=session('aunt.id');
    }
    
    /**
     * 全部预约
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function message($uid='',$cate='',$limit='',$create_time=''){
      
        if(strlen($create_time)<2){
          $create_time=time();
        }

        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        //抢单
        $query .= "( FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET(`areaid`,'$emp->area_id')  AND";

        $query .= " server.create_time+ ".config('Grab.effective')." >=".time().' AND';

        $query .= " LENGTH(server.sfuserid) <= 0";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9) ) OR";

        //待预约
        $query .= "( FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask=0";

        $query .=' AND order.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9) ) OR";

        //待上门
        $query .= "( FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";
        
        $query .= " server.status = 0";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9) ) OR";


        //服务中
        $query .= "( FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";

        $query .= " server.status = 1";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9) ) OR";


        //服务完成
        $query .= "( FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";

        $query .= " server.status = 4";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9) )";


        if(!empty($create_time)){

            $query .=' AND server.create_time < '.$create_time;

        }

        if(strlen($limit)<=0){
             $s_data =  $this->model
                        ->with(['order','code','card'])
                        ->alias('server')
                        ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                        ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                        ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                        ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                        ->join('fa_region region','region.region_id = server.areaid','LEFT')
                        ->where($query)
                        ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                        ->order('id desc')
                        ->limit($limit)
                        ->select();

             $res['url']=config('mobileUrl.evaluate');
             $res['url_out']=config('mobileUrl.details');
             $res['msg']='全部数据';
             $res['count']=count($s_data);
             $res['is_ture']=1;
             $res['create_time']=$create_time;
             $res['data']=$s_data;
        }
        else{

            $limit_a=explode(',', $limit);
            if(count($limit_a)==2){
               $need_count=$limit_a[1];
            }
            else{
                $need_count=$limit_a[0];
            }
            $s_data = $this->model
                     ->with(['order','code','card'])
                     ->alias('server')
                     ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                     ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                     ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                     ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                     ->join('fa_region region','region.region_id = server.areaid','LEFT')
                     ->where($query)
                     ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                     ->order('id desc')
                     ->limit($limit)
                     ->select();

            if($need_count==count($s_data)){
                $res['msg']='数据合法';
                $res['count']=count($s_data);
                $res['is_ture']=1;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
            else{
                $res['msg']='数据缺少';
                $res['count']=count($s_data);
                $res['is_ture']=0;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
        }
        return $this->success('请求成功','/',$res);

    }
}