<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as Server;

/**
 * 
 * 服务中
 * 
 */

class Serverin extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        parent::__construct();
        $this->model = new Server();

        $this->uid=session('aunt.id');
    }
    
    /**
     * 服务信息
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function message($uid='',$cate='',$limit=''){
      
        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";

        $query .= " server.status = 1";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9)";

        $res  =  $this->model
                 ->with(['order','code','card'])
                 ->alias('server')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->where($query)
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                 ->order('fu_start desc')
                 ->limit($limit)
                 ->select();

        return $this->success('请求成功','/',$res);

    }
}