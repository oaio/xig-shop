<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as Server;

/**
 * 
 * 待上门
 * 
 */

class Goup extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        parent::__construct();
        $this->model = new Server();

        $this->uid=session('aunt.id');
    }
    /**
     * 全部
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function all($uid='',$cate='',$limit=''){
      
        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";
        
        $query .= " server.status = 0";

        $query .=' AND server.create_time > 1558631041';
        
        $query .=" AND order.schedule not in(4,9)";

        
        $res  =  $this->model
                 ->with(['order','code','card'])
                 ->alias('server')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->where($query)
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                 ->order('id desc')
                 ->limit($limit)
                 ->select();

        return $this->success('请求成功','/',$res);

    }


    /**
     * 今天
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function today($uid='',$cate='',$limit=''){
        
        $today=strtotime(date('Y-m-d',time()));

        $nex_day=strtotime('1 days',strtotime(date('Y-m-d',time())));

        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";
        
        $query .= " server.status = 0 AND";

        $query .= " server.appoint_start BETWEEN {$today} AND $nex_day AND";

        $query .= " server.appoint_start>".time();

        $query .=" AND order.schedule not in(4,9)";

        $res  =  $this->model
                 ->with(['order','code','card'])
                 ->alias('server')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->where($query)
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                 ->order('id desc')
                 ->limit($limit)
                 ->select();

        return $this->success('请求成功','/',$res);

    }

    /**
     * 超时
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function out($uid='',$cate='',$limit=''){

        $out_time=time();

        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";
        
        $query .= " server.status <=0 AND";

        $query .= " server.appoint_start < ".time();

        $query .=" AND order.schedule not in(4,9)";

        $res  =  $this->model
                 ->with(['order','code','card'])
                 ->alias('server')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->where($query)
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                 ->order('id desc')
                 ->limit($limit)
                 ->select();

        return $this->success('请求成功','/',$res);

    }
}