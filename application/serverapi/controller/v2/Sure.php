<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as Server;
use Redis;
use wechat\Fictitious;


/**
 * 
 * 待确认类
 * 
 */

class Sure extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        parent::__construct();
        $this->model = new Server();

        $this->uid=session('aunt.id');
    }
    /**
     * 待抢单
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function grab($uid='',$cate='',$limit='',$create_time=''){
      
        if(strlen($create_time)<2){
          $create_time=time();
        }

        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET(`areaid`,'$emp->area_id')  AND";

        $query .= " server.create_time+ ".config('Grab.effective')." >=".time().' AND';

        $query .= " LENGTH(server.sfuserid) <= 0";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9)";

        if(!empty($create_time)){

            $query .=' AND server.create_time < '.$create_time;

        }

        if(strlen($limit)<=0){
             $s_data =  $this->model
                        ->with(['order','code','card','temp'])
                        ->alias('server')
                        ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                        ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                        ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                        ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                        ->join('fa_region region','region.region_id = server.areaid','LEFT')
                        ->where($query)
                        ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                        ->order('id desc')
                        ->limit($limit)
                        ->select();
             $res['url']=config('mobileUrl.evaluate');
             $res['url_out']=config('mobileUrl.details');
             $res['msg']='全部数据';
             $res['count']=count($s_data);
             $res['is_ture']=1;
             $res['create_time']=$create_time;
             $res['data']=$s_data;
        }
        else{

            $limit_a=explode(',', $limit);
            if(count($limit_a)==2){
               $need_count=$limit_a[1];
            }
            else{
                $need_count=$limit_a[0];
            }
            $s_data = $this->model
                     ->with(['order','code','card','temp'])
                     ->alias('server')
                     ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                     ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                     ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                     ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                     ->join('fa_region region','region.region_id = server.areaid','LEFT')
                     ->where($query)
                     ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                     ->order('id desc')
                     ->limit($limit)
                     ->select();
            if($need_count==count($s_data)){
                $res['msg']='数据合法';
                $res['count']=count($s_data);
                $res['is_ture']=1;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
            else{
                $res['msg']='数据缺少';
                $res['count']=count($s_data);
                $res['is_ture']=0;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
        }
        return $this->success('请求成功','/',$res);

    }

    /**
     * 确认抢单
     * @Author   ksea
     * @DateTime 2019-05-14T15:57:17+0800
     * @param    string                   $uid [description]
     * @return   [type]                        [description]
     */
    public function sure_grab($severid,$uid=''){

        /***************2019-05-16**********************/
        $orderid=is_get_val('shop_service_order',$severid,'orderid');
        Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>2]);
        /***************2019-05-16**********************/

        if(is_null($severid)){
            return $this->success('failure','','缺少参数');
        }
        $status_ser = Db::name('shop_service_order')->where(['id'=> $severid,'sfuserid'=>''])->find();

        /************扭转记录********************/
        model('admin/shop/ShopOrderHistory')::create([
           'oid'=>$orderid,
           'desc' => session('employee.name').' 抢单'
        ]);
        /************扭转记录********************/
        if(is_null($status_ser)){
            return $this->error('failure','',array('没有可以抢的单'));
        }
        $userid =  Db::name('employee')->alias('a')
            ->where(['a.id'=>$uid])
            ->find();

        if(is_null($userid)){
            return $this->success('failure','','用户不存在');
        }
       // $order = new Grab();
        //把抢单的用户加入到队列 pconnect
        $redis = new Redis();
        $redis->connect('127.0.0.1',6379);
        //判断这个订单是否被抢
        //if($redis->sCard($severid) == 0) {
            $redis->sAdd($severid, $userid['id']);
            $redis->EXPIRE($severid, 30);
            $phone = new Fictitious();
            $order=Db::name('shop_order')->where(['id'=> $status_ser['orderid']])->find();
            $mobile = $order['mobile'];

            $rivacy_phone = getFictitious();//获取有效虚拟号

            //根据虚拟号是否存在修改判定
            if(is_array($rivacy_phone)){
                if(!is_null($mobile) && !is_null($rivacy_phone)) {
                    $phone_resul = $phone->createphone($rivacy_phone['phone'], $mobile, $userid['phone']);
                    if( $phone_resul['resultdesc'] == 'Success') {
                        Db::name('rivacy_phone')->where('id', $rivacy_phone['id'])->update(['Binding_time' => time()]);
                        Db::name('rivacy_phone_record')->insert(['callerNum'=>$mobile,'calleeNum'=>$userid['phone'],'relationNum'=>$rivacy_phone['phone'],'Binding_time' => time(),'subscriptionId'=>$phone_resul['subscriptionId']]);
                    }
                }
                $service =  Db::name('shop_service_order')->where(['id'=>$severid,'sfuserid'=>''])->update(['push_time'=>time(),'sfuserid'=>$userid['id'],'mobile'=>$rivacy_phone['phone'],'mobile_status'=>config('Fictitious.mobile_status')]);
            }
            else{
                
                myErrorLog('rivacy.txt',json_encode($rivacy_phone),'fictitious');
                $service =  Db::name('shop_service_order')->where(['id'=>$severid,'sfuserid'=>''])->update(['push_time'=>time(),'sfuserid'=>$userid['id'],'mobile'=>0,'mobile_status'=>0]);

            }

            if($service > 0){
                $status = 'success';
            }else{
                $status = 'failure';
            }
            $ret['row'] = $service;
            $ret['url'] = '/aunt/index/visit';
            /**********************发送抢单信息*******************************/
            $pht=new \aliyun\SendTemp();
            $pht::ServerGrab(session('employee.phone'),$order['snorder']);
            $re1=$pht::UserGrab($mobile,$order['snorder']);
            /**********************发送抢单信息********************************/
            return $this->success($status,'',$ret);
        //}else{
         //   return $this->error('failure','','抢单失败');
        //}
    }
    /**
     * 抢单结果
     * @Author   ksea
     * @DateTime 2019-06-01T14:28:20+0800
     * @return   [type]                   [description]
     */
    public function ret_grab($severid){
        $redis = new Redis();
        $redis->connect('127.0.0.1',6379);

        if($severid){

            $where['id']=$severid;

            $res=Db::name('shop_service_order')->where($where)->find();   
            
            if($redis->sCard($severid)==1){
                
                return $this->error('failure','','该订单已被抢');

            }
            else {

                $res1=Db::name('shop_service_order')->where($where)->find();

                if(!$res1['sfuserid']){
                     return $this->success('ok','','订单状态正常');
                }
                else{
                    return $this->error('failure','','该订单已被抢');
                }
            }
        }
    }
    /**
     * 预约时间
     * @Author   ksea
     * @DateTime 2019-05-14T19:31:50+0800
     * @param    string                   $uid   [description]
     * @param    string                   $cate  [description]
     * @param    string                   $limit [description]
     * @return   [type]                          [description]
     */
    public function server($uid='',$cate='',$limit='',$create_time=''){

        if(strlen($create_time)<2){
          $create_time=time();
        }

        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();

        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask=0";

        $query .=' AND order.create_time > 1558631041';

        if(!empty($create_time)){

            $query .=' AND order.create_time < '.$create_time;

        }

        $query .=" AND order.schedule not in(4,9)";

        if(strlen($limit)<=0){
             $s_data =  $this->model
                        ->with(['order','code','card','temp'])
                        ->alias('server')
                        ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                        ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                        ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                        ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                        ->join('fa_region region','region.region_id = server.areaid','LEFT')
                        ->where($query)
                        ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                        ->order('id desc')
                        ->limit($limit)
                        ->select();
             $res['url']=config('mobileUrl.evaluate');
             $res['url_out']=config('mobileUrl.details');
             $res['msg']='全部数据';
             $res['count']=count($s_data);
             $res['is_ture']=1;
             $res['create_time']=$create_time;
             $res['data']=$s_data;
        }
        else{

            $limit_a=explode(',', $limit);
            if(count($limit_a)==2){
               $need_count=$limit_a[1];
            }
            else{
                $need_count=$limit_a[0];
            }
            $s_data = $this->model
                     ->with(['order','code','card','temp'])
                     ->alias('server')
                     ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                     ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                     ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                     ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                     ->join('fa_region region','region.region_id = server.areaid','LEFT')
                     ->where($query)
                     ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                     ->order('id desc')
                     ->limit($limit)
                     ->select();
            if($need_count==count($s_data)){
                $res['msg']='数据合法';
                $res['count']=count($s_data);
                $res['is_ture']=1;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
            else{
                $res['msg']='数据缺少';
                $res['count']=count($s_data);
                $res['is_ture']=0;
                $res['create_time']=$create_time;
                $res['data']=$s_data;
            }
        }
        return $this->success('请求成功','/',$res);

    }
    /**
     * 上门时间修改
     * @Author   ksea
     * @DateTime 2019-05-15T14:40:45+0800
     * @param    [type]                   $serverid   [服务id]
     * @param    string                   $uptime     [修改时间年-月-日]
     * @param    string                   $usernote [服务备注]
     * @param    string                   $message    [短信信息]
     * @return   [type]                               [description]
     */
    public function up_time($serverid,$uptime='',$usernote='',$message=''){


        /***************2019-05-16**********************/
        $orderid=is_get_val('shop_service_order',$serverid,'orderid');

        $or=model('admin/shop/order')->get($orderid);
        if( $or['schedule'] == 4){
           return  $this->error('订单已取消');
        }
        Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>3]);
        /***************2019-05-16**********************/

        /************扭转记录********************/
        model('admin/shop/ShopOrderHistory')::create([
           'oid'=>$orderid,
           'desc' => session('employee.name').' 预约时间：'.$uptime,
        ]);
        /************扭转记录********************/
        $time=$uptime;

        if(empty($uptime)){
            $time=time();
        }
        $server=$this->model->where('id',$serverid)->with(['order','emp'])->find();

        $data['appoint_start']=strtotime($time);

        $data['is_ask']=$server['is_ask']+1;

        if(!empty($usernote)){
            $data['usernote']=$usernote;
        }        
        $res=$this->model->where('id',$serverid)->update($data);
        $pht=new \aliyun\SendTemp();
        if($data['is_ask']==1){

            foreach ($server['emp'] as $key => $value) {
                /**********************发送确上门时间信息*******************************/
                $pht::ServerSureGo($value['phone'],$server['order']['snorder'],$uptime);
                $pht::UserSureGo($server['order']['mobile'],$server['order']['snorder'],$value['name'],date('Y-m-d h:i:s'),$uptime);
                /**********************发送确上门时间信息********************************/
            }

        }
        else{
            foreach ($server['emp'] as $key => $value) {
                /**********************发送改约信息*******************************/
                $pht::ServerChange($value['phone'],$server['order']['snorder'],$uptime);
                $pht::UserChange($server['order']['mobile'],$server['order']['snorder'],$value['name'],date('Y-m-d h:i:s'),$uptime);
                /**********************发送改约信息********************************/
            }
        }

        return $this->success('请求成功','/',$res);
    }
    /**
     * 联系失败
     * @Author   ksea
     * @DateTime 2019-05-15T14:40:45+0800
     * @param    [type]                   $serverid   [服务id]
     * @param    string                   $messageuser [用户信息]
     * @param    string                   $messagesever [服务员信息]
     * @return   [type]                               [description]
     */
    public function err_notice($serverid,$usernote='',$new_time=''){
        $new_time=$new_time?$new_time:'1';
        $server=$this->model->where('id',$serverid)->with(['order','emp','empone'])->find();
        $data['is_errnotice']=$server['is_errnotice']+1;
        $data['usernote']=$usernote;
        /**********************发送联系失败信息*******************************/
        $pht=new \aliyun\SendTemp();
        $pht::ServerlinkErr($server['empone']['phone'],$server['order']['snorder'],$new_time);
        $pht::UserlinkErr($server['order']['mobile'],$server['order']['snorder'],$server['empone']['name'],date('Y-m-d h:i:s'),$new_time);
        /**********************发送联系失败信息********************************/
        $res=$this->model->where('id',$serverid)->update($data);
        return $this->success('请求成功','/',$res);
    }
}