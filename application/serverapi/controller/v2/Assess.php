<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\ShopServerAssess as MyAssess;

class Assess extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        $this->uid=session('aunt.id');
        parent::__construct();
        $this->model = new MyAssess();

    }


    /**
     * 服务人员评分数据
     * @param  [type] $goodsid [description]
     * @return [type]          [description]
     */
    public function getAvgAssess($uid=''){

        if($this->uid){
          $uid=$this->uid;
        }

        $base=$this->model;

        $query  ='';

        $query .= " FIND_IN_SET($uid,`sfuserid`) ";
        
        $wh['status']=$where['status']=1;
        $res=$base->field("ROUND(AVG(avg_assess),2) as avg_s")->where($where)->where($query)->select();
        if(!$res[0]['avg_s']){
            $dat['avg']="5.0";
        }
        else{
            $dat['avg']=$res[0]['avg_s'];
        }

        $wh['create_time']=['between',strtotime(date("Y-m-d",time())).",".strtotime(date("Y-m-d",time()) ." 1 days")];
        $dat['today']     =$base->where($wh)->where($query)->count();

        $where['avg_assess']=5;

        $dat_temp['a']=$base->where($where)->where($query)->select();
        $where['avg_assess']=['between','4,4.99'];
        $dat_temp['b']=$base->where($where)->where($query)->select();
        $where['avg_assess']=['between','3,3.99'];
        $dat_temp['c']=$base->where($where)->where($query)->select();
        $where['avg_assess']=['between','2,2.99'];
        $dat_temp['d']=$base->where($where)->where($query)->select();
        $where['avg_assess']=['between','1,1.99'];
        $dat_temp['e']=$base->where($where)->where($query)->select();
        $where['avg_assess']=['between','0,0.99'];
        $dat_temp['f']=$base->where($where)->where($query)->select();

        //好评数据
        $assess_goods=[];
        $assess_goods=array_merge($assess_goods,$dat_temp['a']);
        $dat['analysis']['goods'] = [
                                        'count'=>count($dat_temp['a']),
                                        'assessdata' =>[
                                            'mydata'=>[
                                                $assess_goods
                                            ],
                                        ],
                                    ];
        //中评
        $assess_mid=[];
        $assess_mid=array_merge($assess_mid,$dat_temp['b']);
        $assess_mid=array_merge($assess_mid,$dat_temp['c']);
        $dat['analysis']['mid']   = [
                                        'count'=>count($dat_temp['b'])+count($dat_temp['c']),
                                        'assessdata' =>[
                                            'mydata'=>[
                                                $assess_mid
                                            ],
                                        ],
                                    ];
        //差评
        $assess_bad=[];
        $assess_bad=array_merge($assess_bad,$dat_temp['d']);
        $assess_bad=array_merge($assess_bad,$dat_temp['e']);
        $assess_bad=array_merge($assess_bad,$dat_temp['f']);
        $dat['analysis']['bad']   = [
                                        'count'=>count($dat_temp['d'])+count($dat_temp['e'])+count($dat_temp['f']),
                                            'mydata'=>[
                                                $assess_bad
                                            ],
                                    ];

        return $this->success('获取成功','/',$dat);
    }
}