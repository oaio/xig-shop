<?php

namespace app\serverapi\controller\v2;

use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\Serverorder as Server;

/**
 * 
 * 已完成
 * 
 */

class Serverend extends Mycommon{

    /*******默认模型*****/
    private $model;

    private $uid;

    public function __construct()
    {

        parent::__construct();
        $this->model = new Server();

        $this->uid=session('aunt.id');
    }
    
    /**
     * 服务信息
     * @Author   ksea
     * @DateTime 2019-05-14T15:05:56+0800
     * @return   [type]                   [description]
     */
    public function message($uid='',$cate='',$limit=''){
      
        $query='';

        if($this->uid){

          $uid=$this->uid;
        
        }

        $emp=model('admin/shop/Employee')->where('id',$uid)->find();
        
        $use_cate = $emp->job;
        
        if($cate){
           $use_cate=$cate;
        }

        $query .= " FIND_IN_SET(`cate`,'$emp->job')  AND";

        $query .= " FIND_IN_SET($uid,`sfuserid`) AND";

        $query .= " server.is_ask > 0 AND";

        $query .= " server.status = 4";

        $query .=' AND server.create_time > 1558631041';

        $query .=" AND order.schedule not in(4,9)";

        $res  =  $this->model
                 ->with(['order','code','card','assess','extrkv','temp'])
                 ->alias('server')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->where($query)
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,order.actualpay,goods_msg,discount.bean,discount.dismoney,region.region_name,region.region_id')
                 ->order('fu_end desc')
                 ->limit($limit)
                 ->select();

        return $this->success('请求成功','/',$res);

    }
    /**
     * 添加，修改，更新服务拓展数据
     * @Author   ksea
     * @DateTime 2019-06-20T13:06:04+0800
     * @param    string                   $serverid [description]
     * @param    array                    $kv       [description]
     * @param    string                   $extrid   [description]
     */
    public function upExtrKv($serverid='',$kv=array(),$extrid=''){
        $model=model('admin/shop/Serverorderextr');
        if(empty($kv)){
            $data=$model::all(['serverid'=>$serverid]);
        }
        else{
            
            if(!in_array($kv['key'],['back','pic']))return $this->error('更新范围不在可执行范围内','/',$kv);
            
            if(!empty($extrid)){

                $model->save([
                    $kv['key'] =>$kv['value'],
                ],['id'=>$extrid]);

                $data=$model->get($extrid);

            }
            else{
                $serverData=$this->model->get(['id',$serverid])->toJson();
                $andData=[
                    $kv['key']=>$kv['value'],
                    'serverid'=>$serverid,
                    'auntid'  =>session('aunt.id')?session('aunt.id'):0,
                    'json_server'=>$serverData,
                ];
                $model->save($andData);
                $data=$model->get($model->id);
            }
        }
        return $this->success('请求成功','/',$data);
    }

}