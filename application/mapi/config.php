<?php

//配置文件
return [
	 'mobileUrl' =>[
	 	 'index'					=> '/index.php/mobile/index/index/v/'.config('fastadmin.v_api').'/rand/'.time(), //商城首页
	 	 'shopGoodsList'			=> '/index.php/mobile/goods/goods_list/v/'.config('fastadmin.v_api').'/rand/'.time(), //店铺商品列表
	 	 'shopGoodsDetail'			=> '/index.php/mobile/goods/goods_detail/v/'.config('fastadmin.v_api').'/rand/'.time(), //商品详情
         'shopStoreList'            => '/index.php/mobile/store/get_list',	//店铺列表
	 	 'sureBuy'					=> '/index.php/mobile/goods/buy/v/'.config('fastadmin.v_api').'/rand/'.time(),	//确认购买页面
	 	 'category'					=> '',	//分类跳转链接
         'success'                  =>'/index.php/mobile/pay/pay_succeed/v/'.config('fastadmin.v_api').'/rand/'.time(), //支付成功页面
         'fail'                     =>'/index.php/mobile/pay/pay_come/v/'.config('fastadmin.v_api').'/rand/'.time(), //支付失败页面
	 	 'shopOrder'				=>'/index.php/mobile/order/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//订单订单列表
	 	 								//'/index.php/mobile/order/detail/v/'.config('fastadmin.v_api').'/rand/'.time(),//订单订单详情
	 	 'shopOrderDetail'			=>'/index.php/mobile/goods/goods_detail/v/'.config('fastadmin.v_api').'/rand/'.time(),
	 	 'user'						=>'/index.php/mobile/user/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//个人中心
	 	 'userLog'					=>'/index.php/mobile/user/login/v/'.config('fastadmin.v_api').'/rand/'.time(),//登陆
	 	 'bindUser'					=>'/index.php/mobile/Controllercom/BindUser/v/'.config('fastadmin.v_api').'/rand/'.time(),//绑定操作
		 'setting'					=>'/index.php/mobile/setting/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//设置页面
	 	 'feedback'					=>'/index.php/mobile/setting/feedback/v/'.config('fastadmin.v_api').'/rand/'.time(),//反馈中心
	 	 'aboutus'					=>'/index.php/mobile/setting/about/v/'.config('fastadmin.v_api').'/rand/'.time(),//关于我们
	 	 'address'					=>'/index.php/mobile/address/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//用户地址
	 	 'pay'						=>'/index.php/mobile/pay/auth',//支付地址
	 	 'error'					=>'/v/'.config('fastadmin.v_api').'/rand/'.time(),//操作错误统一地址当前页面
	 	 'selfpay'					=>'/index.php/mobile/pay/center_pay/v/'.config('fastadmin.v_api').'/rand/'.time(),//自定义支付
	 	 'evaluate'					=>'/index.php/mobile/order/evaluate/v/'.config('fastadmin.v_api').'/rand/'.time(),//添加评论
	 	 'details'					=>'/index.php/mobile/order/details/v/'.config('fastadmin.v_api').'/rand/'.time(),//订单详情
		 'comment'			        =>'/index.php/mobile/goods/comment/v/'.config('fastadmin.v_api').'/rand/'.time(),//评论地址
         'group'                    =>'/index.php/mobile/fightgroup/group_order/v/'.config('fastadmin.v_api').'/rand/'.time(),//拼团地址
		 'vip'			=>'/index.php/mobile/vip/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//会员缴费
		 'invitegift'	=>'/index.php/mobile/index/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//邀请有礼
		 'userstore'	=>'/index.php/mobile/Userstore/index/v/'.config('fastadmin.v_api').'/rand/'.time(),//小店主页
	 ],
];
