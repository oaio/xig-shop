<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapi\controller;

use app\admin\model\ShopComment;
use app\admin\model\ShopCommentLike;
use think\controller\Rest;
/**
 * Like.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2/14/2019  2:46 PM
 * @see      https://gitee.com/d3li/February
 * @version 1.0.0214
 * @describe
 */
class Like extends Rest
{
	/**
	 *
	 * @author d3li 2/14/2019
	 * @return \think\Response
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		switch ($this->method){
			case 'post':
				return $this->add();
				break;
			case 'delete':
				return $this->delete(input('param.id'));
				break;
		}
		return $this->error(__('Unknown request format'));
	}

	/**
	 * 添加
	 * @author d3li 2/14/2019
	 * @return \think\Response
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function add()
	{
		$comment_id = intval(input('param.comment_id', 0));
		$res = ShopComment::get($comment_id);
		if($res) {
			$res->setInc('like_num');
			$ary = [
				'uid' => intval(input('param.uid', 0)),
				'commentid' => $comment_id
			];
			if(ShopCommentLike::get($ary))
				return $this->error(__('Record already exists'));
			$res = ShopCommentLike::create($ary);
			if ($res && $res->id)
				return $this->success();
		}
		return $this->error(__('No results were found'));
	}

	/**
	 * 删除
	 *
	 * @author d3li 2/14/2019
	 * @param int $id
	 * @return \think\Response
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function delete($id = 0)
	{
		if( !$id)
			$id = [
				'uid' => intval(input('param.uid', 0)),
				'commentid' => intval(input('param.comment_id', 0)),
			];
		$v = ShopCommentLike::get($id);
		if($v) {
			ShopComment::get($v->commentid)
				->setDec('like_num');
			$result = $v->delete();
			if ($result) {
				return $this->success();
			}
			return $this->error(__('No rows were deleted'));
		}
		return $this->error(__('No results were found'));
	}
}