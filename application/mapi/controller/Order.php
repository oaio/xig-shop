<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Order as morder;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
use think\Cache;
use think\Config;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Order extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new morder();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->add_order();

                break;
            case 'get':
                $uid=input('param.uid');
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $status=input('param.status','1');
                return $this->get_order($uid,$limit,$getTime,$status);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/
    /**
     * 添加订单api
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function add_order(){
        $data=input('param.');
        $data['admin_id'] = is_get_val('store_id',session('user.id'),'');
        //$data['city_code'] = session('user.use_city_code');没有session会出错
        $num=$data['num']=isset($data['num'])?$data['num']:1;

        $num=1;//强制生成一次预约

        /************************先判断店铺有木有地址id*****************************/
        if(!isset($data['address_id'])&&$data['store_id']>1){
            $storeData=model('admin/shop/Store')->get(['store_id'=>$data['store_id']]);
            if($storeData['address_id']>0&&isset($storeData['address_id'])){
                $data['address_id']=$storeData['address_id'];
            }
            else{
                /***********添加地址处理**************/
//                $addressData=[
//                    'name'    =>isset($storeData['ruler'])?$storeData['ruler']:$storeData['nickname'],
//                    'phone'   =>isset($storeData['ruler_phone'])?$storeData['ruler_phone']:$storeData['admin_nickname'],
//                    'uid'     =>0,
//                    'address' =>$storeData['address'],
//                    'region'  =>[$storeData['province_id'],$storeData['city_id'],$storeData['area_id']],
//                ];
                $addressData=[
                    'name'    =>'',
                    'phone'   =>'',
                    'uid'     =>0,
                    'address' =>'',
                    'region'  =>[$storeData['province_id'],$storeData['city_id'],$storeData['area_id']],
                ];
                if(strlen(implode(',', $addressData['region']))!=20){
                    $addressData['region']=['440000','440000','440306'];
                }
                $add_res=model('admin/shop/Address')->addAddress($addressData);
                if($add_res['code']==1){
                    $data['address_id']=$add_res['data'];
                    model('admin/shop/Store')->save(['address_id'=>$data['address_id']],['store_id'=>$storeData['store_id']]);
                }
                
                /***********添加地址处理**************/
            
            }
        }
        
        /************************先判断店铺有木有地址id*****************************/

        /********************地址获取**************************/

        $address=model('admin/shop/Address')->where('id',$data['address_id'])->find();
        $data['nickname'] = $address['name'];
        $data['mobile']   = $address['phone'];
        $data['address']   = $address['address'];
        $address_arr=explode(',', $address['region']);
        $areaid=$address_arr[2];
        $data['city_code']=$address_arr[1];
        
        /********************地址获取**************************/

        $res=$this->model->add_order($data);
        if($res['code']==1){
             /**********添加卡片/钩子***************/
             $param=[
                'name'=>$res['data']['goods_name'],
                'uid'=>$res['data']['uid'],
                'status'=>1,
                'times'=> is_get_val('shop_goods',$res['data']['goodsid'],'times')*$num,
                'sum_times'=>is_get_val('shop_goods',$res['data']['goodsid'],'times')*$num,
                'valitimes'=>0,
                'howlong'=>'10000',
                'orderid'=>$res['data']['id'],
                'admin_id'=>$res['data']['admin_id'],
             ];
             Hook::listen('set_card',$param);
             /***********添加卡片/钩子**************/
            $ret['data']=$res;
            if(strlen(session('user.phone'))<11&&session('user')){
                $ret['action']='binduser';
                $ret['url']=config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id');
            }
            else{
                $ret['action']='pay';
                $ret['url'] =config('mobileUrl.pay');
            }



            /*****************准备抢单数据下单完成发送抢单****************/

           // $CacheName='order_'.$ret['data']['data']['id'];
            $street = $address['street'];
            $address  =$data['address'];
            $sfuserid =input('param.sfuserid','');
            $areaid   =$areaid;
            $time     =input('param.appoint_start','');
            $note     =input('param.bak','');
            $d_time   =input('param.d_time','08:00~12:00');
            $d_data   =input('param.d_data',date('Y-m-d'));
            $appoint_start=$time?$time:switchtime($d_data,$d_time);
            $cate_id   =input('param.cate_id','');
            $time_part =input('param.time_part','');
            $eventtype = input('param.event','');
            //$actiontime=240;//过去时间

            $Cachedata =[
                'orderid'       =>$ret['data']['data']['id'],
                'street'       =>$street,
                'address'       =>$address,
                'sfuserid'      =>$sfuserid,
                'areaid'        =>$areaid,
                'note'          =>$note,
                'd_time'        =>$d_time,
                'd_data'        =>$d_data,
                'appoint_start' =>$appoint_start,
                'cate_id'       =>$cate_id,
                'time_part'     =>$time_part,
                'eventtype'     =>$eventtype,
                //'actiontime'    =>time()+$actiontime,
            ];

            //Cache::set($CacheName,$Cachedata,$actiontime);//有效时间设置4分钟//存缓存
            $orderTemp=model('admin/shop/OrderTemp');
            $orderTemp->save($Cachedata);//存库
            $tempid=$orderTemp->id;
            /******************准备抢单数据下单完成发送抢单***************/
            $ret['cache'] =$orderTemp->get($tempid); //Cache::get($CacheName);
            //小程序下单特殊处理-by JJ
            //Log::record(json_encode($res),'统一下单');
            if(isset($data['site']) && $data['site'] == 'mini' && isset($res['order_data'])){
                $ret['mini_data'] = self::miniOrder($res['order_data']);
            }
            return $this->success('请求成功','',$ret);
        
        }else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$ret);
        }
    }





    protected function get_order($uid,$limit='',$getTime='',$status=1){

        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->get_order($uid,$limit,$getTime,$status);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }
    /***************************************异步操作***************************************/

    /**
     * 小程序统一下单
     * @auth JJ
     * @param $data
     * @return array|bool
     */
    protected function miniOrder( $data ){
        $pays = Config::get('pay');
        $params['trade_type'] = 'JSAPI';
        $params['appid'] = $pays['mini_appid'];
        $params['body'] = $data['goods_name'];
        $params['openid'] = $data['mini_openid'];
        $params['out_trade_no'] = $data['snorder'];
        $params['total_fee'] = $data['actualpay']*100;

        $api = \wechat\Wepay::getInstance();

        $data = $api->setApp(isset($params['appid'])?$params['appid']:'wxa998328e720628f1')
//			->setKey('70389806194021fa5773236b95b9d705')
            ->setNotify(\think\Request::instance()->domain() . '/mapiv2/myorder/notify')
            ->prepay($params);
        return $data;
    }


}