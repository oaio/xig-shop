<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Order as morder;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Orderdetail extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new morder();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->add_order();

                break;
            case 'get':
                $id=input('param.id','');
                return $this->detail($id);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 商品详情
     * 
     * @Author   ksea
     * @DateTime 2019-01-03T18:41:08+0800
     * @return   [type]                   [description]
     */
    protected function detail($id=''){

         $validate=new Validate([
            'id'    =>'require',
         ],[
            'id.require'    =>'id不允许为空',
         ]);

         if(!$validate->check(['id'=>$id])){
            return $this->error('请求失败','',$validate->getError());
         }

         $data=$this->model->detail_order($id);
         if(empty($data)){
            return $this->success('请求成功','/',['data'=>$data,'msg'=>'数据为空']);
         }
         else{
            return $this->success('请求成功','/',['data'=>$data->toarray()]);
         }
    }


    /***************************************异步操作***************************************/
}