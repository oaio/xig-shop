<?php

namespace app\mapi\controller;

use think\controller\Rest;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class User extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->user();
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                // $comvali=$this->common();
                // if($comvali){
                //     return $this->error($comvali);
                // }
                $data=input('param.');
                return $this->update($data);
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    
    /***************************************异步操作***************************************/
    /**
     * 用户数据
     * 链接数据
     * @Author   ksea
     * @DateTime 2019-01-03T13:47:40+0800
     * @return   [type]                   [description]
     */
    public function user(){

        $ret['vip']=[
            'msg'   => '会员续费',
            'url'   => session('user.vip')?config('mobileUrl.vip'):config('mobileUrl.vip'),
        ];
        $ret['order']=[
            'msg'   => '订单中心',
            'url'   => session('user.phone')?config('mobileUrl.shopOrder'):config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id'),
        ];
        $ret['address']=[
            'msg'   => '地址中心',
            'url'   => session('user.phone')?config('mobileUrl.address'):config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id'),
        ];
        $ret['setting']=[
            'msg'   => '设置中心',
            'url'   => session('user.phone')?config('mobileUrl.setting'):config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id'),
        ];
        $ret['selfpay']=[
            'msg'   => '自定义支付',
            'url'   => session('user.phone')?config('mobileUrl.selfpay'):config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id'),
        ];
        $ret['invitegift']=[
            'msg'   => '邀请有礼',
            'url'   => session('user.invitegift')?config('mobileUrl.invitegift'):config('mobileUrl.invitegift'),
        ];
        $ret['userstore']=[
            'msg'   => '小店主页',
            'url'   => session('user.userstore')?config('mobileUrl.userstore'):config('mobileUrl.userstore'),
        ];
        return $this->success('请求成功','',$ret);

    }
    /**
     * 数据更新
     * @Author   ksea
     * @DateTime 2019-01-15T14:46:52+0800
     * @return   [type]                   [description]
     * true 
     */
    public function update($data){
        
        $res=model('admin/shopuser/user')->up_user($data);
        
        if($res['code']==1){
            return $this->success('请求成功','',$res);
        }
        else{
            return $this->error('请求失败','',$res);
        }
    }

    /***************************************异步操作***************************************/
}