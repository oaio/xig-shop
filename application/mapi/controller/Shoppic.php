<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapi\controller;

use think\controller\Rest;
use think\Db;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * Shoppic.php
 *
 * @author d3li <d3li@sina.com>
 * @create：1/9/2019  11:18 AM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0109
 * @describe
 */
class Shoppic extends Rest
{
	public function index()
	{
		$empty = json_decode("{}");
		if($this->method == 'get'){
			try {
				$res = Db::table('fa_shop_index')
					->field('id,img_link as img_url')
					->where('admin_id', 'IN', function($query){
						$query->table('fa_store')->where('store_id', input('store_id'))->field('admin_id');
					})->find();
				return json(['code'=>'1','data'=>$res ?: $empty]);
			} catch (DataNotFoundException $e) {
			} catch (ModelNotFoundException $e) {
			} catch (DbException $e) {
			}
		}
		return json(['code'=>'0','data'=>$empty]);
	}
}