<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Store as mstore;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;

class Storedetail extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new mstore();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->get_store(input('param.store_id'));
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    
    /***************************************异步操作***************************************/
    /**
     * datatime 2018-12-27
     * ajax坐标获取店铺
     * @param  string $latitude [description]
     * @param  string $longitude [description]
     * @return [type]               [description]
     */
    protected function get_store($store_id=''){
        $store_id=isset($store_id)?$store_id:'';

        $res=$this->model->store_detail($store_id);

        if(empty($res)){
            return $this->success('没有数据','',$res);
        }
        else{
            return $this->success('获取成功','',$res);
        }
    }

    /***************************************异步操作***************************************/
}