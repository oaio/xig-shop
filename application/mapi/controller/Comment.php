<?php

namespace app\mapi\controller;

use app\admin\model\ShopCommentLike;
use think\controller\Rest;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use app\admin\model\ShopComment;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Comment extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new ShopComment();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $data=input('param.');
                return $this->add($data);
                break;
            case 'get':

                return $this->error('请求未定义');
                break;

            case 'delete':
                return $this->error('请求未定义');
                break;

            case 'put':

                return $this->error('请求未定义');
                break; 

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    
    /***************************************异步操作***************************************/
    /**
     * 添加请求
     * @Author   ksea
     * @DateTime 2019-02-14T14:27:57+0800
     */
    public function add($data){
        $this->model->data($data);
        $this->model->allowField(true)->save();
        $url=config('mobileUrl.shopOrder');
        if($this->model->id){
            $ret=$this->model->id;
            return $this->success('请求成功',$url,$ret);
        }
        else{
            return $this->error('请求失败','/');
        }
    }
    /**
     * [del description]
     * @Author   ksea
     * @DateTime 2019-02-14T14:29:08+0800
     * @return   [type]                   [description]
     */
    public function del(){

    }
    /**
     * [exit description]
     * @Author   ksea
     * @DateTime 2019-02-14T14:28:45+0800
     * @return   [type]                   [description]
     */
    public function edit(){
        
    }
    /**
     * [show description]
     * @Author   ksea
     * @DateTime 2019-02-14T14:29:25+0800
     * @return   [type]                   [description]
     */
    public function show(){

    }
    /**
     * 辅助显示(列表行为)
     * @Author   ksea
     * @DateTime 2019-02-14T14:30:01+0800
     * @return   [type]                   [description]
     */
    public function h_show(){

    }
    /***************************************异步操作***************************************/

	/**
	 * 评论列表
	 *
	 * @author d3li 2/18/2019
	 * @return \think\Response
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
    public function index()
    {
    	if('get' == $this->method)
	    {
	    	$gid = intval(input('param.gid'));
	    	$start = intval(input('param.start', 0));
		    $limit = intval(input('param.limit', 10));
		    $total = $this->model
			    ->where(['gid' => $gid, 'is_show' => 0])
			    ->field('id')
			    ->count();
		    $list = $this->model
			    ->with('user')
			    ->where(['gid' => $gid, 'is_show' => 0])
			    ->order(['is_top'=>'desc','create_time'=>'desc'])
			    ->limit($start, $limit)
			    ->select();
		    $res = ShopCommentLike::all(['uid'=>intval(input('param.uid', 0))]);
		    $ary =[];
		    foreach ($res as $value){
		    	$ary[] = $value['commentid'];
		    }
		    foreach ($list as $value){
		    	$value['like'] = in_array($value['id'], $ary) ? 1:0;
		    }
		    $result = array('total' => $total, 'list' => $list);

		    return $this->success('请求成功', config('mobileUrl.comment'), $result);
	    }
	    return $this->error('请求未定义');
    }
}