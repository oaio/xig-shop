<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Store as mstore;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;

class Banner extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new mstore();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->banner();
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    
    /***************************************异步操作***************************************/
    /**
     * datatime 2018-12-27
     * ajax坐标获取店铺
     * @param  string $latitude [description]
     * @param  string $longitude [description]
     * @return [type]               [description]
     */
    /**
     * 商城轮播图
     * @author  baohb
     * @datetime 2018-12-29
     * true
     */

    protected function banner()
    {
        $city_code = session('user.use_city_code')?session('user.use_city_code'):440300;
        $where['city_code'] = array('in',[1,$city_code]);
        $img_index=Db::name('banner')->order('hot desc id')->limit("0,3")->select();
        return json($img_index);
    }

    /***************************************异步操作***************************************/
}