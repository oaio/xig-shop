<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapi\controller;

use think\controller\Rest;
use app\admin\model\ShopFeedback;
/**
 * Feedback.php 反馈接口
 *
 * @author d3li <d3li@sina.com>
 * @create：1/2/2019  10:10 AM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0102
 * @describe
 */
class Feedback extends Rest
{
	public function index()
	{
		switch ($this->method){
			case 'get':     //查询
				return $this->get(input('param.id'));
				break;
			case 'post':    //新增
				return $this->add();
				break;
			case 'put':     //修改
				return $this->update(input('param.id'));
				break;
			case 'delete':  //删除
				return $this->delete(input('param.id'));
				break;
		}
	}

	/**
	 * 查询
	 *
	 * @param int $id
	 * @return \think\response\Json
	 * @throws \think\exception\DbException
	 */
	public function get($id = 0)
	{
		if(empty($id)){
			return json(['error_code'=>'1','data'=>'']);
		}
		$res = ShopFeedback::get($id);
		unset($res['admin_id']);
		return json(['error_code'=>'1','data'=>$res]);
	}

	/**
	 *  添加消息
	 *
	 * @return \think\response\Json
	 */
	public function add()
	{
		$post = input();
		$result = 0;
		$v = validate()->rule(['feedback' => 'require|max:100']);
		if ($v->check($post)) {
			$res = ShopFeedback::create($post);
			$result = $res->id;
		}
		return json($result ? ['error_code' => '0', 'msg' => '反馈成功']
			: ['error_code' => '1', 'msg' => '请检查提交的数据']);
	}

	public function update($id = 0)
	{
		return 0;
	}

	public function delete($id = 0)
	{
		return 1;
	}
}