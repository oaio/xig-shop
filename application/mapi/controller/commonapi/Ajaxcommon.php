<?php

namespace app\mapi\controller\commonapi;

use think\Db;
use think\Validate;
use think\Controller;


header('Access-Control-Allow-Origin:*');
// 响应类型
header('Access-Control-Allow-Methods:*');
// 响应头设置
header('Access-Control-Allow-Headers:x-requested-with,Authorization,content-type');

class Ajaxcommon extends Controller{
    /**
     * 图片上传
     * @Author   ksea
     * @DateTime 2018-11-13T18:02:06+0800
     * @return   [type]                   [description]
     */
    public function upload_img(){
         $nickname=input('post.nickname');
         $base64=input('post.base64');
         $group=input('post.group','base_img');
         $data =
         [
            'group'    => $group,
            'nickname' => $nickname,
            'base64'   =>$base64,
         ];

         $res=model('admin/common/File')->create($data);
         if(isset($res)){
            $r=$this->save_up($base64,$res['id'],$group);
            if($r['code']==1){
                 $sa['url']=$r['url'];
                 $sa['id']=$res['id'];
                 $res=model('admin/common/File')->update($sa);
            }
         }
         else{
            $r =['data'=>null,"code"=>2,"massage"=>"图片生成失败"];
         }
         return $r;
    }
    /**
     * 保存base64图片
     * @Author   ksea
     * @DateTime 2018-11-13T17:33:33+0800
     * @return   [type]                   [description]
     */
    public function save_up($base64,$id,$group=''){
            $image=$base64;
            $imageName = $id."_".date("His",time())."_".rand(1111,9999).'.png';

            if (strstr($image,",")){
                $image = explode(',',$image);
                $image = $image[1];
            }

            $path = "uploads/".date("Ymd",time());
            if($group){
                $path.='/'.$group;
            }
            if (!is_dir($path)){
                mkdir($path,0777,true);
            }
            $imageSrc=  $path."/". $imageName;
            $r = file_put_contents(ROOT_PATH ."/public/".$imageSrc, base64_decode($image));
            if (!$r) {
                return ['data'=>null,"code"=>2,"massage"=>"图片生成失败"];
            }else{
                return ['data'=>1,"code"=>1,"massage"=>"图片生成成功",'url'=>$imageSrc];
            }

    }
    /**
     * （废弃）
     * 图片删除
     * @Author   ksea
     * @DateTime 2018-11-13T18:17:48+0800
     * @return   [type]                   [description]
     */
    public function delimg(){

    }
    /**
     * 获取码表下面全部地址
     * @Author   ksea
     * @DateTime 2019-01-03T14:08:07+0800
     * @return   [type]                   [description]
     */
    public function get_region(){
        if(request()->isAjax()){
            $regionParentId=input('param.region_id','');
            //默认取用户选中区域-JJ
            if(!$regionParentId){
                $regionParentId = session('user.use_city_code');
            }
            $pro = '';
            if(strlen($regionParentId)>0){
                 $returnAjax=Db::name('region')->where('region_parent_id',$regionParentId)->select();
                 $pro = Db::name('region')->where('region_id',$regionParentId)->find();
            } else{
                 $returnAjax=Db::name('region')->where('region_parent_id=1')->select();
            }

            $res['url']='';
            $res['msg']='';
            $res['count']=count($returnAjax);
            if($res['count']==0){
                $res['msg']='没有数据';
            }
            $res['data']=$returnAjax;
            $res['data']['province_id'] = $pro?$pro['region_parent_id']:'';
            $this->success('请求成功','/',$res);
        }
        else{
            $this->error('请求方式有误,只可以ajax请求');
        }
    }
    /**
     * 用户数据获取
     * @return [type] [description]
     */
    public function getUserData($openid=''){
        if(session('user')){//方便测试使用
            /************openid前端是否有存*****************/
             $data=[
                'openid' => $openid,
             ];
             $rule=[
                'openid'       => 'require',
             ];
             $msg=[
                'openid.require'=>'openid不能为空',
             ];
             $valOpenid=new Validate($rule,$msg);
             if(!$valOpenid->check($data)){
                 $this->error($valOpenid->getError(),'',$openid);
             }
            /************openid前端是否有存*****************/

            /************openid存在数据库校验*****************/

            $userh5=model('admin/shopuser/user')->with(['userStore','qrcode','bean','gift'])->where('openid',$openid)->find();
            $userapp=model('admin/shopuser/user')->with(['userStore','qrcode','bean','gift'])->where('mini_openid',$openid)->find();

            $userid=$userh5['id']?$userh5['id']:$userapp['id'];
            if(!file_exists('./qrcode/usercode/'.$userid.'.png')){
                $rst=creat_code('http://'.$_SERVER['HTTP_HOST'].'?pid='.$userid,$userid,5,0,false,'qrcode/usercode/');
            }

            else{
                $rst='/qrcode/usercode/'.$userid.'.png';
            }
            
            if(empty($userh5)&&empty($userapp)){
                $this->error('openid为加入数据库','/',$openid);
            }
            
            else{
                if(empty($userh5)){
                    $user=$userapp->toarray();
                }
                else{
                    $user=$userh5->toarray();
                }
                //二维码关系获取直接使用
                $user['qrcode']['usercodetemp']=[
                        'usercode' =>$rst,
                        'link' =>'http://'.$_SERVER['HTTP_HOST'].'?pid='.$userid,
                       ];

            }
            /************openid存在数据库校验*****************/

            if($user['vip_end_time']>=time()){
                    $user['interval']=1;
            }
            else{
                if($user['vip_end_time']==0){
                     $user['interval']=0;
                }
                else{
                     $user['interval']="-1";
                }
            }

            //获取用户对应地区列表
            $city_code = isset($user['use_city_code'])?$user['use_city_code']:'';
            $user['area_list'] = Db::table('fa_city_list')->where(['city_code'=>$city_code])->select();

            $this->success('请求成功','',$user);
        }
        else{
            $this->error('请先登陆');
        }
    }
    /**
     * 发送短信验证码
     * @Author   ksea
     * @DateTime 2018-12-11T11:55:47+0800
     * @return   [type]                   [description]
     */
    public function sms($phone='',$type='1'){
        if(input('param.')&&$type==1){
            $validate=new Validate([
               'phone' =>'unique:shop_userinfo',
            ],[
               'phone.unique' =>'手机号码已存在',
            ]);
            $vali_data=[
              'phone' => input('param.phone'),
            ];
            if(!$validate->check($vali_data)){
               $this->error($validate->getError());
            }
        }
        if(empty($phone)){
            $phone=input('param.phone','');
        }
        $data=[
            'phone' => $phone,
        ];
        $rule=[
            'phone' => 'require|/^1[3456789]\d{9}$/',
        ];
        $msg=[
            'phone.require' =>'手机号码不可以为空',
            'phone./^1[3456789]\d{9}$/'   =>'请输入正确的手机号码',
        ];
        if(validate()->make($rule,$msg)->check($data)){
            $SendTemp=new \aliyun\SendTemp();
            $content=randStr('6',"NUMBER");
            $res=$SendTemp->SendCode($phone,$content);
            $code=$res->Code;
            $ph=$phone;
            $msg=$content;
            $data=[
                'phone' =>$ph,
                'code' =>$msg,
            ];
            $data=json_encode($data);
            $file = fopen('sendmsg.txt', 'a'); 
            fwrite($file, '发送短信信息:' . $data . '  ' . date('Y-m-d H:i:s'). PHP_EOL);
            fwrite($file, '返回数据:' . json_encode($res) . '  ' . date('Y-m-d H:i:s'). PHP_EOL);
            if($code =='OK'){
                Db::name('shop_sms')->insert([
                    'code'  =>$content,
                    'phone' =>$phone,
                    'data'  =>json_encode($res),
                ]);
                $this->success('验证码发送成功','/',$content);
            }
            else{
                $this->error($res->Message);
            }
        }
        else{
            $this->error(validate()->make($rule,$msg)->getError());
        }
    }
    /**
     * diy信息
     * @Author   ksea
     * @DateTime 2019-05-21T17:20:42+0800
     * @param    [type]                   $phone   [description]
     * @param    [type]                   $message [description]
     * @return   [type]                            [description]
     */
    public function sms_diy($arr){

        $cout=count($arr);
        $e=0;
        $s=0;
        $s_arr=[];
        $e_arr=[];
        foreach ($arr as $key => $value) {
            $res=send_sms($arr[$key]['phone'],$arr[$key]['message']);
            if($res['code']=='ok'){
               $s++;
               array_push($s_arr, $arr[$key]);
            }
            else{
               $e++;
               array_push($e_arr, $arr[$key]);
            }
        }
        $this->success('发送结果','/',[
            'success'=>[
                'count'=>$s,
                'data' =>$s_arr
            ],
            'error'=>[
                'count'=>$e,
                'data' =>$e_arr
            ]
        ]);


    }
    /**
     * token 创建(内部测试使用)（针对添加，修改，使用）
     * 2018-12-28 23:53:10
     * @return [type] [description]
     */
    public function createToken(){
        $this->success(request()->token());
    }
    /**
     * 图片盗链处理
     * @Author   ksea
     * @DateTime 2019-02-14T11:59:18+0800
     * @return   [type]                   [description]
     */
    public function ajax_curl_img($mediaid=''){
            $jssdk=new \wechat\WechatAuth(config('wechat.appid'),config('wechat.secret'),config('wechat.token'));
            $token=$jssdk->showtoken();
            $mediaid=$mediaid?$mediaid:'t79tioO23095aa9Rw2Mmdu6P8TUv3dR271yDArRTDzjybtPsw3i5MX51I8N-pjNo';
            $curl1 = 'https://api.weixin.qq.com/cgi-bin/media/get?access_token='.$token.'&media_id='.$mediaid;
            $url=input('url')?input('url'):$curl1;
            $img_arr=explode('.',$url);
            $img_type='jpeg';
            $curl = curl_init();   
            curl_setopt($curl, CURLOPT_URL,$url);  
            curl_setopt($curl, CURLOPT_HEADER, 0);  
            curl_setopt($curl, CURLOPT_NOBODY, 0);

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);  
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);  

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
            $data = curl_exec($curl);  
            $httpinfo=curl_getinfo($curl);
            curl_close($curl);  
            if($data){
                $base_path='public/download/'.date('Ym');
                if (!is_dir($base_path)){
                    mkdir($base_path,0777,true);
                }
                $new_path=$base_path.'/'.microtime(true).'.'.$img_type;
                $write = fopen($new_path,"w");  
                fwrite($write,$data);  
                fclose($write);
                $msg['code']=1;
                $msg['message']='盗链成功';
                $msg['data']='/'.$new_path;  
            }
            else{
                $msg['code']=2;
                $msg['message']='盗链失败,链接已做防盗处理';
                $msg['data']=$data;   
            }
           return json($msg);  
    }
    /**
     * 获取全部链接
     * @Author   ksea
     * @DateTime 2019-03-07T13:31:55+0800
     * @return   [type]                   [description]
     */
    public function getUrl()
    {
        $conf = config('mobileUrl');
        if($name = input('name')){
            if(strpos($name, ',')>0){
                $ary = explode(',', $name);
                if($ary){
                    $temp = [];
                    foreach ($ary as $n){
                        isset($conf[$n]) && $temp[$n] = $conf[$n];
                    }
                    $conf = $temp;
                }
            }else {
                $conf = isset($conf[$name]) ? [$name => $conf[$name]] : json_decode("{}");
            }
        }
        return json(['code'=>'1','data'=>$conf]);
    }
    /**
     * 获取相似商品
     * @Author   ksea
     * @DateTime 2019-04-23T17:06:21+0800
     * @param    [type]                   $store_id [description]
     * @param    [type]                   $name     [description]
     * @return   [type]                             [description]
     */
    
    public function getlike($store_id=1,$name){
      $admin_id=is_get_val('store',$store_id,'admin_id','store_id');
      $nedarr=['深度保洁','日常保洁'];
      $is_use='';
      foreach ($nedarr as  $value) {
          $strpos=strrpos($name, $value);
          if(is_numeric($strpos)){
              $is_use=$value;
              break;
          }     
      }
      if(!empty($is_use)){
          $where['name']=array('like',"%".$is_use."%");
          $where['admin_id']=$admin_id;
          $where['is_del']=0;
          $where['status']=1;
          $where['cate']  =1;
          $select=model('admin/shop/Goods')->with(['cateData'])->where($where)->select();
          $ret=[];
          foreach ($select as $k => $v) {
            $num=$this->findNum($v['name']);
            $ret[$num]=$v;
          }
          ksort($ret);
          $this->success('请求成功','',$ret);
      }
      else{
        $this->error('请求关键词不在范围内');
      }
    }
    /**
     * 辅助截取
     * @Author   ksea
     * @DateTime 2019-04-23T19:42:02+0800
     * @param    string                   $str [description]
     * @return   [type]                        [description]
     */
    public function findNum($str=''){ 
        $str=trim($str); 
        if(empty($str)){return '';} 
        $temp=array('1','2','3','4','5','6','7','8','9','0'); 
        $result='';
        $break='1'; 
        for($i=0;$i<strlen($str);$i++){ 
            if(in_array($str[$i],$temp)){ 
                $result.=$str[$i];
                $break=2;
            }
            else{
              if($break==2){
                 break;
              } 
            }
        } 
        return $result; 
    } 
}