<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Categrory as mcategrory;
use app\mapi\controller\commonapi\Mycommon;

class Category extends Mycommon{

  /*******默认模型*****/
  private $model;

  public function __construct()
  {

    parent::__construct();
    $this->model = new mcategrory();

  }

	public function rest(){
	   switch ($this->method) {
	   		case 'post':
	   		
	   			$comvali=$this->common();
	   			if($comvali){
	   				return $this->error($comvali);
	   			}
	   			return $this->error('请求未定义');
	   			break;
	   		case 'get':

	   			return $this->getfloorcate();
	   			break;

	   		case 'delete':

	   			$comvali=$this->common();
	   			if(isset($comvali)){
	   				return $this->error($comvali);
	   			}

	   			return $this->error('请求未定义');
	   			break;

	   		case 'put':

	   			$comvali=$this->common();
	   			if(isset($comvali)){
	   				return $this->error($comvali);
	   			}

	   			return $this->error('请求未定义');
	   			break;

	   		default:
	   			return $this->error('未定义请求类型');
	   			break;
	   	}	
	}
   /***************************************异步操作***************************************/
  /**
   * 一次性获取全部分类
   * @Author   ksea
   * @DateTime 2018-11-13T11:39:31+0800
   * @return   [type]                   [description]
   */
   public function getall(){
     if(request()->isAjax()){

         $ret=$this->model->getallcate();
	       $ret_data['url']=config('mobileUrl.category');
	       $ret_data['msg']='';
	       $ret_data['data']=$ret;
         return $this->success('数据捕获成功','',$ret);
     }
     else{
         return $this->error('请求访问错误,需要ajax','',['request'=>request()->method()]);
     }
   }
   /**
    * 获取某一个父类的子类
    * @Author   ksea
    * @DateTime 2018-11-12T11:39:47+0800
    * @return   [type]                   [description]
    */
   public function getfloorcate(){
     if(request()->isAjax()){
        $pid=input('param.pid','0');
        $ret=$this->model->getfloorcate($pid);
        if(is_array($ret)){
	        $ret_data['url']=config('mobileUrl.category');
	        $ret_data['msg']='';
	        $ret_data['data']=$ret;
           return $this->success('数据获取成功','',$ret_data);
        }
        else{
	        $ret_data['url']=config('mobileUrl.category');
	        $ret_data['msg']='';
	        $ret_data['data']=$ret;
           return $this->success('没有匹配数据','',$ret_data);
        }
     }
     else{
           return $this->error('请求访问错误,需要ajax，post','',['request'=>request()->method()]);
     }
   }
   /***************************************异步操作***************************************/
}