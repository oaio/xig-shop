<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Store as mstore;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Log;

class Store extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new mstore();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->get_list(input('param.longitude'),input('param.latitude'),input('param.limit'),input('param.create_time'),input('param.other/a'));
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    
    /***************************************异步操作***************************************/
    /**
     * datatime 2018-12-27
     * ajax坐标获取店铺
     * @param  string $latitude [description]
     * @param  string $longitude [description]
     * @return [type]               [description]
     */
    protected function get_list($longitude='',$latitude='',$limit='',$getTime='',$other){

        $coordinate['x']=isset($latitude)?$latitude:'';
        $coordinate['y']=isset($longitude)?$longitude:'';
        $res=$this->model->get_list($coordinate,$limit,$getTime,$other);

        if(strlen($latitude)<=0 || strlen($longitude)<=0){

        }else{
            foreach($res['data'] as $key => $value){
                $mapp=new Map();
                Log::record('[ 地图位置问题 ] ' . var_export($value['latitude'], true), 'info');
                $s=$mapp->GetDistance($longitude,$latitude,$value['longitude'],$value['latitude']);
                $res['data'][$key]['distance']=$s;

            }
        }

        if(empty($res)){
            return $this->success('没有数据','',$res);
        }
        else{
            return $this->success('获取成功','',$res);
        }
    }

    /***************************************异步操作***************************************/
}