<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shopuser\User as muser;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Address extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new muser();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->error('请求未定义');
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->update(input('param.'));
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 更新用户数据
     * 地址
     * @Author   ksea
     * @DateTime 2019-01-03T18:31:45+0800
     * @return   [type]                   [description]
     * true
     */
    protected function update($data=array()){
        $validate=new Validate([
            'id'    =>  'require',
            'area_id'    =>  'require',
            'area'    =>  'require',
            'address'    =>  'require',
        ],[
            'id.require'    => 'id不能为空',
            'area.require'    => '区域不能为空',
            'area_id.require'    => '区域id不能为空',
            'address.require'    => '地址不能为空',
        ]);
        unset($data['__token__']);
        if(!$validate->check($data)){
            return $this->error($validate->getError());
        }
        $res=Db::name('shop_userinfo')->update($data);
        if(is_numeric($res)){
             $ret['data']=$res;
             return $this->success('请求成功','',$ret);
        }
        else{
             return $this->error('请求请求失败','',$ret);
        }
    }
    


    /***************************************异步操作***************************************/
}