<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Goods as mgoods;
use app\mapi\controller\commonapi\Mycommon;

class Goods extends Mycommon{
    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new mgoods();
    }

    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->goods_by_store(input('param.store_id'),input('param.limit'),input('param.create_time'),input('param.other/a'),input('param.search'));
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }
    }

    //搜索商品
    //luojunbo
    //20190715
    public function search()
    {
//        $id = session('user.id');
//        $user = model('admin/shopuser/User')->where(['id'=>$id])->find();
        if ($this->method != 'get') {
            return $this->error('未定义的请求方式');
        }
        $city_code = input('param.city_code');
        if (isset($city_code) == false) {
            $city_code = session('user.use_city_code');
        }
        $search = input('param.search');
        $cate = input('param.cate');
        $limit = input('param.limit');
        if ($limit == null || isset($limit) == false) {
            $limit = 10;
        }
        $last_id = input('param.last_goods_id');
        $order = input('param.order');
        if ($order == null || isset($order) == false) {
            $order = 'deal_times desc,commonprice asc';
        }
        trace("city_code:" . $city_code);
        trace("search:" . $search);
        trace("cate:" . $cate);
        trace("limit:" . $limit);
        trace("last_id:" . $last_id);
        trace("order:" . $order);
        $res = $this->model->search_goods($city_code, $search, $cate, $limit, $last_id, $order);
        if (isset($res['data'])) {
            foreach ($res['data'] as $key => $value) {
                $img = explode(',', $value['pic']);
                $res['data'][$key]['img'] = $img;
            }
        }
        if (empty($res)) {
            return $this->error('没有数据', '', $res);
        } else {
            return $this->success('获取成功', '', $res);
        }
    }

    /***************************************异步操作***************************************/
    /**
     * datatime 2018-12-27
     * 店铺获取商品
     * 多个商品
     * @param  string store_id [description]
     * @return [type]               [description]
     */
    protected function goods_by_store($store_id,$limit='',$getTime='',$other,$search){

        $data=[
            'store_id' =>$store_id,
        ];

        $rule=[
            'store_id' =>'require|min:1',
        ];

        $msg=[
            'store_id.require'=>'店铺id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->goods_by_store($store_id,$limit,$getTime,$other,$search);

            foreach($res['data'] as $key => $value){

                $img=explode(',',$value['pic']);
                $res['data'][$key]['img']=$img;
            }
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            }
        }
    }
    /***************************************异步操作***************************************/

    //获取推荐搜索关键字。luo 20190716。
    //没有区分城市。
    public function get_hot_search_keywords()
    {
        $searchHotMode = model('admin/shop/SearchHot');
        $hot_keywords = $searchHotMode->where(['status' => 10])->order('sequence asc')->field('keyword,number,sequence')->select();
        return $this->success('获取成功', '', $hot_keywords);
    }

}