<?php

namespace app\mapi\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Goods as mgoods;
use app\mapi\controller\commonapi\Mycommon;

class Goodsdetail extends Mycommon{
    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new mgoods();
    }

    public function rest(){

       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->get_goods_detail(input('param.id'),input('param.store_id'));
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }  
    }

    /***************************************异步操作***************************************/
    /**
     * datatime 2018-12-27
     * 某个店铺单个商品请求
     * 
     * @param  string store_id [description]
     * @return [type]               [description]
     */
    protected function get_goods_detail($id,$store_id){

        $data=[
            'store_id' =>$store_id,
            'id' =>$id,
        ];

        $rule=[
            'store_id' =>'require|min:1',
            'id' =>'require|min:1',
        ];

        $msg=[
            'store_id.require'=>'店铺id不允许为空',
            'id.require'=>'商品id不能为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->get_goods_detail($id,$store_id,'');
            if(empty($res)){

                $ret_data['url']=config('mobileUrl.sureBuy');
                $ret_data['msg']='';
                $ret_data['data']=$res;
                return $this->error('没有数据','',$ret_data);
            
            }
            else{
                $res['pic']=explode(',',$res['pic']);
                $ret_data['url']=config('mobileUrl.sureBuy');
                $ret_data['msg']='';
                $ret_data['data']=$res;
                return $this->success('获取成功','',$ret_data);

            } 
        }
    }


    /***************************************异步操作***************************************/
}