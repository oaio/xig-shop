<?php

namespace app\mapi\controller;

use app\admin\model\shopuser\User as muser;
use app\mapi\controller\commonapi\Mycommon;
use think\controller\Rest;
use think\Db;
use think\Log;
use think\Request;
use think\Session;
use think\Validate;

/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Binduser extends Mycommon {

	/*******默认模型*****/
	private $model;

	public function __construct() {

		parent::__construct();
		$this->model = new muser();

	}
	public function rest() {
		switch ($this->method) {
		case 'post':

			// $comvali=$this->common();
			// if($comvali){
			//     return $this->error($comvali);
			// }

			return $this->bind();

			break;
		case 'get':

			return $this->error('请求未定义');
			break;

		case 'delete':

			$comvali = $this->common();
			if ($comvali) {
				return $this->error($comvali);
			}

			return $this->error('请求未定义');
			break;

		case 'put':

			$comvali = $this->common();
			if ($comvali) {
				return $this->error($comvali);
			}

			return $this->error('请求未定义');
			break;

		default:
			return $this->error('未定义请求类型');
			break;
		}
	}

	/***************************************异步操作***************************************/
	/**
	 * 用户绑定手机操作
	 * @Author   ksea
	 * @DateTime 2019-01-03T13:47:40+0800
	 * @return   [type]                   [description]
	 */
	protected function bind() {
		$id = request()->param('id');
		if (empty($id)) {
			$id = session('wechat_a.2')['openid'] ? is_get_val('shop_userinfo', session('wechat_a.2')['openid'], 'id', 'openid') : is_get_val('shop_userinfo', input('param.openid', ''), 'id', 'mini_openid');
		}

		$phone = input('param.phone', '');
		$name = input('param.name', '');

		if (empty($name)) {
			$name = isset(session('wechat_a.2')['nickname']) ? session('wechat_a.2')['nickname'] : '昵称未获取';
		}

		if (input('param.mstore_id', '')) {
			$mstore_id = input('param.mstore_id');
		} else {
			$mstore_id = input('param.mstore_id', config('sys.def_store'));
		}
		$sex = input('param.sex', '1');
		$province_id = input('param.province_id', '440000');
		$province = input('param.province', '广东省');
		$city_id = input('param.city_id', '440300');
		$city = input('param.city', '深圳市');
		$area_id = input('param.area_id', '440306');
		$area = input('param.area', '宝安区');
		$address = input('param.address', '');
		$passwd = MD5(input('param.passwd', '123456'));

		/******************绑定完成之后调回支付页面自定订单********************/
		$oid = input('param.id', '');
		/******************绑定完成之后调回支付页面自定订单********************/

		/****************用户基础数据验证**********************/
		if (input('param.')) {

			$YouData = Db::name('shop_userinfo')->where('id', $id)->find();
			$query = '';
			$query .= 'phone=' . $phone . ' AND ';
			$query .= 'LENGTH(OPENID) = 0';
			$DataExt = Db::name('shop_userinfo')->where($query)->find();
			if ($DataExt) {
				$extid = $DataExt['id']; //外部订单录入生成用户
			} else {
				$validate = new Validate([
					'phone' => 'unique:shop_userinfo',
				], [
					'phone.unique' => '手机号码已存在',
				]);
				$vali_data = [
					'phone' => $phone,
				];
				if (!$validate->check($vali_data)) {
					return $this->error($validate->getError());
				}
			}
		}
		/****************用户基础数据验证**********************/

		if (request()->isAjax()) {
			$val_user = new Validate([
				'id' => 'require',
				'phone' => 'require',
				'sex' => 'require',
				'province_id' => 'require',
				'province' => 'require',
				'city_id' => 'require',
				'city' => 'require',
				'area' => 'require',
				'area_id' => 'require',
				//'address' => 'require',
			], [
				'id.require' => '账号不允许为空',
				'phone.require' => '手机号码不为空',
			]);
			$data = [
				'id' => $id,
				'phone' => $phone,
				'name' => $name,
				'passwd' => $passwd,
				'store_id' => $mstore_id,
				'sex' => $sex,
				'province_id' => $province_id,
				'province' => $province,
				'city_id' => $city_id,
				'city' => $city,
				'area_id' => $area_id,
				'area' => $area,
				'address' => $address,
			];
			if ($val_user->check($data)) {
				Db::startTrans();
				try {

					/****用户切换操作****/
					if (isset($DataExt) && !empty($DataExt)) {
						$data['id'] = $DataExt['id'];
						$data['nickname'] = $YouData['nickname'];
						$data['name'] = $name;
						$data['openid'] = $YouData['openid'];
						$data['unionid'] = $YouData['unionid'];
						$data['mini_openid'] = $YouData['mini_openid'];
						$data['pic'] = $YouData['pic'];
						$data['json_wechat'] = $YouData['json_wechat'];
						Db::name('shop_userinfo')->update([
							'id' => $id,
							'status' => 0,
							'openid' => '',
							'unionid' => '',
							'mini_openid' => '',
							'pic' => '',
							'json_wechat' => '',
							'update_time' => time(),
						]);
						$beanModel = model('admin/shop/EffectiveBean');

						$oldBean = $beanModel->where('userid', $id)->find();

						$beanModel->create([
							'userid' => $data['id'],
							'effective_bean' => $oldBean['effective_bean'],
							'receive_state' => $oldBean['receive_state'],
							'today_buy' => $oldBean['today_buy'],
						]);

						$id = $data['id']; //用户切换
					}
					/****用户切换操作****/
					$data['update_time'] = time();
					Db::name('shop_userinfo')->update($data);
					Db::commit();
				} catch (\Exception $e) {

					Log::init([
						'func' => 'BindUser',
						'error' => $e,
						'data' => $data,
					]);
					Db::rollback();
					return $this->error('绑定失败');
				}
				$user = Db::name('shop_userinfo')->where('id', $id)->find();
				session('user', $user);

				//邀请关系
				if ($user['pid'] > 0) {
					$inv = model('admin/shop/user')->invite($user);
				}
				
				//记录
				model('admin/shop/AnalysisMix')->bindlog($user,1);

				return $this->success('手机号码绑定成功', config('mobileUrl.bindUser') . 'id=' . $oid, $user);
			} else {
				return $this->error($val_user->getError());
			}

		}
	}

	/***************************************异步操作***************************************/
}