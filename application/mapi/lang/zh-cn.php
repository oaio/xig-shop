<?php
/**
 * 中文语言包
 * 
 */
return[
   /**
    * common
    */
   'success'=>'请求成功',
   'error'=>'请求失败',
   'comm_insert_success'=>'添加请求成功',
   'comm_insert_error'=>'添加请求失败',
   'comm_update_success'=>'修改成功',
   'comm_update_error'=>'修改失败',
   'start_time' =>'开始时间',
   'end_time' =>'结束时间',
   /**
    * BbsComatype->comadd
    */
   'comatype_addtime' =>'创建时间',
   'comatype_updatetime' =>'更新时间',
   'web_Comatype_title_add'=>'模块添加页面',
   'comatype_type'=> '模块类型(英文名称)',
   'comatype_status'=> '状态',
   'comatype_status_on' =>'开启',
   'comatype_status_off' =>'关闭',
   'comatype_status_tip' =>'状态提示',
   'ch_type'=> '模块类型(中文名称)',
   'ico_pic'=> '小图标',
   /**
    * BbsTab
    */
   'tab_error' =>'标签错误',
   'bbs_title'=>'社区标签',
   'comtab_nickname'=>'昵称',
   'comtab_title'=>'标签名称',
   'pic'=>'小图标',
   'comtab_type'=>'标签类型',
   'comtab_status'=>'状态',
   'comtab_status_on'=>'开启',
   'comtab_status_off'=>'关闭',
   'comtab_status_tip' =>'状态提示',
   /**
    * BbsColunm
    */
   'comgcol_create'=>'专栏添加',
   'comgcol_update'=>'专栏修改',
   'comgcol_title'=>'标题',
   'comgcol_nickname'=>'专栏昵称',
   'comgcol_text'=>'简述',
   'comgcol_status'=>'状态',
   'comgcol_status_on'=>'开启',
   'comgcol_status_off'=>'关闭',
   'comgcol_status_tip' =>'状态提示',
   'comgcol_select_tab'=>'标签选择',
   'comgcol_pic'=>'栏目小图标',
   /**
    * BbsTyle
    */
   'bbs_fenlei' => '文章分类',
   'type_create'=>'分类添加',
   'type_update'=>'分类修改',
   'type_title'=>'标题',
   'type_nickname'=>'分类昵称',
   'type_text'=>'简述',
   'type_status'=>'状态',
   'type_status_on'=>'开启',
   'type_status_off'=>'关闭',
   'type_status_tip' =>'状态提示',
   'type_select_tab'=>'标签选择',
   'type_pic'=>'栏目小图标',
   /**
    * 文体管理
    * 2018-09-11 18:18:08
    */
   'edit_data'=>'内容编辑',
   'add_data'=>'内容添加',
   'coma_title'=>'标题',
   'coma_second_title'=>'副标题',
   'coma_smpic'=>'文章缩略图',
   'coma_content'=>'内容',
   'comgcol_id'=>'专栏id',
   'coma_status'=>'状态',
   'coma_status_on'=>'开启',
   'coma_status_off'=>'关闭',
   'coma_status_tip' =>'状态',
   'coma_smcontent'=>'简要内容',
   'is_hot'=>'热搜',
   'is_hot_on'=>'开启',
   'is_hot_off'=>'关闭',
   'is_hot_tip'=>'提示',
   'is_new'=>'最新',
   'is_new_on'=>'开启',
   'is_new_off'=>'关闭',
   'is_new_tip'=>'提示',
   'edit_pc'=> 'pc端文本编辑',
   'edit_mobile'=> '手机端文本编辑',
   'comgtab_group'=>'使用标签',
   'coma_pic'=>'文章图片',
   'coma_content_mobile'=>'手机版文章内容',

   /**
    *  评论/点赞
    */
	'Unknown request format' => '请求未定义',
	'No results were found' => '记录未找到',
	'No rows were deleted' => '未删除任何行',
	'Record already exists' => '已经点赞过了',
];