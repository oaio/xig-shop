<?php
/**
 * 2018-09-04 16:24:23
 * ksea
 */
// 英文语言包
return [
   /**
    * common
    */
   'success'=>'success',
   'error'=>'error',
   'comm_insert_success'=>'insert_success',
   'comm_insert_error'=>'insert_error',
   'comm_update_success'=>'update_success',
   'comm_update_error'=>'update_error',
   'comm_delete_success'=>'delete_success',
   'comm_delete_error'=>'delete_error',
   'start_time' =>'start_time',
   'end_time' =>'end_time',
   /**
    * BbsComatype->comadd
    */
   'comatype_addtime' =>'comatype_addtime',
   'comatype_updatetime' =>'comatype_updatetime',
   'web_Comatype_title_add'=>'web_Comatype_title_add',
   'comatype_type'=> 'comatype_type(us)',
   'comatype_status'=> 'status',
   'comatype_status_on' =>'on',
   'comatype_status_off' =>'off',
   'comatype_status_tip' =>'tip',
   'ch_type'=> 'comatype_type(ch)',
   'ico_pic'=> 'smpic',
   /**
    * BbsTab
    */
   'tab_error' =>'tab_error',
   'bbs_title'=>'bbstip',
   'comtab_nickname'=>'comtab_nickname',
   'comtab_title'=>'comtab_title',
   'pic'=>'smpic',
   'comtab_type'=>'type',
   'comtab_status'=>'status',
   'comtab_status_on'=>'on',
   'comtab_status_off'=>'off',
   'comtab_status_tip' =>'tip',
   /**
    * BbsColunm
    */
   'comgcol_title'=>'comgcol_title',
   'comgcol_nickname'=>'comgcol_nickname',
   'comgcol_text'=>'comgcol_text',
   'comgcol_status'=>'status',
   'comgcol_status_on'=>'on',
   'comgcol_status_off'=>'off',
   'comgcol_status_tip' =>'tip',
   'comgcol_select_tab'=>'checkbox_tab',
   'comgcol_pic'=>'pic',
   /**
    * BbsTyle
    */
   'bbs_fenlei' => 'bbs_fenlei',
   'type_create'=>'type_create',
   'type_update'=>'type_update',
   'type_title'=>'type_title',
   'type_nickname'=>'type_nickname',
   'type_text'=>'type_text',
   'type_status'=>'type_status',
   'type_status_on'=>'type_status_on',
   'type_status_off'=>'type_status_off',
   'type_status_tip' =>'type_status_tip',
   'type_select_tab'=>'type_select_tab',
   'type_pic'=>'type_pic',
   /**
    * 文体管理
    * 2018-09-11 18:18:08
    */
   'coma_title'=>'coma_title',
   'coma_smpic'=>'coma_smpic',
   'coma_content'=>'coma_content',
   'comgcol_id'=>'comgcol_id',
   'coma_status'=>'coma_status',
   'coma_status_on'=>'coma_status_on',
   'coma_status_off'=>'coma_status_off',
   'coma_status_tip' =>'coma_status_tip',
   'coma_smcontent'=>'smcontent',
   'coma_title'=>'coma_title',
   'is_hot'=>'set hot',
   'is_new'=>'set new',
   'comgtab_group'=>'comgtab_group',
   'coma_pic'=>'coma_pic',
   'coma_content_mobile'=>'content_mobile',

];
