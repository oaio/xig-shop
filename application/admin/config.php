<?php

//配置文件
return [
    'url_common_param'       => true,
    'url_html_suffix'        => '',
    'controller_auto_search' => true,
    // 默认模块名
    'default_module'         => 'admin',
	//客服组
	'waiter_ids' => [8],
	//可切换城市的管理组
	'city_group_ids' => [1, 8, 10]
];
