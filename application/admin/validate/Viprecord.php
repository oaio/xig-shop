<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\validate;

use think\Validate;
class Viprecord extends Validate
{
	protected $rule = [
		'uid'  => 'require|integer',
		'stoptime' => 'require|egt:actiontime',
	];

	protected $field = [
		'uid' => '用户ID',
		'stoptime' => '开始时间',
		'actiontime' => '结束时间',
	];

	/**
	 * 提示消息
	 */
	protected $message = [
		'stoptime.require' => '请选择到期时间',
		'stoptime.egt' => '到期时间大于等于今天',
	];
}