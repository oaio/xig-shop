<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/24 0024
 * Time: 20:47
 */

namespace app\admin\behavior;

use wechat\WxpayService as MyWxpayService;

class Cash
{
	public function __construct()
	{
		\wechat\Log::init('../runtime/log/cash.txt');
	}

	public function run(&$param){
        $wxpay = new MyWxpayService;
        $wxpay->createJsBizPackage($param);
    }

	public function log(&$param)
	{
		$admin = session('admin');
		$data = array_merge([
			'admin_id'=> $admin['id'],
			'username'=> $admin['username'],
			'title'=> '赠送小哥豆',
			'content'=> '',
			'ip'=> request()->ip(),
			'useragent'=> request()->server('HTTP_USER_AGENT'),
		], $param);
		model('SecurityLog')::create($data);
	}

	/**
	 * 提现推送。如有疑问，请拨打客服热线
	 *
	 * @author d3li 2019/7/2
	 * @param $param
	 */
	public function push(&$param)
	{
		\wechat\Api::getInstance()->send_template($param['openid'],
			'Pqek-tpYi9DKk4bQjJySdo4A8mNC1x_jqFwnACqz93Y',
			[
				'first'=>"您已成功申请提现，请您注意查收。",
				'money'=> $param['cost'],
				'timet'=> date('Y-m-d H:i'),
				'remark' => '预计10分钟内到账，如有疑问请拨打客服热线：'.config('site.tel400')
			],
			request()->domain().'/mobile/user/index.html'
		);
	}

	public function check(&$param)
	{
		\wechat\Api::getInstance()->send_template($param[0],
//		'18kO4mePqL_YPTYnpcWGZpznapPYelUlHNJ--_cJzq4',//测试
			config('wechattemplate.cash'),
			[
				'first'=>"恭喜您，您的门店资金审核通过，请及时查看！",
				'keyword1'=> $param[1],
				'keyword2'=> '通过',
				'keyword3'=> date('Y年m月d日 H:i', $param[2]),
				'remark' => '感谢您的支持！若有疑问请拨打小哥帮客服热线：'.config('site.tel400')
			],
			config('wechattemplate.storeorder')["link"]
		);
	}

	/**
	 *
	 * @author d3li 2019/7/24
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function notice()
	{
		$start = microtime(1);
		$zero = strtotime(date('Ymd'));
		$list = model('admin/shop/Serverorder')
			->where('fu_start', 'between', [$zero-86400, $zero])
			->where('status', 1)
			->where('is_ask', '>', 0)
			->select();
		$ary = $data = $phone = [];
		foreach ($list as $value){
			$ary[] = $value['sfuserid'];
		}
		$data = array_unique(explode(',', implode(',', $ary)));
		$list = model('admin/shop/Employee')
			->where('id', 'in', $data)
			->select();
		foreach ($list as $v){
			empty($v['phone']) || $phone[] = $v['phone'];
		}
		if(!empty($phone))(new \aliyun\SendTemp())->SendEnd($phone);
		printf('process in %.3fs, sent %d SMS',
			microtime(1) - $start, count($phone));
	}

	/**
	 * 红包推送
	 *
	 * @author d3li 2019/7/2
	 * @param $param
	 */
	public function gain(&$param)
	{
		\wechat\Api::getInstance()->send_template($param['openid'],
			'djot2ds1Edvbl_YX_fTwEozzOUpfyjYitTy2NBZkKPs',
			[
				'first'=>"收到客服{$param['nickname']}赠送的红包，请查收！",
				'keyword1'=> sprintf('%.2f', $param['cost']),
				'keyword2'=> date('Y-m-d H:i'),
				'remark' => '感谢您的支持！客服热线：'.config('site.tel400')
			],
			request()->domain().'/mobile/user/index.html'
		);
	}

	/**
	 * 群发红包推送/短信
	 *
	 * @param $param
	 * @throws \think\exception\DbException
	 */
	public function appEnd(&$param)
	{
		$api = \wechat\Api::getInstance();
		$ids = cookie('uid');
		$cost = sprintf('%.2f', cookie('cost'));
//		$tel = config('site.tel400');
//		$name = session('admin.nickname');
		$domain = request()->domain();
		$list = model('ShopUserinfo')->where('id', 'in', $ids)->select();
		$sms = new \aliyun\SendTemp();
		foreach ($list as $v){
			$api->send_template($v['openid'],
				config('wechattemplate.bean'),
				[
					//'first'=>'收到客服' . $name . '赠送的红包，请查收！',
					'first'=>'[恭喜发财，大吉大利]',
					'keyword1'=> $cost,
					'keyword2'=> date('Y-m-d H:i'),
					'remark' => '小哥帮给您发了一个红包，点击立即领取哦！'
				],
				$domain . '/mobile/user/index.html'
			);
			$sms::SendDiy($v['phone'],'SMS_172357944', ['red_value'=>$cost-0]);
		}
	}
}