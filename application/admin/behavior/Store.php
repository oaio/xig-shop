<?php

namespace app\admin\behavior;

use think\Db;
use think\Log;
use app\admin\model\shop\Store as myStore;
class Store
{
	public function run(&$param){
		if(isset($param['func'])){
			$func =$param['func'];
		}
		else{
			$func='add';
		}
		$this->$func($param);
	}
	/**
	 * 添加用户钩子
	 * @Author   ksea
	 * @DateTime 2018-11-28T15:39:33+0800
	 */
	public function add(&$param){
		$Store=new myStore();
		$res=$Store->add_store($param['request']);	
		if($res['code']==1&&isset($res['code'])){
			$param['request']['store_id']=$res['data'];
		}
	}
	/**
	 * 删除门店
	 * @Author   ksea
	 * @DateTime 2018-12-26T13:17:24+0800
	 * @param    [type]                   &$param [admin_id]
	 * @return   [type]                           [description]
	 */
	public function del(&$param){
		$Store=new myStore();
		$param['request']['return']=$Store->del_store(['store_id'=>is_get_val('store',$param['request']['id'],'store_id','admin_id')]);
	}
	/**
	 * 门店编辑
	 * @Author   ksea
	 * @DateTime 2019-02-12T17:15:12+0800
	 * @param    [type]                   $param [description]
	 * @return   [type]                          [description]
	 */
	public function edit(&$param){
		$Store=new myStore();
		$param['request']['return']=$Store->up_store($param['request']);
	}

}