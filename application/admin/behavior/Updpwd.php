<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/17
 * Time: 11:14
 */

namespace app\admin\behavior;

use Think\Db;
use Think\Hook;

class Updpwd
{
    public function run($data)
    {
//        $uid = Session::get('admin.id');

        $res = Db::name('store')->field('ruler_phone')->where('admin_id','=',$data['admin_id'])->find();
        $phone = $res['ruler_phone'];
        $pwd= $data['pwd'];
        $msg="您的小哥帮后台密码已修改为：（".$pwd."），小哥帮短信通知，请勿回复！";

        send_sms($phone,$msg);

        $module_name='个人设置';//模块(菜单)名称(必填)
        $op_name='密码修改';//操作内容(必填)
        $remark=json_encode($msg);//操作备注(选填)
        $param=[
            'module_name'=>$module_name,
            'op_name'=>$op_name,
            'remark'=>$remark,
        ];
        \think\Hook::listen('activelog',$param);
    }
}