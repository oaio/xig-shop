<?php

namespace app\admin\behavior;

use think\Db;
use think\Log;
use app\admin\model\shop\Store as myStore;
class Storecode
{
	public function run(&$param){
		$model=new myStore;
		$model->create_code($param);
	}
}