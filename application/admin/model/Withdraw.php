<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;
use think\Model;

/**
 * Withdraw.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/7/7  14:54
 * @see      https://gitee.com/d3li/
 * @version 2.07.07
 * @describe
 */
class Withdraw extends Model
{
	public function u()
	{
		return $this->hasOne('ShopUserinfo', 'id', 'uid');
	}

	public function w()
	{
		return $this->hasOne('Withdraw', 'id');
	}
}