<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * ShopCard.php
 *
 * @author d3li <d3li@sina.com>
 * @create：1/15/2019  4:50 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0115
 * @describe
 */
class ShopCard extends Model
{

}