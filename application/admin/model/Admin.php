<?php

namespace app\admin\model;

use think\Model;
use think\Session;

class Admin extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function city()
    {
    	return $this->hasOne('Region', 'region_id', 'city_code');
    }

    /**
     * 重置用户密码
     * @author baiyouwen
     */
    public function resetPassword($uid, $NewPassword)
    {
        $passwd = $this->encryptPassword($NewPassword);
        $ret = $this->where(['id' => $uid])->update(['password' => $passwd]);
        return $ret;
    }

    // 密码加密
    protected function encryptPassword($password, $salt = '', $encrypt = 'md5')
    {
        return $encrypt($password . $salt);
    }

    public function cklogin($username,$password){
        $result=$this->where(['username'=>$username])->find();
        if($result){
            $password=md5(md5($password).$result['salt']);
            $where['username']=$username;
            $where['password']=$password;
            $ret=$this->where($where)->find();
            if($ret){
                session('admin',$ret);
                return true;    
            }
        }
        return false;
    }

    public function list($search=array()){
        $temp_search=$search;
        if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3]?:'desc';
            $is_page=$temp[4];
        }
        $where=[];
        $where['type']=array('eq','2');
        if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                if(strlen($value)==0){
                    unset($other[$key]);
                }
                else{
                    $where[$key]=$value;
                }
            }
        }
        if($search){
            $san_like=[
                'admin_nickname',
                'phone',
                'nickname',
                'address'
            ];
            $link_scane=implode('|', $san_like);
            $data=$this
                ->where($link_scane,'like',"%{$search}%")
                ->where($where)
                ->order('city_code '.$order)
                ->paginate($paginate);

        }
        else{
            $data=$this
                ->where($where)
                ->order('city_code '.$order)
                ->paginate($paginate);
        }
        $page=$data->render();
        return [$data,$page,$temp_search];
    }
}
