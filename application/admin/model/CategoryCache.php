<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * CategoryCache.php 分类时间规则缓存
 *
 * @author d3li <d3li@sina.com>
 * @create：15/04/2019  3:57 PM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.15
 * @describe
 */
class CategoryCache extends Model
{

	/**
	 * 查询满单信息
	 *
	 * @author d3li 20/04/2019
	 * @param int $id
	 * @return array
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function days($id = 0)
	{
		$row=CategoryRule::get(['cid'=>$id]);
		$max = empty($row->count3) ? 1 : 2;
		$zero = strtotime(date('Ymd'));
		$ary = $weeks = $data = [];
		$res = $this->field(
			'time,CASE WHEN COUNT(time)>'.$max
			.' and SUM(count)=0 THEN 0 ELSE 1 END AS `active`')
			->where('id', $id)
			->group('time')
			->select();
		foreach ($res as $v){
			$data[$v->time] = $v->active;
		}
		$max ++;
		$flag = date('H')>intval($row["time$max"]);
		$stay = date('d');
		$month = date('Y-m');
		$max = date('t')+1;
//		for($i=date('w'); $i>0; $i--){
//			$ary[] = ['day'=>$stay-$i, 'month'=>$month, 'active'=>0];
//		}
		for($i=$stay; $i<$max; $i++){
			$ary[] = ['day'=>$i, 'month'=>$month, 'active'=>isset($data[$zero]) ? $data[$zero] : 1];
			$zero += 86400;
		}
		if($flag){
			$ary[0]['active'] = 0;
		}
		$month = date('Y-m', $zero);
		for($i=1; $i<$stay; $i++){
			$ary[] = ['day'=>$i, 'month'=>$month, 'active'=>isset($data[$zero]) ? $data[$zero] : 1];
			$zero += 86400;
		}
		return $ary;
	}

	
	/**
	 * 获取余数列表
	 *
	 * @param  int $cate_id 分类ID
	 * @param string|int $date 日期或时间戳
	 * @return array
	 * @throws \think\exception\DbException
	 */
	public function search($cate_id, $date)
	{
		if(!is_numeric($date) || strlen($date)<10)$date = strtotime($date);

		$row = CategoryRule::get(['cid'=>$cate_id]);
		$max = 3;
		if(!$row)return [];
		if(empty($row['time3']))$max -=1;
		$diff =$date-$_SERVER['REQUEST_TIME'];//strtotime('2019-06-21 19:21:00');
		//$_SERVER['REQUEST_TIME'];
		$hour = date('H');
		if ($diff < -86400) {
			$list = [];
			for ($i = 0; $i < $max; $i++) {
				$list[] = ['id' => $cate_id, 'part' => $i, 'count' => 0];
			}

		}else if($diff>=-43200 && $diff<10800) {
			$res = $this->get(['id' => $cate_id, 'part' => 2, 'time' => $date]);
			$temp=[];
			for ($i = 1; $i <= $max; $i++) {
				if($i==1){
					$temp[] = ['id' => $cate_id, 'part' => $i, 'count' =>0 , 'note'=>$row['note'.($i)], 'time'=>$row['time'.($i)]];
				}
				else{
					$temp[] = ['id' => $cate_id, 'part' => $i, 'count' =>$row['count'.($i)] , 'note'=>$row['note'.($i)], 'time'=>$row['time'.($i)]];
				}
			}
			return $temp;

		}else {
			if ($diff < 0 ) {
				$list = [];
				for ($i = 0; $i < $max; $i++) {
					$list[] = ['id' => $cate_id, 'part' => $i, 'count' =>0];
				}
				if($diff>=-57000&&$max==3){
					$list[2] =['id' => $list[2]['id'], 'part' =>$list[2]['part'], 'count' =>$row["count2"]];
				}
			} else {
				$list = $this->where(['id' => $cate_id, 'time' => $date])->select();
				/** @var array $list */
				$list = collection($list)->toArray();
				$ary = [];
				foreach ($list as $value) {
					$ary[] = $value['part'];
				}

				for ($i = 1; $i <= $max; $i++) {
					if ($diff < 0) {
						list($start) = explode('~', $row["time$i"]);
						if ($start) {
							if ($hour > intval($start)) {
								$row["count$i"] = 0;
								foreach ($list as &$v) {
									if ($v['part'] == $i) $v['count'] = 0;
								}
							}
						}
					}
					if (in_array($i, $ary)) continue;
					$list[] = [
						'id' => $cate_id,
						'part' => $i,
						'count' => $row["count$i"],
					];
				}

			}
		}
		$res =  [];
		foreach ($list as $key => $value){
			$i = $key+1;
			if($i>3) break;
			$value['note'] = $row["note$i"];
			$value['time'] = $row["time$i"];
			$res[$value['part']] = $value;
		}
		sort($res);
		return $res;
	}

	/**
	 * 添加/更新规则记数
	 *
	 * @param int $cate_id 分类ID
	 * @param int $time_part 时间段(1-3)。如用户选time1，则传：1
	 * @param string $date 日期，格式：2019-04-16
	 * @return int 当天余数
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function add($cate_id, $time_part, $date)
	{
		$rule = CategoryRule::get(['cid'=>$cate_id]);
		$time = strtotime($date);
		$count = 0;
		if($rule) {
			list($start) = explode('~', $rule["time$time_part"]);
			if($_SERVER['REQUEST_TIME']>strtotime($date.' '.$start)) return 0;
			$row = $this->get(['id' => $cate_id, 'part' => $time_part, 'time' => $time]);
			if ($row) {
				if ($row['count'] < 1) return 0;
				$count = $row['count'] -= 1;
				$row->save();
			} else {
				$rule = $rule->toArray();
				$count = $rule["count$time_part"] - 1;
				$this->save(['id' => $cate_id, 'part' => $time_part, 'count' => $count, 'time' => $time]);
			}
		}
		return $count;
	}

	/**
	 * 清理缓存记录（昨天的）
	 *
	 * @return CategoryCache
	 */
	public function clean()
	{
		$date = strtotime('-1 day');
		$zero = strtotime(date('Ymd', $date))+1;
		$this->where('time', '<', $zero)->delete();
//		$count = 0;
//		foreach ($list as $k => $v) {
//			$count += $v->delete();
//		}
//		return $count;
		return $this;
	}
	
}