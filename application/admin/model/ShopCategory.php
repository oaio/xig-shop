<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * ShopCategrory.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/21/2018  11:42 AM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1221
 * @describe
 */
class ShopCategory extends Model
{
	protected $name='shop_categrory';

	protected function base($query)
	{
		$query->where('status', 1);
	}

	/**
	 * 软删除
	 *
	 * @return int|string
	 * @throws \think\Exception
	 * @throws \think\exception\PDOException
	 */
	public function artdel()
	{
		$where = $this->getWhere();
		$result = $this->getQuery()->where($where)->update(['status' => 0]);
		// 清空原始数据
		$this->origin = [];
		return $result;
	}

	/**
	 * 获取分类数据
	 *
	 * @return array
	 * @throws \think\exception\DbException
	 */
	public function parent()
	{
		$res = $this::all();
		$ary = ['选择父级'];
		foreach ($res as $value){
			$ary[$value['id']] = $value['name'];
		}
		return $ary;
	}
}