<?php

/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\model;
use think\Model;

/**
 * ShopAddress.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/04/2019  10:29 AM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.12
 * @describe
 */
class ShopAddress extends Model
{
	protected $autoWriteTimestamp = true;
}