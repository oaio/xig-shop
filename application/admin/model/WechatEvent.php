<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * WechatEvent.php
 *
 * @author d3li <d3li@sina.com>
 * @create：27/03/2019  5:36 PM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.27
 * @describe
 */
class WechatEvent extends Model
{

}