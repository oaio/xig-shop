<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
use think\Session;

/**
 * ShopOrder.php
 *
 * @author d3li <d3li@sina.com>
 * @create：1/11/2019  8:32 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0111
 * @describe
 */
class ShopOrder extends Model
{
	protected $admin_id;
	protected function initialize()
	{
		parent::initialize(); // TODO: Change the autogenerated stub
		$this->admin_id = Session::get('admin.id');
	}

	public function base($query)
	{//'shop_order.admin_id'=>$this->admin_id,
		$query->with('store,card,paylog,service.emp,virdiscount,f')->where(['is_pay'=>1]);
	}

	public function service()
	{
		return $this->hasOne('app\mapiv2\model\ShopServiceOrder', 'orderid');
	}

	public function store()
	{
		//return $this->belongsTo('Store', 'store_id', 'store_id')->setEagerlyType(0);
		return $this->hasOne('Store', 'store_id', 'store_id');
	}

	public function card()
	{
		return $this->hasOne('ShopCard', 'orderid');
	}

	public function a()
	{
		return $this->hasOne('ShopOrder', 'id', 'id');
	}

	public function f()
	{
		return $this->hasOne('app\admin\model\ShopUserinfo', 'id', 'uid');
	}
    /**
     * 追加订单关联
     * @Author   ksea
     * @DateTime 2019-02-27T10:26:40+0800
     * @return   [type]                   [description]
     */
    public function paylog(){
        return $this->hasMany('Wechatpay','oid','id');
    }
	  /**
	   * 小哥豆抵扣信息
	   * @Author   ksea
	   * @DateTime 2019-05-07T14:38:03+0800
	   * @return   [type]                   [description]
	   */
	  public function virdiscount(){
	    $where=[
	      'is_pay'=>1,
	    ];
	    return $this->hasOne('app\admin\model\shop\Virdiscount', 'id', 'discount_id')->where($where);
	  }
}