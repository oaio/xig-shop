<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * Store.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/29/2018  2:26 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1229
 * @describe
 */
class Store extends Model
{

	public function admin()
	{
		return $this->hasOne('admin', 'id', 'admin_id');
	}

	public function city()
	{
		return $this->hasOne('region', 'region_id', 'city_id');
	}

	public function m()
	{
		return $this->hasOne('store', 'store_id', 'store_id');
	}

	public function bu()
	{
		return $this->hasOne('app\admin\model\shopuser\User', 'store_bind');
	}
}