<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
use think\Session;
use traits\model\SoftDelete;

/**
 * Employee.php 员工管理模型
 *
 * @author d3li <d3li@sina.com>
 * @create：12/21/2018  5:45 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1221
 * @describe
 */
class Employee extends Model
{
	protected $autoWriteTimestamp = 'int';
//	protected $insert = ['cid'];

	public function base($query)
	{
		$query->where('delete_time', 'eq', 0);

        $mod=model('AuthGroupAccess')->aga(session('admin.id'));
        //需要过滤角色住
        $guishu=[
        	'6','14'
        ];

        if(in_array($mod['group_id'],$guishu)&&session('admin.type')!=2){

        	$query->where('admin_id', Session::get('admin.id'));

        }
    }

	protected static function init()
	{
		self::afterInsert(function ($user) {
			$cid = 'EM' . date('ym') . $user->id;
			self::update(['cid' => $cid], ['id'=>$user->id]);
		});
	}

}