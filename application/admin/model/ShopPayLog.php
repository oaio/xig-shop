<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
use think\Session;

/**
 * ShopPayLog.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/29/2018  4:35 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1229
 * @describe
 */
class ShopPayLog extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $insert = ['username'];
	protected $admin_id = 0;

	protected function initialize()
	{
		parent::initialize();
		$this->admin_id = Session::get('admin.id');
	}

	public function base($query)
	{
		$query->alias('a')
			->join('fa_shop_order o', 'a.trade_no = o.snorder', 'LEFT')
			->where(['a.admin_id' =>$this->admin_id])
			->field('a.*,o.admin_id as sid');
	}

	/**
	 * 设置用户名
	 *
	 * @param $value
	 * @param $data
	 * @return mixed
	 * @throws \think\exception\DbException
	 */
	public function setUsernameAttr($value, $data)
	{
		$user = ShopUserinfo::get(['id'=>$data['uid']]);
		unset($value);
		return $user->name;
	}

	/**
	 * 修改显示的订单编号
	 *
	 * @param $value
	 * @param $data
	 * @return mixed
	 */
	public function getTradeNoAttr($value, $data)
	{
		$length = strlen($value);
		if($length>10 && $this->admin_id != $data['sid']){
			$value = substr($value,0, 8) . '********' . substr($value, $length-4);
		}
		return $value;
	}

	public function buy()
	{
		return $this->hasOne('shopOrder', 'snorder', 'trade_no')
			->field('snorder,store_id');
	}
}