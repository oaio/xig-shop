<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * GroupBuying.php 参团表
 *
 * @author d3li <d3li@sina.com>
 * @create：2/28/2019  10:01 AM
 * @see      https://gitee.com/d3li/February
 * @version 2.02.28
 * @describe
 */
class GroupBuyingDetails extends Model
{
	public function team()
	{
		return $this->belongsTo('GroupBuying', 'group_id')->setEagerlyType(0);
	}
	public function user()
	{
		return $this->hasOne('ShopUserinfo', 'id', 'user_id')
			->field('id,name,nickname,phone,address');
	}
}