<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use app\admin\library\Auth;
use think\Model;
/**
 * OpHistory.php 操作日志
 *
 * @author d3li <d3li@sina.com>
 * @create：1/14/2019  2:15 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0114
 * @describe
 */
class OpHistory extends Model
{
	// 开启自动写入时间戳字段
	protected $autoWriteTimestamp = 'int';
	// 定义时间戳字段名
	protected $createTime = 'createtime';
	protected $updateTime = '';
	public static function record($title = '')
	{
		$auth = Auth::instance();
		dump($auth);
	}

	public function admin()
	{
		return $this->belongsTo('Admin', 'admin_id')
			->field('username')
			->setEagerlyType(0);
	}

	public function store()
	{
		return $this->belongsTo('Store', 'admin_id', 'admin_id')->setEagerlyType(0);
	}

}