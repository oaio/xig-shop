<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use PDO;
use think\Model;
use think\model\Merge;
/**
 * ShopUserinfo.php 用户模型
 *
 * @author d3li <d3li@sina.com>
 * @create：12/29/2018  1:47 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1229
 * @describe
 */
class ShopUserinfo extends Model
{
/*	protected $relationModel = ['s'=>'fa_store'];
	protected $pk = 'store_id';

	// 定义关联外键
	protected $fk = 'store_id';
	protected $mapFields = ['sid' =>'s.store_id','store_id'=>'ShopUserinfo.store_id',
		'mid'=>'s.id','id'=>'ShopUserinfo.id'];*/

	public function getEmailAttr($value, $data)
	{
		return $data['vip_end_time']>$_SERVER['REQUEST_TIME'] ? __('Yes') : __('No');
	}

	public function s()
	{
		return $this->hasOne('Store','store_id', 'store_bind');
	}

	public function store()
	{
		return $this->hasOne('Store','store_id', 'store_id');
	}
    public function getStatusTextAttr($value,$data)
    {
        $status = [-1=>'删除',0=>'禁用',1=>'正常',2=>'待审核'];
        return $status[$data['status']];
    }

    public function admin()
    {
    	return $this->hasOne('Store', 'store_id', 'store_bind');
    }

    public function lottery()
    {
    	return $this->hasOne('\app\admin\model\shop\EffectiveBean', 'userid');
    }

    public function city()
    {
    	return $this->hasOne('Region', 'region_id', 'use_city_code');
    }

	public function area()
	{
		return $this->hasOne('Region', 'region_id', 'area_id');
	}

	public function faShopUserinfo()
	{
		return $this->hasOne('ShopUserinfo', 'id');
	}
}