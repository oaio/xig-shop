<?php
namespace app\admin\model\shopuser;

use think\Db;
use think\Model;
use think\Log;
use think\Hook;
use think\Validate;
use app\common\model\Common;

class User extends Model{
  protected $name='shop_userinfo';
  // 开启自动写入时间戳字段
  protected $autoWriteTimestamp = 'int';
  protected $insert = ['cid'];

  public function setCidAttr($value)
  {
    $value='XGB'.create_no();
    return $value;
  }
  public function base($query){
     $query->with(['invitebargain']);
  }

/**
   * 用户信息列表  
   * @Author   ksea
   * @DateTime 2018-11-20T11:03:45+0800
   * @param    string                   $search   [昵称，手机，联系名称，用户编号]
   * @param    string                   $paginate [几条数据一页]
   * @param    string                   $other    [其查询属性，存储格式二位数组]
   * @param    string                   $order    [顺序(asc)，倒叙(desc)]
   * @param    string                   $is_page    []
   * @return   [type]                             [description]
   */ 
  public  function get_user($search='',$paginate='20',$other=array(),$order='desc',$is_page=true){
    
        if(!empty($search)){
          if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3];
            $is_page=$temp[4];
            if(isset($temp[5])){
              $admin_id=$temp[5];
            }
          }
          $where=$whereOr=[];
          if($search){
            if(is_numeric($search)) {
              $where['a.phone'] = array('like', "%{$search}%");
            }else {
              $where['a.name'] = array('like', "%{$search}%");
            }
            //$whereOr['sn_info']=array('like',"%{$search}%");
          }
            if(isset($admin_id)&&!empty($admin_id)){
                $where['a.admin_id']=$admin_id;
            }
//            $where['a.type']=1;
          if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                  if(!empty($value)||isset($value)){
                      if($key!='status'&&$key!='id'&&$key!='type'){
                          $where[$key]=['like',"%{$value}%"];
                      }
                      else{
                          $where['a.'.$key]=$value;
                      }
                  }
            }
          }
      if($is_page){
                $data=$this->alias('a')
//            ->join('fa_shop_address b','a.addressid = b.id','left')
            ->where($where)
            ->whereOr($whereOr)
            ->paginate($paginate);
      }
      else{

        $data=$this->alias('a')
            ->join('fa_shop_address b','a.addressid = b.id','left')
            ->where($where)
            ->whereOr($whereOr)
            ->field('a.*,b.id as adds_id,b.name as adds_name,b.phone as adds_phone,b.address as adds_address ,b.region')
            ->select();
      }
        }
        else{
          if($is_page){
        $data=$this->alias('a')->join('fa_shop_address b','a.addressid = b.id')->field('a.*,
          b.id as adds_id,
          b.name as adds_name,
          b.phone as adds_phone,
          b.address as adds_address,
          b.region')->paginate($paginate);
          }
          else{
        $data=$this->alias('a')->join('fa_shop_address b','a.addressid = b.id')->field('a.*,
          b.id as adds_id,
          b.name as adds_name,
          b.phone as adds_phone,
          b.address as adds_address,
          b.region')->select();
          }
        }
        if($is_page)
        {
          $page=$data->render();
          return [$data,$page];
        }
    else
    {
      return $data;
    }
  }
  
  /**
   * 数据更新
   * @Author   ksea
   * @DateTime 2019-01-15T14:45:26+0800
   * @param    array                    $dat [description]
   * @return   [type]                        [description]
   */
  public function up_user($dat=array()){
      $allow=[
        'pic',
        'province_id',
        'province',
        'city_id',
        'city',
        'area_id',
        'area',
        'address',
        'use_city_code',
      ];
      if($dat&&!empty($dat['id'])){
          $id=$dat['id'];
          unset($dat['id']);
          $this->allowField($allow)->save($dat,['id'=>$id]);
          return ['code'=>1,'msg'=>'更新成功'];
      }
      else{
          return ['code'=>0,'msg'=>'没有更新条件'];
      }

  }

  /**
   *  用户添加
   * @Author   ksea
   * @DateTime 2018-11-28T14:12:50+0800
   * @param    [type]                   $post_data [description]
   * @param    integer                  $pid       [description]
   */
  public function add_user($post_data){
      $pid=isset($post_data['pid'])?$post_data['pid']:0;
      $data_validate=array(
      'unionid' => isset($post_data['unionid'])?$post_data['unionid']:'',
      );
      $rule=[
      'unionid'=>'require|unique:shop_userinfo',
      ];
      $msg=[
      'unionid.unique' =>'用户已经注册或者微信已经绑定',
      'unionid.require' =>'微信openid不能为空',
      'type.require' =>'用户类型定义错误 type,不允许为空',
      ];

      if(validate()->make($rule,$msg)->check($data_validate)){
          Db::startTrans();
          try {

              $data=
              [
              'name'=>isset($post_data['name'])?$post_data['name']:'',
              'nickname'=>isset($post_data['nickname'])?$post_data['nickname']:'',
              'passwd'=>isset($post_data['passwd'])?md5($post_data['passwd']):'',
              'phone'=>isset($post_data['phone'])?$post_data['phone']:'',
              'email'=>isset($post_data['email'])?$post_data['email']:'',
              'pid'=>isset($post_data['pid'])?$post_data['pid']:'',
              'openid'=>isset($post_data['openid'])?$post_data['openid']:'',
              'mini_openid'=>isset($post_data['mini_openid'])?$post_data['mini_openid']:'',
              'unionid'=>isset($post_data['unionid'])?$post_data['unionid']:'',
              'sn_info'=>"US".create_no(),
              'worker_type'=>isset($post_data['worker_type'])?$post_data['worker_type']:'',
              'pic'=>isset($post_data['pic'])?$post_data['pic']:'',
              'json_wechat'=>isset($post_data['json_wechat'])?$post_data['json_wechat']:'',
              'create_time' => time(),
              'update_time' => time(),
              'store_id' => isset($post_data['mstore_id'])?$post_data['mstore_id']:'',
              ];

              $r=$this->create($data,true);
              if(isset($post_data['pid'])&&$post_data['pid']!=0)$post_data['id_link']=$this->where("id={$pid}")->value('id_link');
              
              if(isset($post_data['id_link'])){
                $post_data['id_link'].=','.$r['id'];
              }
              else{
                $post_data['id_link']='0,'.$r['id'];
                }

                $this->where("id",'=',$r['id'])->update(['id_link'=>$post_data['id_link']]);

                $res['code']=1;
                $res['mesage']='添加成功';
                $res['data']=$r['id'];
              /*********红包钩子**********/
              if(true){
                $param['uid']=$r['id'];
                $param['func'] ='send_hb_toregist';
                Hook::listen('send_hb_toregist',$param);
              }
              /*********红包钩子**********/

              /*********个人小店钩子*********/
              if(true){
                Hook::listen('add_store',$r);
              }
              /*********个人小店钩子*********/

              /*********生成基础二维码列*********/
              if(true){
                Hook::listen('base_code',$r);
              }
              /*********生成基础二维码列*********/
              
              /*********生成各人二维码*********/
              if(true){
                $param_mycode['func']='set_code';
                $param_mycode['request']['id']=$r['id'];
                Hook::listen('set_code',$param_mycode);
              }
              /*********生成各人二维码*********/

              /*********粉丝二维码*********/
              if(true){
                $fans_code['request']['id']=$r['id'];
                Hook::listen('fans_code',$fans_code);
              }
              /*********粉丝二维码*********/

              session('user',Db::name('shop_userinfo')->where('id',$r['id'])->find());
              Db::commit();
          } catch ( \Exception $e) {
              Log::init([
              'add_user'=>[
              'func' => 'add_user',
              'msg'  => $e,
              'time' =>date('Y/m/d H:i:s'),
              ]
              ]);
              $res['code']=2;
              $res['mesage']='用户添加失败';
              Db::rollback();

              echo "<h1>请勿在微信昵称里面填写实体字符</h1>";
              die;
          }

      }
      else{
        return validate()->make($rule,$msg)->getError();
      }
      return $res;
  }


    public function bind($param){

        $id= $param['uid'];
        $mstore_id=$param['store_id'];

            $val_user=new Validate([
                'id' => 'require',

            ],[
                'id.require' => '账号不允许为空',
            ]);
            $data=[
                'id' =>$id,
                'store_id' =>$mstore_id,
            ];
            if($val_user->check($data)){
                Db::startTrans();
                try {

                        $dat=[
                            'user_id' =>$id,
                            'store_id' =>$mstore_id,
                            'create_time'=>time(),
                        ];
                        Db::name('store_user_history')->insert($dat);

                    Db::commit();
                } catch ( \Exception $e) {

                    Log::info([
                        'func'  => 'BindStore',
                        'error' => $e,
                        'data'  => $data,
                    ]);
                    Db::rollback();
                }
                return true;
            }
            else{
                return false;
            }
    }

    /**
     * 更新会员续费时间
     * @param  [type] $data ['uid','num','interval']
     * @return [type]       [description]
     */
    
    public function updateInterval($data){
        $success=[
          'code'  =>'1',
          'message'  =>'',
          'data'  =>'',
        ];
        $error=[
          'code'  =>'0',
          'message'  =>'',
          'data'  =>'',
        ];

        $val_user=new Validate([
                      'uid' => 'require',
                      'num' => 'require',
                      'interval' => 'require',

                  ],[
                      'uid.require' => '账号不允许为空',
                      'num.require' => '数目不允许为空',
                      'interval.require' => '有效时长不允许为空',
                  ]);

        $ckdata=[
            'uid'       => isset($data['uid'])?$data['uid']:'',
            'num'       => isset($data['num'])?$data['num']:'',
            'interval'  => isset($data['interval'])?$data['interval']:'',
        ];

        if($val_user->check($ckdata)){
          
           $user_data=$this->get($data['uid']);

           if(!empty($user_data)){

              $myuser=$user_data->toarray();

              /***********增加时长计算*****************/

              if(empty($myuser['vip_end_time'])||$myuser['vip_end_time']==0 || $myuser['vip_end_time'] < time()){

                  $now = date('Y-m-d H:i:s',time());
                  $step = strtotime("+ ".$data['interval']." months",strtotime($now));

              }
              else{
                  $step = strtotime("+ ".$data['interval']." months",$myuser['vip_end_time']);
              }
              /***********增加时长计算*****************/

              $this->where('id',$data['uid'])->update(['vip_end_time'=>$step]);
              $success['message']='更新成功';
              $success['data']=$step;
              return $success;
           }
           else{

              $error['data']=$user_data;
              $error['message']='账号不存在';
              return $error;

           }

        }
        else{

          $error['data']=$val_user->getError();
          $error['message']='数据不全';
          return $error;

        }
    }
    /**
     * 支付完成之后更新关注状态以及个人数据
     * @Author   ksea
     * @DateTime 2019-03-02T10:32:41+0800
     * @param    [type]                   $uid         [用户id]
     * @param    [type]                   $json_wechat [用户数据]array()
     * @return   [type]                                [description]
     */
    public function subscribe($uid,$json_wechat){

      $u_data=$this->where(['id'=>$uid])->find();
      /**
       * 当前没关注
       * @var string
       */
      $mystring    = '{"subscribe":0';
      $pos = stripos($u_data['json_wechat'],$mystring);
      /**
       * 以及关注传入数据
       * @var string
       */
      $mystring1    = '{"subscribe":1';
      $pos1 = stripos(json_encode($json_wechat),$mystring1);

      if (is_numeric($pos)&&is_numeric($pos1)) {
           $up_date['json_wechat']=json_encode($json_wechat);
           $up_date['pic']=$json_wechat['headimgurl'];
           $up_date['nickname']=$json_wechat['nickname'];

           $this->save($up_date,['id'=>$uid]);

           if($this->id){
                Log::init([
                   'subscribe'=>[
                    'func' => 'subscribe',
                    'msg'  => $up_date,
                    'time' =>date('Y/m/d H:i:s'),
                    'type'  =>'关注数据修改成功',
                   ]
                ]);
                session('user',Db::name('shop_userinfo')->where(['id'=>$uid])->find());
           }
           else{
                Log::init([
                   'subscribe'=>[
                    'func' => 'subscribe',
                    'msg'  => $up_date,
                    'time' =>date('Y/m/d H:i:s'),
                    'type'  =>'关注数据修改失败',
                   ]
                ]);
           }
      }

    }
    /**
     * 小店关联
     * @Author   ksea
     * @DateTime 2019-04-01T17:45:15+0800
     * @return   [type]                   [description]
     */
    public function userStore(){
        return $this->hasMany('app\admin\model\ShopUserinfoStore','uid','id');
    }
    
    /**
     * 粉丝表关联
     * @Author   ksea
     * @DateTime 2019-04-15T21:14:04+0800
     * @return   [type]                   [description]
     */
    public function qrcode(){
        return $this->hasOne('UserCode','uid','id');
    }
    /**
     * 小哥豆
     * @Author   ksea
     * @DateTime 2019-05-04T20:29:31+0800
     * @return   [type]                   [description]
     */
    public function bean(){
       return $this->hasOne('app\admin\model\shop\EffectiveBean','userid');
    }
    /**
     * 用户抵扣卷相关数据(可使用)
     * @Author   ksea
     * @DateTime 2019-05-04T20:29:31+0800
     * @return   [type]                   [description]
     */
    public function invitebargain(){
      $where['status']=0;
      $where['validtime']=[
          'egt',
          time()   
      ];
      return $this->hasMany('app\admin\model\shop\FreeRoll','uid')->where($where);
    }
	/**
	 * 赠送小哥豆
	 *
	 * @author d3li 2019/7/5
	 * @return $this
	 */
	public function gift()
    {
    	return $this->hasOne('app\common\model\RedGift', 'uid')->where('got', 0);
    }
}