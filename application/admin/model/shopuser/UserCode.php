<?php

namespace app\admin\model\shopuser;

use think\Db;
use think\Model;
use think\Session;
use think\Validate;
use think\Hook;

/**
 * 用户二维码创建
 */
class UserCode extends Model
{
	public $name='shop_userinfo_code';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * 生成基础二维码
     * @Author   ksea
     * @DateTime 2019-04-15T14:35:56+0800
     * @param    [type]                   $uid [description]
     */
    public function add_code($uid){
        $dat=[
            'uid' =>$uid,
        ];
        $this->save($dat);
    }
}