<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;

class ServerCache extends Model{

	protected $name='server_cache';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    /**
     * [sadd description]
     * @Author   ksea
     * @DateTime 2019-04-24T10:33:26+0800
     * @param    [type]                   $dat [
     *                                         	cate_id=>'分类',
     *                                         	time_part=>'时间段',
     *                                         	serverid=>'预约id',
     *                                         	ap_time=>'时间段（01:00~06:00）',
     *                                         	ap_date=>'日期',
     *                                         
     * ]
     * @return   [type]                        [description]
     */
	public function add($dat){
		/******************修改预约记录锚点**********************/
		$validata_cache=new Validate([
			 'cate_id' =>'require',
			 'time_part' =>'require',
			 'ap_date' =>'require',
		]);
		$cache=[
			'cate_id'	=>isset($dat['cate_id'])?$dat['cate_id']:'',
			'time_part'	=>isset($dat['time_part'])?$dat['time_part']:'',
			'ap_date'	=>isset($dat['ap_date'])?$dat['ap_date']:'',
		];
		if(!$validata_cache->check($cache)){
			$res['code']=0;
			$res['message']=$validata_cache->getError();
			$res['data']=$cache;
			return $res;
		}
		$ret=model('admin/CategoryCache')->add($dat['cate_id'], $dat['time_part'], $dat['ap_date']);
		$dat['count']=$ret;
		/******************修改预约记录锚点**********************/

		$allowfield=['serverid','count','ap_time','ap_date'];
		$validate=new Validate([
			'serverid' =>'require',
			'count'    =>'require',
			'ap_time'  =>'require',
			'ap_date'  =>'require',
		]);
		if(!$validate->check($dat)){
			$res['code']=0;
			$res['message']=$validate->getError();
			$res['data']=$dat;
			return $res;
		}
		$this->allowField($allowfield)->save($dat);
		$res['code']=1;
		$res['message']='添加成功';
		$res['data']=$this->id;
		return $res;
	}
	/**
	 * [sever description]
	 * @Author   ksea
	 * @DateTime 2019-04-24T09:37:57+0800
	 * @return   [type]                   [description]
	 */
	public function sever(){

		$this->hasOne('app\\admin\model\\shop\\Serverorder','id','serverid');
	
	}
}