<?php

namespace app\admin\model\shop;

use think\Model;


class Employee extends Model
{
	protected $name = 'employee';
	/**
	 * 更新雇员表
	 * @Author   ksea
	 * @DateTime 2019-02-28T17:51:20+0800
	 */
	public function UpdataEmp($EmpTemp){
		$dat=$EmpTemp;
		if($dat&&!empty($dat['openid'])){
		  $this->allowField(true)->save($dat,['phone'=>$dat['phone']]);
		  return ['code'=>1,'msg'=>'更新成功'];
		}
		else{
		  return ['code'=>0,'msg'=>'没有更新条件'];
		}
	}

    /**
     * 获取职员数据
     * 完整查询
     * @Author   ksea
     * @DateTime 2019-04-25T17:38:48+0800
     * @param    string                   $limit   [限制条数]
     * @param    string                   $getTime [时间节点 ]
     * @param    string                   $search  [搜索定义]
     * @param    array                    $other   [自定义搜索数据]
     * @return   [type]                            [description]
     */
    public function getSf($cate='1',$limit='',$getTime='',$search='',$other=array()){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);

            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
                    if(empty($value)){
                        unset($other[$key]);
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
            /*****************自定义查询*******************/
            
            $query='';
            
            /*****************分类指向********************/
            if(!empty($cate)&&in_array($cate,config('aunt.can_see'))){
                $query="FIND_IN_SET({$cate},job)";
            }
            else{
                $query="FIND_IN_SET(0,job)";
            }
            /******************分类指向*******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'name',
                    'cid',
                    'nickname',
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            $where['openid']=array('neq','null');
            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->where($query)->order('id desc')->select();
                 $res['url']=config('mobileUrl.shopGoodsDetail');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

}