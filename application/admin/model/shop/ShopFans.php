<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;

/**
 * 粉丝
 */

class ShopFans extends Model
{

	protected $name='ShopFans';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

   /**
     * 粉丝数据
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function get_fans($uid,$limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);
            $where['fuid']=$uid;
            $where['status']=$status;
            $where['uid']=array('neq',$uid);

            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->order('id desc')->select();
                 $res['url']=config('mobileUrl.fansContribute');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']=config('mobileUrl.fansContribute');
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']=config('mobileUrl.fansContribute');
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

    /**
     * 粉丝绑定
     * @Author   ksea
     * @DateTime 2019-04-01T14:23:06+0800
     * @param    [type]                   $swtich [description]
     */
    public function SwitchFens($swtich){

        $Muser=model('admin/shopuser/user');

        $error=[
            'code'   =>0,
            'message'=>'',
            'data'   =>'',
        ];

        $success=$error;
        $success['code']=1;
        
        $from=$swtich['uid'];
        $to=$swtich['fuid'];

        $SwitchVali=new Validate([
            'fuid' =>'require',
            'uid'  =>'require',
        ]);

        if(!$SwitchVali->check($swtich)){
           $error['message']=$SwitchVali->getError();
           return $error;
        }

        $oldFans=$this->where(['status'=>1,'uid'=>$from])->find();
        $user=$Muser->where(['id'=>$from])->find();
        $thesame=$this->where(['status'=>1,'uid'=>$from,'fuid'=>$to])->find();
        
        $link=$this->where(['fuid'=>$user['id']])->value('fans_link');

        if(empty($link)){
            $fans_link='0';
        }
        else{
            $fans_link=$link;
        }

        if(empty($user)){
            $error['message']='用户不存在';
            return $error;
        }

        if (empty($to) || $user->id == $to) {
            $relationModel = model('WechatUserRelation');
            $relationInfo = $relationModel->where(['openid' => $user['openid']])->order('id ASC')->find();
            if (!empty($relationInfo) && !empty($relationInfo->userid)) {
                $to = $relationInfo->userid;
            }
        }

        if(empty($thesame)){

            if(!empty($oldFans)){
                // $update=[
                //     'status'   => 0,
                // ];
                // $this->update($update,['id'=>$oldFans->id]);



                // $data=[
                //     'userpic'    =>$user->pic,
                //     'name'       =>$user->name?$user->name:$user->nickname,
                //     'uid'        =>$user->id,
                //     'fuid'       =>$to,
                //     'oid'        =>'',
                //     'status'     =>'1',
                //     'sumorder'   =>'0',
                //     'sumsales'   =>'0',
                //     'sumoutcome' =>'0',
                //     'create_time'=>time(),
                //     'update_time'=>time(),
                // ];
                // $res=$this->insertGetId($data);

                // //更新
                // $fans_link=$res.','.$fans_link;
                // $this->update(['fans_link'=>$fans_link],['id'=>$res]);

                // $success['message']='却换成功';
                // $success['data']=$res;

            }
            else{
                //约束不能成为自己的粉丝
                //30秒内没有绑定粉丝关系的不可以绑定关系
                if($user->id!=$to&&(time()-$user->create_time)<30){

                    $data=[
                        'userpic'    =>$user->pic,
                        'name'       =>$user->name?$user->name:$user->nickname,
                        'uid'        =>$user->id,
                        'fuid'       =>$to,
                        'oid'        =>'',
                        'status'     =>'1',
                        'sumorder'   =>'0',
                        'sumsales'   =>'0',
                        'sumoutcome' =>'0',
                    ];
                    $this->save($data);
                    //更新
                    $fans_link=$this->id.','.$fans_link;
                    $id=$this->id;
                    $this->update(['fans_link'=>$fans_link],['id'=>$id]);

                    $success['message']='添加成功';
                    $success['data']=$this->id;

                }

            }
            return $success;


        }
        $success['message']='无更新';
        return $success;
    }
}
