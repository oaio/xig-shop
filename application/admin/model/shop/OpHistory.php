<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
//操作记录模型
class OpHistory extends Model{

	public function setlog($post_data){
		$module_name=$post_data['module_name'];
		$op_name=$post_data['op_name'];
		$remark=$post_data['remark'];
/*		dump(request()->controller());
		dump(request()->action());
		$controller=request()->controller();
		$action=request()->action();
		if($controller){

		}
		elseif () {
			# code...
		}
		elseif () {
			# code...
		}
		else{

		}
		die;*/
		$data=[
			'admin_id'=>session('admin.id'),
			'module_name'=>$module_name,
			'op_name'=>$op_name,
			'remark'=>$remark,
			'create_time'=>time(),
		];

		$res=$this->create($data,true);
		if($res->id){
			$ret=[
				'code'=>'1',
				'message'=>'记录成功',
				'data'=>$res,
			];
		}
		else{

			$ret=[
				'code'=>'0',
				'message'=>'记录失败',
				'data'=>$res,
			];
		}
		return $ret;
	}
}
