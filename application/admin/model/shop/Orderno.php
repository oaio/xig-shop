<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;

class Orderno extends Model{
  	protected $name='shop_trade_log';

  	public function add_orderno($dat){
       $data=
       [
        'order_typeid'=>$dat['order_typeid'],
        'orderno'=>'JO'.create_no(),
        'money'=>$dat['money'],
        'create_time'=>$dat['create_time'],
       ];
       $r=$this->create($data);
       return $r;

  	}
  	/**
  	 * 获取流水列表
  	 * @Author   baohb
  	 * @DateTime 2018-12-4T19:29:30+0800
  	 * @return   [type]                   [description]
  	 */
  	public function list_orderno($search='',$paginate='20',$other=array(),$order='desc',$is_page=true,$admin_id=''){
        if(!empty($search)){
          if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3];
            $is_page=$temp[4];
            if(isset($temp[5])){
              $admin_id=$temp[5];
            }
          }
          $whereOr=[];
          if($search){
            $whereOr['orderno']=array('like',"%{$search}%");
            $whereOr['money']=array('like',"%{$search}%");
          }
          $where=[];
          if($admin_id){
            $where['admin_id']=$admin_id;
          }
          if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                if(!empty($value)){
                    if($key!='id' && $key != "order_typeid"){
                        $where[$key]=['like',"%{$value}%"];
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
          }
      if($is_page){

        $data=$this
            ->where($where)
            ->whereOr($whereOr)
           ->paginate($paginate);


      }
      else{
        $data=$this
            ->where($where)
            ->whereOr($whereOr)
            ->select();
      }
        }
        else{
          if($is_page){
        $data=$this
            ->paginate($paginate);
          }
          else{
        $data=$this
            ->select();
          }
        }
        if($is_page)
        {
          $page=$data->render();
          return [$data,$page];
        }
    else
    {
      return $data;
    }
  	}

    /**
     * 某个流水的全部详情
     * @Author   baohb
     * @DateTime 2018-12-4T19:05:29+0800
     * @param    [type]                   $id [description]
     * @return   [type]                       [description]
     */
  	public function get_orderno_detail($id){
      $detail = $this->where('id','=',$id)->find()->getData();

      return $detail;
  	}


}