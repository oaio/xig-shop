<?php

namespace app\admin\model\shop;

use think\Model;

//操作记录模型
class AnalysisStore extends Model {

	protected $name = 'analysis_store';

	public function analysis($order) {

		/***********************店铺支付详情**************************/
		$whereStore = [
			'city_code' => $order['city_code'],
			'store_id' => $order['store_id'],
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisStore = model('admin/shop/AnalysisStore');

		$analysis_temp = $modelAnalysisStore->where($whereStore)->find();
		if (!$analysis_temp) {

			$AnalysisStoreData = [
				'city_code' => $order['city_code'],
				'store_id' => $order['store_id'],
				'userpaynum' => 1,
				'suserpaysum' => $order['shouldpay'],
				'auserpaysum' => $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] : 0,
				'paynum' => 1,
				'create_time' => strtotime(date('Y-m-d')),
			];

			$modelAnalysisStore->save($AnalysisStoreData);

		} else {

			$jaindan = model('admin/shop/order')->where([
				'is_pay' => 1,
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
				'uid' => $order['uid'],
			])->select();

			$paynum = $analysis_temp['paynum'];
			if (count($jaindan) == 1) { 
				$paynum =$paynum+1;
			}

			$AnalysisStoreData = [
				'userpaynum' => $analysis_temp['userpaynum'] + 1,
				'suserpaysum' => $analysis_temp['suserpaysum'] + $order['shouldpay'],
				'auserpaysum' => $analysis_temp['auserpaysum'] + $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] + $analysis_temp['discountsum'] : $analysis_temp['discountsum'],
				'paynum' => $paynum,
			];
			$analysis_temp->save($AnalysisStoreData);
		}
		/***********************店铺支付详情**************************/

	}
	public function store() {
		return $this->hasOne('Store', 'store_id', 'store_id');
	}
}
