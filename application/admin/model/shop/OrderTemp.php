<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
//订单暂存
class OrderTemp extends Model{

    // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
    protected $autoWriteTimestamp=true;
    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';

	  protected $name='shop_order_temp';

}
