<?php
namespace app\admin\model\shop;

use think\Model;

class User extends Model {
	protected $name = 'shop_userinfo';

	protected $autoWriteTimestamp = true;
	// 创建时间字段
	protected $createTime = 'create_time';
	// 更新时间字段
	protected $updateTime = 'update_time';

	public function addressData() {
		return $this->hasMany('Address', 'uid', 'id');
	}
	/**
	 * 个人免单卷数据
	 * @Author   ksea
	 * @DateTime 2019-07-13T16:06:00+0800
	 * @return   [type]                   [description]
	 */
	public function freeroll() {
		return $this->hasMany('FreeRoll', 'uid', 'id');
	}
	/**
	 * 归属订单
	 * @Author   ksea
	 * @DateTime 2019-07-31T16:50:40+0800
	 * @return   [type]                   [description]
	 */
	public function order(){
		return $this->hasMany('Order', 'uid', 'id')->where('is_pay',1);
	}
	public function getRed(){
		return $this->hasMany('RedEnvelopesRecord', 'userid', 'id')->where('type',1);
	}

	/**
	 * 绑定关系赠送兑换卷操作
	 * ksea
	 * 2019-07-17 22:05:42
	 * @param [type] $user [description]
	 */
	
	public function invite($user) {
		
		$conf = model('config')::get(['name' => 'invite']);
		list($config['total'], $config['eachget'], $config['onydayget'], $config['usercount'],
			$config['validtime']) = json_decode($conf['value'], 1);

		$fuser = model('admin/shop/user')->with(['freeroll'])->where('id', $user['pid'])->find()->toArray();

		$openid_arr = [
			$fuser['openid'],
		];

		$InviteModel = model('admin/shop/Invite');
		//确定关系
		$insertData = [
			'uid' => $user['id'],
			'fuid' => $user['pid'],
			'invite_city' => $user['use_city_code'],
			'origin' => '二维码',
			'status' => 0,
			'invitee' => $user['nickname'],
			'avatar' => $user['pic'],
			'tel' => $user['phone'],
		];
		$InviteModel->save($insertData);

		//统计
		$InviteUse = $InviteModel->where(['status' => 0, 'fuid' => $user['pid']])->select();

		$count = count($InviteUse);

		//有效人数达到
		if ($count >= $config['usercount']) {

			$countFreeroll = $fuser['freeroll'] ? count($fuser['freeroll']) : 0;

			//个人达到数目不发放
			if ($countFreeroll >= $config['eachget']) {
				myErrorLog('countfreeroll.txt', '个人达到数目不发放:' . json_encode([$countFreeroll,$config['eachget'],$fuser, $config]), 'invite');
				return '个人达到数目不发放';
			}

			//库存用完不在放发放
			if ($config['total'] <= 0) {
				myErrorLog('total.txt', '库存用完不在放发放:' . json_encode([$config['total']]), 'invite');
				return '库存用完不在放发放';
			}

			//个人单天获取数据达到不发放

			$redis = new \Redis();

			$redis->connect('127.0.0.1', 6379);

			$temp_name = 'invite_'.date('Y-m-d') . '_' . $fuser['id'];

			if ($redis->get($temp_name) >= $config['onydayget']) {
				myErrorLog('onydayget.txt', '个人单天获取数据达到不发放:' . json_encode([$temp_name,$redis->get($temp_name), $config['onydayget']]), 'invite');
				return '个人单天获取数据达到不发放';
			}

			//发放操作
			$FreeData = [
				'uid' => $fuser['id'],
				'validtime' => time() + ($config['validtime'] * 60 * 60 * 24),
			];
			model('admin/shop/FreeRoll')->save($FreeData);

			//更新获取redis
			$redis->INCR($temp_name);

			//更新剩余总量
			$config['total']=$config['total']-1;

			$totalData=[
				'value' =>json_encode(array_values($config))
			];

			model('config')->where(['name' => 'invite'])->update($totalData);

			//通知操作
			$InviteModel = model('admin/shop/Invite');

			$data = [
				'first' => '小主，恭喜您成功邀请' . $config['usercount'] . '名好友，赠送您一张免单券！',
				'keyword1' => $fuser['nickname'],
				'keyword2' => $fuser['phone'],
				'keyword3' => date('Y-m-d H:i:s', $fuser['create_time']),
				'remark' => '点击立即领取哦！',
			];

			$link = config('wechattemplate.bargain')['link'];
			$template_id = config('wechattemplate.bargain')['tempid'];
			$myGrab = new \wechat\DiytTemplate($template_id);
			$reds = $myGrab::SendGroup($openid_arr, $data, $link);
			//更新抵扣数据
			$uidArr = [];

			foreach ($InviteUse as $key => $value) {
				array_push($uidArr, $value->id);
			}

			$uidArr = implode($uidArr, ',');
			$wher['id'] = array('in', $uidArr);
			$InviteModel->where($wher)->update(['status' => 1]);

		}
	}

}