<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//免单卷模型
class FreeRoll extends Model{

	protected $name='shop_free_roll';
	
    protected $autoWriteTimestamp=true;

	public function user()
	{
		return $this->hasOne('user', 'id', 'uid');
	}

	public function m()
	{
		return $this->hasOne('FreeRoll', 'id', 'id');
	}

	public function getStatusAttr($v, $row)
	{
		return !$v && $row['validtime'] <  $_SERVER['REQUEST_TIME']  ? 2 : $v;
	}

}
