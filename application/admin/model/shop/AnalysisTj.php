<?php

namespace app\admin\model\shop;

use think\Model;

//操作记录模型
class AnalysisTj extends Model {

	protected $name = 'analysis_tj';

	public function analysis($order) {

		/***********************推荐店铺数**************************/
		$whereTj = [
			'tj' => isset($order['tj']['invite_phone']) ? $order['tj']['invite_phone'] : '',
			'tjname' => isset($order['tj']['tjname']) ? $order['tj']['tjname'] : '',
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisTj = model('admin/shop/AnalysisTj');

		$analysis_temp = $modelAnalysisTj->where($whereTj)->find();

		if (!$analysis_temp) {

			$AnalysisTjData = [
				'tjstorecount' => 1,
				'payusercount' => 1,
				'suserpaysum' => $order['shouldpay'],
				'auserpaysum' => $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] : 0,
				'tj' => isset($order['tj']['invite_phone']) ? $order['tj']['invite_phone'] : '',
				'tjname' => isset($order['tj']['tjname']) ? $order['tj']['tjname'] : '',
				'create_time' => strtotime(date('Y-m-d')),
				'tjstore_link' => $order['store_id'],
			];

			$modelAnalysisTj->save($AnalysisTjData);

		} else {

			$AnalysisTjData = [
				'suserpaysum' => $analysis_temp['suserpaysum'] + $order['shouldpay'],
				'auserpaysum' => $analysis_temp['auserpaysum'] + $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] + $analysis_temp['discountsum'] : $analysis_temp['discountsum'],
			];

			$ckstore = $modelAnalysisTj->where("FIND_IN_SET(" . $order['store_id'] . ",tjstore_link)")->select();
			if (count($ckstore) == 0) {
				$AnalysisTjData['tjstorecount'] = $analysis_temp['tjstorecount'] + 1;
				$AnalysisTjData['tjstore_link'] = $analysis_temp['tjstore_link'] . ',' . $order['store_id'];
			}

			$ckorder = model('admin/shop/order')->where([
				'uid' => $order['uid'],
				'store_id' => $order['store_id'],
				'is_pay' => 1,
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();
			if (count($ckorder) == 1) {
				$AnalysisTjData['payusercount'] = $analysis_temp['payusercount'] + 1;
			}
			$analysis_temp->save($AnalysisTjData);
		}
		/***********************推荐店铺数**************************/

	}
}
