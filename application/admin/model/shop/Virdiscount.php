<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;
use think\Session;

class Virdiscount extends Model{

	//数据初始化
	//INSERT INTO fa_effective_bean (userid) (SELECT id FROM fa_shop_userinfo)
	
	protected $name='Virdiscount';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected $uid=0;

	protected function initialize(){
	    parent::initialize();
	    $this->uid = Session::get('user.id');
	}


	/**
	 * 创建添加
	 * @Author   ksea
	 * @DateTime 2019-05-05T09:57:04+0800
	 * @param    [type]                   $bean [description]
	 * @param    string                   $uid  [description]
	 * @param    string                   $name [description]
	 * @param    string                   $type [description]
	 * 
	 */
	public static function addDis($bean,$uid='',$name='',$type=''){

		$bean=is_numeric($bean)?$bean:0;
		$EffectiveBean=model('admin/shop/EffectiveBean')->where('userid',$uid)->find();
		if($EffectiveBean['effective_bean'] < $bean){
			$dat=[
				'code'	  =>0,
				'message' =>'豆不足',
				'data'	  =>$EffectiveBean,
			];
			return $dat;
		}
		$per=Db::name('config')->where(['group'=>'xgbpeas'])->value('value');
		$dismoney=$bean*$per;
		$dat=[
			'name'	  =>empty($name)?'小哥豆直接兑换':$name,
			'uid'	  =>isset($uid)?$uid:Session::get('user.id'),
			'bean'	  =>$bean,
			'is_pay'  =>0,
			'dismoney'=>$dismoney,
			'type'	  =>0,
		];
		$allowfield=['name','uid','bean','is_pay','dismoney'];
		$validate=new Validate([
			'name' 	 	 =>'require',
			'uid'    	 =>'require',
			'bean'	     =>'require',
			'is_pay'	 =>'require',
			'dismoney'	 =>'require',
		]);
		if(!$validate->check($dat)){
			$res['code']=0;
			$res['message']=$validate->getError();
			$res['data']=$dat;
			return $res;
		}
		$rt=self::create($dat);
		$res['code']=1;
		$res['message']='添加成功';
		$res['data']=$rt['id'];
		$res['dismoney']=$dismoney;
		$res['type']=$dat['type'];
		return $res;
	}
	/**
	 * 1.用户支付成功
	 * 2.支付预购买小哥豆商品
	 * 3.扣除个人小哥豆
	 * 4.可选同步钩子(返利)
	 * @Author   ksea
	 * @DateTime 2019-05-05T09:57:19+0800
	 * @return   [type]                   [description]
	 */
	public function payDis($id,$oid=''){
		
		$validate=new Validate([
			'id' 	 	 =>'require',
		]);
		if(!$validate->check(['id'=>$id])){
			$res['code']=0;
			$res['message']=$validate->getError();
			$res['data']=$id;
			return $res;
		}

		$data=$this->get($id);

		if($data->is_pay==1){
			$res['code']=0;
			$res['message']='已经支付,请勿重复支付';
			$res['data']=$id;
			myErrorLog('paydis.txt',$res,'virdiscount');
			return $res;
		}

		$this->save(['is_pay'=>1],['id'=>$id]);

		$mybean=model('admin/shop/EffectiveBean')->where('userid',$data['uid'])->value('effective_bean');

		$newbean=$mybean-$data['bean'];
		if($newbean>=0){
			$update_data=[
				'effective_bean'=>$newbean,
			];
			$rt=model('admin/shop/EffectiveBean')->where('userid',$data['uid'])->update($update_data);
			$res['code']=1;
			$res['message']='预支付成功';
			$res['data']=$rt;
			if(!empty($oid)){
				RedEnvelopesRecord::add(2,$data['bean'],$oid);
			}
			model('BeanLog')->add(['cost' => $data['bean'],
				'uid' => $data['uid'], 'kind' => 2], [$mybean, $newbean]);
		}
		else{
			$res['code']=0;
			$res['message']='小哥豆剩余，余额不足';
			$res['data']=$data;
			myErrorLog('paydis.txt',$res,'virdiscount');
		}
		return $res;
	}

}