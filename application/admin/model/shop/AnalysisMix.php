<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//操作记录模型
class AnalysisMix extends Model{

	protected $name='analysis_mix';
	

	/**
	 * 最后补全数据
	 * 可有可无
	 * @Author   ksea
	 * @DateTime 2019-08-01T17:51:40+0800
	 * @return   [type]                   [description]
	 */
	public function endtime(){

	}

	public function loginlog($user,$orgin){

		$city_id=model('admin/shop/Store')->
				 where('store_id',$user['store_id'])->
				 value('city_id');

		if(!$city_id){
			$city_id='440300';
		}

		$whereMix = [
			'orgin' => $orgin,
			'city_code' => $city_id,
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisMix = model('admin/shop/AnalysisMix');

		$analysis_temp = $modelAnalysisMix->where($whereMix)->find();

		if(!$analysis_temp){

			$AnalysisMixData = [
				'city_code'   => $city_id,
				'orgin'       => $orgin,
				'create_time' => strtotime(date('Y-m-d')),
				'loginin'     => 1,
				'uid_link'    => $user['id']
			];

			$modelAnalysisMix->save($AnalysisMixData);

		}else{

			$AnalysisMixData = [
				'loginin'  => $analysis_temp['loginin']+1,
			];
			
			$setFindIn=$this->where("FIND_IN_SET(" . $user['id'] . ",uid_link)")->find();

			if(!$setFindIn){
				$AnalysisMixData['uid_link']=empty($analysis_temp['uid_link'])?$user['id']:$analysis_temp['uid_link'].','.$user['id'];
			}
			$analysis_temp->save($AnalysisMixData);
		
		}

		
	}

	public function bindlog($user,$orgin){

		$city_id=model('admin/shop/Store')->
				 where('store_id',$user['store_id'])->
				 field('city_id')->
				 find();

		if(!$city_id){
			$city_id='440300';
		}

		$whereMix = [
			'orgin' => $orgin,
			'city_code' => $city_id,
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisMix = model('admin/shop/AnalysisMix');

		$analysis_temp = $modelAnalysisMix->where($whereMix)->find();

		if(!$analysis_temp){

			$AnalysisMixData = [
				'city_code'   => $city_id,
				'orgin'       => $orgin,
				'create_time' => strtotime(date('Y-m-d')),
				'reviewuser'  =>1,
				'uid_link'    => $user['id'],
			];

			$modelAnalysisMix->save($AnalysisMixData);

		}else{

			$AnalysisMixData = [
				'reviewuser'  =>$analysis_temp['reviewuser']+1,
			];

			$setFindIn=$this->where("FIND_IN_SET(" . $user['id'] . ",uid_link)")->find();

			if(!$setFindIn){
				$AnalysisMixData['uid_link']=empty($analysis_temp['uid_link'])?$user['id']:$analysis_temp['uid_link'].','.$user['id'];
			}

			$analysis_temp->save($AnalysisMixData);
		
		}

		
	}
	// public function redislogin($uid){
	      
	//       /**************记录用户登陆次数(一天校验一次)**************/
	//       $uid=session('user.id');
	//       $redis_data='Y-m-d';
	//       //如果授权，就当这个人登陆
	//       $redis = new \Redis();
	//       $redis->connect('127.0.0.1', 6379);
	//       $temp_name = 'login_'.date($redis_data);
	//       if($redis->scard($temp_name)>0){
	//          if($redis->sismember($temp_name,$uid)==0){
	//             $redis->sadd($temp_name,$uid);
	//          }
	//       }else{
	//         $redis->sadd($temp_name,$uid);
	//       }
	//       /*************记录用户登陆次数(一天校验一次)***************/
	// }

	public function analysis($order){

        /***********************综合分析**************************/

		$user=  model('admin/shop/ShopFans')->
		        alias('fans')->
		        join('fa_shop_userinfo user','fans.fuid=user.id')->
		        where('fans.uid',$order['uid'])->
		        field('user.*')->
		        find();
		
		if($user){
			$user=$user->toArray();
		}
		else{
			$user['store_bind']=0;
		}

		$whereMix = [
			'orgin' => $order['orgin'],
			'city_code' => $order['city_code'],
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisMix = model('admin/shop/AnalysisMix');

		$analysis_temp = $modelAnalysisMix->where($whereMix)->find();
		if (!$analysis_temp) {

			$AnalysisMixData = [
				'city_code' => $order['city_code'],
				'orgin' => $order['orgin'],
				'create_time' => strtotime(date('Y-m-d')),
			];

			$temp1 = model('admin/shop/order')->where([
				'store_id' => [
					'=',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'<>',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();
			if (count($temp1) < 2) {
				$AnalysisMixData['payonline'] = 1;
			}

			$temp2 = model('admin/shop/order')->where([
				'store_id' => [
					'>',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'=',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();

			if (count($temp2) == 1) {
				$AnalysisMixData['paystorenum'] = 1;
			}

			$temp3 = model('admin/shop/order')->where([
				'store_id' => [
					'=',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'<>',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();

			if (count($temp3) == 1 && $user['store_bind'] > 1) {
				$AnalysisMixData['payonlinestorenum'] = 1;
			}

			$modelAnalysisMix->save($AnalysisMixData);

		} else {

			$AnalysisMixData = [
				'city_code' => $order['city_code'],
				'orgin' => $order['orgin'],
				'create_time' => strtotime(date('Y-m-d')),
			];

			$temp1 = model('admin/shop/order')->where([
				'store_id' => [
					'=',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'<>',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();
			if (count($temp1) < 2) {
				$AnalysisMixData['payonline'] = isset($analysis_temp['payonline']) ? $analysis_temp['payonline'] + 1 : 1;
			}

			$temp2 = model('admin/shop/order')->where([
				'store_id' => [
					'>',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'=',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();

			if (count($temp2) == 1) {
				$AnalysisMixData['paystorenum'] = isset($analysis_temp['paystorenum']) ? $analysis_temp['paystorenum'] + 1 : 1;
			}

			$temp3 = model('admin/shop/order')->where([
				'store_id' => [
					'=',
					'1',
				],
				'is_pay' => 1,
				'uid' => $order['uid'],
				'eventtype' => [
					'<>',
					'storescan',
				],
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();
			if (count($temp3) == 1 && $user['store_bind'] > 1) {
				$AnalysisMixData['payonlinestorenum'] = isset($analysis_temp['payonlinestorenum']) ? $analysis_temp['payonlinestorenum'] + 1 : 1;
			}
			$analysis_temp->save($AnalysisMixData);

		}
        /***********************综合分析**************************/

	}
}
