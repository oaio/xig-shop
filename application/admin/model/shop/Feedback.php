<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;

class Feedback extends Model{
  	protected $name='shop_feedback';

  	/**
  	 * 获取意见反馈列表
  	 * @Author   baohb
  	 * @DateTime 2018-12-7T19:29:30+0800
  	 * @return   [type]                   [description]
  	 */
  	public function feedback_list($search='',$paginate='20',$other=array(),$order='desc',$is_page=true,$admin_id=''){
        if(!empty($search)){
          if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3];
            $is_page=$temp[4];
            if(isset($temp[5])){
              $admin_id=$temp[5];
            }
          }
          $whereOr=[];
          if($search){
            $whereOr['title']=array('like',"%{$search}%");
            $whereOr['feedback']=array('like',"%{$search}%");
          }
          $where=[];
          $where['admin_id']=$admin_id;
          if(isset($admin_id)&&!empty($admin_id)){
             $where['admin_id']=$admin_id;
          }
          if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                if(!empty($value)){
                    if($key!='id' && $key != "is_type"){
                        $where[$key]=['like',"%{$value}%"];
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
          }
      if($is_page){

        $data=$this->alias('feek')
            ->join('shop_userinfo u','feek.uid =u.id','left')
            ->where($where)
            ->whereOr($whereOr)
            ->paginate($paginate);
      }
      else{
        $data=$this
            ->where($where)
            ->whereOr($whereOr)
            ->select();
      }
        }
        else{
          if($is_page){
        $data=$this
            ->paginate($paginate);
          }
          else{
        $data=$this
            ->select();
          }
        }
        if($is_page)
        {
          $page=$data->render();
          return [$data,$page];
        }
    else
    {
      return $data;
    }
  	}
}