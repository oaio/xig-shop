<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Log;

class Goods extends Model{

    protected $name='shop_goods';

    protected $autoWriteTimestamp = true;

    public function base($query){
        $query->with(['bargainData','detailData']);
    }

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    public function add_goods($dat)
  {
        if(isset($dat['pic'])&&is_array($dat['pic'])) $dat['pic']= implode(',', $dat['pic']);

        if(isset($dat['inoc'])&&is_array($dat['inoc'])) $dat['inoc']= implode(',', $dat['inoc']);

        if(isset($dat['sku'])&&is_array($dat['sku'])) $dat['sku']= implode(',', $dat['sku']);

        if(isset($dat['status'])==true){
            $cate = $dat['status'];
        }
        if(isset($dat['city_code'])){
            $city_code = $dat['city_code'];
        }elseif(session('admin.city_code')){
            $city_code = session('admin.city_code');
        }else{
            $city_code = 1;
        }
       $data=
       [
           'name'=>$dat['name'],
           'commonprice'=>$dat['commonprice'],
           'brief'=>$dat['brief'],
           'times'=>$dat['times'],
           'power' =>$dat['power'],
           'vipprice' =>$dat['vipprice'],
           'costprice' =>$dat['costprice'],
           'unit'=>$dat['unit'],
           'cate'=>$dat['status'],
           'pic'=>$dat['pic'],
           'inoc'=>$dat['inoc'],
           'grabprice'=>$dat['grabprice'],
           'is_grob'=>$dat['is_grob'],
           'virtual_times'=>$dat['virtual_times'],
           'lib_type'=>isset($dat['lib_type'])?$dat['lib_type']:0,
           'libid'=>isset($dat['libid'])?$dat['libid']:'',
           'bak'=>$dat['bak'],
           'update_time' =>time(),
           'status'=>0,
           'create_time' =>time(),
           'update_time' =>time(),
           'sngoods'=>'GD'.create_no(),
           'city_code'=>$city_code,
           'is_scan'=>$dat['is_scan'],
       ];
       if($dat['admin_id']){
           $data['admin_id']=$dat['admin_id'];
       }
       $r=$this->create($data);
       Db::name('shop_goods_detail')->insert(
        [
          'detail'  => $dat['detail'],
          'is_use'  => 1,
          'goodsid' => $r['id']
        ]
       );
       if($r){
         $ret=[
           'code' => 1,
           'message' => '添加成功',
           'data' => $r['id'],
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
       }
       return $ret;
    }
    /**
     * 获取商品列表
     * @Author   ksea
     * @DateTime 2018-11-13T19:29:30+0800
     * @return   [type]                   [description]
     */
    public function show_goods_list($search=array())
  {
        if(input('param.page','')&&session('s_show_goods_list')){
            $search=session('s_show_goods_list');
        }
        else{
            session('s_show_goods_list',$search);
        }

        $temp_search=$search;
        if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3]?:'desc';
            $is_page=$temp[4];
            if(isset($temp[5])){
                $admin_id=$temp[5];
            }
        }
        $where=[];
        if(isset($admin_id)&&!empty($admin_id)){
            if(session('admin.type')!=2){
              $where['sg.admin_id']=$admin_id;
            }
        }
        if (!empty($temp_search[2]['cate'])){
            $where['sg.cate']=$temp_search[2]['cate'];
        }
        else{
            $dlist=model('admin/shop/Categrory')->where('status=1')->column('id');
            $where['sg.cate']=array('in',$dlist);
        }
        $where['is_del']=array('eq','0');//删除不显示
        $where['origin_table']='shop_goods';//其他来源添加不显示
        if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                if(empty($value)){
                    unset($other[$key]);
                }
                else{
                    $where[$key]=$value;
                }
            }
        }
      $city_code = session('admin.city_code');
      if(session('admin.type') == 2 && $city_code){
//            unset($where['sg.admin_id']);
          $where['city_code'] = array('in', $city_code);
      }
        if($search){
            $san_like=[
                'sg.name',
                'sg.sngoods',
            ];
            $link_scane=implode('|', $san_like);
            $data=$this->alias('sg')->join('fa_shop_categrory sc','sg.cate=sc.id','left')
                ->field('sg.*,sc.name catename')
                ->where($link_scane,'like',"%{$search}%")
                ->where($where)
                ->order('sg.id desc')
                ->paginate($paginate);

        }
        else{
            $data=$this->alias('sg')->join('fa_shop_categrory sc','sg.cate=sc.id','left')
                ->field('sg.*,sc.name catename')
                ->where($where)
                ->order('sg.id desc')
                ->paginate($paginate);

        }

        //获取地区数据
        $city_list = Db::table('fa_city')->order('id desc')->column('city_name','city_code');
        $page=$data->render();
        return [$data,$page,$temp_search,$city_list];

    }
    /**
     * 商品更行处理
     * @Author   ksea
     * @DateTime 2018-11-13T19:40:42+0800
     * @param    [type]                   $id [商品id]
     * @return   [type]                       [description]
     */
    public function update_goods($dat)
    {
        if(isset($dat['pic'])&&is_array($dat['pic'])) $dat['pic']= implode(',', $dat['pic']);

        if(isset($dat['inoc'])&&is_array($dat['inoc'])) $dat['inoc']= implode(',', $dat['inoc']);

        if(isset($dat['sku'])&&is_array($dat['sku'])) $dat['sku']= implode(',', $dat['sku']);

       $data=
       [
          'name'=>$dat['name'],
          'cate'=>$dat['cate'],
          'commonprice'=>$dat['commonprice'],
          'vipprice'=>$dat['vipprice'],
          'brief'=>$dat['brief'],
          'times'=>$dat['times'],
          'power' =>$dat['power'],
          'unit'=>'元',
          'pic'=>$dat['pic'],
          'inoc'=>$dat['inoc'],
          'bak'=>$dat['bak'],
          'grabprice'=>$dat['grabprice'],
          'is_grob'=>$dat['is_grob'],
          'costprice'=>$dat['costprice'],
          'virtual_times'=>$dat['virtual_times'],
          'is_scan'=>$dat['is_scan'],
          'update_time' =>time(),
          'city_code'  =>isset($dat['city_code'])?$dat['city_code']:440300,

       ];
       $where['id']=$dat['id'];
       if(isset($dat['admin_id'])){
            $where['admin_id']=$dat['admin_id'];
       }
       $this->where($where)->update($data);
       $d=
            [
                'detail' =>$dat['detail']
            ];
       $where_detail['goodsid']=$dat['id'];
       $where_detail['is_use']='1';
       if(isset($dat['admin_id'])){
            $where_detail['admin_id']=$dat['admin_id'];
       }
       $r=Db::name('shop_goods_detail')->where($where_detail)->update($d);
       if(is_numeric($r)){
         $ret=[
           'code' => 1,
           'message' => '修改成功',
           'data' => $r,
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '修改失败',
           'data' => $r,
         ];
       }
       return $ret;
    }
    /**
     * 同步更新商品(针对商品库商品)
     * @Author   ksea
     * @DateTime 2019-03-26T09:15:41+0800
     * @return   [type]                   [description]
     */
    public function togetherUpdate($libid,$Field=array()){
        
        $goodslibrary=model('admin/shop/GoodsLibrary');
        
        $libdata=$goodslibrary->get($libid);
        if(empty($libdata)) return false;
        unset($libdata['id']);
        $allowField=[
          'name',
          'vipprice',
          'commonprice',
          'pic',
          'inoc',
          'times',
          'cate',
          'brief',
          'bak',
          'group_number',
          'grabprice',
          'is_grob',
          'costprice'
        ];
        $allow=empty($Field)?$allowField:$Field;
        $res=$this->allowField($allow)->save($libdata->toArray(),['libid'=>$libid]);
    }
    /**
     * 某个商品的全部详情
     * @Author   ksea
     * @DateTime 2018-11-13T19:05:29+0800
     * @param    [type]                   $id [description]
     * @return   [type]                       [description]
     */
    public function get_goods_detail($id,$store_id='',$admin_id){
      $where=[];

      if(isset($id))$where['id']=$id;
      //前台店铺获取商品条件
      if(strlen($store_id)>0){

         $where['admin_id']=is_get_val('store',$store_id,'admin_id','store_id');
      }
      if(strlen($admin_id)>0)
      {
         $where['admin_id']=$admin_id;
      }
      
      $data=$this->with(['cateData'])->where($where)->find();
      if(!empty($data)){
        $detail        = $data;
        $detail['detail'] =Db::name('shop_goods_detail')->where(['goodsid'=>$id,'is_use'=>1])->find();
      }
      else{
        $detail=false;
      }
      return $detail;
    }

    /**
     * 商品状态更新处理
     * @Author   baohb
     * @DateTime 2018-12-19T10:30:42+0800
     * @param    [type]                   $id [商品id]
     */
    public function upordo($data)
  {
       $res= $this->where(['id'=>$data['id']])->update(['status'=>$data['status']]);
        if(is_numeric($res)){
            $ret=[
                'code' => 1,
                'message' => '产品状态更新成功',
                'data' => $res,
            ];
        }
        else{
            $ret=[
                'code' => 2,
                'message' => '产品状态更新失败',
                'data' => $res,
            ];
        }
        return $ret;
    }

    /**
     * 商品删除更新处理
     * @Author   baohb
     * @DateTime 2018-12-19T15:35:42+0800
     * @param    [type]                   $id [商品id]
     */
    public function hidden_good($data)
  {
        $res= $this->where(['id'=>$data['id']])->update(['is_del'=>$data['is_del']]);
        if(is_numeric($res)){
            $ret=[
                'code' => 1,
                'message' => '产品状态更新成功',
                'data' => $res,
            ];
        }
        else{
            $ret=[
                'code' => 2,
                'message' => '产品状态更新失败',
                'data' => $res,
            ];
        }
        return $ret;
    }

    /**
     *  
     * @Author   baohb
     * @DateTime 2018-12-30T14:24:29+0800
     * @param    [type]                   $store_id [门店id]
     * @param    string                   $limit    [条数限制]
     * @param    string                   $getTime  [第一次获取数据时间节点]
     * @return   [type]                             [description]
     */
    public function goods_by_store($store_id,$limit='',$getTime='',$other,$search='',$order='power,id desc'){

            $admin_id=Db::name('store')->where(['store_id'=>$store_id])->value('admin_id');

            if(strlen($getTime)<2)
            {
              $getTime=time();
            }
            $where['create_time']=array('lt',$getTime);
            //过滤扫码商品不显示
            $where['is_scan']=0;
            $where['admin_id'] = $admin_id;
            $where['status']=1;
            $where['is_del']=0;
            $where['origin_table']='shop_goods';
            //区分城市
            $city_code = session('user.use_city_code');
            $where['city_code']=array('in',[1,$city_code]);
            /********公共条件*********/
            if(strlen($other['cate'])>0)
            {
                 $where['cate']=$other['cate'];
                 $data= $this->where($where)
                    ->order($order)
                    ->select();
                 $res['url']=config('mobileUrl.shopGoodsList');
                 $res['msg']='分类数据';
                 $res['count']=count($data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$data;
            }
            else{
                   $arr_cate=Db::name('shop_categrory')->column('id');
                   $where['cate']=array('in',implode(',',$arr_cate));
            }

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'name',
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/

            if(strlen($limit)<=0)
            {

                 $s_data = $this->where($where)->order($order)->select();
                 $res['url']=config('mobileUrl.shopGoodsDetail');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->limit($limit)->order($order)->select();
                if($need_count==count($s_data)){
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
        }

      /**
       * @author  baohb
       * @datetime 2019-1-9
       * @param string goodsid
       */
      public function add_deal_times($id)
      {

         $res=Db::name('shop_order')->field('goodsid')->where('id',$id)->find();

         $re=$this->where('id',$res['goodsid'])
                   ->setInc('deal_times');

      }

    /******************************特惠商品*******************************************/
    /**
     * 添加特惠商品
     * @Author   ksea
     * @DateTime 2019-07-11T18:34:50+0800
     */
    public function addGodsBargain($data){
      
    }
    /**
     * 修改特惠商品
     * @Author   ksea
     * @DateTime 2019-07-11T18:35:36+0800
     * @return   [type]                   [description]
     */
    public function updateGoodsBargain($data){

    }
    /**
     * 显示特惠商品
     * @Author   ksea
     * @DateTime 2019-07-11T18:36:42+0800
     * @param    [type]                   $data [description]
     * @return   [type]                         [description]
     */
    public function showGoodsBargain($data){

    }
    /******************************特惠商品*******************************************/

    /**
     * 分类关联
     * @Author   ksea
     * @DateTime 2019-07-11T20:15:43+0800
     * @return   [type]                   [description]
     */
    public function cateData(){
      return $this->hasOne('Categrory','id','cate');
    }
    /**
     * 城市数据
     * @Author   ksea
     * @DateTime 2019-07-11T21:25:20+0800
     * @return   [type]                   [description]
     */
    public function cityData(){
       return $this->hasOne('City','city_code','city_code');
    }
    /**
     * 详情数据
     * @Author   ksea
     * @DateTime 2019-07-11T21:25:20+0800
     * @return   [type]                   [description]
     */
    public function detailData(){
       return $this->hasOne('Detail','goodsid','id');
    }
    /**
     * 特惠商品
     * @Author   ksea
     * @DateTime 2019-07-12T10:43:07+0800
     * @return   [type]                   [description]
     */
    public function bargainData(){
       return $this->hasOne('Bargain','id','origin_id');
    }

    //搜索商品
    public function search_goods($city_code, $search, $cate, $limit, $last_id, $order)
    {
        if ($last_id) {
            $last_goods_info = Db::name('shop_goods')->where(['id' => $last_id])->find();
            if ($last_goods_info) {
                $where['deal_times'] = array(['elt', $last_goods_info['deal_times']]);
                $where['commonprice'] = array(['egt', $last_goods_info['commonprice']]);
                $where['id'] = array(['neq', $last_id]);
            }
        }
        if ($city_code) {
            $where['city_code'] = array('in', [1, $city_code]);
        } else {
            $where['city_code'] = 1;
        }
        if ($search) {
            $san_like = [
                'name',
                'brief'
            ];
            $link_scane = implode('|', $san_like);
            $where[$link_scane] = array('like', "%{$search}%");
        }
        if ($cate) {
            $where['cate'] = $cate;
        }
        $where['status'] = 1;
        $where['is_del'] = 0;
        $where['admin_id'] = 2;
        $where['origin_table'] = 'shop_goods';
        $s_data = $this->with('store')->where($where)->limit($limit)->order($order)->select();
        if (count($s_data) > 0) {
            $res['url'] = config('mobileUrl.shopGoodsDetail');
            $res['msg'] = '搜索结果';
            $res['count'] = count($s_data);
            $res['is_ture'] = 1;
            $res['data'] = $s_data;
        } else {
            $res['url'] = config('mobileUrl.shopGoodsDetail');
            $res['msg'] = '没有相关数据';
            $res['count'] = 0;
            $res['is_ture'] = 0;
        }
        return $res;
    }

    //关联店铺，获得店铺基本信息。
    public function store(){
        return $this->hasOne('Store','admin_id','admin_id')->field('store_id,admin_id,nickname,legal_person,address,area');
    }

}