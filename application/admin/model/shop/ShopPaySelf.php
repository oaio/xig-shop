<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\model\shop;
use think\Model;

/**
 * ShopPaySelf.php
 *
 * @author d3li <d3li@sina.com>
 * @create：23/05/2019  9:50 AM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.23
 * @describe
 */
class ShopPaySelf extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';

	public function user()
	{
		return $this->hasOne('User', 'id', 'uid');
	}


}