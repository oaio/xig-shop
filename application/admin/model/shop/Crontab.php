<?php

namespace app\admin\model\shop;

use think\Model;

//定时任务集合操作

class Crontab extends Model{

    public function assess(){
          $redis_data = 'Y-m-d';//处理层级
          $data_name  =date($redis_data,strtotime(date($redis_data).' -15 days'));
          $redis      = new \Redis();
          $redis->connect('127.0.0.1', 6379);
          $temp_name = 'server_assess_'.$data_name;
          $ShopServerAssess=model('admin/shop/ShopServerAssess');
          $serverData=model('admin/shop/Serverorder');
          if($redis->scard($temp_name)>0){
                foreach ($redis->smembers($temp_name) as $key => $value) {
                    $server=$serverData->where('id',$value)->find();
                    $data['serverid']     = $server->id;
                    $data['uid']          = $server->uid;
                    $data['meter_score']  = '5';
                    $data['server_score'] = '5';
                    $data['manner_score'] = '5';
                    $red=$ShopServerAssess->webAdd($data);
                    $redis->srem($temp_name,$server->id);
                }

          }
    }

}
