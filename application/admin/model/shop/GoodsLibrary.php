<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//操作记录模型
class GoodsLibrary extends Model{

	protected $name='GoodsLibrary';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    
    /**
     * 分类
     * @Author   ksea
     * @DateTime 2019-03-25T14:33:28+0800
     * @return   [type]                   [description]
     */
    public function cate(){
        return $this->hasOne('Categrory','id','cate');
    }
}
