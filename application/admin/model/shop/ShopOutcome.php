<?php
namespace app\admin\model\shop;
use think\Db;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\DbException;
use think\Model;
use think\Validate;
use think\Log;

/**
 * 个人分销
 */

class ShopOutcome extends Model
{

	protected $name='ShopOutcome';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

   /**
     * 贡献数据
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function get_outcome($fansid,$limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);
            $where['fansid']=$fansid;
            
            //$where['status']=$status;没有状态

            if(strlen($limit)<=0){
                 $s_data = $this->with(['order'])->where($where)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->with(['order'])->where($where)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

    /**
     * 粉丝贡献计算
     * @Author   ksea
     * @DateTime 2019-04-02T15:14:17+0800
     * 订单单独使用
     */
    public function setOutcome($param){

        $oid=$param['oid'];
        
        $error=[
            'code'   =>0,
            'message'=>'',
            'data'   =>'',
        ];

        $success=$error;
        $success['code']=1;        

        /**********订单验证是否有订单***********/
        $valiorder=new Validate([
            'oid'   =>'require',
        ]);
        if(!$valiorder->check(['oid'=>$oid])){
             $error['message']=$valiorder->getError();
             return $error;
        }
        /**********订单验证是否有订单***********/


        /**********订单验证是否(分销)***********/
        $valiisset=new Validate([
            'oid'   =>'unique:shop_outcome',
        ]);
        if(!$valiisset->check(['oid'=>$oid])){
             myErrorLog('shopoutcome.txt',json_encode(['订单已经分销',$oid]),'fans');
             $error['message']='订单已经分销';
             return $error;
        }
        /**********订单验证是否(分销)***********/

        $order=model('admin/shop/Order');//订单表

        /***********校验数据是否需要分销**************/

        $od=$order->where(['id'=>$oid])->find();
        $fanswhere['uid']=$od['uid'];
        $fanswhere['status']=1;
        $is_ck=model('admin/shop/ShopFans')->where($fanswhere)->find();

        if(empty($is_ck)){
             $error['message']='用户没有粉丝关系';
             return $error;
        }
        /***********校验数据是否需要分销**************/


        /****************根据店铺使用规则判定是否分销****************/
        $Swhere=[
           'store_id' =>$od['store_id'],
        ];
        $store=model('admin/shop/Store')->where($Swhere)->find();

        if(isset($store['rule_type'])&&$store['rule_type']==2){
             $error['message']='使用店铺规则2不产生分销';
             return $error;
        }

        /****************根据店铺使用规则判定是否分销****************/



        $rule=model('admin/shop/DistributionRule')->where(['floor'=>1])->select();//规则表
        $rule_arr=[];
        foreach ($rule as $key => $value) {
            array_push($rule_arr,$value['cate']);
        }
        $myorder=$order
                 ->where(['a.id'=>$oid,'b.status'=>1,'is_pay'=>1])
                 ->alias('a')
                 ->join('fa_shop_fans b','a.uid=b.uid','left')
                 ->field('a.*,b.id as fansid,b.userpic,b.status as fanstatus,b.uid,b.fuid')
                 ->find();
        $fuid=$myorder->fuid;
        $uid=$myorder->uid;
        $price=$myorder->actualpay;

        if(!empty($myorder)){
            $goods=json_decode($myorder->goods_msg,true);
            $cate =$goods['cate'];

            /***************分销规则***************/
            $points=0;

            if(in_array($cate,$rule_arr)){

                $DistributionRule= model('admin/shop/DistributionRule')->where(['floor'=>1,'cate'=>$cate])->find();
                $mytype=$DistributionRule->toArray();

                $rule_type= $mytype['type'];
                $rule_num = $mytype['rule'];
                if($price < $mytype['to']){
                    switch ($rule_type) {
                        case '1':
                            $points = $rule_num * $price / 100;
                            break;
                        case '2':
                            $points = $rule_num;
                            break;
                        default:
                            $points=0;
                            break;
                    }
                }else {
                    $points=$mytype['give'];
                }
            }else{
                myErrorLog('shopoutcome.txt',json_encode(['没有该分类分销规则',$cate,$rule_arr]),'fans');
                $error['message']='没有该分类分销规则';
                return $error;
            }
            /***************分销规则***************/

            /***************数据中转操作**************/
            /**
             * 扫码购买的商品如果是到店的商品一个用户只会又一次有返利
             */
            if($myorder['eventtype']=='storescan'&&$myorder['admin_id']>2){
                $oWhere=[
                    'uid'       => $uid,
                    'is_pay'    => 1,
                    'eventtype' => 'storescan',
                    'admin_id'  =>  ['gt',2],
                ];
                $ArrOrder=model('admin/shop/order')->where($oWhere)->select();
                if(count($ArrOrder)>1){
                    myErrorLog('shopoutcome.txt',json_encode(['该用户以到店购买过商品,不在分佣金',$ArrOrder]),'fans');
                    $error['message']='该用户以到店购买过商品';
                    return $error;
                }
            }

            /***************数据中转操作**************/

            $data=[
                'username'=>$myorder->nickname,
                'uid'=>$uid,
                'userpic'=>$myorder->userpic,
                'fuid'=>$fuid,
                'fansid'=>$myorder->fansid,
                'price'=>$price,
                'points'=>$points,
                'backup'=>$myorder->goods_name,
                'type'=>'1',
                'oid'=>$myorder->id,
                'add'=>0,
            ];
            if($cate==5){

               //用户更新
               $old_user=model('admin/shopuser/User')->where(['id'=>$fuid])->find();
               $new_user=[
                 'sumincome'  => ($old_user['sumincome']+$points),
                 'sumwithdraw'=> ($old_user['sumwithdraw']+$points),
               ];
               $data['add']=1;
               model('admin/shopuser/User')->where(['id'=>$fuid])->update($new_user);
              
            }

            $this->save($data);


            //粉丝更新
            $old_fans= model('admin/shop/ShopFans')->where(['uid'=>$uid,'status'=>1])->find();
            $new_fans=[
              'sumorder'   => ($old_fans['sumorder']+1),
              'sumsales'   => ($old_fans['sumsales']+$price),
              'sumoutcome' => ($old_fans['sumoutcome']+$points),
            ];
            if($old_fans['oid']==0 || empty($old_fans['oid'])){
                $new_fans['oid']=$myorder->id;
            }
            $new_fans['endoid'] = $myorder->id;
            model('admin/shop/ShopFans')->where(['uid'=>$uid,'status'=>1])->update($new_fans);

            $success['message']='分销成功';
            $success['data']=$this->id;
            return $success;

        }

    }

	/**
	 * 更新用户收益
	 *
	 * @author d3li 2019/8/16
	 * @param array $param
	 */
	public function avail($param)
	{
		$out = self::get($param);
		if($out && !$out->add){
			$m = model('admin/shopuser/User')::get($out['fuid']);
			$m->setInc('sumincome',  $out['points']);
			$m->setInc('sumwithdraw',  $out['points']);
			$out->add = 1;
			$out->isUpdate(1)->save();
		}
	}

    public function order(){
      return $this->hasOne('Order','id','oid');
    }
}
