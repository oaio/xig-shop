<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;

use think\Model;
use think\Session;
/**
 * ShopIndex.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/20/2018  7:00 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1220
 * @describe
 */
class ShopIndex  extends Model
{
	protected function base($query)
	{
		$query->where('admin_id', Session::get('admin.id'));
	}

}