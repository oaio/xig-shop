<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
use think\Validate;

class Viprecord extends Model{

	protected $name='Viprecord';
	/**
	 * 添加
	 * @Author   ksea
	 * @DateTime 2019-03-20T16:07:22+0800
	 * @param    [type]                   $dat [uid,vipid,num]
	 */
	public function addVip($dat){

	   $vip=model('admin/shop/vipcard')->get($dat['vipid']);
	   $where=[
	   		'uid'=>$dat['uid'],	
	   		'is_pay'=>1,	
	   ];

	   $check=$this->where($where)->find();
	   $is_new=0;
	   if(empty($check))$is_new=1;
        $user = Db::name('shop_userinfo')->where(['id'=>$dat['uid']])->find();
        if(empty($user) || $user['vip_end_time'] == 0 || $user['vip_end_time'] < time()){
            $now = date('Y-m-d H:i:s',time());
        }else {
            $now = date('Y-m-d H:i:s', $user['vip_end_time']);
        }
        $stoptime =strtotime("+ ".$vip['effectivetime']." months",strtotime($now));
       
       $data=
       [
       		'uid'=>isset($dat['uid'])?$dat['uid']:0,
       		'vipid'=>isset($dat['vipid'])?$dat['vipid']:0,
       		'actiontime'=>time(),
       		'stoptime'=>$stoptime,
       		'vipname'=>$vip['name'],
       		'price'=>$vip['discoutprice']?$vip['discoutprice']:$vip['price'],
       		'is_new'=>$is_new,
       		'num'=>isset($dat['num'])?$dat['num']:1,
       		'interval'	=>$vip['effectivetime']?$vip['effectivetime']:1,
       		'createtime'=>time(),
       ];

       $r=$this->create($data,true);
       if($r){
         $ret=[
           'code' => 1,
           'message' => '添加订单成功',
           'data' => $r,
         ];

       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
         Log::info([
         	'func'	=>'addVip',
         	'data'	=>$ret,
         ]);
       }
       return $ret;
	}
    /**
     * 获取会员套餐订单
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function getVip($uid,$limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['createtime']=array('lt',$getTime);
            $where['uid']=$uid;
            $where['is_pay']=$status;
            if($status==1){
              $where['is_pay']=1;
            }

            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }
	  /**
	   * 用户
	   * @Author   ksea
	   * @DateTime 2019-01-24T13:53:03+0800
	   * @return   [type]                   [description]
	   */
	  public function user(){
	      return $this->hasOne('User','id','uid');
	  }
	  /**
	   * 会员类型
	   * @Author   ksea
	   * @DateTime 2019-01-24T13:53:03+0800
	   * @return   [type]                   [description]
	   */
	  public function vipCode(){
	      return $this->hasOne('Vipcard','id','vipid');
	  }
}
