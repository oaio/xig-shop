<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;
use think\Session;

class EffectiveBean extends Model{

	//数据初始化
	//INSERT INTO fa_effective_bean (userid) (SELECT id FROM fa_shop_userinfo)
	
	protected $name='effective_bean';

    //protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    //protected $createTime = 'create_time';
    //protected $updateTime = 'update_time';

    protected $uid=0;


	protected function initialize(){
	    parent::initialize();
	    $this->uid = Session::get('user.id');
	}
	/**
	 * 添加
	 * @Author   ksea
	 * @DateTime 2019-05-04T21:42:09+0800
	 * @param    [type]                   $dat [description]
	 */
	public  function addBean($dat){

		$allowfield=['userid','effective_bean','receive_state'];
		$validate=new Validate([
			'userid' 			=>'require',
			'effective_bean'    =>'require',
			'receive_state'  	=>'require',
		]);
		if(!$validate->check($dat)){
			$res['code']=0;
			$res['message']=$validate->getError();
			$res['data']=$dat;
			return $res;
		}
		$this->allowField($allowfield)->save($dat);
		$res['code']=1;
		$res['message']='添加成功';
		$res['data']=$this->id;
		return $res;
	}
	/**
	 * 红包获取小哥豆
	 * @Author   ksea
	 * @DateTime 2019-05-04T21:16:01+0800
	 * @return   [type]                   [description]
	 */
	public function gethbbean($uid=''){
		$uid=isset($uid)?$uid:$this->uid;
		$data=$this->where(['userid'=>$uid])->find();
		if(empty($data)){
			$ret['code']=0;
			$ret['message']='数据错误';
			$ret['data']=$data;
			return $ret;
		}
		$data['gift'] = model('RedGift')::get(['uid'=>$uid, 'got'=>0]) ? 1 : 0;

		if($data->receive_state==1){
		     $beans=mt_rand(config('cBeans.min'),config('cBeans.max'));
		     $this->where('userid',$uid)->setInc('effective_bean',$beans);
			 $ret['code']=1;
			 $ret['message']='新人红包领取成功';
			 $ret['data']=$beans;
			 $this->where('userid',$uid)->update(['receive_state'=>2]);
//			 model('UserStats')->add(['active'=>$beans, 'new'=>$beans, 'new_num'=>1]);
			 RedEnvelopesRecord::add(3,$beans,0,'新人红包',$uid);
			 model('BeanLog')->add(['cost' => $beans, 'uid' => $uid, 'kind' => 2]);

		}
		else{
			$ret['code']=0;
			if($data->receive_state==2){
				$ret['message']='新人红包已被领取';
			}
			else{
				$ret['message']='数据错误';
			}
			$ret['data']=$data;
		}
		return $ret;
	}	

}