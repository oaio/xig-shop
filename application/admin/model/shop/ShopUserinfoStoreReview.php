<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;
use think\Session;
use think\Cache;

/**
 * 小店审核 
 */

class ShopUserinfoStoreReview extends Model
{

	protected $name='ShopUserinfoStoreReview';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected $uid=0;


  protected function initialize()
  {
    parent::initialize();
    $this->uid = Session::get('user.id');
  }

  /**
   * 
   * @Author   ksea
   * @DateTime 2019-04-04T11:32:26+0800
   * @param    [type]                   $dat [description]
   */
  public function addReview($dat){

      $validate=new Validate([
        'uid' =>'require',
        'realname' =>'require',
        'phone' =>'require',
        'sex' =>'require',
        'address' =>'require',
        'idnumber' =>'require',
      ]);

      $dat=[
        'uid'       =>isset($dat['uid'])?$dat['uid']:$this->uid,
        'realname'  =>isset($dat['realname'])?$dat['realname']:'',
        'phone'     =>isset($dat['phone'])?$dat['phone']:'',
        'sex'       =>isset($dat['sex'])?$dat['sex']:'',
        'address'   =>isset($dat['address'])?$dat['address']:'',
        'idnumber'  =>isset($dat['idnumber'])?$dat['idnumber']:'',
        'note'      =>isset($dat['note'])?$dat['note']:'',
      ];


      $validateuid=new Validate([
        'uid'=>'unique:shop_userinfo_store_review',
      ]);

      if(!$validateuid->check($dat)){

          $ret['code']=0;
          $ret['message']='用户已申请小店，请勿重新申请';
          return $ret;
      }


      if($validate->check($dat)){

        $ret['code']=1;
        $ret['message']='申请成功';
        $this->save($dat);

      }
      else{
  
        $ret['code']=0;
        $ret['message']=$validate->getError();
  
      }
      return $ret;
  }

	/**
	 * 审核数据修改
	 * @Author   ksea
	 * @DateTime 2019-03-28T21:44:27+0800
	 * @param    [type]                   $dat [description]
	 * @return   [type]                        [description]
	 */
   public function upReview($dat){
    /*****************运行范围*******************/
        $area=[
            'realname',
            'phone',
            'sex',
            'address',
            'idnumber',
            'note',
        ];
        foreach ($dat as $key => $value){
           if(!in_array($key,$area)){
               $rest['code']=2;
               $rest['msg']='不在可运行区域内';
               $rest['data'] =[$key,$value];
           }
        }
        if(isset($rest)){
           return $rest;
        }
    /******************运行范围******************/
        $res=$this->update($dat);
        if($res!=false){
             $rest['code']=1;
             $rest['msg']='更新成功';
             $rest['data'] =$res;
        }
        else{
             $rest['code']=3;
             $rest['msg']='更新失败';
             $rest['data'] =$res;
        }
        return $rest;
   }
  /**
   * 虚假次数
   * @Author   ksea
   * @DateTime 2019-04-12T16:00:02+0800
   */
  public function BumReview(){
      if(!Cache::get('bumreviewnum')){
        $bumnum=rand(236421,250000);
        Cache::set('bumreviewnum',$bumnum);
      }
      else{
        $rand=rand(1,3);
        Cache::set('bumreviewnum',Cache::get('bumreviewnum')+$rand);
      }
      return Cache::get('bumreviewnum'); 
  }

  public function review()
  {
  	return $this->hasOne('ShopUserinfoStoreReview', 'id');
  }
}
