<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;

class Categrory extends Model{
	protected $name='shop_categrory';

	
	/**
	 * 无限极分类
	 * @param  array   $data  [description]
	 * @param  integer $pid   [description]
	 * @param  integer $level [description]
	 * @return [type]         [description]
	 * 2018-11-15 00:22:11
	 */
	public function getallcate($data=[],$pid=0,$level=0){
		return getallcate();
	}
	/**
	 * 获取某个父类下的一层子类
	 *二维数组
	 * @Author   ksea
	 * @DateTime 2018-11-12T10:41:48+0800x
	 * @return   [type]                   [description]
	 */
   public function getfloorcate($pid,$field=''){
   	  return $this->where('pid','=',$pid)->column($field); 
   }

   

}