<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//特惠专区数据模型数据拓展
class Bargain extends Model{

	protected $name='shop_goods_bargain';


    public function goodsData(){
        return $this->hasOne('Goods','origin_id','id');
    }
}
