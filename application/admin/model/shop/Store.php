<?php

/**
* 商铺
* @Author   ksea
* @DateTime 2018-12-18T11:17:47+0800
*/

namespace app\admin\model\shop;

use think\Db;
use think\Log;
use think\Model;
use think\Hook;
use wechat\Api;

class Store extends Model
{
   public $name ='Store';
   /**
    * 添加商铺
    * @Author   ksea
    * @DateTime 2018-12-18T11:17:47+0800
    */
   public function add_store($dat){
       $dat['st_no']="ST".create_no();
   	   $dat['create_time']=time();
       $dat['update_time']=time();
       $dat['area']="广东省,深圳市,罗湖区";
   	   $res=$this->allowField(true)->create($dat);
   	   if($res){
   	   		$rt['code']='1';
   	   		$rt['message']='添加成功';
   	   		$rt['data']=$res['store_id'];
          $param=[
            'store_id'=>$res->store_id,
          ];
          Hook::listen('store_code',$param);
   	   }
   	   else{
   	   		$rt['code']='2';
   	   		$rt['message']='添加失败';
   	   		$rt['data']=$res;
   	   		Log::init([
   	   			'func' => 'add_store',
   	   			'data' => [
   	   						'msg'=>'添加失败',
   	   						'result'=>$res,
   	   						'data'=>$dat,
   	   					  ],
   	   		]);
   	   }
   	   return $rt;
   }
   /**
    * 修改商铺
    * @Author   ksea
    * @DateTime 2018-12-18T11:18:14+0800
    * @return   [type]                   [description]
    */
   public function up_store($dat){
    
        if(isset($dat['bind_user'])&&strlen($dat['bind_user'])>2){

           $is_false=false;
           
           //检测当前用户是否绑定了不是当前店铺的店铺
           $userTemp=User::get($dat['bind_user']);

           if(empty($userTemp)){
             $rest['code']=3;
             $rest['msg']='用户不存在,或已被删除';
             return $rest;
           }

           if(($userTemp->store_bind!=$dat['store_id'])&&!empty($userTemp->store_bind)){
             $is_false=true;
           }
           
           if($is_false){
             $rest['code']=3;
             $rest['msg']='用户已绑定店铺';
             return $rest;
           }

           $user = User::get(['store_bind'=>intval($dat['store_id'])]);
           if(!$user){
             $u_data=[
               'id'=>$dat['bind_user'],
               'store_bind'=>$dat['store_id'],
             ];
             Db::name('shop_userinfo')->update($u_data);
           }
           
        }
        unset($dat['bind_user']);
    /*****************运行范围*******************/
        $dat['update_time']=time();
        $area=[
          'store_id',
          'admin_id',
          'nickname',
          'legal_person',
          'coordinate',
          'address',
          'area',
          'ico',
          'phone',
          'company',
          'store_type',
          'admin_nickname',
          'back',
          'ruler',
          'ruler_phone',
          'is_del',
          'update_time',
          'longitude',
          'latitude',
          'area_id',
          'city_id',
          'province_id',
          'percent',
          'rule_type'
        ];
//        foreach ($dat as $key => $value){
//           if(!in_array($key,$area)){
//               $rest['code']=2;
//               $rest['msg']='不在可运行区域内';
//               $rest['data'] =[$key,$value];
//           }
//        }
        if(isset($rest)){
           return $rest;
        }
    /******************运行范围******************/
        $res=$this->update($dat);
        if($res!=false){
             $rest['code']=1;
             $rest['msg']='更新成功';
             $rest['data'] =$res;
        }
        else{
             $rest['code']=3;
             $rest['msg']='更新失败';
             $rest['data'] =$res;
        }
        return $rest;
   }
    /**
     * 门店模型
     * @Author   baohb
     * @DateTime 2018-12-7T19:29:30+0800
     * @return   [type]                   [description]
     */
    public function store_list($search=array()){
            if(input('param.page','')&&session('s_store_list')){
              $search=session('s_store_list');
            } 
            else{
              session('s_store_list',$search);
            }
            $temp_search=$search;
            if(is_array($search)){
              $temp=$search;
              $search=$temp[0];
              $paginate=$temp[1];
              $other=$temp[2];
              $order=$temp[3]?:'desc';
              $is_page=$temp[4];
            }
            $where=[];
            $where['is_del']=array('eq','0');
            $city_code = session('admin.city_code');
            $city_type = session('admin.type');
            if($city_type == 2 && $city_code){
                $where['city_id'] = $city_code;
            }
            if(!empty($other)&&is_array($other)){
              foreach ($other as $key => $value) {
                  if(strlen($value)==0){
                      unset($other[$key]);
                  }
                  else{
                    $where[$key]=$value;
                  }
              }
            }
            if($search){
                $san_like=[
                   'admin_nickname',
                   'legal_person',
                   'phone',
                   'company',
                   'nickname',
                   'address',
                   'salesphone',
                   'salesmen'
                ];
                $link_scane=implode('|', $san_like);
                $data=$this->with('v')
                    ->where($link_scane,'like',"%{$search}%")
                    ->where($where)
                    ->order('store_id '.$order)
                    ->paginate($paginate);

            }
            else{
              $data=$this->with('v')
                  ->where($where)
                  ->order('store_id '.$order)
                  ->paginate($paginate);
            }
            $page=$data->render();
            return [$data,$page,$temp_search];
    }
    /**
     * 门店删除
     * @Author   ksea
     * @DateTime 2018-12-18T16:53:59+0800
     * @return   [type]                   [description]
     */
    public function del_store($dat){
       $dat['is_del']=1;
       $res=$this->up_store($dat);
       if($res['code']==1){
         $ret=$res;
         $ret['msg']='软删除成功';
       }
       else{
         $ret=$res;
         $ret['msg']='软删除失败';
       }
       return $ret;
    }
    public function w_store($username){
       $admin_id=Db::name('admin')->where('username',$username)->value('id');
       if(empty($admin_id)){
         return true;
       }
       $res=model('admin/AuthGroupAccess')->aga($admin_id);
       $s_t=$this->where('admin_nickname',$username)->value('store_id');
       
       if($res['authgroup']['rules']!='*'&&!empty($s_t)){
          $store_data= $this->where('admin_id',$res['uid'])->find();
          if(isset($store_data)&&!empty($store_data)){
               return $store_data->toarray();
          }
          else{
            return true;
          }
       }
       else{
        return true;
       }
    }
    /**
     * 店铺坐标选址
     * @return [type] [description]
     */
    public function get_list($coordinate,$limit,$getTime,$other){
       /********公共条件*********/

        if(strlen($getTime)<2){
            $getTime=time();
        }

        $where['is_del']=0;
        $where['is_af']=array('in','2,3');
        $where['province_id']=440000;
        $where['city_id']= session('user.use_city_code')?session('user.use_city_code'):440300;
        /********公共条件*********/
       if (strlen($other['area_id'])>0){
           $where['area_id']=$other['area_id'];
           $s_data = $this
               ->where($where)
               ->limit($limit)
               ->order('store_id desc')
               ->select();

           $res['url']=config('mobileUrl.shopGoodsList');
           $res['msg']='全部数据';
           $res['count']=count($s_data);
           $res['is_ture']=1;
           $res['create_time']=$getTime;
           $res['data']=$s_data;


       }
       if(strlen($coordinate['x'])<=0 || strlen($coordinate['y'])<=0){

           if(strlen($limit)<=0) {
               $s_data = $this
                   ->with(['shopIndex'])
                   ->where($where)
                   ->limit($limit)
                   ->order('store_id desc')
                   ->select();

               $res['url']=config('mobileUrl.shopGoodsList');
               $res['msg']='全部数据';
               $res['count']=count($s_data);
               $res['is_ture']=1;
               $res['create_time']=$getTime;
               $res['data']=$s_data;
           }else
               {
               $limit_a=explode(',', $limit);
               if(count($limit_a)==2){
                   $need_count=$limit_a[1];
               }
               else{
                   $need_count=$limit_a[0];
               }
               $s_data = $this
                   ->with(['shopIndex'])
                   ->where($where)
                   ->limit($limit)
                   ->order('store_id desc')
                   ->select();

               if($need_count==count($s_data)){
                   $res['url']=config('mobileUrl.shopGoodsList');
                   $res['msg']='数据合法';
                   $res['count']=count($s_data);
                   $res['is_ture']=1;
                   $res['create_time']=$getTime;
                   $res['data']=$s_data;
               }
               else{
                   $res['url']=config('mobileUrl.shopGoodsList');
                   $res['msg']='数据缺少';
                   $res['count']=count($s_data);
                   $res['is_ture']=0;
                   $res['create_time']=$getTime;
                   $res['data']=$s_data;
               }
           }

       }
       else
       {
           if(strlen($limit)<=0) {
               $s_data = $this
                   ->field("*,ABS(".$coordinate['y']."-longitude)+ABS(".$coordinate['x']."-latitude) as exp_coordinate")
                   ->with(['shopIndex'])
                   ->where($where)
                   ->limit($limit)
                   ->order('exp_coordinate, store_id desc')
                   ->select();

               $res['url']=config('mobileUrl.shopGoodsList');
               $res['msg']='全部数据';
               $res['count']=count($s_data);
               $res['is_ture']=1;
               $res['create_time']=$getTime;
               $res['data']=$s_data;
           }else
           {
               $limit_a=explode(',', $limit);
               if(count($limit_a)==2){
                   $need_count=$limit_a[1];
               }
               else{
                   $need_count=$limit_a[0];
               }
               $s_data = $this
                   ->field("*,ABS(".$coordinate['y']."-longitude)+ABS(".$coordinate['x']."-latitude) as exp_coordinate")
                   ->with(['shopIndex'])
                   ->where($where)
                              ->limit($limit)
                              ->order('exp_coordinate,store_id desc')
                              ->select();
               if($need_count==count($s_data)){
                   $res['url']=config('mobileUrl.shopGoodsList');
                   $res['msg']='数据合法';
                   $res['count']=count($s_data);
                   $res['is_ture']=1;
                   $res['create_time']=$getTime;
                   $res['data']=$s_data;
               }
               else{
                   $res['url']=config('mobileUrl.shopGoodsList');
                   $res['msg']='数据缺少';
                   $res['count']=count($s_data);
                   $res['is_ture']=0;
                   $res['create_time']=$getTime;
                   $res['data']=$s_data;
               }
           }

       }

       return $res;
    }

    /**
     * 门店二维码2.0
     * @Author   ksea
     * @DateTime 2019-05-06T18:11:52+0800
     * @param    [type]                   $param [description]
     * @return   [type]                          [description]
     */
    public function create_code($param){
        $store_id=$param['store_id'];
        //$p_root=$_SERVER['HTTP_HOST'];
        //$p_root='192.168.5.149/xgb/xig-shop/public';
        //$rst=creat_code('http://'.$p_root.'/index.php/mobile/goods/goods_list/mstore_id/'.$store_id.'/id/'.$store_id,$store_id.'_'.microtime(TRUE),'5','0',false,'storecode/');
        $rst=Api::getInstance()->qrcode('store|mobile|goods|goods_list|mstore_id|'.$store_id.'|id|'.$store_id,$store_id.'_'.microtime(TRUE),'qrcode');
        if(!!is_array($rst)){
           Db::name('store')->where('store_id',$store_id)->update([
                'store_code' =>$rst
           ]);
           $ms='二维码生成成功';
           $url=$rst;
        }
        else{
           $ms=json_encode($rst);
        }
        return $ms;
    }

    /**
     * @author baohb
     * @datetime 2019-1-3 10:39
     * @param string store_id
     */
    public function store_detail($store_id){

        $store =$this->with('banner')->where('store_id','=',$store_id)->find();
        return $store;
    }
  /**
   * 归属订单
   * @Author   ksea
   * @DateTime 2019-07-31T16:50:40+0800
   * @return   [type]                   [description]
   */
  public function order(){
    return $this->hasMany('Order', 'store_id', 'store_id')->where('is_pay',1);
  }

	/**
	 * 顶部图片
	 *
	 * @author d3li 1/9/2019
	 * @return \think\model\relation\HasOne
	 */
	public function banner(){
		return  $this->hasOne('Banner','admin_id','admin_id')
			->field('id,img_link as img,admin_id');
	}
  /**
   * 店铺对应bannner
   * @Author   ksea
   * @DateTime 2019-01-24T13:53:03+0800
   * @return   [type]                   [description]
   */
  public function shopIndex(){
      return $this->hasMany('shopIndex','admin_id','admin_id');
  }
  public function bindstore(){
    return $this->hasOne('user','store_bind','store_id');
  }

	/**
	 *
	 * @author d3li 2019/5/29
	 * @return \think\model\relation\HasOne
	 */
	public function cash()
	{
		return $this->hasOne('app\admin\model\Cash', 'store_id', 'store_id')
			->field('store_id,SUM(costs) AS money')
			->where('status', 2)
			->group('store_id');
    }

    public function v()
    {
	    return $this->hasMany('StoreExt', 'store_id', 'store_id')
		    ->order('id', 'DESC');
    }
}