<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\model\shop;
use think\Model;

/**
 * StoreExt.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/8/3  0:08
 * @see      https://gitee.com/d3li/
 * @version 2.08.03
 * @describe
 */
class StoreExt extends Model
{
	public function r()
	{
		return $this->hasOne('app\admin\model\Region', 'region_id', 'region');
	}
}