<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/26
 * Time: 9:43
 */

namespace app\admin\model\shop;

use think\Db;
use think\Model;

class Groupbuydetails extends Model{
    protected $name="group_buying_details";

    /**
     * 查询是否参加过某个商品拼团活动
     * @author  hobin
     * @DateTime 2019-2-26T21:37:01+0800
     */
    public function get_join($params){

        $res =$this->alias('g')
                   ->join('shop_order o','o.group_details_id=g.id','left')
                   ->where('user_id','=',$params['user_id'])
                   ->where('group_id','=',$params['group_id'])
                   ->where('o.is_pay','=',1)
                   ->find();

        return $res['user_id'];

    }

    /**
     * 获取某个拼团信息
     * @Author   hobin
     * @DateTime 2019-2-26T10:41:48+0800x
     * @return   [type]                   [description]
     */
    public function get_assemable_byid($group_id){

//        $ret = $this
//
//            ->with('user')
//
//            ->where(['group_id'=>$group_id])
//
//            ->select();

     $ret=$this->alias('a')
            ->join('shop_userinfo u','a.user_id = u.id','left')
            ->join('shop_order o','a.id = o.group_details_id','left')
            ->field('u.id,u.pic')
            ->where('a.group_id',$group_id)
            ->where('o.is_pay','=',1)
            ->select();

        return $ret;

    }

    /**
     * 参加某个商品拼团活动
     * @author  hobin
     * @DateTime 2019-2-26T21:37:01+0800
     */
    public function add_assemble($data){
       return $this->create($data);

    }


    public function add_groupbuydetails($data){
        return $this->create($data);

    }



    /**
     * 拼团数据
     * @Author   hobin
     * @DateTime 2019-2-26T19:58:01+0800
     * @return   [type]                   [description]
     */
    public function assemble(){
        return  $this->hasOne('Assemble','id','group_id');
    }


    public function user(){
        return  $this->hasMany('User','id','user_id');
    }


}