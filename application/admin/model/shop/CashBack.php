<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
use app\admin\model\shopuser\User;
use app\admin\model\shop\Goods;
use app\admin\model\Config;

/**
 * 小哥豆抵扣规则
 * 
 */
class Cashback extends Model{
    //主表选用订单表
    protected $name='shop_order';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    public function GetBack($oid,$uid){
    
        /**
         * 返回基础定义
         */
        
        $retData=[
            'code' => 1,
            'msg'  => '获取成功',
            'data' => '',
        ];

        /*****************用户数据初始化*********************/
        
        $userModel  = new User();
        $uWhere=[
            'id' =>$uid,
        ];
        $user=$userModel->where($uWhere)->find()->toArray();

        $oWhere=[
            'id' =>$oid,
        ];
        $order=$this->where($oWhere)->find()->toArray();

        $oWhereAll=[
            'uid'    => $uid,
            'is_pay' => 1,
            'status' => 1,
        ];
        $orderAllTemp=$this->where($oWhereAll)->select();
        $orderAll=json_decode(json_encode($orderAllTemp),true);

        $cWhere=[
            ''
        ];
        $rule = model('config')::get(['name' => 'cashback'])->toArray();
        $rule['value']=json_decode($rule['value'],true);
        /*****************用户数据初始化*********************/

        $tempRule=1;
        /*****************rule数据完整性校验*********************/

        if(!isset($rule['value']['novip'])||!isset($rule['value']['vip'])||empty($rule['value']['vip'])||empty($rule['value']['novip'])){
            $retData['code'] =0;
            $retData['msg']  ='请先设置返利规则';
            $tempRule=0;
            return $retData;
        }

        /*****************rule数据完整性校验*********************/

        /**********************基础数据判断*********************/
        /**
         * 订单生成用户和当前用户比对
         */
        if($order['uid']!=$uid){
            $retData['code'] = 0;
            $retData['msg']  ='请当前用户支付不允许代付款';
            return $retData;
        }
        /**
         * 订单支付状态
         */
        if($order['is_pay']!=1){
            $retData['code'] = 0;
            $retData['msg']  ='请先支付订单';
            return $retData;
        }
        /**********************基础数据判断*********************/

        //不用类型用户使用小哥豆比例
        $type=$user['vip_end_time']>time()?'vip':'novip';

        //实付金额
        $actualpay=$order['actualpay'];
        //默认返回比例
        $useper=0;
        //抵扣百分比例
        $suredis=0.01;

        if($tempRule==1){
            $ruleData=$rule['value'][$type];
            /**
             * 根据规则条数，如果内城规则不足者使用最有添加一条
             */
            if(isset($ruleData[count($orderAll)])){
                $useper=$ruleData[count($orderAll)];
            }
            else{
                $useper=$ruleData[count($ruleData)-1];
            }
        }

        $back=$actualpay*$useper*$suredis;
        $retData['msg']  ='返利计算完成';
        $retData['data'] =(string)round($back,2);
        return $retData;
    }

    /**
     * 订单金额追加抵扣计算
     * @Author   ksea
     * @DateTime 2019-08-06T15:31:33+0800
     * @return   [type]                   [description]
     */
    public function disMoney($userpaym,$uid,$oid){
        /**
         * 返回基础定义
         */
        
        $retData=[
            'code' => 1,
            'msg'  => '获取成功',
            'data' => '',
        ];

        /*****************用户数据初始化*********************/
        
        $userModel  = new User();
        $uWhere=[
            'id' =>$uid,
        ];
        $user=$userModel->where($uWhere)->find()->toArray();
        $oWhere=[
            'id' =>$oid,
        ];
        $order=$this->where($oWhere)->find()->toArray();

        $oWhereAll=[
            'uid'    => $uid,
            'is_pay' => 1,
            'status' => 1,
        ];
        $orderAllTemp=$this->where($oWhereAll)->select();
        $orderAll=json_decode(json_encode($orderAllTemp),true);

        $cWhere=[
            ''
        ];
        $rule = model('config')::get(['name' => 'cashback'])->toArray();
        $rule['value']=json_decode($rule['value'],true);
        /*****************用户数据初始化*********************/

        $tempRule=1;
        /*****************rule数据完整性校验*********************/

        if(!isset($rule['value']['novip'])||!isset($rule['value']['vip'])||empty($rule['value']['vip'])||empty($rule['value']['novip'])){
            $retData['code'] =0;
            $retData['msg']  ='请先设置返利规则';
            $tempRule=0;
            return $retData;
        }

        /*****************rule数据完整性校验*********************/

        /**********************基础数据判断*********************/
        /**
         * 订单生成用户和当前用户比对
         */
        if($order['uid']!=$uid){
            $retData['code'] = 0;
            $retData['msg']  ='请当前用户支付不允许代付款';
            return $retData;
        }
        /**
         * 订单支付状态
         */
        if($order['is_pay']!=1){
            $retData['code'] = 0;
            $retData['msg']  ='请先支付订单';
            return $retData;
        }
        /**********************基础数据判断*********************/

        //不用类型用户使用小哥豆比例
        $type=$user['vip_end_time']>time()?'vip':'novip';

        //实付金额
        $actualpay=$userpaym;
        //默认返回比例
        $useper=0;
        //抵扣百分比例
        $suredis=0.01;

        if($tempRule==1){
            $ruleData=$rule['value'][$type];
            /**
             * 根据规则条数，如果内城规则不足者使用最有添加一条
             */
            if(isset($ruleData[count($orderAll)])){
                $useper=$ruleData[count($orderAll)];
            }
            else{
                $useper=$ruleData[count($ruleData)-1];
            }
        }

        $back=$actualpay*$useper*$suredis;
        $retData['msg']  ='返利计算完成';
        $retData['data'] =(string)round($back,2);
        return $retData;
    }


}
