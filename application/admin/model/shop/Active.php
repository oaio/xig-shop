<?php

namespace app\admin\model\shop;

use think\Model;
use think\Session;
use think\Validate;

/**
 * 活动
 */
class Active extends Model
{
	protected $name='shop_active';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * 获取活动数据
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    
    public function getActive($limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);

            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->order('endtime desc,power desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->limit($limit)->order('endtime desc,power desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

	/**
	 * 过期活动
	 *
	 * @author d3li 12/04/2019
	 * @throws \think\exception\DbException
	 */
	public static function expire()
    {
	    $list = self::all([
	    	'is_end'=>0,
		    'endtime' => ['lt', $_SERVER['REQUEST_TIME']]
	    ]);
        if($list) {
	        foreach ($list as $k => $v) {
		        $v->allowField(true);
		        $v->save(['is_end'=>1]);
	        }
        }
    }
}