<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;

use think\Model;
/**
 * GoodsCore.php
 *
 * @author d3li <d3li@sina.com>
 * @create：09/04/2019  4:28 PM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.09
 * @describe
 */
class GoodsCore extends Model
{
	protected $autoWriteTimestamp = 'int';



	public function goods()
	{
		return $this->belongsTo('Goods', 'gid');
	}
    /**
     * 获取核心商品
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function getCore($limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);
            $where['city_code']=session('user.use_city_code');
            if(strlen($limit)<=0){
                 $s_data = $this->with(['goods'])->where($where)->order('weigh desc')->select();
                 $res['url']=config('mobileUrl.shopGoodsDetail');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->with(['goods'])->where($where)->limit($limit)->order('weigh desc')->select();
                $arr=[];
                foreach ($s_data as $key => $value) {
                    
                    $arr[$key]=$value;
                    $arr[$key]['label']=json_decode($value['label']);

                }
                if($need_count==count($arr)){
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据合法';
                    $res['count']=count($arr);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$arr;
                }
                else{
                    $res['url']=config('mobileUrl.shopGoodsDetail');
                    $res['msg']='数据缺少';
                    $res['count']=count($arr);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$arr;
                }
            }

            return $res;
    }

}