<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;

class Serverorderextr extends Model{
	
	protected $name='ShopServerOrderExtr';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}