<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//操作记录模型
class ArticleLibrary extends Model{

	protected $name='ArticleLibrary';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    
}
