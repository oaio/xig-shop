<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;

use app\common\model\Common;

class Card extends Model{
	protected $name='shop_card';
	/**
	 * 获取某人的全部卡片
	 * @Author   ksea
	 * @DateTime 2018-11-15T10:58:11+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function mycard($uid='',$status=0,$paginate='20')
	{
        if(!empty($uid)){
			switch ($status) {
				case '0':
					$where['uid']=$uid;
					break;
				default:
					$where['uid']=$uid;
					$where['status']=$status;
					break;
			}
			$data=$this->alias('c')->join('shop_userinfo u','c.uid=u.id','left')
                       ->where($where)
                       ->paginate($paginate);

        }
        else{
			$data=$this->alias('c')->join('shop_userinfo u','c.uid=u.id','left')
                       ->field('c.*,u.nickname')
                       ->paginate($paginate);
        }
		$page = $data->render();
		return [$data,$page];
	}
	/**
	 * 获取其中一个，显示更新页面
	 * @Author   ksea
	 * @DateTime 2018-11-19T10:23:36+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function getone($data){

		if(!empty($data)){
			return $this->where('id','=',$data)->find()->getData();
		}
		else{
			return false;
		}
	}
	/**
	 * 修改
	 * @Author   ksea
	 * @DateTime 2018-11-15T11:43:42+0800
	 * @return   [type]                   [description]
	 */
	public function upcard($post_data){
        if(isset($post_data['name']))$data['name']=$post_data['name'];
        if(isset($post_data['status']))$data['status']=$post_data['status'];
        if(isset($post_data['sum_times']))$data['sum_times']=$post_data['sum_times'];
        if(isset($post_data['times']))$data['times']=$post_data['times'];
        if(isset($post_data['valitimes']))$data['valitimes']=$post_data['valitimes'];
        if(isset($post_data['howlong']))$data['howlong']=$post_data['howlong'];
        $data['update_time']=time();
		$where['id']=$post_data['id'];
        if(isset($post_data['admin_id'])){
        	$where['admin_id']=$post_data['admin_id'];
        }
		$r= $this->where($where)->update($data);
		if($r){
			$res['code']=1;
			$res['mesage']='修改成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=1;
			$res['mesage']='修改失败';
			$res['data']=$r;
		}
		return $res;
	}
	/**
	 * 添加
	 * @Author   ksea
	 * @DateTime 2018-11-19T10:33:15+0800
	 * @return   [type]                   [description]
	 */
	public function addcard($post_data){
		$data=
		[
			'name'=>$post_data['name'],
			'uid'=>$post_data['uid'],
			'status'=>$post_data['status'],
			'times'=>$post_data['times'],
			'sum_times'=>$post_data['sum_times'],
			'valitimes'=>$post_data['valitimes'],
			'howlong'=>$post_data['howlong'],
			'orderid'=>$post_data['orderid'],
			'admin_id'=>$post_data['admin_id'],
			'update_time'=>time(),
			'create_time'=>time(),
		];
		$r= $this->create($data);
		if($r){
			$res['code']=1;
			$res['mesage']='添加成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=1;
			$res['mesage']='添加失败';
			$res['data']=$r;
		}
		return $res;
	}
	/**
	 * 删除 需要结构
	 * 
	 * @Author   ksea
	 * @DateTime 2018-11-15T11:42:48+0800
	 * @param    [type]                   $id [需要结构string 或者array（id1，id2）]
	 * @return   [type]                       [description]
	 */
	public function delcard($id){
		if(is_array($id))
		{
			$where['id']=$id;
		}
		else
		{
			$where['id'] =array('in',implode(',',$id));
		}
		$r= $this->where($where)->delete();
		if(is_numeric($r)){
			$res['code']=1;
			$res['mesage']='删除成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=1;
			$res['mesage']='删除失败';
			$res['data']=$r;
		}
		return $res;
	}

  	/**
  	 * 店铺关联
  	 * @Author   ksea
  	 * @DateTime 2019-01-14T14:15:45+0800
  	 * 过滤有问题，有空再修
  	 * @return   [type]                   [description]
  	 */
    public function store(){
    	  $where['is_del']=0;
    	  $where['is_af']=2;
	      return $this->hasOne('Store','admin_id','admin_id')->where($where);
    }
  	/**
  	 * 订单关联
  	 * @Author   ksea
  	 * @DateTime 2019-01-14T14:15:45+0800
  	 * 过滤有问题，有空再修
  	 * @return   [type]                   [description]
  	 */
    public function order(){
	      return $this->hasOne('Order','id','orderid')->where('is_pay=1');
    }
}