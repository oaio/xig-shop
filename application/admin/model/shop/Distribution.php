<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;
/**
 * 分销
 */
class Distribution extends Model
{
	protected $name='shop_pay_log';
	protected $autoWriteTimestamp = true;
	/**
	 * 添加分销分成,记录
	 * 先分成后添加记录
	 * @Author   ksea
	 * @DateTime 2019-01-09T13:56:35+0800
	 * @param    array                    $param [description]
	 * @return   [type]                          [description]
	 */
	public function share_benefits($id){
		/**********订单是否存在************/
		$validateOrder=new Validate([
			'id'	=>'unique:shop_order',
		]);
		if($validateOrder->check(['id'=>$id])){
			 $ret=[
			 	'code'=>2,
			 	'msg' => '订单不存在,不分销',
			 	'id'  => $id,
			 ];
			 Log::info($ret);
			 return $ret;
		}
		/***********订单是否存在***********/

		
		/***********订单其他数据校验**********/
		
		$order=model('admin/shop/order')->get($id)->toarray();
		if(
			$order['is_pay']!=1||
			$order['status']!=1||
			strlen($order['pay_id'])<=0
		){
			$ret=[
				'code'	=>3,
				'msg'	=>'订单未支付成功,不分销',
				'data'		=>$order,
			];
		    Log::info($ret);	
			return $ret;
		}
		/***********订单其他数据校验**********/

		/**************备用订单分成记录字段判断*************/
		
		/**************备用订单分成记录字段判断*************/
		$step=is_get_val('store',is_get_val('shop_userinfo',$order['uid'],'store_id'),'percent','store_id')*$order['actualpay'];
		$store_id=is_get_val('shop_userinfo',$order['uid'],'store_id','id');
		$admin_id=is_get_val('store',$store_id,'admin_id','store_id');
		$step=$step/100;
		/*********事务处理*******/
		Db::startTrans();
		try{
			$store_data=Db::name('store')->where('store_id',$store_id)->setInc('burse',$step);
			/*****分销记录*****/
			$log_data=[
				'uid'=>$order['uid'],
				'username'=>is_get_val('shop_userinfo',$order['uid'],'name'),
				'openid'=>is_get_val('shop_userinfo',$order['uid'],'openid'),
				'pay_type'=>1,
				'trade_no'=>$order['snorder'],
				'total_fee'=>$order['actualpay'],
				'rebate_fee'=>$step,
				'admin_id'=>$admin_id,
				'store_id'=>$store_id,
				'order_time'=>$order['create_time'],
				'create_time'=>time(),
			];
			/*****分销记录*****/
			$store_log=Db::name('shop_pay_log')->insertGetId($log_data);
		    Db::commit();    
		} catch (\Exception $e) { 
		    Log::info([
		    	'msg'=>'分销错误',
		    	'order'=>$order,
		    	'data'=>$e,
		    ]);
		    Db::rollback();
		}


	}
	/**
	 * 自定义付款分销
	 * 用户基准
	 * $pay_data	=>  array(
	 * 						'payid'	=>	'',校验唯一 ,attach=pay_type在2，3
	 * 					)
	 * @Author   ksea
	 * @DateTime 2019-01-22T13:52:17+0800
	 * 一条记录只会可以产生一条分销记录一一对应,如果支付记录表切换，请保留关键字段，不然分销失败['id','attach','total_fee','openid'];
	 * @return   [type]                   [description]
	 */
	public function share_self($payid=''){


		if(
		  !isset($payid)
		){
			$ret=['code'=>0,'msg'=>'数据格式错误','data'=>$payid];
			return $ret;
		}
		/*******************************数据校验*************************************/
								/*********支付表的校验*********/
		//payid唯一
		$validateuWp=new Validate([
			'id'	=>'unique:wechat_pay',
		]);
		if($validateuWp->check(['id'=>$payid])){

			$ret=['code'=>0,'msg'=>'支付记录不存在','data'=>$payid];
			Log::info($ret);
			return $ret;
		}


		//自定义付款类型符合条件
		$pay_d=is_get_val('wechat_pay',$payid,'attach,total_fee,openid,note');
		$pay_type=$pay_d['attach'];
		$openid=$pay_d['openid'];
		$money=$pay_d['total_fee']/100;
		$note=$pay_d['note'];
		$validateiWp=new Validate([
			'pay_type'	=>'in:2,3',
		]);
		if(!$validateiWp->check(['pay_type'=>$pay_type])){

			$ret=['code'=>0,'msg'=>$validateiWp->getError(),'data'=>$payid];
			Log::info($ret);
			return $ret;
		}

								/*********支付表的校验*********/



								/*********分销表的校验*********/
								
		//每个支付id只能使用一次
		$validatespl=new Validate([
			'pay_code'	=>'unique:shop_pay_log',
		]);
		if(!$validatespl->check(['pay_code'=>$payid])){

			$ret=['code'=>0,'msg'=>'支付记录已经分销','data'=>$payid];
			Log::info($ret);
			return $ret;
		}

								/*********分销表的校验*********/


		/*******************************数据校验*************************************/



		$user=is_get_val('shop_userinfo',$openid,'store_id,id,name,openid','openid');
		$uid=$user['id'];
		$store_id=$user['store_id'];
		$username=$user['name'];
		$step=is_get_val('store',$store_id,'percent','store_id')*$money;
		$step=$step/100;
		$admin_id=is_get_val('store',$store_id,'admin_id','store_id');
		/*********事务处理*******/
		Db::startTrans();
		try{
			$store_data=Db::name('store')->where('store_id',$store_id)->setInc('burse',$step);
			/*****分销记录*****/
			$log_data=[
				'uid'=>$uid,
				'username'=>$username,
				'openid'=>$openid,
				'trade_no'=>'',
				'total_fee'=>$money,
				'rebate_fee'=>$step,
				'admin_id'=>$admin_id,
				'store_id'=>$store_id,
				'order_time'=>'',
				'create_time'=>time(),
				'pay_type'	=>$pay_type,
				'note'		=>$note,
				'pay_code'	=>$payid,
			];
			/*****分销记录*****/
			$store_log=Db::name('shop_pay_log')->insertGetId($log_data);
		    Db::commit();    
		} catch (\Exception $e) { 
		    Log::info([
		    	'msg'=>'自定义分销错误',
		    	'data'=>$e,
		    ]);
		    Db::rollback();
		}

	}
}


?>