<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Log;
use think\Cache;

class ShopServerAssess extends Model{

	protected $name='shop_server_assess';

    protected $autoWriteTimestamp=true;
    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';

    /******************************手机端**************************************/
	/**
	 * 评论添加
	 * @Author   ksea
	 * @DateTime 2019-05-27T11:56:49+0800
	 * @param    array                    $data [description]
	 * @return   [type]                         [description]
	 */
	public function webAdd($data=[]){

		$serverid = $data['serverid'];
		$uid	  = $data['uid'];
		$ServerD  = model('admin/shop/Serverorder')->where('id',$serverid)->find();
		$orderD   = model('admin/shop/Order')->where('id',$ServerD['orderid'])->find();
		$userD    = model('admin/shopuser/User')->where('id',$uid)->find();
		
		if($userD['vip_end_time']>time()){
			$userD['vip_now']=1;
		}
		else{
			$userD['vip_now']=0;
		}

        //去除redis入口
        model('admin/shop/MyRedis')->assessRem($serverid,$ServerD['fu_end']);

		$meter_score=$data['meter_score'];
		$server_score=$data['server_score'];
		$manner_score=$data['manner_score'];
		$avg=($meter_score+$server_score+$manner_score)/3;

		$data['avg_assess']  = round($avg,2);
		$data['goodsid']     = $ServerD->goodsid;
		$data['sfuserid']    = $ServerD->sfuserid;
		$data['goods_name']  = $orderD->goods_name;
		$data['json_goods']  = $orderD->goods_msg;
		$data['json_server'] = json_encode($ServerD->toArray());
		$data['json_user']   = json_encode($userD);
		$res=$this->allowField(true)->create($data);
		return $res;
	}
	/**
	 * 获取商品关联评论
	 * @Author   ksea
	 * @DateTime 2019-05-27T21:29:44+0800
	 * @param    [type]                   $uid     [description]
	 * @param    string                   $limit   [description]
	 * @param    string                   $getTime [description]
	 * @param    string                   $search  [description]
	 * @param    array                    $other   [description]
	 * @return   [type]                            [description]
	 */
    public function getGodosAssess($goodsid='',$limit='',$getTime='',$search='',$other=array()){

            $query='';
            if(strlen($getTime)<2){
              $getTime=time();
            }

            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
	                if(strlen($value)<=0){
	                    unset($other[$key]);
	                }
	                else{
	                    $where[$key]=$value;
	                }
                }
            }
            /*****************自定义查询*******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'detail',
                    'goods_name',
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            if(empty($goodsid)){
            
                $where['goodsid']=$goodsid;

            }
            $base=$this;
            if(strlen($limit)<=0){
                 $s_data = $base->where($where)->where($query)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $base->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

    /**
     * 首页评论
     * @Author   ksea
     * @DateTime 2019-05-27T21:29:44+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    string                   $search  [description]
     * @param    array                    $other   [description]
     * @return   [type]                            [description]
     */
    public function getGodosAssessMix($limit='',$getTime=''){
        

            if(strlen($getTime)<2){
              $getTime=time();
            }
            
            $where['assess.status']=1;

            $base=$this;
            if(strlen($limit)<=0){
                 $s_data = $base->alias('assess')->
                            join('shop_userinfo info','assess.uid=info.id','LEFT')->
                            join('shop_service_order service','service.id=assess.serverid','LEFT')->
                            where($where)->
                            order('assess.id desc')->
                            field('service.address,assess.create_time,assess.detail as commit,assess.goods_name as goodsname,info.pic as icon,assess.avg_assess as star,info.nickname as uname')->
                            select();
                 $res['url']=config('mobileUrl.Bumarticle');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $base->alias('assess')->
                          join('shop_userinfo info','assess.uid=info.id','LEFT')->
                          join('shop_service_order service','service.id=assess.serverid','LEFT')->
                          where($where)->
                          limit($limit)->
                          order('assess.id desc')->
                          field('service.address,assess.create_time,assess.detail as commit,assess.goods_name as goodsname,info.pic as icon,assess.avg_assess as star,info.nickname as uname')->
                          select();
                if($need_count==count($s_data)){
                    $res['url']=config('mobileUrl.Bumarticle');
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']=config('mobileUrl.Bumarticle');
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

    /******************************手机端**************************************/
    public function user(){
        return $this->hasOne('app\admin\model\shop\User','id','uid');
    }
    /**
     * 源数据
     * @Author   ksea
     * @DateTime 2019-06-22T09:36:13+0800
     * @return   [type]                   [description]
     */
    public function emp(){
        return $this->hasMany('app\admin\model\shop\Employee','id','sfuserid');
    }
    /**
     * 重置数据
     * @Author   ksea
     * @DateTime 2019-06-22T09:31:01+0800
     * @param    [type]                   $key   [description]
     * @param    [type]                   $value [description]
     * @return   [type]                          [description]
     */
    public function getEmpAttr($key,$value){
        $emp=model('admin/shop/Employee')->where('id','in',$value['sfuserid'])->select();
        return $emp;
    }
}