<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;
use think\Db;
use think\Exception;
use think\Model;
/**
 * RedEnvelopes.php 待领红包
 *
 * @author d3li <d3li@sina.com>
 * @create：04/05/2019  10:40 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.04
 * @describe
 */
class RedEnvelopes extends Model
{
//	private $uid;

//	protected function initialize()
//	{
//		parent::initialize();
//		$this->uid = session('user.id');
//	}


	/**
	 * 获取红包池总额
	 *
	 * @access public
	 * @return float|int
	 */
	public function total($uid)
	{
		return $this->where(['state'=>1, 'userid'=>$uid])
			->sum('money');
	}

	/**
	 * 领红包
	 *
	 * @access public
	 * @throws Exception
	 */
	public function pop($uid)
	{
		if(empty($uid)) return 0;//'大家太热情了，请稍后再试...';
		$zero = strtotime(date('Ymd'));
		$pop = RedEnvelopesRecord::where(['type'=>1, 'userid'=>$uid,
			'transaction_ime'=>['>', $zero]])->count();
		$bean = EffectiveBean::get(['userid'=>$uid]);
		if($pop>0) return 0;//'请明天来开红包';
		$row = $this->where(['state'=>1, 'userid'=>$uid,
			'receive_time' => ['<', $_SERVER['REQUEST_TIME']]])->order('id')->find();
		if($row) {
			$add = $row->money - 0;
			//model('admin/ShopUserinfo')->get($uid)->setInc('xgbpeas', $add);
			if($bean) {
				$msg = RedEnvelopesRecord::add(1, $add, 0, '每日拆红包', $uid);
				if(!0 == $msg) {
					$bean->setInc('effective_bean', $add);
					$row->setInc('state');
					model('BeanLog')->add(['kind'=>1, 'cost'=> - $add, 'uid'=>$uid])
						->add(['kind'=>2, 'cost'=> $add, 'uid'=>$uid]);
				}else{
					return $msg;
				}
				//$pop && $bean->today_buy>1 && $bean->setDec('today_buy');
				return $add;
			}
		}
		return 0;//'您的红包已经领完';
	}
}