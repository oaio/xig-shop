<?php
namespace app\admin\model\shop;
use think\Db;
use think\Model;
use think\Validate;
use think\Log;

/**
 * 个人分销
 */

class ShopUserinfoStore extends Model
{

	protected $name='ShopUserinfoStore';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';


  /**
   * 更新数据
   * @Author   ksea
   * @DateTime 2019-04-04T11:32:26+0800
   * @param    [type]                   $dat [description]
   */
  public function add_store($dat){
      $dat=[
        'name'      =>$dat['nickname'],
        'uid'       =>$dat['id'],
        'pic'       =>$dat['pic'],
        'backgroup' =>'',
        'sign' =>'',
      ];
      $this->save($dat);
  }

	/**
	 * 个人小店更新
	 * @Author   ksea
	 * @DateTime 2019-03-28T21:44:27+0800
	 * @param    [type]                   $dat [description]
	 * @return   [type]                        [description]
	 */
   public function up_store($dat){
    /*****************运行范围*******************/
        $area=[
          'id',
          'name',
          'pic',
          'backgroup',
          'sign',
        ];
        foreach ($dat as $key => $value){
           if(!in_array($key,$area)){
               $rest['code']=2;
               $rest['msg']='不在可运行区域内';
               $rest['data'] =[$key,$value];
           }
        }
        if(isset($rest)){
           return $rest;
        }
    /******************运行范围******************/
        $res=$this->update($dat);
        if($res!=false){
             $rest['code']=1;
             $rest['msg']='更新成功';
             $rest['data'] =$res;
        }
        else{
             $rest['code']=3;
             $rest['msg']='更新失败';
             $rest['data'] =$res;
        }
        return $rest;
   }
}
