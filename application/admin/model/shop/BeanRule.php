<?php

namespace app\admin\model\shop;

use app\admin\model\shopuser\User;
use app\admin\model\shop\Goods;
use app\admin\model\shop\Order;
use app\admin\model\shop\Store;

use think\Model;

/**
 * 小哥豆抵扣规则
 *
 */
class BeanRule extends Model {

	protected $name = 'shop_usebean_rule';

	protected $autoWriteTimestamp = true;

	// 定义时间戳字段名
	protected $createTime = 'create_time';
	protected $updateTime = 'update_time';

	//获取规则格式化
	public function getRuleAttr($value) {

		return json_decode($value, true);
	}

	/**
	 * 抵扣小哥豆计算(接口返回处理)(调用)
	 * @Author   ksea
	 * @DateTime 2019-07-22T14:18:04+0800
	 * @param    [type]                   $goodsid [商品id]
	 * @param    [type]                   $num     [数量]
	 * @param    string                   $uid     [description]
	 */
	public function GetBean($goodsid = '', $num = '', $uid = '') {
		/**
		 * 返回基础定义
		 */

		$retData = [
			'code' => 1,
			'msg' => '获取成功',
			'data' => '',
		];
		/*********************数据初始化*************************/

		$userModel = new User();
		$orderModel = new Order();
		$goodsModel = new Goods();
		$storeModel = new Store();

		//用户
		$uWhere = [
			'id' => $uid,
		];
		$user = $userModel->with(['bean'])->where($uWhere)->find()->toArray();

		//订单
		$oWhere = [
			'uid' => $uid,
			'is_pay' => 1,
		];
		$orderTemp = $orderModel->where($oWhere)->select();
		$order = json_decode(json_encode($orderTemp), true); //数组转换

		//商品
		$gWhere = [
			'id' => $goodsid,
		];
		$goods = $goodsModel->where($gWhere)->find()->toArray();

		//店铺
		$Swhere = [
			'admin_id' =>$goods['admin_id']
		];
		$store=$storeModel->where($Swhere)->find()->toArray();

		//规则
		$Rwhere = [
			'status' => 1,
		];
		$ord = 'times ASC';
		$ruleTemp = $this->where($Rwhere)->order($ord)->select();
		$rule = json_decode(json_encode($ruleTemp), true); //数组转换

		/*********************数据初始化*************************/

		/*******************抵扣规则判断************************/

		//没有规则不使用抵扣
		if (empty($rule)) {
			$retData['code'] = 0;
			$retData['msg'] = '没有抵扣规则';
			$retData['data'] = 0;
			return $retData;
		}

		/*******************抵扣规则判断************************/

		/*******************基础数据*********************/

		//用户小哥豆
		$userbean = $user['bean']['effective_bean'];

		//不用类型用户使用小哥豆比例
		$per = $user['vip_end_time'] > time() ? 1 : 1;

		//根据用户属性
		$price = $user['vip_end_time'] > time() ? $goods['vipprice'] : $goods['commonprice'];

		$userPay = round($price * $num, 2);

		//新用户到店抵扣
			
		if(isset($store['rule_type'])&&$store['rule_type']==2){
			$oncePay = 3;
		}
		else{
			$oncePay = 1;
		}

		//抵扣百分比例
		$suredis = 0.01;

		/*******************基础数据*********************/
		//全局使用非第一次有特殊者全局false
		if (false/*count($order)<=0*/) {

			//是否到店商品判断
			if ($goods['is_scan'] == 1) {
				if ($userbean < $oncePay) {
					$retData['msg'] = '新用户第一次到店抵扣';
					$retData['data'] = (string) (round($userbean, 2));

				} else {
					$retData['msg'] = '新用户第一次到店抵扣';
					$retData['data'] = (string) $oncePay;
				}
			} else {

				//规则约束
				$disBeanTemp = $userPay * $rule[0]['discount'] * $per * $suredis;

				if ($disBeanTemp > $userbean) {
					$disBean = $userbean;
				} else {
					$disBean = $disBeanTemp;
				}
				$retData['msg'] = '抵扣实例化1';
				if ($userPay == 0) {
					$disBean = 0;
				}
				$disBean = round($disBean, 2);

				$retData['data'] = (string) $disBean;
			}
			if ($userPay == 0) {
				$retData['data'] = 0;
			}
			return $retData;

		} else {
			if (count($order) <= 0 && $goods['cate'] == 5) {
				if ($userbean < $oncePay) {
					$retData['msg'] = '新用户第一次到店抵扣';
					$retData['data'] = (string) (round($userbean, 2));
				} else {
					$retData['msg'] = '新用户第一次到店抵扣';
					$retData['data'] = (string) $oncePay;
				}
				return $retData;
			} else {
				//非第一次购买到店商品不抵扣
				if ($goods['is_scan'] == 1) {
					$retData['code'] = 0;
					$retData['msg'] = '到店商品只有新用户有抵扣';
					$retData['data'] = 0;

					if ($userPay == 0) {
						$retData['data'] = 0;
					}
					return $retData;
				}
			}

			$Rwhere['times'] = count($order) + 1;
			$tR = $this->where($Rwhere)->find();
			/**
			 * 根据购买次数确定使用规则
			 * 如果购买的次数没有对应规则使用times最多的一条
			 * 或者说是最后一条
			 */
			if (empty($tR)) {
				unset($Rwhere['times']);
				$singleRule = $this->where($Rwhere)->order('times desc')->find()->toArray();
			} else {
				$singleRule = $tR->toArray();
			}
			//规则约束
			$disBeanTemp = $userPay * $singleRule['discount'] * $per * $suredis;
			if ($disBeanTemp > $userbean) {
				$disBean = $userbean;
			} else {
				$disBean = $disBeanTemp;
			}
			/******************层级规则初始化**********************/
			if ($singleRule['rule'] != NUll) {

				foreach ($singleRule['rule'] as $Rkey => $Rvalue) {
					//比对数值
					if ($userPay >= $Rvalue['areamin'] && $userPay < $Rvalue['areamax'] && !empty($Rvalue['areamax'])) {
						//
						if ($disBean > $Rvalue['getmoney']) {
							$disBean = $Rvalue['getmoney'];
						}
					}
					//设置最后一个数值
					if ($userPay >= $Rvalue['areamin'] && $Rvalue['areamax'] == 'N') {
						if ($disBean > $Rvalue['getmoney']) {
							$disBean = $Rvalue['getmoney'];
						}
					}
				}
			}
			/******************层级规则初始化**********************/
			$retData['msg'] = '抵扣实例化2';
			$disBean = round($disBean, 2);
			$retData['data'] = (string) $disBean;

			if ($userPay == 0) {
				$retData['data'] = 0;
			}

			return $retData;
		}

	}
	/**
	 * 订单金额追加抵扣计算
	 * @Author   ksea
	 * @DateTime 2019-08-06T15:31:33+0800
	 * @return   [type]                   [description]
	 */
	public function disMoney($userpaym,$uid,$orderid){

		/**
		 * 返回基础定义
		 */

		$retData = [
			'code' => 1,
			'msg' => '获取成功',
			'data' => '',
		];
		/*********************数据初始化*************************/

		$userModel = new User();
		$orderModel = new Order();
		$goodsModel = new Goods();
		$storeModel = new Store();

		//用户
		$uWhere = [
			'id' => $uid,
		];
		$user = $userModel->with(['bean'])->where($uWhere)->find();

		//订单
		$oWhere = [
			'uid' => $uid,
			'is_pay' => 1,
		];
		$orderTemp = $orderModel->where($oWhere)->select();
		
		$order = json_decode(json_encode($orderTemp), true); //数组转换

		$now_order=model('admin/shop/Order')->where(['id'=>$orderid,'is_pay'=>1,'uid'=>$uid])->find();
		
		if(!$now_order){
			$retData['code'] = 0;
			$retData['msg'] = '未查询到合法订单';
			$retData['data'] = 0;
			return $retData;
		}

		//商品
		$gWhere = [
			'id' => $now_order['goodsid'],
		];
		$goods = $goodsModel->where($gWhere)->find()->toArray();

		//店铺
		$Swhere = [
			'admin_id' =>$goods['admin_id']
		];
		$store=$storeModel->where($Swhere)->find()->toArray();

		//规则
		$Rwhere = [
			'status' => 1,
		];
		$ord = 'times ASC';
		$ruleTemp = $this->where($Rwhere)->order($ord)->select();
		$rule = json_decode(json_encode($ruleTemp), true); //数组转换

		/*********************数据初始化*************************/

		/*******************抵扣规则判断************************/

		//没有规则不使用抵扣
		if (empty($rule)) {
			$retData['code'] = 0;
			$retData['msg'] = '没有抵扣规则';
			$retData['data'] = 0;
			return $retData;
		}

		/*******************抵扣规则判断************************/

		/*******************基础数据*********************/

		//用户小哥豆
		$userbean = $user['bean']['effective_bean'];

		//不用类型用户使用小哥豆比例
		$per = $user['vip_end_time'] > time() ? 1 : 1;

		//传入价格定义抵扣
		$price = $userpaym;

		$userPay = round($price, 2);

		//新用户到店抵扣
			
		if(isset($store['rule_type'])&&$store['rule_type']==2){
			$oncePay = 3;
		}
		else{
			$oncePay = 1;
		}

		//抵扣百分比例
		$suredis = 0.01;

		/*******************基础数据*********************/
		if (count($order) <= 0 && $goods['cate'] == 5) {
			if ($userbean < $oncePay) {
				$retData['msg'] = '新用户第一次到店抵扣';
				$retData['data'] = 0;//(string) (round($userbean, 2));约束抵扣到店商品不在发生抵扣
			} else {
				$retData['msg'] = '新用户第一次到店抵扣';
				$retData['data'] = 0;//(string) $oncePay;约束抵扣到店商品不在发生抵扣
			}
			return $retData;
		} else {
			//非第一次购买到店商品不抵扣
			if ($goods['is_scan'] == 1) {
				$retData['code'] = 0;
				$retData['msg'] = '到店商品只有新用户有抵扣';
				$retData['data'] = 0;

				if ($userPay == 0) {
					$retData['data'] = 0;
				}
				return $retData;
			}
		}

		$Rwhere['times'] = count($order) + 1;
		$tR = $this->where($Rwhere)->find();
		/**
		 * 根据购买次数确定使用规则
		 * 如果购买的次数没有对应规则使用times最多的一条
		 * 或者说是最后一条
		 */
		if (empty($tR)) {
			unset($Rwhere['times']);
			$singleRule = $this->where($Rwhere)->order('times desc')->find()->toArray();
		} else {
			$singleRule = $tR->toArray();
		}
		//规则约束
		$disBeanTemp = $userPay * $singleRule['discount'] * $per * $suredis;
		if ($disBeanTemp > $userbean) {
			$disBean = $userbean;
		} else {
			$disBean = $disBeanTemp;
		}
		/******************层级规则初始化**********************/
		if ($singleRule['rule'] != NUll) {

			foreach ($singleRule['rule'] as $Rkey => $Rvalue) {
				//比对数值
				if ($userPay >= $Rvalue['areamin'] && $userPay < $Rvalue['areamax'] && !empty($Rvalue['areamax'])) {
					//
					if ($disBean > $Rvalue['getmoney']) {
						$disBean = $Rvalue['getmoney'];
					}
				}
				//设置最后一个数值
				if ($userPay >= $Rvalue['areamin'] && $Rvalue['areamax'] == 'N') {
					if ($disBean > $Rvalue['getmoney']) {
						$disBean = $Rvalue['getmoney'];
					}
				}
			}
		}
		/******************层级规则初始化**********************/
		$retData['msg'] = '抵扣实例化2';
		$disBean = round($disBean, 2);
		$retData['data'] = (string) $disBean;

		if ($userPay == 0) {
			$retData['data'] = 0;
		}

		return $retData;
	}

}
