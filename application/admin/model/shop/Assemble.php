<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/26
 * Time: 9:43
 */

namespace app\admin\model\shop;

use think\Model;
use think\Log;

class Assemble extends Model{
    protected $name="group_buying";

    /**
     * 发起拼团
     * @author  hobin
     * @DateTime 2018-11-13T19:59:43+0800
     */
    public function add_aorder($dat){

        $data=
            [
                'num'=>$dat['num'],
                'shouldpay'=>is_get_val('shop_goods',$dat['goodsid'],'grabprice')*$dat['num'],
                'actualpay'=>is_get_val('shop_goods',$dat['goodsid'],'grabprice')*$dat['num'],
                'status' => 0,
                'bak'    => $dat['bak']?:'',
                'goodsid'=> $dat['goodsid'],
                'uid'	 =>$dat['uid'],
                'snorder'=> 'OR'.create_no(),
                'create_time'=>time(),
                'update_time'=>time(),
                'is_pay'	=>0,
                'store_id' =>$dat['store_id'],
                'type'=>1
            ];

        /************************************校验***********************************/

        /**********商品判断开始***********/
        $goods_data=is_get_val('shop_goods',$dat['goodsid'],'id,name');
        if($goods_data){
            $data['goods_name']= $goods_data['name']?:'';
            $data['goodsid']   = $goods_data['id']?:'';
        }
        else{
            $ret=[
                'code' => 3,
                'message' => '没有指定商品',
                'data' => [
                    'data'=>$goods_data,
                    'where'=>$dat['goodsid']
                ],
            ];
            return $ret;
        }
        $goods =is_get_val('shop_goods',$dat['goodsid'],'id,name,price,vipprice,commonprice,grabprice,unit,pic,inoc,status,create_time,update_time,sngoods,group_number,is_type,admin_id,power');
        $data['goods_msg']=json_encode($goods);
        /**********商品判断结束***********/




        /**********店铺判断***********/
        $admin_id=is_get_val('store',$dat['store_id'],'admin_id','store_id');
        if($admin_id){
            $data['admin_id']= $admin_id?:'';
        }
        else{
            $ret=[
                'code' => 3,
                'message' => '店铺不存在',
                'data' => [
                    'data'=>$admin_id,
                ],
            ];
            return $ret;
        }

        /**********店铺判断***********/


        /**********用户数据判断***********/

        $user_data=is_get_val('shop_userinfo',$dat['uid'],'phone,name,address');


        if($user_data){
            $data['nickname'] = $user_data['name']?:'';

            $data['mobile']   = $user_data['phone'];

            $data['address']   = $user_data['address'];
        }
        else{
            $ret=[
                'code' => 3,
                'message' => '没有对应用户',
                'data' => [
                    'data'=>$user_data,
                    'where'=>$dat['uid']
                ],
            ];
            return $ret;
        }
        /**********用户数据判断***********/

        /***********优惠券判断************/
        if(isset($dat['discount_id'])){
            $data['discount_id']=$dat['discount_id'];
        }
        /***********优惠券判断************/

        /**************************************校验*********************************/

        $r=model('admin/shop/order')->create($data,true);

        if($r){
            $ret=[
                'code' => 1,
                'message' => '发起拼团成功',
                'data' => $r,
            ];

        }
        else{
            $ret=[
                'code' => 2,
                'message' => '发起拼团失败',
                'data' => $r,
            ];
            Log::info([
                'func'	=>'add_order',
                'data'	=>$ret,
            ]);
        }
        return $ret;
    }


    public function add_assemble($param){
       $this->save($param);
       return $this->id;
    }




    /**
     * 获取某个商品的所有拼团信息
     * @Author   hobin
     * @DateTime 2019-2-26T10:41:48+0800x
     * @return   [type]                   [description]
     */
    public function get_assemble_bygid($goodsid){

        $ret= $this->where('goods_id','=',$goodsid)
                   ->with('user')
                   ->select();

        return json_encode($ret);
    }

    /**
     *  获取某个拼团信息
     * @author  hobin
     * @datetime 2019-2-27
     * @param string goodsid
     */
    public function get_group_number($group_id)
    {
        $num = $this->field('number')->where('id','=',$group_id)->find();

        return $num['number'];


    }

    /**
     *  更新某个拼团状态
     * @author  hobin
     * @datetime 2019-3-5
     * @param string goodsid
     */
//    public function up_group_status($group_id){
//        $params['status'] = 2;
//        return $this->save($params,['id'=>$group_id]);
//
//    }

    public function up_group_status($param){
        $group_id=$param['group_id'];

        $params['status'] = 2;
        $this->where('id',$group_id)
            ->setInc('number');
        return $this->save($params,['id'=>$group_id]);

    }




    /**
     * 商品数据
     * @Author   hobin
     * @DateTime 2019-2-26T19:58:01+0800
     * @return   [type]                   [description]
     */
    public function goods(){
        return  $this->hasOne('Goods','id','goodsid');
    }


    public function store(){
        $allow=[
            'address',
            'company',
            'store_id',
            'telphone',
            'nickname',
            'ruler',
            'ruler_phone',
        ];
        $allowfiled=implode(',',$allow);
        return $this->hasOne('Store','store_id','store_id')->field($allowfiled);
    }
    /**
     * 关联模型
     * @Author   hobin
     * @DateTime 2019-01-03T20:36:22+0800
     * @return   [type]                   [description]
     */
    public function card(){
        return $this->hasOne('Card','orderid','id');
    }

    public function paylog(){
        return $this->hasMany('Wechatpay','oid','id');
    }
    public function user(){
        return  $this->hasOne('User','id','originator');
    }

}