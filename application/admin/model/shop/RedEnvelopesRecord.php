<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;
use think\Model;
/**
 * RedEnvelopesRecord.php 红包记录
 *
 * @author d3li <d3li@sina.com>
 * @create：04/05/2019  7:55 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.04
 * @describe
 */
class RedEnvelopesRecord extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $createTime = 'transaction_ime';
	protected $updateTime = '';
	private static $conf = ['', '红包活动', '消费抵扣', '新人红包', '消费返现', '客服赠送'];

	public static function add($data, $amount = 0, $oid = 0, $source ='', $uid = 0)
	{
		if(!is_array($data)) {
			$data = [
				'type' => $data,
				'amount' => $amount,
				'source' => $source,
				'userid' => $uid ?: session('user.id')
			];
		}
		if ($data['type'] > count(self::$conf)) return '类型错误';
		if($oid) {
			try {
				$res = model('admin/shop/Order')->get($oid);
			} catch (DbException $e) {
				return !1;
			}
			if (!$res) return ' 订单不存在';
			$goods= json_decode($res->goods_msg);
			$data['userid'] =$res->uid;
			$data['pic'] = '/' . $goods->pic;
			$data['source'] = $res->goods_name;
			$data['orderid'] = $res->id;
		}
		empty($data['source']) && $data['source'] = self::$conf[$data['type']];
		if(empty($data['userid'])) return '用户不存在';
		return self::create($data);
	}

	/**
	 * 贡献明细列表
	 *
	 * @access public
	 * @param $limit
	 * @return false|\PDOStatement|string|\think\Collection
	 */
	public function listing($uid, $limit)
	{
		try {
			return $this->where('userid', $uid)
				->limit($limit)
				->order('id DESC')
				->select();
		} catch (DataNotFoundException $e) {
		} catch (ModelNotFoundException $e) {
		} catch (DbException $e) {
		}
		return !1;
	}

	/**
	 * 获取个人红包相关信息
	 *
	 * @access public
	 * @param $uid
	 * @return array
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 * @throws \think\Exception
	 */
	public function info($uid)
	{
		$res = $this->field('type,SUM(amount) as total,count(id) as day')
			->group('type')
			->where('userid', $uid)
			->select();
		$data =['add'=>0, 'days'=>0, 'save'=>'0.00'];
		foreach ($res as $v){
			if($v['type'] ==1){
				$data['add'] = $v['total'];
				$data['days'] = $v['day'];
			}elseif($v['type'] ==2){
				$data['save'] = sprintf('%.2f',$v['total']);
			}
		}
		$zero = strtotime(date('Ymd'));
		$res = $this->where(['type'=>1, 'userid'=>$uid])
			->order('transaction_ime DESC')->find();
		$data['last'] = $res ? substr($res->amount, 1) : 0;
		$pop = self::where(['type'=>1, 'userid'=>$uid,'transaction_ime'=>['>', $zero]])->count();
		//$bean = EffectiveBean::get(['userid'=>$uid]);
		$row = RedEnvelopes::get(['state'=>1, 'userid'=>$uid,
			'receive_time' => ['<', $_SERVER['REQUEST_TIME']]]);
		$data['today'] = empty($pop) && $row  ? 0 : 1;//<1 || ($pop<2 && $bean->today_buy>1) ? 0 : 1;
		return $data;
	}

	public function getAmountAttr($value, $row)
	{
		return ($row['type'] == 2? '- ':'+ ').$value;
	}

	public function getTransactionImeAttr($value)
	{
		return date('Y/m/d H:i', $value);
	}
}