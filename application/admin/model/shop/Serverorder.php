<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Log;
use think\Cache;
use wechat\Fictitious;

class Serverorder extends Model{

    // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
    protected $autoWriteTimestamp=true;
    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';

	protected $name='shop_service_order';

	public function base($query){
		$query->with(['extrkv','user','emp','extrorder','code','temp']);
	}

	public function getEmpAttr($key,$value){
		$emp=model('admin/shop/Employee')->where('id','in',$value['sfuserid'])->select();
		return $emp;
	}
	/**
	 * 获取某人的全部服务
	 * @Author   baohb
	 * @DateTime 2018-11-15T10:58:11+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function myserver($search=array())
    {
        if(input('param.page','')&&session('s_myserver')){
            $search=session('s_myserver');
        }
        else{
            session('s_myserver',$search);
        }

        $temp_search=$search;
        if(is_array($search)){
            $temp=$search;
            $search=$temp[0];
            $paginate=$temp[1];
            $other=$temp[2];
            $order=$temp[3]?:'desc';
            $is_page=$temp[4];
            if(isset($temp[5])){
                $admin_id=$temp[5];
            }
        }
        $where=[];
        if(isset($admin_id)&&!empty($admin_id)){
            $where['s.admin_id']=$admin_id;
        }
        $splice=[
            'create_time',
            'fu_start',
            'appoint_start',
        ];
        $common=[
            'create_time',
            'update_time',
        ];

        if(!empty($other)&&is_array($other)){
            foreach ($other as $key => $value) {
                if(strlen($value)==0){
                    unset($other[$key]);
                }
                else{
                    if(in_array($key,$splice)){
                        if(in_array($key,$common)){
                            $where['s.'.$key]=['BETWEEN',strtotime($value).','.(strtotime($value)+86400)];
                        }
                        else{
                            $where[$key]=['BETWEEN',strtotime($value).','.(strtotime($value)+86400)];
                        }
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
        }
        if($search){
            $san_like=[
                's.snserver',
                'o.snorder',
                'u.name',
                'u.phone',
               
            ];
            $link_scane=implode('|', $san_like);
            $data=$this->alias('s')
                           ->join('shop_userinfo u','s.uid=u.id','left')
                           ->join('shop_order o','s.orderid=o.id','left')
                           ->join('region g','s.areaid=g.region_id','left')
                           ->field('s.*,u.province,u.name,u.phone,u.city,u.area,s.address,o.address as o_address,o.goods_name,o.snorder,g.region_name')
                           ->where($link_scane,'like',"%{$search}%")
                           ->where($where)
                           ->order('s.id desc')
                           ->paginate($paginate);

        }
        else{

            $data=$this->alias('s')
                ->join('shop_userinfo u','s.uid=u.id','left')
                ->join('shop_order o','s.orderid=o.id','left')
				->join('region g','s.areaid=g.region_id','left')
				->field('s.*,u.province,u.name,u.phone,u.city,u.area,s.address,o.address as o_address,o.goods_name,o.snorder,g.region_name')
                ->where($where)
                ->order('s.id desc')
                ->paginate($paginate);

        }
        $page=$data->render();
        return [$data,$page,$temp_search];

    }


	/**
	 * 获取其中一个，显示更新页面
	 * @Author   baohb
	 * @DateTime 2018-12-11T10:23:36+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function getone($data)
    {

		if(!empty($data)){
            $data=$this->alias('s')
                ->join('shop_userinfo u','s.uid=u.id','left')
                ->join('shop_order o','s.orderid=o.id','left')
                ->join('region k','s.areaid=k.region_id','left')
                ->field('s.*,u.province,u.name,u.phone,u.city,s.areaid,k.region_name,s.address,o.address as o_address,o.goods_name,o.snorder')
                         ->where('s.id','=',$data)
                         ->find();
           return $data;
		}
		else{
			return false;
		}
	}
	/**
	 * 修改---修改的时候会绑定虚拟号吗，每次修改的时候会先解绑该订单之前绑定的虚拟号，
	 * @Author   ksea
	 * @DateTime 2018-11-15T11:43:42+0800
	 * @return   [type]                   [description]
	 * 废弃(2019-8-12)
	 */
	public function upserver($post_data)
    {
        //第一步查询是否有绑定虚拟号码。
        $phone = new Fictitious();
        $shop_order =  $this->where(['id'=>$post_data['id']])->find();
        if($shop_order['mobile'] != 0){
            //解绑
            $phone_j = $phone->Untying($shop_order['mobile']);
            if( $phone_j['resultdesc'] != 'Success') {
                $res['code']=2;
                $res['mesage']='修改失败,虚拟号解绑失败';
                $res['data']='';
                //return $res;
            }else{
                 Db::name('rivacy_phone')->where('phone',$shop_order['mobile'])->update(['Binding_time' => 0]);
				 //释放暂存
				 setFictitious($shop_order['mobile'],$post_data['id']);    
            }
        }
        //绑定
        $customer_mobile = Db::name('shop_order')->where(['id'=> $shop_order['orderid']])->value('mobile');

        $rivacy_phone = getFictitious();//获取有效虚拟号

        $sfuserid = explode(",", $post_data['sfuserid']);
        $userid =  Db::name('employee')->alias('a')
            ->where('a.id','in',$sfuserid[0])
            ->find();
        if(!is_null($userid) && !is_null($rivacy_phone)) {
            $phone_resul = $phone->createphone($rivacy_phone['phone'], $customer_mobile, $userid['phone']);
            if(  $phone_resul['resultdesc'] == 'Success') {
                $revacy = Db::name('rivacy_phone')->where('id', $rivacy_phone['id'])->update(['Binding_time' => time()]);
                $revacy_c =  Db::name('rivacy_phone_record')->insert(['callerNum'=>$customer_mobile,'calleeNum'=>$userid['phone'],'relationNum'=>$rivacy_phone['phone'],'Binding_time' => time(),'subscriptionId'=>$phone_resul['subscriptionId']]);
                if($revacy == 0 && $revacy_c == 0){
                    $res['code']=2;
                    $res['mesage']='修改失败,虚拟号绑定失败';
                    $res['data']='';
                    return $res;
                }
            }
        }
    	$post_data['appoint_start']=strtotime($post_data['appoint_start']);
        $post_data['update_time']=time();
        $post_data['mobile']   =  $rivacy_phone['phone'];
		$where['id']=$post_data['id'];
		if(isset($post_data['admin_id'])){
			$where['admin_id']=$post_data['admin_id'];
		}
		$r= $this->where($where)->update($post_data);
		if($r){
			$res['code']=1;
			$res['mesage']='修改成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=2;
			$res['mesage']='修改失败';
			$res['data']=$r;
		}
		return $res;
	}
	/**
	 * 添加
	 * @Author   baohb
	 * @DateTime 2018-11-17T13:49:02+0800
	 * @return   [type]                   [description]
	 */
	public function addserver($post_data)
    {
		$data=
		[
			'sfuserid'=>$post_data['sfuserid'],
			'fu_start'=>$post_data['fu_start'],
			'fu_end'=>$post_data['fu_end'],
			'uid'=>$post_data['uid'],
			'goodsid'=>$post_data['goodsid'],
			'create_time'	=>time(),
			'update_time'	=>time(),
            'sngoods'=>'JB'.create_no(),
		];
		if(isset($post_data['admin_id'])){
			$data['admin_id']=$post_data['admin_id'];
		}
		if(isset($post_data['goodsid']))$data['goodsid']=$post_data['goodsid'];
		if(isset($post_data['orderid']))$data['orderid']=$post_data['orderid'];

		$r= $this->create($data);
		if($r){
			$res['code']=1;
			$res['mesage']='添加成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=1;
			$res['mesage']='添加失败';
			$res['data']=$r;
		}
		return $res;
	}
	/**
	 * 删除 需要结构
	 * 
	 * @Author   baohb
	 * @DateTime 2018-11-15T11:42:48+0800
	 * @param    [type]                   $id [需要结构string 或者array（id1，id2）]
	 * @return   [type]                       [description]
	 */
	public function delserver($id)
    {
		if(is_array($id))
		{
			$where['id']=$id;
		}
		else
		{
			$where['id'] =array('in',implode(',',$id));
		}
		$r= $this->where($where)->delete();
		if(is_numeric($r)){
			$res['code']=1;
			$res['mesage']='删除成功';
			$res['data']=$r['id'];
		}
		else{
			$res['code']=1;
			$res['mesage']='删除失败';
			$res['data']=$r;
		}
		return $res;
	}
	/**
	 * 开始服务
	 * @Author   baohb
	 * @DateTime 2018-11-26T15:08:17+0800
	 * @param    [type]                   $server_id [服务id]
	 * @param    [type]                   $sfuserid  [服务人员id]
	 * @return   [type]                              [description]
	 */
	public function start_server($server_id,$sfuserid)
    {
		$res=[];
		if(is_check_true('0','shop_service_order','status')){
			$now=time();
			Db::startTrans();
			try {
				$this->update([
					'id'=>$server_id,
					'sfuserid'=>$sfuserid,
					'status'=>1,
					'fu_start'=>$now,
				]);
				$res['code']=1;
				$res['msg']='服务开始';
				Db::commit();
			} catch (\Exception $e) {
				$res['code']=2;
				$res['msg']='服务开始失败,请联系客服';
				Log::init([
					'start_server' =>
					[
						$server_id,
						$sfuserid,
						$now
					]
				]);
				Db::rollback();
			}
		}
		else{
				$res['code']=3;
				$res['msg']='已经开始';
		}
		return $res;
	}
	/**
	 * 结束服务
	 * @Author   baohb
	 * @DateTime 2018-11-26T15:08:17+0800
	 * @param    [type]                   $server_id [服务id]
	 * @return   [type]                              [description]
	 */
	public function end_server($server_id)
    {
		$res=[];
		if(is_check_true('4','shop_service_order','status')){
			$now=time();
			Db::startTrans();
			try {
				$dt=$this->where("id={$server_id}")->find();
				if($dt['appoint_end']==0){
					$data=
					[
						'id'=>$server_id,
						'fu_end'=>$now,
						'fu_start'=>$now,
						'status'=>4,
						'appoint_end'=>$now,
					];
				}
				else{
					/**********没有约定时间的情况下，完成预约，就是实际约定时间*******/
					$data=
					[
						'id'=>$server_id,
						'fu_end'=>$now,
						'fu_start'=>$now,
						'status'=>4
					];
					/**********没有约定时间的情况下，完成预约，就是实际约定时间*******/
				}
				$res['code']=1;
				$res['msg']='服务结束';
				$this->update($data);
				Db::commit();
			} catch (\Exception $e) {
				Log::init([
					'start_server' =>
					[
						$server_id,
						$now
					]
				]);
				$res['code']=2;
				$res['msg']='end_server 失误';
				Db::rollback();
			}
		}
		else{
				$res['code']=3;
				$res['msg']='已经结束';
		}
		return $res;
	}


	/**
	 * 获取服务数量
	 * @Author   ksea
	 * @DateTime 2019-04-12T13:25:08+0800
	 * @param    string                   $create_time [description]
	 * @return   [type]                                [description]
	 */
	public function getServerNum($create_time=''){

	    if(strlen($create_time)<2){

	    	$where['status']=array('in','0,1,2,3,4');
			$time=time();
			$count=$this->where($where)->count();
			$ret['code']=1;
			$ret['msg']='数据拉取成功';
			$ret['data']['create_time']=$time;
			$ret['data']['count']=$count;
	    
	    }
		else{
			
	    	$where['status']=array('in','0,1,2,3,4');
	    	$where['create_time']=array('egt',$create_time);
			$time=time();
			$count=$this->where($where)->count();
			$ret['code']=1;
			$ret['msg']='数据拉取成功';
			$ret['data']['create_time']=$time;
			$ret['data']['count']=$count;

		}
		return $ret;
	}
	/**********************************手机端接口***************************************************/

    public function getMserverStatus($uid,$limit='',$getTime='',$search='',$other=array()){

            $query='';

            if(strlen($getTime)<2){
              $getTime=time();
            }

            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
                	if(is_array($value)){
                		$where[$key]=$value;
                	}
                	else{
	                    if(strlen($value)<=0){
	                        unset($other[$key]);
	                    }
	                    else{
	                        $where[$key]=$value;
	                    }
                	}
                }
            }
            /*****************自定义查询*******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'snserver',
                    'address',
                    'note',
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            $where['uid']=$uid;
            $base=$this->with(['order.paylog','assess','emp','temp']);
            if(strlen($limit)<=0){
                 $s_data = $base->where($where)->where($query)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $base->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }
	/**********************************手机端接口***************************************************/
	/**
	 * 虚假次数
	 * @Author   ksea
	 * @DateTime 2019-04-12T16:00:02+0800
	 */
	public function BumServerNum(){

      if(!Cache::get('bumnum')){
        $bumnum=rand(236421,250000); 
        Cache::set('bumnum',$bumnum);
      }
      else{
      	$rand=rand(0,1);
        Cache::set('bumnum',Cache::get('bumnum')+$rand);
      }
      return Cache::get('bumnum'); 
	}

	/**
	 * 服务人员关联
	 * @Author   ksea
	 * @DateTime 2019-05-27T09:58:07+0800
	 * @return   [type]                   [description]
	 */
	public function emp(){
		return $this->hasMany('Employee', 'id', 'sfuserid');
	}
	/**
	 * 关联一个服务人员
	 * @Author   ksea
	 * @DateTime 2019-05-28T22:37:23+0800
	 * @return   [type]                   [description]
	 */
	public function empone(){
		return $this->hasOne('Employee', 'id', 'sfuserid');
	}
	/**
	 * 订单
	 * @Author   ksea
	 * @DateTime 2019-05-14T15:02:35+0800
	 * @return   [type]                   [description]
	 */
	public function order(){
		return $this->hasOne('order','id','orderid');
	}
	/**
	 * 二维码
	 * @Author   ksea
	 * @DateTime 2019-05-14T15:02:44+0800
	 * @return   [type]                   [description]
	 */
	public function code(){
		return $this->hasOne('code','id','codeid');
	}
	/**
	 * 卡片
	 * @Author   ksea
	 * @DateTime 2019-05-14T15:02:56+0800
	 * @return   [type]                   [description]
	 */
	public function card(){
		return $this->hasOne('card','id','cardid');
	}
	/**
	 * 
	 * @Author   ksea
	 * @DateTime 2019-05-27T09:54:53+0800
	 * @return   [type]                   [description]
	 */
	public function admin(){
		return $this->hasOne('app\admin\model\Admin','id','admin_id');
	}
	/**
	 * 评价关联
	 * @Author   ksea
	 * @DateTime 2019-05-27T09:55:26+0800
	 * @return   [type]                   [description]
	 */
	public function assess(){
		return $this->hasMany('ShopServerAssess','serverid','id')->order('power desc , id desc');
	}
	/**
	 * 服务拓展数据关联
	 * @Author   ksea
	 * @DateTime 2019-06-20T13:30:33+0800
	 */
	public function extrkv(){
		return $this->hasMany('Serverorderextr', 'serverid', 'id');
	}
	/**
	 * 用户信息输出
	 * @Author   ksea
	 * @DateTime 2019-06-25T02:24:15+0800
	 * @return   [type]                   [description]
	 */
	public function user(){
		return $this->hasOne('app\admin\model\shop\User','id','uid');
	}
	/**
	 * 追加订单数据
	 * @Author   ksea
	 * @DateTime 2019-07-25T17:22:00+0800
	 * @return   [type]                   [description]
	 */
	public function extrorder(){
		$where=[
			'attach' =>'3',
		];
		return $this->hasMany('app\admin\model\shop\Wechatpay', 'oid', 'orderid')->where($where)->field('total_fee,create_time,oid');
	}

	public function ad()
	{
		return $this->hasOne('Address','uid', 'uid')->order('id', 'DESC');
	}

	public function temp()
	{
		return $this->hasOne('OrderTemp','orderid', 'orderid');
	}
}