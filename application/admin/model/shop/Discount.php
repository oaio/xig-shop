<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use traits\model\SoftDelete;

/**
 * 优惠券管理
 */
class Discount extends Model 
{
	protected $name='shop_discount';
	/**
	 * 添加
	 * @Author   baohb
	 * @DateTime 2018-11-13T19:59:43+0800
	 */
	public function add_discount($dat){
       $data=
       [
        'name'=>$dat['name'],
        'actualpay'=>$dat['actualpay'],
        'status'=>1,
        'bak'=>$dat['bak'],
        'goodsid'=>$dat['goodsid'],
        'goods_name'=>$dat['goods_name'],
        'snorder'=>'sn_'.create_no(),
        'address' =>$dat['address'],
        'mobile' =>$dat['mobile'],
        'nickname' =>$dat['nickname'],
        'banner' =>$dat['banner'],
        'create_time'=>time(),
        'update_time'=>time(),
       ];
       if(isset($dat['admin_id'])){
            $data['admin_id']=$dat['admin_id'];
       }
       $goods =Db::name('shop_goods')->where("id={$dat['goodsid']}")->find();
       $data['goods_msg']=serialize($goods);
       $r=$this->create($data);
       if($r){
         $ret=[
           'code' => 1,
           'message' => '添加成功',
           'data' => $r['id'],
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
       }
       return $ret;
	}

	/**
	 * 
	 * @Author   baohb
	 * @DateTime 2018-12-7T19:59:30+0800
	 * @return   [type]                   [description]
	 */
	public function list_discount($search='',$paginate='20',$other=array(),$order='desc',$is_page=true,$admin_id=''){

        if(!empty($search)){
        	if(is_array($search)){
        		$temp=$search;
        		$search=$temp[0];
        		$paginate=$temp[1];
        		$other=$temp[2];
        		$order=$temp[3];
        		$is_page=$temp[4];
                if(isset($temp[5])){
                    $admin_id=$temp[5];
                }
        	}
        	$whereOr=[];
        	if($search){
	        	$whereOr['name']=array('like',"%{$search}%");
	        	$whereOr['uid']=array('like',"%{$search}%");
	        	$whereOr['from_id']=array('like',"%{$search}%");
        	}
            $where=[];
            if(isset($admin_id)&&!empty($admin_id)){
                $where['admin_id']=$admin_id;
            }
        	if(!empty($other)&&is_array($other)){
        		foreach ($other as $key => $value) {
	                if(!empty($value)){
	                    if($key!='dis_type'){
	                        $where[$key]=['like',"%{$value}%"];
	                    }
	                    else{
	                        $where[$key]=$value;
	                    }
	                }
        		}
        	}
			if($is_page){
				$data=$this->alias('d')->join('shop_userinfo u','d.uid=u.id','left')
                      ->field('d.*,u.nickname')
					  ->where($where)
					  ->whereOr($whereOr)
					  ->paginate($paginate);
			}
			else{
				$data=$this
					  ->where($where)
					  ->whereOr($whereOr)
					  ->select();
			}
        }
        else{
        	if($is_page){
				$data=$this
						->paginate($paginate);
        	}
        	else{
				$data=$this
						->select();
        	}
        }
        if($is_page)
        {
        	$page=$data->render();
        	return [$data,$page];
        }
		else
		{
			return $data;
		}
	}
	/**
	 * 详情
	 * @Author   baohb
	 * @DateTime 2018-12-7T19:59:51+0800
	 * @return   [type]                   [description]
	 */
    public function update_discount($dat)
    {
        $data=
            [
                'dis_type'=>$dat['dis_type'],
                'name'=>$dat['name'],
                'status'=>$dat['status'],
                'discount'=>$dat['discount'],
                'update_time'=>time()
        ];
        $where['id']=$dat['id'];
        if(isset($dat['admin_id'])){
            $where['admin_id']=$dat['admin_id'];
        }
        $r=$this->where($where)->update($data);
        if(is_numeric($r)){
            $ret=[
                'code' => 1,
                'message' => '修改成功',
                'data' => $r,
            ];
        }
        else{
            $ret=[
                'code' => 2,
                'message' => '修改失败',
                'data' => $r,
            ];
        }
        return $ret;
    }
}
