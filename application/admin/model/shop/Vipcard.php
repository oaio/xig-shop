<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
//操作记录模型
class Vipcard extends Model{

	protected $name='Vipcard';
    /**
     * 会员套餐
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function get_card($status=1){
    	 $where=[
    	 	'status'=>$status,
    	 ];
         $s_data = $this->where($where)->order('power desc')->select();
         $res['url']='';
         $res['msg']='全部数据';
         $res['count']=count($s_data);
         $res['is_ture']=1;
         $res['data']=$s_data;
         return $res;
    }
}
