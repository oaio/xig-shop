<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use traits\model\SoftDelete;
use think\Log;
use think\Hook;

/**
 * 订单管理
 */
class Order extends Model 
{
	protected $name='shop_order';
  
  public function base($query)
  {
    $query->with(['virdiscount','extrorder','tj']);
  }

	/**
	 * 添加
	 * @Author   ksea
	 * @DateTime 2018-11-13T19:59:43+0800
	 */
	public function add_order($dat){
       $type=0;
       /***************价格计算需要会员********************/
       $PayPrice=is_get_val('shop_goods',$dat['goodsid'],'commonprice,vipprice');

       $sPayPrice=$PayPrice['commonprice'];//应付款

       $aPayPrice=$PayPrice['commonprice'];//实付款

       $user_data=Db::name('shop_userinfo')->where('id',$dat['uid'])->find();
       /****会员价格介入***/
       if($user_data['vip_end_time']>=time()){
          $aPayPrice=$PayPrice['vipprice']?$PayPrice['vipprice']:$PayPrice['commonprice'];
          $type=2;
       }
       /*******优惠券介入*********/
       /**
        * 合成规则
        * 1.获取小哥豆比例
        * 2.初始化数据
        * 3.调用数据id
        * 4.简单数据输出[bean_id,discount_money](折扣id,能抵用金额)
        */
       /***********小哥豆介入************/
       if(isset($dat['bean'])&&!empty($dat['bean'])){
          //小哥豆接替
          $goodsid=$dat['goodsid'];
          $num=$dat['num'];
          $uid=$dat['uid'];
          $beanData=model('admin/shop/BeanRule')->GetBean($goodsid,$num,$uid);
          $dat['bean']=$beanData['data'];
          $rDis=Virdiscount::addDis($dat['bean'],$dat['uid']);
          if($rDis['code']==1){
             $discount_id=$rDis['data'];
          }
          else{
             Log::info([
              'func'     => 'bean',
              'message'  => '小哥豆介入失败',
              'data'     => $rDis,
             ]);
          }
       }
       /***********小哥豆介入************/
       /*******优惠券介入*********/
       //获取店铺城市编码
       if(!isset($dat['city_code'])){
           $city_id = is_get_val('store',$dat['store_id'],'city_id');
       }else{
           $city_id = $dat['city_code'];
       }

       $data=
       [
        'discount_id'=>isset($discount_id)?$discount_id:0,
        'num'=>$dat['num'],
        'address'=>isset($dat['address'])?$dat['address']:'',
        'shouldpay'=>$sPayPrice*$dat['num'],
        'actualpay'=>$aPayPrice*$dat['num'],
        'status' => 0,
        'bak'    => $dat['bak']?:'',
        'goodsid'=> $dat['goodsid'],
        'uid'	 =>$dat['uid'],
        'snorder'=> 'OR'.create_no(),
        'create_time'=>time(),
        'update_time'=>time(),
        'is_pay'	=>0,
        'store_id' =>$dat['store_id'],
        'type'  =>$type,
        'nickname'=>$dat['nickname'],
        'mobile'=>$dat['mobile'],
        'orgin' =>isset($dat['orgin'])?$dat['orgin']:'1',
        'eventtype'     =>isset($dat['event'])?$dat['event']:'',
        'platform_code'=> isset($dat['platform_code'])?$dat['platform_code']:'',
        'extr'         => isset($dat['extr'])?$dat['extr']:'0',
        'city_code' => $city_id,
       ];

       /*************整体价格修改**********/
       if(isset($dat['bean'])&&!empty($dat['bean'])){
          if($rDis['code']==1){
             $data['actualpay']-=$rDis['dismoney'];
             //发生错误完全抵扣
             if($data['actualpay']<0){
                $data['actualpay']=0;
             }
          }
          else{
             Log::info([
              'func'     => 'bean',
              'message'  => '小哥豆介入失败',
              'data'     => $rDis,
             ]);
          }
       }
       /*************整体价格修改**********/
       
       /************************************校验***********************************/
       /**********商品判断***********/
       $goods_data=is_get_val('shop_goods',$dat['goodsid'],'id,name');
       if($goods_data){
       	   $data['goods_name']= $goods_data['name']?:'';
       	   $data['goodsid']   = $goods_data['id']?:'';
       }
       else{
         $ret=[
           'code' => 3,
           'message' => '没有指定商品',
           'data' => [
           				'data'=>$goods_data,
           				'where'=>$dat['goodsid']
           			],
         ];
         return $ret;
       }
       
       $goods =model('admin/shop/Goods')->where('id',$dat['goodsid'])->find()->toArray();

       //来源记录
       if($goods['origin_id']>0){
         $data['goods_origin']=$goods['origin_table'].'_'.$goods['bargain_data']['freecount'];
       }


       $data['goods_msg']=json_encode($goods);
       
       /**********商品判断***********/




       /**********店铺判断***********/
       $admin_id=is_get_val('store',$dat['store_id'],'admin_id','store_id');
       if($admin_id){
           $data['admin_id']= $admin_id?:'';
       }
       else{
         $ret=[
           'code' => 3,
           'message' => '店铺不存在',
           'data' => [
                  'data'=>$admin_id,
                ],
         ];
         return $ret;
       }

       /**********店铺判断***********/

       /**************************************校验*********************************/
       $r=$this->create($data,true);
       if($r){
        
         /*******************公共下单成功锚点***********************/
         Hook::listen('orderset',$r);
         /*******************公共下单成功锚点***********************/

         $data['mini_openid'] = isset($user_data['mini_openid'])?$user_data['mini_openid']:'';
         $ret=[
           'code' => 1,
           'message' => '添加订单成功',
           'data' => $r,
           'order_data' => $data,//小程序使用——by JJ
         ];

       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
         Log::info([
         	'func'	=>'add_order',
         	'data'	=>$ret,
         ]);
       }
       return $ret;
	}
	/**
	 * 
	 * @Author   ksea
	 * @DateTime 2018-11-17T16:30:47+0800
	 * @param    [type]                   $dat [description]
	 * @return   [type]                        [description]
	 */
	public function update_order($dat){
      if(isset($dat['shouldpay']))$data['shouldpay']=$dat['shouldpay'];
      if(isset($dat['actualpay']))$data['actualpay']=$dat['actualpay'];
      if(isset($dat['status']))$data['status']=$dat['status'];
      if(isset($dat['bak']))$data['bak']=$dat['bak'];
      if(isset($dat['goodsid']))$data['goodsid']=$dat['goodsid'];
      if(isset($dat['goods_name']))$data['goods_name']=$dat['goods_name'];
      if(isset($dat['address']))$data['address']=$dat['address'];
      if(isset($dat['mobile']))$data['mobile']=$dat['mobile'];
      if(isset($dat['nickname']))$data['nickname']=$dat['nickname'];
      if(isset($dat['is_type']))$data['is_type']=$dat['is_type'];
      if(isset($dat['pay_id']))$data['pay_id']=$dat['pay_id'];
      if(isset($dat['is_pay']))$data['is_pay']=$dat['is_pay'];

	  $where['id']=$dat['id'];
	  $r=$this->where($where)->update($data);
	  if($r){
         $ret=[
           'code' => 1,
           'message' => '修改成功',
           'data' => $r,
         ];
	  }
	  else{
         $ret=[
           'code' => 2,
           'message' => '修改失败',
           'data' => $r,
         ];
         Log::info([
         	'func'	=>'update_order',
         	'data'	=>$ret,
         ]);
	  }
	  return $ret;
	}

    /**
     * 
     * @Author   ksea
     * @DateTime 2018-12-24T10:46:14+0800
     * @param    array
     * $search [
     *     0=> '搜索'
     *     1=> '单页数据'
     *     2=>  [
     *           'id'  = 1,
     *           'key' = 2,
     *          ]
     *     3=> 'desc / '
     *     4=> '是否分页返回'
     *     5=> '归属'
     *     ]
     * @return   [type]                           [description]
     */
    public function order_list($search=array()){
            if(input('param.page','')&&session('s_order_list')){
              $search=session('s_order_list');
            } 
            else{
              session('s_order_list',$search);
            }
            $temp_search=$search;
            if(is_array($search)){
              $temp=$search;
              $search=$temp[0];
              $paginate=$temp[1];
              $other=$temp[2];
              $order=$temp[3]?:'desc';
              $is_page=$temp[4];
              $admin_id=$temp[5];
            }
            $where=[];
            if(isset($admin_id)&&!empty($admin_id)){
               $where['admin_id']=$admin_id;
            }
            /**已支付订单**/
            $where['is_pay']=1;
            /*****删除代用***/
            //$where['is_del']=array('eq','0');
            $splice=[
              'create_time',
              'update_time',
            ];
            if(!empty($other)&&is_array($other)){
              foreach ($other as $key => $value){
                  if(strlen($value)==0){
                      unset($other[$key]);
                  }
                  else{
                      if(in_array($key,$splice)){
                          $sp_data=explode(' ', $value);
                          $where[$key]=['BETWEEN',strtotime($sp_data[0]).','.strtotime($sp_data[2])];
                      }
                      else{
                          $where[$key]=$value;
                      }
                  }
              }
            }
            if($search){
                $san_like=[
                   'snorder',
                   'nickname',
                   'mobile',
                   'address',
                ];
                $link_scane=implode('|', $san_like);
                $data=$this
                    ->with(['paylog','card','service.emp','virdiscount'])
                    ->where($link_scane,'like',"%{$search}%")
                    ->where($where)
                    ->order('id '.$order)
                    ->paginate($paginate);
            }
            else{
              $data=$this
                  ->with(['paylog','card','service.emp','virdiscount'])
                  ->where($where)
                  ->order('id '.$order)
                  ->paginate($paginate);

            }
            $page=$data->render();
            return [$data,$page,$temp_search];
    }
    /**
     * 手机端列表
     * 用户获取
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function get_order($uid,$limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);
            $where['uid']=$uid;
            $where['status']=$status;
            if($status==1){
              $where['is_pay']=1;
            }

            if(strlen($limit)<=0){
                 $s_data = $this->with(['goods','card','store'])->where($where)->order('id desc')->select();
                 $res['url']=config('mobileUrl.evaluate');
                 $res['url_out']=config('mobileUrl.details');
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->with(['goods','card','store'])->where($where)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']=config('mobileUrl.evaluate');
                    $res['url_out']=config('mobileUrl.details');
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']=config('mobileUrl.evaluate');
                    $res['url_out']=config('mobileUrl.details');
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

    public function add_buying_details_id($params){
       $this->where('id',$params['id'])->update(['group_details_id'=>$params['group_details_id']]);
    }

  /**
   * 店铺
   * @Author   ksea
   * @DateTime 2019-01-12T12:07:34+0800
   * @return   [type]                   [description]
   */
  public function store(){
      $allow=[
        'address',
        'company',
        'store_id',
        'telphone',
        'nickname',
        'ruler',
        'ruler_phone',
      ];
      $allowfiled=implode(',',$allow);
      return $this->hasOne('Store','store_id','store_id')->field($allowfiled);
  }
    /**
     * 卡片 
     * @Author   ksea
     * @DateTime 2019-01-03T20:36:22+0800
     * @return   [type]                   [description]
     */
    public function card(){
      return $this->hasOne('Card','orderid','id');
    }
	/**
	 * 详情
	 * @Author   ksea
	 * @DateTime 2018-11-13T19:59:51+0800
	 * @return   [type]                   [description]
	 */
	public function detail_order($id){
		    return $this->with(['goods'])->where('id','=',$id)->find();
	}
  /**
   * 商品数据
   * @Author   ksea
   * @DateTime 2019-01-03T18:58:01+0800
   * @return   [type]                   [description]
   */
    public function goods(){
        return  $this->hasOne('Goods','id','goodsid');
    }
    /**
     * 追加订单关联
     * @Author   ksea
     * @DateTime 2019-02-27T10:26:40+0800
     * @return   [type]                   [description]
     */
    public function paylog(){
        return $this->hasMany('Wechatpay','oid','id');
    }
  /**
   * 单个预约关联
   * @Author   ksea
   * @DateTime 2019-04-29T16:26:39+0800
   * @return   [type]                   [description]
   */
	public function service(){
    $where['status']=array('in','0,1,2,3,4');
		return $this->hasOne('Serverorder','orderid')->where($where)->order('id decs');
	}

	public function getCountingAttr($value, $data)
	{
		return $_SERVER['REQUEST_TIME'] - $data['create_time'];
	}

	public function todo()
	{
		return $this->hasOne('ShopOrderTodo','oid');
	}

	public function user()
	{
		return $this->hasOne('User','id', 'uid');
	}
  /**
   * 多个预约关联
   * @Author   ksea
   * @DateTime 2019-04-29T16:26:52+0800
   * @return   [type]                   [description]
   */
  public function serverMany(){
    $where['status']=array('in','0,1,2,3,4');
    return $this->hasMany('Serverorder','orderid')->where($where)->order('id decs');
  }
  /**
   * 小哥豆抵扣信息
   * @Author   ksea
   * @DateTime 2019-05-07T14:38:03+0800
   * @return   [type]                   [description]
   */
  public function virdiscount(){
    $where=[
      'is_pay'=>1,
    ];
    return $this->hasOne('app\admin\model\shop\Virdiscount', 'id', 'discount_id')->where($where);
  }
    /**
     * 手机端店铺后台订单查询
     * 店铺获取
     * @DateTime 2019-04-29T16:36:39+0800
     * @param    [type]                   $admin_id [店铺id]
     * @param    string                   $limit    [限制条数]
     * @param    string                   $getTime  [时间节点]
     * @param    string                   $search   [查询条件]
     * @param    array                    $other    [自定义查询]
     * @param    string                   $btime    [查询时间节点]
     * @return   [type]                             [description]
     */
    public function StoreGetOrder($admin_id,$btime='0',$limit='',$getTime='',$search='',$other=array()){

            $query='';
            if(strlen($getTime)<2){
              $getTime=time();
            }
            /***********时间节点***************/
            if(!empty($btime)&&$btime!='0'){
              $btime=strtotime($btime);
              $query.="`create_time` BETWEEN $btime  AND  $getTime";
            }
            /**********时间节点****************/
            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
                    if(empty($value)){
                        unset($other[$key]);
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
            /*****************自定义查询*******************/
            
            /*****************店铺数据******************/
            $where['admin_id']=$admin_id;
            /*****************店铺数据******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'snorder',
                    'bak',
                    'goods_name',
                    'nickname',
                    'mobile'
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            $where['status']=1;
            if(strlen($limit)<=0){
                 $s_data = $this->with(['serverMany','virdiscount'])->where($where)->where($query)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=date('Y-m-d',$getTime);
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->with(['serverMany','virdiscount'])->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }

	/**
	 * 订单转入待办
	 *
	 * @author d3li 17/05/2019
	 * @param int $oid 订单id
	 * @return bool
	 * @throws \think\exception\DbException
	 */
	public function pause($oid)
    {
	    $row = self::get($oid);
	    if(!$row) return !1;
	    $info = session('admin');
	    $data = [
		    'oid'=>$oid,
		    'admin_id'=>$info['id'],
		    'admin'=>$info['nickname'],
		    'diary'=>$row->schedule,
	    ];
	    $row->schedule = 9;
	    $row->save();
	    $gd = Serverorder::get(['orderid' => $oid]);
	    privacy_release($gd['mobile']);

		$gd->save(['status'=> 0, 'sfuserid'=>'', 'push_time'=>0]);// 'appoint_start'=>0,

	    $res = ShopOrderTodo::get($oid);
	    if($res){
	    	$res->update($data);
	    }else{
		    ShopOrderTodo::create($data);
	    }
	    ShopOrderHistory::create([
	    	'oid'=>$oid,
		    'desc' => $info['nickname'].' 转入待办'
	    ]);
	    return true;
    }

	/**
	 * 取消订单
	 *
	 * @author d3li 2019/8/5
	 * @param $oid
	 * @return bool
	 * @throws \think\exception\DbException
	 */
	public function cancel($oid)
    {
	    $row = self::get($oid);
	    if(!$row) return !1;
	    $row->schedule = 4;
	    $row->save();
	    $gd = Serverorder::get(['orderid'=>$oid]);
	    privacy_release($gd['mobile']);
	    Serverorder::update(['status'=>3], ['orderid'=>$oid]);
	    ShopOrderHistory::create([
		    'oid'=>$oid,
		    'desc' => session('admin.nickname').' 取消订单'
	    ]);
    }

    public function getScheduleAttr($value, $row)
    {
    	return empty($value)?$_SERVER['REQUEST_TIME'] - $row['create_time'] > 7200 ? 1 : 0 : $value;
    }

    /**
     * 发送模板消息
     * @Author   ksea
     * @DateTime 2019-07-07T16:58:24+0800
     * @return   [type]                   [description]
     */
    public function sendtemp($params){

      $oid=$params['oid'];

      $openid_arr=[];

      $orderdata=$this
                ->alias('order')
                ->with(['virdiscount','storeuserinfo'])
                ->where('id',$oid)
                ->find();

      
      if(!$orderdata['storeuserinfo']){
        return '用户未绑定';
      }

      $dismoney=$orderdata['virdiscount']?$orderdata['virdiscount']['dismoney']:0;

      $pay=$orderdata['actualpay'];

      $goods_name=$orderdata['goods_name'];

      $openid_arr=[
        $orderdata['storeuserinfo']['openid'],
      ];

      $data=[
        'first' =>'嘿！老板你有一个新订单！小哥豆抵扣'.$dismoney.'元！',
        'orderMoneySum'=>$pay,
        'orderProductName'=>$goods_name,
        'Remark'=>'若有问题请致电400-8185-088，小哥帮将竭诚为您服务。',
      ];
      
      $link=config('wechattemplate.storeorder')['link'];

      $template_id=config('wechattemplate.storeorder')['tempid'];
      $myGrab=new \wechat\DiytTemplate($template_id);
      
      $myGrab::SendGroup($openid_arr,$data,$link);


    }

    /**
     * 订单对应店铺对应绑定用户
     * @Author   ksea
     * @DateTime 2019-07-07T17:35:24+0800
     * @return   [type]                   [description]
     */
    public function storeuserinfo(){
      return $this->hasOne('User','store_bind', 'store_id');
    }
  /**
   * 追加订单数据
   * @Author   ksea
   * @DateTime 2019-07-25T17:22:00+0800
   * @return   [type]                   [description]
   */
  public function extrorder(){
    $where=[
      'attach' =>'3',
    ];
    return $this->hasMany('app\admin\model\shop\Wechatpay', 'oid', 'id')->where($where)->field('total_fee,create_time,oid');
  }

  /**
   * 推荐数据
   * @Author   ksea
   * @DateTime 2019-08-02T10:34:22+0800
   * @return   [type]                   [description]
   */
  
  public function tj(){

      return $this->hasOne('app\admin\model\shop\Storekeeper','store_id','store_id');
  
  }

  public function expand()
  {
  	 return $this->hasMany('UserWantPay','orderid')->where('is_pay', 1);
  }

  public function a()
  {
	  return $this->hasOne('Address','uid', 'uid')
		  ->order('id', 'DESC');
  }

  public function temp()
  {
	  return $this->hasOne('OrderTemp','orderid');
  }
}
