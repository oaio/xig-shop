<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;
use think\Model;

/**
 * Invite.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/7/11  10:44
 * @see      https://gitee.com/d3li/
 * @version 2.07.11
 * @describe
 * 邀请模型
 */
class Invite extends Model
{
	protected $table = 'fa_shop_invite';
	protected $autoWriteTimestamp = 'int';

	public function c()
	{
		return $this->hasOne('app\admin\model\Region',
			'region_id', 'invite_city');
	}

	public function m()
	{
		return $this->hasOne('Invite', 'fuid', 'fuid')
			->group('fuid')
			->field('fuid,COUNT(*) as num');
	}

	public function success()
	{
		return $this;
	}

    /**
     * 用户邀请数据
     * 店铺获取
     * @DateTime 2019-04-29T16:36:39+0800
     * @param    [type]                   $uid [用户id]
     * @param    string                   $limit    [限制条数]
     * @param    string                   $getTime  [时间节点]
     * @param    string                   $search   [查询条件]
     * @param    array                    $other    [自定义查询]
     * @param    string                   $btime    [查询时间节点]
     * @return   [type]                             [description]
     */
    public function GetInvite($uid,$limit='',$getTime='',$search='',$other=array()){

            $query='';
            if(strlen($getTime)<2){
              $getTime=time();
            }

            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
                    if(empty($value)){
                        unset($other[$key]);
                    }
                    else{
                        $where[$key]=$value;
                    }
                }
            }
            /*****************自定义查询*******************/
            
            /*****************用户邀请者数据******************/
            $where['fuid']=$uid;
            /*****************用户邀请者数据******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'invitee',
                    'tel'
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->where($query)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=date('Y-m-d',$getTime);
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }
}