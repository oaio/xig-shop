<?php
namespace app\admin\model\shop;
use think\Db;
use think\Model;
use think\Validate;
use think\Log;

/**
 * 主表用户表
 */

class UserDistribution extends Model
{

	protected $name='ShopUserinfo';
    
    public function base($query){
        $query->where("phone !=''");
    }

    /**
     * 贡献表
     * @Author   ksea
     * @DateTime 2019-03-27T20:28:04+0800
     * @return   [type]                   [description]
     */
    public function outcome(){

    	return $this->hasMany('ShopOutcome','uid','id');

    }

	/**
	 * 收益统计
	 * @author d3li 2019/5/28
	 * @return \think\model\relation\HasOne
	 */
	public function income()
    {
    	return $this->hasOne('ShopOutcome', 'fuid', 'id')
		    ->field('fuid,COUNT(id) as num,SUM(price) as total');
    }

    public function store()
    {
    	return $this->hasOne('Store', 'store_id', 'store_bind');
    }

    /**
     * 粉丝表
     * @Author   ksea
     * @DateTime 2019-03-27T20:28:04+0800
     * @return   [type]                   [description]
     */
    public function fans(){

        return $this->hasMany('ShopFans','fuid','id');

    }
    /**
     * 获取粉丝收益
     * @Author   ksea
     * @DateTime 2019-03-28T12:53:32+0800
     */
    public function GetFansData(){
        return $this->hasOne('ShopFans','fuid','id')->field("fuid,sum(sumsales) as mysumsales,sum(sumorder) as mysumorder");
    }
}
