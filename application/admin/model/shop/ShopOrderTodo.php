<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model\shop;
use think\Model;

/**
 * ShopOrderTodo.php 转入待办时，保存原数据
 *
 * @author d3li <d3li@sina.com>
 * @create：15/05/2019  7:48 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.15
 * @describe
 */
class ShopOrderTodo extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}