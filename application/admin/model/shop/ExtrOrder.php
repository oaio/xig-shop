<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;
//操作记录模型
class ExtrOrder extends Model{

    // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
    protected $autoWriteTimestamp=true;
    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';

	protected $name='extr_order';
	/**
	 * 绑定管理员
	 * @Author   ksea
	 * @DateTime 2019-06-11T16:45:35+0800
	 * @return   [type]                   [description]
	 */
   	public function admin(){

   		return $this->hasOne('app\admin\model\Admin','id','admin_id');
   
    }
    /**
     * 商品
     * @Author   ksea
     * @DateTime 2019-06-11T19:11:10+0800
     * @return   [type]                   [description]
     */
   	public function goods(){

   		return $this->hasOne('app\admin\model\shop\Goods','id','goodsid');
   
    }
}
