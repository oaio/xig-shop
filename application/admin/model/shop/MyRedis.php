<?php

namespace app\admin\model\shop;

use think\Model;

//redis操作

class MyRedis extends Model{

    /***************************************评价************************************************/
    /**
     * 添加某个准备评价数据进如redis集合
     * @Author   ksea
     * @DateTime 2019-08-08T13:19:10+0800
     * @return   [type]                   [description]
     */
    public function assessAdd($server_id){

      $redis_data = 'Y-m-d';
      $redis      = new \Redis();
      $redis->connect('127.0.0.1', 6379);
      $temp_name = 'server_assess_'.date($redis_data);

      if($redis->scard($temp_name)>0){
         if($redis->sismember($temp_name,$server_id)==0){
            $redis->sadd($temp_name,$server_id);
         }
      }else{
        $redis->sadd($temp_name,$server_id);
      }

    }

    /**
     * 去除某个服务评价在redis里面
     * @Author   ksea
     * @DateTime 2019-08-08T13:19:53+0800
     * @return   [type]                   [description]
     */
    
    public function assessRem($server_id,$end_time){

      $redis_data = 'Y-m-d';//处理层级
      $data_name  =date($redis_data,strtotime(date($redis_data,strtotime($end_time))));
      $redis      = new \Redis();
      $redis->connect('127.0.0.1', 6379);
      $temp_name = 'server_assess_'.$data_name;
      if($redis->scard($temp_name)>0){
         if($redis->sismember($temp_name,$server_id)==1){
            //去除
            $redis->srem($temp_name,$server_id);
         }
         else{
            myErrorLog('redisError.txt',json_encode([$temp_name,$server_id]),'redis');
         }
      }else{
            myErrorLog('redisError.txt',json_encode([$temp_name,$server_id]),'redis');
      }

    }
    /***************************************评价************************************************/
}
