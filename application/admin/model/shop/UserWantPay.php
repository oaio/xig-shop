<?php

namespace app\admin\model\shop;
use think\Model;

class UserWantPay extends Model{

	protected $name='user_want_pay';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

}