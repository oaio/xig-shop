<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;

class Banner extends Model{
    protected $name='shop_index';

    public function getImgLinkAttr($value)
    {
	    return empty($value) ? '/assets/img/default.png' : $value;
    }
}