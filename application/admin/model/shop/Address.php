<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Session;
use think\Validate;
use think\Hook;

class Address extends Model
{
	public $name='shop_address';

    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected $uid=0;


	protected function initialize()
	{
		parent::initialize();
		$this->uid = Session::get('user.id');
	}

	/**
	 * 添加地址
	 * @Author   ksea
	 * @DateTime 2019-04-10T10:33:25+0800
	 * @param    [type]                   $dat [description]
	 */
	public function addAddress($dat){
		// $dat=
		// [
		// 	'name' =>'我是默认开发者ksea';
		// 	'phone' =>13549906145;
		// 	'address' =>'宝安区平安路保安小学';
		// 	'uid' =>2;
		// 	'region' =>
		// 	[
		// 		'9655',//省id
		// 		'9683',//市id
		// 		'9684',//区id
		// 	];
		// ];

        $vali_date = [
            'name' => isset($dat['name']) ? $dat['name'] : '',
            'phone' => isset($dat['phone']) ? $dat['phone'] : '',
            'address' => isset($dat['address']) ? $dat['address'] : '',
            'uid' => isset($dat['uid']) ? $dat['uid'] : $this->uid,
            'region' => isset($dat['region']) ? $dat['region'] : ''
        ];
        $rule =
            [
//                'name' => 'require|max:60',
//                'phone' => 'number|max:11|min:11',
//                'address' => 'require|max:120',
                'uid' => 'require',
                'region' => 'require',
            ];
        $validate = new Validate($rule);
        if ($validate->check($vali_date)) {
            $dat['region'] = implode(',', $dat['region']);
            $dat['street'] = isset($dat['street']) ? $dat['street'] : '';
            $dat['lat'] = isset($dat['lat']) ? $dat['lat'] : '';
            $dat['lng'] = isset($dat['lng']) ? $dat['lng'] : '';
            $this->allowField(true)->save($dat);
            $res['code'] = 1;
            $res['msg'] = '添加成功';
            $res['data'] = $this->id;
        } else {
            $res['code'] = 0;
            $res['msg'] = $validate->getError();
        }
        return $res;
	}
    /**
     * 获取用户地址
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function getAddress($uid='',$limit='',$getTime='',$status=1){

            if(strlen($getTime)<2){
              $getTime=time();
            }

            $where['create_time']=array('lt',$getTime);
            $where['uid']=empty($uid)?$this->uid:$uid;

            if(strlen($limit)<=0){
                 $s_data = $this->where($where)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $this->where($where)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
            }

            return $res;
    }
    /**
     * 地址更新
     * @Author   ksea
     * @DateTime 2019-04-10T10:37:18+0800
     * @param    [type]                   $data [description]
     * @return   [type]                         [description]
     */
    public function updateAddress($dat){
		$dat['region']=implode(',', $dat['region']);

		$vali_date=[
			'id' =>$dat['id'],
		];
        $rule=
        [
            'id' =>'require',
        ];
        $validate=new Validate($rule);
        if($validate->check($vali_date)){
            unset($dat['__token__']);
            $r=$this->allowField(true)->update($dat);
            $res['code']=1;
            $res['msg']='修改成功';
            $res['data']=$r;
            
        }
        else{
            $res['code']=0;
            $res['msg']=$validate->getError();
        }
        return $res;
    }
    /**
     * 删除操作
     * @Author   ksea
     * @DateTime 2019-04-10T10:37:18+0800
     * @param    [type]                   $data [description]
     * @return   [type]                         [description]
     */
    public function deleteAddress($dat){

		$vali_date=[
			'id' =>$dat['id'],
		];
        $rule=
        [
            'id' =>'require',
        ];
        $validate=new Validate($rule);
        if($validate->check($vali_date)){

            $r=$this->where(['id'=>$dat['id']])->delete();
            $res['code']=1;
            $res['msg']='删除成功';
            $res['data']=$r;
            
        }
        else{
            $res['code']=0;
            $res['msg']=$validate->getError();
        }
        return $res;
    }
    /**
     * 默认地址
     * @Author   ksea
     * @DateTime 2019-04-11T11:39:31+0800
     */
    public function setDefault($id){

        $validate=new Validate([
            'id' => 'require',
        ]);
        $vali_date=[
            'id' => $id,
        ];
        if($validate->check($vali_date)){
            $address=$this->get($id)->toArray();
            $this->save(['is_default'=>0],['uid'=>$address['uid']]);
            $this->save(['is_default'=>1],['id'=>$id]);
            /******************设置绑定************************/
            $region=Db::name('region')->where(['region_id'=>['in',$address['region']]])->limit(3)->select();
            $param=[
                'province_id'=>$region[0]['region_id'],
                'province'   =>$region[0]['region_name'],
                'city_id'    =>$region[1]['region_id'],
                'city'       =>$region[1]['region_name'],
                'area_id'    =>$region[2]['region_id'],
                'area'       =>$region[2]['region_name'],
                'address'    =>$address['address'],
                'id'         =>$address['uid'],
            ];
            Hook::listen('setDefaultAddress',$param);
            /*******************设置绑定***********************/
            $res['code']=1;
            $res['msg'] ='设置成功';
        }
        else{
            $res['code']=0;
            $res['msg']=$validate->getError();
        }
        return $res;
    }
}