<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//操作记录模型
class AnalysisBuy extends Model{

	protected $name='analysis_buy';

	public function base($query){

		$query->with(['thatbean']);
	
	}

    public function getGetbeanAttr($key,$value)
    {	
    	$res=input('param.');
    	$res_arr=json_decode($res['filter'],true);
    	$temp_bean=0;
    	$where['userid']=$value['uid'];
    	$where['type']=array('in','1,3');
    	if(!empty($res_arr['create_time'])){
    		$temp_time=explode(' - ',$res_arr['create_time']);
    		$where['transaction_ime']=[
    			'between',
    			 strtotime($temp_time[0])>=strtotime('2019-08-04 00:00:00')?strtotime($temp_time[0]).','.strtotime($temp_time[1]):strtotime('2019-08-04 00:00:00').','.strtotime($temp_time[1]),
    		];
    	}
    	else{
    		$where['transaction_ime']=[
    			'>=',
    			 strtotime('2019-08-04 00:00:00'),
    		];
    	}
    	$temp_bean=model('admin/shop/RedEnvelopesRecord')->where($where)->count();
        return $temp_bean;
    }

	public function analysis($order){

		/***********************用户购买详情**************************/

		$whereBuy=[
		  'uid' =>$order['uid'],
		  'orgin' =>$order['orgin'],
		  'city_code' =>$order['city_code'],
		  'create_time'=>strtotime(date('Y-m-d')),
		];

		$modelAnalysisBuy=model('admin/shop/AnalysisBuy');

		$analysis_temp=$modelAnalysisBuy->where($whereBuy)->find();
		if(!$analysis_temp){

		  $AnalysisBuyData=[
		    'uid'         => $order['uid'],
		    'userpaynum'  => 1,
		    'suserpaysum' => $order['shouldpay'],
		    'auserpaysum' => $order['actualpay'],
		    'discountsum' => isset($order['virdiscount']['dismoney'])?$order['virdiscount']['dismoney']:0,
		    'city_code'   => $order['city_code'],
		    'orgin'          => $order['orgin'],
		    'create_time' => strtotime(date('Y-m-d')),
		  ];

		  $modelAnalysisBuy->save($AnalysisBuyData);

		}
		else{

		  $jaindan=model('admin/shop/order')->where([
		    'is_pay'=>1,
		    'create_time'=>[
		      'between',
		       strtotime(date('Y-m-d')).','.strtotime(date('Y-m-d').'+1 day'),
		    ],
		    'uid'=>$order['uid'],
		  ])->select();

		  $paynum=$analysis_temp['userpaynum'];
		  if(count($jaindan)==1){
		    $paynum=$paynum+1;
		  }
		  
		  $AnalysisBuyData=[

		    'userpaynum'  => $analysis_temp['userpaynum']+1,
		    'suserpaysum' => $analysis_temp['suserpaysum']+$order['shouldpay'],
		    'auserpaysum' => $analysis_temp['auserpaysum']+$order['actualpay'],
		    'discountsum' => isset($order['virdiscount']['dismoney'])?$order['virdiscount']['dismoney']+$analysis_temp['discountsum']:$analysis_temp['discountsum'],

		  ];

		  $analysis_temp->save($AnalysisBuyData);

		}
		/***********************用户购买详情**************************/

	}
	public function user(){
		return $this->hasOne('User','id','uid');
	}

	public function thatbean(){

		return $this->hasMany('RedEnvelopesRecord','userid','uid');
	
	}

}
