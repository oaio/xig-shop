<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\model\shop;
use think\Model;

/**
 * ShopOrderHistory.php 订单流转记录
 *
 * @author d3li <d3li@sina.com>
 * @create：16/05/2019  10:26 AM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.16
 * @describe
 */
class ShopOrderHistory extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}