<?php

namespace app\admin\model\shop;

use think\Db;
use think\Model;
use think\Request;

//操作记录模型
class City extends Model{

	protected $name='City';

	/**
	 * 获取城市数据
	 * @Author   ksea
	 * @DateTime 2019-08-05T14:06:55+0800
	 * @return   [type]                   [description]
	 */
	public function getCity(){

		$data=$this->where('status',1)->select();
		$data_json=[];
		foreach ($data as $key => $value) {
			$data_json[$value->city_code]=$value->city_name;
		}
		if(empty($data_json)){
			$data_json=[
				'440300' =>'深圳'
			];
		}
		return $data_json;
	}

}
