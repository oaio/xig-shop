<?php
namespace app\admin\model\shop;

use think\Db;
use think\Model;

use app\common\model\Common;

class Sku extends Model{
  	protected $name='shop_sku';
    /**
     * 获取对应商品sku的数据关联列表
     * 返回二位数组
     * array(
     *   key =>value
     *   sku_list_data =>array(
     *     key => value
     *    )
     * )
     * @Author   ksea
     * @DateTime 2018-11-17T10:41:53+0800
     * @param    [type]                   $sku_group_id [description]
     * @return   [type]                             [description]
     */
    public function get_sku_goods_data($sku_group_id){
        if(isset($sku_group_id)){
           $where['id']=array('in',$sku_group_id);
           $ret=Db::name('shop_sku_group')->where($where)->select();
           if(count($ret)>0){
             foreach ($ret as &$value) {
                 $value['sku_list_data']=$this->show_sku_list($value['sku_list']);
             }
           }
           return $ret;
           exit();
        }
        return null;
    }

    /**************************sku组的操作***************************/
    public function add_sku_group($dat){
       $data=
       [
        'name'=>$dat['name'],
       ];
       $r=Db::name('shop_sku_group')->insertGetId($data);
       if($r){
         $ret=[
           'code' => 1,
           'message' => '添加成功',
           'data' => $r,
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
       }
       return $ret;
    }


    /**
     * 更新数据
     * @Author   ksea
     * @DateTime 2018-11-13T19:22:01+0800
     * @param    [type]                   $data [所需数据]
     * @return   [type]                         [description]
     */
    public function update_sku_group($dat){

      if(isset($dat['name']))$data['name']=$dat['name'];
      if(isset($dat['sku_list']))$data['sku_list']=$dat['sku_list'];
      
       $data['id']=$dat['id'];
       $data['status']=1;
       $r=Db::name('shop_sku_group')->update($data);
       if($r){
         $ret=[
           'code' => 1,
           'message' => '修改成功',
           'data' => $r,
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '修改失败',
           'data' => $r,
         ];
       }
       return $ret;
    }
    /**
     * 更新数据
     * @Author   ksea
     * @DateTime 2018-11-13T19:22:01+0800
     * @param    [type]                   $data [所需数据]
     * @return   [type]                         [description]
     */
    public function del_sku_group($id){
       // Db::startTrans();
        $temp_r=Db::name('shop_sku_group')->where(['id'=>$id])->value('sku_list');
        $r=Db::name('shop_sku_group')->delete($id);
        if(is_numeric($r)){
           $rt=Db::name('shop_sku')->delete($temp_r);
           if(is_numeric($rt)){
               $ret=[
                 'code' => 1,
                 'message' => '删除成功',
                 'data' => $r,
               ];
               //Db::commit();    
           }
           else{
               $ret=[
                 'code' => 3,
                 'message' => '删除失败',
                 'data' => $r,
               ];
               //Db::rollback();
           }
        }
        else{
           $ret=[
             'code' => 2,
             'message' => '删除失败',
             'data' => $r,
           ];
           Db::rollback();
        }
       return $ret;
    }
    /**************************sku组的操作***************************/





    /**************************sku的操作***************************/
  	public function add_sku($dat){
       $data=
       [
        'name'=>$dat['name'],
        'price'=>$dat['price'],
        'vipprice'=>$dat['vipprice'],
        'times'=>$dat['times']
       ];
       $r=$this->create($data);
       if($r){
         $ret=[
           'code' => 1,
           'message' => '添加成功',
           'data' => $r['id'],
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '添加失败',
           'data' => $r,
         ];
       }
       return $ret;
  	}
    /**
     * 显示对应商品sku列表
     * @Author   ksea
     * @DateTime 2018-11-13T19:09:01+0800
     * @param    [type]                   $id [description]
     * @return   [type]                   $all  [显示全部sku]
     */
  	public function show_sku_list($id='',$all=false){
       $getall =new Common();
       $id_arr['id']=array('in',$id);
       if($all){
          $data=$this->select();
       }
       else{
          $data=$this->where($id_arr)->select();
       }
       return $getall->getdataAll($data);
        
  	}

    /**
     * 更新数据
     * @Author   ksea
     * @DateTime 2018-11-13T19:22:01+0800
     * @param    [type]                   $data [所需数据]
     * @return   [type]                         [description]
     */
  	public function update_sku($data){
      if(isset($dat['name']))$data['name']=$dat['name'];
      if(isset($dat['price']))$data['price']=$dat['price'];
      if(isset($dat['vipprice']))$data['vipprice']=$dat['vipprice'];
      if(isset($dat['times']))$data['times']=$dat['times'];
       $data['id']=$dat['id'];
       $r=$this->update($data);
       if(is_numeric($r)){
         $ret=[
           'code' => 1,
           'message' => '修改成功',
           'data' => $r,
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '修改失败',
           'data' => $r,
         ];
       }
       return $ret;
  	}
    /**
     * 更新数据
     * @Author   ksea
     * @DateTime 2018-11-13T19:22:01+0800
     * @param    [type]                   $data [所需数据]
     * @return   [type]                         [description]
     */
    public function del_sku($id){
       $r=$this->delete(['id'=>$id]);
       if(is_numeric($r)){
         $ret=[
           'code' => 1,
           'message' => '删除成功',
           'data' => $r,
         ];
       }
       else{
         $ret=[
           'code' => 2,
           'message' => '删除失败',
           'data' => $r,
         ];
       }
       return $ret;
    }
    /**************************sku的操作***************************/
}