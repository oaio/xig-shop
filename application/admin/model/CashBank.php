<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * CashBank.php
 *
 * @author d3li <d3li@sina.com>
 * @create：29/03/2019  10:50 AM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.29
 * @describe
 */
class CashBank extends Model
{
	public function cash()
	{
		return $this->belongsTo('Cash', 'cashid');
	}
}