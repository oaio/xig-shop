<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * Cash.php 提现表
 *
 * @author d3li <d3li@sina.com>
 * @create：29/03/2019  9:30 AM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.29
 * @describe
 */
class Cash extends Model
{
	protected $autoWriteTimestamp = 'int';

	public function bank()
	{
		return $this->hasOne('CashBank', 'cashid');
	}

	public function user()
	{
		return $this->hasOne('ShopUserinfo', 'openid', 'openid');
	}

	public function store()
	{
		return $this->hasOne('Store', 'store_id', 'store_id');
	}

	/**
	 * 自动提现
	 *
	 * @author d3li 2019/5/30
	 * @return $this
	 * @throws \Exception
	 */
	public function au2()
	{
		$start = explode(' ',microtime());
		$order = model('app\admin\model\shop\Order');
		$zero = strtotime(date('Ymd'));
		$data = $order
			->alias('o')
			->field('f.id, f.openid, f.nickname, 
			o.store_id,SUM(o.shouldpay) as total,GROUP_CONCAT(o.id) as ids')
			->join('fa_shop_userinfo f', 'f.store_bind = o.store_id')
			->where(['o.is_pay'=>1, 'o.cash'=> 0, 'o.create_time'=>['<' , $zero]])//, 'o.schedule'=> ['in', [6,7]]
			->group('o.store_id')
			->select();
		$result = $ids = $excess = [];
		foreach ($data as $v){
			if(1 > $v->total) continue;
			$result[] = [
				'cash_num' => uniqid(),
				'username' => $v->nickname,
				'openid' => $v->openid,
				'userid' => $v->id,
				'costs' => $v->total,
				'amount' => $v->total,
				'transfer'=> 0,
//				'amount' => 5000 < $v->total ? 5000 : $v->total,
//				'transfer'=>5000 < $v->total ? $v->total- 5000 : 0,
				'store_id' => $v->store_id,
				'order_ids' => $v->ids,
				'status' => 1,
				'origin' => 2,
				'type' => 1,
				'notes' => '',
			];
			$ids[] = $v->ids;
		}
		$res = $this->saveAll($result);
		if($res && $ids) {
			$ids = implode(',', $ids);
			$res = $order->where('id', 'in', $ids)->update(['cash'=>1]);
		}//计算运行时间
		$diff = array_sum(explode(' ',microtime())) - array_sum($start) ;
		if(is_numeric($res))
			printf('%s orders cashed, processed in %s second.', $res, round($diff, 3));
		else echo \think\Request::instance()->server('REQUEST_TIME');
		return $this;
	}

	/**
	 * 用户
	 *
	 * @author d3li 2019/5/30
	 * @return $this
	 * @throws \Exception
	 * 
	 */
	public function au3()
	{
		$start = explode(' ',microtime());
		$userstore = model('app\admin\model\shop\User');
		$zero = strtotime(date('Ymd'));
		$data = $userstore
		    ->where([
		    	'store_bind'=>['gt',0],
		    	'sumwithdraw' =>['gt',0]
		    ])
		    ->where("LENGTH(`openid`) > 0")
			->select();
		$result = $ids = $excess = [];
		foreach ($data as $v){
			$result[] = [
				'cash_num' => uniqid(),
				'username' => $v->nickname,
				'openid' => $v->openid,
				'userid' => $v->id,
				'costs' => $v->sumwithdraw,
				'amount' => $v->sumwithdraw,
				'transfer'=>0,
				'store_id' => $v->store_id,
				'order_ids' =>0,
				'status' => 1,
				'origin' => 1,
				'type' => 1,
				'notes' => '',
			];
			$ids[] = $v->id;
		}
		$res = $this->saveAll($result);
		$userstore->where('id','in',$ids)->update(['sumwithdraw'=>0]);
		//计算运行时间
		$diff = array_sum(explode(' ',microtime())) - array_sum($start) ;
		if(is_numeric($res))
			printf('%s orders cashed, processed in %s second.', $res, round($diff, 3));
		else echo \think\Request::instance()->server('REQUEST_TIME');
		return $this;
	}
	/**
	 * 提现列表
	 * @Author   ksea
	 * @DateTime 2019-05-27T21:29:44+0800
	 * @param    [type]                   $uid     [description]
	 * @param    string                   $limit   [description]
	 * @param    string                   $getTime [description]
	 * @param    string                   $search  [description]
	 * @param    array                    $other   [description]
	 * @return   [type]                            [description]
	 */
    public function getCash($uid='',$limit='',$getTime='',$search='',$other=array()){
            $query='';
            if(strlen($getTime)<2){
              $getTime=time();
            }

            /*****************自定义查询*******************/
            if(!empty($other)&&is_array($other)){
                foreach ($other as $key => $value) {
	                if(strlen($value)<=0){
	                    unset($other[$key]);
	                }
	                else{
	                    $where[$key]=$value;
	                }
                }
            }
            /*****************自定义查询*******************/

            /*****************搜索范围定义******************/
            if($search){
                $san_like=[
                    'detail',
                    'goods_name',
                ];
                $link_scane=implode('|', $san_like);
                $where[$link_scane]=array('like',"%{$search}%");
            }
            /*****************搜索范围定义*******************/
            $where['userid']=$uid;
            $base=$this;
            if(strlen($limit)<=0){
                 $s_data = $base->where($where)->where($query)->order('id desc')->select();
                 $res['url']='';
                 $res['msg']='全部数据';
                 $res['count']=count($s_data);
                 $res['is_ture']=1;
                 $res['create_time']=$getTime;
                 $res['data']=$s_data;
            }
            else{

                $limit_a=explode(',', $limit);
                if(count($limit_a)==2){
                   $need_count=$limit_a[1];
                }
                else{
                    $need_count=$limit_a[0];
                }
                $s_data = $base->where($where)->where($query)->limit($limit)->order('id desc')->select();
                if($need_count==count($s_data)){
                    $res['url']='';
                    $res['msg']='数据合法';
                    $res['count']=count($s_data);
                    $res['is_ture']=1;
                    $res['create_time']=$getTime;
                    $res['data']=$s_data;
                }
                else{
                    $res['url']='';
                    $res['msg']='数据缺少';
                    $res['count']=count($s_data);
                    $res['is_ture']=0;
                    $res['create_time']=date('Y-m-d',$getTime);
                    $res['data']=$s_data;
                }
            }

            return $res;
    }
}