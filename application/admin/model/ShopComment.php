<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\model;

use think\Model;
/**
 * ShopComment.php
 *
 * @author d3li <d3li@sina.com>
 * @create：1/28/2019  10:00 AM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0128
 * @describe
 */
class ShopComment extends Model
{
	protected $autoWriteTimestamp = true;
	
	public function user()
	{
		return $this->hasOne('ShopUserinfo', 'id', 'uid')
			->field('id,name,phone,pic');
	}

	public function shop()
	{
		return $this->hasOne('ShopOrder', 'id', 'order_id');
	}

	public function goods()
	{
		return $this->hasOne('app\admin\model\shop\Goods', 'id', 'gid');
	}

	/**
	 * 点赞表
	 * @Author   ksea
	 * @DateTime 2019-02-14T17:43:46+0800
	 * @return   [type]                   [description]
	 */
	public function like(){
		return $this->hasOne('ShopCommentLike','commentid','id');
	}
}