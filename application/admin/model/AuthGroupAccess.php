<?php

namespace app\admin\model;

use think\Model;

class AuthGroupAccess extends Model
{
    
    /**
     * 获取三表关联数据
     * @Author   ksea
     * @DateTime 2018-12-18T18:02:48+0800
     * @return   [type]                   [description]
     */
    public function aga($admin_id=''){
    	$dis=$this->with(['admin','authgroup'])
    		->where('uid',$admin_id?$admin_id:session('admin.id'))
        	->find()
        	->toarray();
   	    return $dis;
    }
    /**
     * 关联模型{admin}
     * @Author   ksea
     * @DateTime 2018-12-18T17:49:38+0800
     * @return   [type]                   [description]
     */
    public function admin(){
       return $this->hasOne('admin','id','uid')->field('*');
    }
    /**
     * 关联模型{admin}
     * @Author   ksea
     * @DateTime 2018-12-18T17:49:38+0800
     * @return   [type]                   [description]
     */
    public function authgroup(){
       return $this->hasOne('auth_group','id','group_id')->field('*');
    }
}
