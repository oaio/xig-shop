<?php

return [
    'NO_AUTH'             => '没有该接口权限',
    'AMOUNT_LIMIT'       => '打款金额应为 1~5000元',
    'NOTENOUGH'       => '您的付款帐号余额不足',
    'SYSTEMERROR'       => '系统繁忙，请稍后再试',
    'FREQ_LIMIT'       => '超过频率限制，请稍后再试。',
    'MONEY_LIMIT'       => '已经达到今日付款总额上限',
    'SENDNUM_LIMIT'       => '该用户今日付款次数超过限制',
    'NAME_MISMATCH'       => '姓名校验出错',
    'SEND_FAILED'       => '付款错误，请查单确认付款结果',
    'V2_ACCOUNT_SIMPLE_BAN'       => '无法给非实名用户付款',
    'RECV_ACCOUNT_NOT_ALLOWED'       => '收款账户不在收款账户列表',
];
