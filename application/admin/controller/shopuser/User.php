<?php
namespace app\admin\controller\shopuser;

use app\common\controller\Backend;
use think\Db;


class User extends Backend{
 	private $use_model;
 	public function _initialize(){
        parent::_initialize();
        $this->model= $this->use_model=model('admin/shopuser/User');
    }


	/**
	 * 显示某人基本信息，或者全部人的全部基本信息
	 * @Author   baohb
	 * @DateTime 2018-11-19T09:55:29+0800
	 * @return   [type]                   [description]
	 */
	public function get_info(){
		$data[0]=input('param.search','');
		$data[1]=input('param.paginate',20);
		$data[2]=input('param.other/a');
		$data[3]=input('param.orderby','desc');
		$data[4]=true;
        if (power_judge()!="*"){
            $data[5]=power_judge();
        }
        $res=$this->use_model->get_user($data);
		foreach ($res[0] as &$value) {
		    //mp(ana_region($value['region']),'array');
            if(!empty($value['region'])){
                $value['region']=ana_region($value['region'],'str');
            }
		}

		$area=Db::name('region')->where('parent_id=9683')->field('id,region_name')->select();

		$this->assign('area',$area);
	    $this->assign('list',$res[0]);
	    $this->assign('page',$res[1]);
	    return $this->fetch();
	}

	public function add()
	{
		/*$category = model('shopCategory')->parent();
		unset($category[0]);*/
		$this->view->assign('works', build_select('row[worker_type]', [1=>'维修小哥',2=>'保洁阿姨']));
		return parent::add();
	}

	/**
	 * 查看和编辑
	 * 规定单一数值传
	 * $id
	 * @Author   baohb
	 * @DateTime 2018-11-20T15:26:00+0800
	 * @return   [type]                   [description]
	 */
	public function get_user_detail(){
		$data[0]=input('param.search','');
		$data[1]=input('param.paginate',20);
		$data[2]=input('param.other/a');
		$data[3]=input('param.orderby','desc');
		$data[4]=false;

        if(input('id')){
            $data[2]=[
                'id'=>input('param.id')
            ];
        }
		$res=$this->use_model->get_user($data);
        if(empty($res)){
        	$this->error('找不到这个用户','shopuser.user/get_info');
        }

      	$data=$res[0]->getData();
//	    $data['region']=ana_region($data['region']);
	    $this->assign('data',$data);
		$this->view->assign('works', build_select('row[worker_type]', [1=>'维修小哥',2=>'保洁阿姨'],$data['worker_type']));
	    return $this->fetch();
	}
	/**
	 * 
	 * @Author   baohb
	 * @DateTime 2018-11-21T18:49:37+0800
	 * @return   [type]                   [description]
	 */
	public function up_user(){

		$data=input('param.');
		$result=$this->use_model->up_user($data['row']);
		if($result['code']==1){
            $this->success('修改成功','shopuser.user/get_info',$result['message']);
        }
        $this->error('修改失败','shopuser.user/get_info',$result['message']);
	}

}