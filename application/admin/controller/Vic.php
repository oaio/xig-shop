<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/15 0015
 * Time: 18:20
 */

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Request;
use think\Session;
use think\Validate;
use \wechat\Api;

class Vic
{
    //批量同步脚本
    public function index()
    {
        ignore_user_abort (true);
        $users = Db::name('admin')->where("nickname",'like', '%开锁%')->select();
        $goods = Db::name('shop_goods') ->where(['admin_id' => 2 , 'cate' => 7])->select();
        foreach($users as $user) {
            foreach ($goods as $key => $good) {
                 $goods_data = [
                     'admin_id'=>$user['id'],
                     'sngoods' => $good['sngoods'],
                     'name' => $good['name'],
                     'price' =>$good['price'],
                     'vipprice'=>$good['vipprice'],
                     'commonprice'=>$good['commonprice'],
                     'unit'=>$good['unit'],
                     'pic'=>$good['pic'],
                     'inoc'=>$good['inoc'],
                     'status'=>$good['status'],
                     'valid_times'=>$good['valid_times'],
                     'deal_times'=>$good['deal_times'],
                     'times'=>$good['times'],
                     'sku'=>$good['sku'],
                     'cate'=>$good['cate'],
                     'brief'=>$good['brief'],
                     'is_type'=>$good['is_type'],
                     'discount_id'=>$good['discount_id'],
                     'power'=>$good['power'],
                     'is_del'=>$good['is_del'],
                     'bak'=>$good['bak'],
                     'update_time'=>$good['update_time'],
                     'create_time'=>$good['create_time'],
                     'group_number'=>$good['group_number'],
                     'grabprice'=>$good['grabprice'],
                     'is_grob'=>$good['is_grob'],
                     'costprice'=>$good['costprice'],
                     'libid'=>$good['libid'],
                     'lib_type'=>$good['lib_type'],
                     'virtual_times'=>$good['virtual_times']
                 ];
                $shop_goods_detail = Db::name('shop_goods')->where('goodsid',$good['id']);

                $goodsid = Db::name('shop_goods')->insertGetId($goods_data);
                $goods_detail = ['goodsid'=>$goodsid,'detail'=>$shop_goods_detail['detail'],'is_use'=>$shop_goods_detail['is_use'],'admin_id'=>$user['id']];
                Db::name('shop_goods_detail')->insert($goods_detail);
            }
        }
    }
}