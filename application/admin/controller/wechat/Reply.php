<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\wechat;

use app\common\controller\Backend;
use think\Config;

/**
 * Reply.php
 *
 * @author sean <d3li@sina.com>
 * @create：12/5/2018  10:35 AM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1207
 * @describe
 */
class Reply extends  Backend
{
	public function index()
	{
		if($this->request->isAjax()){
			$params = $this->request->post();
			if($params){
				foreach ($params as $k => &$v) {
					$v = is_array($v) ? implode(',', $v) : $v;
				}
				$result = config($params);
				if ($result !== false) {
					$this->refreshFile($params);
					$this->success();
				} else {
					$this->error($this->model->getError());
				}
			}
			return json(['success']);
		}
		$conf = Config::get('reply');

		$this->assign('conf', $conf ? $conf :['default_msg'=>'','reply'=>'{}']);
		return $this->fetch();
	}

	/**
	 * 刷新配置文件
	 * @param $config
	 * @param string $name
	 */
	protected function refreshFile($config, $name = 'reply')
	{
		file_put_contents(APP_PATH . 'extra' . DS . $name . '.php',
			'<?php' . "\n\nreturn " . var_export($config, true) . ";");
	}
}