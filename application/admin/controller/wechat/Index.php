<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\wechat;

use app\common\controller\Backend;
use think\Config;
use wechat\Api;
/**
 * Index.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/5/2018  9:12 AM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1207
 * @describe
 */
class Index extends Backend
{

	public function index()
	{
		if($this->request->isAjax()) {
			$params = $this->request->post();
			if ($params) {
				$this->refreshFile($params, 'wechat');
			}
			$this->success();
			return json(['success']);
		}
		$conf = Config::get('wechat');
		$this->assign('conf', $conf);
		return $this->fetch();
	}

	public function template()
	{
		$api = Api::getInstance();
		$res = $api->get_template_list();
		$list = array();
		if(isset($res['template_list'])) {
			$template_list = $res['template_list'];
			$list['total'] = count($template_list);
			foreach ($template_list as &$value) {
				$value['id'] = $value['template_id'];
			}
			$list['rows'] = $template_list;
		}
		return json($list);
	}

	public function del($ids = 0)
	{
		if($id = input('ids')) {
			$api = Api::getInstance();
			$api->del_template($id);
			$this->success();
		}
		return '';
	}

	public function detail()
	{
		if ($this->request->isPost()){
			$params = $this->request->post();
			$api = Api::getInstance();
			$res = $api->send_template(
				$params['user'],
				input('ids'),
				json_decode($params['data'],!0)
			);
			if(empty($res)){
				$this->error('服务器忙不过来了');
			}else{
				if(isset($res['errcode']) && $res['errcode']==0){
					$this->success('发送成功');
					return '';
				}
				$this->error($res['errmsg']);
			}
		}
		return $this->fetch();
	}

	/**
	 * 群发消息
	 *
	 * @return mixed|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function push($ids = 0)
	{
		$this->request->filter(['strip_tags']);
		if($this->request->isPost()){
			$params = $this->request->post('row/a');
			$params = array_merge(['ids' => 0, 'title' => '',
				'remark' => '', 'link' => ''], $params);
			$list = model('ShopUserinfo')
				->where('id', 'in', $params['ids'])
				->select();
			// fixme nothing
			if($list) {
				$api = Api::getInstance();
				foreach ($list as $v) {
					$api->send_template($v['openid'], $ids, [
						'first' => $params['title'],
						'remark' => $params['remark']
					], $params['link']);
				}
				$this->success('公众号消息推送成功');
			}
			$this->error();
		}
		$model = model('store');
		if ($this->request->isAjax()) {
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model->alias('a')
				->join('store m', 'm.store_id=a.store_id')
				->join('shop_userinfo bu', 'bu.store_bind=a.store_id')
				->where($where)
				->count();

			$list = $model->alias('a')->with(['m', 'bu'])->field('a.*,bu.id')
				->join('store m', 'm.store_id=a.store_id')
				->join('shop_userinfo bu', 'bu.store_bind=a.store_id')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->fetch();
	}

	/**
	 * 刷新配置文件
	 *
	 * @param $config
	 * @param string $name
	 */
	protected function refreshFile($config, $name = 'reply')
	{
		file_put_contents(APP_PATH . 'extra' . DS . $name . '.php',
			'<?php' . "\n\nreturn " . var_export($config, true) . ";");
	}
}