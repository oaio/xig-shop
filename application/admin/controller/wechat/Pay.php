<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\wechat;

use app\common\controller\Backend;
use think\Config;

/**
 * Pay.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/5/2018  2:31 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1207
 * @describe
 */
class Pay extends Backend
{
	protected $model = null;
	protected $searchFields = 'id,oid,tradeno';

	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('WechatPay');
	}

	public function detail($ids)
	{
		$log = $this->model->get(['id' => $ids]);
		$this->assign('log',$log->toArray());
		return $this->fetch();
	}

	public function config()
	{
		if($this->request->isPost()){
			$params = $this->request->post();
			$this->refreshFile($params['config']);
			$this->success();
		}else if(IS_AJAX) {
			$config = Config::get('pay');
			return json($config);
		}
	}

	/**
	 * 刷新配置文件
	 *
	 * @param $config
	 * @param string $name
	 */
	protected function refreshFile($config, $name = 'pay')
	{
		file_put_contents(APP_PATH . 'extra' . DS . $name . '.php',
			'<?php' . "\n\nreturn " . var_export($config, true) . ";");
	}
}