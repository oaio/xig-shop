<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\auth;

use app\common\controller\Backend;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * Storelog.php 店铺操作日志
 *
 * @author d3li <d3li@sina.com>
 * @create：1/14/2019  2:06 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0114
 * @describe
 */
class Storelog extends Backend
{
	/**
	 * @var \app\admin\model\AdminLog
	 */
	protected $model = null;
	protected $childrenGroupIds = [];
	protected $childrenAdminIds = [];

	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('opHistory');

		$controllername = strtolower($this->request->controller());
		$actionname = strtolower($this->request->action());

		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		// 判断控制器和方法判断是否有对应权限
		if (!$this->auth->check($path)) {
			$this->redirect('shop.error/error_url');
		}
	}

	/**
	 * 查看
	 *
	 * @throws \think\Exception
	 */
	public function index()
	{
		if ($this->request->isAjax())
		{
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->where($where)
//				->where('admin_id', 'in', $this->childrenAdminIds)
//				->order($sort, $order)
				->count();

			$list = $this->model
				->with('admin,store')
				->where($where)
//				->where('admin_id', 'in', $this->childrenAdminIds)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	/**
	 * 详情
	 *
	 * @param $ids
	 * @return string
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function detail($ids)
	{
		$row = $this->model->with('store')->where(['id' => $ids])->find();
		if (!$row) {
			$this->error(__('No Results were found'));
		}
		$admin= model('admin')->where('id', $row->admin_id)->find();
		$row->data('admin', $admin);
		$this->view->assign("row", $row->toArray());
		return $this->view->fetch();
	}

	/**
	 * 添加
	 * @internal
	 */
	public function add()
	{
		$this->error();
	}

	/**
	 * 编辑
	 * @internal
	 */
	public function edit($ids = NULL)
	{
		$this->error();
	}

	/**
	 * 删除
	 */
	public function del($ids = "")
	{
		$this->error();
		return '';
		if ($ids)
		{
			$childrenGroupIds = $this->childrenGroupIds;
			try {
				$adminList = $this->model->where('id', 'in', $ids)->where('admin_id', 'in', function ($query) use ($childrenGroupIds) {
					$query->name('auth_group_access')->field('uid');
				})->select();
			} catch (DataNotFoundException $e) {
			} catch (ModelNotFoundException $e) {
			} catch (DbException $e) {
			}
			if ($adminList)
			{
				$deleteIds = [];
				foreach ($adminList as $k => $v)
				{
					$deleteIds[] = $v->id;
				}
				if ($deleteIds)
				{
					$this->model->destroy($deleteIds);
					$this->success();
				}
			}
		}
		$this->error();
	}

	/**
	 * 批量更新
	 * @internal
	 */
	public function multi($ids = "")
	{
		// 管理员禁止批量操作
		$this->error();
	}

}