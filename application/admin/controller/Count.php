<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller;

use app\common\controller\Backend;
/**
 * Count.php 订单统计
 *
 * @author d3li <d3li@sina.com>
 * @create：18/03/2019  2:28 PM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.18
 * @describe
 */
class Count extends Backend
{
	public function _initialize()
	{
		parent::_initialize(); // TODO: Change the autogenerated stub
		$this->model = model('WechatPay');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$type = $this->request->param('of', 'month');
			$ary = array(
				'month' => "COUNT(id) as id,sum(total_fee) as money,
				DATE_FORMAT(FROM_UNIXTIME(create_time) ,'%Y-%m 月') as date",
				'day'=>"COUNT(id) as id,sum(total_fee) as money,
				DATE_FORMAT(FROM_UNIXTIME(create_time) ,'%Y-%m-%d') as date",
				'week'=>"COUNT(id) as id,sum(total_fee) as money,
				DATE_FORMAT(FROM_UNIXTIME(create_time) ,'%x-%v') as date",
				'year'=>"COUNT(id) as id,sum(total_fee) as money,
				DATE_FORMAT(FROM_UNIXTIME(create_time) ,'%Y') as date"
			);
			$total = $this->model
				->where($where)
				->field($ary[$type])
				->group('date')
				->count();

			/** @var array $list */
			$list = $this->model
				->where($where)
				->field($ary[$type])
				->order($sort, $order)
				->group('date')
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			if('week' == $type){
				foreach ($list as $k=>$v){
					$a = explode('-', $v['date']);
					$year = strtotime($a['0'].'-01-01');
					$pos = date('N',$year);
					$start = date('Y-m-d',
						strtotime('+'.($a['1']-1).' week -'.($pos-1).'day', $year ));
					$end = date('Y-m-d',
						strtotime('+'.($a['1']).' week -'.($pos-1).'day', $year ));
					$list[$k]['date'] = implode('',[$v['date'], '周(',$start, '~',$end, ')']);
				}
			}
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	/**
	 * 详情
	 *
	 * @param string $ids
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail($ids = '')
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			if(is_numeric($ids)){//年
				$this->model->whereTime('create_time',
					'between', [$ids.'-01-01', ($ids+1).'-01-01']);
			}elseif($ids && strpos($ids, '周')){
				preg_match_all('/\d{4}-\d+-\d+/', $ids, $ary);
				$ary && $this->model
					->whereTime('create_time', 'between', [$ary[0][0], $ary[0][1]]);
			}elseif($ids && strpos($ids, '月')){
				$start = strtotime(substr($ids, 0, 7) .'-01');
				$this->model->where('create_time',
					'between', [$start, strtotime("+1 month", $start)]);
			}else{
				$start = strtotime($ids);
				$this->model->where('create_time',
					'between', [$start, strtotime("+1 day", $start)]);
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$option = $this->model->getOptions('where')['AND'];
			$list = $this->model
				->with('pay,info,orders')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$total = $this->model
				->where($where)
				->where($option)
				->count();
			/** @var array $list */
			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch('',[ 'ids'=>$ids]);
	}
}