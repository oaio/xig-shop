<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller;

use app\common\controller\Backend;
/**
 * Group.php  拼团
 *
 * @author d3li <d3li@sina.com>
 * @create：3/3/2019  10:11 AM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.03
 * @describe
 */
class Group extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\GroupBuying $model
	 */
	protected $model;

	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('GroupBuying');
	}

	public function user()
	{
		if ($this->request->isAjax()) {
			$model = model('GroupBuyingDetails');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model
				->where($where)
				->count();
			$list = $model
				->with('user')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			return json(['total' => $total, 'rows' => $list]);
		}
		return $this->view->fetch();
	}
}