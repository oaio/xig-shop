<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\controller\shop;
/**
 * Other.php 其它平台订单录入
 *
 * @icon fa fa-paint-brush
 * @author d3li <d3li@sina.com>
 * @create：15/05/2019  5:25 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.15
 * @describe
 */
class Other extends Close
{
	protected $option = ['orgin'=>['>', 1]];

	public function add()
	{
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
					$params[$this->dataLimitField] = $this->auth->id;
				}
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
						$this->model->validate($validate);
					}
					$result = $this->model->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($this->model->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		return $this->view->fetch();
	}

	public function edit($ids = 0)
	{
		$row = $this->model->with('goods')->find($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$result = $row->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($row->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign('row', $row);
		return $this->view->fetch();
	}

	public function plat()
	{
		$conf = model('config')->get(['name'=>'source']);
		$data = [];
		foreach (json_decode($conf->value) as  $k=>$v)
		{
			if(!$k)continue;
			$data[] = ['name'=>$v, 'value'=>$k];
		}
		$this->success('','',$data);
	}
	/**
	 * 区域
	 * @Author   ksea
	 * @DateTime 2019-06-10T14:46:57+0800
	 * @return   [type]                   [description]
	 */
	public function area(){
		$res=model('admin/Region')->where('region_parent_id=440300')->select();
		$ret=[];
		foreach ($res as $key => $value) {
			$ret[$key]['name']  = $value->region_name;
			$ret[$key]['value'] = $value->region_id;
		}
		$this->success('','',$ret);
	}
}