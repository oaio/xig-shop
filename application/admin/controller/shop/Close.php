<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Partner;
use think\Session;
/**
 * Close.php 客服后台-Close（完结）
 *
 * @icon fa fa-window-close
 * @author d3li <d3li@sina.com>
 * @create：15/05/2019  4:50 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.15
 * @describe
 */
class Close extends Partner
{
	protected $option;
	protected $dataLimitField = 'o.city_code';
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/Order');
	}

	public function index($ids = 0)
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			$this->model->where('schedule', 'in', [4,7]);
			if($ids){
				$this->model->where('o.id', $ids);
			}
			empty($this->option) &&
				$this->option = $this->model->getOptions('where')['AND'];
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			//城市合伙人
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';

			$total = $this->model
			        ->with(['serverMany'])
					->alias('o')
					->field('o.*,r.region_name,e.name')
					->join('fa_shop_service_order service','service.orderid=o.id', 'RIGHT')
					->join('fa_admin a', 'a.id=service.admin_id', 'LEFT')
					->join('fa_shop_goods g', 'g.id=o.goodsid', 'LEFT')
					->join('fa_region r','r.region_id=service.areaid', 'LEFT')
					->join('fa_employee e', 'e.id=service.sfuserid', 'LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
					->where($this->option)
					->where($where)
					->order($sort, $order)
					->count();

			$list = $this->model
					->alias('o')->with(['serverMany','service','expand','user'])
					->field('o.*,service.mark,r.region_name,a.nickname as admin,e.name,g.cate,x.coupons')
					->join('fa_shop_service_order service','service.orderid=o.id', 'RIGHT')
					->join('fa_admin a', 'a.id=service.admin_id', 'LEFT')
					->join('fa_shop_goods g', 'g.id=o.goodsid', 'LEFT')
					->join('fa_region r','r.region_id=service.areaid', 'LEFT')
					->join('fa_employee e', 'e.id=service.sfuserid', 'LEFT')
					->join('fa_extr_order x','x.id = o.extr','LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
					->where($this->option)
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);
			return json($result);
		}
		$res = model('ShopCategory')->all();
		$data = [];
		foreach($res as $v){
			$data[$v->id] = $v->name;
		}
		return $this->view->fetch('',
			['ids' => $ids, 'cate'=>json_encode($data)]);
	}

	public function detail($ids = 0)
	{
		$source = model('config')->get(['name'=>'source']);
		$list = model('admin/shop/Serverorder')
			->with(['order.paylog','code','card','emp', 'temp'])
			->alias('server')
			->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
			->join('fa_shop_order order','server.orderid = order.id','LEFT')
			->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
			->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
			->join('fa_region region','region.region_id = server.areaid','LEFT')
			->join('fa_employee e', 'e.id=server.sfuserid', 'LEFT')
			->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,
			goods_msg,discount.bean,discount.dismoney,order.goods_name,e.name as sf,
			order.shouldpay,order.actualpay,order.mobile,region.region_name,
			region.region_id,order.snorder,order.nickname,order.num,order.type')
			->where('orderid', $ids)
			->find();
		$this->assign('date', model('admin/shop/OrderTemp')::get(['orderid'=> $list['orderid']]));
		$this->assign('pay', model('admin/shop/UserWantPay')::all(['is_pay'=>1,'orderid'=>$ids]));
		return $this->view->fetch('', ['list'=>$list, 'source'=>json_decode($source->value)]);
	}

	protected function buildparams($searchfields = null, $relationSearch = null)
	{
		$searchfields = is_null($searchfields) ? $this->searchFields : $searchfields;
		$search = $this->request->get("search", '');
		$filter = $this->request->get("filter", '');
		$op = $this->request->get("op", '', 'trim');
		$sort = $this->request->get("sort", "id");
		$order = $this->request->get("order", "DESC");
		$offset = $this->request->get("offset", 0);
		$limit = $this->request->get("limit", 0);
		$filter = (array)json_decode($filter, TRUE);
		$op = (array)json_decode($op, TRUE);
		$filter = $filter ? $filter : [];
		$where = [];
		$tableName = '';
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			$where[] = [$tableName . $this->dataLimitField, 'in', $adminIds];
		}
		if ($search) {
			$searcharr = is_array($searchfields) ? $searchfields : explode(',', $searchfields);
			foreach ($searcharr as $k => &$v) {
				$v = stripos($v, ".") === false ? $tableName . $v : $v;
			}
			unset($v);
			$where[] = [implode("|", $searcharr), "LIKE", "%{$search}%"];
		}
		foreach ($filter as $k => $v) {
			$sym = isset($op[$k]) ? $op[$k] : '=';
			if (stripos($k, ".") === false) {
				$k = $tableName . $k;
			}
			$v = !is_array($v) ? trim($v) : $v;
			$sym = strtoupper(isset($op[$k]) ? $op[$k] : $sym);
			switch ($sym) {
				case '=':
				case '!=':
					$where[] = [$k, $sym, (string)$v];
					break;
				case 'LIKE':
				case 'NOT LIKE':
				case 'LIKE %...%':
				case 'NOT LIKE %...%':
					$where[] = [$k, trim(str_replace('%...%', '', $sym)), "%{$v}%"];
					break;
				case 'FIND':
				case 'FIND_IN_SET':
					$where[] = "FIND_IN_SET('{$v}', " . ($relationSearch ? $k : '`' . str_replace('.', '`.`', $k) . '`') . ")";
					break;
				case 'RANGE':
				case 'NOT RANGE':
					$v = str_replace(' - ', ',', $v);
					$arr = array_slice(explode(',', $v), 0, 2);
					$arr = array_map(function ($v){return strtotime($v);}, $arr);
					if (stripos($v, ',') === false || !array_filter($arr))
						break;
					//当出现一边为空时改变操作符
					if ($arr[0] === '') {
						$sym = $sym == 'RANGE' ? '<=' : '>';
						$arr = $arr[1];
					} else if ($arr[1] === '') {
						$sym = $sym == 'RANGE' ? '>=' : '<';
						$arr = $arr[0];
					}
					$where[] = [$k, str_replace('RANGE', 'BETWEEN', $sym), $arr];
					break;
				default:
					break;
			}
		}
		$where = function ($query) use ($where) {
			foreach ($where as $k => $v) {
				if (is_array($v)) {
					call_user_func_array([$query, 'where'], $v);
				} else {
					$query->where($v);
				}
			}
		};
		return [$where, $sort, $order, $offset, $limit];
	}
}