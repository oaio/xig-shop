<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;
use think\Request;
use think\Session;
use think\Validate;
use \wechat\Api;
use think\Log;

/**
 * 不用ajax
 */

class Goods extends Backend{
    protected $autoWriteTimestamp = true;
   /**
    * 自动验证
    * 添加
    * @Author   baohb
    * @DateTime 2018-11-26T09:32:52+0800
    */
   public function 	add_goods(){
       
     $date['create_time']= strtotime(date('Y-m-d h:i:s', time()));
     $date['update_time']= strtotime(date('Y-m-d h:i:s', time()));
     $date['admin_id'] =power_judge();
   	 if(request()->isPost()){

         $goods_data=[
             'name'=>input('name',''),
             'commonprice'  => input('commonprice',''),
             'vipprice'	=> input('vipprice',''),
             'brief'	=> input('brief',''),
             'times'	=> input('times',''),
             'power'	=> input('power',''),
             'cate' => input('cate',''),
             'is_grob'  => input('is_grob','1'),
             'costprice'	=> input('costprice','0'),
             'snserver'      =>    'JB'.create_no(),
             'city_code'      =>    input('city_code','1'),
         ];
         $rule_data=[
             'commonprice'=>'number|max:10',
             'vipprice'=>'number|max:10',
             'times'=>'number|between:1,100',
             'power'=>'number|between:1,100000',
         ];
         $msg_data=[
             'commonprice' => '非法价格',
             'vipprice' => '非法价格',
             'times' => '非法预约次数',
             'power' => '非法权重',
         ];

         $validate_server=validate()->make($rule_data,$msg_data);
         if(!$validate_server->check($goods_data)){
             $m_goods=$validate_server->getError();
             $error['msg']=$m_goods;
             return  $this->error(join($error),'shop.goods/show_list');
             //$this->error($m_server);
             exit();
         }

            $post_data=input('post.');
            $post_data=array_merge($post_data,$date);
    		$res=model('admin/shop/Goods')->add_goods($post_data);
    		if($res['code']=1){
                $module_name='商品管理';//模块(菜单)名称(必填)
                $op_name='商品添加';//操作内容(必填)
                $remark=json_encode($res);//操作备注(选填)
                $param=[
                    'module_name'=>$module_name,
                    'op_name'=>$op_name,
                    'remark'=>$remark,
                ];
                \think\Hook::listen('activelog',$param);
    			$this->success('添加产品成功','shop.goods/show_list');

    			exit();
    		}
    		else{
    			$this->error('添加产品失败');
    			exit();
    		}
      }
      //获取地区数据
      $city_list = Db::table('fa_city')->order('id desc')->column('city_name','city_code');
   	  $this->assign('admin_id', session('admin.id'));
   	  $this->assign('city_list', $city_list);
   	  return $this->fetch();
   }
   /**
    * 显示列表
    * @Author   ksea
    * @DateTime 2018-11-16T09:39:31+0800
    * @return   [type]                   [description]
    */
   public function show_list(){
//      $api=new Api();
      $link='|uid|10|id|20';
//      $qrcode=$api->textCreat($link);
      $data[0]=input('param.search','');
      $data[1]=input('param.paginate',10);
      $data[2]=input('param.other/a');
      $data[3]=input('param.orderby','desc');
      $data[4]=true;

      $data[5]=power_judge();
      $res=model('admin/shop/Goods')->show_goods_list($data);
      $resu=Db::name('shop_categrory')->field('id cid,name catename')->select();

      $this->assign('list', $res[0]);
      $this->assign('resu', $resu);
      $this->assign('page', $res[1]);
      $this->assign('city_list', $res[3]);
      if(isset($res[2])){
           $this->assign('search_data', $res[2]);
       }
      if(request()->isAjax()){
         return $this->fetch('shop/goods/ajax_show_list');
      }
      else{
         return $this->fetch();
      }
   }
   /**
    * 获取某个商品详情
    * @Author   ksea
    * @DateTime 2018-11-16T09:48:01+0800
    * @return   [type]                   [description]
    */
   public function goods_detail(){

   	  $id=input('id');
       $admin_id = power_judge();
   	  if($id){
   	  	$res=model('admin/shop/Goods')->get_goods_detail($id,'',$admin_id);

        if (power_judge()==null||$res['admin_id']==power_judge()){
            $cate_data=is_get_val('shop_categrory',$res['cate'],'id,name');
            $this->assign('cate',$cate_data);

            $this->assign("data",$res);
        }else{
            $this->error('数据不存在','shop.error/error_url');
        }
   	  }
   	  else{
   	  	$this->error('请求错误');
   	  }
       //获取地区数据
       $city_list = Db::table('fa_city')->order('id desc')->column('city_name','city_code');
       $this->assign('city_list', $city_list);
       $this->assign('admin_id', session('admin.id'));
      return $this->fetch(); 	  
   }
   /**
    * 更新某个商品
    * @Author   ksea
    * @DateTime 2018-11-16T09:48:28+0800
    * @return   [type]                   [description]
    */
   public function up_goods(){

       $date['update_time']= strtotime(date('Y-m-d h:i:s', time()));
   	 if(request()->isPost()){
         $post_data=input('post.');
         $post_data=array_merge($post_data,$date);
    		$res=model('admin/shop/Goods')->update_goods($post_data);
    		if($res['code']=1){
                $module_name='商品管理';//模块(菜单)名称(必填)
                $op_name='商品修改';//操作内容(必填)
                $remark=json_encode($res);//操作备注(选填)
                $param=[
                    'module_name'=>$module_name,
                    'op_name'=>$op_name,
                    'remark'=>$remark,
                ];
                \think\Hook::listen('activelog',$param);
    			$this->success('更新产品成功');
    			exit();
    		}
    		else{
                $this->error('更新产品失败');
    			exit();
    		}
   	 }
   	 return $this->fetch();
   }

    /**
     * 上架/下架某个商品
     * @Author   baohb
     * @DateTime 2018-12-19T010:30:28+0800
     * @return   [type]                   [description]
     */
    public function upordo()
    {
        if (input('status')==1){
            $data['status'] =0;
        }
        if (input('status')==0){
            $data['status'] =1;
        }
        $data['id']=input('id');

        $res=model('admin/shop/Goods')->upordo($data);
        if($res['code']=1){
            $this->success('产品状态更新成功');
            exit();
        }
        else{
            $this->error('产品状态更新失败');
            exit();
        }
    }
    /**
     * 软删除某个商品
     * @Author   baohb
     * @DateTime 2018-12-19T015:34:28+0800
     * @return   [type]                   [description]
     */
    public function hidden_good()
    {
        if (input('is_del')==0 && input('deal_times')==0){
            $data['is_del'] =1;
            $data['id']=input('id');

            $res=model('admin/shop/Goods')->hidden_good($data);
            if($res['code']=1){
                $module_name='商品管理';//模块(菜单)名称(必填)
                $op_name='商品删除';//操作内容(必填)
                $remark=json_encode($res);//操作备注(选填)
                $param=[
                    'module_name'=>$module_name,
                    'op_name'=>$op_name,
                    'remark'=>$remark,
                ];
                \think\Hook::listen('activelog',$param);
                $this->success('产品删除成功');
                exit();
            }
            else{
                $this->error('产品删除失败');
                exit();
            }
        }else{
            $this->error('该产品已被下单，只能查看');
            exit();
        }
    }

    /**
     * 查看某个商品详情
     * @Author   baohb
     * @DateTime 2018-12-19T16:08:01+0800
     * @return   [type]                   [description]
     */
    public function show_goods_detail(){
        $id=input('id');
        $admin_id = power_judge();
        $store_info = Db::name('store')->field('store_id')->where('admin_id','=',$admin_id)->find();
        $store_id = $store_info['store_id'];
        if($id){
            $res=model('admin/shop/Goods')->get_goods_detail($id,$store_id,$admin_id);

            if (power_judge()==null||$res['admin_id']==power_judge()){
                $pic=explode(',',$res['pic']);
                $this->assign('pic',$pic);
                $this->assign("data",$res);
            }else{
                $this->error('数据不存在','shop.error/error_url');
            }
        }
        else{
            $this->error('请求错误');
        }
        return $this->fetch();
    }

	/**
	 * 商品二维码
	 *
	 * @author d3li 07/05/2019
	 */
	public function qrcode($ids)
	{
		$store = model('admin/shop/goods')
			->alias('g')
			->join('fa_store s', 's.admin_id=g.admin_id')
			->where('g.id', $ids)
			->find();
    if(empty($store))return '平台不允许添加商品';
    $user = model('admin/shopuser/User')
          ->where('store_bind',$store->store_id)
          ->find();
    if(!$user){
      return '店铺未绑定用户';
    }
		$res = Api::getInstance()->qrcode("goodsbase|id|$ids|to|{$user->id}|store_id|{$store->store_id}", $ids, 'qrcode/goods');
		if(is_array($res)) dump($res);
		else echo '<img src="', $res, '">';
		exit;
	}

    /**
     * 复制商品
     */
	public function copy_goods(){
        $id=input('id');
        $code = input('city_code');
        $admin_id = power_judge();
        $store_info = Db::name('store')->field('store_id')->where('admin_id','=',$admin_id)->find();
        $store_id = $store_info['store_id'];
        if($id){
            $res=model('admin/shop/Goods')->get_goods_detail($id,$store_id,$admin_id);

            if (power_judge()==null||$res['admin_id']==power_judge()){

                $data=
                    [
                        'name'=>$res['name'],
                        'commonprice'=>$res['commonprice'],
                        'brief'=>$res['brief'],
                        'times'=>$res['times'],
                        'power' =>$res['power'],
                        'vipprice' =>$res['vipprice'],
                        'costprice' =>$res['costprice'],
                        'unit'=>$res['unit'],
                        'cate'=>$res['status'],
                        'pic'=>$res['pic'],
                        'inoc'=>$res['inoc'],
                        'grabprice'=>$res['grabprice'],
                        'is_grob'=>$res['is_grob'],
                        'virtual_times'=>$res['virtual_times'],
                        'lib_type'=>isset($res['lib_type'])?$res['lib_type']:0,
                        'libid'=>isset($res['libid'])?$res['libid']:'',
                        'bak'=>$res['bak'],
                        'update_time' =>time(),
                        'status'=>0,
                        'create_time' =>time(),
                        'update_time' =>time(),
                        'sngoods'=>'GD'.create_no(),
                        'admin_id' => $res['admin_id'],
                        'detail' => $res['detail'],
                    ];
                $code = explode(',',$code);
                if(is_array($code)){
                    foreach ($code as $city){
                        $data['city_code'] = $city;
//                        Log::record('开始：'.json_encode($data),'复制商品');
                        $res=model('admin/shop/Goods')->add_goods($data);
                        if($res['code']=1){
                            $module_name='商品管理';//模块(菜单)名称(必填)
                            $op_name='商品添加';//操作内容(必填)
                            $remark=json_encode($res);//操作备注(选填)
                            $param=[
                                'module_name'=>$module_name,
                                'op_name'=>$op_name,
                                'remark'=>$remark,
                            ];
                            \think\Hook::listen('activelog',$param);
                        }
                    }
                }
                return $this->success();
            }else{
                $this->error('数据不存在','shop.error/error_url');
            }
        } else{
            $this->error('请求错误');
        }
        return $this->success();
    }
}  