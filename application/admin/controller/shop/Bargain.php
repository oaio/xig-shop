<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;

/**
 * 特惠商品
 * 
 */

class Bargain extends Backend
{

	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/Goods');
	}
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $query='';
            $query.="origin_table = 'shop_goods_bargain'";
            $query.=" and is_del = 0";
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
            	->with(['cateData','cityData','detailData','bargain_data'])
                ->where($where)
                ->where($query)
                ->order($sort, $order)
                ->count();

            $list = $this->model
            	->with(['cateData','cityData','detailData','bargain_data'])
                ->where($where)
                ->where($query)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
	    $res = model('ShopCategory')->all();
	    $data = [];
	    foreach($res as $v){
		    $data[$v->id] = $v->name;
	    }
        return $this->view->fetch('', ['cate'=>$data]);
    }
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params['sngoods']='GD'.create_no();
            $params['origin_table']='shop_goods_bargain';
            $params['admin_id']=2;
            $params['inoc']=substr($params['inoc'],1);
            $params['pic']=substr($params['pic'],1);
            if ($params) {
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    //详情
                    $detail=[
                        'goodsid' =>$this->model->id,
                        'detail'  =>$params['detail'],
                        'is_use'  =>1,
                        'admin_id'=>2,
                    ];
                    model('admin/shop/Detail')->save($detail);
                    
                    //免单卷
                    $bargain=[
                        'freecount' =>$params['freecount'],
                    ];
                    $bargainModel=model('admin/shop/Bargain');
                    $bargainModel->save($bargain);
                    //数据更新
                    $bid=$bargainModel->id;
                    $cgoodsid=$this->model->id;

                    $r1=$this->model->allowField(true)->save(['origin_id'=>$bid],['id'=>$cgoodsid]);

                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $cate=model('admin/shop/categrory')->where(['pid'=>0,'status'=>1])->order('power desc')->column('name','id');
        $this->assign('cate',$cate);
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model->where('id',$ids)->with(['cateData','cityData','detailData','bargainData'])->find();

        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params['inoc']=substr($params['inoc'],1);
            $params['pic']=substr($params['pic'],1);
            if ($params) {
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validate($validate);
                    }

                    //详情
                    $detail=[
                        'detail'  =>$params['detail'],
                    ];

                    model('admin/shop/Detail')->save($detail,['goodsid'=>$ids]);
                    
                    //免单卷
                    $bargain=[
                        'freecount' =>$params['freecount'],
                    ];

                    $bargainModel=model('admin/shop/Bargain');

                    $bargainModel->save($bargain,['id'=>$row['origin_id']]);

                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($row->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $cate=model('admin/shop/categrory')->where(['pid'=>0,'status'=>1])->order('power desc')->column('name','id');
        $this->assign('cate',$cate);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $count = $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();
            $count = 0;
            foreach ($list as $k => $v) {
                $count += $v->save([
                    'is_del'=>1],[
                    'id'    =>$v['id']
                    ]);
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 分类
     * @Author   ksea
     * @DateTime 2019-07-11T20:17:17+0800
     * @return   [type]                   [description]
     */
    public function cate(){
        $cate=model('admin/shop/Categrory')->where('status=1')->column('name','id');
        $city=model('admin/shop/City')->column('city_name','city_code');
        return json([$cate,$city]);
    }

}