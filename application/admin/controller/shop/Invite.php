<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;
use app\common\controller\Backend;

/**
 * Invite.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/7/11  10:42
 * @see      https://gitee.com/d3li/
 * @version 2.07.11
 * @describe
 */
class Invite extends Backend
{
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/Invite');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->where($where)
				->count();

			$list = $this->model->with('c,m')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			/** @var array $list */
			$list = collection($list)->toArray();
			$result = array('total' => $total, 'rows' => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	public function config()
	{
		$model = model('config')::get(['name' => 'invite']);
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				// if($params['total'] < $params['single'])
				// 	$this->error('全站总发放量不能小于单人最大限额');
				if($params['single'] < $params['day'])
					$this->error('单人最大限额不能小于每天限额');
				$str = json_encode(array_values($params));
				try {
					$result = $model->allowField(true)->save(['value'=>$str]);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($model->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
		}
		$row = [];
		$ary = json_decode($model['value'], 1);
		foreach (['total','single','day','rule','tip'] as $k=>$v)$row[$v] = $ary[$k];
		return $this->view->fetch('', ['row'=> $row]);
	}
}