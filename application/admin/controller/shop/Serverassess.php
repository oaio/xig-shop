<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
use think\Db;

class Serverassess extends Backend
{
	protected $searchFields = ['detail','user.phone','user.name'];
	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/ShopServerAssess');
	}
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $reqjson=$this->request->request('filter');
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
            	->with(['user','emp'])
            	->alias('assess')
            	->join('fa_shop_userinfo user','assess.uid=user.id','LEFT')
            	->join('fa_employee emp','assess.sfuserid=emp.id','LEFT')
                ->where($where)
                ->field('assess.* ,assess.status as astatus')
                ->order($sort, $order)
                ->count();

            $list = $this->model
            	->with(['user','emp'])
            	->alias('assess')
            	->join('fa_shop_userinfo user','assess.uid=user.id','LEFT')
            	->join('fa_employee emp','assess.sfuserid=emp.id','LEFT')
                ->where($where)
                ->field('assess.*,assess.status as astatus')
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 评分
     * @Author   ksea
     * @DateTime 2019-03-28T14:23:39+0800
     * @param    string                   $ids [description]
     * @return   [type]                        [description]
     */
    public function score($ids=''){
    	$this->request->filter(['strip_tags']);
    	$data=$this->model->where(['id'=>$ids])->find();
    	return $this->view->fetch('',['assess'=>$data]);
    }
    /**
     * 详细
     * @Author   ksea
     * @DateTime 2019-03-28T14:23:39+0800
     * @param    string                   $ids [description]
     * @return   [type]                        [description]
     */
    public function mydetail($ids=''){
    	$this->request->filter(['strip_tags']);
    	$data=$this->model->where(['id'=>$ids])->find();
    	return $this->view->fetch('',['assess'=>$data]);
    }

    /**
     * 审核
     *
     * @param int $ids
     * @return mixed
     */
    public function check($ids = 0)
    {
        return $this->edit($ids);
    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model->with(['Emp'])->where('id',$ids)->find();
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if($params['status']==1){

                /**************短信通知这些二货*****************/
                $pht=new \aliyun\SendTemp();
                foreach ($row['emp'] as $key => $value) {
                    $sfphone   = $value['phone'];

                    $sfmsgGood =  '好';
                    $sfmsgMid  =  '中';
                    $sfmsgBad  =  '差';

                    $send_msg='';

                    if($row['avg_assess']=='5'){
                        $send_msg=$sfmsgGood;
                    }
                    elseif ('3'<=$row['avg_assess']&&$row['avg_assess']<'5') {
                        $send_msg=$sfmsgMid;
                    }
                     else {
                        $send_msg=$sfmsgBad;
                    }
                    $pht::assess($sfphone,$send_msg);
                }

                /**************短信通知这些二货*****************/
            }
            if ($params) {
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($row->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}