<?php

/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\controller\shop;
use app\common\controller\Backend;

/**
 * SelfPay.php 自定义支付
 *
 * @author d3li <d3li@sina.com>
 * @create：12/06/2019  9:31 AM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.17
 * @describe
 */
class SelfPay extends Backend
{
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/ShopPaySelf');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->alias('s')->with('user')
				->join('fa_shop_userinfo `user`','`user`.id=s.uid', 'LEFT')
				->where('is_pay', 1)
				->where($where)
				->order($sort, $order)
				->count();

			$list = $this->model
				->alias('s')->with('user')->field('s.*')
				->join('fa_shop_userinfo `user`','`user`.id=s.uid', 'LEFT')
				->where('is_pay', 1)
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			/** @var array $list */
			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	/**
	 * 补充缺失的uid字段
	 *
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function fill()
	{
		$list = $this->model
			->where('uid', 0)
			->select();
		$user = model('admin/shop/user');
		foreach ($list as $v){
			if($v['openid']){
				$v['uid'] = $user->where($v['origin']==2 ? 'mini_openid' : 'openid', $v['openid'])->max('id');
				$v->save();
			}
		}
		$this->success();
	}
}