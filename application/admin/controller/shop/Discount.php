<?php

namespace app\admin\controller\shop;


use app\common\controller\Backend;
use think\Db;

class Discount extends Backend
{

    private $use_model;

    public function _initialize()
    {
        parent::_initialize();
        $this->use_model = model('admin/shop/Discount');
    }

    public function 	add_discount(){
        if(request()->isPost()){
            $res=$this->add_discount(input('post.'));
            if($res['code']=1){
                $this->success('添加优惠券成功');
                exit();
            }
            else{
                $this->error('添加优惠券失败');
                exit();
            }
        }
        return $this->fetch();
    }

    /**
     * 显示优惠券列表
     * @Author   baohb
     * @DateTime 2018-12-6T14:59:33+0800
     * @return   [type]                   [description]
     */
    public function discount_list()
    {
        $data[0] = input('param.search', '');
        $data[1] = input('param.paginate', 20);
        $data[2] = input('param.other/a');
        $data[3] = input('param.orderby', 'desc');
        $data[4] = true;
       ;
        $res = $this->use_model->list_discount($data);
        $this->assign('list', $res[0]);
        $this->assign('page', $res[1]);

        return $this->fetch();
    }

    /**
     * 最新标准输出
     * @Author   baohb
     * @DateTime 2018-12-6T19:02:38+0800
     * @return   [type]                   [description]
     */
    public function discount_detail()
    {
        $data[0] = input('param.search', '');
        $data[1] = input('param.paginate', 1);
        $data[2] = input('param.other/a');
        $data[3] = input('param.orderby', 'desc');
        $data[4] = false;
        $res = $this->use_model->list_discount($data);

        $data = [];
        if (isset($res) && !empty($res)) {
            $data = $res[0]->getData();
        }
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     *
     * @Author   baohb
     * @DateTime 2018-12-6T18:49:37+0800
     * @return   [type]                   [description]
     */
    public function update_discount(){

        if(request()->isPost()){
            $res=model('admin/shop/Discount')->update_discount(input('post.'));
            if($res['code']=1){
                $this->success('修改优惠券成功');
                exit();
            }
            else{
                $this->error('修改优惠券失败');
                exit();
            }
        }
        return $this->fetch();
    }

}