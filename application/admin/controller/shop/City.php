<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;

class City extends Backend
{

	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/City');
	}

}