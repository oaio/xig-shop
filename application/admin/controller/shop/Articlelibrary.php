<?php
namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
use think\Validate;

class Articlelibrary extends Backend{
    
    protected $searchFields = 'id,title,brief';

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/ArticleLibrary');
	}
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                    	$id=$this->model->id;
                    	$link='/api/commonapi.ajaxcommon/articlelibrary/libid/'.$id;
                    	$this->model->allowField(true)->save(['link'=>$link],['id'=>$this->model->id]);
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 调用码校验重复
     * @Author   ksea
     * @DateTime 2019-04-28T19:42:30+0800
     * @return   [type]                   [description]
     */
    public function lck(){
        $usecode=input('param.row.usecode');

        $val=new Validate([
            'usecode' => 'unique:article_library',
        ]);
        $data=[
            'usecode' => $usecode,
        ];
        if(!$val->check($data)){
            $this->error('调用码重复','',$usecode);
        }
        else{
            $this->success('ok','',$usecode);
        }
    }
}  