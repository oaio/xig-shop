<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;

class Viprecord extends Backend
{

	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/viprecord');
	}
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->alias('a')->with(['user','vipCode'])
	            ->join('fa_shop_userinfo `user`', '`user`.id = a.uid', 'LEFT')
	            ->field('a.id')
                ->where($where)
                ->where(['is_pay'=>1])
                ->order($sort, $order)
                ->count();

           $list=$this->model->alias('a')->with(['user','vipCode'])
	           ->join('fa_shop_userinfo `user`', '`user`.id = a.uid', 'LEFT')
	           ->field('a.*,user.name,user.nickname,user.phone')
                ->where($where)
                ->where(['is_pay'=>1])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function add()
    {
	    if ($this->request->isPost()) {
		    $params = $this->request->post("row/a");
		    $zero = strtotime(date('Ymd'));
		    $params['actiontime'] = $zero;
		    $params['stoptime'] = strtotime($params['endtime']);
		    $params['is_new'] = $params['stoptime'] == $zero ? 2 : 1;
		    $params['vipname'] = '自定义(赠送)';
		    $params['createtime'] = $_SERVER['REQUEST_TIME'];
		    $result = $this->model->allowField(true)->validate(true)->save($params);
		    if ($result !== false) {
		    	model('admin/shop/user')->update([
		    		'id'=>$params['uid'], 'vip_end_time'=>$params['stoptime']]);
			    $this->success();
		    } else {
			    $this->error($this->model->getError());
		    }
	    }
	    return $this->view->fetch();
    }
}