<?php

namespace app\admin\controller\shop;

use app\common\controller\Partner;
use think\Db;
use think\Validate;
use think\Session;

class Order extends Partner
{

    private $use_model;
    protected $dataLimitField = 'o.city_code';

    public function _initialize()
    {
        parent::_initialize();
        $this->use_model = model('admin/shop/Order');
    }

	public function index()
	{
		$this->request->filter(['strip_tags']);
        $admin_info = Session::get('admin');
        $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
        $type = isset($admin_info['type'])?$admin_info['type']:'';
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
            $query =' o.create_time > 1558631041';

			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->use_model
				->alias('o')
				->field('o.*,r.region_name')
				->join('fa_shop_service_order service','service.orderid=o.id', 'LEFT')
				->join('fa_region r','r.region_id=service.areaid', 'LEFT')
				->join('fa_employee e', 'e.id=service.sfuserid', 'LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
				->join('fa_virdiscount d','d.id = o.discount_id','LEFT')
				->where('o.is_pay', 1)
				->where($where)
                ->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('o.city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->count();

			$list = $this->use_model
                ->with(['todo','service.emp','expand','user','store','temp'])
				->alias('o')
				->field('o.*,service.mark,r.region_name,d.dismoney,t.region_name as rn')
				->join('fa_shop_service_order service','service.orderid=o.id', 'LEFT')
				->join('fa_region t','t.region_id=o.city_code', 'LEFT')
				->join('fa_region r','r.region_id=service.areaid', 'LEFT')
				->join('fa_employee `service.emp`', '`service.emp`.id=service.sfuserid', 'LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
				->join('fa_virdiscount d','d.id = o.discount_id','LEFT')
				->where('o.is_pay', 1)
				->where($where)
                ->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('o.city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	public function status()
	{
		$res = model('config')->get(['name'=>'status']);
		return json(json_decode($res->value));
	}

	public function source()
	{
		$res = model('config')->get(['name'=>'source']);
		return json(json_decode($res->value));
	}

    /**
     * 显示列表
     * 可以查询
     * @Author   ksea
     * @DateTime 2018-11-21T14:59:33+0800
     * @return   [type]                   [description]
     */
    public function order_list()
    {
        $data[0] = input('param.search', '');
        $data[1] = input('param.paginate', 10);
        $data[2] = input('param.other/a');
        $data[3] = input('param.orderby', 'desc');
        $data[4] = true;
        $data[5]=power_judge();
        $res = $this->use_model->order_list($data);
        $this->assign('list', $res[0]);
        $this->assign('page', $res[1]);
        if(isset($res[2])){
            $this->assign('search_data', $res[2]);
        }
        return $this->fetch();
    }

    /**
     * 最新标准输出
     * @Author   ksea
     * @DateTime 2018-11-21T19:02:38+0800
     * @return   [type]                   [description]
     */
    public function get_order()
    {
        $data=[
            'id' =>input('param.id'),
        ];
        $Validate=new Validate([
            'id' => 'require',
        ],[
            'id.require',
        ]);
        if(!$Validate->check($data)){
            $this->error('数据错误','/',$Validate->getError());
        }
        $id=input('param.id');
        $data=model('admin/shop/order')->where('id',$id)->with(['paylog','card','virdiscount'])->find();
        $uid=$data['uid'];

        if (power_judge()==null||$data['admin_id']==power_judge()){
            $data['pca'] = is_get_val('shop_userinfo',$uid,'province,city,area');
            $this->assign('data', $data);
            return $this->fetch();
        }else{
            $this->error('数据不存在','shop.error/error_url');
        }
    }
    /**
     * 生成工单
     * @Author   ksea
     * @DateTime 2018-12-24T16:43:14+0800
     * @return   [type]                   [description]
     */
    public function creataServer(){
        
        $uid=Db::name('shop_order')->where('id',input('param.orderid'))->value('uid');
        $area=is_get_val('shop_userinfo',$uid,'area_id','id');
        $address=is_get_val('shop_userinfo',$uid,'address','id');
        $res=controller('api/commonapi/Scan')->CreateScanCode(input('param.orderid'),input('param.appoint_start',''),input('param.sfuserid',''),$area,$address);
        $module_name='工单管理';//模块(菜单)名称(必填)
        $op_name='添加工单';//操作内容(必填)
        $remark=json_encode($res);//操作备注(选填)
        $param=[
        'module_name'=>$module_name,
        'op_name'=>$op_name,
        'remark'=>$remark,
        ];
        \think\Hook::listen('activelog',$param);
        
         if($res['code']!=1||empty($res['data'])){

            $this->error($res['msg'],$res['url'],$res['data'],$res['wait']);
         }
         $img=explode('/',$res['data']);
         $code=explode('_',$img[2]);
         $this->redirect("admin/shop.serverorder/upserver",['id'=>is_get_val('shop_code',$code[0],'serverid')]);         
    }
    /**
     *
     * @Author   ksea
     * @DateTime 2018-11-21T18:49:37+0800
     * @return   [type]                   [description]
     */
    public function up_order()
    {
        $data = input('param.');

        $this->use_model->update_order($data);
    }

	public function service($ids = 0)
	{
		$res = model('app\admin\model\shop\Serverorder')->with('emp')->where('orderid', $ids)->select();
		$this->assign('list', $res);
		return $this->fetch();
	}
}