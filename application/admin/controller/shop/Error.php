<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;

/**
 * 不用ajax
 */

class Error extends Backend{
    /**
     * 添加
     * @Author   baohb 
     * @DateTime 2018-12-8T09:32:52+0800
     */
    public function 	error_url(){
        return $this->fetch();
    }

}