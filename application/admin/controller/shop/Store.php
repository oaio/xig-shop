<?php

namespace app\admin\controller\shop;

use think\Db;
use think\Log;
use app\common\controller\Backend;

class Store extends Backend{
	/**
	 * 限制只有后台用户可以使用
	 * @Author   ksea
	 * @DateTime 2018-12-18T18:41:41+0800
	 * @return   [type]                   [description]
	 */
    public function _initialize()
    {
        parent::_initialize();
/*        $res=model('admin/AuthGroupAccess')->aga();
        if($res['authgroup']['rules']!='*'){
        	 $this->redirect('admin/shop.Error/error_url');
        	 exit();
        }*/
    }

	public function ind($store_id){
          $param=[
            'store_id'=>$store_id,
          ];
          Hook::listen('store_code',$param);
	}
	/**
	 * 添加商户
	 * @Author   ksea
	 * @DateTime 2018-12-18T15:22:10+0800
	 */
	public function add(){
		if(request()->isPost()){
			$res=model('admin/shop/Store')->add_store(input('param.'));
			if($res['code']!=1){
				$this->error($res['msg'],'/',$res);
			}
			else{
				$this->success('添加商户成功');	
			}
		}
		else{
		    $last_user=Db::name('admin')
		      			->alias('a')
		      			->join('fa_store b','a.id=b.admin_id','left')
		      			->field('id,a.username')
		      			->where("admin_id is null")
		      			->select();
		    $this->assign('last_user',$last_user);
			return $this->fetch('shop/store/store_add');
		}
	}
	/**
	 * 商户删除
	 * @Author   ksea
	 * @DateTime 2018-12-18T17:20:04+0800
	 * @return   [type]                   [description]
	 */
	public function delete(){
		$res=model('admin/shop/Store')->del_store(input('param.'));
		if($res['code']!=1){
			$this->error('删除商户失败','/',$res);
		}
		else{
			$this->success('删除商户成功');	
		}
	}
	/**
	 * 商户编辑
	 * @Author   ksea
	 * @DateTime 2018-12-18T15:28:56+0800
	 * @return   [type]                   [description]
	 */
	public function update(){
		$store_id=input('param.store_id');
		$data=model('admin/shop/store')->with(['bindstore'])->where('store_id',$store_id)->find();
		if(request()->isPost()){
			$params = input('param.');
			empty($params['city_id']) && $this->error('请选择店铺所在地区');
			if(isset($params['row'])){
				model('admin/shop/StoreExt')::update($params['row'], ['id'=>$data['ext_id']]);
				unset($params['row']);
			}
			$res=model('admin/shop/Store')->up_store($params);
			if($res['code']!=1){
				$this->error($res['msg'],'admin/shop.store/store_list',$res);
			}
			else{
				$this->success('修改商户成功','admin/shop.store/store_list');	
			}
		}
//		else{
	        $last_user=Db::name('admin')
	      			->alias('a')
	      			->join('fa_store b','a.id=b.admin_id','left')
	      			->field('id,a.nickname')
	      			->where("admin_id is null")
	      			->select();
	        $this->assign('last_user',$last_user);

			if($data['update_time']<($data['update_time']+5)){
				$new=1;
			}
			else{
				$new=0;
			}
			$this->assign('new',$new);
			$this->assign('data',$data);
			$this->assign('row', model('admin/shop/StoreExt')::get($data['ext_id']));

			return $this->fetch('shop/store/store_update');
//		}
	}

	/**
	 * 商户列表
	 * @Author   ksea
	 * @DateTime 2018-12-18T17:24:21+0800
	 * @return   [type]                   [description]
	 * @throws \think\Exception
	 */
	public function store_list(){
		  
	      $data[0]=input('param.search','');
	      $data[1]=input('param.paginate',10);
	      $data[2]=input('param.other/a');
	      $data[3]=input('param.orderby','desc');
	      $data[4]=true;
	      $res=model('admin/shop/Store')->store_list($data);
	      $this->assign('list', $res[0]);
	      $this->assign('page', $res[1]);
	      if(isset($res[2])){
	      	$this->assign('search_data', $res[2]);
	      }
	      //fixme add by d3li
	      $mod = model('admin/shop/StoreExt');
	      $day = date('Y-m-d');
	      $month = date('Y-m');
		$this->assign('count', [
			$mod->where('status', '>', 10)->count(),
			$mod->where('status', 10)->count(),
			$mod->where("DATE_FORMAT(FROM_UNIXTIME(check_time), '%Y-%m-%d')='$day'")
				->where('status',60)->count(),
			$mod->where("DATE_FORMAT(FROM_UNIXTIME(check_time), '%Y-%m')='$month'")
				->where('status',60)->count(),
			$mod->where("DATE_FORMAT(FROM_UNIXTIME(check_time), '%Y-%m')='$month'")
				->where('status',40)->count()
	      ]);
		//fixme end
          $type = session('admin.type');
          $this->assign('type', $type);
	      return $this->fetch();
	}
    /**
     * 冻结修改操作
     * @Author   ksea
     * @DateTime 2018-12-18T20:11:58+0800
     * @return   [type]                   [description]
     */
    public function up_freeze(){
       $is_af=input('param.is_af');
       $store_id=input('param.store_id');
       if(0 == $is_af){
       	model('store')::update(['is_af'=>1], ['store_id'=>$store_id]);
	       $this->success('审核成功');
       }
       $fr=config('is_af');
       $now_af=$is_af+ (1 == $is_af ? 1 : 2);
       $is_fr=$fr[$now_af];
       $rest=Db::name('store')->where('store_id',$store_id)->update(['is_af'=>$is_fr['0']]);
       if(is_numeric($rest)){
       	 $this->success('修改成功');
       }
       else{
       	 $this->error('修改失败');
       }
    }

	/**
	 * 审核操作
	 *
	 * @author d3li 2019/8/3
	 * @param int $store_id
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function check($store_id = 0)
    {
	    $mod = model('admin/shop/StoreExt');
	    $info = $mod->with('r')
		    ->where(['store_id' => $store_id])
		    ->order('id', 'DESC')
		    ->find();
	    if ($this->request->isPost()) {
		    $params = $this->request->post("row/a");
		    $ary = explode(',', $info['region']);
		    $ary = array_replace($ary, [
			    input('param.province_id'),
			    input('param.city_id'),
			    input('param.area_id')
		    ]);
		    $ret = model('region')::all(['region_id' => ['in', $ary]]);
		    $arr = [];
		    foreach ($ret as $v) $arr[$v['region_id']] = $v['region_name'];
		    ksort($arr);
		    $data = [
			    'nickname' => $params['store_name'],
			    'ruler' => $params['ruler'],
			    'ruler_phone' => $params['ruler_phone'],
			    'address' => $params['address'],
			    'area' => implode(',', $arr),//$info['region_str'],
			    'back' => $params['store_type'],
			    'is_af' => 1,
			    'province_id' => $ary[0],
			    'city_id' => $ary[1],
			    'area_id' => $ary[2],
			    'ext_id' => $params['id'],
			    'salesphone' => $params['invite_phone'],
		    ];
		    model('store')::update($data, ['store_id' => $store_id]);
		    $info->update(['id' => $info['id'], 'check_time' => $this->request->time(), 'status' => 60]);
		    (new \aliyun\SendTemp())::AuditResultsSuccess($params['ruler_phone']);
		    $this->success();
	    }
	    $reg = Db::name('storekeeper_register_logs')->where('store_id', $store_id)->find();
	    $info['invite'] = $reg['invite_phone'];
	    $info['ruler_phone'] = $reg['phone'];
	    return $this->fetch('', ['info' => $info]);
    }

	/**
	 * 是否开通店铺
	 *
	 * @author d3li 2019/8/16
	 * @param int $id
	 * @throws \think\exception\DbException
	 */
	public function open($id = 0)
    {
	    $row = model('admin/shop/StoreExt')::get($id);
    	model('store')::update(['store_id' => $row['store_id'], 'is_af'=>2]);
	    $this->success('开通成功');
    }

	/**
	 * 拒绝
	 *
	 * @author d3li 2019/8/3
	 * @throws \think\exception\DbException
	 */
	public function deny()
    {
    	$id = input('param.id', 0, 'intval');
    	$reason = input('param.reason');
	    $row = model('admin/shop/StoreExt')::get($id);
	    $id && $row->update([
	    	'id' => $id,
		    'check_time' => $this->request->time(),
		    'reason' => $reason,
		    'status' => 40
	    ]);
	    $reg = Db::name('storekeeper_register_logs')
		    ->where('store_id', $row['store_id'])->find();
	    (new \aliyun\SendTemp())::AuditResultsError($reg['phone'], $reason);
	    $this->success('', '', 1);
    }
}