<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Backend;
/**
 * Orders.php 全部订单（for 财务）
 *
 * @author d3li <d3li@sina.com>
 * @create：1/15/2019  3:38 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0115
 * @describe
 */
class Orders extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\ShopOrder $model
	 */
	protected $model;
	protected $searchFields = [
		'a.snorder',
		'a.nickname',
		'a.mobile',
		'a.address'
	];

	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('ShopOrder');
	}

	/**
	 * 全部订单
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model->alias('a')
				->join('fa_store store', 'store.store_id = a.store_id', 'LEFT')
				->join('fa_shop_userinfo f', 'f.id = a.uid', 'LEFT')
				->where($where)
				->count();

			$sum = $this->model->alias('a')
				->join('fa_store store', 'store.store_id = a.store_id', 'LEFT')
				->join('fa_shop_userinfo f', 'f.id = a.uid', 'LEFT')
				->where($where)
				->sum('actualpay');
			try {
				$query = $this->model->getQuery();
				call_user_func_array($where, [&$query]);
				$where_arr = $query->getOptions('where')['AND'];
				$extend = model('WechatPay')
					->alias('a')
					->join('fa_shop_order shop_order', 'shop_order.id = a.oid', 'LEFT')
					->join('fa_store store', 'store.store_id = shop_order.store_id', 'LEFT')
					->where('a.attach', 3);
				if ($where_arr) {
					foreach ($where_arr as $k => $v) {
						if ('between time' == strtolower($v[0])) {
							$date = array_map(function ($date) {
								return strtotime($date);
							}, $v[1]);
							$date[1] += 86399;
							$extend = $extend->where($k, 'between', $date);
						} else {
							$extend = $extend->where($k, $v[0], $v[1]);
						}
					}
				}
				$extend = $extend->sum('a.total_fee');
				$extend = sprintf('%.2f', $extend / 100);
			}catch (\Exception $e){
				$extend = 0;
			}

			$list = $this->model
				->alias('a')->field('a.*')->with('a,f')
				->join('fa_store store', 'store.store_id = a.store_id', 'LEFT')
				->join('fa_shop_userinfo f', 'f.id = a.uid', 'LEFT')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array('total' => $total, 'rows' => $list, 'sum'=>$sum, 'extend'=>$extend);

			return json($result);
		}
		return $this->view->fetch();
	}

	/**
	 *  自定义付款
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 */
	public function custom()
	{
		if ($this->request->isAjax()) {
			$model = model('WechatPay');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams(['f.name','f.phone','f.address']);
			$total = $model
				->alias('a')
				->join('fa_shop_userinfo f', 'f.openid = a.openid', 'LEFT')
				->where($where)
				->where(['attach' => 2])
				->count();
			$pay = $model
				->alias('a')
				->join('fa_shop_userinfo f', 'f.openid = a.openid', 'LEFT')
				->where($where)
				->where(['attach' => 2])
				->sum('a.total_fee');
			$list = $model
				->alias('a')
				->join('fa_shop_userinfo f', 'f.openid = a.openid', 'LEFT')
				->join('fa_store s', 's.store_id = f.store_id', 'LEFT')
				->where(['a.attach' => 2])
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->field('a.*,f.name,f.phone,f.address,f.nickname as names,s.nickname,s.ruler,s.ruler_phone')
				->select();
			$result = array('total' => $total, 'rows' => $list, 'extend'=>sprintf('%.2f', $pay/100));

			return json($result);
		}
		return $this->view->fetch();
	}

	/**
	 * 添加
	 * @internal
	 */
	public function add()
	{
		$this->error();
	}

	/**
	 * 编辑
	 * @internal
	 */
	public function edit($ids = 0)
	{
		$this->error();
	}

	/**
	 * 删除
	 */
	public function del($ids = 0)
	{
		$this->error();
	}

	/**
	 * 详情
	 *
	 * @param int $ids
	 * @return mixed
	 * @throws \think\exception\DbException
	 */
	public function detail($ids = 0)
	{
		$this->assign('info', $this->model->get($ids));
		return $this->fetch();
	}
}