<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;

class Userdistribution extends Backend
{
	protected $searchFields = 'id,name,phone';
	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/UserDistribution');
	}
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //城市合伙人条件
            $city_code = session('admin.city_code')?session('admin.city_code'):'';
            $type = session('admin.type')?session('admin.type'):'1';
            $where2 = '';
            if($type == 2 && $city_code){
                $where2 = 'use_city_code = '.$city_code.'';
            }
            $total = $this->model
                ->where($where)
                ->where($where2)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($where2)
                ->with(['outcome','income','store','fans','GetFansData'])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 粉丝概况
     * @Author   ksea
     * @DateTime 2019-03-28T14:23:39+0800
     * @param    string                   $ids [description]
     * @return   [type]                        [description]
     */
    public function fans($ids=''){
    	$this->request->filter(['strip_tags']);
    	$data=model('admin/shop/ShopFans')->where(['fuid'=>$ids])->field("sum(sumorder) as mysumorder,sum(sumsales) as mysumsales,sum(sumoutcome) as mysumoutcome,count(*) as mycount")->find();
    	return $this->view->fetch('',['fans'=>$data]);
    }
    /**
     * 粉丝数据明细
     * @Author   ksea
     * @DateTime 2019-03-28T13:52:42+0800
     * @param    string                   $ids [description]
     * @return   [type]                        [description]
     */
	public function fansdetail($ids = '')
	{
		$model=model('admin/shop/ShopFans');
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}

			$model->where(['fuid'=>$ids]);
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$option = $model->getOptions('where')['AND'];
			$list = $model
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$total = $model
				->where($where)
				->where($option)
				->count();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch('',[ 'ids'=>$ids]);
	}
    /**
     * 收益明细
     * @Author   ksea
     * @DateTime 2019-03-28T13:52:42+0800
     * @param    string                   $ids [description]
     * @return   [type]                        [description]
     */
	public function incomedetail($ids = '')
	{
		$model=model('admin/shop/ShopOutcome');
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}

			$model->where(['fuid'=>$ids]);
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$option = $model->getOptions('where')['AND'];
			$list = $model->with('order')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$total = $model
				->where($where)
				->where($option)
				->count();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch('',[ 'ids'=>$ids]);
	}
	/**
	 * 店铺明细
	 * @Author   ksea
	 * @DateTime 2019-03-28T15:18:06+0800
	 * @return   [type]                   [description]
	 */
	public function store($ids=''){
    	$this->request->filter(['strip_tags']);
    	$data=model('admin/shop/ShopUserinfoStore')->where(['uid'=>$ids])->find();
    	return $this->view->fetch('',['store'=>$data]);
	}
}