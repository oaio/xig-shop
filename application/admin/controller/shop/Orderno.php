<?php

namespace app\admin\controller\shop;


use app\common\controller\Backend;
use think\Db;

class Orderno extends Backend
{

    private $use_model;

    public function _initialize()
    {
        parent::_initialize();
        $this->use_model = model('admin/shop/Orderno');
    }

    /**
     * 显示列表
     * @Author   baohb
     * @DateTime 2018-12-4T14:59:33+0800
     * @return   [type]                   [description]
     */
    public function orderno_list()
    {
        $data[0] = input('param.search', '');
        $data[1] = input('param.paginate', 20);
        $data[2] = input('param.other/a');
        $data[3] = input('param.orderby', 'desc');
        $data[4] = true;

        $res = $this->use_model->list_orderno($data);

        $this->assign('list', $res[0]);
        $this->assign('page', $res[1]);

        return $this->fetch();
    }

    /**
     * 流水详情
     * @Author   baohb
     * @DateTime 2018-12-4T19:02:38+0800
     * @return   [type]                   [description]
     */
    public function orderno_detail(){
        $id=input('id');
        if($id){
            $res=model('admin/shop/Orderno')->get_orderno_detail($id);

            $this->assign("data",$res);
        }
        else{
            $this->error('请求错误');
        }
        return $this->fetch();
    }


}