<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;

/**
 * 不用ajax
 */

class Feedback extends Backend{

   /**
    * 显示意见反馈列表
    * @Author   ksea
    * @DateTime 2018-11-16T09:39:31+0800
    * @return   [type]                   [description]
    */
   public function feedback_list(){

      $data[0]=input('param.search','');
      $data[1]=input('param.paginate',20);
      $data[2]=input('param.other/a');
      $data[3]=input('param.orderby','desc');
      $data[4]=true;

      $res=model('admin/shop/feedback')->feedback_list($data);
      $this->assign('list', $res[0]);

      $this->assign('page', $res[1]);

         return $this->fetch();
      }

}  