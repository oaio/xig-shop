<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;


class Categrory extends Backend{

   /**
    * 分类列表
    */
   public function getcate(){
      $ret=model('admin/shop/Categrory')->getallcate();
      $this->assign('data',$ret);
      return $this->fetch('shop/categrory/getcate');
   }
   /**
    * 删除
    * @Author   ksea
    * @DateTime 2018-11-15T19:06:57+0800
    * @return   [type]                   [description]
    */
   public function delcate(){
      $res=controller('api/shopapi/Restcategrory')->delcate()->getData();
      if($res['code']==1){
         return $this->getcate();
      }
      else{
         $this->error('删除错误');
      }
   }
   /**
    * 修改
    * @Author   ksea
    * @DateTime 2018-11-15T19:06:57+0800
    * @return   [type]                   [description]
    */
   public function upcate(){
      $res=controller('api/shopapi/Restcategrory')->editcate()->getData();
      if($res['code']==1){
          return $res;
      }
      else{
          $this->error('删除错误');
      }
   }
   /**
    * 添加分类
    * @Author   ksea
    * @DateTime 2018-11-16T09:06:46+0800
    * @return   [type]                   [description]
    */
   public function addcate(){
      return $this->fetch();
   }
}

