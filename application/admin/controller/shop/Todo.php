<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Partner;
use think\Db;
use think\Session;

/**
 * Todo.php 客服后台-待办池
 *
 *  @icon fa fa-hand-stop-o
 * @author d3li <d3li@sina.com>
 * @create：15/05/2019  9:50 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.15
 * @describe
 */
class Todo extends Partner
{
	protected $searchFields = ['o.snorder','o.nickname','o.mobile','e.name'];
	protected $dataLimitField = 'o.city_code';
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/Order');
	}

	public function index($ids = 0)
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$this->model->where('schedule', 9);
			if($ids){
				$this->model->where('o.id', $ids);
			}
			$option = $this->model->getOptions('where')['AND'];
            //城市合伙人条件
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';
			$total = $this->model
				->with('todo')
				->alias('o')
				->field('o.id,')
				->join('fa_shop_order_todo todo', 'todo.oid = o.id')
				->join('fa_shop_service_order s','s.orderid=o.id', 'RIGHT')
				->join('fa_region r','r.region_id=s.areaid', 'LEFT')
				->join('fa_employee e', 'e.id=s.sfuserid', 'LEFT')
				->where($where)
				->where($option)
				->order($sort, $order)
				->count();

			$list = $this->model
				->with('todo,paylog')
				->alias('o')
				->field('o.*,s.mark,r.region_name,e.name')
				->join('fa_shop_order_todo todo', 'todo.oid = o.id')
				->join('fa_shop_service_order s','s.orderid=o.id', 'RIGHT')
				->join('fa_region r','r.region_id=s.areaid', 'LEFT')
				->join('fa_employee e', 'e.id=s.sfuserid', 'LEFT')
				->where($where)
				->where($option)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch('', ['ids'=>$ids]);
	}

	/**
	 * 改派服务人员
	 *
	 * @param int $ids
	 * @return string
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function edit($ids = 0)
	{
		$model = model('admin/shop/Serverorder');
		if($this->request->isAjax()){
			$param=$this->request->param('row/a');
			$row=$model->get(['orderid'=>$ids]);
			if(!is_null($param['sfuserid'])){
				$this->model->isUpdate(true)->save(['id'=>$ids, 'schedule'=>2]);
				$emp = model('admin/shop/Employee')::get($param['sfuserid']);
				model('admin/shop/ShopOrderHistory')->create([
					'oid'=>$ids,
					'desc' => session('admin.nickname').' 改派给 '.$emp['name']
				]);
				$param['id'] = $row['id'];
				$param['mobile'] = privacy_call($row['order']['mobile'], $emp['phone']);
			}
			$model->isUpdate(1)->save($param);
			$this->success('ok','/', $row);
		}
		$source = model('config')->get(['name'=>'source']);
		$list = $model
			->with(['order.expand','code','card','emp', 'ad','temp'])
			->alias('server')
			->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
			->join('fa_shop_order order','server.orderid = order.id','LEFT')
			->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
			->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
			->join('fa_region region','region.region_id = server.areaid','LEFT')
			->join('fa_employee e', 'e.id=server.sfuserid', 'LEFT')
			->field('server.*,cate.name as cate_name,cate.id as cate_id,e.name as sf,
			goods.cate,goods_msg,discount.bean,discount.dismoney,
			order.goods_name,order.shouldpay,order.actualpay,order.mobile,
			region.region_name,region.region_id,order.snorder,order.nickname,order.num')
			->where('orderid', $ids)
			->find();
		return $this->view->fetch('', ['list'=>$list,'source'=>json_decode($source->value)]);
	}

	public function history($ids = 0)
	{
		$m = model('admin/shop/Serverorder')::get($ids);
		$list=model('admin/shop/ShopOrderHistory')
			->where('oid', $m->orderid)->select();
		return $this->view->fetch('', ['list'=>$list]);
	}

	public function diary($ids = 0)
	{
		$list=model('admin/shop/ShopOrderHistory')
			->where('oid', $ids)->select();
		return $this->view->fetch('history', ['list'=>$list]);
	}

	public function emp($gid = 1)
	{
		$good = model('admin/shop/goods')->get($gid);
		$list = model('admin/shop/employee')->getSf($good->cate);
		$data = [];
		foreach($list as $row)
		{
			$data[] = [
				'name' =>$row->name,
				'value' => $row->id
			];
		}
		$this->success('', null, $data);
	}

	public function pause($ids = 0)
	{
		$this->model->pause($ids);
		$this->success(__('Operation completed'), 'shop/todo');
	}

	public function cancel($ids = 0)
	{
		$this->model->cancel($ids);
		$this->success(__('Operation completed'), 'shop/close');
	}

	public function mark($ids = 0)
	{
		$m = model('admin/shop/Serverorder');
		$m = $m::get($ids);
		if($this->request->isPost()) {
			$mark = input('mark', '', 'strip_tags');
			$re = $m->update(['mark'=>$mark, 'id'=>$ids]);
			if ($re){
				$red=model('admin/shop/ShopOrderHistory')::create([
					'oid'=>$m->orderid,
					'desc' => session('admin.nickname').' 修改了客诉:'.$mark,
				]);
				$this->success();
			}
			$this->error();
		}
		return $this->view->fetch('',['row'=> $m]);
	}

	public function sign($ids = 0)
	{
		$m = model('admin/shop/Serverorder');
		$m = $m::get(['orderid'=>$ids]);
		if($this->request->isPost()) {
			$mark = input('mark', '', 'strip_tags');
			$re = $m->update(['mark'=>$mark],['orderid'=>$ids]);
			if ($re){
				$red=model('admin/shop/ShopOrderHistory')::create([
					'oid'=>$ids,
					'desc' => session('admin.nickname').' 修改了客诉:'.$mark,
				]);
				$this->success();
			}
			$this->error();
		}
		return $this->view->fetch('mark',['row'=> $m]);
	}
}