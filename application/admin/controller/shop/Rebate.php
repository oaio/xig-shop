<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Backend;
/**
 * Rebate.php 商户分销
 *
 * @author d3li <d3li@sina.com>
 * @create：12/29/2018  5:07 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1229
 * @describe
 */
class Rebate extends Backend
{
	/**
	 * @var \app\admin\model\ShopPayLog
	 */
	protected $model = null;
	protected $searchFields = 'a.trade_no,a.username';


	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('shopPayLog');
	}

	/**
	 * 列表页
	 *
	 * @throws \think\Exception
	 */
	public function index()
	{
		//设置过滤方法
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->where($where)
				->order($sort, $order)
				->count();
			$list = $this->model
				//->with('store')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->fetch();
	}
}