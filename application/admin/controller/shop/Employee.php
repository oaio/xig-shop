<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Partner;
/**
 * Employee.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/21/2018  5:53 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1221
 * @describe
 */
class Employee extends Partner
{
	/**
	 * @internal
	 * @var \app\admin\model\Employee $model
	 */
	protected $model = null;
	protected $searchFields = 'name,phone';

	/**
	 *  初始化设置
	 *
	 * @author d3li 12/21/2018
	 */
	public function _initialize(){
		parent::_initialize();
		$this->model= model('Employee');
		$res = model('ShopCategory')->where('pid', 0)->select();
		$ary = array();
		if($res)foreach ($res as $v){
			$ary[$v->id] = $v->name;
		}
        //城市合伙人条件
        $city_code = session('admin.city_code')?session('admin.city_code'):440300;
		$res_area = model('Region')->where('region_parent_id', $city_code)->select();
		$area = array();
		if($res_area)foreach ($res_area as $v){
			$area[$v->region_id] = $v->region_name;
		}
		$this->assign('jobs', $ary);
		$this->assign('area',$area);
	}

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //城市合伙人条件
            $city_code = session('admin.city_code')?session('admin.city_code'):'';
            $type = session('admin.type')?session('admin.type'):'1';
            $where2 = '';
            if($type == 2 && $city_code){
                $where2 = 'city_code = '.$city_code.'';
            }
            $total = $this->model
                ->where($where)
                ->where($where2)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($where2)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

	/**
	 * 检测身份证是否存在
	 *
	 * @author d3li 12/25/2018
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function card()
	{
		$res = $this->model->where('cards',input('row.cards'))->find();
		if (empty($res)){
			exit(json_encode(['code'=>1,'msg'=>'成功']));
		}else {
			exit(json_encode(['code'=>0,'msg'=>'这个证件号码已经存在']));
		}
	}

	/**
	 * 检测手机号码
	 *
	 * @author d3li 12/25/2018
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function phone()
	{
		$res = $this->model->where('phone',input('row.phone'))->find();
		if (empty($res)){
			exit(json_encode(['code'=>1,'msg'=>'成功']));
		}else {
			exit(json_encode(['code'=>0,'msg'=>'这个号码已经存在']));
		}
	}

	/**
	 * 添加
	 */
	public function add()
	{
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			$res = model('admin/shop/Employee')->where('phone',input('row.phone'))->find();
			if($res&&!empty($res))return $this->error('这个号码已经存在');
			if ($params) {
				if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
					$params[$this->dataLimitField] = $this->auth->id;
				}
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
						$this->model->validate($validate);
					}
					$group = model('AuthGroupAccess')::get(['uid'=>session('admin.id')]);
					if($group->group_id != 6){
						$params['admin_id'] = 2;
					}
                    //城市合伙人条件
                    $city_code = session('admin.city_code')?session('admin.city_code'):'';
                    $type = session('admin.type')?session('admin.type'):'1';
                    if($type == 2 && $city_code){
                        $params['city_code'] = $city_code;
                    }
					$params['job'] = implode(',', $params['job']);
					$params['area_id'] = implode(',', $params['area_id']);
					$result = $this->model->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($this->model->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		return $this->view->fetch();
	}

	/**
	 * 编辑
	 */
	public function edit($ids = NULL)
	{
		$row = $this->model->get($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$params['job'] = implode(',', $params['job']);
					$params['area_id'] = implode(',', $params['area_id']);
					$result = $row->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($row->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign("row", $row);
		return $this->view->fetch();
	}


	public function log()
	{
		$action = $this->request->action();
		$ary = ['add'=>'新增','edit'=>'编辑','del'=>'删除'];
		if ($this->request->isPost() && in_array($action, array_keys($ary))){
			$param=[
				'module_name'=>'员工管理',
				'op_name'=>$ary[$action] . '员工',
				'remark'=>json_encode(input()),
			];
			\think\Hook::listen('activelog',$param);
		}
	}

	public function get()
	{
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->where($where)
				->count();
			$list = $this->model
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
	}

	public function all($cate=1){
        if ($this->request->isAjax()) {
        	$query="";

        	$query.=" FIND_IN_SET({$cate},job)";

            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
            	if(strlen($query)>2){
					return $this->selectpage($query);
            	}
            	else{
                    return $this->selectpage();
            	}
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //城市合伙人条件
            $city_code = session('admin.city_code');
            $type = session('admin.type');
            $total = $this->model
                ->where($where)
                ->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('city_code',$city_code);
                    }
                })
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('city_code',$city_code);
                    }
                })
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
                
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
	}


    /**
     * Selectpage的实现方法
     *
     * 当前方法只是一个比较通用的搜索匹配,请按需重载此方法来编写自己的搜索逻辑,$where按自己的需求写即可
     * 这里示例了所有的参数，所以比较复杂，实现上自己实现只需简单的几行即可
     *
     */
    protected function selectpage($query='')
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'htmlspecialchars']);

        //搜索关键词,客户端输入以空格分开,这里接收为数组
        $word = (array)$this->request->request("q_word/a");
        //当前页
        $page = $this->request->request("pageNumber");
        //分页大小
        $pagesize = $this->request->request("pageSize");
        //搜索条件
        $andor = $this->request->request("andOr", "and", "strtoupper");
        //排序方式
        $orderby = (array)$this->request->request("orderBy/a");
        //显示的字段
        $field = $this->request->request("showField");
        //主键
        $primarykey = $this->request->request("keyField");
        //主键值
        $primaryvalue = $this->request->request("keyValue");
        //搜索字段
        $searchfield = (array)$this->request->request("searchField/a");
        //自定义搜索条件
        $custom = (array)$this->request->request("custom/a");
        $order = [];
        foreach ($orderby as $k => $v) {
            $order[$v[0]] = $v[1];
        }
        $field = $field ? $field : 'name';

        //如果有primaryvalue,说明当前是初始化传值
        if ($primaryvalue !== null) {
            $where = [$primarykey => ['in', $primaryvalue]];
        } else {
            $where = function ($query) use ($word, $andor, $field, $searchfield, $custom) {
                $logic = $andor == 'AND' ? '&' : '|';
                $searchfield = is_array($searchfield) ? implode($logic, $searchfield) : $searchfield;
                foreach ($word as $k => $v) {
                    $query->where(str_replace(',', $logic, $searchfield), "like", "%{$v}%");
                }
                if ($custom && is_array($custom)) {
                    foreach ($custom as $k => $v) {
                        $query->where($k, '=', $v);
                    }
                }
            };
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            $this->model->where($this->dataLimitField, 'in', $adminIds);
        }
        $list = [];
        $total = $this->model->where($where)->where($query)->count();
        if ($total > 0) {
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $datalist = $this->model->where($where)->where($query)
                ->order($order)
                ->page($page, $pagesize)
                ->field($this->selectpageFields)
                ->select();
            foreach ($datalist as $index => $item) {
                unset($item['password'], $item['salt']);
                $list[] = [
                    $primarykey => isset($item[$primarykey]) ? $item[$primarykey] : '',
                    $field      => isset($item[$field]) ? $item[$field] : ''
                ];
            }
        }
        //这里一定要返回有list这个字段,total是可选的,如果total<=list的数量,则会隐藏分页按钮
        return json(['list' => $list, 'total' => $total]);
    }
}