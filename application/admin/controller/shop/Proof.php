<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;
use app\common\controller\Backend;

/**
 * Proof.php 免费券列表
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/7/16  9:17
 * @see      https://gitee.com/d3li/
 * @version 2.07.16
 * @describe
 */
class Proof extends Backend
{
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/FreeRoll');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model->alias('m')
				->with('user')->field('m.*')
				->join('fa_shop_userinfo `user`', '`user`.id=m.uid', 'LEFT')
				->where($where)
				->count();

			$list = $this->model->alias('m')
				->with('m,user')->field('m.*')
				->join('fa_shop_userinfo `user`', '`user`.id=m.uid', 'LEFT')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			/** @var array $list */
			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

}