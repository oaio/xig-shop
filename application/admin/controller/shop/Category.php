<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
/**
 * Category.php 分类管理
 *
 * @author d3li <d3li@sina.com>
 * @create：12/21/2018  11:51 AM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1221
 * @describe
 */
class Category extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\ShopCategory $model
	 */
	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();
		$controllername = strtolower($this->request->controller());
		$actionname = strtolower($this->request->action());

		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		// 判断控制器和方法判断是否有对应权限
		if (!$this->auth->check($path)) {
			$this->redirect('shop.error/error_url');
		}
		$this->model = model('shopCategory');
		//$this->assign('parent', $this->model->parent());
	}

	/**
	 * 删除操作
	 *
	 * @internal
	 * @param string $ids
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function del($ids = "")
	{
		if ($ids) {
			$pk = $this->model->getPk();
			$list = $this->model->where($pk, 'in', $ids)->select();
			$count = 0;
			foreach ($list as $k => $v) {
				$count += $v->artdel();
			}
			if ($count) {
				$this->success();
			} else {
				$this->error(__('No rows were deleted'));
			}
		}
		$this->error(__('Parameter %s can not be empty', 'ids'));
	}

	/**
	 * 批量更新
	 *
	 * @internal
	 * @param string $ids
	 */
	public function multi($ids = "")
	{
		// 组别禁止批量操作
		$this->error();
	}

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost())
        {
            $params = $this->request->post("row/a");
            if ($params)
            {

            	if($params['pid']==0){
            		$params['floorid']=0;
            	}
            	else{
            		$params['floorid']=1;
            	}
            	$this->model->data($params)->allowField(true)->save();
                $this->success();
            }
            $this->error();
        }
        $groupdata_obj=$this->model->where('floorid=0')->select();
        $groupdata=[];
        $groupdata[0]="顶级分类";
        foreach ($groupdata_obj as $k=> $v) {
        	 $groupdata[$v->id]='--'.$v->name;
        }
        $this->assign('groupdata',$groupdata);
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model->get($ids);
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($row->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        $groupdata_obj=$this->model->where('floorid=0')->select();
        $groupdata=[];
        $groupdata[0]="顶级分类";
        foreach ($groupdata_obj as $k=> $v) {
        	 $groupdata[$v->id]='--'.$v->name;
        }
        $groupids=$row->pid;
        $this->assign('groupids',$groupids);
        $this->assign('groupdata',$groupdata);
        return $this->view->fetch();
    }

	/**
	 * @return Auth
	 * @throws \think\exception\DbException
	 */
	public function get()
	{
		$res = $this->model->all();
		$data = [];
		foreach($res as $v){
			$data[$v->id] = $v->name;
		}
		return json($data);
	}
}