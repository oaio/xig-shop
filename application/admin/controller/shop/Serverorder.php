<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;
use think\Session;
use \think\Validate;

class Serverorder extends Backend{
 	private $use_model;
 	public function _initialize()
	{
        parent::_initialize(); 
        $this->use_model=model('admin/shop/Serverorder');
    }
	/**
	 * 显示某人预约卡片，或者全部人的全部卡片
	 * @Author   baohb
	 * @DateTime 2018-12-11T09:55:29+0800
	 * @return   [type]                   [description]
	 */
	public function server()
    {

		$data[0]=input('param.search','');
		$data[1]=input('param.paginate',10);
		$data[2]=input('param.other/a');
		$data[3]=input('param.orderby','desc');
		$data[4]=true;
		$data[5]=power_judge();
        $res=$this->use_model->myserver($data);
        if(isset($res[2])){
            $this->assign('search_data', $res[2]);
        }
        $tab="<script>Backend.api.closetabs('/index.php/admin/shop/order/order_list');Backend.api.addtabs('/index.php/admin/shop/serverorder/server')</script>";
        $this->assign('tab',$tab);
	    $this->assign('list',$res[0]);
	    $this->assign('page',$res[1]);
	    return $this->fetch();
	}
	/**
	 * 更新操作或者跳转更新页面
	 * @Author   baohb
	 * @DateTime 2018-12-11T10:25:54+0800
	 * @return   [type]                   [description]
	 */
	public function upserver()
    {

        $date['update_time']= strtotime(date('Y-m-d h:i:s', time()));
		if(request()->isPost()){
			$post_data=input('post.');

			$validate=new Validate([
				'address'	=> 'require',
			]);

			if (!$validate->check($post_data)) {
			   	$this->error($validate->getError());
			}
            $post_data=array_merge($post_data,$date);
			if(!empty($post_data['id'])){
				$res=$this->use_model->upserver($post_data);
				$module_name='工单管理';//模块(菜单)名称(必填)
				$op_name='更新工单';//操作内容(必填)
				$remark=json_encode($res);//操作备注(选填)
				$param=[
				'module_name'=>$module_name,
				'op_name'=>$op_name,
				'remark'=>$remark,
				];
				\think\Hook::listen('activelog',$param);
				/*********用户所属区域绑定发送**********/
				$gettemplate=[
					'server_id'	=> $post_data['id'],
					'sfuserid'	=> $post_data['sfuserid'],
				];
				/**********用户所属区域绑定发送*********/
				//\think\Hook::listen('gettemplate',$gettemplate);
				if($res['code']==1){
					$this->redirect('admin/shop.serverorder/server');
					exit();
				}
				else{
					$this->error('更新错误','',$res);
					exit();
				}
			}
			else{
				return false;
			}
		}
		else{
		    $res=$this->use_model->getone(input('param.id'));
            //可选员工
            $where['admin_id']=session('admin.id');
            $where['delete_time'] = 0;
            $staff =Db::name('employee')->where($where)->field('id,name')->select();
            $uid=$res['uid'];
            $res['pca'] = is_get_val('shop_userinfo',$uid,'province,city,area');
            if (power_judge()==null||$res['admin_id']==power_judge()) {
                $this->assign('staff',$staff);
                $this->assign('staff_json',json_encode($staff));
                $this->assign('data', $res);
                return $this->fetch('shop/serverorder/getone');
                exit();
            }else{
                $this->error('数据不存在','shop.error/error_url');
            }

		}
	}
	/**
	 * 删除卡片
	 * @Author   baohb
	 * @DateTime 2018-12-11T10:26:55+0800
	 * @return   [type]                   [description]
	 */
	public  function delserver()
    {
		if(input('id')){
			$res=$this->use_model->delserver(input('id'));
			if($res['code']==1){
				$this->success('删除成功');
				exit();
			}
			else{
				$this->error('删除错误','',$res);
				exit();
			}
		}
		else{
			$this->error('操作错误');
		}
	}
}