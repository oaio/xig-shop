<?php

namespace app\admin\controller\shop;

use app\admin\model\shop\ShopOutcome;
use app\common\controller\Backend;
use think\Db;
use think\Session;

/**
 * 新单池
 */
class Customer extends Backend
{

    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();

        $this->model = model('admin/shop/Serverorder');
    }
    /**
     * 查看
     */
    public function index($ids = 0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $query="";

            $query.=" length(server.sfuserid) <= 0 AND";

            $query.=" server.create_time+".config('Customer.send')." >= ".time()." AND";

            $query.=" order.schedule not in(4,7,9)";

            $query .=' AND order.create_time > 1558631041';

            if($ids){
                $query .= " AND order.id = $ids";
            }
            //城市合伙人条件
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';

            if($type == 2 && $city_code){
                $query .= " AND order.city_code = $city_code";
            }

            $total = $this->model
                 ->with(['order','code','card'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)
                 ->order($sort, $order)
                 ->count();

            $list = $this->model
                 ->with(['order.paylog','code','card','user','temp'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)
                 ->order($sort, $order)
                 ->limit($offset, $limit)
                 ->select();
            $list = collection($list)->toArray();
            
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch('', ['ids' => $ids]);
    }

    /**
     * 人工处理
     * @Author   ksea
     * @DateTime 2019-05-16T14:47:32+0800
     * @param    integer                  $ids [description]
     * @return   [type]                        [description]
     */
    public function deal($ids=0){

        if($this->request->isAjax()){
            $ret=$this->request->param('row/a');
            if(!is_null($ret['sfuserid'])&&!empty($ret['sfuserid'])){
                $ret['push_time']=time();
                $ret['admin_id'] =session('admin.id');
                $sd=$this->model->with(['emp','order'])->where('id',$ids)->find();

                if(is_null($sd['sfuserid'])||empty($sd['sfuserid'])){
                    $ret['is_ask']=0;
                }

                /*************************校验当前是否已经绑定人员，已绑定而且不是当前传入，解绑虚拟号**********************************/
                $nowData=$this->model->where('id',$ids)->find();
                if(config('Fictitious.mobile_status')>0){
                    if($nowData['sfuserid']  != $ret['sfuserid']){
                        if($nowData['mobile']>0){
                            /**************释放虚拟号******************/

                            $Fphone=$nowData->mobile;
                            $phone = new \wechat\Fictitious();
                            $Fret=$phone->Untying($Fphone);
                            if($Fret['resultcode'] == 0){
                                Db::name('rivacy_phone')->where('phone',$Fphone)->update(['Binding_time'=>0]);
                                //释放暂存
                                setFictitious($Fphone,$ServerData['id']);

                            }

                            /**************释放虚拟号******************/
                        }
                        $phone = new \wechat\Fictitious();
                        $mobile = Db::name('employee')->where('id',$ret['sfuserid'])->value('phone');
                        $userphone=$sd['order']['mobile'];
                        
                        $rivacy_phone = getFictitious();//获取有效虚拟号
                        if(is_array($rivacy_phone)){
                            if(!is_null($mobile) && !is_null($rivacy_phone)) {
                                $phone_resul = $phone->createphone($rivacy_phone['phone'], $mobile, $userphone);
                                if( $phone_resul['resultdesc'] == 'Success') {
                                    Db::name('rivacy_phone')->where('id', $rivacy_phone['id'])->update(['Binding_time' => time()]);
                                    Db::name('rivacy_phone_record')->insert(['callerNum'=>$mobile,'calleeNum'=>$userphone,'relationNum'=>$rivacy_phone['phone'],'Binding_time' => time(),'subscriptionId'=>$phone_resul['subscriptionId']]);
                                }
                            }
                            $ret['mobile']=$rivacy_phone['phone'];
                            $ret['mobile_status']=config('Fictitious.mobile_status');

                        }
                        else{

                            myErrorLog('rivacy.txt',[$rivacy_phone],'fictitious');
                            $ret['mobile']='0';
                            $ret['mobile_status']=0;

                        }
                    }
                }
                /***************************校验当前是否已经绑定人员，已绑定而且不是当前传入，解绑虚拟号********************************/
                $OrderData=model('admin/shop/order')->find($sd['orderid']);
                if(in_array($OrderData['schedule'], [0,1])){
                    model('admin/shop/order')->update(['id'=>$sd['orderid'],'schedule'=>'2']);
                }
            
            }else{
                $this->error('至少指派一个服务人员');
            }

            $dat=$this->model->allowField(true)->save($ret,['id'=>$ids]);

            if(!is_null($ret['sfuserid'])&&!empty($ret['sfuserid'])){
                $sd=$this->model->with(['emp','order'])->where('id',$ids)->find();
                $upServer=$sd->toArray();
                /**************短信通知这些二货*****************/
                $snorder   = $upServer['order']['snorder'];

                $userphone = $upServer['order']['mobile'];//用户手机号码

                $pht=new \aliyun\SendTemp();
                foreach ($upServer['emp'] as $key => $value) {
                    $pht::order($value['phone'],$snorder);
                    /************扭转记录********************/
                    model('admin/shop/ShopOrderHistory')::create([
                       'oid'=>$sd['orderid'],
                       'desc' => session('admin.nickname').' 派单给'.$value['name'],
                    ]);
                    /************扭转记录********************/
                }


                /**************短信通知这些二货*****************/

            }

            $this->success('ok','/',$dat);
        }

        $query='';

        $query.=" server.id = $ids";

        $list = $this->model
             ->with(['order.paylog','code','card','admin','emp'])
             ->alias('server')
             ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
             ->join('fa_shop_order order','server.orderid = order.id','LEFT')
             ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
             ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
             ->join('fa_region region','region.region_id = server.areaid','LEFT')
             ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num,order.schedule')
             ->where($query)
             ->find();

	    $this->assign('date', model('admin/shop/OrderTemp')::get(['orderid'=> $list['orderid']]));
        return $this->view->fetch('', ['list'=>$list]);
    }

    public function complete($ids = 0)
    {
        $row = $this->model->get($ids);
        if($row){
            model('admin/shop/order')::get($row['orderid'])
                ->save(['schedule'=>6]);
            model('admin/shop/ShopOrderHistory')::create([
                'oid'=>$row['orderid'],
                'desc' => '客服[' . session('admin.nickname') . '] 更新订单为完成',
            ]);
	        model('admin/shop/ShopOutcome')->avail(['oid'=>$row['orderid']]);

            $data = ['status'=> 4, 'is_ask'=> 1 , 'admin_id'=>session('admin.id')];
            empty($row['sfuserid']) && $data['sfuserid'] = 58;
            empty($row['appoint_start']) && $data['appoint_start'] = $_SERVER['REQUEST_TIME'];
            empty($row['fu_start']) && $data['fu_start'] = $_SERVER['REQUEST_TIME'];
            empty($row['fu_end']) && $data['fu_end'] = $_SERVER['REQUEST_TIME'];
            if($row['mobile']>0){
                /**************释放虚拟号******************/
                if(config('Fictitious.mobile_status')>0){
                    $Fphone=$row->mobile;
                    $phone = new \wechat\Fictitious();
                    $Fret=$phone->Untying($Fphone);
                    if($Fret['resultcode'] == 0){
                        Db::name('rivacy_phone')->where('phone',$Fphone)->update(['Binding_time'=>0]);
                        model('admin/shop/Serverorder')->where('id',$row['id'])->update(['mobile_status'=>2]);
                    }
                }
                /**************释放虚拟号******************/
            }
            $row->save($data) && $this->success();
        }
        $this->error();
    }

}