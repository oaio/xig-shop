<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;

/**
 * 轮播图管理（店铺）
 */

class Banner extends Backend{

	/**
	 * @create_by d3li 12/20/2018
	 */
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('shop.shopIndex');
	}
    /**
     * 添加
     * @Author   baohb 
     * @DateTime 2018-12-8T09:32:52+0800
     */
    public function add_banner(){
        $date['admin_id'] =power_judge();
        if(request()->isPost()){
            $post_data=input('post.');
            $post_data=array_merge($post_data,$date);
            $res=model('admin/shop/banner')->ins_banner($post_data);
            if ($res){
                $this->success('banner图添加成功');
            }else{
                $this->error('banner图添加失败');
            }

        }
        //获取地区数据
        $city_list = Db::table('fa_city')->order('id desc')->column('city_name','city_code');
        $this->assign('city_list', $city_list);
        return $this->fetch();
    }
    /**
     * 显示banner列表
     * @Author   baohb 
     * @DateTime 2018-12-8T09:39:31+0800
     * @return   [type]                   [description]
     */
    public function banner_list(){
        $data[0]=input('param.search','');
        $data[1]=input('param.paginate',20);
        $data[2]=input('param.other/a');
        $data[3]=input('param.orderby','desc');
        $data[4]=true;
        $data[5]=power_judge();

        $res=model('admin/shop/banner')->show_banner_list($data);

        $this->assign('list', $res[0]);

        $this->assign('page', $res[1]);

        if(request()->isAjax()){
            return $this->fetch('shop/goods/ajax_show_list');
        }
        else{
            return $this->fetch();
        }
    }
    /**
     * 获取banner详情
     * @Author   baohb 
     * @DateTime 2018-12-8T09:48:01+0800
     * @return   [type]                   [description]
     */
    public function banner_detail(){
        $id=input('id');
        if($id){
            $res=model('admin/shop/banner')->get_banner($id);

            if (power_judge()==null||$res['admin_id']==power_judge()){
                $this->assign("data",$res);
            }else{
                $this->error('数据不存在','shop.error/error_url');
            }
        }
        else{
            $this->error('请求错误');
        }
        return $this->fetch();
    }
    /**
     * 更新banner图片
     * @Author   baohb 
     * @DateTime 2018-12-8T09:48:28+0800
     * @return   [type]                   [description]
     */
    public function up_banner()
    {
        if (request()->isPost()) {
            $id = input('id');
            $data['img_link'] = input('img_arr');
            $data['pow'] = input('pow');
            $res = Db::name('shop_index')->where(['id' => $id])->update($data);

            if ($res){
                $this->success('更新成功');
            }else{
                $this->error('更新失败');
            }
        }
    }
    /**
     * 更新banner图片
     * @Author   baohb
     * @DateTime 2018-12-20T17:12:28+0800
     * @return   [type]                   [description]
     */
    public function hidden()
    {
        $data['id']=input('id');
        $data['admin_id']=power_judge();
        $res =hidden($data);
        if ($res){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

	public function log()
	{
		$action = $this->request->action();
		$ary = ['add'=>'新增','edit'=>'编辑','del'=>'删除'];
		if ($this->request->isPost() && in_array($action, array_keys($ary))){
			$param=[
				'module_name'=>'广告图管理',
				'op_name'=>$ary[$action] . '广告图',
				'remark'=>json_encode(input()),
			];
			\think\Hook::listen('activelog',$param);
		}
	}
}