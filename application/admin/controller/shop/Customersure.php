<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
use think\Db;
use think\Session;

/**
 * 待师傅确认
 */
class Customersure extends Backend
{

    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();

        $this->model = model('admin/shop/Serverorder');
    }
    /**
     * 查看
     */
    public function index($ids = 0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $query="";
            
            $query.=" is_ask=0 AND";

            $query.=" length(server.sfuserid)>0 AND";

            $query.=" order.schedule not in(4,7,9)";

            $query .=' AND order.create_time > 1558631041';

	        if($ids){
		        $query .= " AND order.id = $ids";
	        }
            //城市合伙人条件
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';

            if($type == 2 && $city_code){
                $query .= " AND order.city_code = $city_code";
            }
            $total = $this->model
                 ->with(['order','code','card','admin','emp'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)
                 ->order($sort, $order)
                 ->count();

            $list = $this->model
                 ->with(['order.paylog','code','card','admin','emp','user'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)                 
                 ->order($sort, $order)
                 ->limit($offset, $limit)
                 ->select();
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch('', ['ids' => $ids]);
    }
    /**
     * 确定时间
     * @Author   ksea
     * @DateTime 2019-06-10T09:44:46+0800
     * @return   [type]                   [description]
     */
    public function suretime($ids = 0){
        $model = model('admin/shop/Serverorder');
        $model = $model::get($ids);
        if($this->request->isPost()) {
            $data = $this->request->param('row/a');
            $data['is_ask']=1;
            $data['appoint_start']=strtotime($data['appoint_start']);
            $res = $model->where('id',$ids)->update($data);
            if ($res){
                $uptime=$data['appoint_start'];
                /***************2019-05-16**********************/
                $orderid=is_get_val('shop_service_order',$ids,'orderid');
                Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>3]);
                /***************2019-05-16**********************/
                /************扭转记录********************/
                model('admin/shop/ShopOrderHistory')::create([
                   'oid'=>$orderid,
                   'desc' => '客服【'.session('admin.name').'】 预约时间：'.$uptime,
                ]);
                /************扭转记录********************/
                $this->success();
            }
            $this->error();
        }
        return $this->view->fetch();
    }
}