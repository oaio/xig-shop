<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
use think\Session;

/**
 * 待客服分配分配
 */
class Customerassign extends Backend
{

    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();

        $this->model = model('admin/shop/Serverorder');
    }
    /**
     * 查看 -  等待客服指派
     */
    public function index($ids = 0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $query="";

            $query.="server.create_time+".config('Customer.send')." < ".time().' AND';

            $query.=" length(server.sfuserid)<=0 AND";

            $query.=" order.schedule not in(4,7,9)";

            $query .=' AND order.create_time > 1558631041';

	        if($ids){
		        $query .= " AND order.id = $ids";
	        }
            //城市合伙人条件
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';

            if($type == 2 && $city_code){
                $query .= " AND order.city_code = $city_code";
            }

            $total = $this->model
                 ->with(['order','code','card','admin','emp'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)
                 ->order($sort, $order)
                 ->count();

            $list = $this->model
                 ->with(['order.paylog','code','card','admin','emp','user','temp'])
                 ->alias('server')
	            ->join('fa_shop_userinfo `user`','server.uid = `user`.id','LEFT')
                 ->join('fa_shop_goods goods','server.goodsid = goods.id','LEFT')
                 ->join('fa_shop_order order','server.orderid = order.id','LEFT')
                 ->join('fa_shop_categrory cate','goods.cate = cate.id','LEFT')
                 ->join('fa_virdiscount discount','discount.id = order.discount_id','LEFT')
                 ->join('fa_region region','region.region_id = server.areaid','LEFT')
                 ->field('server.*,cate.name as cate_name,cate.id as cate_id,goods.cate,goods_msg,discount.bean,discount.dismoney,order.goods_name,order.shouldpay,order.actualpay,order.mobile,region.region_name,region.region_id,order.snorder,order.nickname,order.num')
                 ->where($where)
                 ->where($query)                 
                 ->order($sort, $order)
                 ->limit($offset, $limit)
                 ->select();
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch('', ['ids' => $ids]);
    }
}