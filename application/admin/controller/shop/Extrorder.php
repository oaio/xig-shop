<?php

namespace app\admin\controller\shop;

use app\admin\library\Auth;
use app\common\controller\Backend;
use think\Db;

class Extrorder extends Backend
{

	protected $model = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->model = model('admin/shop/ExtrOrder');
	}

	public function setting()
	{
		$row = model('config')::get(['name'=>'source']);
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params['value']) {
				$ary = [];
				foreach ($params['value'] as $v)
					$ary[] = $v;
				$row->value = json_encode($ary, JSON_UNESCAPED_UNICODE);
				if ($this->modelValidate) {
					$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
					$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
					$row->validate($validate);
				}
				$result = $row->allowField(true)->save();
				if ($result !== false) {
					$this->success();
				} else {
					$this->error($row->getError());
				}
			}
		}
		return $this->view->fetch('', ['time'=>$row['value']]);
	}

	/**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with(['goods','admin'])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
            	->with(['goods','admin'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

	public function add()
	{
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				$params['admin_id']=session('admin.id');
				$params['time']	   =strtotime($params['time']);
				$params['region']  ='440000,440300,'.$params['area'];
				if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
					$params[$this->dataLimitField] = $this->auth->id;
				}
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
						$this->model->validate($validate);
					}
					$result = $this->model->allowField(true)->save($params);
					if ($result !== false) {
						$row = model('admin/shop/Goods')::get($params['goodsid']);
						(new \aliyun\SendTemp())::SendDiy($params['mobile'],
							'SMS_172010845', ['results'=>$row['name']]);
						$this->success();
					} else {
						$this->error($this->model->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		return $this->view->fetch();
	}

	public function edit($ids = 0)
	{
		$row = $this->model->with('goods')->find($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			$params['admin_id']=session('admin.id');
			if ($params) {
				$params['time']	   =strtotime($params['time']);
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$result = $row->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($row->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign('row', $row);
		return $this->view->fetch();
	}
	/**
	 * extr特殊数据修改
	 * @Author   ksea
	 * @DateTime 2019-06-24T16:38:55+0800
	 * @param    integer                  $ids [description]
	 * @return   [type]                        [description]
	 */
	public function extr($ids = 0)
	{
		$row = $this->model->with('goods')->find($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			$params['admin_id']=session('admin.id');
			if ($params) {
				isset($params['time'])?$params['time']=strtotime($params['time']):'';
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$result = $row->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($row->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign('row', $row);
		return $this->view->fetch();
	}
	/**
	 * 查看单挑数据
	 * @Author   ksea
	 * @DateTime 2019-06-24T16:39:31+0800
	 * @param    integer                  $ids [description]
	 * @return   [type]                        [description]
	 */
	public function show($ids = 0)
	{
		$row = $this->model->with('goods')->find($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$result = $row->allowField(true)->save($params);
					if ($result !== false) {
						$this->success();
					} else {
						$this->error($row->getError());
					}
				} catch (\think\exception\PDOException $e) {
					$this->error($e->getMessage());
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign('row', $row);
		return $this->view->fetch();
	}
	public function plat()
	{
		$conf = model('config')->get(['name'=>'source']);
		$data = [];
		foreach (json_decode($conf->value) as  $k=>$v)
		{
			if($k<3)continue;
			$data[] = ['name'=>$v, 'value'=>$k];
		}
		$this->success('','',$data);
	}

	/**
	 * 区域
	 * @Author   ksea
	 * @DateTime 2019-06-10T14:46:57+0800
	 * @return   [type]                   [description]
	 */
	public function area(){
		$res=model('admin/Region')->where('region_parent_id=440300')->select();
		$ret=[];
		foreach ($res as $key => $value) {
			$ret[$key]['name']  = $value->region_name;
			$ret[$key]['value'] = $value->region_id;
		}
		$this->success('','',$ret);
	}

	/**
	 * 订单关联拓展数据
	 * @Author   ksea
	 * @DateTime 2019-06-24T09:14:15+0800
	 * @return   [type]                   [description]
	 */
	public function porgin(){
		$res=model('config')->get(['name'=>'source']);
		$source=json_decode($res['value']);

		$res1=$this->model->alias('extr')->join('fa_admin admin','extr.admin_id=admin.id','LEFT')->field('admin.*')->select();
		$admin=[];
		foreach ($res1 as $key => $value) {

			if($value['id'])$admin[$value['id']]=$value['nickname'];
		}
		$ret=['source'=>$source,'admin'=>$admin];
		return json($ret);
	}
	/**
	 * 创建数据
	 * @Author   ksea
	 * @DateTime 2019-06-11T21:31:58+0800
	 * @param    string                   $ids [description]
	 * @return   [type]                        [description]
	 */
	public function cdata($ids='0'){

		  $region=model('admin/region');
		  $user=model('admin/shop/user');
		  $AddressModel=model('admin/shop/address');
		  $extrOrder=$this->model::get($ids);
		  if($extrOrder['status']==1)$this->error('每个外部订单只能下单一次');
		  $userData=$user->with(['addressData'])->where('phone',$extrOrder['mobile'])->find();
		  $regionArea=$region->where('region_id',$extrOrder['area'])->find();

		  $province_id='440000';
		  $province='广东省';
		  $city_id='440300';
		  $city='深圳市';
		  $area_id=$regionArea->region_id;
		  $area=$regionArea->region_name;
		  $address=$extrOrder->address;


		  /******************用户相关判断**************************/
		  if(empty($userData)){
		  	 $udata=[
		  	 	'nickname'   =>$extrOrder['nickname'],
		  	 	'name'       =>$extrOrder['nickname'],
		  	 	'passwd'     =>md5('123456'),
		  	 	'phone'      =>$extrOrder['mobile'],
		  	 	'sn_info'    =>"US".create_no(),
		  	 	'province_id'=>$province_id,
		  	 	'province'   =>$province,
		  	 	'city_id'    =>$city_id,
		  	 	'city'       =>$city,
		  	 	'area_id'    =>$area_id,
		  	 	'area'		 =>$area,
		  	 	'address'	 =>$address,
		  	 ];
		  	 $userData=$user::create($udata);
		  	 $id_link="0,".$userData['id'];

			 $user->save([
			 'id_link'=>$id_link
			 ],['id' => $userData['id']]);

		  	 /*********创建默认地址************/
		  	 // $Adata=[
		  	 // 	'name'      =>$extrOrder['nickname'],
		  	 // 	'phone'     =>$extrOrder['mobile'],
		  	 // 	'address'   =>$address,
		  	 // 	'uid'       =>$userData['id'],
		  	 // 	'region'    =>$extrOrder['region'],
		  	 // 	'is_default'=>1,
		  	 // ];
		  	 // $addressAata=$AddressModel::create($Adata);
		  	 /*********创建默认地址************/
		  	 //$userData=$user->where('id',$userData['id'])->with(['addressData'])->find();
		  }
		  	 /*********创建默认地址************/
	  	  $Adata=[
	  	 	'name'      =>$extrOrder['nickname'],
	  	 	'phone'     =>$extrOrder['mobile'],
	  	 	'address'   =>$address,
	  	 	'uid'       =>$userData['id'],
	  	 	'region'    =>$extrOrder['region'],
	  	 	'is_default'=>0,
	  	  ];
	  	  $addressAata=$AddressModel::create($Adata);
	  	  /*********创建默认地址************/
	  	  $userData=$user->where('id',$userData['id'])->with(['addressData'])->find();
	  	  
		  $ArrUdata=$userData->toArray();
		  /******************用户相关判断**************************/

		  /*****************订单相关判断**************************/
	      $url = request()->domain()."/mapi/order/rest";
          $post_data = [
            'num'       =>$extrOrder['num'],
            'bak'       =>$extrOrder['bak'],
            'goodsid'   =>$extrOrder['goodsid'],
            'uid'		=>$ArrUdata['id'],
            'store_id'  =>$extrOrder['store_id'],
            'cate_id'   =>$extrOrder['cate'],
            'address_id'=>$ArrUdata['address_data'][count($ArrUdata['address_data'])-1]['id'],
            'appoint_start'=>$extrOrder['time'],
            'extr'		=>$ids,
            'orgin'		=>$extrOrder['orgin'],
            'sfuserid'  =>$extrOrder['sfuserid']
          ];
		  $corder=curlpost($url,$post_data);
		  if($corder['code']==0)$this->error('下单失败','',$corder);
		  /*****************订单相关判断**************************/
		  
		  /*****************付款相关判断**************************/
		  $payUrl=request()->domain()."/mapiv2/commonapi/Comapi/zeropay";
		  $payUrlData=[
		  	'oid'=>$corder['data']['data']['data']['id']
		  ];
		  $cpay=curlpost($payUrl,$payUrlData);
		  if($cpay['code']==0)$this->error('支付失败','',$cpay);
		  /*****************付款相关判断**************************/

		  $this->model->save(['status'=>1],['id'=>$ids]);

		  $this->success('外部订单录入成功');
	}
}