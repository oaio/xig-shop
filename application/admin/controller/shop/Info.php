<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Session;

/**
 * Info.php 店铺信息管理
 *
 * @author d3li <d3li@sina.com>
 * @create：12/22/2018  4:22 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1222
 * @describe
 */
class Info extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\shop\Store $model
	 */
	protected $model = null;


	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('shop.Store');
	}

	public function index()
	{
		$id = Session::get('admin.id');
		if ($this->request->isPost()) {
			$row = input();
			$row = $row['row'];
//			$row['telphone'] = implode('|', $row['tel']);
//			unset($row['tel']);
			$this->model->where('admin_id', $id)->update($row);
			$param=[
				'module_name'=>'店铺管理',
				'op_name'=>'更新店铺信息',
				'remark'=>json_encode($row, JSON_UNESCAPED_UNICODE),
			];
			\think\Hook::listen('activelog',$param);
			$this->success('保存成功');
		}
		$res = $this->model->where('admin_id', $id);
		/** @var TYPE_NAME $res */
		$data = $res->find();
		if (empty($data)) {
			//throw new \think\exception\HttpException(403, '信息不存在');

			$this->error('店铺不存在','shop.error/error_url',0, 0);
		}
//		$temp = explode('|', $data->telphone);
//		$data->tel = $temp ? $temp[0] : '';
//		$data->telphone = isset($temp[1]) ? $temp[1] : '';
		$this->assign('row', $data);
		return $this->fetch();
	}

	public function qrcode()
	{
		$id = Session::get('admin.id');
		$s = $this->model->where('admin_id', $id)->find();
		if(isset($s['store_code'])) {
			$file = ROOT_PATH .'public'. $s['store_code'];
			$fp = fopen($file, "r");
			$data = fread($fp, 4096);
			fclose($fp);
			header('Content-Disposition: attachment;filename=' . basename($file));
			header('Content-type: application/octet-stream');
			ob_clean();
			flush();
			exit($data);
		}else{
			return '';
		}

	}
}