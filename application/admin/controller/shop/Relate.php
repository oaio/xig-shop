<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\shop;

use app\common\controller\Partner;
use think\Session;

/**
 * Relate.php 客诉管理
 *
 * @author d3li <d3li@sina.com>
 * @create：17/05/2019  9:31 AM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.17
 * @describe
 */
class Relate extends Partner
{
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/Order');
	}

	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //城市合伙人条件
            $admin_info = Session::get('admin');
            $city_code = isset($admin_info['city_code'])?$admin_info['city_code']:'';
            $type = isset($admin_info['type'])?$admin_info['type']:'';
			$total = $this->model
				->alias('o')
				->field('o.*,r.region_name')
				->join('fa_shop_service_order s','s.orderid=o.id', 'RIGHT')
				->join('fa_region r','r.region_id=s.areaid', 'LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
				->where('is_pay', 1)
				->where('s.mark', '<>', '')
				->where($where)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->count();

			$list = $this->model
				->alias('o')->with('paylog','user')
				->field('o.*,s.mark,r.region_name')
				->join('fa_shop_service_order s','s.orderid=o.id', 'RIGHT')
				->join('fa_region r','r.region_id=s.areaid', 'LEFT')
				->join('fa_shop_userinfo `user`', '`user`.id=o.uid', 'LEFT')
				->where('is_pay', 1)
				->where('s.mark', '<>', '')
				->where($where)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}

	public function edit($ids = 0)
	{
		$row = $this->model->get($ids);
		return $this->view->fetch('', ['row'=>$row]);
	}
}