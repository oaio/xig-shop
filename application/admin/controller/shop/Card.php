<?php
namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;


class Card extends Backend{
 	private $use_model;
 	public function _initialize(){
        parent::_initialize(); 
        $this->use_model=model('admin/shop/Card');
    }
	/**
	 * 显示某人预约卡片，或者全部人的全部卡片
	 * @Author   ksea
	 * @DateTime 2018-11-19T09:55:29+0800
	 * @return   [type]                   [description]
	 */
	public function card(){
		$uid = input('uid','');
		$status = input('status','');
		if(empty($uid)){
			$res=$this->use_model->mycard();
		}
		else{
			if($status){
				$res=$this->use_model->mycard($id,$status);
			}
			else{
				$res=$this->use_model->mycard($id);
			}
		}
		$this->assign('list',$res[0]);
		$this->assign('page',$res[1]);
		return $this->fetch();
	}
	/**
	 * 更新操作或者跳转更新页面
	 * @Author   ksea
	 * @DateTime 2018-11-19T10:25:54+0800
	 * @return   [type]                   [description]
	 */
	public function upcard(){
		if(request()->isPost()){
			$post_data=input('post.');
			if(!empty($post_data['id'])){
				$res=$this->use_model->upcard($post_data);
				if($res['code']==1){
					$this->success('更新成功');
					exit();
				}
				else{
					$this->error('更新错误','',$res);
					exit();
				}
			}
			else{
				return false;
			}
		}
		else{
		    $res=$this->use_model->getone(input('id'));
		    $this->assign('data',$res);
		    return $this->fetch('shop/Card/getone');
		    exit();
		}
	}
	/**
	 * 删除卡片
	 * @Author   ksea
	 * @DateTime 2018-11-19T10:26:55+0800
	 * @return   [type]                   [description]
	 */
	public  function delcard(){
		if(input('id')){
			$res=$this->use_model->delcard(input('id'));
			if($res['code']==1){
				$this->success('删除成功');
				exit();
			}
			else{
				$this->error('删除错误','',$res);
				exit();
			}
		}
		else{
			$this->error('操作错误');
		}
	}
}