<?php

namespace app\admin\controller\shop;

use app\common\controller\Backend;
use think\Db;

/**
 * 数据分析，数据统计
 */
class Analysis extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\ShopOrder $model
	 */
	protected $model;


	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/order');
	}
	/**
	 * 平台城市代码
	 * @Author   ksea
	 * @DateTime 2019-07-31T19:35:20+0800
	 * @return   [type]                   [description]
	 */
	public function ptcity(){
		$data=[];
		$conf = model('config')->get(['name'=>'source']);
		$data = [];
		foreach (json_decode($conf->value) as  $k=>$v)
		{
			if($k<3)continue;
			$data_temp[] = ['name'=>$v, 'value'=>$k];
		}
		$data['pt']=$data_temp;
		$data['city']=Db::name('city')->field('city_name as name , city_code as value')->select();
		return json($data);
	}
	/**
	 * 综合详情
	 * @Author   ksea
	 * @DateTime 2019-07-31T11:39:09+0800
	 * @return   [type]                   [description]
	 */
	public function mixanalysis(){
		if ($this->request->isAjax()) {
			$model=model('admin/shop/AnalysisMix');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model
					->where($where)
					->group('create_time')
					->count();
			$list = $model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->group('create_time')
					->field("id,SUM(loginin) as loginin,SUM(reviewuser) as reviewuser,SUM(payonline) as payonline,SUM(paystorenum) as paystorenum,SUM(payonlinestorenum) as payonlinestorenum,create_time")
					->select();
			$extend=$model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->field("SUM(loginin) as loginin,SUM(reviewuser) as reviewuser,SUM(payonline) as payonline,SUM(paystorenum) as paystorenum,SUM(payonlinestorenum) as payonlinestorenum,create_time")
					->find()->toArray();
			foreach ($extend as $key => $value) {
				if(!isset($value)){
					$extend[$key]=0;
				}else{
					$extend[$key] = (string)$value;
				}
			}
			$result = array('total' => $total, 'rows' => $list ,'extend' =>$extend);
			return json($result);
		}
	}
	/**
	 * 用户购买详情
	 * @Author   ksea
	 * @DateTime 2019-07-31T11:33:47+0800
	 * @return   [type]                   [description]
	 */
	public function buyanalysis(){
		if ($this->request->isAjax()) {
			$model=model('admin/shop/AnalysisBuy');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model
					->with(['user'])
					->where($where)
					->group('uid')
					->count();
			$list = $model
					->with(['user'])
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->group('uid')
					->field("id,uid,SUM(userpaynum) as userpaynum,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum,SUM(getbean) as getbean")
					->select();
			$extend=$model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->field("uid,SUM(userpaynum) as userpaynum,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum,SUM(getbean) as getbean")
					->find()->toArray();
			foreach ($extend as $key => $value) {
				if(!isset($value)){
					$extend[$key] = 0;
				}else{
					if($key!='thatbean'){
						$extend[$key] = (string)$value;
					}
				}
			}
			$result = array('total' => $total, 'rows' => $list ,'extend' =>$extend);

			return json($result);
		}
	}
	/**
	 * 店铺支付详情
	 * @Author   ksea
	 * @DateTime 2019-07-31T11:33:47+0800
	 * @return   [type]                   [description]
	 */
	public function storeanalysis(){
		if ($this->request->isAjax()) {
			$model=model('admin/shop/AnalysisStore');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model
					->with(['store'])
					->where($where)
					->group('store_id')
					->count();
			$list = $model
					->with(['store'])
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->group('store_id')
					->field("id,store_id,SUM(userpaynum) as userpaynum,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum,SUM(paynum) as paynum")
					->select();

			$extend=$model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->field("id,store_id,SUM(userpaynum) as userpaynum,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum,SUM(paynum) as paynum")
					->find()->toArray();
			foreach ($extend as $key => $value) {
				if(!isset($value)){
					$extend[$key]=0;
				}else{
					$extend[$key] = (string)$value;
				}
			}
			$result = array('total' => $total, 'rows' => $list ,'extend' =>$extend);

			return json($result);
		}
	}

	/**
	 * 推荐人统计
	 * @Author   ksea
	 * @DateTime 2019-07-31T11:33:47+0800
	 * @return   [type]                   [description]
	 */
	public function tjanalysis(){
		if ($this->request->isAjax()) {
			$model=model('admin/shop/AnalysisTj');
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $model
					->where($where)
					->group('tj')
					->count();
			$list = $model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->group('tj')
					->field("id,tj,tjname,SUM(tjstorecount) as tjstorecount,SUM(payusercount) as payusercount,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum")
					->select();

			$extend=$model
					->where($where)
					->order($sort, $order)
					->limit($offset, $limit)
					->field("id,tj,tjname,SUM(tjstorecount) as tjstorecount,SUM(payusercount) as payusercount,SUM(suserpaysum) as suserpaysum,SUM(auserpaysum) as auserpaysum,SUM(discountsum) as discountsum")
					->find()->toArray();
			foreach ($extend as $key => $value) {
				if(!isset($value)){
					$extend[$key]=0;
				}else{
					$extend[$key] = (string)$value;
				}
			}
			$result = array('total' => $total, 'rows' => $list ,'extend' =>$extend);

			return json($result);
		}
	}


}