<?php
namespace app\admin\controller\store;

use app\common\controller\Backend;
use think\Db;
use think\Request;
use think\Session;


/**
 * 不用ajax
 */

class Store extends Backend{

    /**
     * 更新操作或者跳转更新页面
     * @Author   baohb
     * @DateTime 2018-12-20T16:11:54+0800
     * @return   [type]                   [description]
     */
    public function up_shop(){

        $date['update_time']= strtotime(date('Y-m-d h:i:s', time()));
        if(request()->isPost()){
            $post_data=input('post.');
            $post_data=array_merge($post_data,$date);
            if(!empty($post_data['id'])){
                $res=Db::name('store')->where(['admin_id'=>power_judge()])->update($post_data);
                if($res['code']==1){
                    $this->success('店铺信息更新成功');
                    exit();
                }
                else{
                    $this->error('店铺更新失败','',$res);
                    exit();
                }
            }
            else{
                return false;
            }
        }
        else{
            $res=Db::name('store')->where(['admin_id'=>power_judge()])->find();
            if ($res){
                $this->assign('data',$res);
                return $this->fetch();
            }else{
                $this->error('该加盟商还未添加店铺！','shop.error/error_url');
            }
        }
    }
}  