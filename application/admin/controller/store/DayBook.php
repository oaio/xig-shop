<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\store;

use app\common\controller\Backend;
/**
 * DayBook.php
 *
 * @icon fa fa-line-chart
 * @author d3li <d3li@sina.com>
 * @create：2019/5/27  10:02
 * @see      https://gitee.com/d3li/
 * @version 2.05.27
 * @describe
 */
class DayBook extends Backend
{
	protected $searchFields = ['store.ruler','store.ruler_phone','store.nickname'];

	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('admin/shop/order');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //城市合伙人条件
            $city_code = session('admin.city_code')?session('admin.city_code'):'';
            $type = session('admin.type')?session('admin.type'):'1';
            $where2 = '';
			if($type == 2 && $city_code){
				$where2 = '`store`.city_id = '.$city_code.'';
			}

			$total = $this->model
				->alias('order')
				->field('`order`.*')
				->join('fa_virdiscount discount','`order`.`discount_id`=`discount`.`id`','LEFT')
				->join('fa_store store','`store`.`store_id`=`order`.`store_id`','LEFT')
//				->join('fa_shop_userinfo storeuserinfo','`storeuserinfo`.`store_bind`=`order`.`store_id`','LEFT')
				->where($where)
                ->where($where2)
				->where('`order`.is_pay = 1')
				->group('`order`.store_id')
				->count();

			$list = $this->model
				->alias('order')
				->with('store.cash,storeuserinfo')
				->field('`order`.*,ROUND(SUM(`order`.`actualpay`+`discount`.`dismoney`),2) AS store_total,
				ROUND(SUM(`order`.`actualpay`),2) AS pay,ROUND(SUM(`discount`.`dismoney`),2) AS coupon,
				COUNT(*) AS store_count')
				->join('fa_virdiscount discount','`order`.`discount_id`=`discount`.`id`','LEFT')
				->join('fa_store store','`store`.`store_id`=`order`.`store_id`','LEFT')
//				->join('fa_shop_userinfo storeuserinfo','`storeuserinfo`.`store_bind`=`order`.`store_id`','LEFT')
				->where($where)
                ->where($where2)
				->where('`order`.is_pay = 1')
				->group('`order`.store_id')
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->view->fetch();
	}


	/**
	 * 详情
	 *
	 * @param int $ids
	 * @return mixed|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail($ids = 0)
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->alias('order')
				->join('fa_virdiscount discount','`discount`.`id`=`order`.`discount_id`','LEFT')
				->where($where)
				->where('store_id', $ids)
				->where('`order`.is_pay = 1')
				->order($sort, $order)
				->count();

			$list = $this->model
				->alias('order')
				->field('`order`.*,`discount`.`dismoney`')
				->join('fa_virdiscount discount','`discount`.`id`=`order`.`discount_id`','LEFT')
				->where($where)
				->where('store_id', $ids)
				->where('`order`.is_pay = 1')
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->fetch('', ['ids' => $ids]);
	}
}