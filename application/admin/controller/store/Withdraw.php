<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\store;
use app\common\controller\Backend;

/**
 * Withdraw.php
 *
 * @author d3li <d3li@sina.com>
 * @create：2019/7/7  14:56
 * @see      https://gitee.com/d3li/
 * @version 2.07.07
 * @describe
 */
class Withdraw extends Backend
{
	protected $searchFields = ['w.uid', 's.ruler', 'u.phone'];
	public function _initialize()
	{
		parent::_initialize(); // TODO: Change the autogenerated stub
		$this->model = model('Withdraw');
	}

	/**
	 * 列表
	 *
	 * @return string|\think\response\Json
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function index()
	{
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model->alias('w')
				->join('fa_shop_userinfo u', 'u.id=w.uid', 'LEFT')
				->join('fa_store s', 's.store_id=u.store_bind', 'LEFT')
				->where($where)
				->count();

			$list = $this->model
				->alias('w')->field('w.*')->with('w,u.s')
				->join('fa_shop_userinfo u', 'u.id=w.uid', 'LEFT')
				->join('fa_store s', 's.store_id=u.store_bind', 'LEFT')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();

			$list = collection($list)->toArray();
			$result = array("total" => $total, "rows" => $list,
				'extend' => $this->model->alias('w')
					->join('fa_shop_userinfo u', 'u.id=w.uid', 'LEFT')
					->join('fa_store s', 's.store_id=u.store_bind', 'LEFT')
					->where($where)->sum('money'));

			return json($result);
		}
		return $this->view->fetch();
	}


}