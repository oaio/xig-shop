<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\store;

use app\admin\model\Admin;
use app\common\controller\Backend;
use fast\Random;
/**
 * Index.php 密码重置管理
 *
 * @author d3li <d3li@sina.com>
 * @create：1/25/2019  9:15 AM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0125
 * @describe
 */
class Index extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\Store $model
	 */
	protected $model = null;
	protected $searchFields = ['nickname', 'ruler', 'ruler_phone'];

	public function _initialize()
	{
		parent::_initialize();
		$controllername = strtolower($this->request->controller());
		$actionname = strtolower($this->request->action());

		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		// 判断控制器和方法判断是否有对应权限
		if (!$this->auth->check($path)) {
			$this->redirect('shop.error/error_url');
		}
		$this->model = model('Store');
	}

	/**
	 * 重置密码
	 *
	 * @param int $ids
	 * @throws \think\exception\DbException
	 */
	public function pass($ids = 0)
	{
		$password = $this->request->post("password");
		$admin = Admin::get(intval($ids));
		if(!$admin){
			$this->error('该店铺登录信息不存在！');
		}
		if($password) {
			$salt = Random::alnum();
			$admin->salt = $salt;
			$admin->password = md5(md5($password) . $salt);
			$admin->save();
			$param=[
				'module_name'=>'全部店铺',
				'op_name'=>'密码重置',
				'remark'=>json_encode(['admin_id'=>$this->auth->id,'pwd'=>$password]),
			];
			\think\Hook::listen('activelog',$param);
			$this->success('密码重置成功');
		}
	}
}