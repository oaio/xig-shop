<?php

/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */

namespace app\admin\controller\store;
use app\common\controller\Backend;
use app\common\model\WechatInvite;
use wechat\Api;

/**
 * Platform.php 推荐平台管理
 *
 *@icon fa fa-twitter-square
 * @author d3li <d3li@sina.com>
 * @create：05/05/2019  2:18 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.05
 * @describe
 */
class Platform extends Backend
{
	public function _initialize()
	{
		parent::_initialize();
		$this->model = model('Platform');
	}

	/**
	 * 同步邀请数
	 *
	 * @author d3li 05/05/2019
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function update()
	{
		$model = new WechatInvite();
		$res = $this->model->select();
		foreach ($res as $v){
			$v->invite = $model
				->where(['uid'=>$v->id, 'source'=> 'platform'])
				->group('openid')
				->count();
			$v->save();
		}
		$this->success();
	}

	/**
	 * 查看二维码
	 *
	 * @author d3li 05/05/2019
	 * @param $ids
	 */
	public function qrcode($ids)
	{
		$res = Api::getInstance()->qrcode("platform|$ids", $ids, 'qrcode/platform');
		if(is_array($res)) dump($res);
		else echo '<img src="', $res, '">';
		exit;
	}
}