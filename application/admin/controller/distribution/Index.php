<?php

namespace app\admin\controller\distribution;

use app\common\controller\Backend;
use think\Config;
use wechat\Api;

class Index extends Backend
{

	public function index()
	{
		if($this->request->isAjax()) {
			$params = $this->request->post();
			trace("Index");
			trace($params);
			if ($params) {
			    if(empty($params['min']) || empty($params['max'])){
                    $this->error('数值为空');
                }
			    if(!is_numeric($params['min']) || !is_numeric($params['max'])){
                    $this->error('请输入数字');
                }
			    if($params['min'] > $params['max']){
                    $this->error('数值大小不符合');
                }
			    $this->refreshFile($params, 'cBeans');
			}
			$this->success();
			return json(['success']);
		}
		$conf = Config::get('cBeans');
		$this->assign('conf', $conf);
		return $this->fetch();
	}
	/**
	 * 刷新配置文件
	 *
	 * @param $config
	 * @param string $name
	 */
	protected function refreshFile($config, $name = 'reply')
	{
		file_put_contents(APP_PATH . 'extra' . DS . $name . '.php',
			'<?php' . "\n\nreturn " . var_export($config, true) . ";");
	}
}