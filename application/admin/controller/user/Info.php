<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\user;

use app\common\controller\Partner;
/**
 * Info.php 用户管理
 *
 * @author d3li <d3li@sina.com>
 * @create：12/28/2018  8:22 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1228
 * @describe
 */
class Info extends Partner
{
	/**
	 * @var \app\admin\model\shopuser\User
	 */
	protected $model = null;
	protected $searchFields = 'name,phone,area';
	protected $dataLimitField = 'use_city_code';

	public function _initialize()
	{
		parent::_initialize();
		$controllername = strtolower($this->request->controller());
		$actionname = strtolower($this->request->action());

		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		// 判断控制器和方法判断是否有对应权限
		if (!$this->auth->check($path)) {
			$this->redirect('shop.error/error_url');
		}
		$this->model = model('shopUserinfo');
	}

	/**
	 * 列表页
	 *
	 * @throws \think\Exception
	 */
	public function index()
	{
		//设置过滤方法
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			//如果发送的来源是Selectpage，则转发到Selectpage
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			$query='';
			$query.='status = 1';//过滤注销用户
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //城市合伙人条件
            $city_code = session('admin.city_code');
            $type = session('admin.type');
			$total = $this->model
				->join('fa_store store', 'store.store_id=fa_shop_userinfo.store_id', 'LEFT')
				->where($where)
				->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('use_city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->count();
			$list = $this->model
				->with('fa_shop_userinfo,city,store,lottery')
				->join('fa_store store', 'store.store_id=fa_shop_userinfo.store_id', 'LEFT')
				->where($where)
				->where($query)
                ->where(function($query2) use($type,$city_code){
                    if($type == 2 && $city_code){
                        $query2->where('use_city_code',$city_code);
                    }
                })
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$result = array("total" => $total, "rows" => $list);

			return json($result);
		}
		return $this->fetch();
	}

	/**
	 * 详情页
	 *
	 * @author d3li 1/10/2019
	 * @param int $ids
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail($ids = 0)
	{
		$info = $this->model->where('id',$ids)->find();
		$this->assign('info', $info);
		return $this->fetch();
	}

	/**
	 * 赠送小哥豆
	 *
	 * @author d3li 2019/6/21
	 * @param int $ids
	 * @return string
	 * @throws \think\Exception
	 * @throws \think\exception\DbException
	 */
	public function edit($ids = 0)
	{
		$row = $this->model->get($ids);
		if (!$row)
			$this->error(__('No Results were found'));
		$adminIds = $this->getDataLimitAdminIds();
		if (is_array($adminIds)) {
			if (!in_array($row[$this->dataLimitField], $adminIds)) {
				$this->error(__('You have no permission'));
			}
		}
		if ($this->request->isPost()) {
			$params = $this->request->post("row/a");
			if ($params) {
				try {
					//是否采用模型验证
					if ($this->modelValidate) {
						$name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
						$validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
						$row->validate($validate);
					}
					$cost = $params['cost'] = $params['cost']-0;
//					$bean = model('admin/shop/EffectiveBean')::get(['userid'=>$ids]);
//					$bean['effective_bean'] += $cost;
//					$result = $bean->allowField(true)->save();
					$result = model('RedGift')::create(['uid' => $ids, 'award' => $cost]);
					if ($result !== false) {
						$params['content'] = implode('',[$this->auth->username,
							'赠送小哥豆给', $ids, '（', $row['name'] ?: $row['nickname'], '）']);
						\think\Hook::exec('app\\admin\\behavior\\Cash','log', $params);
						$params = array_merge($params, [
							'openid' => $row['openid'],
							'nickname' => $this->auth->nickname,
						]);
						\think\Hook::exec('app\\admin\\behavior\\Cash','gain', $params);
//						model('BeanLog')->add(['cost' => $cost, 'uid' => $ids, 'kind' => 2]);
						$this->success('赠送成功');
					}
//					else {
//						$this->error($bean->getError());
//					}
				} catch (\think\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			$this->error(__('Parameter %s can not be empty', ''));
		}
		$this->view->assign("row", $row);
		return $this->view->fetch();
	}

	public function json($ids = 0)
	{
		$row = $this->model->get($ids);
		return json($row);
	}

	public function check()
	{
		$row = $this->model->get(input('row.uid'));
		$row && exit(json_encode(['code'=>1,'msg'=>'成功']))
		|| exit(json_encode(['code'=>0,'msg'=>'用户不存在']));
	}

	public function test()
	{
		model('admin/shop/RedEnvelopesRecord')
			->add(5, .01, 0, $this->auth->nickname.' 赠送', 4671);
	}


}