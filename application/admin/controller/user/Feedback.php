<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\admin\controller\user;

use app\common\controller\Backend;
/**
 * Feedback.php 显示意见反馈
 *
 * @author d3li <d3li@sina.com>
 * @create：1/10/2019  3:10 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0110
 * @describe
 */
class Feedback extends Backend
{
	/**
	 * @internal
	 * @var \app\admin\model\ShopFeedback $model
	 */
	protected $model = null;
	protected $searchFields = 'id,feedback';
	public function _initialize()
	{
		parent::_initialize();
		$controllername = strtolower($this->request->controller());
		$actionname = strtolower($this->request->action());

		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		// 判断控制器和方法判断是否有对应权限
		if (!$this->auth->check($path)) {
			$this->redirect('shop.error/error_url');
		}
		$this->model = model('shopFeedback');
	}

	/**
	 * 列表页
	 *
	 * @throws \think\Exception
	 */
	public function index()
	{
		//设置过滤方法
		$this->request->filter(['strip_tags']);
		if ($this->request->isAjax()) {
			if ($this->request->request('keyField')) {
				return $this->selectpage();
			}
			list($where, $sort, $order, $offset, $limit) = $this->buildparams();
			$total = $this->model
				->where($where)
				->count();
			$list = $this->model
				->with('user')
				->where($where)
				->order($sort, $order)
				->limit($offset, $limit)
				->select();
			$result = array('total' => $total, 'rows' => $list);

			return json($result);
		}
		return $this->fetch();
	}

	/**
	 * 详情页
	 *
	 * @author d3li 1/10/2019
	 * @param int $ids
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail($ids = 0)
	{
		$this->model->where('id',$ids)->update(['admin_id'=>1]);
		$info = $this->model->with('user')->where('id',$ids)->find();
		$this->assign('info', $info);
		return $this->fetch();
	}
}