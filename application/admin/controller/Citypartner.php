<?php

namespace app\admin\controller;

use think\Db;
use think\Log;
use app\common\controller\Backend;

class Citypartner extends Backend{
    /**
     * 城市合伙人
     * @auth JJ
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Admin');
    }

	/**
	 * 删除合伙人
	 * @DateTime 2018-12-18T17:20:04+0800
	 * @return   [type]                   [description]
	 */
	public function delete(){
		$res=model('admin/shop/Store')->del_store(input('param.'));
		if($res['code']!=1){
			$this->error('删除商户失败','/',$res);
		}
		else{
			$this->success('删除商户成功');	
		}
	}
	/**
	 * 商户编辑
	 * @Author   ksea
	 * @DateTime 2018-12-18T15:28:56+0800
	 * @return   [type]                   [description]
	 */
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['id' => $ids]);
        if (!$row)
            $this->error(__('No Results were found'));
        if ($this->request->isPost())
        {
            $params = $this->request->post("row/a");
            if ($params)
            {

                //这里需要针对username和email做唯一验证
                $adminValidate = \think\Loader::validate('Admin');
                $adminValidate->rule([
                    'username' => 'require|max:50|unique:admin,username,' . $row->id,
                    'email'    => 'require|email|unique:admin,email,' . $row->id
                ]);
                $result = $row->validate('Admin.edit')->save($params);
                if ($result === false)
                {
                    $this->error($row->getError());
                }


                $this->success();
            }
            $this->error();
        }

        $groupids = Db::table('fa_city')->order('id desc')->column('city_name','city_code');
        $this->view->assign("row", $row);
        $this->assign("city_code", $groupids);
        return $this->view->fetch();
    }

    /**
     * 合伙人列表
     * @Auth JJ
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //$where['type'] = 2;
            $total = $this->model
                ->where($where)
                ->where('type', 'eq', 2)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where('type', 'eq', 2)
                ->field(['password', 'salt', 'token'], true)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k => &$v)
            {
                $groups = isset($adminGroupName[$v['id']]) ? $adminGroupName[$v['id']] : [];
                $v['groups'] = implode(',', array_keys($groups));
                $v['groups_text'] = implode(',', array_values($groups));
            }
            unset($v);
            $count = model('shopFeedback')->where(['admin_id'=>0])->count();
            $result = array("total" => $total, "rows" => $list, 'new' =>$count);

            return json($result);
        }
        return $this->view->fetch();
    }

}