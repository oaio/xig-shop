<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2018, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\uploads\controller;

use think\Request;
/**
 * Error.php
 *
 * @author d3li <d3li@sina.com>
 * @create：12/25/2018  7:07 PM
 * @see      https://gitee.com/d3li/December
 * @version 1.0.1225
 * @describe
 */
class Error
{
	public function _empty(Request $request)
	{
		echo $request->url(),'<br />文件不存在或者已被删除，请与管理员联系！';
	}
}