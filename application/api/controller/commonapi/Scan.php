<?php

namespace app\api\controller\commonapi;

use think\Controller;
use think\Db;
use think\Validate;
use think\Log;
use app\admin\model\shop\Code;
use think\Request;
use think\Hook;
use think\Cache;

class Scan extends Controller
{
	/**
	 * 需要解耦!!!!
	 * 二维码生成减去一次预约
	 * 传入卡片id
	 * 返回卡片二维码
	 * @Author   ksea
	 * @DateTime 2018-12-07T19:41:14+0800
	 * true
	 */
	public function CreateScanCode($orderid='',$appoint_start='',$sfuserid='',$areaid='440309',$address='',$note=''){
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/
        // if(session('user')){
        //     switch (Session::get('user.type')) {
        //         case '0':
        //             $this->success('用户已经登陆',url('User/index'));
        //             break;
        //         case '1':
        //             $this->success('用户已经登陆',url('User/IndexWorker'));
        //             break;
        //         default:
        //             $this->success('用户已经登陆',url('User/index'));
        //             break;
        //     }
        // }
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/

		$success=[
		    "code" =>  1,
		    "msg" =>  "",
		    "data" =>  "",
		    "url" =>  "",
		    "wait" =>  3
		];
		$error=[
		    "code" =>  0,
		    "msg" =>  "",
		    "data" =>  "",
		    "url" =>  "",
		    "wait" =>  3
		];
		
		/******************归属转换**********************/

		$session_adminid=session('admin.id')?session('admin.id'):2;

		$admindata=model('admin/AuthGroupAccess')->aga($session_adminid);

		if(in_array($admindata['authgroup']['id'],[6])){
		
			$session_adminid=$session_adminid;
		
		}
		else{
		
			$session_adminid=2;
		
		}

		/*****************归属转换***********************/

        /**************需要参数验证****************/
        $cardid=is_get_val('shop_card',$orderid,'id','orderid');
        $uid=is_get_val('shop_card',$orderid,'uid','orderid');
        $goodsid=is_get_val('shop_order',$orderid,'goodsid');
 	    $server_data=[
 	    	 'goodsid'	=> input('param.goodsid',$goodsid),
 	    	 'orderid'	=> $orderid,
 	    	 'sfuserid'	=> $sfuserid,
 	    	 'appoint_start'	=> $appoint_start?$appoint_start:time(),
 	    	 'appoint_end'	=> input('param.appoint_end',time()),
 	    	 'uid'	=> input('param.uid',$uid),
 	    	 'cardid'	=> input('param.cardid',$cardid),
 	    	 'create_time'	=>time(),
 	    	 'update_time'	=>time(),
 	    	 'snserver'      =>    'JB'.create_no(),
 	    	 'admin_id'		=>$session_adminid,
 	    	 'fu_start'		=>time(),
 	    	 'areaid'		=>$areaid,
 	    	 'address'		=>$address,
 	    	 'note'			=>$note,
 	    	 'push_time'    =>isset($sfuserid)?time():'0',
 	    ];
 	    $server_rule=[
 	    	'goodsid'	=>'require',
 	    	'orderid'	=>'require',
 	    	//'sfuserid'	=>'require',
 	    	//'appoint_start'	=>'require',
 	    	//'appoint_end'	=>'require',
 	    	'cardid'	=> 'require',
 	    	//'uid'	=>'require',
 	    ];
 	    $server_msg=[
 	    	'goodsid.require' => '商品id不能为空，关联使用',
 	    	'orderid.require' => '订单id不能为空',
 	    	'sfuserid.require' => '没有选中服务职员',
 	    	'appoint_start.require' => '没有开始时间',
 	    	'appoint_end.require' => '没有确定结束时间',
 	    	'uid.require' => '没有指定用户，请确保登陆状态',
 	    	'cardid.require'	=>'卡片未选中',
 	    ];
 	    $validate_server=validate()->make($server_rule,$server_msg);
 	    if(!$validate_server->check($server_data)){
 	    	$m_server=$validate_server->getError();
 	    	$error['msg']=$m_server;
 	    	return $error;
 	    	//$this->error($m_server);
 	    	exit();
 	    }
        /**************需要参数验证****************/

		if($server_data['cardid'])
		{
			 $data=[
			 	'uid'   => 	 input('param.uid',$uid),
			 	'id'	=>	 input('param.cardid',$server_data['cardid']),

			 ];

			 $rule=[
			 	'uid'	=>	'require',
			 	'id'	=>	'require',
			 ];

			 $msg=[
			 	'require.uid' => '用户id错误',
			 	'require.id' => '卡片错误',
			 ];
			 $validata_card = new Validate($rule);
			 if($validata_card->check($data)){
			 	$ms=$url='';

				Db::startTrans();
				try{
				    $card_where['id']=$data['id'];
				    $card_where['uid']=$data['uid'];
				    $card_where['status']=array('in','1,2');
				    $card=Db::name('shop_card')->where($card_where)->find();
				    /************判断卡片是否已经激活**********/
				    if($card){
					    if($card['valitimes']){
					    	 if($card['times']-1>0){
						    	 $up_data=[
						    	 	 'id'	     => $card['id'],
						    	 	 'times'     => ($card['times']-1),
						    	 	 'status' 	 => 2,
						    	 ];
					    	 }
					    	 else{
						    	 $up_data=[
						    	 	 'id'	     => $card['id'],
						    	 	 'times'     => ($card['times']-1),
						    	 	 'status' 	 => 4,
						    	 ];
					    	 }
					    }
					    else{
					    	 $valitimes=time()+($card['howlong']*24*60*60);
					    	 if($card['times']-1>0){
						    	 $up_data=[
						    	 	 'id'	     => $card['id'],
						    	 	 'valitimes' => $valitimes,
						    	 	 'times'     => ($card['times']-1),
						    	 	 'status' 	 => 2,
						    	 ];
					    	 }
					    	 else{
						    	 $up_data=[
						    	 	 'id'	     => $card['id'],
						    	 	 'valitimes' => $valitimes,
						    	 	 'times'     => ($card['times']-1),
						    	 	 'status' 	 => 4,
						    	 ];
					    	 }
					    }
					    Db::name('shop_card')->update($up_data);
					    /****************预约操作**********************/
					    $new_server_id=Db::name('shop_service_order')->insertGetId($server_data);

					        /*****************2019-05-16************/
					        if(strlen($server_data['sfuserid'])>0){
					    		Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>2]);
					        }
					        else{
					    		Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>1]);
					        }
					        /*****************2019-05-16************/

			                /************扭转记录********************/
			                model('admin/shop/ShopOrderHistory')::create([
			                   'oid'=>$orderid,
			                   'desc' => session('user.nickname').' 预约成功'
			                ]);
			                /************扭转记录********************/

					    	/************抢单模版消息钩子***************/
						    $params['goodsid']=$server_data['goodsid'];
						    $params['areaid']=$server_data['areaid'];
						    $params['server_id']=$new_server_id;
						    $params['sfuserid']=$server_data['sfuserid'];
						    Hook::listen('stemplate',$params);
					    	/************抢单模版消息钩子***************/

							/******************数据构建钩子*********************/
							$Cachedata=model('admin/shop/OrderTemp')->where('orderid',$orderid)->find();//Cache::get('order_'.$orderid);
							$param=[
							'cate_id'	=>$Cachedata['cate_id'],
							'time_part'	=>$Cachedata['time_part'],
							'ap_time'	=>$Cachedata['d_time'],
							'serverid'	=>$new_server_id,
							'ap_date'	=>$Cachedata['d_data'],
							];
							Hook::listen('servercache',$param);

							/*****************数据构建钩子**********************/

					    	/*****************二维码操作*********************/
					    	 $code_data=[
					    	 	 'userid'	=> input('param.uid',$uid),
					    	 	 'cardid'	=> $card['id'],
					    	 	 'scaneuserid'	=> $server_data['sfuserid'],
					    	 	 'serverid'	=> $new_server_id,
					    	 	 'valid_time'	=>time()+60*1,
					    	 	 'create_time'	=>time(),
					    	 	 'update_time'	=>time(),
					    	 	 'admin_id'		=>$session_adminid,
					    	 ];
					    	 $new_code_id=Db::name('shop_code')->insertGetId($code_data);
					    	 /********二维码id反插入服务表*********/
					    	 Db::name('shop_service_order')->update(['id'=>$new_server_id,'codeid'=>$new_code_id]);
					    	 /********二维码id反插入服务表*********/
					    	 $p_root=$_SERVER['HTTP_HOST'];
					    	 //$p_root='192.168.5.149/xgb/xig-shop/public';
					    	 $rst=creat_code('http://'.$p_root.'/index.php/mobile/Scan/codeAnalysis?code='.$new_code_id,$new_code_id.'_'.microtime(TRUE),'5','0',false,'servercode/');
							if($rst!='is_in_file'){
						    	 Db::name('shop_code')->update([
						    	 	 'id'  => $new_code_id,
						    	 	 'code'=> $rst,
						    	 ]);
								 $ms='预约成功';
								 $url=$rst;
						         /*******************公共预约成功锚点***********************/
						         Hook::listen('serverset',$server_data);
						         /*******************公共预约成功锚点***********************/
							}
							else{
								 $ms='二维码已经生成';
							}
					    	/*****************二维码操作*********************/

			                /*************************校验当前是否已经绑定人员，已绑定而且不是当前传入，解绑虚拟号**********************************/

			                $nowData=model('admin/shop/Serverorder')->with(['emp','order'])->where('id',$new_server_id)->find();
			                if(config('Fictitious.mobile_status')>0){
			                    if(true){
			                        $phone = new \wechat\Fictitious();
			                        $mobile = Db::name('employee')->where('id',$server_data['sfuserid'])->value('phone');
			                        $userphone=$nowData['order']['mobile'];
			                        $Binding_time = strtotime('- 3 day');
			                        $rivacy_phone = Db::name('rivacy_phone')->where('Binding_time','<',$Binding_time)->find();
			                        if(!is_null($mobile) && !is_null($rivacy_phone)) {
			                            $phone_resul = $phone->createphone($rivacy_phone['phone'], $mobile, $userphone);
			                            if( $phone_resul['resultdesc'] == 'Success') {
			                                Db::name('rivacy_phone')->where('id', $rivacy_phone['id'])->update(['Binding_time' => time()]);
			                                Db::name('rivacy_phone_record')->insert(['callerNum'=>$mobile,'calleeNum'=>$userphone,'relationNum'=>$rivacy_phone['phone'],'Binding_time' => time(),'subscriptionId'=>$phone_resul['subscriptionId']]);
			                            }
			                        }

			                        $ret['mobile']=$rivacy_phone['phone'];
			                        $ret['mobile_status']=config('Fictitious.mobile_status');

			                    }
			               		 model('admin/shop/Serverorder')->allowField(true)->save($ret,['id'=>$new_server_id]);
			                }


			                /***************************校验当前是否已经绑定人员，已绑定而且不是当前传入，解绑虚拟号********************************/
					    /****************预约操作**********************/
				    }
				    else{
				    	$ms='卡片不存在!,或者已经使用完毕';
				    	$success['url']='admin/shop.order/order_list';
				    }
				    Db::commit();    
				} catch (\Exception $e) {
					$ms='预约失败';
				    Db::rollback();
					return $error;
				}
				$url=$url?:'';
				$success['server']=isset($new_server_id)?$new_server_id:'';
				$success['msg']=$ms;
				$success['data']=$url;
				return $success;
				//$this->success($ms,'',$url);
			 }
			 else{
			 	$m=$validata_card->getError();
			 	$error['msg']=$m;
			 	return $error;
			 	//$this->error($m);
			 }
		}
		else
		{
		 	$error['msg']='预约卡片不存在';
		 	return $error;
			//$this->error('预约卡片不存在');
		}
	}
	/**
	 * 操作是用户生成二维码
	 * 解析二维码
	 * @return [type] [description]
	 * 可以是一个独立的页面，只需要数据同步
	 */
	public function codeAnalysis(){
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/
 
        $uid=input('param.uid');//session('user .id');
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/
	    $code_id=input('param.code','');
		if($code_id&&request()->isAjax()){
			$data=[
				'id' => input('param.code',''),
				'uid' => $uid,
			];
			$rule=[
				'id'  =>'require',
				'uid' =>'require',
			];
			if(!validate()->make($rule)->check($data)){
				$ret=validate()->make($rule)->getError();
			}
			else{
				if(is_get_val('shop_code',$code_id,'id')){
					$get_code_data=Db::name('shop_code')->where('id',$code_id)->find();
					if($get_code_data['status']!=0){
						$this->error('二维码已经使用','');
					}
					if($get_code_data['userid']!=$uid){
						$this->error('非法使用他人二维码,或者拿错了，请确认，不然把你毙了','');
					}
					if($get_code_data['valid_time']<=time()){
						$this->error('二维码已过期','',$get_code_data['valid_time']);
					}
				}
				else{
					$rMsg='卡片id不存在或者没有卡片id';
					$this->error($rMsg,'',input('param.',''));
				}

			}
			$this->success('合法二维码');
			exit();
		}
		//return $this->fetch();
	}
	/**
	 * 开始服务请求
	 * @Author   ksea
	 * @DateTime 2018-12-10T11:35:39+0800
	 * @return   [type]                   [description]
	 * id       =>      二维码id
	 * true
	 */
	public function action_now(){

		if(!is_get_val('shop_code',input('param.id',''),'id')) $this->error('请求错误');
		if(is_get_val('shop_service_order',is_get_val('shop_code',input('param.id',''),'serverid'),'status')!=0) $this->error('只可以开启未服务预约,请勿重新操作');
		$id=input('param.id','');

		$serverid=is_get_val('shop_code',$id,'serverid');
		
		$orderid=is_get_val('shop_service_order',is_get_val('shop_code',input('param.id',''),'serverid'),'orderid');
		
		$or=model('admin/shop/order')->get($orderid);

		if($or['schedule']==4)$this->error('订单已取消');

		$is_ok=1;
		Db::startTrans();
		try {
			model('admin/shop/code')->update([
				'id'	=> $id,
				'status'=> 1,
			]);
			model('admin/shop/Serverorder')->update([
				'id'	   => $serverid,
				'fu_start' => time(),
				'status'   => '1',
			]);
			Db::commit();
		} catch (Exception $e) {
			Log::init([
				'func'	  => 'action_now',
				'msg'     => $e,
				'request' => input('param.'),
			]);
			$is_ok=0;
			Db::rollback();
		}
		if($is_ok){
			/************2019-05-16*****************/

			

		    Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>5]);

			/************2019-05-16*****************/

	        /************扭转记录********************/
	        model('admin/shop/ShopOrderHistory')::create([
	           'oid'=>$orderid,
	           'desc' => session('employee.name').' 上门服务'
	        ]);
	        /************扭转记录********************/

			/*******************公共开始服务锚点***********************/
			$ServerBeginData=[
					'serverid' =>$serverid,
					'orderid'  =>$orderid,
			];
			Hook::listen('server_begin',$ServerBeginData);
			/*******************公共开始服务锚点***********************/

			$this->success('请求成功,开始服务');
		}
		else{
			$this->error('请求失败,联系客服');
		}
	}
	/**
	 * 结束服务服务
	 * @Author   ksea
	 * @DateTime 2019-03-07T11:16:19+0800
	 * @return   [type]                   [description]
	 * id       =>      二维码id
	 */
	public function stop_now(){
		
		$id=input('param.id','');
		$serverid=is_get_val('shop_code',$id,'serverid');
		
		$orderid=is_get_val('shop_service_order',is_get_val('shop_code',input('param.id',''),'serverid'),'orderid');
		
		$or=model('admin/shop/order')->get($orderid);

		if($or['schedule']==4)$this->error('订单已取消');

		$card=Db::name('shop_card')->where('id',is_get_val('shop_code',input('param.id',''),'cardid','id'))->find();
		if($card['status']==4){
	    	Db::name('shop_order')->update([
	    		'finish'=>1,
	    		'id'=>$card['orderid']
	    	]);
		}

		$serverOrder = model('admin/shop/Serverorder');

		$ServerData  = $serverOrder->where('id',$serverid)->find();

		if(!is_get_val('shop_code',input('param.id',''),'id')) $this->error('请求错误');

		if($ServerData['status']!=1) $this->error('只可以结束服务中预约，请勿重新操作');

		$is_ok=1;
		Db::startTrans();
		try {

			model('admin/shop/Serverorder')->update([
				'id'	   => $ServerData['id'],
				'fu_end' => time(),
				'status'   => '4',
			]);
			Db::commit();
		} catch (Exception $e) {
			Log::init([
				'func'	  => 'stop_now',
				'msg'     => $e,
				'request' => input('param.'),
			]);
			$is_ok=0;
			Db::rollback();
		}
		if($is_ok){
			/************2019-05-16*****************/
			
			$orderid=$ServerData['orderid'];

		    Db::name('shop_order')->where('id',$orderid)->update(['schedule'=>6]);
			
			/************2019-05-16*****************/

	        /************扭转记录********************/
	        model('admin/shop/ShopOrderHistory')::create([
	           'oid'=>$orderid,
	           'desc' => session('employee.name').' 完成服务'
	        ]);
			model('admin/shop/ShopOutcome')->avail(['oid'=>$orderid]);
	        /************扭转记录********************/

	        /**************释放虚拟号******************/
			if(config('Fictitious.mobile_status')>0){
		        $Fphone=$ServerData->mobile;
				$phone = new \wechat\Fictitious();
				$ret=$phone->Untying($Fphone);
				if($ret['resultcode'] == 0){
					Db::name('rivacy_phone')->where('phone',$Fphone)->update(['Binding_time'=>0]);
					model('admin/shop/Serverorder')->where('id',$ServerData['id'])->update(['mobile_status'=>2]);
					//释放暂存
					setFictitious($Fphone,$ServerData['id']);
				}
		    }
			/**************释放虚拟号******************/

			/*******************公共结束服务锚点***********************/
			$ServerEndData=[
					'serverid' =>$serverid,
					'orderid'  =>$orderid,
			];
			Hook::listen('server_end',$ServerEndData);
			/*******************公共结束服务锚点***********************/
			
			$this->success('请求成功,结束服务');
		}
		else{
			$this->error('请求失败,联系客服');
		}
	}
	/**
	 * 二维码废弃回收判断是否使用，没有使用预约次数+1
	 * 使用范围
	 * 1.过期二维码回收
	 * 2.预约取消回收(服务中不可以取消)
	 * @Author   ksea
	 * @DateTime 2018-12-07T19:43:01+0800
	 * id       =>      serverid
	 */
	public function GetBack(){
		$cancel=input('param.cancel',false);
		$id=input('param.id','');
  		if($id){
	  		if($cancel){
				if(!is_get_val('shop_code',$id,'id')) $this->error('请求错误');
				if(is_get_val('shop_service_order',is_get_val('shop_code',$id,'serverid'),'status')!=0) $this->error('只有服务中可以取消，请勿重新操作');
				$is_ok=1;
				Db::startTrans();
				try {

					model('admin/shop/Serverorder')->update([
						'id'	   => is_get_val('shop_code',$id,'serverid'),
						'status'   => '3',
					]);
					model('admin/shop/Code')->update([
						'id'	   => $id,
						'status'   => '2',
					]);
					$res=Db::name('shop_card')->where('id',is_get_val('shop_code',$id,'cardid'))->setInc('times');
					Db::commit();
				} catch (Exception $e) {
					Log::init([
						'func'	  => 'GetBack',
						'msg'     => $e,
						'request' => input('param.'),
					]);
					$is_ok=0;
					Db::rollback();
				}
				if($is_ok){
					$this->success('请求成功,取消服务');
				}
				else{
					$this->error('请求失败,联系客服');
				}
	  		}
			if(!is_get_val('shop_code',$id,'id')) $this->error('请求错误');
			if(is_get_val('shop_service_order',is_get_val('shop_code',$id,'serverid'),'status')!=0) $this->error('只有服务中可以取消，请勿重新操作');
			$is_ok=1;
			Db::startTrans();
			try {

				model('admin/shop/Serverorder')->update([
					'id'	   => is_get_val('shop_code',$id,'serverid'),
					'status'   => '3',
				]);
				model('admin/shop/Code')->update([
					'id'	   => $id,
					'status'   => '2',
				]);
				$res=Db::name('shop_card')->where('id',is_get_val('shop_code',$id,'cardid'))->setInc('times');
				Db::commit();
			} catch (Exception $e) {
				Log::init([
					'func'	  => 'GetBack',
					'msg'     => $e,
					'request' => input('param.'),
				]);
				$is_ok=0;
				Db::rollback();
			}
			if($is_ok){
				$this->success('请求成功,取消服务');
			}
			else{
				$this->error('请求失败,联系客服');
			}
  		}
  		else{
  			$this->error('请输入id');
  		}
	}
	/**
	 * [clearSf description]
	 * @param  [type] $serverid [预约id]
	 * 2019-03-09 10:58:29
	 * 清除预约者
	 * 后台不需要验证当前用户，阿姨需要当前阿姨id，用户端需要当前用户id
	 */
	public function clearSf($serverid){
        $res=input('server.HTTP_REFERER');
        $uid=input('uid','');
        $sfuserid=input('sfuserid','');
        $res_arr=explode('/',$res);
        $ck_admin=in_array('admin',$res_arr);
        $ck_mobile=in_array('mobile',$res_arr);
        $ck_aunt=in_array('aunt',$res_arr);
        $id=$serverid;
        $status=0;//指定清除状态

        if($ck_admin&&!$ck_mobile&&!$ck_aunt){
    		$admin_where['id']=$id;
    		$admin_where['status']=$status;
    		$sever_data=model('admin/shop/Serverorder')->where($admin_where)->find();
    		if(!$sever_data||empty($sever_data)){
    			$this->error('没有对应数据');
    		}
    		$r=model('admin/shop/Serverorder')->update(['sfuserid'=>''],['id'=>$id]);
    		$this->success('清除成功');
        }
        elseif (!$ck_admin&&$ck_mobile&&!$ck_aunt) {
        	$validate_mobile=new Validate([
        		'uid'=>'require',
        	]);
        	$mobile_data=[
        		'uid'	=>$uid,
        	];

        	if(!$validate_mobile->check($mobile_data)){
        		$this->error($validate_mobile->getError());
        	}

    		$mobile_where['uid']=$uid;
    		$mobile_where['id']=$id;
    		$mobile_where['status']=$status;
    		$sever_data=model('admin/shop/Serverorder')->where($mobile_where)->find();
    		if(!$sever_data||empty($sever_data)){
    			$this->error('没有对应数据');
    		}
    		$r=model('admin/shop/Serverorder')->update(['sfuserid'=>''],['id'=>$id]);
    		$this->success('清除成功');
        }
        elseif (!$ck_admin&&!$ck_mobile&&$ck_aunt) {
        	$validate_mobile=new Validate([
        		'sfuserid'=>'require',
        	]);
        	$mobile_data=[
        		'sfuserid'	=>$sfuserid,
        	];

        	if(!$validate_mobile->check($mobile_data)){
        		$this->error($validate_mobile->getError());
        	}

    		$aunt_where['id']=$id;
    		$aunt_where['status']=$status;
    		$sever_data=model('admin/shop/Serverorder')->where("`sfuserid` in(".$sfuserid.")")->where($aunt_where)->find();
    		if(!$sever_data||empty($sever_data)){
    			$this->error('没有对应数据');
    		}
    		$sf_string=$sever_data->sfuserid;
    		$sf_arr=explode(',', $sf_string);
    		$new_arr=array_diff($sf_arr,[$sfuserid]);
    		$newsf_string=implode($new_arr, ',');
    		$r=model('admin/shop/Serverorder')->update(['sfuserid'=>$newsf_string],['id'=>$id]);
    		$this->success('清除成功');
        }

        else{
        	$this->error('非法请求源');
        }
	}

}
