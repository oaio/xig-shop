<?php

namespace app\api\controller\commonapi;

use think\Db;
use think\Validate;
use think\Controller;

class Ajaxcommon extends Controller{
    /**
     * 图片上传
     * @Author   ksea
     * @DateTime 2018-11-13T18:02:06+0800
     * @return   [type]                   [description]
     * true
     */
    public function upload_img(){
    	 $nickname=input('post.nickname');
    	 $base64=input('post.base64');
    	 $group=input('post.group','base_img');
    	 $data =
    	 [
    	 	'group'    => $group,
    	 	'nickname' => $nickname,
    	 ];
         if(config('base64')){
            $data['base64']=$base64;
         }
    	 $res=model('admin/common/File')->create($data);
    	 if(isset($res)){
    	 	$r=$this->save_up($base64,$res['id'],$group);
    	 	if($r['code']==1){
    	 		 $sa['url']=$r['url'];
    	 		 $sa['id']=$res['id'];
    	 		 $res=model('admin/common/File')->update($sa);
    	 	}
    	 }
    	 else{
 			$r =['data'=>null,"code"=>2,"massage"=>"图片生成失败"];
    	 }
    	 return $r;
    }
    /**
     * 保存base64图片
     * @Author   ksea
     * @DateTime 2018-11-13T17:33:33+0800
     * @return   [type]                   [description]
     */
    public function save_up($base64,$id,$group=''){
			$image=$base64;
            $imageName = $id."_".date("His",time())."_".rand(1111,9999).'.png';

            if (strstr($image,",")){
                $image = explode(',',$image);
                $image = $image[1];
            }

            $path = "uploads/".date("Ymd",time());
            if($group){
            	$path.='/'.$group;
            }
            if (!is_dir($path)){
                mkdir($path,0777,true);
            }
            $imageSrc=  $path."/". $imageName;
            $r = file_put_contents(ROOT_PATH ."/public/".$imageSrc, base64_decode($image));
            if (!$r) {
                return ['data'=>null,"code"=>2,"massage"=>"图片生成失败"];
            }else{
                return ['data'=>1,"code"=>1,"massage"=>"图片生成成功",'url'=>$imageSrc];
            }

    }
    /**
     * （废弃）
     * 图片删除
     * @Author   ksea
     * @DateTime 2018-11-13T18:17:48+0800
     * @return   [type]                   [description]
     */
    public function delimg(){

    }
    /**
     * 发送短信验证码(弃用)
     * @Author   ksea
     * @DateTime 2018-12-11T11:55:47+0800
     * @return   [type]                   [description]
     */
    public function sms($phone='',$type='1'){
        if(input('param.')&&$type==1){
            $validate=new Validate([
               'phone' =>'unique:shop_userinfo',
            ],[
               'phone.unique' =>'手机号码已存在',
            ]);
            $vali_data=[
              'phone' => input('param.phone'),
            ];
            if(!$validate->check($vali_data)){
               $this->error($validate->getError());
            }
        }
        if(empty($phone)){
            $phone=input('param.phone','');
        }
        $data=[
            'phone' => $phone,
        ];
        $rule=[
            'phone' => 'require|/^1[345879]\d{9}$/',
        ];
        $msg=[
            'phone.require' =>'手机号码不可以为空',
            'phone./^1[34589]\d{9}$/'   =>'请输入正确的手机号码',
        ];
        if(validate()->make($rule,$msg)->check($data)){
            $res=send_sms($phone);
            if($res['code']=='ok'){
                Db::name('shop_sms')->insert([
                    'code'  =>$res['validate'],
                    'phone' =>$phone,
                    'data'  =>json_encode($res),
                ]);
                $this->success('验证码发送成功','/',$res['validate']);
            }
            else{
                $this->error('发送验证码失败');
            }
        }
        else{
            $this->error(validate()->make($rule,$msg)->getError());
        }
    }
    /**
     * 已支付通知商家(弃用)
     * 订单走法
     * @Author   ksea
     * @DateTime 2018-12-11T11:55:47+0800
     * @return   [type]                   [description]
     */
    public function sms_store_order($id=''){
        $viliorder=new Validate([
          'id'=>'require|min:1',
        ]);
        $vali_data=[
          'id' => $id,
        ];
        if(!$viliorder->check($vali_data)){
           $this->error($viliorder->getError());
        }
        $order=model('admin/shop/order')->with(['store'])->where(['id'=>$id])->find();
        $name=$order['nickname'];
        $phone=$order['mobile'];
        $goods_name=$order['goods_name'];
        $snorder=$order['snorder'];
        //您好，顾客刘先生（手机：132152233556）在您店铺购买商品【空调清洗1次】，单号为OR2019011411265213，请登陆店铺后台快速派单。【小哥帮短信通知，请勿回复】
        $message="您好,顾客".$name."(手机:".$phone.")在您店铺购买商品【".$goods_name."】，单号为".$snorder."，请登陆店铺后台快速派单。【小哥帮短信通知，请勿回复】";

        /*************************商户通知短信主体*********************************/
        $phone=$order['store']['ruler_phone'];//管理员手机
        if(empty($phone)){
            $phone=input('param.phone','');
        }
        $data=[
            'phone' => $phone,
        ];
        $rule=[
            'phone' => 'require|/^1[345879]\d{9}$/',
        ];
        $msg=[
            'phone.require' =>'手机号码不可以为空',
            'phone./^1[34589]\d{9}$/'   =>'请输入正确的手机号码',
        ];
        if(validate()->make($rule,$msg)->check($data)){
            $res=send_sms($phone,$message);
            if($res['code']=='ok'){

                $this->success($message,'/',$res['validate']);
            }
            else{
                $this->error('发送验证码失败');
            }
        }
        else{
            $this->error(validate()->make($rule,$msg)->getError());
        }
        /****************************商户通知短信主体******************************/

    }
    /**
     * 定时任务走法
     * 订单扫
     * @Author   ksea
     * @DateTime 2019-01-14T13:55:04+0800
     * @return   [type]                   [description]
     */
    public function sms_crontab(){
        $user_obj=model('admin/shop/Card')->with(['store','order'])->where('status=1')->select();
        $user_arr=[];
        $message='您好，您有顾客订单尚未处理，请尽快登陆后台处理订单';

        foreach ($user_obj as $key => $value) {
            $temp=$value->toarray();
            if(is_array($temp['order'])){
                 if(!in_array($temp['store']['ruler_phone'],$user_arr)){
                     array_push($user_arr,$temp['store']['ruler_phone']);
                     $res=send_sms($temp['store']['ruler_phone'],$message);
                 }
            }
        }
    }
    /**
     * 获取文章
     * @Author   ksea
     * @DateTime 2019-03-26T16:40:17+0800
     * @return   [type]                   [description]
     */
    public function articlelibrary($libid='',$usecode='',$group=''){

            $model=model('admin/shop/ArticleLibrary');

            $where=[];
            if(!empty($libid)){
                $where['id']=$libid;
            }
            if(!empty($usecode)){
                $where['usecode']=$usecode;
            }

            if(!empty($group)){

                $where['group']=$group;
                $query=$model->where($where);
                
                $res=$query->select();

            }else{

                $query=$model->where($where);
                $res=$query->find();
            }

            $this->success('获取成功','',$res);
    
    }


	public function clear()
	{
		
        $model = model('admin/CategoryCache')->clean();

		$zero = strtotime(date('Ymd'));
		for($i=1; $i<3; $i++) {
			$data = [
				'id' => 2,
				'part' => $i,
				'time' => $zero
			];
			if($row = $model->get($data)){
				$row->save(['count'=>0]);
			}else{
				$data['count'] = 0;
				$model->create($data);
			}
		}
	}

    /**
     * 公共函数测试匿名验证
     * @Author   ksea
     * @DateTime 2019-05-13T15:11:48+0800
     * @param    [type]                   $fun   [description]
     * @param    array                    $param [description]
     * @return   [type]                          [description]
     */
    public function check_com($fun,$param=array()){

        if(!empty($param)){
           foreach ($param as $key => $value) {
            
             $fun();

           }
        }
        else{
    
            $fun();
    
        }
        exit();
    }

	public function auto()
	{

		model('admin/Cash')->au2();
	
    }

    public function auto2(){
        model('admin/Cash')->au3();
    }
    
    public function auto3(){
        
        model('admin/shop/Crontab')->assess();

    }
}