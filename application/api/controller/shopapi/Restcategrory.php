<?php

namespace app\api\controller\shopapi;

use think\Db;
use think\controller\Rest;
use think\Controller;

class Restcategrory extends Rest{
	public function rest(){
	   switch ($this->method) {
	   		case 'post':
	   			return $this->addcate();
	   			break;
	   		case 'get':
	   			return $this->getfloorcate();
	   			break;
	   		case 'delete':
	   			return $this->delcate();
	   			break;
	   		case 'put':
	   			return $this->editcate();
	   			break;
	   		default:
	   			# code...
	   			break;
	   	}	
	}

   /***************************************异步操作***************************************/
  
  /**
   * 一次性获取全部分类
   * @Author   ksea
   * @DateTime 2018-11-13T11:39:31+0800
   * @return   [type]                   [description]
   */
   public function getall(){
     if(request()->isAjax()){
           //$max_floor=Db::name('shop_categrory')->field('MAX(floorid) as max_floor')->find();
           $ret=model('admin/shop/Categrory')->getallcate();
           $r['code']=1;
           $r['message']='数据捕获成功';
           $r['data']=$ret;
     }
     else{
           $r['code']=2;
           $r['message']='请求访问错误,需要ajax';
           $r['request']=request()->method();
     }
      return json($r);
   }
   /**
    * 添加
    * @Author   ksea
    * @DateTime 2018-11-13T11:41:34+0800
    * @return   [type]                   [description]
    */
   public function addcate(){
     if(request()->isAjax()){
     	$id=input('param.pid','0');
     	if($id==0){
     	   $p_data=
     	   [
     	   	 'id_link'=>0,
     	   	 'floorid'=>0,
     	   ];
     	}
     	else{
     	   $p_data=model('admin/shop/Categrory')->where('id','=',$id)->field('id_link,floorid')->find();
     	}
     	$data=
     	[
     		'pid'=>$id,
     		'name'=>input('param.name','名字待定'),
     		'icon'=>input('param.icon','www.baidu.com'),
        'floorid' =>$p_data['floorid'],
        'id_link' =>$p_data['id_link'],

     	];
     	$res_id=model('admin/shop/Categrory')->insertGetId($data);
     	$up_data['id_link']=$p_data['id_link'];
     	$up_data['id_link'].=','.$res_id;
     	$res=model('admin/shop/Categrory')->where('id','=',$res_id)->update($up_data);
     	if($res){
           $r['code']=1;
           $r['message']='添加成功';
           $r['data']=$res_id;
     	}
     	else{
           $r['code']=3;
           $r['message']='添加失败';
           $r['data']=$res_id;
     	}
     }
     else{
           $r['code']=2;
           $r['message']='请求访问错误,需要ajax，post';
           $r['request']=request()->method();
     }
      return json($r);
   }
   /**
    * 获取某一个父类的子类
    * @Author   ksea
    * @DateTime 2018-11-12T11:39:47+0800
    * @return   [type]                   [description]
    */
   public function getfloorcate(){
     if(request()->isAjax()){
        $pid=input('param.pid','0');
        $ret=model('admin/shop/Categrory')->getfloorcate($pid);
        if(is_array($ret)){
           $r['code']=1;
           $r['message']='数据获取成功';
           $r['data']=$ret;
        }
        else{
           $r['code']=1;
           $r['message']='没有匹配数据';
           $r['data']=$ret;
        }
     }
     else{
           $r['code']=2;
           $r['message']='请求访问错误,需要ajax，post';
           $r['request']=request()->method();
     }
      return json($r);
   }
   /**
    * 编辑
    * @Author   ksea
    * @DateTime 2018-11-12T15:40:30+0800
    * @return   [type]                   [description]
    */
   public function editcate(){
     if(request()->isAjax()){
         	$id=input('param.id','0');
      		$up_data=
      		[
           		'name'=>input('param.name','名字待定'),
           		'icon'=>input('param.icon','www.baidu.com')
      		];
         	$res=model('admin/shop/Categrory')->where('id','=',$id)->update($up_data);
         	if(is_numeric($res)){
               $r['code']=1;
               $r['message']='修改成功';
         	}
         	else{
               $r['code']=3;
               $r['message']='修改失败';
               $r['data']=$res;
         	}
       }
       else{
             $r['code']=2;
             $r['message']='请求访问错误,需要ajax，post';
             $r['request']=request()->method();
       }
      return json($r);
   }
   /**
    * 删除
    * @Author   ksea
    * @DateTime 2018-11-13T11:23:54+0800
    * @return   [type]                   [description]
    */
   public function delcate(){

        $id=input('param.pid','0');
        $res=model('admin/shop/Categrory')->where("FIND_IN_SET({$id},id_link)")->delete();
        if($res){
             $r['code']=1;
             $r['message']='删除成功';
        }
        else{
             $r['code']=3;
             $r['message']='删除失败';
        }
        return json($r);
     }
      
   /***************************************异步操作***************************************/
}