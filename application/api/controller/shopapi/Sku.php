<?php
namespace app\api\controller\shopapi;

use think\DB;
use think\Controller;

class Sku extends Controller{
   /**
    * 删除对
    * @Author   ksea
    * @DateTime 2018-11-17T11:38:40+0800
    * @return   [type]                   [description]
    */
   public function del_sku_group(){
   		$r=model('admin/shop/Sku')->del_sku_group(input('post.id'));
   		dump($r);
   }
   public function add_sku_group(){
   		$r=model('admin/shop/Sku')->add_sku_group(input('post.'));
   }
   public function update_sku_group(){
   		$r=model('admin/shop/Sku')->update_sku_group(input('post.'));
   		return $r;
   }
   public function del_sku(){
   		$r=model('admin/shop/Sku')->del_sku(input('post.id'));
   		dump($r);
   }
   public function add_sku(){
   		$r=model('admin/shop/Sku')->add_sku(input('post.'));
   }
   public function get_sku_goods_data(){
   		$sku_group_list=input('post.');
   		$r=model('admin/shop/Sku')->get_sku_goods_data($sku_group_list);
   		return $r;
   }
}