<?php

namespace app\api\controller\shopapi;


use think\Controller;

class Assemble extends Controller{

    /**
     * 获取某个拼团信息
     * @author hobin
     * @DateTime 2019-2-26
     * @return   [type]                   [description]
     */
     public function get_assemable_detail()
     {
//         if (request()->isPost())
//         {
             $group_id = input('param.group_id');

             $detail = model('admin/shop/groupbuydetails')->get_assemable_byid($group_id);
             return json_encode($detail);

//         }
     }


    /**
     * 获取某个拼团商品的全部信息
     * @author hobin
     * @DateTime 2019-2-26
     * @return   [type]                   [description]
     */
    public function get_goods_assemble()
    {
            $goodsid=input('param.goods_id');
            $result = model('admin/shop/assemble')->get_assemble_bygid($goodsid);
            return $result;
    }

}