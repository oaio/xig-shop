<?php

namespace app\api\controller\shopapi;

use think\Model;
use think\Controller;

class Goods extends Controller{
  public function text(){
      $id=input('param.id');
      $detail = model('admin/shop/goods')->get_goods_detail($id);
      return $detail;
  }
   /**
    * 添加
    * @Author   ksea
    * @DateTime 2018-11-16T09:32:52+0800
    */
   public function 	add_goods(){
   	 if(request()->isPost()){
    		$res=model('admin/shop/Goods')->add_goods(input('post.'));
    		if($res['code']=1){
    			$this->success('添加商品成功');
    			exit();
    		}
    		else{
    			$this->error('添加商品失败');
    			exit();
    		}
      }
      else{
				$this->error('请求错误');
    			exit();
      }
   }
    /**
     * 商品库商品同步更新
     * @Author   ksea
     * @DateTime 2019-03-26T09:54:33+0800
     * @return   [type]                   [description]
     * post_data=>id
     * post_data=>field
     */
    public function togetherUpdate(){
        if(true){
            $post_data=input('param.');
            $libid=$post_data['id'];
            $field=[];
            if(!empty($post_data['field'])){
                $field=explode(',',$post_data['field']);
            }
            model("admin/shop/Goods")->togetherUpdate($libid,$field);
            $this->success('同步成功');
        }else{
            $this->error('请求错误');
        }        
    }
   /**
    * 获取某个商品的详情基本信息，sku，商品 详情
    * @Author   ksea
    * @DateTime 2018-11-17T15:02:30+0800
    * @return   [type]                   [description]
    */
   public function get_goods_detail(){
     if(request()->isPost()){
        $id=input('param.id');
        $detail = model('admin/shop/goods')->get_goods_detail($id);
          $this->success('获取成功','',$detail);
      }
      else{
        $this->error('请求错误');
        exit();
      }   
    }
}