<?php

namespace app\api\controller\shopapi;

use think\Db;
use think\Controller;

class Store extends Controller{
    protected $model='';

    public function _initialize()
    {
        $this->model=model('admin/shop/Store');
        parent::_initialize();
    }
    /**
     * ajax坐标获取店铺
     * @param  string $latitude [description]
     * @param  string $longitude [description]
     * @return [type]               [description]
     */
    public function get_list($latitude='',$longitude=''){
        $coordinate['x']=isset($latitude)?$latitude:'';
        $coordinate['y']=isset($longitude)?$longitude:'';
        $res=$this->model->get_list($coordinate);
        if(empty($res)){
             $this->success('没有数据','',$res);
        }
        else{
             $this->success('获取成功','',$res);
        }
    }
}
 

?>