<?php
namespace app\aunt\controller;

use wechat\Jssdk;
use wechat\Wechat;
use wechat\WechatAuth;
use wechat\WechatCrypt;
use think\Controller;
use think\Hook;
use think\Db;
use think\Validate;
use think\Log;

class Controllercom extends Controller
{
    protected $id,$secret,$token,$web_url;
    
    public function _initialize(){
      header("Content-Type: text/html; charset=utf-8");
      $wechat= array('token'=>'wechat','web_url'=>'http://'.$_SERVER['HTTP_HOST'].'/aunt/Controllercom/');
      $this->token =config('wechat.token');
      $this->secret=config('wechat.secret');
      $this->id=config('wechat.appid');
      $this->web_url=$wechat['web_url'];
    }

    /**
     * 微信获取code换取当前用户信息
     * @Author   ksea
     * @DateTime 2019-02-28T20:00:14+0800
     * @return   [type]                   [description]
     */
    public  function code(){
        $local_res='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $state='wechat';
        $APPID=$this->id;
        $pid=input('param.pid','');
        $REDIRECT_URI=$this->web_url.'getcode?appid='.$APPID."&&appsecret=".$this->secret.'&&local_url='.$local_res;
        //$REDIRECT_URI='2508b1b1.nat123.cc/wechat/ncp/ncp.php/Home/Wchat/getcode';
        //$scope='snsapi_base';//不需要授权
        $scope='snsapi_userinfo';//需要授权
        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$APPID.'&redirect_uri='.urlencode($REDIRECT_URI).'&response_type=code&scope='.$scope.'&state='.$state.'#wechat_redirect';
        header("Location:".$url);
      
    }
    
    
    /**
     * 利用code获取access_token等个人用户信权
     * @Author   ksea
     * @DateTime 2019-02-28T20:00:07+0800
     * @return   [type]                   [description]
     */
    public function  getcode(){

          $appid=input('appid');
          $appsecret=input('appsecret');
          $code = input('code'); 
          $pid = input('pid','0');
          $set_localhost=input('local_url','aunt/index/index');
          $get_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$appsecret."&code=".$code."&grant_type=authorization_code ";
          $ch = curl_init($get_token_url);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_HEADER, true); 
          $res=curl_exec($ch);
          curl_close($ch);
          $res=strstr($res, "{");
          session('wechat_a.0',json_decode($res,true));
          session('wechat_a.1',input('get.'));
          /**
          * 获取微信唯一id
          */
          $api=new WechatAuth($appid,$appsecret);
          $session_token=session('wechat_a.0');
          $openid=$session_token['openid'];
          session('wechat_a.2',$api->getUserInfo($openid,'zh_CN'));
          $wechat_userdata=session('wechat_a.2');
          $param['request']['openid']=$openid;
          if(is_get_val('employee',$openid,'openid','openid')
            //&&is_get_val('shop_userinfo',$openid,'phone','openid')
          )
          {
            /**********************自动登陆操作*****************************/
            session('employee',Db::name('employee')->where(['openid'=>$openid])->find());
            /************输出openid到页面**************/
            $openid=session('wechat_a.2')['openid'];
            echo "<script type='text/javascript'>
                          localStorage.setItem('auntopenid','".$openid."');
                        </script>";
            /************输出openid到页面**************/
            /**********************自动登陆操作*****************************/
            $this->redirect($set_localhost);

          }
          $param['request']['json_wechat']=json_encode($wechat_userdata);

          $param['request']['pic']=isset($wechat_userdata['headimgurl'])?$wechat_userdata['headimgurl']:'https://pub-static.haozhaopian.net/static/web/site/features/cn/crop/images/crop_20a7dc7fbd29d679b456fa0f77bd9525d.jpg';

          $param['request']['nickname']=isset($wechat_userdata['nickname'])?$wechat_userdata['nickname']:'昵称未获取';
          $param['request']['pid']=$pid;

          /**
           * 阿姨端绑定娿
           */
          $this->assign('wechat_json',$param['request']);
          $this->assign('__token__',request()->token());
          
          return $this->fetch('employee/bind');
    }

    /**
     * 添加
     * @Author   ksea
     * @DateTime 2019-07-29T11:56:27+0800
     */
    // public function add(){
      
    //     if ($this->request->isPost()) {
    //       $params = $this->request->post("row/a");
    //       $res = model('admin/shop/Employee')->where('phone',input('row.phone'))->find();
    //       if($res&&!empty($res))return $this->error('这个号码已经存在');
    //       if ($params) {
    //         if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
    //           $params[$this->dataLimitField] = $this->auth->id;
    //         }
    //         try {
    //           $params['admin_id'] = 2;
    //           //城市合伙人条件
    //           $city_code = session('admin.city_code')?session('admin.city_code'):'';
    //           $type = session('admin.type')?session('admin.type'):'1';
    //           if($type == 2 && $city_code){
    //               $params['city_code'] = $city_code;
    //           }
    //           $params['job'] = implode(',', $params['job']);
    //           $params['area_id'] = implode(',', $params['area_id']);
    //           $result = model('admin/shop/Employee')->allowField(true)->save($params);
    //           if ($result !== false) {
    //             $this->success();
    //           } else {
    //             $this->error(model('admin/shop/Employee')->getError());
    //           }
    //         } catch (\think\exception\PDOException $e) {
    //           $this->error($e->getMessage());
    //         } catch (\think\Exception $e) {
    //           $this->error($e->getMessage());
    //         }
    //       }
    //       $this->error(__('Parameter %s can not be empty', ''));
    //     }
    //     return $this->view->fetch();

    // }

}
