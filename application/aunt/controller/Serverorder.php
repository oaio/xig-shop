<?php
namespace app\aunt\controller;

use app\aunt\common\Com;
use think\Db;
use think\Hook;
use wechat\Jssdk;
use think\Session;
use wechat\Grab;

class Serverorder extends Com
{
    public function _initialize()
    {
        parent::_initialize();
        if(!Session::get('employee')){
           $this->redirect('employee/login');
        }
    }
    public function ordersdetails(){
        return $this->fetch('serverorder/serve');
    }
}
