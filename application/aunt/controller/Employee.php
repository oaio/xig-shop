<?php
namespace app\aunt\controller;

use app\aunt\common\Com;
use think\Db;
use think\Hook;
use wechat\Jssdk;
use think\Session;

class Employee extends Com
{

    public static $model;

    public function _initialize()
    {
        self::$model=model('admin/shop/Employee');
        parent::_initialize();
    }

    /**
     * 登入
     * @Author   ksea
     * @DateTime 2018-12-06T19:32:53+0800
     * @return   [type]                   [description]
     */
    public function login(){
        /*******************判断职员是否已经登陆，登陆职员直接跳转个人中心**************************/
        if(Session::get('employee')){
            $this->redirect('index/index');
        }
        /*******************判断职员是否已经登陆，登陆职员直接跳转个人中心**************************/
        /*******************登陆信息验证**************************/
        if(input('post.')){
             $data=[
                'cid'  => input('post.cid',''),
                'name' => input('post.name',''),
                'phone'=> input('phone'),
             ];
             $rule=[
                //'cid'  => 'require|max:18',
                //'name' => 'require',
             ];
             $msg=[
                 'cid.require'  => '用户编号不能为空',
                 'cid.max'      => '请输入正确的18位手机号',
                 'name.require' => '用户名不为空',
             ];
             if(validate()->make($rule,$msg)->check($data)){
                 $where_employee['cid']=$data['cid'];
                 $employee=self::$model->where($where_employee)->find();
                 if(!empty($employee)||true){
                      $where_employee['name']=$data['name'];
                      unset($employee);
                      unset($where_employee);
                      $where_employee['phone']=$data['phone'];
                      //$where_employee['admin_id']=2;//限制某个店铺添加的用户才可以登陆客户端
                      $employee=self::$model->where($where_employee)->where("LENGTH(openid)>0")->find();
                      if(!empty($employee)){
                            Session::set('employee',$employee->Getdata());
                            $this->success('登陆成功',url('index/index'),session('employee'));
                      }
                      else{
                         $this->error('用户名错误,，或者用户位绑定');
                      }
                 }
                 else{
                    $this->error('账号错误');
                 }
                 $this->error($m);
             }
             else{
                $m=validate()->make($rule,$msg)->GetError();
                $this->error($m);
             }
             die;
        }
        /*******************登陆信息验证**************************/
        return $this->fetch();
    }


    /**
     * 登出
     * @Author   ksea
     * @DateTime 2018-12-06T19:33:02+0800
     * @return   [type]                   [description]
     */
    public function logout(){
        if(Session::clear()&&Session::get('employee')==NULL){
            $this->error('登出失败');
        }
        else{
            $this->success('登出成功','aunt/index/index');
        }
    }
    /**
     * 清除绑定
     * @Author   ksea
     * @DateTime 2019-05-21T11:07:39+0800
     * @return   [type]                   [description]
     */
    public function clearbind(){
        $emp=Session::get('employee');

        $dat=[
            'pid'          =>'',
            'openid'       =>'',
            'json_wechat'  =>'',
            'pic'          =>'',
            'nickname'     =>'',
        ];
        self::$model->save($dat,['id'=>$emp['id']]); 
        $this->logout();
    }
    /**
     * 个人中心
     * @Author   ksea
     * @DateTime 2018-12-06T19:33:19+0800
     * @return   [type]                   [description]
     */
    public function index(){
        if(!Session::get('employee')){
           $this->redirect('employee/login');
        }
        $this->assign('__token__',request()->token());
        return $this->fetch();
    }

}
