<?php
namespace app\aunt\common;

use wechat\Jssdk;
use wechat\Wechat;
use wechat\WechatAuth;
use wechat\WechatCrypt;
use think\Controller;
use think\Validate;
use think\Hook;
use think\Db;
class Com extends Controller
{
  protected $id,$secret,$token,$web_url;
  public function _initialize(){
    $ua = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos($ua, 'MicroMessenger') == false && strpos($ua, 'Windows Phone') == false){

    }
    else{
       if(
         !session('wechat_a.2')||
         !session('wechat_a.1')||
         !session('wechat_a.0')||
         is_check_true(session('wechat_a.2')['openid'],'employee','openid')||
         !session('employee')
         //||!is_get_val('shop_userinfo',session('wechat_a.2')['openid'],'phone','openid')
       ){
                controller('Controllercom')->code();
         }
         else{
              /************输出openid到页面**************/
             $openid=session('employee.openid');
             echo "<script type='text/javascript'>
                    localStorage.setItem('auntopenid','".$openid."');
                  </script>";
              /************输出openid到页面**************/
         }

    }
    $Jssdk=new Jssdk(config('wechat.appid'),config('wechat.secret'));
    $jssdk_res=$Jssdk->getSignPackage();
    $this->assign('jssdk',$jssdk_res);
  }
  /**
   * 空操作
   * @Author   ksea
   * @DateTime 2018-12-13T14:24:15+0800
   * @param    [type]                   $name [description]
   * @return   [type]                         [description]
   */
  public function _empty($name){
     $view=request()->controller().'/'.$name;
     return $this->fetch(strtolower($view));
  }
}

