<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/4 0004
 * Time: 21:52
 */

namespace app\mapiv2\model;
use think\Exception;
use think\Model;

class RedEnvelopes  extends Model
{
    /*
        0-100元：90次
        100-500元：180次
        500-1000元：360次
        1000-5000元：900次
     */
    public function rand_section($total,$userid)
    {
	    $min = 0.01;//每个人最少能收到0.01元
	    $zero = strtotime(date('Y-m-d').' 1 days');
	    $data = [];
	    $conf = config('RedEnvelopes');
	    arsort($conf);
	    foreach ($conf as $limit=>$item) {
	    	empty($item['min']) || $min = $item['min'];
	    	$max = (isset($item['max']) && $item['max'] > 0 ? $item['max'] : ($min+1)) * 100;
		    while ($total > $limit) {
			    $money = mt_rand($min*100, $max)/100;
			    $total -= $total < $money ? $money = $total : $money;
			    $limit || $limit=0;
		    	$data[] = array('money'=>$money,'userid'=>$userid, 'receive_time'=> $zero);
		    }
	    }
	    return $data;

        // if($total <= 10){
        //     $num = 1;
        // }elseif($total > 10 && $total <= 100) {
        //     $num = 30;
        // }elseif($total > 100 && $total <= 500) {
        //     $num = 60;
        // }elseif($total > 500 && $total <= 1000) {
        //     $num = 180;
        // }elseif($total > 1000 && $total <= 3000) {
        //     $num = 360;
        // }elseif($total > 3000) {
        //     $num = 720;
        // }
/*
        if($total <= config('RedEnvelopes.total')){
            $num = 1;
        }elseif($total > config('RedEnvelopes.total0')['min'] && $total <= config('RedEnvelopes.total0')['max']) {
            $num = config('RedEnvelopes.total0')['num'];
        }
        elseif($total > config('RedEnvelopes.total1')['min'] && $total <= config('RedEnvelopes.total1')['max']) {
            $num = config('RedEnvelopes.total1')['num'];
        }elseif($total > config('RedEnvelopes.total2')['min'] && $total <= config('RedEnvelopes.total2')['max']) {
            $num = config('RedEnvelopes.total2')['num'];
        }elseif($total > config('RedEnvelopes.total3')['min'] && $total <= config('RedEnvelopes.total3')['max']) {
            $num = config('RedEnvelopes.total3')['num'];
        }elseif($total > config('RedEnvelopes.total4')['min'] && $total <= config('RedEnvelopes.total4')['max']) {
            $num = config('RedEnvelopes.total4')['num'];
        }elseif($total > config('RedEnvelopes.total5')['min']) {
            $num = config('RedEnvelopes.total5')['num'];
        }
        for ($i = 1; $i < $num; $i++) {
            $safe_total = ($total - ($num - $i) * $min) / ($num - $i);//随机安全上限
            $money = mt_rand($min * 100, $safe_total * 100) / 100;
            $total = $total - $money;

            $data[] =  array('money'=>$money,'userid'=>$userid,'receive_time'=>strtotime(date('Y-m-d').' 1 days'));
        }
        $data[] = array('money'=>$total,'userid'=>$userid,'receive_time'=>strtotime(date('Y-m-d').' 1 days'));
        return $data;*/
    }
	/*
	* $total 返现红包的总额度
	* $userid 用户自增id
	*/
    public function saveredenvelopespond($total,$userid){
        $number = $this->rand_section($total,$userid);
        //model('UserStats')->add(['rebate'=>$total, 'rebate_num'=>1]);
        model('BeanLog')->add(['kind'=>1,
	        'cost'=>$total, 'uid'=>$userid, 'end' => -1]);
        return $this->saveAll($number);
        $ponds = RedEnvelopes::all(['userid' => $userid,'state' => 1]);
        if(!empty($ponds)) {
            foreach($ponds as $key=> $pond){
                if(isset($number[$key])){
                    $pond->money += $number[$key]['money'];
                    $pond->receive_time = strtotime(date('Y-m-d').' 1 days');
                    $res = $pond->save();
					if( $res !== false ){
						unset($number[$key]);
					}
                }
            }
        }
        $numbers = array_values($number);
        $user = new RedEnvelopes;
        $user->saveAll($numbers);

    }

	/**
	 *
	 * @author d3li 2019/6/29
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function stats()
    {
	    $zero = strtotime(date('Ymd'));
	    $start = $zero - 86400;
	    $list = model('admin/shop/RedEnvelopesRecord')
		    ->field('type,SUM(amount) as cost,COUNT(id) as num')
		    ->where('transaction_ime', 'between', [$start, $zero])
		    ->group('type')
		    ->select();
	    $data = $record = array();
	    foreach ($list as $k=>$v){
		    $record[$v['type']] = $v;
	    }

	    if(isset($record[1])){
	    	$data['active'] = $record[1]['cost'];
		    $data['open'] = $record[1]['cost'];
		    $data['open_num'] = $record[1]['num'];
	    }
	    if(isset($record[3])){
		    $data['active'] += $record[3]['cost'];
		    $data['new'] = $record[3]['cost'];
		    $data['new_num'] = $record[3]['num'];
	    }

	    if(isset($record[2])){
		    $data['discount'] = $record[2]['cost'];
		    $data['discount_num'] = $record[2]['num'];
	    }
	    if(isset($record[4])){
		    $data['rebate'] = $record[4]['cost'];
		    $data['rebate_num'] = $record[4]['num'];
	    }

	    $data['date'] = date('m-d', $start);
	    $data['rebate'] = $this->where('receive_time', $start)->sum('money');
	    $row = model('UserStats')->get(['create_time' => $start]);
	    if($row)
	        $row->save($data, ['create_time' => $start]);
	    else {
		    $data['create_time'] = $start;
		    model('UserStats')->create($data);
	    }
    }
}