<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\model;

use think\Model;
/**
 * ShopOrder.php 订单
 *
 * @author d3li <d3li@sina.com>
 * @create：2/26/2019  11:19 AM
 * @see      https://gitee.com/d3li/February
 * @version 2.02.26
 * @describe
 */
class ShopOrder extends Model
{
	public function base($query)
	{
		$query->where('is_pay', 1);
	}
	public function service()
	{
		return $this->hasMany('ShopServiceOrder', 'orderid', 'id')->order('id desc');
	}

	public function goods()
	{
		return $this->hasOne('ShopGoods', 'id', 'goodsid')
			->field('id,sngoods,name,inoc,brief,group_number,grabprice,bak');
	}

	public function groups()
	{
		return $this->hasOne('GroupBuying', 'goods_id', 'goodsid')
			->field('id,goods_id');
	}

	public function card()
	{
		return $this->hasOne('app\admin\model\shop\Card', 'orderid', 'id');
	}
	
	/**
	 * 小哥豆抵扣信息
	 * @Author   ksea
	 * @DateTime 2019-05-07T14:38:03+0800
	 * @return   [type]                   [description]
	 */
	public function virdiscount(){
		$where=[
			'is_pay'=>1,
		];
		return $this->hasOne('app\admin\model\shop\Virdiscount', 'id', 'discount_id')->where($where);
	}
}