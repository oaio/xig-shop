<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\model;

use think\Model;
/**
 * ShopFans.php
 *
 * @author d3li <d3li@sina.com>
 * @create：28/03/2019  9:32 AM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.28
 * @describe
 */
class ShopFans extends Model
{
	protected $autoWriteTimestamp = 'int';
	protected $updateTime = '';
}