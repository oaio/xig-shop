<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\model;

use think\Model;
/**
 * ShopServerOrder.php 工单
 *
 * @author d3li <d3li@sina.com>
 * @create：2/26/2019  11:32 AM
 * @see      https://gitee.com/d3li/February
 * @version 2.02.26
 * @describe
 */
class ShopServiceOrder extends Model
{
	public function emp()
	{
		return $this->hasMany('Employee', 'id', 'sfuserid');
	}

	public function card()
	{
		return $this->hasOne('app\admin\model\shop\Card', 'id', 'cardid');
	}
}