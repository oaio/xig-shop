<?php
namespace app\mapiv2\model;

use think\Db;
use think\Model;
use think\Validate;
use think\Log;
use think\Session;


class Cashapi extends Model
{

    protected $name='Cash';

    protected $autoWriteTimestamp=true;
    
    // 定义时间戳字段名
    //protected $cashtime = 'cashtime';

    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';
    /**
     * 粉丝数据
     * @Author   ksea
     * @DateTime 2019-01-03T20:21:45+0800
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @return   [type]                            [description]
     */
    public function insertCash($data){
        $insert=$this->create($data);
        if($insert) {
            $ret = [
                'code' => 1,
                'message' => '添加成功',
                'data' => ''
            ];
            return true;
        }
    }

    /**
     * 查询提现列表
     * @Author   ksea
     * @DateTime 2019-04-01T14:23:06+0800
     * @param    [type]                   $swtich [description]
     */
    public function cashList($userid,$size= 20,$currentpage = 0){
        $this->alias('a');
        $this->join('cash_bank b','a.id = b.cashid','LEFT');
        $this->where('a.userid',$userid);
        $this->where('a.origin',1);
        if($currentpage > 0){

            $this->where('a.id','<',$currentpage);
        }
        return $this->limit($size)->order('a.id desc')->select();
    }
    /**
     * 查询是否有未处理的提现
     * @Author   ksea
     * @DateTime 2019-04-01T14:23:06+0800
     * @param    [type]                   $swtich [description]
     */
    public function getcashList($openid){
        $this->where('status',1);
        $this->where('origin',1);
        return $this->where('openid',$openid)->select();
    }
    /**
     * 查询提现详情
     * @Author   ksea
     * @DateTime 2019-04-01T14:23:06+0800
     * @param    [type]                   $swtich [description]
     */
    public function cashDetails($userid,$id){
        $this->where('userid',$userid);
        $this->where('id',$id);
        return $this->select();
    }
}
