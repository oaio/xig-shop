<?php

function build_where($search_fields = 'id')
{
	$where = is_array($search_fields) ? $search_fields : explode(',', $search_fields);
	$where = function ($query) use ($where) {
		foreach ($where as $k => $v) {
			if (is_array($v)) {
				call_user_func_array([$query, 'where'], $v);
			} else {
				$input = input($v);
				is_null($input) || $query->where($v, $input);
			}
		}
	};
	return $where;
}