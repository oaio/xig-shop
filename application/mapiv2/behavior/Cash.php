<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/24 0024
 * Time: 20:47
 */

namespace app\mapiv2\behavior;

use think\Db;
use think\Log;
use wechat\WxpayService as MyWxpayService;

class Cash
{

    public function run(&$param){
        $wxpay = new MyWxpayService;
        $wxpay->createJsBizPackage($param);
    }

}