<?php
return [

    /**
     * 拼团详情
     */
    'group_buying_details'  =>[
        'app\\mobile\\behavior\\Groupbuydetails',
    ],
    /**
     * 修改拼团详情
     */
    'update_group_buying'  =>[
        'app\\mobile\\behavior\\Upgroupbuy',
    ],

    /**
     * 修改拼团详情
     */
    'order_buying_details'  =>[
        'app\\mobile\\behavior\\Order',
    ],

	/**
	 * 企业付款到用户零钱的钩子
	 * 2019-04-04 14:28:21
	 */
	    'wxpay_wallet'  =>[
        'app\\mapiv2\\behavior\\Cash',
    ],
    /**
     * 用户默认地址关联修改
     */
    'setDefaultAddress'=>[
        'app\\mapiv2\\behavior\\Defaultaddress',
    ],


];