<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/1 0001
 * Time: 15:44
 */

namespace app\mapiv2\controller;
use think\Db;
use think\controller\Rest;
use think\Response;
use app\mapiv2\model\Cashapi as indexCash;
use app\mapiv2\model\CashBank;
use wechat\Map;
use app\admin\model\shopuser\User;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Session;
use app\admin\model\shop\Goods as mygoods;

class Cash extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new indexCash();

    }
    /***************************************异步操作***************************************/

    /**
     * 提现首页
     * @Author   vincent
     * @DateTime 2019-04-1T20:38:32+0800
     * @param    [type]                   $type     [description]
     */
    public function index_cash(){
        $openid = input('param.openid');
        $userinfo = new User();
        $users = $userinfo->where('openid',$openid)->find();
        $status = $this->model->getcashList($openid);
        if($users){
            $res['effective'] = $users['sumwithdraw'];
            //总收益
            $res['totamount'] = $users['sumincome'];
            //已提现金额
            $res['withdrawn'] = $users['useincome'];
            //是否可以提现
            $res['url']   =[
                'WithdrawWechatSet'=>[
                    'msg'   => '提现微信设置',
                    'url'   => config('mobileUrl.WithdrawWechatSet'),
                ],
                'WithdrawBankSet'=>[
                    'msg'   => '提现银行卡设置',
                    'url'   => config('mobileUrl.WithdrawBankSet'),
                ],
                'WithdrawRecord'=>[
                    'msg'   => '提现记录',
                    'url'   => config('mobileUrl.WithdrawRecord'),
                ],
            ];
            if($status){
                $res['status'] = 1;
            }else{
                $res['status'] = 2;
            }

            return $this->success('获取成功', '', $res);
        }else {
            return $this->error('没有数据', '', '');
        }
    }

    /**
     * @param int $length
     * @return string
     * @生成随机数
     */
    public function createNonceStr_q($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
    /**
     *发起提现到零钱
     */
    public function small_change_cash(){
        //提现金额
        $userinfo = new User();
        $users = $userinfo->where('id',Session::get('user.id'))->find();
        $sumwithdraw = $users['sumwithdraw'] - input('param.amount');
        if($sumwithdraw < 0){
            return $this->error('余额不足', '', array('status'=>0));
        }
        Db::startTrans();
        try {
            $data['amount'] = input('param.amount');
            $data['cash_num'] =  uniqid();//$this->createNonceStr_q(32);
            $data['username'] =  Session::get('user.name');
            $data['openid'] =  Session::get('user.openid');
            $data['status'] =  1;
            $data['type'] =  1;
            $data['notes'] =  input('param.notes');
            $data['userid'] =  Session::get('user.id');
            $insert=$this->model->create($data);
            $user = new User();
            $user->where('id',$data['userid'])->update(['sumwithdraw'=>$sumwithdraw]);
            Db::commit();
            return $this->success('申请成功', config('mobileUrl.WithdrawApp'), array('status'=>1));
        }catch (\Exception $e) {
            $res['code']=0;
            $res['msg']='申请失败,请稍后重试';
            Db::rollback();
            return $this->error('申请失败', '', array('status'=>0));
        }
    }
    /**
     * 提现列表
     */
    public function cash_list(){
        $userid = Session::get('user.id');
        $page = input('param.page');
        $size= input('param.size');
        //提现金额
        $insert=$this->model->cashList($userid,$size,$page);
        if(!empty($insert)) {
            return $this->success('获取成功', '', $insert);
        }else{
            return $this->error('没有数据', '', $insert);
        }
    }
    /**
     * 提现详情
     */
    public function cash_details(){
        $id=input('param.id');
        $userid = Session::get('user.id');
        //提现金额
        $insert=$this->model->cashDetails($userid,$id);
        if(!empty($insert)) {
            return $this->success('获取成功', '', $insert);
        }else{
            return $this->error('没有数据', '', $insert);
        }
    }

    public function auto()
    {
    	model('admin/Cash')->au2();
    }
}
