<?php

namespace app\mapiv2\controller;
use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use app\api\controller\commonapi\Scan;
use wechat\Grab;
use app\aunt\common\Com;

class Cron extends Mycommon{
    public function __construct()
    {

        parent::__construct();

    }
    //统计
    public function cron()
    {
        ignore_user_abort (true);
        $users =    Db::name('employee') ->where('delete_time = 0')->select();
        $time = '1552233600';
        $endtime = strtotime("-1day");
        foreach($users as $key=>$user){
            $actualpay = Db::name('shop_order')->alias('a')
                ->join('shop_service_order b','a.id = b.orderid')
                ->join('shop_goods c','c.id = a.goodsid')
                ->where("FIND_IN_SET({$user['id']},b.sfuserid)")
                ->where("b.status = 4")
                ->sum('c.costprice');

            $number = Db::name('shop_order')->alias('a')
                ->join('shop_service_order b','a.id = b.orderid')
                ->where("FIND_IN_SET({$user['id']},b.sfuserid) AND b.fu_start > $time AND b.fu_start < $endtime")
                ->count();

            Db::name('employee')->where('id', $user['id'])->update(['money' => $actualpay,'orders'=>$number]);
        }
    }
}