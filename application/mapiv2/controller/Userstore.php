<?php

namespace app\mapiv2\controller;

use app\mapi\controller\commonapi\Mycommon;
use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Goods;
use app\admin\model\shop\ShopUserinfoStore;
use app\admin\model\shop\Tip;
use app\mapi\config;

class Userstore extends Mycommon{


    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new ShopUserinfoStore();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':
                $uid=input('uid');
                return $this->index($uid);
                break;

            case 'delete':

                $comvali=$this->common();

                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                $dat=input('param.');
                return $this->up_store($dat);
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 首页数据
     * @Author   ksea
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function index($uid){

        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $goods=new Goods();
            $tip=new Tip();
            $Store_data=$this->model->where(['uid'=>$uid])->find();
            $tip=$tip->where('status=1')->order('power decs,id')->select();
            $top_goods=$goods->where(['is_del'=>0,'status'=>1,'admin_id'=>2])->order('power desc id')->limit(2)->select();
            $top_goods=[
                'data' =>$top_goods,
                'url'  =>config('mobileUrl.shopGoodsDetail'),
                'count'=>count($top_goods),
            ];
            return $this->success('请求成功','',['store_data'=>$Store_data,'tip_data'=>$tip,'top_goods'=>$top_goods,'url'=>config('mobileUrl.UserStoreDetail')]);
        }
    }
    /**
     * 小店更新
     * @Author   ksea
     * @DateTime 2019-03-28T21:54:11+0800
     * @param    [type]                   $dat [description]
     * @return   [type]                        [description]
     */
    public function up_store($dat){
        if(!isset($dat['id']))return $this->error('小店id错误');
        unset($dat['__token__']);
        $res=$this->model->up_store($dat);
        if($res['code']==1){
            return $this->success('修改成功',config('mobileUrl.UserStoreShow'));
        }
        else{
            return $this->error('修改失败','',$res['msg']);
        }
    }
}