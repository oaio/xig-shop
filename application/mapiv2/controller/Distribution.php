<?php

namespace app\mapiv2\controller;

use think\Db;
use think\Validate;
use think\Log;
use think\Hook;
use app\admin\model\shop\ShopOutcome as ShopOutcome;
use app\mapi\controller\commonapi\Mycommon;

class Distribution extends Mycommon{
    /*******默认模型*****/

    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new ShopOutcome();

    }

}