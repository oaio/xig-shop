<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\controller;

use think\controller\Rest;
/**
 * Time.php
 *
 * @author d3li <d3li@sina.com>
 * @create：17/04/2019  9:54 AM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.17
 * @describe
 */
class Time extends Rest
{
	private $model;
	public function __construct()
	{
		parent::__construct();
		$this->model = model('app\admin\model\CategoryCache');
	}

	public function setadd(){
		$cate_id=input('cate_id','');
		$time_part=input('time_part','');
		$date=input('date','');
		$res=$this->model->add($cate_id, $time_part, $date);
		return $this->success('info','',$res);
	}
	/**
	 * 获取时间列表
	 *
	 * @return \think\Response
	 */
	public function index()
	{
		$cate_id = input('category', 0, 'intval');
		$date = input('date', 0);
		$res = $this->model->search($cate_id, $date);
		return $this->success('', null, $res);
	}

	/**
	 * 满单日历
	 *
	 * @param int $category
	 * @return \think\Response
	 */
	public function days($category=0)
	{
		return $this->success('',
			config('mobileUrl.orderDetails'),
			$this->model->days($category));
	}
}