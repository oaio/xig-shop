<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Viprecord as mviprecord;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
/**
 * 购买会员下单api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Viprecord extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new mviprecord();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->addVip();

                break;
            case 'get':
                $uid=input('param.uid');
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $status=input('param.status','1');
                return $this->getVip($uid,$limit,$getTime,$status);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/
    /**
     * 添加订单api
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function addVip(){
        $data=input('param.');



        $num=$data['num']=isset($data['num'])?$data['num']:1;
        

        $res=$this->model->addVip($data);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.pay').'/id/'.$res['data']['id'].'/type/5';
            return $this->success('请求成功','/',$ret);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$ret);
        }
    }

    protected function getVip($uid,$limit='',$getTime='',$status=1){
        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->getVip($uid,$limit,$getTime,$status);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }

}