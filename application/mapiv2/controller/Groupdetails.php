<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Assemble as maorder;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Hook;
/**
 * 参与拼团api
 * @Author   hobin
 * @DateTime 2019-03-2T14:07:10+0800
 * true
 */
class Groupdetails extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new maorder();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $data=input('param.');
                return $this->add_aorder($data);

                break;
            case 'get':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }
    }
    /***************************************异步操作***************************************/
    /**
     * 参与拼团api
     * @Author   hobin
     * @DateTime 2019-03-03T15:54:36+0800
     */

    protected function add_aorder($data)
    {

        $data['admin_id'] = is_get_val('store_id', session('user.id'), '');
        $num = $data['num'] = isset($data['num']) ? $data['num'] : 1;
        $params = [
            'group_id' => $data['group_id'],
            'user_id' => $data['uid'],
        ];
        $is_join = model('admin/shop/groupbuydetails')->get_join($params);

        if ($is_join)
        {
            $message = '你已经参加此次拼团活动了！';
            return $message;
        }else
        {
            $res = $this->model->add_aorder($data);
            $order_id = $res['data']['id'];
            if ($res['code'] == 1)
            {
                /**********添加卡片/钩子开始***************/
                $param = [
                    'name' => $res['data']['goods_name'],
                    'uid' => $res['data']['uid'],
                    'status' => 1,
                    'times' => is_get_val('shop_goods', $res['data']['goodsid'], 'times') * $num,
                    'sum_times' => is_get_val('shop_goods', $res['data']['goodsid'], 'times') * $num,
                    'valitimes' => 0,
                    'howlong' => '10000',
                    'orderid' => $res['data']['id'],
                    'admin_id' => $res['data']['admin_id'],
                ];
                Hook::listen('set_card', $param);
                /***********添加卡片/钩子结束**************/
                /**********修改团购状态开始***************/
                $group_data = [
                    'status' => 2,
                    'group_id' => $data['group_id'],
                ];
                Hook::listen('update_group_buying', $group_data);
                /**********添加团购信息结束***************/

                $ret['data'] = $res;

                /**********添加团购详情信息开始***************/
                $group_datail_data = [
                    'group_id' => $data['group_id'],
                    'user_id' => $res['data']['uid'],
                ];


                Hook::listen('group_buying_details', $group_datail_data);
                /**********添加团购详情信息结束***************/

                Db::name('shop_order')->where(['id'=>$order_id])->update(['group_details_id'=>$group_datail_data['group_details_id']]);
                if (strlen(session('user.phone')) < 11 && session('user')) {
                    $ret['action'] = 'binduser';
                    $ret['url'] = config('mobileUrl.bindUser') . '/mstore_id/' . session('user.store_id');

                } else {
                    $ret['action'] = 'pay';
                    $ret['url'] = config('mobileUrl.pay');
                }
                return $this->success('请求成功', '', $ret);

            } else {
                $ret['data'] = $res;
                $ret['url'] = config('mobileUrl.error');
                return $this->error('请求失败', '/', $ret);
            }
        }
    }
    protected function get_aorder($uid,$limit='',$getTime=''){

        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->get_aorder_byid($uid,$limit,$getTime);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }
    /***************************************异步操作***************************************/



}