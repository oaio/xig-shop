<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Serverorder as myservernum;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
/**
 *预约数量
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Servernum extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new myservernum();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                break;
            case 'get':
                //真实请求
                // $create_time=input('create_time','');
                // return $this->getServerNum($create_time);
                
                   return $this->bumNum();
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/


    /**
     * 真实数据
     * @Author   ksea
     * @DateTime 2019-04-12T16:26:54+0800
     * @param    [type]                   $create_time [description]
     * @return   [type]                                [description]
     */
    protected function getServerNum($create_time){

            $res=$this->model->getServerNum($create_time);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 

    }
    /**
     * 虚假数据
     * @Author   ksea
     * @DateTime 2019-04-12T16:27:38+0800
     * @return   [type]                   [description]
     */
    protected function bumNum(){

            $res=$this->model->BumServerNum();

            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
    }
    /***************************************异步操作***************************************/
}