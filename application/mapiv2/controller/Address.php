<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Address as maddress;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;




header('Access-Control-Allow-Origin:*');
// 响应类型
header('Access-Control-Allow-Methods:*');
// 响应头设置
header('Access-Control-Allow-Headers:x-requested-with,Authorization,content-type');


/**
 * 购买会员下单api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Address extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new maddress();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->addAddress();

                break;
            case 'get':
                $uid=input('param.uid');
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $status=input('param.status','1');
                return $this->getAddress($uid,$limit,$getTime,$status);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->deleteAddress();
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->updateAddress();
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/
    /**
     * 添加地址api
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function addAddress(){
        $data=input('param.');
        $res=$this->model->addAddress($data);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] ='';
            return $this->success('请求成功','/',$res);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$res);
        }
    }

    //腾讯的行政ID转我们的行政ID。
    //luojunbo 20190723
    public function transform_city_code_from_tx()
    {
        $data = input('param.');
        if(!empty($data['province'])){
            $province = $data['province'];
        }else{
            $province = 0;
        }

        $city = $data['city'];
        $district = $data['district'];
        trace("transform_city_code_from_tx province:", $province);
        trace("transform_city_code_from_tx city:", $city);
        trace("transform_city_code_from_tx district", $district);
        $regionTx = model('RegionOfTx');
        if (!empty($province)) {
            $ret = $regionTx->where(['region_id' => $province])->find();
            if (!empty($ret)) {
                $province = $ret['my_region_id'];
            } else {
                $province = 0;
            }
        }
        if (!empty($city)) {
            $ret = $regionTx->where(['region_id' => $city])->find();
            if (!empty($ret)) {
                $city = $ret['my_region_id'];
            } else {
                $city = 440300;
            }
        }
        if (!empty($district)) {
            $ret = $regionTx->where(['region_id' => $district])->find();
            if (!empty($ret)) {
                $district = $ret['my_region_id'];
            } else {
                $district = 440306;
            }
        }
        if (empty($province)) {
            if (!empty($city)) {
                $ret = model('admin/Region')->where(['region_id' => $city])->find();
                if (!empty($ret)) {
                    $province = $ret['region_parent_id'];
                } else {
                    $province = 440000;
                }
            }
        }
        return $this->success('获取成功', '', ['province' => $province, 'city' => $city, 'district' => $district]);
    }

    /**
     * 获取地址api
     * @Author   ksea
     * @DateTime 2019-04-10T10:54:14+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    integer                  $status  [description]
     * @return   [type]                            [description]
     */
    protected function getAddress($uid,$limit='',$getTime='',$status=1){
        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->getAddress($uid,$limit,$getTime,$status);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }
    /**
     * 修改api
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function updateAddress(){
        $data=input('param.');
        $res=$this->model->updateAddress($data);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] ='';
            return $this->success('请求成功','/',$ret);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$ret);
        }
    }
    /**
     * 删除api
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function deleteAddress(){
        $data=input('param.');
        $res=$this->model->deleteAddress($data);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] ='';
            return $this->success('请求成功','/',$ret);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$ret);
        }
    }
    /**
     * 设置默认
     * @Author   ksea
     * @DateTime 2019-04-11T14:16:41+0800
     * @return   [type]                   [description]
     */
    public function setdefault(){
        $id=input('param.id');
        $res=$this->model->setDefault($id);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] ='';
            return $this->success('请求成功','/',$ret);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$ret);
        }
    }
}