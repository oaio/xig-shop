<?php

namespace app\mapiv2\controller;

use app\mapi\controller\commonapi\Mycommon;
use app\admin\model\shop\EffectiveBean as bean;

class Hbbean extends Mycommon{

    /************红包提取请求*************/
    public function __construct()
    {

        parent::__construct();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':
                $uid=input('param.uid');
                return $this->getHb($uid);
                break;

            case 'delete':

                $comvali=$this->common();

                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 首页数据
     * @Author   ksea
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function getHb($uid){
        $bean =new bean();
        $res=$bean->gethbbean($uid);
        return $this->success('获取成功','',$res);
    }

    public function open($uid = 0)
    {
    	$res = model('RedGift')->open($uid);
	    return $this->success('请求成功','',$res);
    }
}