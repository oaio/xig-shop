<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/27
 * Time: 20:04
 */

namespace app\mapiv2\controller;

use app\mobile\common\Controllercom;
use wechat\Jssdk;
class Forward extends Controllercom
{
    /**
     * 拼团支付成功后分享
     * @author hobin
     * @DateTime 2019-2-26 17:15
     * @return mixed
     */
    public function paysuccess(){

        $Jssdk=new Jssdk(config('wechat.appid'),config('wechat.secret'));
        $jssdk_res=$Jssdk->getSignPackage();

        return json_encode($jssdk_res);

    }
}