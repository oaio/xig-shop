<?php

namespace app\mapiv2\controller\commonapi;

use think\Db;
use think\Validate;
use think\Controller;
use wechat\Grab;
use think\Hook;
use think\Cache;

class Comapi extends Controller{
	/**
	 * 零元支付(通用)
	 * @Author   ksea
	 * @DateTime 2019-06-11T16:14:59+0800
	 * @param    [type]                   $oid [description]
	 * @return   [type]                        [description]
	 */
	public function zeropay($oid){
			$order=model('admin/shop/order')->get($oid);
			if($order['is_pay']!=1){

				$data=[
					'id'=>$order['id'],
					'status'=>'1',
					'is_pay'=>'1',
					'pay_id'=>'00',
				];
				$res=model('admin/shop/order')->update_order($data);
				/**********分销钩子************/
				if(is_get_val('shop_userinfo',$order['uid'],'store_id','id')!=0){
					$param=[
						'id' =>$order['id'],
					];
					Hook::listen('distribution',$param);
				}
				/**********分销钩子************/

				/*************虚拟优惠券使用抵扣****************/
				if($order['discount_id'] > 0){
					model('admin/shop/Virdiscount')->payDis($order['discount_id'],$order['id']);
				}
				/*************虚拟优惠券使用抵扣****************/
				
				/*************贡献钩子*****************/
				$Outcome=['oid'=>$order['id']];
				Hook::listen('outcome',$Outcome);
				/*************贡献钩子*****************/

				$Cachedata=model('admin/shop/OrderTemp')->where('orderid',$order['id'])->find();//Cache::get('order_'.$order['id']);
				$cares=controller('api/commonapi/Scan')->CreateScanCode($Cachedata['orderid'],$Cachedata['appoint_start'],$Cachedata['sfuserid'],$Cachedata['areaid'],$Cachedata['address'],$Cachedata['note']);
				if($cares && isset($cares['msg']) && $cares['msg'] == '预约成功' && $cares['code']==1){
					model('admin/shop/OrderTemp')->where('orderid',$Cachedata['orderid'])->update(['status'=>1]);
					//Cache::rm('order_'.$order['id']); 

				}
				else{

					$dataMsg=[
						'code'=>0,
						'data'=>$Cachedata,
						'url' =>'',
						'msg' =>'预约失败',
						'wait'=>3,
					];

					return json($dataMsg);

				}
				$dataMsg=[
					'code'=>1,
					'data'=>$order,
					'url' =>'',
					'msg' =>'支付成功',
					'wait'=>3,
				];

		         /*******************零元支付成功锚点***********************/
		         Hook::listen('zeropay',$order);
		         /*******************零元支付成功锚点***********************/
			
			}
			else{
				$dataMsg=[
					'code'=>0,
					'data'=>$order,
					'url' =>'',
					'msg' =>'请勿重复支付',
					'wait'=>3,
				];
			}
			return json($dataMsg);
	}
}