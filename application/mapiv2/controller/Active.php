<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Active as mactive;
use app\mapi\controller\commonapi\Mycommon;
use think\Hook;
/**
 * 购买会员下单api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Active extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new mactive();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('未定义请求类型');

                break;
            case 'get':
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $status=input('param.status','1');
                return $this->getActive($limit,$getTime,$status);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('未定义请求类型');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('未定义请求类型');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/

    /**
     * 获取活动页
     * @Author   ksea
     * @DateTime 2019-04-10T10:54:14+0800
     * @param    [type]                   $uid     [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    integer                  $status  [description]
     * @return   [type]                            [description]
     */
    protected function getActive($limit='',$getTime='',$status=1){

            $res=$this->model->getActive($limit,$getTime,$status);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 

    }
    /**
     * 活动结束
     * @Author   ksea
     * @DateTime 2019-04-17T14:20:29+0800
     * @return   [type]                   [description]
     */
    public function expire(){
            Hook::exec('app\\mobile\\behavior\\Active','run',$params);
    }


    public function rebate($uid=0)
    {
    	//dump(model('RedEnvelopes')->saveredenvelopespond(.01, $uid));
    }
}