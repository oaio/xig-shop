<?php

namespace app\mapiv2\controller;
use think\Db;
use app\mapi\controller\commonapi\Mycommon;
use app\admin\model\shop\RivacyPhoneRecord;
use app\api\controller\commonapi\Scan;
use wechat\Grab;
use app\aunt\common\Com;
use wechat\Fictitious;
use Redis;

class Staff extends Mycommon{


    public function __construct()
    {

        parent::__construct();

    }

    /**
     * 待抢单,每个阿姨限定能接单的区域,只要是该区域内的订单都可以抢单。
     * @Author   vincent
     * @DateTime 2019-02-28T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function await_assign(){
        //根据session 查询出该用户所在的区域 region
        //auntopenid
        $openid = input('auntopenid');
        $userid =  Db::name('employee')->alias('a')
            ->where(['a.openid'=>$openid])
            ->find();
        if(empty($userid)||is_null($openid)){
            return $this->success('failure','','非法的请求');
        }
        $cate = explode(",",$userid['job']);
        $area_id = explode(",",$userid['area_id']);

        $temp_server=Db::name('shop_service_order')->alias('a')
            ->join('shop_order u','a.orderid=u.id','left')
            ->join('shop_goods b','b.id=a.goodsid','left')
            ->join('region rg','a.areaid=rg.region_id','left')
            ->field('u.goods_name,a.address,a.appoint_start,u.num,a.id,b.pic,u.snorder,rg.region_name,a.areaid')
            ->where(['a.sfuserid'=>''])
            ->where('a.areaid','in',$area_id)
            ->where('u.store_id = 1')
            ->where('b.cate','in',$cate)
            ->where('b.cate = 1')
            ->order('a.id desc')
            ->select();

        return $this->success('success','',$temp_server);
    }
    /**
     * 根据单个订单显示详细
     * @Author   vincent，无法根据订单金额，推测出佣金。
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function assign(){
        $uid= input('orderid');
        if(is_null($uid)){
            return $this->success('failure','','');
        }
        $temp_server=model('ShopOrder')
            ->alias('u')
            ->with(['virdiscount'])
            ->join('shop_service_order a','a.orderid=u.id','left')
            ->join('shop_goods b','b.id=a.goodsid','left')
            ->field('u.mobile,u.nickname,a.status,u.goods_name,a.address,a.appoint_start,u.num,a.id,b.pic,a.codeid,u.snorder,u.bak,u.actualpay,u.discount_id')
            ->where(['a.id' => $uid])
            ->order('u.id desc')
            ->select();
        return $this->success('success','',$temp_server);
    }

    /**
     * 服务详情
     * @Author   vincent
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]:  a.sfuserid
     */
    public function service_details(){
        $openid = input('auntopenid');
        if(is_null($openid)){
            return $this->success('failure','','缺少参数');
        }
        $userid =  Db::name('employee')->alias('a')
            ->field('a.id')
            ->where(['a.openid'=>$openid])
            ->select();
        $id = isset($userid[0]['id'])?$userid[0]['id']:0;
        if($id == 0){
            return $this->success('failure','','非法参数');
        }
        //根据session查出用户id
        $temp_server=Db::name('shop_service_order')->alias('a')
            ->join('fa_shop_order u','a.orderid=u.id','left')
            ->join('shop_goods b','b.id=a.goodsid','left')
            ->field('a.sfuserid,u.goods_name,a.address,a.appoint_start,u.num,a.id,a.status,u.nickname,b.pic,u.snorder,u.mobile')
            ->where("FIND_IN_SET({$id},sfuserid) and a.status in (0,1)")
	        ->order('a.create_time DESC')
            ->select();
        $data['now'] = $temp_server;
        $history=Db::name('shop_service_order')->alias('a')
            ->join('fa_shop_order u','a.orderid=u.id','left')
            ->join('shop_goods b','b.id=a.goodsid','left')
            ->field('a.sfuserid,u.goods_name,a.address,a.appoint_start,u.num,a.id,a.status,u.nickname,b.pic,u.snorder')
            ->where("FIND_IN_SET({$id},sfuserid) and a.status = 4")
	        ->order('a.create_time DESC')
            ->select();
        $data['history'] = $history;
        $data['url']	=config('mobileUrl.beginServer');
        return $this->success('success','/index.php/aunt/serverorder/ordersdetails',$data);
    }

    //个人中心
    public function personal($id = 1){
        $openid = input('auntopenid');
        if(is_null($openid)){
            return $this->success('failure','','');
        }
        $store =  Db::name('employee')->alias('a')
            ->field('a.cid,a.name,a.phone,a.admin_id,b.nickname,a.orders,a.money,a.avatar,a.id,a.job,a.area_id')
            ->join('store b','a.admin_id = b.admin_id','left')
            ->where(['a.openid'=>$openid])
            ->select();
        return $this->success('success','',$store);
    }

}