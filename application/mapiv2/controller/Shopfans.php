<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\ShopFans as myShopFans;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
/**
 * 绑定api
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Shopfans extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new myShopFans();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                break;
            case 'get':
                $uid=input('param.uid');
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $status=input('param.status','1');
                return $this->get_fans($uid,$limit,$getTime,$status);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/


    /**
     * 获取粉丝
     * @Author   ksea
     * @DateTime 2019-03-28T20:38:32+0800
     * @param    [type]                   $uid     [description]
     * @param    [type]                   $fansid  [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    integer                  $status  [description]
     * @return   [type]                            [description]
     */
    protected function get_fans($uid,$limit='',$getTime='',$status=1){

        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->get_fans($uid,$limit,$getTime,$status);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }
    /**
     * 粉丝全部贡献总和
     * @Author   ksea
     * @DateTime 2019-03-28T20:47:38+0800
     * @param    [type]                   $uid [description]
     * @return   [type]                        [description]
     */
    public function sum_income($uid){
        $data=[
            'uid' =>$uid,
        ];

        $rule=[
            'uid' =>'require|min:1',
        ];

        $msg=[
            'uid.require'=>'用户id不允许为空',
        ];

        if(!validate()->make($rule,$msg)->check($data)){
            return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
        }
        else{
            $res=$this->model->where(['fuid'=>$uid,'status'=>1])->field('SUM(sumoutcome) as sumcome')->find();
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
        }
    }
    /***************************************异步操作***************************************/
}