<?php

namespace app\mapiv2\controller;

use app\mapi\controller\commonapi\Mycommon;
use app\admin\model\shop\Employee as myEmployee;

/**
 * 发起拼团api
 * @package app\mapiv2\controller
 * true
 */
class Employee extends Mycommon{


    public function __construct()
    {

        parent::__construct();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->error('请求未定义');
                break;

            case 'delete':

                $comvali=$this->common();

                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                // $comvali=$this->common();
                // if($comvali){
                //     return $this->error($comvali);
                // }

                $data=input('param.');
                return $this->upemployee($data);
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }

    /**
     * 阿姨绑定页面
     * @Author   ksea
     * @DateTime 2019-02-28T19:57:55+0800
     * @return   [type]                   [description]
     */
    public function upemployee($param){
        $m_model=new myEmployee();
        if($param){
          if($param['cid'])$where['cid']=$param['cid'];
          if($param['name'])$where['name']=$param['name'];
          unset($param['cid']);
          unset($param['name']);
          $where['phone'] = $param['phone'];
          $m_data=$m_model->where($where)->find();
          if(empty($m_data)){
              return $this->error('请检查是否输入正确');
          }
          if(!empty($m_data->json_wechat)||$m_data->json_wechat){
              return $this->error('账号已绑定');
          }
          $res=$m_model->UpdataEmp($param);
          if($res['code']==1){
            return  $this->success('绑定成功');
          }else{
            return $this->error('绑定失败');
          }
        }
    }

	/**
	 * 获取所有员工
	 *
	 * @author d3li 2019/6/19
	 * @return \think\response\Json
	 * @throws \think\exception\DbException
	 */
	public function all()
    {
    	$list = model('Employee')::all();
    	$list = model('Employee')::all(['id'=>['NOT BETWEEN',[129,244]]]);
    	$data = [];
    	foreach ($list as $value){
    		$data[$value['id']] = $value['name'];
	    }
	    return json($data);
    }

	/**
	 *
	 * @author d3li 2019/6/19
	 * @throws \think\exception\DbException
	 */
	public function lottery($act=3)
    {
    	$data = model('RedEnvelopes')::all(['state'=>1, 'money'=>['>',$act]]);
    	array_map(function ($v){
		    $zero = strtotime(date('Ymd'));
    		$data = [];
    		while ($v['money']>3){
    			$cost = mt_rand(50, 300)/100;
    			$data[] = ['money'=>$cost, 'receive_time'=>$zero, 'userid'=>$v['userid']];
    			$v['money'] -= $cost;
		    }
		    $v->save();
    		$v->isUpdate(false)->saveAll($data);
	    }, $data);
    	dump(count($data));
    }
}