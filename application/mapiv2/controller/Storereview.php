<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\ShopUserinfoStoreReview as mStoreReview;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;

/**
 * 申请小店
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Storereview extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new mStoreReview();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':
                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->addReview();

                break;
            case 'get':

                $uid=input('param.uid');
                return $this->getReview($uid);

                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('未定义请求类型');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('未定义请求类型');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/  
    /**
     * 添加申请
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function addReview(){
        $data=input('param.');
        $res=$this->model->addReview($data);
        if($res['code']==1){
            $ret['data']=$res;
            $ret['url'] ='';
            return $this->success('请求成功','/',$res);
        }
        else{
            $ret['data']=$res;
            $ret['url'] =config('mobileUrl.error');
            return $this->error('请求失败','/',$res);
        }
    }

    /**
     * 获取申请
     * @Author   ksea
     * @DateTime 2019-01-03T15:54:36+0800
     */
    protected function getReview($uid){
        $review=$this->model->where(['uid'=>$uid])->find();
        if(empty($review)){

            $res['url']=config('mobileUrl.applica'); 
            $res['data']=$review;
            return $this->error('没有申请数据','/',$res);

        }
        else{
            if($review['status']==0){
                 $res['url']=config('mobileUrl.audit');
                 $res['code']=0;
                 $res['msg']='审核中';
            }
            elseif ($review['status']==1) {

                $res['url']=config('mobileUrl.userStoreIndex');
                $res['code']=1;
                $res['msg']='审核通过';
                
            }
            elseif ($review['status']==2) {
                 $res['url']='/';
                 $res['code']='审核驳回';
            }
            else{

            }
            $res['data']=$review;
            return $this->success('请求成功','/',$res);

        }
    }

    /**
     * 虚假数据
     * @Author   ksea
     * @DateTime 2019-04-12T16:27:38+0800
     * @return   [type]                   [description]
     */
    
    public function bumReview(){

            $res=$this->model->BumReview();
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 
    }
}