<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\controller;

//use app\mapi\controller\commonapi\MyCommon;
use think\controller\Rest;
use think\exception\DbException;
use think\Exception;
/**
 * Myorder.php 我的订单
 *
 * @author d3li <d3li@sina.com>
 * @create：2/26/2019  10:10 AM
 * @see      https://gitee.com/d3li/February
 * @version 2.02.26
 * @describe
 */
class Myorder extends Rest
{
	private $model;
	public function __construct()
	{
		parent::__construct();
		$this->model = model('ShopOrder');
	}
	/**
	 * 购买前检测
	 * @Author   ksea
	 * @DateTime 2019-03-01T10:54:23+0800
	 * @return   [type]                   [description]
	 */
	public function checkbind($uid='',$phone=''){


		/**************转换判断*******************/
		if(!empty($uid)){

			$user=model('admin/shopuser/User')->get($uid);
			if($user){
				session('user',$user->toarray());
			}
			else{
				return $this->error('数据不存在','',$uid);
			}
		
		}
		/**************转换判断*******************/

        if(strlen(session('user.phone'))==11&&session('user')){
            $ret['action']='buynow';
            $ret['url']=config('mobileUrl.orderDetails');
        	return $this->success('已经绑定','',$ret);
        }
        else{
            $ret['action']='binduser';
            $ret['url']=config('mobileUrl.bindUser').'/mstore_id/'.session('user.store_id');
            return $this->error('现在绑定','',$ret);
        }

	}
	public function index()
	{
//		$validate=$this->common();
//		if($validate){
//			return $this->error($validate);
//		}
		if('get' == $this->method) {
			$uid =  input('uid', 0, 'intval');
			$offset =  input('start', 0, 'intval');
			$limit =  input('count', 10, 'intval');
			$result = [];
			try {
				$result['total'] = $this->model
					->where(['uid'=>$uid, 'finish'=>0])
					->count();
				$result['all'] = $this->model
					->with('service,goods,groups,card')
					->where(['uid' => $uid, 'finish'=>0])
					->order('id desc')
					->limit($offset, $limit)
					->select();
				$result['group'] = $this->model
					->hasWhere('groups', ['status'=>1])
					->with('service,goods,groups')
					->where(['uid'=>$uid, 'type'=>1, 'finish'=>0])
					->order('create_time desc')
					->limit(0, 1)
					->select();
				if($item = end($result['group'])) {
					$result['group_buying'] = model('GroupBuyingDetails')->alias('d')
						->join('fa_group_buying g', 'd.group_id = g.id')
						->where(['g.goods_id' => $item->goodsid, 'd.user_id' => $uid])
						->field('g.*')
						->find();
				}
				$result['history'] = $this->model
					->with('service,goods')
					->where(['uid'=>$uid, 'finish'=>1])
					->order('create_time desc')
					->limit(0, 1)
					->select();
				return $this->success('', '', $result);
			} catch (DBException $e) {
				return $this->error($e->getMessage());
			} catch (Exception $e) {
			}
		}
		return $this ->error();
	}

	/**
	 * 团购/历史订单
	 *
	 * @return \think\Response
	 * @throws Exception
	 */
	public function type()
	{
		if('get' == $this->method) {
			if(! input('uid'))return $this->error();
			$offset =  input('start', 0, 'intval');
			$limit =  input('count', 10, 'intval');
			$where = build_where('uid,type,finish');
			$result = [];
			$model = $this->model;
			if(!input('finish')){
				$model->hasWhere('groups', ['status'=>1])->with('groups');
			}
			$result['total'] = $model
				->where($where)
				->count();
			$result['all'] = $model
				->with('service,groups')
				->where($where)
				->order('create_time desc')
				->limit($offset, $limit)
				->select();
			return $this->success('', '', $result);
		}
		return $this ->error();
	}

	/**
	 * 订单详情
	 *
	 * @return \think\Response
	 * @throws DbException
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 */
	public function detail()
	{
		$res = $this->model
			->alias('a')
			->with(['card','service','virdiscount'])
			->join('fa_shop_service_order s', 's.orderid = a.id', 'LEFT')
			->join('fa_employee e', 'e.id in (s.sfuserid)', 'LEFT')
			->where('a.id',  input('param.id', 0, 'intval'))
			->field('a.*,s.appoint_start,s.status,e.name,e.avatar,e.phone')
			->order('s.create_time DESC')
			->select();
		if($res){
			//foreach ($res as $v) $v->status = $v->status?
			return $this->success('', '', $res);
		}
		return $this ->error();
	}

	/**
	 * 客户修改订单（地址、时间、员工)
	 *
	 * @return \think\Response
	 * @throws DbException
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 */
	public function edit()
	{
		if('post' == $this->method){
			$default = [
				'orderid'=>'',
				'address' => '',
				'time' => $_SERVER['REQUEST_TIME'],
				'worker' => '',
				'areaid'	=>'',
			];
			$params = input('','', ['strip_tags', 'htmlspecialchars']);
			$params = array_intersect_key(array_merge($default, $params), $default);
			if(empty($params['address'])){
				return $this->error('请填写服务地址');
			}else{
				$model = $this->model;
				$order = $model::get($params['orderid']);
				if(!$order)return $this ->error('订单不存在');
				if($order['finish']) return $this ->error('订单已完成');
				//$order->save(array_intersect_key($params, ['address'=>'']));
			}
			$where = [(is_numeric($params['worker']) ? 'id' : 'cid')=>$params['worker']];
			$time = is_numeric($params['time']) && strlen($params['time'])>10 ?
				intval($params['time']/1000) : $params['time'];
			$data = ['appoint_start' => $time];
			$data = ['address' => $params['address']];
			$res = model('Employee')->where($where)->find();
			if ($res){
				$data['sfuserid'] = $res->id;
			}
			$res =controller('api/commonapi/Scan')->CreateScanCode($params['orderid'],$time,$params['worker'],$params['areaid'],$data['address']);
			if($res && isset($res['msg']) && $res['msg'] == '预约成功'){
				model('ShopServiceOrder')
					->where('orderid', $params['orderid'])
					->update($data);
				return $this->success('订单已发送，等待服务人员接单');
			}elseif($res && isset($res['msg'])){
				return $this ->error($res['msg']);
			}
		}
		return $this ->error();
	}

	/**
	 * 历史服务人员
	 *
	 * @return \think\Response
	 * @throws DbException
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 */
	public function staff()
	{
		$no = input('param.code');
		if($no){//搜索服务人员编码
			$res = model('Employee')
				->where('cid', $no)
				->find();
			return $this->success($res?'':'没有找到相关人员', '', $res);
		}

		$job = input('param.job', 1, 'intval');
		return $this->success('','',model('ShopServiceOrder')
			->alias('a')
			->join('fa_employee e', 'e.id in (a.sfuserid)')
			->where('a.uid' , input('param.uid', 0, 'intval'))
			->where("FIND_IN_SET($job,e.job)")
			->field('e.id,e.cid,e.name,e.avatar')
			->group('e.id')
			->select());
	}
	/**
	 * 小程序自定义支付，订单追加支付
	 * @Author   ksea
	 * @DateTime 2019-08-06T21:20:44+0800
	 * @return   [type]                   [description]
	 */
	public function prepay()
	{
		$params = input('', [], 'strip_tags');

		//订单追加支付
		if(isset($params['type']) && $params['type'] == 3 && isset($params['wantpay_id'])){

			$wantPayData=model('admin/shop/UserWantPay')->where(['id'=>$params['wantpay_id']])->find();

			if($wantPayData['discount_id']<=0){
				$params['total_fee']=$wantPayData['money']*100;
			}
			else{
				$params['total_fee']=$wantPayData['discount']*100;
			}

		}
		
		$params = array_filter(array_intersect_key($params,array_flip(array('appid', 'body', 'openid', 'out_trade_no', 'total_fee', 'type'))));
		$params['trade_type'] = 'JSAPI';
		//追加支付
		if(isset($params['type']) && $params['type'] == 2){
			$note = input('note', '', 'strip_tags');
			model('admin/shop/ShopPaySelf')::create([
				'uid' => session('user.id') ?: 0,
				'openid' => $params['openid'],
				'no' => $params['out_trade_no'],
				'money' => $params['total_fee'],
				'note' => $note,
				'origin' => 2
			]);
		}
		$api = \wechat\Wepay::getInstance();
		$data = $api->setApp(isset($params['mini_appid'])?$params['mini_appid']:config('pay.mini_appid'))
//			->setKey('70389806194021fa5773236b95b9d705')
			->setNotify(\think\Request::instance()->domain() . '/mapiv2/myorder/notify')
			->prepay($params);
		unset($data['appId']);
		return json($data);
	}

	public function notify()
	{
		return controller('mobile/pay')->callback();
	}


}