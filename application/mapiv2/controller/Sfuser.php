<?php

namespace app\mapiv2\controller;

use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\Employee as Sf;
use wechat\Map;
use app\mapi\controller\commonapi\Mycommon;
use think\Validate;
use think\Log;
use think\Hook;
/**
 * 师傅数据
 * @Author   ksea
 * @DateTime 2019-01-03T14:07:10+0800
 */
class Sfuser extends Mycommon{

    /*******默认模型*****/
    private $model;

    public function __construct()
    {

        parent::__construct();
        $this->model = new Sf();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                break;
            case 'get':
                $cate=input('param.cate','');
                $limit=input('param.limit','');
                $getTime=input('param.create_time','');
                $search=input('param.search','');
                $other=input('param.other/a');
                return $this->getSf($cate,$limit,$getTime,$search,$other);
                break;

            case 'delete':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /***************************************异步操作***************************************/


    /**
     * 获取服务者
     * @Author   ksea
     * @DateTime 2019-03-28T20:38:32+0800
     * @param    [type]                   $uid     [description]
     * @param    [type]                   $fansid  [description]
     * @param    string                   $limit   [description]
     * @param    string                   $getTime [description]
     * @param    integer                  $status  [description]
     * @return   [type]                            [description]
     */
    protected function getSf($cate,$limit,$getTime,$search,$other){

            $res=$this->model->getSf($cate,$limit,$getTime,$search,$other);
            if(empty($res)){
                return $this->error('没有数据','',$res);
            }
            else{
                return $this->success('获取成功','',$res);
            } 

    }

    /***************************************异步操作***************************************/
}