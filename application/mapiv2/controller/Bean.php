<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\controller;
use app\mapi\controller\commonapi\Mycommon;

/**
 * Bean.php 小哥豆
 *
 * @author d3li <d3li@sina.com>
 * @create：04/05/2019  8:54 PM
 * @see      https://gitee.com/d3li/May
 * @version 2.05.04
 * @describe
 */
class Bean extends Mycommon
{
	public function index($uid = 0)
	{
		$res = model('admin/shop/RedEnvelopesRecord')->info($uid);
		$res['total'] = model('admin/shop/RedEnvelopes')->total($uid);
		$row = model('admin/shop/EffectiveBean')
			->get(['userid'=>$uid]);
		$row && $res['enable'] = $row->effective_bean;
		return $this->success('获取成功', '/', $res);
	}

	/**
	 * 红包记录
	 *
	 * @access public
	 * @return \think\Response
	 */
	public function listing($uid = 0)
	{
		$limit = input('limit',10, 'strip_tags');
		$res = model('admin/shop/RedEnvelopesRecord')
			->listing($uid, $limit);
		return $this->success('获取成功', '/', $res);
	}

	/**
	 * 领红包
	 *
	 * @access public
	 * @return mixed
	 */
	public function open($uid = 0)
	{
		$res = model('admin/shop/RedEnvelopes')->pop($uid);
//		$res>0 && model('UserStats')->add([
//			'active' => $res, 'open'=>$res, 'open_num'=>1]);
		return is_numeric($res) && $res>0 ?
			$this->success('获取成功', '/', $res) :
			$this->error('获取失败', '/', $res);
	}

	public function stats()
	{
		model('RedEnvelopes')->stats();
	}

	public function tet()
	{
		dump(model('RedEnvelopes')->saveredenvelopespond(.01, 4887));
		//model('BeanLog')->add(['cost' => 3, 'uid' => 4887, 'kind' => 1]);
	}

}