<?php

namespace app\mapiv2\controller;

use app\mapi\controller\commonapi\Mycommon;
use app\admin\model\shop\Goods as mygoods;

class Goods extends Mycommon{


    public function __construct()
    {

        parent::__construct();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->index(is_get_val('store','2','store_id','admin_id'),input('param.limit',5),input('param.create_time'),input('param.other/a'));
                break;

            case 'delete':

                $comvali=$this->common();

                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 首页数据
     * @Author   ksea
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function index($store_id,$limit='',$getTime='',$other){
        $goods =new mygoods();
        $res=$goods->goods_by_store($store_id,$limit,$getTime,$other,'deal_times desc id');
        if(empty($res)){
            return $this->error('没有数据','',$res);
        }
        else{
            return $this->success('获取成功','',$res);
        }
    }
}