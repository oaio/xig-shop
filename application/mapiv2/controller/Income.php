<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mapiv2\controller;

use app\admin\model\ShopUserinfo;
use think\controller\Rest;
/**
 * Income.php
 *
 * @author d3li <d3li@sina.com>
 * @create：27/03/2019  8:30 PM
 * @see      https://gitee.com/d3li/March
 * @version 2.03.27
 * @describe
 */
class Income extends Rest
{

	public function index()
	{
		//$uid = session('user.id');
		$uid = input('uid' , 0, 'intval');
		if('get' == $this->method) {
			$result= array(
				'info'=>ShopUserinfo::get($uid)
			);
			$model = model('ShopOutcome');
			$result['month'] = $model->history('month', $uid);
			$result['today'] = $model->history('today', $uid);
			$result['list'] = $model->get_list('today', $uid);

			$result['url'] =[
				'Withdraw'=>[
					'title'=>'提现',
					'url'=>config('mobileUrl.Withdraw'),
				],
				'sumincome'=>[
					'title'=>'总收益',
					'url'=>config('mobileUrl.sumincome'),
				],
				'fans'=>[
					'title'=>'我的粉丝',
					'url'=>config('mobileUrl.fans'),
				],
				'UserStoreShow'=>[
					'title'=>'店铺管理',
					'url'=>config('mobileUrl.UserStoreShow'),
				],
			];

			return $this->success('', '', $result);
		}
		return $this ->error();
	}

	public function history($act = '')
	{
		$uid = input('uid' , 0, 'intval');
		$ary = ['today','yesterday','last7day'];
		$act = in_array($act, $ary) ? $act : 'last7day';
//		$page = input('page', 1, 'intval');
		if('get' == $this->method) {
			$model = model('ShopOutcome');
			$result['money'] = $model->history($act, $uid);
			$result['list'] = $model->get_list($act, $uid);
			return $this->success('', '', $result);
		}
		return $this ->error();
	}

	public function total()
	{
		if('get' == $this->method) {
			$result = model('ShopOutcome')->search(input('month'));
			return $this->success('', '', $result);
		}
		return $this ->error();
	}
}