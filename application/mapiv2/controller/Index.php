<?php

namespace app\mapiv2\controller;

use app\mapi\controller\commonapi\Mycommon;
use think\Db;
use think\controller\Rest;
use think\Response;
use app\admin\model\shop\GoodsHot;
use app\admin\model\shop\CoreArticle;
use app\admin\model\shop\GoodsCore;
use app\admin\model\shop\Tip;
use app\mapiv2\model\City;
use app\mapi\controller\Banner;
use app\mapi\config;
use think\Session;

class Index extends Mycommon{


    public function __construct()
    {

        parent::__construct();

    }
    public function rest(){
       switch ($this->method) {
            case 'post':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;
            case 'get':

                return $this->index();
                break;

            case 'delete':

                $comvali=$this->common();

                if($comvali){
                    return $this->error($comvali);
                }
                return $this->error('请求未定义');
                break;

            case 'put':

                $comvali=$this->common();
                if($comvali){
                    return $this->error($comvali);
                }

                return $this->error('请求未定义');
                break;

            default:
                return $this->error('未定义请求类型');
                break;
        }   
    }
    /**
     * 首页数据
     * @Author   ksea
     * @DateTime 2019-02-26T09:36:10+0800
     * @return   [type]                   [description]
     */
    public function index(){

        $Banner=new Banner();
        $tip=new Tip();
        $CoreArticle=new CoreArticle();
        $GoodsHot=new GoodsHot();
        $GoodsCore=new GoodsCore();

        $tip=$tip->where('status=1')->order('power desc,id')->select();
        $Banner_data=$Banner->rest()->getData();
        $Article = $CoreArticle->getArticle();
        $Hot     = $GoodsHot->getHot(6);
        $Core    = $GoodsCore->getCore(2);
        //城市列表
        $city = City::select();
        $id = session('user.id');
        $user = model('admin/shopuser/User')->where(['id'=>$id])->find();
        $code = isset($user['use_city_code'])?$user['use_city_code']:440300;
        foreach ($city as &$val){
            $val['selected'] = $val['city_code'] == $code?1:0;
        }
        $city_diff = 0;
        if(session('user.use_city_code') != $code){
            $city_diff = 1;
        }
        return $this->success(['banner_data'=>$Banner_data,'tip_data'=>$tip,'Article'=>$Article,'Hot'=>$Hot,'Core'=>$Core, 'city'=>$city, 'city_diff'=>$city_diff]);
    }
    /**
     * 设置城市
     * @return Response
     */
    public function setCity(){
        $city_code = input('city_code',440300);
        $open_id = input('open_id',440300);
        $user = model('admin/shopuser/user')->where(['openid'=>$open_id])->whereOr(['mini_openid'=>$open_id])->find();
        if(!$user){
            return $this->error('查无数据');
        }
        $uid = $user['id'];
        model('admin/shopuser/User')->where(['id'=>$uid])->update(['use_city_code'=>$city_code]);
        session('user',model('admin/shopuser/User')->where(['id'=>$uid])->find());
        return $this->success(['data'=>session('user')]);
    }
}