<?php

// 公共助手函数
use think\Db;
use think\session;

    /**
     * 无限极分类
     * @Author   ksea
     * @DateTime 2018-11-15T10:30:11+0800
     * @param    array                    $data  [description]
     * @param    integer                  $pid   [description]
     * @param    integer                  $level [description]
     * @return   [type]                          [description]
     */
    function getallcate($data=[],$pid=0,$level=0){
        static $list=[];
        static $k=0;
        static $count_data=0;
        if(isset($data)||empty($data)){
            //$where['id']=['in','1,2,3,7,8'];
            $data=Db::name('shop_categrory')->alias('c')->join('shop_goods sg','c.id=sg.cate','left')
                ->field('c.*,count(sg.id) count')
                ->group('c.id')
                ->where('c.status=1')
                ->order('power desc')
                ->select();
            $count_data=count($data);
        }
        foreach ($data as $key => $value) {
            if($value['pid']==$pid){
                 $value['level'] = $level;
                 $list[]=$value;
                 unset($data[$key]);
                 $k++;
                 getallcate($data,$value['id'],$level+1);
            }
        }
         return $list;
    }
    /**
     * 自定义错误日志位置
     * @Author   ksea
     * @DateTime 2019-07-17T00:34:04+0800
     * @param    [type]                   $filename [description]
     * @return   [type]                             [description]
     */
    function myErrorLog($filename,$data='',$group=''){

        $debug=true;

        $path = "errorlog/".date("Ymd",time());

        if($group) $path.='/'.$group;

        if (!is_dir($path)) mkdir($path,0777,true);

        $filepath=$path.'/'.$filename;
        
        if(!file_exists($filepath)) $debug=true;
        
        $file = fopen($filepath, 'a'); 
        
        fwrite($file, $data . '  ' . date('Y-m-d H:i:s'). PHP_EOL);
        
        if($debug) chmod($filepath,0777);
    }

    /**
     * 获取可用虚拟号
     * @Author   ksea
     * @DateTime 2019-08-12T10:31:48+0800
     * @return   [type]                   [description]
     */
    function getFictitious($redis='',$data='',$count='',$key='0'){


      if(($key>=$count)&&!empty($data)){
      
        return '没有可用虚拟号';
      
      }

      if(empty($redis)){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
      }

      if(empty($data)){
      
        $Binding_time = strtotime('- 3 day');
        $data=Db::name('rivacy_phone')->where('Binding_time','<',$Binding_time)->select();
        $count=count($data);
        if(empty($data)){
          return '没有可用虚拟号';
        }
      }

      $rivacyData=$data[$key];

      $rivacyPhone='rivacy_'.$rivacyData['phone'];

      if(!$redis->get($rivacyPhone)){
        
         return $rivacyData;
      
      }else{

         $key++;
      
         return getFictitious($redis,$data,$count,$key);
      
      }
    
    }
    /**
     * 设置虚拟号进入redis暂存1天
     * @Author   ksea
     * @DateTime 2019-08-12T11:35:45+0800
     * @param    [type]                   $phone    [description]
     * @param    [type]                   $serverid [description]
     */
    function setFictitious($phone,$serverid){

        $useData=60*60*24;
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        $rivacyPhone='rivacy_'.$phone;
        $redis->set($rivacyPhone,$serverid);
        $redis->EXPIRE($rivacyPhone, $useData);//一天过期
        $data=[
           'code' =>1,
           'msg'  =>'设置成功',
           'data' =>[$rivacyPhone,$serverid],
           'useData' =>$useData,
        ];
        return $data;

    }
    /**
     * 通用正则匹配
     * 
     * @Author   ksea
     * @DateTime 2018-12-28T19:51:32+0800
     * @param    [type]                   $text [数据]
     * @param    string                   $type [正则选择]
     * @param    boolean                  $ret  [false/ture]
     * @return   [type]                         
     */
    function preg($text,$type='number',$ret=false){
        $return ='';
        switch ($type) {
            case 'number':
                $return ="/^[0-9]*$/";
                break;
            case 'phone':
                $return ="/^1[345789]{1}\d{9}$/";
                break;
            case 'cards':
                 $return ="/^\d{15}|\d{18}$/";
                break;
            case 'EnAndNum':
                $return ="/^.{3,20}$/";
                break;
            case 'Email':
                $return = "/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/";
                break;
            case 'Url':
                $return = "/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(/.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+/.?/";
                break;
            default:
                # code...
                break;
        }
         if($ret){
             return $return;
         }
         else{
             return preg_match($return,$text);
         }

    }
    /**
     * 生成二维码
     * @Author   ksea
     * @DateTime 2018-12-03T15:13:02+0800
     * @param    string                   $url [description]
     * @return   [type]                        [description]
     */
    function creat_code($url='http://www.baidu.com',$filename='',$size=5,$magin=0,$new=false,$base_path='qrcode/')
    {
        if (!is_dir($base_path)){
            mkdir($base_path,0777,true);
        }
         $filename=isset($filename)?$filename:time();
         $name=$filename.'.png';
         $path=$base_path.$name;


        if(!is_file($path)||$new){
             Vendor('phpqrcode.phpqrcode');
             \QRcode::png($url,$path,'QR_ECLEVEL_L',$size,$magin);
            return '/'.$path;
        }
        else{
            return 'is_in_file';
        }
    }
    /**
     * 可以通用文件删除
     * @param  [type] $file      [description]
     * @param  string $base_path [description]
     * @return [type]            [description]
     */
    function del_code($file,$base_path='')
    {
        $base_path=empty($base_path)?config('code_path'):$base_path;
        $filename=$base_path.$file;
        if(is_file($filename)){
            return unlink($filename);
        }
        else{
            return false;
        }
    }
    /**
     * 获取文件列表
     * @return [type] [description]
     */
    function file_list($path='')
    {   
        $base_path=empty($path)?config('code_path'):$path;
        $list=scandir($base_path);
        return $list;
    }
    /**
     * 随机生成字符串
     * @Author   ksea
     * @DateTime 2018-11-16T17:03:14+0800
     * @param    integer                  $len    [description]
     * @param    string                   $format [description]
     * @return   [type]                           [description]
     */
    function randStr($len=8,$format='ALL') { 
         switch($format) { 
            
         case 'ALL':
         $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; break;
         case 'CHAR':
         $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-@#~'; break;
         case 'NUMBER':
         $chars='0123456789'; break;
         break;
         case 'NUMBER':
         $chars='0123456789'; break;
             case 'NO':
             $ddnumber=date("YmdHis").mt_rand(10,99);
             return $ddnumber;
         default :
         $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-@#~'; 
         break;
         }
         $password="";
         while(strlen($password)<$len)
            $password.=substr($chars,(mt_rand()%strlen($chars)),1);
         return $password;
     } 
     /**
      * 用户地址解析
      * @Author   ksea
      * @DateTime 2018-11-20T09:59:50+0800
      * @param    [type]                   $str [description]
      * @return   [type]                        [description]
      */
    function ana_region($str,$type='str'){
        $db= Db::name('region');
        $str_arr=explode(',', $str);
        $t=[];
        foreach ($str_arr as  $value) {
            $res_v=$db->where("id={$value}")->value('region_name');
            array_push($t, $res_v);
        }
        switch ($type) {
            case 'str':
                return implode(',', $t);
                break;
            case 'array':
                return $t;
                break;
            default:
                # code...
                break;
        }
    }
     /**
      *获取订单编号
      * @Author   ksea
      * @DateTime 2018-11-20T18:24:07+0800
      * @return   [type]                   [description]
      */
    function get_snorder($orderid,$k='snorder'){
        $r=Db::name('shop_order')->where("id={$orderid}")->value($k);
        return $r;
    }  
     /**
      *获取产品
      * @Author   ksea
      * @DateTime 2018-11-20T18:24:07+0800
      * @return   [type]                   [description]
      */
    function get_sngoods($orderid,$k='sngoods'){
        $r=Db::name('shop_order')->where("id={$orderid}")->value('goodsid');
        $a=Db::name('shop_goods')->where("id={$r}")->value($k);
        return $a;
    }
     /**
      * 某张表的某个字段是否已经存在某个值 存在 返回false 不存在返回true
      * [is_check_true description]
      * @Author   ksea
      * @DateTime 2018-11-23T14:45:47+0800
      * @param    [type]                   $val   [description]
      * @param    [type]                   $table [description]
      * @param    string                   $key   [description]
      * @return   boolean                         [description]
      */
     function is_check_true($val,$table,$key='id'){
        $data=
        [
            $key=>$val,
        ];
        $rule=
        [
            $key =>'unique:'.$table
        ];
        if(Validate()->make($rule)->check($data)){
            return true;
        }
        else
        {
            return false;
        }
     }
     /**
      * 数据库自定义查询
      * 可优化静态或者缓存
      * @param  [type]  $table [description]
      * @param  [type]  $val   [description]
      * @param  [type]  $field [description]
      * @param  string  $key   [description]
      * @return boolean        [description]
      * 2019-01-22 21:54:56
      */
     function is_get_val($table,$val,$field,$key='id'){
        if(!in_array($table,config('select_table_area')))return false;
        if(count(explode(',', $field))>=2){
            $c_val=Db::name($table)->where($key,'=',$val)->field($field)->find();
        }
        else{
            if(!in_array($field,config('select_field_area')))return false;
            $c_val=Db::name($table)->where($key,'=',$val)->value($field);
        }
        return $c_val;
     }
     /**
      * 价格校验
      * 订单提交前
      * @Author   ksea
      * @DateTime 2018-11-26T23:24:28+0800
      * @param    [type]                   $goods_id      [description]
      * @param    [type]                   $discount_data [description]
      * @param    [type]                   $price         [description]
      * @param    [type]                   $discount_arr  [description]
      * @return   boolean                                 [description]
      */
     function is_check_price($goods_id,$discount_data,$price,$discount_arr){

     }
     /**
      * 某几天时间格式化
      * {"20180114":[1515859200,1515945600],"20180119":[1516291200,1516377600],"20180219":[1518969600,1519056000],"20180319":[1521388800,1521475200]}
      *
        array(4) {
          [20180114] => array(2) {
            [0] => int(1515859200)
            [1] => int(1515945600)
          }
          [20180119] => array(2) {
            [0] => int(1516291200)
            [1] => int(1516377600)
          }
          [20180219] => array(2) {
            [0] => int(1518969600)
            [1] => int(1519056000)
          }
          [20180319] => array(2) {
            [0] => int(1521388800)
            [1] => int(1521475200)
          }
        }
      * @Author   ksea
      * @DateTime 2018-11-26T23:25:11+0800
      * @param    [type]                   $str [description]
      * @return   [type]                        [description]
      */
     function json_sleep_time($str='20180114,20180119,20180219,20180319',$is_json=true)
     {
        $json=array();
        $arr_json=explode(',',$str);
        foreach ($arr_json as $key => $value) {
            $json[$value]=return_area_time($value);
        }
        return $is_json?json_encode($json,true):$json;   
     }
     /**
      * 用户休息时间，json数据修改返回格式化
      * @Author   ksea
      * @DateTime 2018-11-27T00:23:28+0800
      * @param    [type]                   $json_sleep_time [description]
      * @return   [type]                                    [description]
      */
     function json_decode_time($json_sleep_time){
        $return_json=json_decode($json_sleep_time,true);
        foreach ($return_json as $key=>&$value) {
            $value=$key;
        }
        $return_json=implode($return_json, ',');
        return $return_json;
     }
     /**
      * 计算某天某人可约的时间/某人某天可约的时间
      * 返回
      * [
      *     '03:00',
      *     '04:00',
      *     '05:00',
      *     '05:00',
      * ]
      * @Author   ksea
      * @DateTime 2018-11-27T00:48:12+0800
      * @param    [type]                   $uid [description]
      * @return   [type]                        [description]
      */
     function translate_time($data='2018/11/27',$uid='3')
     {  
         static $sleep='';
         static $ret_array=[];
         static $i=0;
         static $worker_time='';
         if(isset($sleep)&&empty($sleep))$sleep=json_sleep_time(is_get_val('shop_userinfo','1','sleep'),false);
         if(isset($worker_time)&&empty($worker_time))$worker_time=config('worker_time');
         $temp=return_area_time($data);
         $where['appoint_start']=array('egt',$temp[0]);
         $where['appoint_end']=array('elt',$temp[1]);
         $where['sfuserid']=$uid;
         $temp_server=Db::name('shop_service_order')->where($where)->select();
         foreach ($temp_server as $k => $v) {
             /**********************获取一个天时间戳范围******00:00->24:00*********************/
             foreach ($worker_time as $key => $value) {
                 if((strtotime($data." ".$value)>=$v['appoint_start'])&&
                    (strtotime($data." ".$value." +1 hour")<=$v['fu_end']))
                 {
                    unset($worker_time[$key]);
                 }
             }
         }
         foreach ($worker_time as $key => $value) {
             array_push($ret_array,$value);
         }
          return  $ret_array;
     }
     /**
      * 格式输出，可选时间
      * @Author   ksea
      * @DateTime 2018-11-27T07:38:31+0800
      * @param    [type]                   $dat [description]
      * @return   [type]                        [description]
      */
     function style_translate_time($dat){
        $c_data=config('worker_time');
        foreach ($c_data as $key => &$value) {
             if(in_array($value,$dat)){
                $value=[$value,1];
             }
             else{
                $value=[$value,0];
             }
        }
        return $c_data;
     }
     /**
      * 返回一个cache里面没有的数字
      * @Author   ksea
      * @DateTime 2018-12-06T20:17:54+0800
      * @return   [type]                   [description]
      */
     // function get_unique(){

     //    $code=randStr('6',"NUMBER");
     //    dump($code);
     //    die;
     //    if(isset(cache($code))){
     //        get_unique();
     //    }
     //    else{
     //        return $code;
     //    }
     // }
     /**
      * 短信接口
      * @Author   ksea
      * @DateTime 2018-12-06T19:48:51+0800
      * @param    string                   $content  [description]
      * @param    string                   $phoneStr [description]
      * @return   [type]                             [description]
      */
    function send_sms($phoneStr='18588252668',$msg='')
    {
        $content=randStr('6',"NUMBER");
        $url='http://139.196.107.151:8868/sms.aspx';
        if(strlen($msg)<=0){
            $msg='【小哥帮】'.$content.'(平台验证码，十分钟之内有效)';
        }
        else{
            $msg='【小哥帮】'.$msg;
        }
        $post_data='action=send&userid=614&account=xiaogebang&password=tongtong53&content='.urlencode($msg).'&mobile='.$phoneStr;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec($ch);
        curl_close($ch);

        $r = simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);
        // print_r(sprintf('%s', $r->returnstatus));
        // print_r(sprintf('%s', $r->message));
        // print_r(sprintf('%s', $r->remainpoint));
        // print_r(sprintf('%s', $r->taskID));
        // print_r(sprintf('%s', $r->successCounts));
        $r=json_decode(json_encode($r),true);
        $data=[
            'code'          => $r['message'],
            'returnstatus'  => $r['remainpoint'],
            'taskID'        => $r['taskID'],
            'validate'      => $content
        ];
        return $data;
    }
     /**
      * 返某一天的时间范围
      * @Author   ksea
      * @DateTime 2018-11-27T03:16:46+0800
      * @param    string                   $data [description]
      * @return   [type]                         [description]
      */
     function return_area_time($data){
        return array(strtotime($data),strtotime($data." +1 day"));
     }
if (!function_exists('__')) {

    /**
     * 获取语言变量值
     * @param string $name 语言变量名
     * @param array $vars 动态变量值
     * @param string $lang 语言
     * @return mixed
     */
    function __($name, $vars = [], $lang = '')
    {
        if (is_numeric($name) || !$name)
            return $name;
        if (!is_array($vars)) {
            $vars = func_get_args();
            array_shift($vars);
            $lang = '';
        }
        return \think\Lang::get($name, $vars, $lang);
    }

}

if (!function_exists('format_bytes')) {

    /**
     * 将字节转换为可读文本
     * @param int $size 大小
     * @param string $delimiter 分隔符
     * @return string
     */
    function format_bytes($size, $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 6; $i++)
            $size /= 1024;
        return round($size, 2) . $delimiter . $units[$i];
    }

}

if (!function_exists('datetime')) {

    /**
     * 将时间戳转换为日期时间
     * @param int $time 时间戳
     * @param string $format 日期时间格式
     * @return string
     */
    function datetime($time, $format = 'Y-m-d H:i:s')
    {
        $time = is_numeric($time) ? $time : strtotime($time);
        return date($format, $time);
    }

}

if (!function_exists('human_date')) {

    /**
     * 获取语义化时间
     * @param int $time 时间
     * @param int $local 本地时间
     * @return string
     */
    function human_date($time, $local = null)
    {
        return \fast\Date::human($time, $local);
    }

}

if (!function_exists('cdnurl')) {

    /**
     * 获取上传资源的CDN的地址
     * @param string $url 资源相对地址
     * @param boolean $domain 是否显示域名 或者直接传入域名
     * @return string
     */
    function cdnurl($url, $domain = false)
    {
        $url = preg_match("/^https?:\/\/(.*)/i", $url) ? $url : \think\Config::get('upload.cdnurl') . $url;
        if ($domain && !preg_match("/^(http:\/\/|https:\/\/)/i", $url)) {
            if (is_bool($domain)) {
                $public = \think\Config::get('view_replace_str.__PUBLIC__');
                $url = rtrim($public, '/') . $url;
                if (!preg_match("/^(http:\/\/|https:\/\/)/i", $url)) {
                    $url = request()->domain() . $url;
                }
            } else {
                $url = $domain . $url;
            }
        }
        return $url;
    }

}


if (!function_exists('is_really_writable')) {

    /**
     * 判断文件或文件夹是否可写
     * @param    string $file 文件或目录
     * @return    bool
     */
    function is_really_writable($file)
    {
        if (DIRECTORY_SEPARATOR === '/') {
            return is_writable($file);
        }
        if (is_dir($file)) {
            $file = rtrim($file, '/') . '/' . md5(mt_rand());
            if (($fp = @fopen($file, 'ab')) === FALSE) {
                return FALSE;
            }
            fclose($fp);
            @chmod($file, 0777);
            @unlink($file);
            return TRUE;
        } elseif (!is_file($file) OR ($fp = @fopen($file, 'ab')) === FALSE) {
            return FALSE;
        }
        fclose($fp);
        return TRUE;
    }

}

if (!function_exists('rmdirs')) {

    /**
     * 删除文件夹
     * @param string $dirname 目录
     * @param bool $withself 是否删除自身
     * @return boolean
     */
    function rmdirs($dirname, $withself = true)
    {
        if (!is_dir($dirname))
            return false;
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirname, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }
        if ($withself) {
            @rmdir($dirname);
        }
        return true;
    }

}

if (!function_exists('copydirs')) {

    /**
     * 复制文件夹
     * @param string $source 源文件夹
     * @param string $dest 目标文件夹
     */
    function copydirs($source, $dest)
    {
        if (!is_dir($dest)) {
            mkdir($dest, 0755, true);
        }
        foreach (
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if ($item->isDir()) {
                $sontDir = $dest . DS . $iterator->getSubPathName();
                if (!is_dir($sontDir)) {
                    mkdir($sontDir, 0755, true);
                }
            } else {
                copy($item, $dest . DS . $iterator->getSubPathName());
            }
        }
    }

}

if (!function_exists('mb_ucfirst')) {

    function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }

}

if (!function_exists('addtion')) {

    /**
     * 附加关联字段数据
     * @param array $items 数据列表
     * @param mixed $fields 渲染的来源字段
     * @return array
     */
    function addtion($items, $fields)
    {
        if (!$items || !$fields)
            return $items;
        $fieldsArr = [];
        if (!is_array($fields)) {
            $arr = explode(',', $fields);
            foreach ($arr as $k => $v) {
                $fieldsArr[$v] = ['field' => $v];
            }
        } else {
            foreach ($fields as $k => $v) {
                if (is_array($v)) {
                    $v['field'] = isset($v['field']) ? $v['field'] : $k;
                } else {
                    $v = ['field' => $v];
                }
                $fieldsArr[$v['field']] = $v;
            }
        }
        foreach ($fieldsArr as $k => &$v) {
            $v = is_array($v) ? $v : ['field' => $v];
            $v['display'] = isset($v['display']) ? $v['display'] : str_replace(['_ids', '_id'], ['_names', '_name'], $v['field']);
            $v['primary'] = isset($v['primary']) ? $v['primary'] : '';
            $v['column'] = isset($v['column']) ? $v['column'] : 'name';
            $v['model'] = isset($v['model']) ? $v['model'] : '';
            $v['table'] = isset($v['table']) ? $v['table'] : '';
            $v['name'] = isset($v['name']) ? $v['name'] : str_replace(['_ids', '_id'], '', $v['field']);
        }
        unset($v);
        $ids = [];
        $fields = array_keys($fieldsArr);
        foreach ($items as $k => $v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $ids[$n] = array_merge(isset($ids[$n]) && is_array($ids[$n]) ? $ids[$n] : [], explode(',', $v[$n]));
                }
            }
        }
        $result = [];
        foreach ($fieldsArr as $k => $v) {
            if ($v['model']) {
                $model = new $v['model'];
            } else {
                $model = $v['name'] ? \think\Db::name($v['name']) : \think\Db::table($v['table']);
            }
            $primary = $v['primary'] ? $v['primary'] : $model->getPk();
            $result[$v['field']] = $model->where($primary, 'in', $ids[$v['field']])->column("{$primary},{$v['column']}");
        }

        foreach ($items as $k => &$v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $curr = array_flip(explode(',', $v[$n]));

                    $v[$fieldsArr[$n]['display']] = implode(',', array_intersect_key($result[$n], $curr));
                }
            }
        }
        return $items;
    }

}

if (!function_exists('var_export_short')) {

    /**
     * 返回打印数组结构
     * @param string $var 数组
     * @param string $indent 缩进字符
     * @return string
     */
    function var_export_short($var, $indent = "")
    {
        switch (gettype($var)) {
            case "string":
                return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                        . ($indexed ? "" : var_export_short($key) . " => ")
                        . var_export_short($value, "$indent    ");
                }
                return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, TRUE);
        }
    }

}

if (!function_exists('privacy_call')) {


	/**
	 * 虚拟号绑定
	 *
	 * @author d3li 2019/8/5
	 * @param string $mobile 关联号码二
	 * @param string $phone 关联号码一
	 * @return int
	 * @throws \think\exception\DbException
	 */
	function privacy_call($mobile, $phone)
	{
		$ps = model('RivacyPhone')::get(['using' => 0]);
		if ($ps) {
			$res = (new \wechat\Fictitious())->createphone($ps['phone'], $mobile, $phone, 259200);
			if (isset($res['resultdesc']) && $res['resultdesc'] == 'Success') {
				$ps->using = 1;
				$ps->Binding_time = request()->time();
				$ps->isUpdate(1)->save();
				Db::name('rivacy_phone_record')->insert([
					'callerNum' => $mobile,
					'calleeNum' => $phone,
					'relationNum' => $ps['phone'],
					'Binding_time' => $ps['Binding_time'],
					'subscriptionId' => $res['subscriptionId']
				]);
				return $ps['phone'];
			}
		}
		return 0;
	}

}

if (!function_exists('privacy_release')) {

	/**
	 * 虚拟号解绑
	 *
	 * @param string $privacy_phone 虚拟号码
	 * @return int | mixed
	 */
	function privacy_release($privacy_phone)
	{
		$ret = (new \wechat\Fictitious())->Untying($privacy_phone);
		if ($ret['resultcode'] == 0) {
			return model('RivacyPhone')->where('phone', $privacy_phone)
				->update(['using' => 0]);
		}
		return 0;
	}

}
    /**
     * 权限分配
     * @Author   baohb
     * @DateTime 2018-12-17T10:30:11+0800
     */
     function power_judge()
     {
         $uid = Session::get('admin.id');
         $res =Db::name('auth_group_access')
                ->alias('a')
                ->join('admin b','a.uid=b.id','left')
                ->join('auth_group c','a.group_id=c.id','left')
                ->field('c.rules,a.group_id')
                ->where(['b.id'=>$uid])
                ->find();
         if ($res['rules'] != "*" && $res['group_id']!=8) {
             return $uid;
         }
     }

        /**
         * 软删除操作
         * @Author   baohb
         * @DateTime 2018-12-17T10:30:11+0800
         */
        function hidden($data)
        {
            $data['status'] = 2;
            $res = Db::name('shop_index')->where(['id'=>$data['id']] and ['admin_id'=>$data['admin_id']])->update($data);
            if ($res){
                return true;
            }else{
                return false;
            }
        }

        /**
         *生成编号，格式：
         *年月日小时分秒毫秒+随机4位数字
         *
         */
        function create_no(){
        	$ddnumber=date("YmdHis").mt_rand(10,99);
        	return $ddnumber;
        }

      /**
       * [SwitchTime description]
       * @Author   ksea
       * @DateTime 2019-04-22T15:00:14+0800
       */
      function switchtime($time='2018-1-12',$pt='01:00~12:00'){
          $arr_hour=explode(':', $pt);
          $hour=$arr_hour[0];
          $time=strtotime("+{$hour} hours",strtotime($time));
          return $time;
      }
      /**
       * curlpost
       * @Author   ksea
       * @DateTime 2019-06-11T21:55:33+0800
       * @return   [type]                   [description]
       */
      function curlpost($url,$post_data){

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
          $output = curl_exec($ch);
          curl_close($ch);
          $out=json_decode($output,true);
          return $out;
      }
