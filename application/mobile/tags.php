<?php
return [
	/**
	 * 帮助注册获取一个，注册者获取一个红包
	 */
	// 'send_hb'=>[
	// 	'app\\mobile\\behavior\\Sendhb',
	// ],
	/**
	 *
	 * 注册者获取一个红包
	 * 
	 */
	'send_hb_toregist'=>[
		'app\\mobile\\behavior\\Sendhb',
	],
	'award'			  =>[
		'app\\mobile\\behavior\\Award',
	],
	/**
	 * 添加用户
	 */
	'add_user'		  =>[
		'app\\mobile\\behavior\\User',
	],

    /**
     * 个人二维码基础列
     */
    'base_code' =>[
        'app\\mobile\\behavior\\Usercode',
    ],


	/**
	 * 生成个人二维码
	 */
	'set_code'		  =>[
		'app\\mobile\\behavior\\User',
	],

	'set_user'		  =>[
		'app\\mobile\\behavior\\User',
	],
    'add_orderno' 	  =>[
        'app\\mobile\\behavior\\Orderno',
    ],

    
    /**
     * 转换店铺
     */
    'switch_store'	  =>[
    	'app\\mobile\\behavior\\Switchstore',
    ],

    /**
     * 自定义付款分销
     * 2019-01-22 22:41:08
     */
    'share_self'	  =>[
    	'app\\mobile\\behavior\\Shareself',
    ],
    /**
     * 会员支付钩子
     * 2019-03-21 02:07:16
     */
    'vippay'	  =>[
    	'app\\mobile\\behavior\\Vippay',
    ],
    /**
     * 个人粉丝绑定二维码
     */
    'fans_code'  =>[
        'app\\mobile\\behavior\\Fans',
    ],
    /**
     * 粉丝切换钩子
     */
    'fansswitch'  =>[
    	'app\\mobile\\behavior\\Fansswitch',
    ],

    /**
     * 小店创建钩子
     */
    'add_store' =>[
        'app\\mobile\\behavior\\Shopuserinfostore',//小店构建钩子
        'app\\mobile\\behavior\\Bean',             //小哥豆包初始化钩子
    ],

];