<?php

namespace app\mobile\behavior;

use think\Db;
use think\Log;

class Sendhb{

	public function run(&$param){
		if(isset($param['func'])){
			$func =$param['func'];
		}
		else{
			$func='send_hb';
		}
		$this->$func($param);
	}
	/**
	 * 
	 * @Author   ksea
	 * @DateTime 2018-11-23T20:08:38+0800
	 * @param    [type]                   &$param [array('uid','from_id')] 注册这id ，推荐者id
	 * @return   [type]                           [description]
	 */
	public function send_hb(&$param){
		$data=[
			'name'=>'注册奖励',
			'uid'=>$param['uid'],
			'dis_type'=>1,
			'discount'=>'',
			'valitime'=>time()+(60*60*24),
			'status'=>0,
			'from_id'=>$param['from_id']?$param['from_id']:0,
		];
		Db::startTrans();
		try{
			Db::name('shop_discount')->insertGetId($data);
			Db::name('shop_userinfo')->where('id','=',$param['uid'])->setInc('discount_times');
			
			/**************************推荐这获取优惠卷************************************/
			if(isset($param['from_id'])){
				$data['uid']=$param['from_id'];
				$data['dis_type']=2;
				$data['from_id']=$param['uid'];
				Db::name('shop_discount')->insertGetId($data);
				Db::name('shop_userinfo')->where('id','=',$param['from_id'])->setInc('discount_times');
			}
		    Db::commit();    
		} catch (\Exception $e) {
			Log::info([
				'error' => '优惠券发送错误',
				'send_hb' =>$param,
			]);
		    Db::rollback();
		}

	}

	/**
	 * 
	 * @Author   ksea
	 * @DateTime 2018-11-23T20:08:38+0800
	 * @param    [type]                   &$param [array('uid')] 注册这id ，推荐者id
	 * @return   [type]                           [description]
	 */
	public function send_hb_toregist(&$param){
		$data=[
			'name'=>'注册奖励',
			'uid'=>$param['uid'],
			'dis_type'=>1,
			'discount'=>'',
			'valitime'=>time()+(60*60*24),
			'status'=>0,
			'from_id'=>0,
			'create_time' =>time(),
			'update_time' =>time(),
		];
		Db::startTrans();
		try{
			Db::name('shop_discount')->insertGetId($data);
			Db::name('shop_userinfo')->where('id','=',$param['uid'])->setInc('discount_times');
		    Db::commit();    
		} catch (\Exception $e) {
			Log::info([
				'error' => '优惠券发送错误',
				'send_hb' =>$param,
			]);
		    Db::rollback();
		}

	}
}