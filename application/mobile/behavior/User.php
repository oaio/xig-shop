<?php

namespace app\mobile\behavior;

use think\Db;
use think\Log;
use wechat\Api;

class User
{
	public function run(&$param){
		if(isset($param['func'])){
			$func =$param['func'];
		}
		else{
			$func='add_user';
		}
		$this->$func($param);
	}
	/**
	 * 添加用户钩子
	 * @Author   ksea
	 * @DateTime 2018-11-28T15:39:33+0800
	 */
	public function add_user(&$param){
		$res=model('admin/shopuser/User')->add_user($param['request']);
		if(!empty($res) && !empty($res['data'])){
            $param['uid']=$res['data'];
        }
		if(isset($res['code'])&&$res['code']=1){
			return json_encode($res,true);
		}
		else{
			if(isset($res['code'])){
				$rst=$res;
			}
			else{
				$rst['code']=5;
				$rst['mesage']=$res;
			}
			return json_encode($rst,true);
		}
	}
	/**
	 * 生成个人的二维码
	 * 某个操作生成
	 * @Author   ksea
	 * @DateTime 2018-12-03T17:50:29+0800
	 */
	public function set_code(&$param){
		if(!is_get_val('shop_userinfo_code',$param['request']['id'],'usercode')||!is_file('/qrcode'.is_get_val('shop_userinfo_code',$param['request']['id'],'usercode'))){
			//$rst=creat_code('http://'.$_SERVER['HTTP_HOST'].'?pid='.$param['request']['id'],microtime(TRUE));
			$rst=Api::getInstance()->qrcode('user|mobile|index|index|pid|'.$param['request']['id'],microtime(TRUE),'qrcode');
			if(!is_array($rst)){
				$param['request']['data']=$rst;
				Db::name('shop_userinfo_code')->where('uid','=',$param['request']['id'])->update(['usercode'=>$rst]);
			}
			else{
				$param['request']['data']=json_encode($rst);
			}
		}
		else{
			    $param['request']['data']='is_in_file';
		}
	}
}