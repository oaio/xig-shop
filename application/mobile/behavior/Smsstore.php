<?php

namespace app\mobile\behavior;

use think\Log;

class Smsstore{
	public function run(&$param){
		controller('api/commonapi/ajaxcommon')->sms_store($param['phone'],$param['msg']);
	}
}