<?php

namespace app\mobile\behavior;

use think\Db;
use think\Log;
use app\admin\model\shop\ShopFans;
use wechat\Api;

class Fans
{	
	/**
	 * [run description]
	 * @Author   ksea
	 * @DateTime 2019-05-06T20:06:17+0800
	 * @param    [type]                   &$param [description]
	 * @return   [type]                           [description]
	 */
	public function run(&$param){
		if(!is_get_val('shop_userinfo_code',$param['request']['id'],'fanscode')||!is_file('/fanscode'.is_get_val('shop_userinfo_code',$param['request']['id'],'fanscode'))){
			//$rst=creat_code('http://'.$_SERVER['HTTP_HOST'].'/mobile/userstore/storeshow/to/'.$param['request']['id'],microtime(TRUE),$size=5,$magin=0,$new=false,$base_path='fanscode/');
			
 			$rst=Api::getInstance()->qrcode('fans|mobile|userstore|storeshow|to|'.$param['request']['id'],microtime(TRUE),'fanscode');
			if(!is_array($rst)){
				$param['request']['data']=$rst;
				Db::name('shop_userinfo_code')->where('uid','=',$param['request']['id'])->update(['fanscode'=>$rst]);
			}
			else{
				$param['request']['data']=json_encode($rst);
			}
		}
		else{
			    $param['request']['data']='is_in_file';
		}
	}

}

?>