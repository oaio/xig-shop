<?php

namespace app\mobile\behavior;

use think\Db;
use think\Log;
/**
 * 自定义付款分销钩子
 * 2019-01-22 22:42:56
 */
class Shareself
{

	public function run(&$param){
		  model('admin/shop/distribution')->share_self($param['id']);
	}

}


?>