<?php

namespace app\mobile\behavior;
use wechat\DiytTemplate;

class WechatTemplate
{
    //关联预约决定数据是最后一挑
    public function run(&$param){

       $template_id = config('wechattemplate.appendpay')['tempid'];
       $link        = config('wechattemplate.appendpay')['link'];

       $Template=new DiytTemplate($template_id);

       $server=model('admin/shop/Serverorder')->with(['order'])->where('orderid',$param['oid'])->order('id desc')->find();
       if(count($server['emp'])>0){
       		$openid_arr=[];
       		foreach ($server['emp'] as $key => $value) {
       			if(isset($value->openid)){
       				array_push($openid_arr,$value->openid);
       			}
       		}

    			$data = [
    				'first' => '嘿，客户新追加了一笔金额，请注意查看哦！',
    				'keyword1' => $server['order']['goods_name'],
    				'keyword2' => '追加',
    				'keyword3' => round($param['total_fee']/100,2),
    				'keyword4' => '已支付',
    				'keyword5' => date('Y-m-d H:i:s',$param['create_time']),
    				'remark' => '请及时确认订单信息及金额！',
    			];
       		$res=$Template->SendMessage($openid_arr,$data,$link);

       		return $res;
       }
       else{
       	    myErrorLog('Msg.txt',$server,'WechatTemplate');
       }


    }
}