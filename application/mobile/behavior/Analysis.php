<?php


namespace app\mobile\behavior;


class Analysis
{
    //添加参与拼团记录钩子
    public function run(&$param){

    	$order['id']=$param['oid'];
    	
		$order=model('admin/shop/order')->get($order['id']);

		myErrorLog('analysis.txt',json_encode(['分析数据',$order]),'analysis');

		model('admin/shop/AnalysisStore')->analysis($order);

		model('admin/shop/AnalysisBuy')->analysis($order);

		model('admin/shop/AnalysisMix')->analysis($order);

		model('admin/shop/AnalysisTj')->analysis($order);

    }
}