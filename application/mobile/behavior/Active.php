<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mobile\behavior;
use think\Exception;
use wechat\Cache;

/**
 * Active.php 更新过期的活动
 *
 * @author d3li <d3li@sina.com>
 * @create：12/04/2019  10:57 AM
 * @see      https://gitee.com/d3li/April
 * @version 2.04.12
 * @describe
 */
class Active
{

	/**
	 * 运行更新
	 *
	 * @param $params
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function run(&$params)
	{
		\app\admin\model\shop\Active::expire();
	}

	/**
	 * 邀请有奖
	 *
	 * @param $user
	 */
	public function invite(&$user)
	{
		try {
			$mod = model('\app\admin\model\shop\Invite');
			$mod::create([
				'uid' => $user['id'],
				'fuid' => $user['pid'],
				'invite_city' => $user['use_city_code'],
				'origin' => isset($user['type']) ? $user['type'] : '二维码',
				'invitee' => $user['name'],
				'avatar' => $user['pic'],
				'tel' => $user['phone'],
			]);
			$count = $mod->where(['fuid' => $user['pid'], 'status' => 0])->count();
			if(10 > $count) return;
			$zero = strtotime(date('Ymd'));
			$max = Cache::get($zero);
			$config = model('config')::get(['name' => 'invite']);
			if(empty($config)) return;
			$config = $config['value'];
			if($config[2] && 1 > $max) return;
		}catch (Exception $e){}
	}
}
