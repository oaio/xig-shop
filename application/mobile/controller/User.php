<?php


namespace app\mobile\controller;

use think\Db;
use app\mobile\common\Controllercom;
use think\Session;
use think\Cache;
use app\api\commonapi\Ajaxcommon;
use wechat\WechatAuth;

class User extends Controllercom
{
    public static $model;
    public function _initialize()
    {
        self::$model=model('admin/shopuser/User');
        parent::_initialize();
    }

    /**
     * 登入
     * @Author   ksea
     * @DateTime 2018-12-06T19:32:53+0800
     * @return   [type]                   [description]
     */
    public function login(){
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/
        if(Session::get('user')){
            $this->redirect("user/index");
        }
        /*******************判断用户是否已经登陆，登陆用户直接跳转个人中心**************************/
        /*******************登陆信息验证**************************/
        if(input('post.')){
             $data=[
                'phone'  => input('post.phone',''),
                'passwd' => input('post.passwd','')?input('post.passwd','','md5'):input('post.passwd',''),
             ];
             $rule=[
                'phone'  => 'require|number|max:11|/^1[3-9]{1}[0-9]{9}$/',
                'passwd' => 'require',
             ];
             $msg=[
                 'phone.require'  => '手机号码不能为空',
                 'phone.number'   => '手机号码只可以是数字',
                 'phone.max'      => '请输入正确的11位手机号',
                 'phone./^1[3-9]{1}[0-9]{9}$/'      => '请输入正确的手机号码',
                 'passwd.require' => '密码不为空',
             ];
             if(validate()->make($rule,$msg)->check($data)){
                 $where_user['phone']=$data['phone'];
                 $user=model('admin/shopuser/User')->where($where_user)->find();
                 if(!empty($user)){
                      $where_user['passwd']=$data['passwd'];
                      unset($user);
                      $user=model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where($where_user)->find();
                      if(!empty($user)){
                            Session::set('user',$user->toArray());
                            //记录用户登陆
                            model('admin/shop/AnalysisMix')->loginlog($user->toArray(),1);
                            $this->success('登陆成功',url('User/index'),session('user'));
                      }
                      else{
                         $this->error('密码错误');
                      }
                 }
                 else{
                    $this->error('账号错误');
                 }
                 $this->error($m);
             }
             else{
                $m=validate()->make($rule,$msg)->GetError();
                $this->error($m);
             }
        }
        /*******************登陆信息验证**************************/
        return $this->fetch();
    }


    /**
     * 登出
     * @Author   ksea
     * @DateTime 2018-12-06T19:33:02+0800
     * @return   [type]                   [description]
     */
    public function logout(){
        if(Session::clear()&&Session::get('user')==NULL){
            $this->error('登出失败');
        }
        else{
            $this->success('登出成功','mobile/index/index');
        }
    }
    public function IndexWorker(){
        if(!Session::get('user')){
           $this->redirect('User/login');
        }
        if(is_get_val('shop_userinfo',session('user.id'),'type')==0){
            $this->redirect('User/index');
            exit();
        }
        return $this->fetch();
    }
    /**
     * 个人中心
     * @Author   ksea
     * @DateTime 2018-12-06T19:33:19+0800
     * @return   [type]                   [description]
     */
    public function index(){
        if(!Session::get('user')){
           $local_res='https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
           session('loc_url',$local_res);
           $this->redirect('User/login');
        }
        $u_data=session('user');
        // $mystring    = '{"subscribe":0';
        // $pos = stripos($u_data['json_wechat'],$mystring);
        // if(is_numeric($pos)){
        //     $appsecret=config('wechat.secret');
        //     $appid=config('wechat.appid');
        //     $api=new WechatAuth($appid,$appsecret);
        //     $wechat_userdata=$api->getUserInfo(session('user.openid'),'zh_CN');
        //     self::$model->subscribe(session('user.id'),$wechat_userdata);
        // }
        $this->assign('__token__',request()->token());
        return $this->fetch();
    }
}