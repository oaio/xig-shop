<?php
namespace app\mobile\controller;

use app\mobile\common\Controllercom;
use think\Db;
use think\Hook;
use wechat\Jssdk;

/**
 * 拼团
 */
class Fightgroup extends Controllercom
{
    public function _initialize()
    {
        parent::_initialize();
    }
    public function group_order(){

        $Jssdk=new Jssdk(config('wechat.appid'),config('wechat.secret'));
        $jssdk_res=$Jssdk->getSignPackage();

        $this->assign('jssdk',$jssdk_res);
        return $this->fetch();
    }


}
