<?php

namespace app\mobile\controller;

use app\mobile\common\Controllercom;
use think\Db;
use \wechat\Api;
use Illuminate\Support\Facades\Redis;


class H5 extends Controllercom{


    /**
     * 商品列表页面
     */

    public function goodsList(){
        return $this->fetch();
    }

	/**
	 * 商品的详情
	 */
	public function goods_detail(){
        if(request()->isAjax()){
            $id = input('param.id');
            $store_id = input('param.store_id');
            $platform_code = input('param.platform_code')?input('param.platform_code'):1;
            $data=[
                'store_id' =>$store_id,
                'id' =>$id,
            ];

            $rule=[
                'store_id' =>'require|min:1',
                'id' =>'require|min:1',
            ];

            $msg=[
                'store_id.require'=>'店铺id不允许为空',
                'id.require'=>'商品id不能为空',
            ];

            if(!validate()->make($rule,$msg)->check($data)){
                return $this->error(validate()->make($rule,$msg)->getError(),request()->domain());
            } else{
                $where=[];
                if(isset($id))$where['g.id']=$id;
                //前台店铺获取商品条件
                if(strlen($store_id)>0){

                    $where['g.admin_id']=is_get_val('store',$store_id,'admin_id','store_id');
                }
                $detail =Db::name('shop_goods')->alias('g')
                    ->join('shop_goods_detail sg','g.id=sg.goodsid','left')
                    ->where($where)->find();
                if(empty($detail)){

                    $ret_data['url']=config('mobileUrl.sureBuy');
                    $ret_data['msg']='';
                    $ret_data['data']=$detail;
                    return $this->error('没有数据','',$ret_data);

                }
                else{
                    $detail['pic']=explode(',',$detail['pic']);
                    $ret_data['url']=config('mobileUrl.sureBuy');
                    $ret_data['msg']='';
                    $ret_data['data']=$detail;
                    $res = Api::getInstance()->qrcode("goods|mobile|goods|goods_detail|s|1|id|$id|jj|$platform_code|to|1|store_id|1", 'h5_'.$platform_code.'_'.$id, 'qrcode/goods');
                    $ret_data['qrcode'] = $res;
                    return $this->success('获取成功','',$ret_data);
                }
            }
        }

        /*******************登陆检测**************************/
		return $this->fetch();
	}

    /**
     * 商品分类
     */
	public function goodsType(){

        $where['c.id']=['in','1,2'];
        $data = Db::name('shop_categrory')->alias('c')->join('shop_goods sg','c.id=sg.cate','left')
            ->field('c.*,count(sg.id) count')
            ->where($where)
            ->group('c.id')
            ->order('power desc')
            ->select();
        $ret_data['url']=config('mobileUrl.category');
        $ret_data['msg']='';
        $ret_data['data']=$data;

        return $this->success('数据捕获成功','',$ret_data);
    }

}

