<?php
namespace app\mobile\controller;

use app\mobile\common\Controllercom;
use think\Controller;
use think\Hook;
use think\Log;
use wechat\Helper;
use wechat\Wepay;

/**
 * 正规锚点
 * @Author   ksea
 * @DateTime 2019-05-08T11:22:45+0800
 */
class Pay extends Controllercom {
	public function _initialize() {
		parent::_initialize();
	}
	/**
	 * @return [type] [description]
	 * 回调主体
	 */
	public function notify() {
		$api = Wepay::getInstance();
		$res = $api->notify();
		Log::info($res);
		if (isset($res['return_code']) && $res['return_code'] == 'SUCCESS') {
			$row = model('WechatPay')->get(['transaction_id' => $res['transaction_id']]);
			if ($row) {
				exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
			}

			$type = $res['attach'];
			$temp = explode('_', $type);
			$type = $temp[0];
			$res['attach'] = $temp[0];

			switch ($type) {
			case '1':
				$this->ordernotify($res); //订单支付回调
				break;
			case '2':
				$res['note'] = $temp[1];
				$this->selfnotify($res); //自定义支付回调
				break;
			case '3':
				$this->appendnotify($res); //追加支付回调
				break;
			case '4':
				$this->assemblenotify($res); //拼团支付回调
				break;
			case '5':
				$this->vipnotify($res); //会员卡购买回调
				break;
			default:
				$this->ordernotify($res); //订单支付回调
				break;
			}
		}
		exit(Helper::array2xml(['return_code' => 'FAIL', 'return_msg' => 'unknown']));
	}
	/**
	 * @return [type] [description]
	 * 普通订单回调 type =1
	 */
	public function ordernotify($res) {
		$model = model('WechatPay')->allowField(true);
		$order = model('admin/shop/order')->get(['snorder' => $res['out_trade_no']]);
		$ary = [
			'oid' => isset($order['id']) ? $order['id'] : '',
			'openid' => $res['openid'],
			'mch_id' => $res['mch_id'],
			'trade_no' => $res['out_trade_no'],
			'transaction_id' => $res['transaction_id'],
			'attach' => $res['attach'],
			'total_fee' => $res['total_fee'],
			'result_code' => $res['result_code'],
			'is_subscribe' => $res['is_subscribe'],
			'pay_time' => $res['time_end'],
			'status' => 1,
			'create_time' => time(),
		];
		$model->save($ary);
		if (true) {
			$order_actualpay = $order['actualpay'] * 100;
			if (!empty($order) /*&&intval($order_actualpay)==intval($res['total_fee'])*/) {
				$data = [
					'id' => $order['id'],
					'status' => '1',
					'is_pay' => '1',
					'pay_id' => $model->id,
				];
				$res = model('admin/shop/order')->update_order($data);

				model('admin/shop/goods')->where('id', $order['goodsid'])->setInc('deal_times'); //真实数量

				model('admin/shop/goods')->where('id', $order['goodsid'])->setInc('virtual_times'); //虚假数量

				/**********分销钩子************/
				if (is_get_val('shop_userinfo', $order['uid'], 'store_id', 'id') != 0) {
					$param = [
						'id' => $order['id'],
					];
					Hook::listen('distribution', $param);
				}
				/**********分销钩子************/

				/*************虚拟优惠券使用抵扣****************/
				if ($order['discount_id'] > 0) {
					model('admin/shop/Virdiscount')->payDis($order['discount_id'], $order['id']);
				}
				/*************虚拟优惠券使用抵扣****************/

				/*************贡献钩子*****************/
				$Outcome = ['oid' => $order['id']];
				Hook::listen('outcome', $Outcome);
				/*************贡献钩子*****************/

				/*************订单模板推送*****************/
				$orderbuy = ['oid' => $order['id']];
				Hook::listen('order', $orderbuy);
				/*************订单模板推送*****************/

				$Cachedata = model('admin/shop/OrderTemp')->where('orderid', $order['id'])->find(); //Cache::get('order_'.$order['id']);

				$cares = controller('api/commonapi/Scan')->CreateScanCode($Cachedata['orderid'], $Cachedata['appoint_start'], $Cachedata['sfuserid'], $Cachedata['areaid'], $Cachedata['address'], $Cachedata['note']);

				/**********************刷数据=>默认吧扫码数据变成服务完成+cloes不完整**************************/



				if (
					isset($Cachedata['eventtype']) &&
					$Cachedata['eventtype'] == 'storescan'
				) {
					$NewOrderData = [
						'id' => $order['id'],
						'schedule' => 7,
					];
					$NewServerData = [
						'id' => $cares['server'],
						'is_ask' => 1,
						'sfuserid' => 58,
						'status' => 4,
						'fu_end' => time(),
						'push_time' => time(),
					];
					model('admin/shop/Order')->update($NewOrderData);
					model('admin/shop/Serverorder')->update($NewServerData);

				}

				/**********************刷数据=>默认吧扫码数据变成服务完成+cloes不完整**************************/

				if ($cares && isset($cares['msg']) && $cares['msg'] == '预约成功') {
					model('admin/shop/OrderTemp')->where('orderid', $Cachedata['orderid'])->update(['status' => 1]);
					//Cache::rm('order_'.$order['id']);

				}

				/**********************数据分析,统计****************************/

				/***获取订单最新数据****/

				$params['oid']=$order['id'];
			    Hook::exec('app\\mobile\\behavior\\Analysis','run',$params);

				/**********************数据分析,统计****************************/

				/**********************返利回调20190723******************************/

				$BackData=model('admin/shop/CashBack')->GetBack($order['id'],$order['uid']);
				if($BackData['code']==1){
					$total=$BackData['data'];
					//返利
					model('mapiv2/RedEnvelopes')->saveredenvelopespond($total, $order['uid']);
					//消费抵扣
					model('admin/shop/RedEnvelopesRecord')::add(4, $total, $order['id'], '消费返现');
				}
				else{
					//记录错误日志
					myErrorLog('getback.txt',json_encode(['返利错误数据',$BackData,$order]),'pay');
				}

				/**********************返利回调******************************/

				/************扭转记录********************/
				model('admin/shop/ShopOrderHistory')::create([
					'oid' => $order['id'],
					'desc' => $order['nickname'] . ' 下单成功',
				]);
				/************扭转记录********************/

		        /*******************公共普通订单支付锚点***********************/
		         Hook::listen('orderpay',$order);
		        /*******************公共普通订单支付锚点***********************/

			} else {
				Log::info([
					'type' => 'error',
					'parameter' => $res,
					'msg' => '订单金额错误',
					'data' => $order,
				]);
			}

		}
		exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
	}
	/**
	 * 自定义金额   type=2
	 * 回调
	 * @return [type] [description]
	 */
	public function selfnotify($res) {
		Log::info('+++++++++++++++++++----------------++++++++++++++++++++');
		Log::info($res);
		Log::info('+++++++++++++++++++----------------++++++++++++++++++++');
		$model = model('WechatPay')->allowField(true);
		$ary = [
			'oid' => '',
			'openid' => $res['openid'],
			'mch_id' => $res['mch_id'],
			'trade_no' => $res['out_trade_no'],
			'transaction_id' => $res['transaction_id'],
			'attach' => $res['attach'],
			'total_fee' => $res['total_fee'],
			'result_code' => $res['result_code'],
			'is_subscribe' => $res['is_subscribe'],
			'pay_time' => $res['time_end'],
			'status' => 1,
			'create_time' => time(),
			'note' => $res['note'],
		];
		$model->save($ary);
		model('admin/shop/ShopPaySelf')::create([
			'openid' => $res['openid'],
			'no' => $res['out_trade_no'],
			'money' => $res['total_fee'],
			'is_pay' => 1,
			'note' => $res['note'],
		]);
		/**********自定义分销付款钩子************/
		if (is_get_val('shop_userinfo', $res['openid'], 'store_id', 'openid') != 0) {
			$param['id'] = $model->id;
			Hook::listen('share_self', $param);
		}
		/**********自定义分销付款钩子************/

        /*******************公共自定义支付锚点***********************/
         Hook::listen('custompay',$ary);
        /*******************公共自定义支付锚点***********************/
		
		exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
	}
	/**
	 * 订单金额追加 type =3
	 * 回调
	 * @return [type] [description]
	 */
	public function appendnotify($res) {
		$model = model('WechatPay')->allowField(true);
		$snorder = explode('_', $res['out_trade_no']);
		$order = model('admin/shop/order')->get(['snorder' => $snorder[0]]);
		$ary = [
			'oid' => isset($order['id']) ? $order['id'] : '',
			'openid' => $res['openid'],
			'mch_id' => $res['mch_id'],
			'trade_no' => $res['out_trade_no'],
			'transaction_id' => $res['transaction_id'],
			'attach' => $res['attach'],
			'total_fee' => $res['total_fee'],
			'result_code' => $res['result_code'],
			'is_subscribe' => $res['is_subscribe'],
			'pay_time' => $res['time_end'],
			'status' => 1,
			'create_time' => time(),
		];
		$model->save($ary);

		$UserWantPayModel=model('admin/shop/UserWantPay');
		
		$UserWantPay=$UserWantPayModel->where('orderid',$order['id'])->order('id desc')->find();

		$UserWantPayModel->save(['is_pay'=>'1'],['id'=>$UserWantPay['id']]);


		/******************扣除用户钱********************/

		if ($UserWantPay['discount_id'] > 0) {

			model('admin/shop/Virdiscount')->payDis($UserWantPay['discount_id'],$ary['oid']);
			
		}
		
		/******************扣除用户钱********************/

		/**********************返利回调20190723******************************/

		$order=model('admin/shop/Order')::get($ary['oid']);
		$disMoney=round($ary['total_fee']/100,2);
		$BackData=model('admin/shop/CashBack')->disMoney($disMoney,$order['uid'],$order['id']);
		if($BackData['code']==1){
			$total=$BackData['data'];
			//返利
			model('mapiv2/RedEnvelopes')->saveredenvelopespond($total, $order['uid']);
			//消费抵扣
			model('admin/shop/RedEnvelopesRecord')::add(4, $total, $order['id'], '订单追加返利');
		}
		else{
			//记录错误日志
			myErrorLog('getback.txt',json_encode(['返利错误数据',$BackData,$order]),'pay');
		}

		/**********************返利回调******************************/

        /*******************公共追加支付锚点***********************/
        Hook::listen('addpay',$ary);
        /*******************公共追加支付锚点***********************/
		exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
	}

	/**
	 * @return [type] [description]
	 * 拼团订单回调 type =4
	 * 、不做统一回调
	 */

	public function assemblenotify($res) {
		$model = model('WechatPay')->allowField(true);
		$snorder = explode('_', $res['out_trade_no']);
		$order = model('admin/shop/order')->get(['snorder' => $snorder[0]]);
		$ary = [
			'oid' => isset($order['id']) ? $order['id'] : '',
			'openid' => $res['openid'],
			'mch_id' => $res['mch_id'],
			'trade_no' => $res['out_trade_no'],
			'transaction_id' => $res['transaction_id'],
			'attach' => $res['attach'],
			'total_fee' => $res['total_fee'],
			'result_code' => $res['result_code'],
			'is_subscribe' => $res['is_subscribe'],
			'pay_time' => $res['time_end'],
			'status' => 1,
			'create_time' => time(),
		];
		$model->save($ary);
		/************************新增代码*********************************/
		if (true) {
			$order_actualpay = $order['actualpay'] * 100;
			if (!empty($order) && $order_actualpay == $res['total_fee']) {
				$data = [
					'id' => $order['id'],
					'status' => '1',
					'is_pay' => '1',
					'pay_id' => $model->id,
				];
				$res = model('admin/shop/order')->update_order($data);
				/**********分销钩子************/
				if (is_get_val('shop_userinfo', $order['uid'], 'store_id', 'id') != 0) {
					$param = [
						'id' => $order['id'],
					];
					Hook::listen('distribution', $param);
				}
				/**********分销钩子************/

				/*************贡献钩子*****************/
				$Outcome = ['oid' => $order['id']];
				Hook::listen('outcome', $Outcome);
				/*************贡献钩子*****************/
			} else {
				Log::info([
					'type' => 'error',
					'parameter' => $res,
					'msg' => '订单金额错误',
					'data' => $order,
				]);
			}

		}
		/************************新增代码*********************************/

		exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
	}

	/**
	 * 会员购买回调
	 * @Author   ksea
	 * @DateTime 2019-03-20T10:30:18+0800
	 * @return   [type]                   [description]
	 */

	public function vipnotify($res) {
		$model = model('WechatPay')->allowField(true);
		$id = (explode('_', $res['out_trade_no']))[2];
		$myvip = model('admin/shop/viprecord')->get($id);
		$ary = [
			'oid' => isset($id) ? $id : '',
			'openid' => $res['openid'],
			'mch_id' => $res['mch_id'],
			'trade_no' => $res['out_trade_no'],
			'transaction_id' => $res['transaction_id'],
			'attach' => $res['attach'],
			'total_fee' => $res['total_fee'],
			'result_code' => $res['result_code'],
			'is_subscribe' => $res['is_subscribe'],
			'pay_time' => $res['time_end'],
			'status' => 1,
			'create_time' => time(),
		];
		$model->save($ary);
		if (true) {
			$myvip_actualpay = $myvip['price'] * 100;
			if (!empty($myvip) && $myvip_actualpay == $res['total_fee']) {
				try {
					$data = [
						'id' => $myvip['id'],
						'is_pay' => '1',
						'pay_id' => $model->id,
					];
					$res = model('admin/shop/viprecord')->update($data);
					/***********更新会员到期时间**************/
					$param['uid'] = $myvip['uid'];
					$param['num'] = $myvip['num'];
					$param['interval'] = $myvip['interval'];
					Hook::exec('app\\mobile\\behavior\\Vippay', 'run', $param);
					/***********更新会员到期时间**************/

        		    /*******************公共会员支付锚点***********************/
         		    Hook::listen('vippay',$data);
        		    /*******************公共会员支付锚点***********************/

				} catch (\Exception $e) {
					\wechat\Log::warn($e->getMessage());
					\wechat\Log::warn($e->getTraceAsString());
				}

			}

		}
		exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
	}
	/**
	 * 发起支付中间页/实体位置
	 * @Author   ksea
	 * @DateTime 2019-01-22T12:39:15+0800
	 * @return   [type]                   [description]
	 */
	public function pay() {
		$type = session('wechat_pay.type') ? session('wechat_pay.type') : 1;
		switch ($type) {
		case '1': //默认订单支付
			$id = input('id', '');
			if (strlen($id) <= 0) {
				$this->error('没有指定订单');
			}
			$order = model('admin/shop/order')->get($id);
			if (empty($order)) {
				$this->error('订单错误');
			}
			$myOrder = $order->toarray();
			$name = $myOrder['goods_name'];
			$snnote = $myOrder['snorder'];
			$money = $myOrder['actualpay'];
			if ($money == 0) {

			}
			break;
		case '2': //自定义支付
			$name = '自定义支付';
			$snnote = 'selfpay' . '_' . time();
			$money = session('wechat_pay.money');
			$type = $type . '_' . session('wechat_pay.note');
			break;
		case '3': //订单追加
			$id = session('wechat_pay.append_order');
			if (strlen($id) <= 0) {
				$this->error('没有指定订单');
			}
			$order = model('admin/shop/order')->get($id);
			if (empty($order)) {
				$this->error('订单错误');
			}
			$myOrder = $order->toarray();
			$name = $myOrder['goods_name'] . '_订单追加';
			$snnote = $myOrder['snorder'] . '_' . time();
			$money = session('wechat_pay.money');
			break;
		case '4': //拼团订单
			$id = session('wechat_pay.assemble_order');
			if (strlen($id) <= 0) {
				$this->error('没有指定订单');
			}
			$order = model('admin/shop/order')->get($id);
			if (empty($order)) {
				$this->error('订单错误');
			}
			$myOrder = $order->toarray();
			$name = $myOrder['goods_name'] . '_拼团订单';
			$snnote = $myOrder['snorder'] . '_' . time();
			$money = $myOrder['actualpay'];
			break;
		case '5': //会员购买
			$id = input('id', '');
			if (strlen($id) <= 0) {
				$this->error('没有指定会员类型');
			}
			$vip = model('admin/shop/Viprecord')->get($id);
			if (empty($vip)) {
				$this->error('会员订单订单错误');
			}
			$myVip = $vip->toarray();
			$name = $myVip['vipname'];
			$snnote = 'vippay_' . time() . '_' . $id;
			$money = $myVip['price'];
			break;
		default: //默认订单支付
			$id = input('id', '');
			if (strlen($id) <= 0) {
				$this->error('没有指定订单');
			}
			$order = model('admin/shop/order')->get($id);
			if (empty($order)) {
				$this->error('订单错误');
			}
			$myOrder = $order->toarray();
			$name = $myOrder['goods_name'];
			$snnote = $myOrder['snorder'];
			$money = $myOrder['actualpay'];
			break;
		}
		$this->assign('actualpay', $money);
		if ($money != 0) {
			$openid = session('user.openid');
			$api = Wepay::getInstance();
			$api->setId($openid);
			$time = $this->request->server('REQUEST_TIME');
			$ary = $api->createPay($name, $snnote, $money, $time, $type);
			$this->assign('jscode', json_encode($ary));
		}
		return $this->fetch();
	}
	/**
	 * 发起支付位置
	 * @Author   ksea
	 * @DateTime 2019-01-22T12:38:53+0800
	 * @param    string                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function auth($id = '') {
		$type = input('type', '1'); //类型
		$id = input('id', ''); //订单id
		$money = input('money', ''); //钱
		$wantpay_id = input('wantpay_id', ''); //预支付生成id(针对订单追加处理金额操作)
		$append_order = input('append_order', ''); //追加订单的id
		$assemble_order = input('assemble_order', ''); //拼团订单的id
		$join_assemble = input('join_assemble', ''); //拼团订单的id
		$note = input('note', '');
		switch ($type) {
		case '1':

			break;
		case '2':
			session('wechat_pay.money', $money);
			session('wechat_pay.note', $note);
			break;
		case '3':
			$wantPayData=model('admin/shop/UserWantPay')->where(['id'=>$wantpay_id])->find();
			if(empty($wantPayData)){
				$this->error('预支付信息错误');
			}
			else{
				if($wantPayData['discount_id']<=0){
					$money=$wantPayData['money'];
				}
				else{
					$money=$wantPayData['discount'];
				}
			}
			session('wechat_pay.money', $money);
			session('wechat_pay.append_order', $append_order);
			break;
		case '4':
			session('wechat_pay.money', $money);
			session('wechat_pay.assemble_order', $assemble_order);
			break;
		case '5':
			session('wechat_pay.money', $money);
			session('wechat_pay.join_assemble', $join_assemble);
			break;
		case '6':

			break;
		default:

			break;
		}
		session('wechat_pay.type', $type);
		$url = config('pay_url') . "/id/{$id}";
		header('Location:' . $url);
	}

	public function callback() {
		$api = Wepay::getInstance();
		$res = $api->notify();
		\think\App::$debug && \wechat\Log::debug($res);
		if ($res && isset($res['return_code']) && 'SUCCESS' == $res['return_code']) {
			if (isset($res['result_code']) && 'SUCCESS' == $res['result_code']) {

				$row = model('WechatPay')->get(['transaction_id' => $res['transaction_id']]);
				if ($row) {
					exit(Helper::array2xml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']));
				}

				switch ($res['attach']) {
				case '2':
					$self = model('admin/shop/ShopPaySelf')::get(['no' => $res['out_trade_no']]);
					$res['note'] = $self->note;
					$self->save(['is_pay' => 1]);
					$this->selfnotify($res); //自定义支付回调
					break;
				case '3':
					$this->appendnotify($res); //追加支付回调
					break;
				case '4':
					$this->assemblenotify($res); //拼团支付回调
					break;
				case '5':
					$this->vipnotify($res); //会员卡购买回调
					break;
				default:
					$this->ordernotify($res); //订单支付回调
				}
			} else {
				Log::error('Pay Error: ' . $res['err_code']);
			}
		} else {
			Log::error('PAY FAIL: ');
		}
		\think\App::$debug && \wechat\Log::debug('Notify Complete!');
		exit(Helper::array2xml(['return_code' => 'FAIL', 'return_msg' => 'unknown']));
	}

}
