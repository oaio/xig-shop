<?php

namespace app\mobile\controller;

use think\Db;
use app\mobile\common\Controllercom;

class Active extends Controllercom
{
	
    public function _initialize()
    {
        parent::_initialize();
    }

    public function index(){
    	return $this->fetch();
    }

    public function notice()
    {
    	\think\Hook::exec('app\\admin\\behavior\\Cash','notice');
    }
}