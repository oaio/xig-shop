<?php
namespace app\mobile\controller;

use app\mobile\common\Controllercom;
use think\Cache;
use think\Db;
use think\Hook;
use think\Log;
use wechat\Api;
use wechat\Grab;
use wechat\Jssdk;
use wechat\WechatAuth;
use wechat\WxpayService;

class Index extends Controllercom {
	public function _initialize() {
		parent::_initialize();
	}
	public function indexx(){

		/**********************返利回调20190723******************************/

		$BackData=model('admin/shop/CashBack')->GetBack($order['id'],$order['uid']);
		if($BackData['code']==1){
			$total=$BackData['data'];
			//返利
			model('mapiv2/RedEnvelopes')->saveredenvelopespond($total, $order['uid']);
			//消费抵扣
			model('admin/shop/RedEnvelopesRecord')::add(4, $total, $order['id'], '消费返现');
		}
		else{
			//记录错误日志
			myErrorLog('getback.txt',json_encode(['返利错误数据',$BackData,$order]),'pay');
		}

		/**********************返利回调******************************/


	}
	public function index1x(){

		myErrorLog('analysistj.txt',666,'analysis');
	}
	/**
	 * 添加某个准备评价数据进如redis集合
	 * @Author   ksea
	 * @DateTime 2019-08-08T13:19:10+0800
	 * @return   [type]                   [description]
	 */
	public function crontab1(){

	  $server_id  = 3593;
      // $redis_data = 'Y-m-d';
      // $redis      = new \Redis();
      // $redis->connect('127.0.0.1', 6379);
      // $temp_name = 'server_assess_'.date($redis_data);

      // if($redis->scard($temp_name)>0){
      //    if($redis->sismember($temp_name,$server_id)==0){
      //       $redis->sadd($temp_name,$server_id);
      //    }
      // }else{
      //   $redis->sadd($temp_name,$server_id);
      // }
	  model('admin/shop/MyRedis')->assessAdd($server_id);
	}

	/**
	 * 去除某个服务评价在redis里面
	 * @Author   ksea
	 * @DateTime 2019-08-08T13:19:53+0800
	 * @return   [type]                   [description]
	 */
	public function crontab2(){

	  $server_id  = 3593;
	  $end_time   ='2019-08-08';//1565163068

   //    $redis_data = 'Y-m-d';//处理层级
	  // $data_name  =date($redis_data,strtotime(date($redis_data,strtotime($end_time))));
   //    $redis      = new \Redis();
   //    $redis->connect('127.0.0.1', 6379);
   //    $temp_name = 'server_assess_'.$data_name;
   //    if($redis->scard($temp_name)>0){
   //       if($redis->sismember($temp_name,$server_id)==1){
   //       	//去除
   //          $redis->srem($temp_name,$server_id);
   //       }
   //       else{
   //       	myErrorLog('redisError.txt',json_encode([$temp_name,$server_id]),'redis');
   //       }
   //    }else{
   //     		myErrorLog('redisError.txt',json_encode([$temp_name,$server_id]),'redis');
   //    }

      model('admin/shop/MyRedis')->assessRem($server_id,$end_time);

	}


	public function crontab3(){
		model('admin/shop/Crontab')->assess();
   //    $redis_data = 'Y-m-d';//处理层级
	  // $data_name  =date($redis_data,strtotime(date($redis_data)/*.' -15 days'*/));
   //    $redis      = new \Redis();
   //    $redis->connect('127.0.0.1', 6379);
   //    $temp_name = 'server_assess_'.$data_name;
   //    $ShopServerAssess=model('admin/shop/ShopServerAssess');
   //    $serverData=model('admin/shop/Serverorder');
   //    if($redis->scard($temp_name)>0){
	  //    	foreach ($redis->smembers($temp_name) as $key => $value) {
	  //    		$server=$serverData->where('id',$value)->find();
	  //    		$data['serverid']     = $server->id;
	  //    		$data['uid']          = $server->uid;
	  //    		$data['meter_score']  = '5';
	  //    		$data['server_score'] = '5';
	  //    		$data['manner_score'] = '5';
	  //    		$red=$ShopServerAssess->webAdd($data);
	  //       	$redis->srem($temp_name,$server->id);
	  //    	}

   //    }
	}
	/**
	 * 商城首页面
	 * @Author   ksea
	 * @DateTime 2018-12-12T18:22:44+0800
	 * @return   [type]                   [description]
	 */
	public function index() {
		return $this->fetch();
	}
	public function adx(){
		$ary=[
			'oid' =>'5099',
			'create_time'=>time(),
			'total_fee' =>10001,
		];
		Hook::listen('addpay',$ary);

	}
	public function mix(){
		$user=Db::name('shop_userinfo')->where('id',5239)->find();
		model('admin/shop/AnalysisMix')->loginlog($user,1);
		model('admin/shop/AnalysisMix')->bindlog($user,1);
	}
	//测试集合数据
	public function setredis() {
		$uid = '999';
		//如果授权，就当这个人登陆

		$redis = new \Redis();

		$redis->connect('127.0.0.1', 6379);

		$temp_name = 'login_' . date('Y-m-d');

		if ($redis->scard($temp_name) > 0) {
			if ($redis->sismember($temp_name, $uid) == 0) {
				$redis->sadd($temp_name, $uid);
			}
		} else {
			$redis->sadd($temp_name, $uid);
		}
	}
	/**
	 *
	 * @Author   ksea
	 * @DateTime 2019-08-01T17:21:36+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function getlogin($data) {

	}
	public function analysis1() {

		$order = model('admin/shop/order')->get('5005');

		/***********************店铺支付详情**************************/
		$whereStore = [
			'city_code' => $order['city_code'],
			'store_id' => $order['store_id'],
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisStore = model('admin/shop/AnalysisStore');

		$analysis_temp = $modelAnalysisStore->where($whereStore)->find();
		if (!$analysis_temp) {

			$AnalysisStoreData = [
				'city_code' => $order['city_code'],
				'store_id' => $order['store_id'],
				'userpaynum' => 1,
				'suserpaysum' => $order['shouldpay'],
				'auserpaysum' => $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] : 0,
				'paynum' => 1,
				'create_time' => strtotime(date('Y-m-d')),
			];

			$modelAnalysisStore->save($AnalysisStoreData);

		} else {

			$jaindan = model('admin/shop/order')->where([
				'is_pay' => 1,
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
				'uid' => $order['uid'],
			])->select();

			$paynum = $analysis_temp['paynum'];
			if (count($jaindan) == 1) {
				$paynum += $paynum;
			}

			$AnalysisStoreData = [
				'userpaynum' => $analysis_temp['userpaynum'] + 1,
				'suserpaysum' => $analysis_temp['suserpaysum'] + $order['shouldpay'],
				'auserpaysum' => $analysis_temp['auserpaysum'] + $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] + $analysis_temp['discountsum'] : $analysis_temp['discountsum'],
				'paynum' => $paynum,
			];
			$analysis_temp->save($AnalysisStoreData);
		}
		/***********************店铺支付详情**************************/

	}

	public function analysis2() {

		$order = model('admin/shop/order')->get('5005');

		/***********************用户购买详情**************************/
		$whereBuy = [
			'uid' => $order['uid'],
			'orgin' => $order['orgin'],
			'city_code' => $order['city_code'],
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisBuy = model('admin/shop/AnalysisBuy');

		$analysis_temp = $modelAnalysisBuy->where($whereBuy)->find();
		if (!$analysis_temp) {

			$AnalysisBuyData = [
				'uid' => $order['uid'],
				'userpaynum' => 1,
				'suserpaysum' => $order['shouldpay'],
				'auserpaysum' => $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] : 0,
				'city_code' => $order['city_code'],
				'orgin' => $order['orgin'],
				'create_time' => strtotime(date('Y-m-d')),
			];

			$modelAnalysisBuy->save($AnalysisBuyData);

		} else {

			$jaindan = model('admin/shop/order')->where([
				'is_pay' => 1,
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
				'uid' => $order['uid'],
			])->select();

			$paynum = $analysis_temp['paynum'];
			if (count($jaindan) == 1) {
				$paynum += $paynum;
			}

			$AnalysisBuyData = [

				'userpaynum' => $analysis_temp['userpaynum'] + 1,
				'suserpaysum' => $analysis_temp['suserpaysum'] + $order['shouldpay'],
				'auserpaysum' => $analysis_temp['auserpaysum'] + $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] + $analysis_temp['paynum'] : $analysis_temp['paynum'],

			];

			$analysis_temp->save($AnalysisBuyData);

		}
		/***********************用户购买详情**************************/
	}

	public function analysis3() {
		$order = model('admin/shop/order')->get('5059');
	    // model('admin/shop/AnalysisMix')->analysis($order);
	    // model('admin/shop/AnalysisBuy')->analysis($order);
	    model('admin/shop/AnalysisTj')->analysis($order);
	    //model('admin/shop/AnalysisStore')->analysis($order);
	}

  public function analysis5() {

    $order = model('admin/shop/order')->get('5017');
    model('admin/shop/AnalysisBuy')->analysis($order);
  }

	public function analysis4() {

		$order = model('admin/shop/order')->get('5005');

		/***********************推荐店铺数**************************/
		$whereTj = [
			'tj' => isset($order['tj']['tj']) ? $order['tj']['tj'] : '',
			'tjname' => isset($order['tj']['tjname']) ? $order['tj']['tjname'] : '',
			'create_time' => strtotime(date('Y-m-d')),
		];

		$modelAnalysisTj = model('admin/shop/AnalysisTj');

		$analysis_temp = $modelAnalysisTj->where($whereTj)->find();

		if (!$analysis_temp) {

			$AnalysisTjData = [
				'tjstorecount' => 1,
				'payusercount' => 1,
				'suserpaysum' => $order['shouldpay'],
				'auserpaysum' => $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] : 0,
				'tj' => isset($order['tj']['tj']) ? $order['tj']['tj'] : '',
				'tjname' => isset($order['tj']['tjname']) ? $order['tj']['tjname'] : '',
				'create_time' => strtotime(date('Y-m-d')),
				'tjstore_link' => $order['store_id'],
			];

			$modelAnalysisTj->save($AnalysisTjData);

		} else {

			$AnalysisTjData = [
				'suserpaysum' => $analysis_temp['suserpaysum'] + $order['shouldpay'],
				'auserpaysum' => $analysis_temp['auserpaysum'] + $order['actualpay'],
				'discountsum' => isset($order['virdiscount']['dismoney']) ? $order['virdiscount']['dismoney'] + $analysis_temp['discountsum'] : $analysis_temp['discountsum'],
			];

			$ckstore = $modelAnalysisTj->where("FIND_IN_SET(" . $order['store_id'] . ",tjstore_link)")->select();
			if (count($ckstore) == 0) {
				$AnalysisTjData['tjstorecount'] = $analysis_temp['tjstorecount'] + 1;
				$AnalysisTjData['tjstore_link'] = $AnalysisTjData['tjstore_link'] . '-' . $order['store_id'];
			}

			$ckorder = model('admin/shop/order')->where([
				'uid' => $order['uid'],
				'store_id' => $order['store_id'],
				'is_pay' => 1,
				'create_time' => [
					'between',
					strtotime(date('Y-m-d')) . ',' . strtotime(date('Y-m-d') . '+1 day'),
				],
			])->select();
			if (count($ckorder) == 1) {
				$AnalysisTjData['payusercount'] = $analysis_temp['payusercount'] + 1;
			}
			$analysis_temp->save($AnalysisTjData);
		}
		/***********************推荐店铺数**************************/
	}
	public function sendtemp1() {
		$params = [
			'oid' => 4592,
		];
		$res = model('admin/shop/Order')->sendtemp($params);
		dump($res);
	}
	public function adc() {

	}
	public function redis() {
		$id = '4896';
		$user = Db::name('shop_userinfo')->where('id', $id)->find();
		//切分钩子
		if ($user['pid'] > 0) {

			$config = [
				'total' => 100, //一共多少张/多少张
				'usercount' => 3, //邀请多少人获取
				'eachget' => 20, //每人最多获取
				'onydayget' => 10, //一天最多获取
				'validtime' => 30, //有效时间
			];

			$fuser = model('admin/shop/user')->with(['freeroll'])->where('id', $user['pid'])->find()->toArray();

			$openid_arr = [
				$fuser['openid'],
			];

			$InviteModel = model('admin/shop/Invite');
			//确定关系
			$insertData = [
				'uid' => $user['id'],
				'fuid' => $user['pid'],
				'invite_city' => $user['use_city_code'],
				'origin' => '二维码',
				'status' => 0,
				'invitee' => $user['nickname'],
				'avatar' => $user['pic'],
				'tel' => $user['phone'],
			];
			$InviteModel->save($insertData);

			//统计
			$InviteUse = $InviteModel->where(['status' => 0, 'fuid' => $user['pid']])->select();

			$count = count($InviteUse);

			//有效人数达到
			if ($count >= $config['usercount']) {

				$countFreeroll = $fuser['freeroll'] ? count($fuser['freeroll']) : 0;

				//个人达到数目不发放
				if ($countFreeroll >= $config['eachget']) {
					echo 1;
				}

				//库存用完不在放发放
				if ($config['total'] <= 0) {
					echo 1;
				}

				//个人单天获取数据达到不发放

				$redis = new \Redis();

				$redis->connect('127.0.0.1', 6379);

				$temp_name = date('Y-m-d') . '_' . $fuser['id'];

				if ($redis->get($temp_name) >= $config['onydayget']) {
					echo 1;
				}

				//发放操作
				$FreeData = [
					'uid' => $fuser['id'],
					'validtime' => time() + ($config['validtime'] * 60 * 60 * 24),
				];
				model('admin/shop/FreeRoll')->save($FreeData);

				//更新获取redis
				$redis->INCR($temp_name);

				//通知操作
				$InviteModel = model('admin/shop/Invite');

				$data = [
					'first' => '小主，恭喜您成功邀请' . $config['usercount'] . '名好友，赠送您一张免单券！',
					'keyword1' => $fuser['nickname'],
					'keyword2' => $fuser['phone'],
					'keyword3' => date('Y-m-d H:i:s', $fuser['create_time']),
					'remark' => '点击立即领取哦！',
				];

				$link = config('wechattemplate.bargain')['link'];
				$template_id = config('wechattemplate.bargain')['tempid'];
				$myGrab = new \wechat\DiytTemplate($template_id);
				$reds = $myGrab::SendGroup($openid_arr, $data, $link);
				//更新抵扣数据
				$uidArr = [];

				foreach ($InviteUse as $key => $value) {
					array_push($uidArr, $value->id);
				}

				$uidArr = implode($uidArr, ',');
				$wher['id'] = array('in', $uidArr);
				$InviteModel->where($wher)->update(['status' => 1]);

			}

		}

	}
	/**
	 * 清空虚假数目
	 * @Author   ksea
	 * @DateTime 2019-04-12T17:17:26+0800
	 * @return   [type]                   [description]
	 */
	public function clearcache() {
		Cache::clear();
	}
	public function checksn() {
		$param = [
			'mch_id' => config('pay.mch_id'),
			'appid' => config('pay.appid'),
			'apiKey' => config('pay.secret'),
			'outTradeNo' => '5cf4912e998b9',
		];
		$wepay = new WxpayService();
		$res = $wepay->checkSn($param);
		dump($res);
	}
	/**
	 * 店铺首页
	 * @Author   ksea
	 * @DateTime 2018-12-12T18:22:44+0800
	 * @return   [type]                   [description]
	 */
	public function store() {
		$img_where = [
			'status' => 1,
		];
		$img_index = Db::name('shop_index')->where($img_where)->order('pow desc id')->limit("0,3")->select();
		$this->assign('img_index', $img_index);
		$data = Db::name('shop_categrory')->where('pid=0')->select();
		$ua = $_SERVER['HTTP_USER_AGENT'];
		if (strpos($ua, 'MicroMessenger') == false && strpos($ua, 'Windows Phone') == false) {

			$jssdk_res = [
				'appId' => '',
				'timestamp' => '',
				'nonceStr' => '',
				'signature' => '',
			];
		} else {
			$Jssdk = new Jssdk(config('wechat.appid'), config('wechat.secret'));
			$jssdk_res = $Jssdk->getSignPackage();
		}
		$temp = session('user');
		unset($temp['json_wechat']);
		$json_user = json_encode($temp, true);
		echo "<script>window.localStorage.setItem('userContent','$json_user')</script>";
		$this->assign('jssdk', $jssdk_res);
		$this->assign('data', $data);
		return $this->fetch();
	}
	/**
	 * 微信jssdk测试
	 * @return [type] [description]
	 */
	public function wechat_text() {

		$Jssdk = new Jssdk(config('wechat.appid'), config('wechat.secret'));
		dump($Jssdk);
		$jssdk_res = $Jssdk->getSignPackage();
		dump($jssdk_res);
		$this->assign('jssdk', $jssdk_res);
		return $this->fetch('index/wechat_text');

	}
	public function UnLock() {
		Grab::UnLock(1);
	}
	public function ReadLock() {
		echo Grab::ReadLock(1);
	}
	/**
	 * [textdemo description]
	 * @Author   ksea
	 * @DateTime 2019-03-04T16:11:48+0800
	 * @return   [type]                   [description]
	 */
	public function textdemo() {
		$Jssdk = new Jssdk(config('wechat.appid'), config('wechat.secret'));
		$jssdk_res = $Jssdk->getSignPackage();
		$this->assign('jssdk', $jssdk_res);
		return $this->fetch();
	}
	/**
	 * [get_cache description]
	 * @Author   ksea
	 * @DateTime 2019-04-24T09:23:08+0800
	 * @param    [type]                   $cache [description]
	 * @return   [type]                          [description]
	 */
	public function get_cache($cache) {
		dump(Cache::get($cache));
	}
	/**
	 * [tadd description]
	 * @Author   ksea
	 * @DateTime 2019-04-24T09:23:04+0800
	 * @return   [type]                   [description]
	 */
	public function sadd() {
		$dat = [
			'serverid' => '9',
			'count' => '6',
			'ap_time' => '01:00~04:00',
			'ap_date' => '2019-1-13',
		];
		model('admin/shop/ServerCache')->add($dat);
	}
	/**
	 * textgongx
	 */
	public function setOutcome($oid) {
		$param['oid'] = $oid;
		$dd = model('admin/shop/ShopOutcome')->setOutcome($param);
		dump($dd);
	}
	public function http1() {
//      dump(Log::$config);
		dump(Log::getLog('info'));
	}
	/**
	 *
	 * @Author   ksea
	 * @DateTime 2019-05-05T14:30:06+0800
	 * @return   [type]                   [description]
	 */
	public function qrcode() {
		$api = new Api();
		$key = 'ids|996';
		$res = $api->textCreat($key, 0);
		$kx = $api->showqrcode($res['ticket']);
		dump($kx);
		dump($res);
	}
	public function qrcode1($str) {
		$api = new Api();
		$key = $str;
		$kx = $api->qrcode($key);
		dump($kx);
	}
	public function ees() {
		$res = model('admin/shop/Virdiscount')->payDis('14', '173');
		dump($res);
	}
	public function ass() {
		$uid = '4753';
		$order_actualpay = '2';
		if (round($order_actualpay, 2) >= config('Rebate.LowestPay')) {

			$total = 0;
			$vip_end_time = is_get_val('shop_userinfo', $uid, 'vip_end_time');
			if ($vip_end_time < time() || empty($vip_end_time)) {
				$total = $order_actualpay * config('Rebate.UserType')[0];
			} else {
				$total = $order_actualpay * config('Rebate.UserType')[1];
			}
			$total = round($total, 2);
			model('mapiv2/RedEnvelopes')->saveredenvelopespond($total, $uid);
		}

	}
	/**
	 * 生成二维码
	 * @Author   ksea
	 * @DateTime 2019-04-16T09:47:58+0800
	 * @param    string                   $res   [description]
	 * @param    string                   $key   [description]
	 * @param    string                   $count [description]
	 * @return   [type]                          [description]
	 */
	public function text($res = '', $key = '', $count = '') {
		if (isset($res) && !empty($res)) {
			$nowC = Cache::get('keyset');
			if ($nowC == $count) {
				return 'ok';
			}
			if ($key + 1 <= $count) {
				/*********生成基础二维码列*********/
				if (true) {
					Hook::listen('base_code', $res[$key]);
				}
				/*********生成基础二维码列*********/

				/*********生成各人二维码*********/
				if (true) {
					$param_mycode['func'] = 'set_code';
					$param_mycode['request']['id'] = $res[$key]['id'];
					Hook::listen('set_code', $param_mycode);
				}
				/*********生成各人二维码*********/

				/*********粉丝二维码*********/
				if (true) {
					$fans_code['request']['id'] = $res[$key]['id'];
					Hook::listen('fans_code', $fans_code);
				}
				/*********粉丝二维码*********/
				$key++;
				Cache::set('keyset', $key);
				return $this->text($res, $key, $count);
			} else {
				echo time();
			}
		} else {
			$res = Db::name('shop_userinfo')->select();
			$count = count($res);
			if (Cache::get('keyset')) {
				$key = Cache::get('keyset');
			} else {
				Cache::set('keyset', 0);
				$key = 0;
			}
			echo time();
			echo "<br>";
			return $this->text($res, $key, $count);
		}
	}
	/**
	 * shabi1
	 * @Author   ksea
	 * @DateTime 2019-06-04T20:51:30+0800
	 * @return   [type]                   [description]
	 */
	public function st() {

		$phone = new \wechat\Fictitious();
		$mobile = '15818532853';
		$userphone = '13066903478';
		$Binding_time = strtotime('- 3 day');
		$rivacy_phone = Db::name('rivacy_phone')->where('Binding_time', '<', $Binding_time)->find();
		if (!is_null($mobile) && !is_null($rivacy_phone)) {
			$phone_resul = $phone->createphone($rivacy_phone['phone'], $mobile, $userphone);
			if ($phone_resul['resultdesc'] == 'Success') {
				Db::name('rivacy_phone')->where('id', $rivacy_phone['id'])->update(['Binding_time' => time()]);
				Db::name('rivacy_phone_record')->insert(['callerNum' => $mobile, 'calleeNum' => $userphone, 'relationNum' => $rivacy_phone['phone'], 'Binding_time' => time(), 'subscriptionId' => $phone_resul['subscriptionId']]);
			}
		}

	}
	public function smstemp() {
		$pht = new \aliyun\SendTemp();
		$r1 = $pht::assess('13066903478', '中');
		$r2 = $pht::SendCode('13066903478', mt_rand());
		$r3 = $pht::order('13066903478', 'OR40011122231');
		$r4 = $pht::ServerlinkErr('13066903478', 'OR400111222342', '2019-06-20 21:30:00');
		$r5 = $pht::UserlinkErr('13066903478', 'OR400111222343', '小明', date('Y-m-d h:i:s'), '2019-07-20 21:30:00');
		$r6 = $pht::ServerChange('13066903478', 'OR41111122234', date('Y-m-d h:i:s'));
		$r7 = $pht::UserChange('13066903478', 'OR41111122235', '小华', date('Y-m-d h:i:s'), '2019-07-21 21:30:00');
		$r8 = $pht::ServerGrab('13066903478', 'OR41111122236');
		$r9 = $pht::UserGrab('13066903478', 'OR41111122237');
		$r10 = $pht::ServerSureGo('13066903478', 'OR41111122238', date('Y-m-d h:i:s'));
		$r11 = $pht::UserSureGo('13066903478', 'OR41111122239', '小风', date('Y-m-d h:i:s'), '2019-07-25 21:30:00');
		$r12 = $pht::AuditResultsSuccess('13066903478');
		$r13 = $pht::AuditResultsError('13066903478', '审核失败了aaa');
	}
	public function jb($po) {
		$phone = new \wechat\Fictitious();
		$ret = $phone->Untying($po);
		dump($ret);die;
	}

	/**
	 * 清空某个数据
	 * @Author   ksea
	 * @DateTime 2019-05-30T20:52:45+0800
	 * @param    [type]                   $name [description]
	 * @return   [type]                         [description]
	 */
	public function clearcachen($name) {
		Cache::pull($name);
	}
	/**
	 * 刷用户数据
	 *
	 */

	public function text1($obj = '', $res = '', $key = '', $count = '') {

		if (isset($res) && !empty($res)) {
			$nowC = Cache::get('keyset1');
			if ($nowC == $count) {
				return 'ok';
			}
			if ($key + 1 <= $count) {

				$myt = $obj->getUserInfo($res[$key]['openid'], 'zh_CN');
				if (isset($myt['unionid'])) {
					model('admin/shopuser/User')->where('openid', $myt['openid'])->update(['unionid' => $myt['unionid'], 'json_wechat' => json_encode($myt)]);
				}
				$key++;
				Cache::set('keyset1', $key);
				return $this->text1($obj, $res, $key, $count);
			} else {
				echo time();
			}
		} else {
			$query = '(LENGTH(unionid)<=0 OR ISNULL(unionid))';
			$query .= ' AND JSON_EXTRACT(json_wechat, "$.subscribe") = 1';
			$appid = config('wechat.appid');
			$appsecret = config('wechat.secret');
			$api = new WechatAuth($appid, $appsecret);
			$res = Db::name('shop_userinfo')->where($query)->select();
			$count = count($res);
			if (Cache::get('keyset1')) {
				$key = Cache::get('keyset1');
			} else {
				Cache::set('keyset1', 0);
				$key = 0;
			}
			echo time();
			echo "<br>";
			if ($count > 0) {
				return $this->text1($api, $res, $key, $count);
			} else {
				echo '结束';
			}
		}
	}
	/**
	 * 自定义公共函数
	 * @Author   ksea
	 * @DateTime 2019-08-12T11:42:00+0800
	 * @param    [type]                   $fun    [description]
	 * @param    array                    $params [description]
	 * @return   [type]                           [description]
	 */
	public function diyfun($fun,$params=[]){
		if(empty($params)){
			$res=$fun();
		}
		else{
			//$strParam=implode(',', $params);
			$strParam='';
			eval($strParam);
			dump($strParam);die;
			$res=$fun();
		}
		dump($res);
	}
	public function setFictitious(){
		setFictitious('17150379771','669');
	}
}
