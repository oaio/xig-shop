<?php

namespace app\mobile\controller;

use app\mobile\common\Controllercom;
use think\Db;
use wechat\Jssdk;
use think\Hook;
use think\Session;

class Goods extends Controllercom{

    /**
     * 商品列表页面
     * @author baohb
     * @datetime 2018-12-29 14:25
     */

    public function goods_list(){

        return $this->fetch();
    }

	/**
	 * 商品的详情
	 * @Author   ksea
	 * @DateTime 2018-12-06T17:30:00+0800
	 * @return   [type]                   [description]
	 */
	public function goods_detail(){

        /*******************登陆检测**************************/
        if(!Session::get('user')){
           $local_res='https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
           session('loc_url',$local_res);
           $this->redirect('User/login');
        }
        /*******************登陆检测**************************/
		return $this->fetch();
	}

}

