<?php
namespace app\mobile\controller;

use wechat\Jssdk;
use wechat\Wechat;
use wechat\WechatAuth;
use wechat\WechatCrypt;
use think\Controller;
use think\Hook;
use think\Db;
use think\Validate;
use think\Log;
use wechat\applets\WXBizDataCrypt;
use think\Session;

class Controllercom extends Controller
{
  
    protected $id,$secret,$token,$web_url,$mini_id,$mini_secret;
    
    public function _initialize(){
      header("Content-Type: text/html; charset=utf-8");
      $wechat= array('token'=>'wechat','web_url'=>'http://'.$_SERVER['HTTP_HOST'].'/mobile/Controllercom/');
      $this->token = config('wechat.token');
      $this->secret=config('wechat.secret');
      $this->id=config('wechat.appid');
      $this->mini_id=config('wechat.mini_appid');
      $this->mini_secret=config('wechat.mini_secret');
      $this->web_url=$wechat['web_url'];
    }


  /*
   * 微信获取code换取当前用户信息
   * Enter description here ...
   */
    public  function code(){
        if(session('loc_url')){
          $local_res=session('loc_url');
        }
        else{
          $local_res='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        }
        $state='wechat';
        $APPID=$this->id;
        $type=input('param.type','0');
        $worker_type=input('param.worker_type','');
        $mstore_id=input('param.mstore_id','');
        $pid=input('param.pid','');
        $REDIRECT_URI=$this->web_url.'getcode?appid='.$APPID."&&appsecret=".$this->secret.'&&type='.$type.'&&worker_type='.$worker_type.'&&pid='.$pid.'&&mstore_id='.$mstore_id.'&&local_url='.$local_res;
        //$REDIRECT_URI='2508b1b1.nat123.cc/wechat/ncp/ncp.php/Home/Wchat/getcode';
        //$scope='snsapi_base';//不需要授权
        $scope='snsapi_userinfo';//需要授权
        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$APPID.'&redirect_uri='.urlencode($REDIRECT_URI).'&response_type=code&scope='.$scope.'&state='.$state.'#wechat_redirect';
        header("Location:".$url);
    }

    public  function codeExt(){
      
//        if(session('loc_url')){
//            $local_res=session('loc_url');
//        }
//        else{
            $local_res='https://store-web.xiaogebang.com/';
//        }

        $state='wechat';
        $APPID=$this->id;
        $type=input('param.type','0');
        $worker_type=input('param.worker_type','');
        $mstore_id=input('param.mstore_id','');
        $pid=input('param.pid','');
        $REDIRECT_URI=$this->web_url.'getcodeExt?appid='.$APPID."&&appsecret=".$this->secret.'&&type='.$type.'&&worker_type='.$worker_type.'&&pid='.$pid.'&&mstore_id='.$mstore_id.'&&local_url='.$local_res;
        //$REDIRECT_URI='2508b1b1.nat123.cc/wechat/ncp/ncp.php/Home/Wchat/getcode';
        //$scope='snsapi_base';//不需要授权
        $scope='snsapi_userinfo';//需要授权
        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$APPID.'&redirect_uri='.urlencode($REDIRECT_URI).'&response_type=code&scope='.$scope.'&state='.$state.'#wechat_redirect';
        header("Location:".$url);
    }
   /*
    * 利用code获取access_token等个人用户信权
    *
    */
    public function  getcode(){
          $type=input('type');
          $worker_type=input('worker_type');
          $appid=input('appid');
          $appsecret=input('appsecret');
          $code = input('code'); 
          $pid = input('pid','0');
          $mstore_id = input('mstore_id',config('sys.def_store'));
          $astore_id = input('mstore_id','');
          $set_localhost=input('local_url','mobile/index/index');
          $get_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$appsecret."&code=".$code."&grant_type=authorization_code ";
          $ch = curl_init($get_token_url);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_HEADER, true); 
          $res=curl_exec($ch);
          curl_close($ch);
          $res=strstr($res, "{");
          session('wechat_a.0',json_decode($res,true));
          session('wechat_a.1',input('get.'));
          /**
          * 获取微信唯一id
          */
          $api=new WechatAuth($appid,$appsecret);
          $session_token=session('wechat_a.0');
          $openid=$session_token['openid'];
          $unionid=$session_token['unionid'];
          $access_token = $session_token['access_token'];
          session('wechat_a.2',$api->getUserInfo2($openid,'zh_CN', $access_token));
          $wechat_userdata=session('wechat_a.2');
          $param['request']['openid']=$openid;
          $param['request']['unionid']=$unionid;


          /**
           * 用户更新unionid
           */

          $tmepD=is_get_val('shop_userinfo',$openid,'openid,create_time,unionid','openid');

          $tmepB=is_get_val('shop_userinfo',$unionid,'openid,create_time,unionid','unionid');

          $userModel=new \app\admin\model\shopuser\User;

          if($tmepD&&$tmepD['create_time'] < '1558966076'){
              if(isset($tmepD['openid']))
              {
                /**********************自动登陆操作*****************************/
                $session_user=model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['openid'=>$openid])->find();
                session('user',$session_user);
                model('admin/shop/AnalysisMix')->loginlog($session_user->toArray(),1);
                /**********************自动登陆操作*****************************/
                /****绑定店铺切换*****/
                
                if($astore_id){
                  $res=$userModel->where('openid',session('wechat_a.2')['openid'])->update(['store_id'=>$astore_id]);
                  /***************店铺切换记录钩子***************/
                  if(strlen($astore_id)>0){
                    $param['store_id']=$astore_id;
                    $param['uid']     =session('user.id');
                    Hook::listen('switch_store',$param);
                  }
                  /***************店铺切换记录钩子***************/
                }
                /****绑定店铺切换*****/
                  /***********旧用户更新unionid**************/
                  $userModel->where('openid',session('wechat_a.2')['openid'])->update(['unionid'=>$unionid]);
                  /***********旧用户更新unionid**************/
                $this->redirect($set_localhost);

              }
          }
          else{

              if(isset($tmepB['unionid']))
              {

                /***********同步小程序先创建用户**************/
                $userModel->where('unionid',$unionid)->update(['openid'=>$openid]);
                /***********同步小程序先创建用户**************/

                /**********************自动登陆操作*****************************/
                $session_user=model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['unionid'=>$unionid])->find();
                session('user',$session_user);
                model('admin/shop/AnalysisMix')->loginlog($session_user->toArray(),1);
                /**********************自动登陆操作*****************************/
                /****绑定店铺切换*****/
                if($astore_id){
                  $res=$userModel->where('unionid',$unionid)->update(['store_id'=>$astore_id]);
                  /***************店铺切换记录钩子***************/
                  if(strlen($astore_id)>0){
                    $param['store_id']=$astore_id;
                    $param['uid']     =session('user.id');
                    Hook::listen('switch_store',$param);
                  }
                  /***************店铺切换记录钩子***************/
                }
                /****绑定店铺切换*****/

                $this->redirect($set_localhost);

              }
          }


          $param['request']['json_wechat']=json_encode($wechat_userdata);

          $param['request']['pic']=isset($wechat_userdata['headimgurl'])?$wechat_userdata['headimgurl']:'https://pub-static.haozhaopian.net/static/web/site/features/cn/crop/images/crop_20a7dc7fbd29d679b456fa0f77bd9525d.jpg';

          $param['request']['nickname']=isset($wechat_userdata['nickname'])?$wechat_userdata['nickname']:'昵称未获取';
          $param['request']['pid']=$pid;
          $param['request']['mstore_id']=$mstore_id;
          Hook::listen('add_user',$param);

          //记录用户登陆
          model('admin/shop/AnalysisMix')->loginlog(session('user'),1);
          
          $this->redirect($set_localhost,['mstore_id'=>$mstore_id]);
    }

    public function  getcodeExt(){
        $type=input('type');
        $worker_type=input('worker_type');
        $appid=input('appid');
        $appsecret=input('appsecret');
        $code = input('code');
        $pid = input('pid','0');
        $mstore_id = input('mstore_id',config('sys.def_store'));
        $astore_id = input('mstore_id','');
        $set_localhost=input('local_url','https://store-web.xiaogebang.com/');
        $get_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$appsecret."&code=".$code."&grant_type=authorization_code ";
        $ch = curl_init($get_token_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $res=curl_exec($ch);
        curl_close($ch);
        $res=strstr($res, "{");
        session('wechat_a.0',json_decode($res,true));
        session('wechat_a.1',input('get.'));
        /**
         * 获取微信唯一id
         */
        $api=new WechatAuth($appid,$appsecret);
        $session_token=session('wechat_a.0');
        $openid=$session_token['openid'];
        $unionid=$session_token['unionid'];
        $access_token = $session_token['access_token'];
        session('wechat_a.2',$api->getUserInfo2($openid,'zh_CN', $access_token));
        $wechat_userdata=session('wechat_a.2');
        $param['request']['openid']=$openid;
        $param['request']['unionid']=$unionid;


        /**
         * 用户更新unionid
         */

        $tmepD=is_get_val('shop_userinfo',$openid,'openid,create_time,unionid','openid');

        $tmepB=is_get_val('shop_userinfo',$unionid,'openid,create_time,unionid','unionid');

        $userModel=new \app\admin\model\shopuser\User;

        if($tmepD&&$tmepD['create_time'] < '1558966076'){
            if(isset($tmepD['openid']))
            {
                /**********************自动登陆操作*****************************/
                $session_user=model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['openid'=>$openid])->find();
                session('user',$session_user);
                model('admin/shop/AnalysisMix')->loginlog($session_user->toArray(),1);
                /**********************自动登陆操作*****************************/
                /****绑定店铺切换*****/

                if($astore_id){
                    $res=$userModel->where('openid',session('wechat_a.2')['openid'])->update(['store_id'=>$astore_id]);
                    /***************店铺切换记录钩子***************/
                    if(strlen($astore_id)>0){
                        $param['store_id']=$astore_id;
                        $param['uid']     =session('user.id');
                        Hook::listen('switch_store',$param);
                    }
                    /***************店铺切换记录钩子***************/
                }
                /****绑定店铺切换*****/
                /***********旧用户更新unionid**************/
                $userModel->where('openid',session('wechat_a.2')['openid'])->update(['unionid'=>$unionid]);
                /***********旧用户更新unionid**************/
                $this->redirect($set_localhost.'?uid='.session('user.id'));

            }
        }
        else{

            if(isset($tmepB['unionid']))
            {

                /***********同步小程序先创建用户**************/
                $userModel->where('unionid',$unionid)->update(['openid'=>$openid]);
                /***********同步小程序先创建用户**************/

                /**********************自动登陆操作*****************************/
                $session_user=model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['unionid'=>$unionid])->find();
                session('user',$session_user);
                //model('admin/shop/AnalysisMix')->loginlog($session_user->toArray(),1);
                /**********************自动登陆操作*****************************/

                /****绑定店铺切换*****/
                if($astore_id){
                    $res=$userModel->where('unionid',$unionid)->update(['store_id'=>$astore_id]);
                    /***************店铺切换记录钩子***************/
                    if(strlen($astore_id)>0){
                        $param['store_id']=$astore_id;
                        $param['uid']     =session('user.id');
                        Hook::listen('switch_store',$param);
                    }
                    /***************店铺切换记录钩子***************/
                }
                /****绑定店铺切换*****/

                $this->redirect($set_localhost.'?uid='.session('user.id'));

            }
        }


        $param['request']['json_wechat']=json_encode($wechat_userdata);

        $param['request']['pic']=isset($wechat_userdata['headimgurl'])?$wechat_userdata['headimgurl']:'https://pub-static.haozhaopian.net/static/web/site/features/cn/crop/images/crop_20a7dc7fbd29d679b456fa0f77bd9525d.jpg';

        $param['request']['nickname']=isset($wechat_userdata['nickname'])?$wechat_userdata['nickname']:'昵称未获取';
        $param['request']['pid']=$pid;
        $param['request']['mstore_id']=$mstore_id;
        Hook::listen('add_user',$param);
        //model('admin/shop/AnalysisMix')->loginlog(session('user'),1);
        $this->redirect($set_localhost.'?uid='.$param['uid']);
    }

    /**
     * 用户手机职员绑定
     * @Author   ksea
     * @DateTime 2018-12-07T18:00:49+0800
     */
    public function BindWorkerUser(){
        $id=input('post.id','');
        $phone=input('param.phone','');
        $name=input('param.name','');
        $passwd=input('param.passwd','')?input('param.passwd','','md5'):input('param.passwd','');
        $worker_type=input('param.worker_type','');

        /***************************************/
        if(input('post.')){
            /****************id验证是否存在**********************/
            if($id){
               $u_data=Db::name('shop_userinfo')->where('id',$id)->find();
               if(!$u_data){
                 $this->error('用户不存在');
               }
            }
            else{
               $this->error('用户id不允许为空');
            }
            /******************id验证是否存在*********************/
            $validate=new Validate([
               'phone' =>'unique:shop_userinfo',
            ],[
               'phone.unique' =>'手机号码已存在',
            ]);
            $vali_data=[
              'phone' => $phone,
            ];
            if(!$validate->check($vali_data)){
               $this->error($validate->getError());
            }
        }
        /***************************************/
        if(request()->isAjax()){
           $val_user=new Validate([
             'id' => 'require',
             'phone' => 'require',
             'passwd' => 'require',
           ],[
             'id.require' => '账号不允许为空',
             'phone.require' => '手机号码不为空',
             'passwd.require' => '密码不允许为空',
           ]);
           $data=[
             'id' =>$id,
             'phone' =>$phone,
             'name' =>$name,
             'passwd' =>$passwd,
           ];
           /**********如果判断职员选择项存在*************/
           if(in_array($worker_type,[0,1])){
              $data['worker_type']=$worker_type;
           }
           /**********如果判断职员选择项存在*************/
           if($val_user->check($data)){

              Db::startTrans();
              try {
                $data['update_time']=time();
                Db::name('shop_userinfo')->update($data);
                Db::commit();
              } catch ( \Exception $e) {
                
                Log::init([
                  'func'  => 'BindUser',
                  'error' => $e,
                  'data'  => $data,
                ]);
                Db::rollback();
                $this->error('绑定失败');
              }
              session('user',model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where('id',$id)->find());

              $this->success('手机号码绑定成功');
           }
           else{
                $this->error($val_user->getError());
           }

        }
        return $this->fetch('user/bind_worker_user');
    }




    /**
     * 利用code获取access_token等个人用户信权(小程序)
     * @Author   ksea
     * @DateTime 2019-04-15T14:51:14+0800
     * @return   [type]                   [description]
     */
    public function  appletsgetcode(){

          $appid     = input('appid');
          $appsecret = input('appsecret');
          $pic       = input('pic');
          $nickname  = input('nickname');
          $code      = input('code'); 
          $pid       = input('pid','0');
          $mstore_id = input('mstore_id',config('sys.def_store'));
          $astore_id = input('mstore_id','');
          $userinfo  = input('userinfo','');
          $encryptedData=input('encryptedData','');
          $iv=  input('iv','');
          $set_localhost=input('local_url','mobile/index/index');
          $get_token_url = "https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$appsecret}&js_code={$code}&grant_type=authorization_code";
          $ch = curl_init($get_token_url);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_HEADER, true); 
          $res=curl_exec($ch);
          curl_close($ch);
          $res=strstr($res, "{");
          session('wechat_a.0',json_decode($res,true));
          session('wechat_a.1',input('get.'));



          /**
          * 获取微信唯一id
          */
          $session_token=session('wechat_a.0');
          $sessionKey =$session_token['session_key'];



          $pc = new WXBizDataCrypt($appid, $sessionKey);
          $errCode = $pc->decryptData($encryptedData, $iv, $data );
          if ($errCode == 0) {
              $session_token=json_decode($data,true);
          } else {
            dump($errCode);
          }

          $openid=$session_token['openId'];
          $unionid=$session_token['unionId'];
          session('wechat_a.2',$userinfo);
          $wechat_userdata=session('wechat_a.2');
          
          $param['request']['mini_openid']=$openid;
          $param['request']['unionid']=$unionid;

          $userModel=new \app\admin\model\shopuser\User;
          if(is_get_val('shop_userinfo',$unionid,'unionid','unionid')
          )
          {
            /***********同步公众号用户数据**************/
            $userModel->where('unionid',$unionid)->update(['mini_openid'=>$openid]);
            /***********同步公众号用户数据**************/
            /**********************自动登陆操作*****************************/
            session('user',$userModel->with(['userStore','qrcode','bean'])->where(['mini_openid'=>$openid])->find());
            /**********************自动登陆操作*****************************/

            $this->success('info','/',['data'=>session('user')]);
            exit();

          }
          $param['request']['json_wechat']=json_encode($wechat_userdata);

          $param['request']['pic']=isset($pic)?$pic:'https://pub-static.haozhaopian.net/static/web/site/features/cn/crop/images/crop_20a7dc7fbd29d679b456fa0f77bd9525d.jpg';

          $param['request']['nickname']=isset($nickname)?$nickname:'昵称未获取';
          $param['request']['pid']=$pid;
          $param['request']['mstore_id']=$mstore_id;
          Hook::listen('add_user',$param);
          /**********************自动登陆操作*****************************/
          session('user',model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['mini_openid'=>$openid])->find());
          /**********************自动登陆操作*****************************/
          $this->success('info','/',['data'=>session('user')]);
    }

    /**
     * 小程序登录接口
     * @auth JJ
     */
    public function miniLogin(){

        //是否已经登录
        if($user_ses = Session::get('user')){
            $need_phone = isset($user_ses['phone']) && $user_ses['phone']?0:1;
            session('user',model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['id'=>$user_ses['id']])->find());
            $this->success('info','/',['data'=>Session::get('user'),'need_phone'=>$need_phone]);
            exit();
        }
        $code      = input('code');
        $mstore_id = input('mstore_id',config('sys.def_store'));
        $encryptedData = input('encryptedData','');
        $iv =  input('iv','');
        $pid = input('pid','0');

        $appid = $this->mini_id;
        $appsecret = $this->mini_secret;
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$appsecret}&js_code={$code}&grant_type=authorization_code";
        $ret = curlGet($url);
        $ret = json_decode($ret,true);
        if(!isset($ret['session_key'])){
            $this->error('小程序授权失败');
        }
        $sessionKey = $ret['session_key'];

        $pc = new WXBizDataCrypt($appid, $sessionKey);
        $data = [];
        $errCode = $pc->decryptData($encryptedData, $iv, $data );

        if ($errCode == 0) {
            $session_token = json_decode($data,true);
        } else {
            dump($errCode);
        }

        $openid = $session_token['openId'];
        $unionid = $session_token['unionId'];
        $nickname = $session_token['nickName'];
        $pic = $session_token['avatarUrl'];

        $userModel=new \app\admin\model\shopuser\User;
        if(is_get_val('shop_userinfo',$unionid,'unionid','unionid')
        )
        {
            /***********同步公众号用户数据**************/
            $userModel->where('unionid',$unionid)->update(['mini_openid'=>$openid]);
            /***********同步公众号用户数据**************/
            /**********************自动登陆操作*****************************/
            $session_user=$userModel->with(['userStore','qrcode','bean'])->where(['mini_openid'=>$openid])->find();
            session('user',$session_user);
            model('admin/shop/AnalysisMix')->loginlog($session_user->toArray(),2);
            /**********************自动登陆操作*****************************/
            //是否有手机号码
            $need_phone = isset(session('user')->phone) && session('user')->phone?0:1;
            $this->success('info','/',['data'=>session('user'),'need_phone'=>$need_phone]);
            exit();

        }
        $param['request']['json_wechat']=json_encode($session_token);
        $param['request']['mini_openid'] = $openid;
        $param['request']['unionid'] = $unionid;
        $param['request']['pic'] = $pic;

        $param['request']['nickname'] = isset($nickname)?$nickname:'昵称未获取';
        $param['request']['pid'] = $pid;
        $param['request']['mstore_id'] = $mstore_id;
        Hook::listen('add_user',$param);
        /**********************自动登陆操作*****************************/
        $user = model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['mini_openid'=>$openid])->find()->toArray();
        session('user',$user);
        model('admin/shop/AnalysisMix')->loginlog($user,2);
        /**********************自动登陆操作*****************************/

        //邀请关系
        if ($user['pid'] > 0) {
          $inv = model('admin/shop/user')->invite($user);
        }
        Log::record(session('user'),'绑定5');
        $this->success('info','/',['data'=>session('user'),'need_phone'=>1]);
    }

    /**
     * 添加授权手机
     * @auth JJ
     */
    public function addPhoneNumber(){
        $code      = input('code');
        $encryptedData = input('encryptedData','');
        $iv =  input('iv','');
        $appid = $this->mini_id;
        $appsecret = $this->mini_secret;
        Log::record($code,'小程序-授权手机code');
        Log::record($encryptedData,'小程序-授权手机date');
        Log::record($iv,'小程序-授权手机iv');
        Log::record($appid.'-'.$appsecret,'小程序-授权手机appid');

        $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$appsecret}&js_code={$code}&grant_type=authorization_code";
        $ret = curlGet($url);
        Log::record($ret,'小程序-授权手机-session');
        $ret = json_decode($ret,true);
        if(!isset($ret['session_key'])){
            $this->error('小程序授权失败');
        }
        $sessionKey = $ret['session_key'];
        Log::record($sessionKey,'小程序-授权手机-sessionkey');
        $pc = new WXBizDataCrypt($appid, $sessionKey);
        $data = [];
        $errCode = $pc->decryptData($encryptedData, $iv, $data );
        Log::record($errCode,'小程序-授权手机-解密1');
        Log::record($data,'小程序-授权手机-解密2');
        if ($errCode == 0) {
            $session_token = json_decode($data,true);
        } else {
            dump($errCode);
        }
        $openid = $ret['openid'];
        //$unionid = $ret['unionid'];
        if(!isset($session_token['phoneNumber'])){
            Log::info('小程序手机授权失败:'.$data);
            $this->error('小程序手机授权失败');
        }
        $phoneNumber = $session_token['phoneNumber'];
        Log::record($phoneNumber,'小程序-授权手机-手机号码');
        $userModel=new \app\admin\model\shopuser\User;
        if($userModel->where(['phone'=>$phoneNumber])->find()){
            $userModel->where(['phone'=>$phoneNumber])->update(['mini_openid'=>$openid]);
        }else{
            $userModel->where(['mini_openid'=>$openid])->update(['phone'=>$phoneNumber]);
        }
        /**********************自动登陆操作*****************************/
        $user = model('admin/shopuser/User')->with(['userStore','qrcode','bean'])->where(['mini_openid'=>$openid])->find();
        session('user',$user);
        //记录
        model('admin/shop/AnalysisMix')->bindlog($user,2);
        $InviteModel=model('admin/shop/Invite');
        $uid = isset($user['id'])?$user['id']:'';
        $InviteModel->where(['uid'=>$uid])->update(['tel'=>$phoneNumber]);
        /**********************自动登陆操作*****************************/
        $this->success('info','/',['data'=>session('user'),'need_phone'=>0]);
    }
}
