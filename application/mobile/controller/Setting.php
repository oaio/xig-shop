<?php
/*
 * This file is part of the thinkphp.
 *
 * Copyright (c) 2019, xiaogebang.com
 *
 * This source file is subject to the CIA license that is bundled
 * with this source code in the file LICENSE.
 */
namespace app\mobile\controller;

use app\mobile\common\Controllercom;
/**
 * Seting.php  我的-设置
 *
 * @author d3li <d3li@sina.com>
 * @create：1/3/2019  5:10 PM
 * @see      https://gitee.com/d3li/January
 * @version 1.0.0103
 * @describe
 */
class Setting extends Controllercom
{
    public function _initialize()
    {
        parent::_initialize();
    }

	public function index()
	{
		return $this->fetch();
	}

	public function feedback()
	{
		return $this->fetch();
	}

	public function about()
	{
		return $this->fetch();
	}

	public function setting()
	{
		return $this->fetch();
	}
}