<?php

namespace app\mobile\controller;

use think\Db;
use app\mobile\common\Controllercom;

class Vip extends Controllercom
{
    public function _initialize()
    {
        parent::_initialize();
    }
    /**
     * 会员套餐
     * @Author   ksea
     * @DateTime 2019-03-21T14:43:59+0800
     * @return   [type]                   [description]
     */
    public function index(){

        $token=request()->token();
        $this->assign('__token__',$token);
        return $this->fetch();
        
    }
}