<?php

namespace app\mobile\controller;

use think\Db;
use wechat\Jssdk;
use app\mobile\common\Controllercom;
use think\Session;

class Order extends Controllercom
{
    public function _initialize()
    {
        parent::_initialize();
        if(!Session::get('user')){
           $this->redirect('User/login');
        }
    }
    /**
     * 订单页面
     * @Author   ksea
     * @DateTime 2018-12-11T10:01:03+0800
     * @return   [type]                   [description]
     */
    public function index(){
    	return $this->fetch();
    }
    /**
     * 订单详情
     * @Author   ksea
     * @DateTime 2018-12-11T10:00:36+0800
     * @return   [type]                   [description]
     */
    public function detail(){
    	return $this->fetch();
    }
    /**
     * 评论
     * @Author   ksea
     * @DateTime 2019-02-12T15:29:59+0800
     * @return   [type]                   [description]
     */
    public function evaluate(){

        return $this->fetch();
    }
    /**
     * 确认支付订单
     * @Author   ksea
     * @DateTime 2019-03-01T15:41:37+0800
     * @return   [type]                   [description]
     */
    public function order_details(){
        $token=request()->token();
        $this->assign('__token__',$token);

        return $this->fetch();
    }

}