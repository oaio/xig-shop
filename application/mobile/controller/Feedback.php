<?php

namespace app\mobile\controller;

use think\Db;
use think\Controller;

class Feedback extends Controller
{
	public function index(){

	}
	/**
	 * 反馈
	 */
	public function FeedBack(){
		if(input('post.')){
			$data=[
				 'title' =>input('post.title','普通反馈'),
				 'feedback' => input('post.feedback',''),
				 'img_arr' => input('post.img_arr',''),
				 'uid' => session('user.id'),
			];
			$rule=[
				//'title' => 'require',
				'feedback' => 'require',
				//'img_arr' => 'require',
				'uid' => 'require',
			];
			if(validate()->make($rule)->check($data)){
				$ret=Db::name('shop_feedback')->insert($data);
				if($ret){
					$this->success('感谢您的反馈!');
					exit();
				}
				else{
					$this->error('提交失败');
					exit();
				}
			}
			else{
				$msg=validate()->make($rule)->GetError();
				$this->error($msg);
			}
		}
		return $this->fetch();
	}
}