<?php

namespace app\mobile\controller;

use think\Db;
use app\mobile\common\Controllercom;
use wechat\Jssdk;
use think\Hook;

class Userstore extends Controllercom
{
    public function _initialize()
    {
        // $review=Db::name('shop_userinfo_store_review')->where(['uid'=>session('user.id')])->find();
        // if(empty($review)){
        //          $this->redirect(config('mobileUrl.applica')); 
        // }
        // else{
        //     if($review['status']==0){
        //          $this->redirect(config('mobileUrl.audit'));
        //     }
        //     elseif ($review['status']==1) {
                
        //     }
        //     elseif ($review['status']==2) {
        //         $this->redirect(config('mobileUrl.user'));
        //     }
        //     else{
        //         $this->redirect(config('mobileUrl.user'));
        //     }

        // }
        parent::_initialize();
    }
    public function index(){
    	return $this->fetch();
    }
    /**
     * 小店更新
     * @Author   ksea
     * @DateTime 2019-03-29T10:43:33+0800
     * @return   [type]                   [description]
     */
    public function amend(){
        $token=request()->token();
        $this->assign('__token__',$token);
        return $this->fetch();
    }
    /**
     * 店铺管理
     * @Author   ksea
     * @DateTime 2019-04-11T11:06:38+0800
     * @return   [type]                   [description]
     */
    public function storeshow(){
        $token=request()->token();
        $this->assign('__token__',$token);
        $from=input('to','');
        $session_uid=input('fromuid','');
        $to=$session_uid?$session_uid:session('user.id');

        if (empty($from)) {
            $Muser = model('admin/shopuser/user');
            $myUser = $Muser->where(['id' => $to])->find();
            if (!empty($myUser)) {
                $relationModel = model('WechatUserRelation');
                $relationInfo = $relationModel->where(['openid' => $myUser['openid']])->order('id ASC')->find();
                if (!empty($relationInfo) && !empty($relationInfo->userid)) {
                    $from = $relationInfo->userid;
                }
            }
        }

        if(
            isset($from)&&
            isset($to)&&
            !empty($from)&&
            !empty($to)
        ){
            $arr=['uid'=>$to,'fuid'=>$from];
            Hook::listen('fansswitch',$arr);
        }

        return $this->fetch();
    }
    /**
     * 
     * @Author   ksea
     * @DateTime 2019-04-13T15:17:56+0800
     * @return   [type]                   [description]
     */
    public function attestation(){

        $token=request()->token();
        $this->assign('__token__',$token);
        return $this->fetch();
      
    }
}