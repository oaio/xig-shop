<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/29
 * Time: 15:55
 */

namespace app\mobile\controller;
use wechat\Map;

class Maps
{
    public function index($lat1,$lng1,$lat2,$lng2){
        $mapp=new Map();
        $s=$mapp->GetDistance($lat1,$lng1,$lat2,$lng2);
        return $s;
    }

    /**
     * 标记大概的距离，做出友好的距离提示
     * @param [$number] 距离数量
     * @return[String] 距离提示
     */
    function mToKm($lat1,$lng1,$lat2,$lng2)
    {
        $mapp=new Map();
        $number=$mapp->GetDistance($lat1,$lng1,$lat2,$lng2);
        if (!is_numeric($number)) return ' ';
        switch ($number) {
            case $number > 1800 && $number <= 2000:
                $v = '2';
                break;

            case $number > 1500 && $number <= 1800:
                $v = '1.8';
                break;

            case $number > 1200 && $number <= 1500:
                $v = '1.5';
                break;
            case $number > 1000 && $number <= 1200:
                $v = '1.2';
                break;

            case $number > 900 && $number <= 1000:
                $v = '1';
                break;

            default:

                $v = ceil($number / 100) * 100;
                break;

        }

        if ($v < 100) {
            $v = '距离我【<font color="#FF4C06"><b>' . $v . '</b></font>】公里内。';
        } else {
            $v = '距离我【<font color="#FF4C06"><b>' . $v . '</b></font>】米内。';
        }
        return $v;
    }



}