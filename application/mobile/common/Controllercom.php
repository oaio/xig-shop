<?php
namespace app\mobile\common;

use wechat\Jssdk;
use wechat\Wechat;
use wechat\WechatAuth;
use wechat\WechatCrypt;
use think\Controller;
use think\Validate;
use think\Hook;
use think\Db;
class Controllercom extends Controller
{
  protected $id,$secret,$token,$web_url;

  public function _initialize(){

    $ua = $_SERVER['HTTP_USER_AGENT'];
    if(strpos($ua, 'MicroMessenger')){
       if(
         !session('wechat_a.2')||
         !session('wechat_a.1')||
         !session('wechat_a.0')||
         is_check_true(session('wechat_a.2')['openid'],'shop_userinfo','openid')||
         !session('user')
       ){
                controller('Controllercom')->code();
         }
         else{
              /************输出openid到页面**************/
             $openid=session('user.openid');
             echo "<script type='text/javascript'>
                    localStorage.setItem('openid','".$openid."');
                  </script>";
              /************输出openid到页面**************/

              /****绑定店铺切换*****/
              if(input('param.mstore_id','')){
                $userModel=new \app\admin\model\shopuser\User;
                $mstore_id=input('param.mstore_id','');

                $res=$userModel->where('openid',$openid)->update(['store_id'=>$mstore_id]);
                /***************店铺切换记录钩子***************/
                if(strlen($mstore_id)>0){
                  $param['store_id']=$mstore_id;
                  $param['uid']     =session('user.id');
                  Hook::listen('switch_store',$param);
                }
                /***************店铺切换记录钩子***************/
              }
              /****绑定店铺切换*****/
         }
    }
    else{
        /****绑定店铺切换*****/
        $openid=session('user.openid');
        if(input('param.mstore_id','')){
          $userModel=new \app\admin\model\shopuser\User;
          $mstore_id=input('param.mstore_id','');

          $res=$userModel->where('openid',$openid)->update(['store_id'=>$mstore_id]);
          /***************店铺切换记录钩子***************/
          if(strlen($mstore_id)>0){
            $param['store_id']=$mstore_id;
            $param['uid']     =session('user.id');
            Hook::listen('switch_store',$param);
          }
          /***************店铺切换记录钩子***************/
        }
        /****绑定店铺切换*****/
    }

    /********粉丝数据初始化*********/

      $from=input('to','');
      $session_uid=input('fromuid','');
      $to=$session_uid?$session_uid:session('user.id');

      if (empty($from)) {
          $Muser = model('admin/shopuser/user');
          $myUser = $Muser->where(['id' => $to])->find();
          if (!empty($myUser)) {
              $relationModel = model('WechatUserRelation');
              $relationInfo = $relationModel->where(['openid' => $myUser['openid']])->order('id ASC')->find();
              if (!empty($relationInfo) && !empty($relationInfo->userid)) {
                  $from = $relationInfo->userid;
              }
          }
      }



      if(
          isset($from)&&
          isset($to)&&
          !empty($from)&&
          !empty($to)
      ){
          $arr=['uid'=>$to,'fuid'=>$from];
          Hook::listen('fansswitch',$arr);
      }

    /********粉丝数据初始化*********/

    $Jssdk=new Jssdk(config('wechat.appid'),config('wechat.secret'));
    $jssdk_res=$Jssdk->getSignPackage();
    $this->assign('jssdk',$jssdk_res);
    //css样式修改
    //echo '<div class="pop"v-if="money!=0"><div class="big"><div class="cash"><div class="circle"></div><img class="tlile"src="'.PUBLIC_RES.'/png/iconic/logo.png"><span>给您发个现金红包</span><img @tap="money=2"v-show="money==1"class="open"src="'.PUBLIC_RES.'/png/iconic/XIAOKAI.png"></div><transition name="fade"><div class="circleShow"v-show="money==2"><div class="board"><h3>恭喜已获得</h3><span><strong>2</strong>元</span><p>价值2小哥豆</p></div></div></transition><div class="closed"@tap="money=0"><img src="'.PUBLIC_RES.'/png/iconic/guanbi.png"></div><div class="use"v-show="money==2">立即使用</div></div></div>';
  }
  /**
   * 空操作
   * @Author   ksea
   * @DateTime 2018-12-13T14:24:15+0800
   * @param    [type]                   $name [description]
   * @return   [type]                         [description]
   */
  public function _empty($name){
     $view=request()->controller().'/'.$name;
     return $this->fetch(strtolower($view));
  }
}

