<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Env;

return [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------
    // 应用命名空间
    'app_namespace'          => 'app',
    // 应用调试模式
    'app_debug'              => Env::get('app.debug', true),
    // 应用Trace
    'app_trace'              => Env::get('app.trace', false),
    'web_host'               => \think\Request::instance()->domain(),       
    // 是否存储base64
    'base64'                 => false,
    // 应用模式状态
    'app_status'             => '',
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 扩展函数文件
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'PRC',
    // 是否开启多语言
    'lang_switch_on'         => true,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,
    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------
    // 默认模块名
    'default_module'         => 'mobile',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => true,
    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------
    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由
    'url_route_on'           => true,
    // 路由使用完整匹配
    'route_complete_match'   => false,
    // 路由配置文件（支持配置多个）
    'route_config_file'      => ['route'],
    // 是否强制使用路由
    'url_route_must'         => false,
    // 域名部署
    'url_domain_deploy'      => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------
    'template'               => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'         => 'Think',
        // 模板路径
        'view_path'    => '',
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板文件名分隔符
        'view_depr'    => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin'    => '{',
        // 模板引擎普通标签结束标记
        'tpl_end'      => '}',
        // 标签库标签开始标记
        'taglib_begin' => '{',
        // 标签库标签结束标记
        'taglib_end'   => '}',
        'tpl_cache'    => true,
    ],
    // 视图输出字符串内容替换,留空则会自动进行计算
    'view_replace_str'       => [
        '__PUBLIC__' => '',
        '__ROOT__'   => '',
        '__CDN__'    => '',
    ],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl'    => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'dispatch_jump.tpl',
    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------
    // 异常页面的模板文件
    'exception_tmpl'         => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'think_exception.tpl',
    // 错误显示信息,非调试模式有效
    'error_message'          => '你所浏览的页面暂时无法访问',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',
    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------
    'log'                    => [
        // 日志记录方式，内置 file socket 支持扩展
        'type'  => 'File',
        // 日志保存目录
        'path'  => LOG_PATH,
        // 日志记录级别
        'level' => [],
    ],
    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace'                  => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],
    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------
    'cache'                  => [
        // 驱动方式
        'type'   => 'File',
        // 缓存保存目录
        'path'   => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],
    // +----------------------------------------------------------------------
    // | 会话设置
    // +----------------------------------------------------------------------
    'session'                => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
    ],
    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie'                 => [
        // cookie 名称前缀
        'prefix'    => '',
        // cookie 保存时间
        'expire'    => 0,
        // cookie 保存路径
        'path'      => '/',
        // cookie 有效域名
        'domain'    => '',
        //  cookie 启用安全传输
        'secure'    => false,
        // httponly设置
        'httponly'  => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],
    //分页配置
    'paginate'               => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],
    //验证码配置
    'captcha'                => [
        // 验证码字符集合
        'codeSet'  => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY',
        // 验证码字体大小(px)
        'fontSize' => 18,
        // 是否画混淆曲线
        'useCurve' => false,
        //使用中文验证码
        'useZh'    => false,
        // 验证码图片高度
        'imageH'   => 40,
        // 验证码图片宽度
        'imageW'   => 130,
        // 验证码位数
        'length'   => 4,
        // 验证成功后是否重置
        'reset'    => true
    ],
    // +----------------------------------------------------------------------
    // | Token设置
    // +----------------------------------------------------------------------
    'token'                  => [
        // 驱动方式
        'type'     => 'Mysql',
        // 缓存前缀
        'key'      => 'i3d6o32wo8fvs1fvdpwens',
        // 加密方式
        'hashalgo' => 'ripemd160',
        // 缓存有效期 0表示永久缓存
        'expire'   => 0,
    ],
    //FastAdmin配置
    'fastadmin'              => [
        //是否开启前台会员中心
        'usercenter'          => true,
        //登录验证码
        'login_captcha'       => false,
        //登录失败超过10则1天后重试
        'login_failure_retry' => true,
        //是否同一账号同一时间只能在一个地方登录
        'login_unique'        => false,
        //登录页默认背景图
        'login_background'    => "/assets/img/loginbg.jpg",
        //是否启用多级菜单导航
        'multiplenav'         => false,
        //自动检测更新
        'checkupdate'         => false,
        //版本号
        'version'             => '1.0.0.20181031_beta',
        //API接口地址
        'api_url'             => 'https://api.fastadmin.net',
        'v_api'               => '3.0.6',
    ],
    // +----------------------------------------------------------------------
    // | 系统设置
    // +----------------------------------------------------------------------
    // 
    'sys'                    =>[
         'name'                 => '小哥帮',
         'def_store'         =>0,

    ],
    // +----------------------------------------------------------------------
    // | 数据表配置
    // +----------------------------------------------------------------------
    /**
     * 可选表范围
     */
    'select_table_area'     =>[
         'admin',
         'shop_address',
         'shop_card',
         'shop_categrory',
         'shop_code',
         'shop_discount',
         'shop_goods',
         'shop_goods_detail',
         'shop_img',
         'shop_login_log',
         'shop_order',
         'shop_pay_log',
         'shop_service_order',
         'shop_sku',
         'shop_sku_group',
         'shop_userinfo',
         'shop_wechat',
         'store',
         'employee',
         'wechat_pay',
         'viprecord',
         'vipcard',
    ],
    /**
     * 可选字段范围
     */
    'select_field_area'       =>[
        'id',
        'name',
        'status',
        'sleep',
        'openid',
        'phone',
        'mycode',
        'type',
        'serverid',
        'cardid',
        'price',
        'vipprice',
        'commonprice',
        'unit',
        'pic',
        'inoc',
        'status',
        'create_time',
        'update_time',
        'sngoods',
        'is_type',
        'admin_id',
        'power',
        'nickname',
        'address',
        'addressid',
        'times',
        'sum_times',
        'uid',
        'goodsid',
        'store_id',
        'burse',
        'percent',
        'company',
        'sex',
        'attach',
        'note',
        'total_fee',
        'commonprice',
        'grabprice',
        'group_number',
        'area_id',
        'vip_end_time',
        'cate',
        'orderid',
        'unionid',
    ],
    /**
     * 客服手机号
     */
    'kefu_mobile'       =>'13549906145',
    'code_path'         =>'./qrcode',
    'pay_url'           =>\think\Request::instance()->domain().'/mobile/pay/pay', //支付地址
    /**
     * 加盟店状态
     */
    
    /**
     * 微信模板消息模板设置
     * 
     */
    //测试
    // 'wechattemplate'=>[
        
    //     //抢单模板消息
    //     'grab' => 'ussXZ-nQwYAT1mZPzmDaBP23KoOPqJEHWspb_6R4Q30',

    //     //商家收款通知
    //     'storeorder' =>[
    //         'tempid' =>'MlCAx6tzkVg_2FtCryRL_unSH0IvLwA2N8_V7GrHs_0',
    //         'link' =>'https://storedev.oaio.online',
    //     ],

    //     //兑换券获取模板
    //     'bargain' =>[
    //         'tempid' =>'ZcNBcly_B1avNiCgzEvO6CYqXV--pe3v8--24gP0qmk',
    //         'link' =>'https://g.oaio.online/mobile/invitebargain',//邀请相关页面
    //     ],

    //     //订单追加模板信息
    //     'appendpay' =>[
    //         'tempid' => 'q8n9iwDQKm5xUHk4erBiXgfPFTa4Zm3Hw-MFQoOAxwM',
    //         'link'   => '',//'https://g.oaio.online/aunt',//默认跳转链接
    //     ],
	   //  'cash' => '18kO4mePqL_YPTYnpcWGZpznapPYelUlHNJ--_cJzq4',//提现审核模板消息
	   //  'bean' => 'djot2ds1Edvbl_YX_fTwEozzOUpfyjYitTy2NBZkKPs',//小哥豆模板消息
    // ],
    //线上
    'wechattemplate'=>[
        
        //抢单模板消息
        'grab' => '5GFg-fr0U61ilLMxbPG__dzrSns-67TzdPNtxZ8u-1I',

        //商家收款通知
        'storeorder' =>[
            'tempid' =>'PZUlx0thAMs_jyL2b0-FsF9iffxyqhqPdQiiDznNVmc',
            'link' =>'https://storedev.oaio.online',
        ],

        //兑换券获取模板
        'bargain' =>[
            'tempid' =>'CMheJeZoMKE8xfwacQDbmKg3U1ZO0-cNwFAXvt73KBQ',
            'link' =>'https://g.oaio.online/mobile/invitebargain',//邀请相关页面
        ],

        //订单追加模板信息
        'appendpay' =>[
            'tempid' => 'q8n9iwDQKm5xUHk4erBiXgfPFTa4Zm3Hw-MFQoOAxwM',
            'link'   => '',//'https://g.oaio.online/aunt',//默认跳转链接
        ],
        'cash' => 'HOkw3MR_JoEXptbyUnHyevxB3m4u0xaOjI6jYz8sMuw',//提现审核模板消息
        'bean' => '2SlkO1Wemb_BOKkXg5efIgnab-yQij_isHdR6HbqOrs',//小哥豆模板消息
    ],
    'is_af'             =>[
        ['0','未开通'],
        ['1','开通'],
        ['2','已开通'],
        ['3','冻结'],
        ['4','已冻结'],
        ['5','重新开通'],
        ['2','已开通'],
    ],
    'aunt'  =>[
        'can_see'   =>[1,5],
    ],

    /**
     * 抢单配置
     */
    'Grab' =>[
        'effective' => 60*30,//可以抢单时间
    ],
    /**
     * 客服配置
     */
    'Customer' =>[
         'send' =>60*30,//自动转派单时间限制s
    ],
    /**
     * 虚拟号配置
     */
    'Fictitious'=>[
        'mobile_status'=>1,//虚拟号默认配置,1使用虚拟号，不适用虚拟号
    ],
    /**
     * 红包分利配置（每次获取）
     */
    'RedEnvelopes'=>[
	    '0'=>[
            'min'=>0.5,
            'max'=>2,
        ],
	    '500'=>[
		    'min'=>0.5,
		    'max'=>3,
	    ],

    ],
];
