<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init'     => [],
    // 应用开始
    'app_begin'    => [],
    // 模块初始化
    'module_init'  => [
        'app\\common\\behavior\\Common',
    ],
    // 插件开始
    'addon_begin'  => [
        'app\\common\\behavior\\Common',
    ],
    // 操作开始执行
    'action_begin' => [],
    // 视图内容过滤
    'view_filter'  => [],
    // 日志写入
    'log_write'    => [],
    // 应用结束
    'app_end'      => [],
    
    /*********************公共锚点**************************/

    /**
     * 设置卡片
     */
    'set_card'    =>[
        'app\\mobile\\behavior\\Card',
    ],

    /**
     * 店铺自发非小店分销
     */
    'distribution'    =>[
        'app\\mobile\\behavior\\Distribution',
        //'app\\mobile\\behavior\\Smsstore',支付完成订单通知
        'app\\mobile\\behavior\\Ordersuccess',
    ],


    /**
     * 拼团(废弃)
     */
    // 'group_buying'  =>[
    //     'app\\mobile\\behavior\\Assemble',
    // ],

    /**
     * 抢单模版消息钩子
     * 2019-03-12 23:27:21
     */
    'stemplate'  =>[
        'app\\mapiv2\\behavior\\Grab',
    ],

    /**
     * 预约时间记录钩子
     */
    'servercache'           =>[
        'app\\mobile\\behavior\\Servercache',
    ],

    /**
     * 预约时间记录钩子
     */
    'servercache'           =>[
        'app\\mobile\\behavior\\Servercache',
    ],


    /**
     * 贡献钩子
     */
    'outcome'  =>[
        'app\\mobile\\behavior\\Outcome',
    ],

    /**
     * 订单钩子
     */
    'order'  =>[
        'app\\mobile\\behavior\\diyttemplate',
    ],

    'orderset'     => [], //下单完成
    'serverset'    => [], //预约完成
    'orderpay'     => [], //订单支付
    'vippay'       => [], //会员支付
    'zeropay'      => [], //零元支付
    'custompay'    => [], //自定义支付
    'addpay'       => [
        'app\\mobile\\behavior\\WechatTemplate',
    ], //追加支付
    'server_begin' => [], //服务开始
    'server_end'   => [
        'app\\mobile\\behavior\\Assess'//评价
    ], //服务结束

    /*********************公共锚点**************************/

];
